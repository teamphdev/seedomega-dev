DROP FUNCTION IF EXISTS `GetAgency`;
DROP FUNCTION IF EXISTS `GetAgents`; 
DROP FUNCTION IF EXISTS `GetAncestry`;
DROP FUNCTION IF EXISTS `GetChildrenByID`;
DROP FUNCTION IF EXISTS `GetCommissionPerBooking`;
DROP FUNCTION IF EXISTS `GetFileSlug`;
DROP FUNCTION IF EXISTS `GetLatestReplyDate`;
DROP FUNCTION IF EXISTS `GetLatestReplyPerClient`;
DROP FUNCTION IF EXISTS `GetParent`;
DROP FUNCTION IF EXISTS `GetSale`;
DROP FUNCTION IF EXISTS `GetTotalPost`;
DROP FUNCTION IF EXISTS `GetTotalRepliesPerClient`;
DROP FUNCTION IF EXISTS `GetTotalReply`;
DROP FUNCTION IF EXISTS `GetTotalTopicsPerClient`;
DROP FUNCTION IF EXISTS `GetTotalViewsPerClient`;

DROP TABLE `accounts`, `accounts_request`, `account_beneficiaries`, `account_commissions`, `account_commission_rates`, `account_fee_rates`, `account_meta`, `activity_logs`, `bank_accounts`, `blogs`, `blog_categories`, `bookings`, `claims`, `clients_team`, `clients_transactions`, `client_categories`, `client_profiles`, `client_profiles_request`, `client_soa`, `comments`, `commission_overriding`, `commission_personal`, `commission_scheme`, `commission_statements`, `contact_us`, `core_bank_accounts`, `core_team`, `email_cron`, `email_queue`, `email_templates`, `faqs`, `files`, `file_categories`, `file_groups`, `file_items`, `file_sub_categories`, `forum`, `forum_categories`, `forum_replies`, `forum_sub_categories`, `forum_topics`, `icons`, `invoices`, `invoices_draft`, `invoice_items`, `invoice_type`, `options`, `option_groups`, `payments`, `products`, `product_items`, `referral_earnings`, `slides`, `subscription_packages`, `terms_and_conditions`, `testimonials`, `tickets`, `ticket_events`, `users`, `user_capabilities`, `user_capability_groups`, `user_levels`, `user_meta`, `user_referrer`, `wallet`, `wallet_method`;