<?php
/**
 * PHP 4, 5, 7
 *
 * MyPHPFrame : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location    /index.php
 * @package     app
 * @version     MyPHPFrame v3.0.0
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 
/**
 *  Get install directory, must be defined here
 */
define('INSTALLDIR', dirname(__FILE__));

// Load bootstrap
require_once 'inc/bootstrap.php';