<?php
/**
 * Menu Walker Class
 *
 * @category   Menu
 * @package    AppMenu
 * @author     Mo <mogol@gmail.com>
 * @copyright  (c) 2018 TechHon Web Solutions
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0.0
 * @link       http://www.techhon.com
 * @since      Class available since Release 1.2.0
 */
class AppMenu { 

	static public function general()
	{
		$userinfo = User::info();
        // Register main menu for seeders
        Menu::item( 'dashboard', array('Label' => 'Home <span>Dashboard</span>', 'Slug'  => $userinfo->DashboardURL) );
	}

	static public function seeder()
	{
		$bookCounts = AppUtility::getBookCountByUserID();
		$pendingBdg = ($bookCounts['BookingPending']) ? 'notif-badge' : '';
        $pending    = ($bookCounts['BookingPending']) ? '<i class="badge pull-right badge-danger">'.intval($bookCounts['BookingPending']).'</i>' : '';
        $verified   = ($bookCounts['BookingVerified']) ? '<i class="badge pull-right badge-default">'.intval($bookCounts['BookingVerified']).'</i>' : '';
        $registered = ($bookCounts['InterestPending']) ? '<i class="badge pull-right badge-default">'.intval($bookCounts['InterestPending']).'</i>' : '';
        $approved   = ($bookCounts['BookingApproved']) ? '<i class="badge pull-right badge-default">'.intval($bookCounts['BookingApproved']).'</i>' : '';

        // Register main menu for seeders
        Menu::item( 'userseeds', array('Label' => 'My Seeds <span>Funded Info</span>','Class' => $pendingBdg, 'Slug'  => 'bookings') );
        Menu::subitem( 'userseeds','pending', array('Label' => 'Pending Booking ' .$pending,'Slug'  => 'bookings/pending') );
        Menu::subitem( 'userseeds','verified', array('Label' => 'Verified Booking '.$verified,'Slug'  => 'bookings/verified') );
        Menu::subitem( 'userseeds','registered', array('Label' => 'Registered Booking '.$registered,'Slug'  => 'bookings/registered') );
        Menu::subitem( 'userseeds','approved', array('Label' => 'Approved Booking '.$approved,'Slug'  => 'bookings/approved') );

        Menu::item( 'userblogs', array('Label' => 'Blogs <span>What\'s New</span>','Slug'  => 'blogs') );
        Menu::item( 'userforums', array('Label' => 'Forums <span>Let\'s Discuss</span>','Slug'  => 'forums') );
        Menu::item( 'support', array('Label' => 'Support <span>Technical Assistance</span>','Slug'  => 'support/dashboard') );
	}		

	static public function client()
	{
		$userinfo = User::info();

		$myacct = $userinfo->Code == 'CLN' ? 'My Account' : 'Company Account';
        $manage = $userinfo->Code == 'CLN' ? 'Assistants, Blogs, FAQs & Teams' : 'Blogs, FAQs & Teams';
        $userID = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
        $blog = AppUtility::getClientBlog( $userID );
        $totalnotapprove = AppUtility::getBookedUsersCount( 'TotalNotApprove' );
        $notapprovebadge = $totalnotapprove != 0 ? 'notif-badge' : '';

        $totalInterest = AppUtility::getBookedUsersCount( 'TotalRegistered' );
        $registered = ( $totalInterest != 0 ) ? '<i class="badge pull-right badge-default">'.$totalInterest.'</i>' : '';

        $totalverified = AppUtility::getBookedUsersCount( 'TotalVerified' );
        $verified = ( $totalverified != 0 ) ? '<i class="badge pull-right badge-danger">'.$totalverified.'</i>' : '';

        $nottreceiptcount = AppUtility::getClientInvoicesNoTTReceipt();
        $invoices = ( $nottreceiptcount != 0 ) ? '<i class="badge pull-right badge-danger">'.$nottreceiptcount.'</i>' : '';

        // Register main menu for clients
        Menu::item( 'fundings', array('Label' => 'Fundings <span>Booking & Invoices</span>','Class' => $notapprovebadge, 'Slug'  => '#', 'Segment1' => array('booked','funds','invoices')) );
        Menu::subitem( 'fundings','seeders', array('Label' => 'Seeders ','Slug'  => 'clients/booked') );
        Menu::subitem( 'fundings','registered', array('Label' => 'Registered '.$registered,'Slug'  => 'clients/booked/registeredinterest') );
        Menu::subitem( 'fundings','acknowledgement', array('Label' => 'Pending Fund Acknowledgement '.$verified,'Slug'  => 'clients/funds') );
        Menu::subitem( 'fundings','invoices', array('Label' => 'Invoices '.$invoices,'Slug'  => 'clients/invoices') );

        Menu::item( 'community', array('Label' => 'Community <span>'.$manage.'</span>', 'Slug' => '#', 'Segment1' => array('assistant','blog','categories','forums','faq','team','activitylogs')) );
        if( $userinfo->Code == 'CLN' ){
            Menu::subitem( 'community','assistants', array('Label' => 'Assistants','Slug'  => 'clients/assistant/manage') );
        }
        Menu::subitem( 'community','blogs', array('Label' => 'Blogs','Slug'  => 'clients/blog/manage/'.$blog->ClientProfileID) );
        Menu::subsubitem( 'community','blogs','categories', array('Label' => 'Categories','Slug'  => 'blogs/categories') );
        Menu::subitem( 'community','forums', array('Label' => 'Forums','Slug'  => 'clients/forums') );
        Menu::subsubitem( 'community','forums','topics', array('Label' => 'Topics', 'Slug' => 'forums/topics' ) );
        Menu::subsubitem( 'community','forums','categories', array('Label' => 'Categories','Slug'  => 'forums/categories') );
        Menu::subsubitem( 'community','forums','subcategories', array('Label' => 'Sub Categories','Slug'  => 'forums/subcategories') );
        Menu::subitem( 'community','faqs', array('Label' => 'FAQs','Slug'  => 'clients/faq/manage/'.$blog->ClientProfileID) );
        Menu::subitem( 'community','teams', array('Label' => 'Teams','Slug'  => 'clients/team/manage') );
        if( $userinfo->Code == 'CLN' ){
            Menu::subitem( 'community','activitylogs', array('Label' => 'Activity Logs','Slug'  => 'clients/activitylogs') );
        }
        Menu::item( 'support', array('Label' => 'Support <span>Technical Assistance</span>','Slug'  => 'support/dashboard') );
	}

	static public function accounting()
	{
		$userinfo = User::info();

		$unpaidcount = AppUtility::getUnpaidInvoiceCount();
        $pendingwithdrawalcount = AppUtility::getPendingWithdrawalCount();
        $noinvoicecount = AppUtility::getNoInvoiceCount();

        $notifcnt = ( $unpaidcount + $noinvoicecount ) > 0 ? 'notif-badge' : '';
        $invoicecnt = ($noinvoicecount != 0 ) ? '<i class="badge pull-right badge-danger"> '.$noinvoicecount.'</i>' : '';

        // Register main menu for seeders
        Menu::item( 'fundings', array('Label' => 'Funds<span>Accounting</span>','Class' => $notifcnt, 'Slug'  => '#') );
        Menu::subitem( 'fundings','invoices', array('Label' => 'Invoices '.$invoicecnt,'Slug'  => 'invoices') );
    	// Menu::subitem( 'fundings','banks', array('Label' => 'Client Bank Accounts ','Slug'  => 'invoices/banks') );
        Menu::subitem( 'fundings','create', array('Label' => 'Create New Invoice ','Slug'  => 'invoices/create') );

        Menu::item( 'manage', array( 'Label' => 'Manage<span>affiliates, bank, wallet, invoice</span>', 'Slug'  => '#') );
        Menu::subitem( 'manage', 'affiliates', array('Label' => 'Affiliates','Slug'  => 'affiliates') );
        Menu::subsubitem( 'manage', 'affiliates', 'earnings', array('Label' => 'Earnings & Withdrawal', 'Slug'  => 'invoices/withdrawal') );
        Menu::subitem( 'manage', 'clientbank', array( 'Label' => 'Client Bank Accounts', 'Slug'  => 'invoices/banks') );
        if( User::can( 'Administer All' ) || User::can( 'Manage Core Bank Accounts' ) ){ 
            Menu::subitem( 'manage', 'sobank', array( 'Label' => 'SO Bank Accounts', 'Slug'  => 'corebankaccounts') );
        }
        if( User::can( 'Manage Wallet' ) ){
            Menu::subitem( 'manage', 'wallet', array('Label' => 'Wallet','Slug'  => '#') );
            Menu::subsubitem( 'manage', 'wallet', 'cashinpending', array('Label' => 'Cash-In : Pending','Slug'  => 'wallet/cashins/pending') );
            Menu::subsubitem( 'manage', 'wallet', 'cashin', array('Label' => 'Cash-In : Approved','Slug'  => 'wallet/cashins/approved') );
            Menu::subsubitem( 'manage', 'wallet', 'cashoutpending', array('Label' => 'Cash-Out : Pending','Slug'  => 'wallet/cashouts/pending') );
            Menu::subsubitem( 'manage', 'wallet', 'cashout', array('Label' => 'Cash-Out : Approved','Slug'  => 'wallet/cashouts/approved') );
        }
        Menu::subitem( 'manage', 'invoice_type', array( 'Label' => 'Invoice Type', 'Slug' => 'manage/invoice_type') );
        Menu::subitem( 'manage', 'packages', array( 'Label' => 'Subscription Package', 'Slug' => 'packages') );

     //    Menu::item( 'affiliates', array('Label' => 'Affiliates<span>Referrals & Earnings</span>','Slug'  => 'affiliates') );
     //    Menu::subitem( 'affiliates','withdrawals', array('Label' => 'Affiliate Earnings & Withdrawal ','Slug'  => 'invoices/withdrawal') );

     //    if( User::can( 'Administer All' ) || User::can( 'Manage Core Bank Accounts' ) ) { 
	    //     Menu::item( 'sobank', array('Label' => 'SO Bank Account <span>Account for Earnings</span>','Slug'  => 'corebankaccounts') );
	    // }

     //    if( User::can( 'Manage Wallet' ) ){
     //    	Menu::item( 'wallet', array('Label' => 'Wallet<span>Manage','Slug'  => '#') );
     //    	Menu::subitem( 'wallet','cashinpending', array('Label' => 'Cash-In : Pending','Slug'  => 'wallet/cashins/pending') );
     //    	Menu::subitem( 'wallet','cashin', array('Label' => 'Cash-In : Approved','Slug'  => 'wallet/cashins/approved') );
     //    	Menu::subitem( 'wallet','cashoutpending', array('Label' => 'Cash-Out : Pending','Slug'  => 'wallet/cashouts/pending') );
     //    	Menu::subitem( 'wallet','cashout', array('Label' => 'Cash-Out : Approved','Slug'  => 'wallet/cashouts/approved') );
    	// }
     //    Menu::item( 'invoice_type', array( 'Label' => 'Invoice Type<span>Manage</span>', 'Slug' => 'manage/invoice_type') );
	}

	static public function hubcare()
	{
        Menu::item( 'approvals', array('Label' => 'Approvals <span>Manage Profiles', 'Slug'  => '#') );
        Menu::subitem( 'approvals','clients', array('Label' => 'Projects','Slug'  => 'cs/clientprofiles') );
    	Menu::subitem( 'approvals','seeders', array('Label' => 'Seeders','Slug'  => 'cs/investorprofiles') );
        Menu::subitem( 'approvals','bookings', array('Label' => 'Bookings','Slug'  => 'cs/bookings') );
        Menu::subitem( 'approvals','documents', array('Label' => 'Documents','Slug'  => 'cs/documents') );

        Menu::item( 'blog', array('Label' => 'Blogs <span>Manage</span>','Slug'  => 'blogs/lists') );
        Menu::subitem( 'blog','add', array('Label' => 'Add New','Slug'  => 'blogs/add') ); 
        Menu::subitem( 'blog','category', array('Label' => 'Manage Categories','Slug'  => 'blogs/categories') ); 

        Menu::item( 'assistance', array('Label' => 'Assistance <span>Inquiries & Support</span>','Slug'  => '#') );
        Menu::subitem( 'assistance','affiliates', array('Label' => 'Affiliates','Slug'  => 'cs/affiliates') );
        Menu::subitem( 'assistance','team', array('Label' => 'Core Team','Slug'  => 'manage/team') );
        Menu::subitem( 'assistance','inquiries', array('Label' => 'Inquiries','Slug'  => 'contactus/lists') );
        Menu::subitem( 'assistance','tickets', array('Label' => 'Support Tickets','Slug'  => 'support/dashboard') );
        Menu::subitem( 'assistance','termsandconditions', array( 'Label' => 'Terms and Conditions', 'Slug'  => 'termsandcondition' ) );
	}

    static public function manager()
    {
        Menu::item( 'accounting', array( 'Label' => 'Accounting <span>Manage</span>', 'Slug' => 'manage/accounting' ) );
        Menu::item( 'cs', array( 'Label' => 'Hub Care <span>Manage</span>', 'Slug' => 'manage/cs' ) );
    }

	static public function admin()
	{
        Menu::item( 'faq', array('Label' => 'FAQ\'s <span>Manage</span>', 'Slug'  => 'faq/manage') );
        Menu::subitem( 'faq','addnew', array('Label' => 'Add New','Slug'  => 'faq/add') ); 

        Menu::item( 'blog', array('Label' => 'Blogs <span>Manage</span>','Slug'  => 'blogs/lists') );
        Menu::subitem( 'blog','add', array('Label' => 'Add New','Slug'  => 'blogs/add') ); 
        Menu::subitem( 'blog','category', array('Label' => 'Manage Categories','Slug'  => 'blogs/categories') ); 

        Menu::item( 'inquiries', array('Label' => 'Inquiries <span>Manage</span>','Slug'  => 'contactus/lists') );

    	Menu::item( 'settings', array('Label' => 'Admin <span>Settings</span>','Slug'  => '#') );
    	Menu::subitem( 'settings','users', array('Label' => 'Users','Slug'  => 'users') ); 
        Menu::subitem( 'settings','roles', array('Label' => 'Roles','Slug'  => 'roles') ); 
        Menu::subitem( 'settings','capability', array('Label' => 'Capability','Slug'  => 'capability') ); 
        Menu::subitem( 'settings','capabilitygroups', array('Label' => 'Capability Groups','Slug'  => 'capabilitygroups') ); 
        Menu::subitem( 'settings','options', array('Label' => 'Manage Options','Slug'  => 'options') ); 
        Menu::subitem( 'settings','adminoptions', array('Label' => 'Site Options','Slug'  => 'admin/options') ); 
        Menu::subitem( 'settings','adminsettings', array('Label' => 'Configurations (mpf)','Slug'  => 'admin/settings') ); 
        Menu::subitem( 'settings','activitylogs', array('Label' => 'Activity Logs','Slug'  => 'admin/activitylogs') ); 

        Menu::item( 'manage', array('Label' => 'Manage <span>Overall</span>','Slug'  => '#') );
        Menu::subitem( 'manage','accounting', array('Label' => 'Accounting','Slug'  => 'manage/accounting') );
        Menu::subsubitem( 'manage','accounting','invoice_type', array('Label' => 'Invoice Type', 'Slug'  => 'manage/invoice_type') );
        Menu::subsubitem( 'manage','accounting','packages', array('Label' => 'Subscription Package', 'Slug'  => 'packages') );
        Menu::subitem( 'manage','affiliates', array('Label' => 'Affiliates','Slug'  => 'affiliates') ); 
        Menu::subitem( 'manage','bookings', array('Label' => 'Bookings','Slug'  => 'cs/bookings') ); 
        Menu::subitem( 'manage','team', array( 'Label' => 'Core Team', 'Slug'  => 'manage/team' ) );
        Menu::subitem( 'manage','clients', array('Label' => 'Clients','Slug'  => 'manage/clients') ); 
        Menu::subsubitem( 'manage','clients','categories', array('Label' => 'Client Categories','Slug'  => 'clients/categories') );
        Menu::subitem( 'manage','cs', array('Label' => 'Customer Service','Slug'  => 'manage/cs') );
        Menu::subitem( 'manage','forums', array('Label' => 'Forums','Slug'  => 'forums') ); 
        Menu::subsubitem( 'manage','forums','cat', array('Label' => 'Categories','Slug'  => 'forums/categories') );
        Menu::subsubitem( 'manage','forums','subcat', array('Label' => 'Sub Categories','Slug'  => 'forums/subcategories') ); 
        Menu::subitem( 'manage','seeders', array('Label' => 'Seeders','Slug'  => 'manage/investors') ); 
        Menu::subsubitem( 'manage','seeders','approved', array('Label' => 'Approved','Slug'  => 'manage/investors/approved') );
        Menu::subsubitem( 'manage','seeders','verification', array('Label' => 'Verification','Slug'  => 'manage/investors/verification') );
        Menu::subsubitem( 'manage','seeders','pending', array('Label' => 'Incomplete','Slug'  => 'manage/investors/incomplete') );
        Menu::subsubitem( 'manage','seeders','create', array('Label' => 'Create New','Slug'  => 'users/signup') );
        if( User::can( 'Administer All' ) || User::can( 'Manage Core Bank Accounts' ) ) { 
            Menu::subitem( 'manage','sobank', array('Label' => 'SO Bank Account','Slug'  => 'corebankaccounts') );
        }
        Menu::subitem( 'manage','slider', array('Label' => 'Slider','Slug'  => 'manage/slider') );
        Menu::subitem( 'manage','support', array('Label' => 'Support','Slug'  => 'support/dashboard') );
        Menu::subitem( 'manage','termsandconditions', array( 'Label' => 'Terms and Conditions', 'Slug'  => 'termsandcondition' ) );
        Menu::subitem( 'manage','testimonials', array('Label' => 'Testimonial','Slug'  => 'manage/testimonial') );
        Menu::subitem( 'manage', 'wallet', array('Label' => 'Wallet','Slug'  => '#') );
        Menu::subsubitem( 'manage', 'wallet', 'cashinpending', array('Label' => 'Cash-In : Pending','Slug'  => 'wallet/cashins/pending') );
        Menu::subsubitem( 'manage', 'wallet', 'cashin', array('Label' => 'Cash-In : Approved','Slug'  => 'wallet/cashins/approved') );
        Menu::subsubitem( 'manage', 'wallet', 'cashoutpending', array('Label' => 'Cash-Out : Pending','Slug'  => 'wallet/cashouts/pending') );
        Menu::subsubitem( 'manage', 'wallet', 'cashout', array('Label' => 'Cash-Out : Approved','Slug'  => 'wallet/cashouts/approved') );

	}

    static public function front()
	{
        Menu::item( 'projects', array('Label' => 'Funding <span>Current Projects</span>', 'Slug'  => 'projects') );

        Menu::item( 'about', array('Label' => 'About Us <span>faq, blog, contacts</span>','Slug'  => 'aboutus') );
        Menu::subitem( 'about', 'aboutus', array('Label' => 'Who We Are','Slug'  => 'aboutus') );
        Menu::subitem( 'about', 'faq', array('Label' => 'Faq\'s','Slug'  => 'faq') );
        Menu::subitem( 'about', 'blogs', array('Label' => 'Blogs','Slug'  => 'blogs') );
        Menu::subitem( 'about', 'contactus', array('Label' => 'Contact Us','Slug'  => 'contactus') );
        
        Menu::item( 'ipoplacement', array('Label' => 'Projects <span>IPO & Placements</span>','Slug'  => 'ipoplacement') );
        Menu::item( 'seeders', array('Label' => 'Seeders <span>Seeding Information</span>','Slug'  => 'seeders') );


    	
        
        
	}

	static public function show() {
		
		if( User::isLoggedIn() ) { 

			self::general();

			$role = User::role();            
			switch( $role ) { 

				case 'User': 
					self::seeder();
				break;

				case 'Client':
				case 'Assistants':
					self::client();
				break;

                case 'Accounting':
                case 'Accounting Manager':
					self::accounting();           
				break;

				case 'Customer Service':
				case 'Hub Care':
					self::hubcare();
				break;

                case 'Manager':
                    self::manager();
                break;

				case 'Administrator':
					self::admin();
				break;
			}
		} else { 
			self::front();
		} 

		echo '<'.Menu::$menuWrapper.'>'.Menu::output().'</'.Menu::$menuWrapper.'>';
	}

	static public function profileLink() {
		$profilelink = false;
		if( User::isLoggedIn() ) {
	        $role = User::role();
	        switch( $role ) { 
				case 'User':
					$profilelink = 'users/profile';
				break;

				case 'Client':
				case 'Assistants':
					$profilelink = 'clients/profile';
				break;

				default: 
					$profilelink = false;
				break;
	        }
      	}

      	return $profilelink;
	}
}
?>