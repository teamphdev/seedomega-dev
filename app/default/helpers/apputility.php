<?php
/**
 * Application Utility Object
 *
 * @category   Utility
 * @package    AppUtility
 * @author     Mo <mogol@gmail.com>
 * @copyright  (c) 2018 TechHon Web Solutions
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0.0
 * @link       http://www.techhon.com
 * @since      Class available since Release 1.2.0
 */
class AppUtility
{
    /**
     * Loop clients
     * 
     * @access public | static
     * @param (array) $users : (required) list of users
     * @return na
     */
    static public function loopClients( $users )
    {
        foreach( $users as $user ){ ?>
        <li class="jstree-open">
            <span class="id"><?php echo $user->Code; ?><?php echo $user->UserID; ?></span>
            <span class="name"><?php echo $user->LastName; ?> <?php echo $user->FirstName; ?> &nbsp;&nbsp;&nbsp;</span>
            <span><strong>JOINED : </strong></span><span class="joined"> <?php echo App::date($user->ApplicationDate); ?>&nbsp;&nbsp;&nbsp;</span>
            <span><strong>DEPOSITED : </strong></span><span class="deposited"> <?php echo number_format($user->DepositedAmount); ?></span>
        </li>
        <?php }
    }

    /**
     * Time elapsed string
     * 
     * @access public | static
     * @param (datetime) $datetime : (required) set datetime
     * @param (boolean) $full      : (optional) default[false] : specify if full string or not
     * @return string
     */
    static public function time_elapsed_string( $datetime, $full = false )
    {
        $now = new DateTime;
        $ago = new DateTime( $datetime );
        $diff = $now->diff( $ago );
        $diff->w = floor( $diff->d / 7 );
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hr',
            'i' => 'min',
            's' => 'sec',
        );
        
        foreach( $string as $k => &$v ){
            if ( $diff->$k ){
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);

        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    public static function getLanguages() {
        return array("en" => '英語 | English',"zh" => '繁體中文 Traditional Chinese',"cn" => '简体中文 Simplified Chinese');
    }

    public static function getSidebars() {
        return array("nav-sm" => 'Narrow',"nav-md" => 'Wide');
    }

    public static function getLanguagesAsOption() {
        $return = array();
        foreach (self::getLanguages() as $key => $value) {
            $return[] = $key.':'.$value;
        }

        return $return;
    }

    public static function getPosition()
    {
        return array("Manager","Agent");
    }
    
    public static function getSalutation()
    {
        return array("Mr","Ms","Mrs","Mdm","Prof","Dr","Datuk","Dato","Datin","Dato Seri","Tan Sri");
    }
    
    public static function getCivilStatus()
    {
        return array("SINGLE","MARRIED","SEPARATED","DIVORCED","WIDOWED");
    }
    
    public static function getCountries()
    {
        return array("China", "Hong Kong", "Taiwan", "Singapore", "Malaysia", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
    }
    
    public static function getDay()
    {
        $d = array();
        $d[] = 'Day';
        for($i=1;$i<32;$i++){
            $d[] = $i;
        }
        return $d;
    }
    
    public static function getMonth()
    {
        $months = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July ',
            'August',
            'September',
            'October',
            'November',
            'December'
        );

        $m = array();
        $i = 0;
        $m[] = ':Month';
        foreach($months as $month) {
            $i++;
            $m[] = $i.':'.$month;
        }
        return $m;
    }
    
    public static function getYear()
    {
        $cy = date('Y');
        $fr = $cy - 10;
        $to = $cy - 110;
        $y = array();
        $y[] = 'Year';
        for($i = $fr;$i > $to; $i--){
            $y[] = $i;
        }
        return $y;
    }
    
    public static function space($cnt=1)
    {
        $return = '';
        for($i = 0;$i <= $cnt; $i++){
            $return .= "&nbsp;";
        }
        return $return;
    }
    
    public static function getAgentsCommission()
    {
        return array(
            "10" => '10% 業務總裁 Office Deputy General Manager - GM',
            "8.5" => '8.5% 業務副總裁 Unit Senior Director - US',
            "7" => '7% 業務總監 Unit Director - UD',
            "5.5" => '5.5% 業務經理 Unit Manager - UM',
            "4" => '4% 業務主任 Sales Supervisor - SS'
        );
    }
    
    public static function getProducts()
    {
        $db = new DB();
        $sql = "SELECT * FROM products";        
        $query = &$db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[$row->ProductID] = $row->ProductName;                     
        }
        unset($query);
        
        return $data;
    }

    public static function getChildrenRecursive($ID)
    {
        $db = new DB();
        $sql = "SELECT u.UserID, u.Level, u.ReferrerUserID, um.FirstName, um.LastName, ul.Code, a.ApplicationDate, a.Commission, (SELECT SUM(aa.DepositedAmount) FROM accounts aa LEFT JOIN users uu ON aa.UserID = uu.UserID WHERE uu.ReferrerUserID = u.UserID) as Total FROM users u LEFT JOIN user_meta um ON u.UserID = um.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID LEFT JOIN accounts a ON a.UserID = u.UserID WHERE u.Active = 1 AND u.Level IN(2,3,4) AND u.ReferrerUserID = $ID";        
        $query = &$db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[$row->UserID] = $row;                     
        }
        unset($query);
        
        return $data;
    }

    static public function getAgencyOf($refID = false)
    {
        $db = new DB();
        if($refID) {
            $sql = "SELECT GetAgency(UserID) as child FROM users WHERE UserID = ".$refID;        

            $data = $db->get_row($sql);
            $ret = false;
            if($data) {
                $ret = $data->child;
            }
            return $ret;
        } else {
            return false;
        }
    }

    static public function getGroupOf($refID = false)
    {
        $db = new DB();
        if($refID) {
            $sql = "SELECT GetAgents(UserID) as child FROM users WHERE UserID = ".$refID;        
            $data = $db->get_row($sql);
            $ret = $refID;
            if($data) {
                $ret = $refID.','.$data->child;
            }
            return $ret;
        } else {
            return false;
        }
    }

    static public function getAgents($refID = false)
    {
        $db = new DB();
        if($refID) {
            $sql = "SELECT GetAgents(UserID) as child FROM users WHERE UserID = ".$refID;        
            $data = $db->get_row($sql);
            $ret = $refID;
            if($data) {
                $ret = $data->child;
            }
            return $ret;
        } else {
            return false;
        }
    }

    static public function getParent($userID = false)
    {
        $db = new DB();
        if($refID) {
            $sql = "SELECT UserID,ReferrerUserID FROM users WHERE UserID = ".$userID;        
            $data = $db->get_row($sql);
            return $data;
        } else {
            return false;
        }
    }

    static public function getParentOf($refID = false)
    {
        $db = new DB();
        if($refID) {
            $sql = "SELECT GetParent(UserID) as parent FROM users WHERE UserID = ".$refID;        

            $data = $db->get_row($sql);
            $ret = false;
            if($data) {
                $ret = $data->parent;
            }
            return $ret;
        } else {
            return false;
        }
    }

    static public function getChildrenOf($refID = false)
    {
        $db = new DB();
        if($refID) {
            $sql = "SELECT GetChildrenByID(UserID) as child FROM users WHERE UserID = ".$refID;        
            $data = $db->get_row($sql);
            return $data->child;
        } else {
            return false;
        }
    }
    
    public static function getChildren($ID)
    {
        $data = array();
        $childs = self::getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $data[$child->UserID]['data'] = $child;
            $data[$child->UserID]['children'] = self::getChildren($child->UserID);
        }
        
        $idArr = self::getChildrenIds($ID);
        
        return implode(',',(array)$idArr);
    }
    
    public static function getChildrensData($ID)
    {
        $data = array();
        $childs = self::getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $data[$child->UserID]['data'] = $child;
            $data[$child->UserID]['children'] = self::getChildren($child->UserID);
        }
        
        return (count($data)) ? $data : '';
    }
    
    public static function getChildrenIds($ID)
    {
        $data = array();
        $childs = self::getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $ch = self::getChildrenIds($child->UserID);
            $data[] = $child->UserID;
            if(is_array($ch)) {
                $data[] = implode(',',$ch);
            }            
        }
        
        return (count($data)) ? $data : '';
    }
    
    public static function getChildrenIdArray($ID)
    {
        $data = array();
        $childs = self::getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $data[$child->UserID] = self::getChildrenIdArray($child->UserID);            
        }
        
        return (count($data)) ? $data : '';
    }

    public static function getClientCategories()
    {
        $db = new DB();
        $sql = "SELECT cc.* 
            FROM client_categories cc";
        $query = &$db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    public static function getAgencyInfo($ID=false)
    {
        $data = false;
        $db = new DB();
        if($ID) {
            $sql = "SELECT GroupLanguage, GroupProducts FROM accounts WHERE UserID = $ID LIMIT 1";
            $data = $db->get_row($sql);
        }
                
        return $data;
    }

    public static function getAgencyInfos($ID=false)
    {
        if($ID) {
            $db = new DB();
            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName, CONCAT(ul.Code,u.UserID) as UserCode, a.CompanyName FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID LEFT JOIN accounts a ON a.UserID = u.UserID WHERE CONCAT(ul.Code,u.UserID) = '$ID' OR u.UserID = '$ID' LIMIT 1";

            //echo $sql.'<br>';

            $query = $db->prepare($sql);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_CLASS);
            unset($query);
            
            return $row;

        } else {
            return false;
        }
    }
    
    public static function outputFileLinks($thefs,$user,$sfv='',$v='',$showupload = false)
    {
        $status = array(
            '<h6 class="text-warning push-5-t"><b>PENDING</b></h6>',
            '<h6 class="text-success push-5-t"><b>APPROVED</b></h6>',
            '<h6 class="text-danger push-5-t"><b>REJECTED</b></h6>'
        );
        
        $sfvout = strlen($sfv) > 0 ? $sfv.': ' : '';
        
        $output = '';
        foreach($thefs as $bf) { 
            $ext = pathinfo($bf->FileSlug, PATHINFO_EXTENSION);
            switch ($ext) {
                case 'pdf':
                    $fileSlugUrl = view::asset("images/pdf.png");
                    break;
                case 'docx':
                    $fileSlugUrl = view::asset("images/word.jpg");
                    break;
                case 'doc':
                    $fileSlugUrl = view::asset("images/word.jpg");
                    break;
                default:
                    $fileSlugUrl = View::asset('files'.$bf->FileSlug);
                    break;
            }
            $output .= '<li class="list-group-item">';
                $output .= '<div class="row">';
                    $output .= '<div class="col-md-12 col-sm-12 col-xs-12">';
                        $output .= '<div class="row">';
                            $output .= '<div class="col-md-2 col-sm-12 col-xs-12">';
                                $output .= $status[$bf->Active];
                            $output .= '</div>';
                            $output .= '<div class="col-md-6 col-sm-12 col-xs-12">';
                                $output .= '<h5><a href="'.View::asset(($bf) ? 'files'.$bf->FileSlug : '#').'" class="html5lightbox" data-group="set1" data-thumbnail="'.$fileSlugUrl.'" title="'.$sfvout.  $bf->FileDescription.'">'.$sfvout.  $bf->FileName.'<br><small> '.date("Y-m-d - g:iA", strtotime($bf->DateAdded)).'</small></a></h5>';
                            $output .= '</div>';

                            if(User::is('Administrator') || User::can('Manage Uploaded Documents')) { 
                            $output .= '<div class="col-md-4 actions-edit col-sm-12 col-xs-12">';
                                $output .= '<div class="cf-actions">';
                                    $output .= '<a href="'. View::asset(($bf) ? 'files'.$bf->FileSlug : '#').'" class="mo-icon text-info" data-toggle="tooltip" title="Download" download><i class="si si-arrow-down"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('bookings/deletefile/'.$bf->FileItemID.'/'.$user->InvestmentBookingID).'/" data-toggle="tooltip" title="Delete" class="mo-icon text-danger" onclick="return confirm(\'Are you sure you want to delete this file?\');"><i class="si si-trash"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('bookings/rejectfile/'.$bf->FileItemID.'/'.$user->InvestmentBookingID).'/" data-toggle="tooltip" title="Reject" rel="'.$bf->FileName.'" file-name="'.$v.'" class="mo-icon text-warning rejectuploadedfile"><i class="si si-dislike"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('bookings/approvefile/'.$bf->FileItemID.'/'.$user->InvestmentBookingID).'/" data-toggle="tooltip" title="Approved" class="mo-icon text-success"><i class="si si-like"></i></a>';
                                $output .= '</div>';
                            $output .= '</div>';
                            }
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
                if($showupload) {
                    $output .= '<br><hr><br><input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="'.$bf->FileDescription.'" data-show-upload="false" data-allowed-file-extensions=\'["pdf","jpeg","png","jpg"]\'><span class="text-muted">Allowed file types: pdf, jpeg, jpg, png</span>';
                }
            $output .= '</li>';       
        }
        
        return $output;
    }
    
    public static function outputFileNoData( $k='', $showupload = false, $close = false, $remove = false )
    {
        $nodata = '';
        $nodata = '<li class="list-group-item">';
            $nodata .= '<div class="row">';
                $nodata .= '<div class="col-md-12">';
                    $nodata .= '<div class="row">';
                        $nodata .= '<div class="col-md-12 col-sm-12 col-xs-12 no-file">';
                            //$nodata .= '<span class="font-10 red bold">NO UPLOADED FILE</span>';                            
                            $nodata .= '<div class="col-lg-6"><span class="font-10 red bold">NO UPLOADED FILE</span></div>';
                            $nodata .= '<div class="col-lg-6"><div class="close remove-form hidden" data-id="'.$k.'" title="Remove this form!"><i class="fa fa-trash"></i> REMOVE</div></div>';
                        $nodata .= '</div>';
                        $nodata .= '<div class="col-md-12 col-sm-12 col-xs-12"></div>';
                        $nodata .= '<div class="col-md-12 actions-edit col-sm-12 col-xs-12">';
                            $nodata .= '<div class="cf-actions"></div>';
                        $nodata .= '</div>';
                    $nodata .= '</div>';
                $nodata .= '</div>';
            $nodata .= '</div>';
            if( $showupload ){
                $nodata .= '<br><hr><br><input id="file-0a" class="file form-control file-input" type="file" data-min-file-count="0" name="'.$k.'" data-show-upload="false" data-allowed-file-extensions=\'["pdf","jpeg","png","jpg"]\'><span class="text-muted">Allowed file types: pdf, jpeg, jpg, png</span> | <span class="text-muted">Allowed maximum file size: 8 MB</span>';
            }
        $nodata .= '</li>';
        
        return $nodata;
    }

    public static function outputFileList( $fileArray, $user, $showupload = false )
    {
        $output = '';
        $output .= '<div class="form-group">';
        foreach($fileArray as $k => $v) { 
            if(is_array($v)) {
                $output .= '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">'.$v[0].'</label>';
                $output .= '<div class="col-md-8 col-sm-8 col-xs-12">';
                    $output .= '<ul class="list-group cf-filelist">';
                    
                    foreach($v[1] as $sfk => $sfv) {
                       if($user->$sfk) {
                            $fc = 1;
                            $thefs = View::common()->getUploadedFiles($user->$sfk);
                            
                            $output .= '<ul class="list-group cf-filelist">';
                            $output .= self::outputFileLinks($thefs,$user,$sfv,$v[0],$showupload);
                            $output .= '</ul>';
                        } else {
                            $output .= '<ul class="list-group cf-filelist">';
                            $output .= self::outputFileNoData($k,$showupload);
                            $output .= '</ul>';
                        }
                    }
                    
                $output .= '</div><div class="clearfix"></div>';
            } else {
                $output .= '<label>'. $v.'</label>';
                $output .= '<ul class="list-group cf-filelist">';
                    
                    if($user->$k) { 
                        $fc = 1;
                        $thefs = View::common()->getUploadedFiles($user->$k);
                        if(count($thefs)) {
                             $output .= self::outputFileLinks($thefs,$user,'',$v,$showupload);
                        } else {
                           $output .= self::outputFileNoData($k,$showupload);
                        } 
                    } else {
                        $output .= self::outputFileNoData($k,$showupload);
                    }
                    
                $output .= '</ul>';

                $output .= '<div class="clearfix"></div>';                
            }
        }
        $output .= '</div>';
        
        return $output;
    }

    public static function outputCustomFileList( $fileArray, $user, $showupload = false, $remove = false )
    {
        $docname = isset( $user->DocumentName ) ? $user->DocumentName : '';
        $disabled = isset( $user->Disabled ) ? $user->Disabled : '';
        // $dnf = explode( '-', $fileArray );
        foreach( $fileArray as $v ){
            $dn = explode( '-', $v );
            $output = '<div id="'.$dn[1].'" class="form-group created-form">';
                $output .= '<input type="hidden" name="docNames[]" value="'.$dn[0].'|'.$docname.'" id="DN-'.$dn[1].'"/>';
                $output .= '<div class="remove-ib push-5">';
                    $output .= '<label>document name *</label> <b><div class="doc-name"><input type="text" value="'.$docname.'" name="DN-'.$dn[1].'" class="docs-input '.$disabled.'" data-placeholder="write document name here"/></div></b>';
                $output .= '</div>';
                $output .= '<ul class="list-group cf-filelist">';
                    if( isset( $user->FileID ) && $user->FileID ){
                        $fc = 1;
                        $thefs = View::common()->getUploadedFiles( $user->FileID );
                        if( count( $thefs ) ){
                             $output .= self::outputUserFileLinks( $thefs, $user, '', $dn[1], $showupload );
                        } else {
                           $output .= self::outputFileNoData( $dn[1], $showupload, $remove );
                        } 
                    } else {
                        $output .= self::outputFileNoData( $dn[1], $showupload, $remove );
                    }
                $output .= '</ul>';
                $output .= '<div class="clearfix"></div>';
            $output .= '</div>';
        }
        
        return $output;
    }

    public static function outputFileRejectPopup()
    {
        $output = '';
                
        $output .= '<div class="rejectfileform"><div class="rejectfileform_wrap">';
        $output .= '<form method="post" action="" id="rejectfile">';
        $output .= '<input type="hidden" name="reject[filename]" value="" id="filen">';
            $output .= '<h3>Reject File</h3>';
            $output .= '<p>Rejecting file <span class="rfilename"></span>, please put a reason and note below:</p>';
            $output .= '<div class="form-group">';
                $output .= '<label class="control-label col-md-3 col-sm-3 col-xs-12">Select Reason</label>';
                $output .= '<div class="col-md-8 col-sm-8 col-xs-12">';
                    $output .= '<label><input type="checkbox" name="reject[reasons][]" value="Not Clear"> Not Clear</label>';
                    $output .= '<label><input type="checkbox" name="reject[reasons][]" value="Not Readable"> Not Readable</label>';
                $output .= '</div>';
            $output .= '</div><div class="clearfix"></div>';
            $output .= '<div class="form-group">';
                $output .= '<label class="control-label col-md-3 col-sm-3 col-xs-12">Notes</label>';
                $output .= '<div class="col-md-8 col-sm-8 col-xs-12">';
                    $output .= '<textarea style="height:120px;" name="reject[notes]" class="form-control"></textarea>';
                $output .= '</div>';
            $output .= '</div><div class="clearfix"></div>';
            $output .= '<div class="form-group">';
                $output .= '<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>';
                $output .= '<div class="col-md-8 col-sm-8 col-xs-12">';
                    $output .= '<input class="btn btn-danger" type="submit" value="Reject">&nbsp;&nbsp;';
                    $output .= '<a href="#" class="btn btn-warning close_rejectfileform">Cancel</a>';
                $output .= '</div>';
            $output .= '</div>';
        $output .= '</form>';
        $output .= '</div></div>';
        
        return $output;
    }    
    
    public static function getBookedTTReceipt($user)
    {
        $fileArray = array(
            'TTReceiptPhoto' => 'Uploaded Receipt Photo'
        );
        
        return self::outputFileList($fileArray,$user);
    }
    
    public static function getFileList($user,$showupload = false)
    {
        $fileArray = array(
            'IdPhoto'=>Lang::get('USR_PRF_UPLGOVID'),
            'AddressPhoto'=>Lang::get('USR_PRF_UPLADDRPRF')
        );
        
        return self::outputFileList($fileArray,$user,$showupload);
    }

    public static function getProoOfAddressFile($user,$showupload = false)
    {
        $fileArray = array(
            'AddressPhoto'=>Lang::get('USR_PRF_UPLADDRPRF')
        );

        return self::outputFileList($fileArray,$user,$showupload);
    }

    public static function getInvestorFileList($user,$showupload = false)
    {
        $fileArray = array(
            'IdPhoto'=>Lang::get('USR_PRF_UPLGOVID'),
            'AddressPhoto'=>Lang::get('USR_PRF_UPLADDRPRF')
        );
        
        return self::outputUserFileList($fileArray,$user,$showupload);
    }

    public static function getClientFileList($user,$showupload = false)
    {
        $fileArray = array(
            'CompanyReleaseFileID'=>Lang::get('CLN_PRF_FILE_COMPRELEASE'),
            'CompanyResearchFileID'=>Lang::get('CLN_PRF_FILE_RESEARCH'),
            'CompanyDueDiligenceFileID'=>Lang::get('CLN_PRF_FILE_DIL')
        );
        
        return self::outputUserFileList($fileArray,$user,$showupload);
    }

    public static function getInvoiceTTReceipt( $user, $showupload = false )
    {
        $fileArray = array( 'TTReceipt' => 'Uploaded TT Receipt Photo' );
        
        return self::outputUserFileList( $fileArray, $user, $showupload);
    }
    
    public static function getCustomFileList( $titleArray, $user, $showupload = false, $remove = false )
    {
        if( is_array( $titleArray ) ){
            $fileArray = array( $titleArray[0] => $titleArray[1] );
        } else {
            $fileArray = array( $titleArray => $titleArray );
        }
        
        return self::outputCustomFileList( $fileArray, $user, $showupload, $remove );
    }

    public static function outputUserFileList($fileArray,$user,$showupload = false)
    {
        $output = '';
        $output .= '<div class="form-group">';
        foreach($fileArray as $k => $v) { 
            if(is_array($v)) {
                $output .= '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">'.$v[0].'</label>';
                $output .= '<div class="col-md-8 col-sm-8 col-xs-12">';
                    $output .= '<ul class="list-group cf-filelist">';
                    
                    foreach($v[1] as $sfk => $sfv) {
                       if($user->$sfk) {
                            $fc = 1;
                            $thefs = View::common()->getUploadedFiles($user->$sfk);                            
                            $output .= '<ul class="list-group cf-filelist">';
                            $output .= self::outputUserFileLinks($thefs,$user,$sfv,$v[0],$showupload);
                            $output .= '</ul>';
                        } else {
                            $output .= '<ul class="list-group cf-filelist">';
                            $output .= self::outputFileNoData($k,$showupload);
                            $output .= '</ul>';
                        }
                    }
                    
                $output .= '</div><div class="clearfix"></div>';
            } else {
                $output .= '<label>'. $v.'</label>';
                    $output .= '<ul class="list-group cf-filelist">';
                        
                        if( isset( $user->$k) ){
                            $fc = 1;
                            $thefs = View::common()->getUploadedFiles( $user->$k );
                            if( count( $thefs ) ){
                                 $output .= self::outputUserFileLinks( $thefs, $user, '', $v, $showupload );
                            } else {
                               $output .= self::outputFileNoData( $k, $showupload );
                            } 
                        } else {
                            $output .= self::outputFileNoData( $k, $showupload );
                        }
                        
                    $output .= '</ul>';

                $output .= '<div class="clearfix"></div>';                
            }
        }
        $output .= '</div>';
        
        return $output;
    }

    public static function outputUserFileLinks($thefs,$user,$sfv='',$v='',$showupload = false)
    {
        $status = array(
            '<h6 class="text-warning push-5-t"><b>PENDING</b></h6>',
            '<h6 class="text-success push-5-t"><b>APPROVED</b></h6>',
            '<h6 class="text-danger push-5-t"><b>REJECTED</b></h6>'
        );
        
        $sfvout = strlen($sfv) > 0 ? $sfv.': ' : '';
        
        $output = '';
        foreach($thefs as $bf) { 
            $ext = pathinfo($bf->FileSlug, PATHINFO_EXTENSION);
            switch ($ext) {
                case 'pdf':
                    $fileSlugUrl = view::asset("images/pdf.png");
                    break;
                case 'docx':
                    $fileSlugUrl = view::asset("images/word.jpg");
                    break;
                case 'doc':
                    $fileSlugUrl = view::asset("images/word.jpg");
                    break;
                default:
                    $fileSlugUrl = View::asset('files'.$bf->FileSlug);
                    break;
            }
            $output .= '<li class="list-group-item">';
                $output .= '<div class="row">';
                    $output .= '<div class="col-md-12 col-sm-12 col-xs-12">';
                        $output .= '<div class="row">';
                            $output .= '<div class="col-md-2 col-sm-12 col-xs-12">';
                                $output .= $status[$bf->Active];
                            $output .= '</div>';
                            $output .= '<div class="col-md-6 col-sm-12 col-xs-12">';
                                $output .= '<h5><a href="'.View::asset(($bf) ? 'files'.$bf->FileSlug : '#').'" class="html5lightbox" data-thumbnail="'.$fileSlugUrl.'" title="'.$sfvout. $bf->FileDescription.'">'.$sfvout. $bf->FileName.'<br><small> '.date("Y-m-d - g:iA", strtotime($bf->DateAdded)).'</small></a></h5>';
                            $output .= '</div>';

                            if( User::is( 'Administrator' ) || User::can( 'Manage Uploaded Documents' ) ) { 
                            $output .= '<div class="col-md-3 actions-edit col-sm-12 col-xs-12">';
                                $output .= '<div class="cf-actions">';
                                    $output .= '<a href="'. View::asset(($bf) ? 'files'.$bf->FileSlug : '#').'" class="mo-icon text-info" data-toggle="tooltip" title="Download" download><i class="si si-arrow-down"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('cs/deletefile/'.$bf->FileItemID.'/'.$user->UserID).'/profile" data-toggle="tooltip" title="Delete" class="mo-icon text-danger" onclick="return confirm(\'Are you sure you want to delete this file?\');"><i class="si si-trash"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('cs/rejectfile/'.$bf->FileItemID.'/'.$user->UserID).'/profile" data-toggle="tooltip" title="Reject" rel="'.$bf->FileName.'" file-name="'.$v.'" class="mo-icon text-warning rejectuploadedfile"><i class="si si-dislike"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('cs/approvefile/'.$bf->FileItemID.'/'.$user->UserID).'/profile" data-toggle="tooltip" title="Approved" class="mo-icon text-success"><i class="si si-like"></i></a>';
                                $output .= '</div>';
                            $output .= '</div>';
                            }
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
                if($showupload) {
                    $output .= '<br><hr><br><input id="file-0a" class="file form-control file-input" multiple="multiple" type="file" data-min-file-count="0" name="'.$bf->FileDescription.'" data-show-upload="false" data-allowed-file-extensions=\'["pdf","jpeg","png","jpg"]\'><span class="text-muted">Allowed file types: pdf, jpeg, jpg, png</span> | <span class="text-muted">Allowed maximum file size: 8 MB</span>';
                }
            $output .= '</li>';
        }
        
        return $output;
    }

    public static function getWalletTTReceipt($user)
    {
        $fileArray = array(
            'TTreceipt' => 'Uploaded Receipt Photo'
        );
        
        return self::outputWalletFileList($fileArray,$user);
    }

    public static function outputWalletFileList( $fileArray, $user, $showupload = false )
    {
        $output = '';
        $output .= '<div class="form-group">';
        foreach($fileArray as $k => $v) { 
            if(is_array($v)) {
                $output .= '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">'.$v[0].'</label>';
                $output .= '<div class="col-md-8 col-sm-8 col-xs-12">';
                    $output .= '<ul class="list-group cf-filelist">';
                    
                    foreach($v[1] as $sfk => $sfv) {
                       if($user->$sfk) {
                            $fc = 1;
                            $thefs = View::common()->getUploadedFiles($user->$sfk);
                            
                            $output .= '<ul class="list-group cf-filelist">';
                            $output .= self::outputWalletFileLinks($thefs,$user,$sfv,$v[0],$showupload);
                            $output .= '</ul>';
                        } else {
                            $output .= '<ul class="list-group cf-filelist">';
                            $output .= self::outputFileNoData($k,$showupload);
                            $output .= '</ul>';
                        }
                    }
                    
                $output .= '</div><div class="clearfix"></div>';
            } else {
                $output .= '<label>'. $v.'</label>';
                $output .= '<ul class="list-group cf-filelist">';
                    
                    if($user->$k) { 
                        $fc = 1;
                        $thefs = View::common()->getUploadedFiles($user->$k);
                        if(count($thefs)) {
                             $output .= self::outputWalletFileLinks($thefs,$user,'',$v,$showupload);
                        } else {
                           $output .= self::outputFileNoData($k,$showupload);
                        } 
                    } else {
                        $output .= self::outputFileNoData($k,$showupload);
                    }
                    
                $output .= '</ul>';

                $output .= '<div class="clearfix"></div>';                
            }
        }
        $output .= '</div>';
        
        return $output;
    }

    public static function outputWalletFileLinks($thefs,$user,$sfv='',$v='',$showupload = false)
    {
        $status = array(
            '<h6 class="text-warning push-5-t"><b>PENDING</b></h6>',
            '<h6 class="text-success push-5-t"><b>APPROVED</b></h6>',
            '<h6 class="text-danger push-5-t"><b>REJECTED</b></h6>'
        );
        
        $sfvout = strlen($sfv) > 0 ? $sfv.': ' : '';
        
        $output = '';
        foreach($thefs as $bf) { 
            $ext = pathinfo($bf->FileSlug, PATHINFO_EXTENSION);
            switch ($ext) {
                case 'pdf':
                    $fileSlugUrl = view::asset("images/pdf.png");
                    break;
                case 'docx':
                    $fileSlugUrl = view::asset("images/word.jpg");
                    break;
                case 'doc':
                    $fileSlugUrl = view::asset("images/word.jpg");
                    break;
                default:
                    $fileSlugUrl = View::asset('files'.$bf->FileSlug);
                    break;
            }
            $output .= '<li class="list-group-item">';
                $output .= '<div class="row">';
                    $output .= '<div class="col-md-12 col-sm-12 col-xs-12">';
                        $output .= '<div class="row">';
                            $output .= '<div class="col-md-2 col-sm-12 col-xs-12">';
                                $output .= $status[$bf->Active];
                            $output .= '</div>';
                            $output .= '<div class="col-md-6 col-sm-12 col-xs-12">';
                                $output .= '<h5><a href="'.View::asset(($bf) ? 'files'.$bf->FileSlug : '#').'" class="html5lightbox" data-thumbnail="'.$fileSlugUrl.'" title="'.$sfvout.  $bf->FileDescription.'">'.$sfvout.  $bf->FileName.'<br><small> '.date("Y-m-d - g:iA", strtotime($bf->DateAdded)).'</small></a></h5>';
                            $output .= '</div>';

                            if(User::is('Administrator') || User::can('Manage Uploaded Wallet Documents')) { 
                            $output .= '<div class="col-md-3 actions-edit col-sm-12 col-xs-12">';
                                $output .= '<div class="cf-actions">';
                                    $output .= '<a href="'. View::asset(($bf) ? 'files'.$bf->FileSlug : '#').'" class="mo-icon text-info" data-toggle="tooltip" title="Download" download><i class="si si-arrow-down"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('wallet/deletefile/'.$bf->FileItemID.'/'.$user->WalletID).'/" data-toggle="tooltip" title="Delete" class="mo-icon text-danger" onclick="return confirm(\'Are you sure you want to delete this file?\');"><i class="si si-trash"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('wallet/rejectfile/'.$bf->FileItemID.'/'.$user->WalletID).'/" data-toggle="tooltip" title="Reject" rel="'.$bf->FileName.'" file-name="'.$v.'" class="mo-icon text-warning rejectuploadedfile"><i class="si si-dislike"></i></a>&nbsp;';
                                    $output .= '<a href="'. View::url('wallet/approvefile/'.$bf->FileItemID.'/'.$user->WalletID).'/" data-toggle="tooltip" title="Approved" class="mo-icon text-success"><i class="si si-like"></i></a>';
                                $output .= '</div>';
                            $output .= '</div>';
                            }
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
                if($showupload) {
                    $output .= '<br><hr><br><input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="'.$bf->FileDescription.'" data-show-upload="false" data-allowed-file-extensions=\'["pdf","jpeg","png","jpg"]\'><span class="text-muted">Allowed file types: pdf, jpeg, jpg, png</span>';
                }
            $output .= '</li>';       
        }
        
        return $output;
    }

    public static function outputSteps($user,$curr=1)
    {
        $output = '<ul class="wizard_steps '.$curr.'">';
                    for($i = 1; $i <= 6; $i++) {
                        
                        $link = ($curr >= $i) ? View::url('casefiles/step'.$i.'/'). $user->AccountID : '#step-'.$i;
                        $done = ($i < $curr) ? 'done' : 'disabled';
                        $sel = ($curr == $i) ? 'selected' : $done;
                        $output .= '<li>
                                        <a href="'.$link.'" class="'.$sel.'">
                                            <span class="step_no">'.$i.'</span>
                                            <span class="step_descr">
                                                '. Lang::get('CASE_STEPNO'.$i).'<br />
                                                <small>'. Lang::get('CASE_STEP'.$i.'_DESC').'</small>
                                            </span>
                                        </a>
                                    </li>';
                    }

                    $output .= '</ul>';

        return $output;
    }
    
    public static function cleanFilename($str="")
    {
        $frm = array('/','   ','  ',' ');
        $tos = array('_','_','_','_');
        $output = str_replace($frm, $tos, $str);

        return $output;
    }

    public static function investorinfo( $key = false, $id = false )
    {
        $return = false;
        $common = new Common();

        if( $id == false ){
            $id = User::info('UserID');
        }

        if( $id ){
            $sql = "SELECT u.*, um.*, ul.Name as UserLevel, ul.Code, 
            a.IdPhoto as InvestorPhotoID, a.AddressPhoto as InvestorAddressPhoto, a.Status as InvestorStatus, 
            (SELECT ffi.Active FROM file_items ffi WHERE ffi.FileID = InvestorPhotoID) as InvtPhotoIDStatus,
            (SELECT ffi.Active FROM file_items ffi WHERE ffi.FileID = InvestorAddressPhoto) as InvtAddressPhotoStatus
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON ul.UserLevelID = u.Level 
            LEFT JOIN accounts a ON a.UserID = u.UserID 
            WHERE CONCAT(ul.Code,u.UserID) = '$id' OR u.UserID = '".$id."' LIMIT 1";

            $userdata = $common->db->get_row($sql);

            $userinfo = $userdata ? (array) $userdata : array();
        } else {
            $userinfo = $common->getSession('userdata');
        }
        
        if($key != false) {
            $return = isset($userinfo[$key]) ? $userinfo[$key] : false;
        } else {
            $return = ($userinfo) ? (object) $userinfo : array();
        }
        
        return $return;
    }

    public static function clientInfo($key = false, $id = false)
    {
        $return = false;
        $common = new Common();

        if( $id == false ){
            $id = User::info( 'UserID' );
        }      

        if( $id ){
            $sql = "SELECT u.*, um.*, ul.Name as UserLevel, ul.Code, cp.ClientProfileID, cp.Status as clientStatus, cp.CompanyName, cp.CompanyPhoto
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON ul.UserLevelID = u.Level 
            LEFT JOIN client_profiles cp ON cp.UserID = u.UserID 
            WHERE CONCAT(ul.Code,u.UserID) = '$id' OR u.UserID = '".$id."' LIMIT 1";

            $userdata = $common->db->get_row($sql);
            $userinfo = $userdata ? (array) $userdata : array();
        } else {
            $userinfo = $common->getSession('userdata');
        }
        
        if( $key != false ){
            $return = isset( $userinfo[$key] ) ? $userinfo[$key] : false;
        } else {
            $return = $userinfo ? (object) $userinfo : array();
        }
        
        return $return;
    }

    public static function getClientBlog( $userID )
    {
        $db = new DB();

        $sql = "select cp.ClientProfileID, b.* FROM client_profiles as cp
              LEFT JOIN blogs as b ON b.ClientProfileID = cp.ClientProfileID WHERE cp.`UserID` = " . $userID;

        $query = $db->prepare( $sql );
        $query->execute();
        $row = $query->fetch( PDO::FETCH_CLASS );
        unset($query);

        return $row;
    }

    public static function excerptText($txt,$limit,$sub = '...')
    {
        return substr(strip_tags($txt),0,$limit) . $sub;
    }

    //will return orignal input value if less than the limit
    public static function excerptAsNeeded( $txt, $limit, $sub = '...' )
    {
        $ret = substr( strip_tags( $txt ), 0, $limit ).$sub;
        return strlen( trim( $txt ) ) <= $limit ? $txt : $ret;
    }

    public static function maskText( $txt, $caps = '*', $small = '*' )
    {
        return preg_replace( array( '#[A-Z0-9]#', '#[a-z]#' ), array( $caps, $small ), $txt );
    }

    public static function groupBookingByStatus( $arrays )
    {
        $newArray = array( 'Pending'=>array(), 'Verified'=>array(), 'Approved'=>array(), 'Interest'=>array() );
        foreach( $arrays as $arr ){
            if( $arr->BookingType == 'Interest' ){
                $newArray['Interest'][] = $arr;
            } else {
                $newArray[$arr->BookingStatus][] = $arr;
            }
        }   

        return $newArray;
    }

    public static function getBookedUsersCount( $key = false, $ClientProfileID = false )
    {
        if( !$ClientProfileID ){
            $userinfo = User::info();            
            $userID = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $ClientProfileID = self::clientInfo( 'ClientProfileID', $userID );
        }

        $return = 0;
        $data = array();
        $db = new DB();
        if( $ClientProfileID ){
            // $sql = "SELECT 
            //     (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Approved' AND bb.ClientProfileID=$ClientProfileID AND bb.BookingType='Booking') as TotalApproved,
            //     (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.ClientProfileID=$ClientProfileID AND bb.BookingType='Booking') as TotalPendings,
            //     (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Verified' AND bb.ClientProfileID=$ClientProfileID AND bb.BookingType='Booking') as TotalVerified,
            //     (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.ClientProfileID=$ClientProfileID AND bb.BookingType='Interest') as TotalRegistered,
            //     (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus IN('Verified','Pending') AND bb.ClientProfileID=$ClientProfileID AND bb.BookingType='Booking') as TotalNotApprove  
            // FROM bookings b 
            // WHERE b.ClientProfileID = $ClientProfileID";
            
            // $countdata = $db->get_row($sql);
            // $countdata = $countdata ? (array) $countdata : array();

            // if( $key != false ){
            //     $return = isset( $countdata[ $key ] ) ? $countdata[ $key ] : array();
            // } else {
            //     $return = $countdata ? (object) $countdata : array();
            // }

            $sql = "SELECT CONCAT( `BookingType`, `BookingStatus`) AS Bookings, COUNT(*) AS Total FROM `bookings` WHERE `ClientProfileID` = $ClientProfileID GROUP BY CONCAT( `BookingType`, `BookingStatus` );";

            $query = &$db->prepare( $sql );
            $query->execute();
            while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                $data[ $row->Bookings ] = $row;
            }
            unset($query);            
            
            if( $key == false ){
                foreach( $data as $k => $v ){ $return += isset( $v->Total ) ? (int) $v->Total : 0; } //return all bookings
            } else {
                switch( $key ){
                    case 'TotalNotApprove':
                        $pending = isset( $data['BookingPending']->Total ) ? (int) $data['BookingPending']->Total : 0;
                        $verified = isset( $data['BookingVerified']->Total ) ? (int) $data['BookingVerified']->Total : 0;
                        $return = $pending + $verified;
                        break;
                    case 'TotalRegistered':
                        $return = isset( $data['InterestPending']->Total ) ? (int) $data['InterestPending']->Total : 0;
                        break;
                    default:
                        $newkey = str_ireplace( 'Total', 'Booking', $key );
                        $newkey = str_ireplace( 'Pendings', 'Pending', $newkey );
                        $return = isset( $data[$newkey]->Total ) ? (int) $data[$newkey]->Total : 0;
                        break;
                }
            }
        }

        return $return;
    }

    public static function getOwnBookedCount($key = false, $UserID = false)
    {
        if($UserID == false){
            $UserID = User::info('UserID');
        } 
        $db = new DB();
        if($UserID) {
             $sql = "SELECT 
                (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Approved' AND bb.UserID=$UserID AND bb.BookingType='Booking') as TotalApproved,
                (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.UserID=$UserID AND bb.BookingType='Booking') as TotalPendings,
                (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Verified' AND bb.UserID=$UserID AND bb.BookingType='Booking') as TotalVerified,
                (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.UserID=$UserID AND bb.BookingType='Interest') as TotalRegistered,
                (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus IN('Verified','Pending') AND bb.UserID=$UserID AND bb.BookingType='Booking') as TotalNotApprove  
            FROM bookings b 
            WHERE b.UserID = $UserID";

            $data = $db->get_row($sql);
            $countdata = $db->get_row($sql);
            $countdata = ($countdata) ? (array) $countdata : array();

            if($key != false) {
                $return = isset($countdata[$key]) ? $countdata[$key] : false;
            } else {
                $return = ($countdata) ? (object) $countdata : array();
            }
        }       
        return $return;
    }

    /**
     * Get book count by UserID
     * 
     * @package
     * @access public | static
     * @param (int) $ID        : (optional) default[Current_UserID] : User ID
     * @return array keys[BookingApproved,BookingPending,BookingVerified,InterestPending]
     **/
    public static function getBookCountByUserID($ID = false)
    {
        $db = new DB(); // Define db object

        if($ID == false){
            $ID = User::info('UserID');
        } 
        
        $return = false;

        if($ID) {
             $sql = "SELECT CONCAT(b.BookingType,b.BookingStatus) as Status,count(b.InvestmentBookingID) as Count
                    FROM bookings b
                    JOIN client_profiles cp ON cp.ClientProfileID=b.ClientProfileID
                    WHERE b.UserID=$ID
                    GROUP BY b.BookingType,b.BookingStatus";

            $query = $db->prepare( $sql );
            $query->execute();

            $data = array('BookingApproved'=>0,'BookingPending'=>0,'BookingVerified'=>0,'InterestPending'=>0);
            while ($row = $query->fetch(PDO::FETCH_CLASS)){
                $data[$row->Status] = $row->Count;
            }
            unset($query);

            $return = $data;
        }       
        
        return $return;
    }

    /**
     * Ger user data
     * 
     * @package
     * @access public | static
     * @param (int) $ID        : (required)                : User ID
     * @param (boolean) $fetch : (optional) default[false] : type of extracting data
     * @return object
     **/
    public static function getUser( $ID, $fetch = false )
    {
        $db = new DB();
        $sql = "SELECT u.*, um.*, ul.`Name`, ul.`Code`, 
                ( SELECT CONCAT( uul.`Code`, uu.`UserID` ) as FirstAgentID FROM `users` uu LEFT JOIN `user_levels` uul ON uu.`Level` = uul.`UserLevelID` WHERE uu.`UserID` = u.`ReferrerUserID` ) as AgentID
            FROM `users` u
            LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID` 
            LEFT JOIN `user_levels` ul ON u.`Level` = ul.`UserLevelID` 
            WHERE u.`UserID` = ".$ID." LIMIT 1";
        
        if( $fetch ){
            $query = $db->prepare( $sql );
            $query->execute();
            $data[] = $query->fetch( PDO::FETCH_CLASS );
            unset( $query );
        } else {
            $data = $db->get_row( $sql );
        }

        return $data;
    }

    /**
     * Get user profile row data by User ID
     * 
     * @access public | static
     * @param (int) $userID : (required) : specified user id
     * @return object
     */
    public static function getUserProfileInfo($userID) {
        $return = false;
        $db = new DB();
        if($userID) {
            $sql = "SELECT a.IdPhoto,a.AddressPhoto,um.Avatar,um.FirstName,um.LastName,u.Email,um.Phone,um.Address,um.City,um.State,um.Country,um.PostalCode FROM users u LEFT JOIN user_meta um ON u.UserID = um.UserID LEFT JOIN accounts a ON u.UserID = a.UserID WHERE u.UserID = '".$userID."' LIMIT 1";

            $return = $db->get_row($sql);            
        }       
        return $return;
    }

    /**
     * Get client profile row data by User ID
     * 
     * @access public | static
     * @param (int) $userID : (required) : specified client user id
     * @return object
     */
    public static function getClientProfileInfo( $userID ){
        $return = false;
        $db = new DB();
        if( $userID ){
            // $sql = "SELECT cf.*,um.FirstName,um.LastName,u.Email,um.Phone,um.Address,um.City,um.State,um.Country,um.PostalCode FROM users u LEFT JOIN user_meta um ON u.UserID = um.UserID LEFT JOIN client_profiles cf ON u.UserID = cf.UserID WHERE u.UserID = '".$userID."' LIMIT 1";

            $sql = "SELECT cf.*,um.`FirstName`,um.`LastName`,u.`Email`,um.`Phone`,um.`Address`,um.`City`,um.`State`,um.`Country`,um.`PostalCode`,
                (SELECT COUNT( `ReferenceID` ) FROM `file_groups` fg LEFT JOIN `file_items` fi ON fi.`FileID` = fg.`FileID` WHERE fg.`ReferenceID` = cf.`ClientProfileID` AND fi.`Active` = 1) AS Documents
            FROM `users` u
            LEFT JOIN `user_meta` um ON u.`UserID` = um.`UserID`
            LEFT JOIN `client_profiles` cf ON u.`UserID` = cf.`UserID`
            WHERE u.`UserID` = ".$userID." LIMIT 1";

            $return = $db->get_row($sql);            
        }       
        return $return;
    }

    /**
     * Get client profile row data by User ID
     * 
     * @access public | static
     * @param (int) $userID : (required) : specified client user id
     * @return object
     */
    public static function getClientProfileInfoByClientProfileID( $clientProfileID ){
        $return = false;
        $db = new DB();
        if( $clientProfileID ){
            $sql = "SELECT cf.*,um.FirstName,um.LastName,u.Email,um.Phone,um.Address,um.City,um.State,um.Country,um.PostalCode FROM users u LEFT JOIN user_meta um ON u.UserID = um.UserID LEFT JOIN client_profiles cf ON u.UserID = cf.UserID WHERE cf.ClientProfileID = $clientProfileID LIMIT 1";

            $return = $db->get_row($sql);
        }       
        return $return;
    }

    /**
     * Get client profile row data by Profile ID
     * 
     * @access public | static
     * @param (int) $profileID : (required) : specified client profile id
     * @return object
     */
    public static function getClientProfileInfoByID($profileID) {
        $return = false;
        $db = new DB();
        if($profileID) {
            $sql = "SELECT cf.*,um.FirstName,um.LastName,u.Email,um.Phone,um.Address,um.City,um.State,um.Country,um.PostalCode FROM client_profiles cf JOIN users u ON u.UserID = cf.UserID JOIN user_meta um ON cf.UserID = um.UserID WHERE cf.ClientProfileID = '".$profileID."' LIMIT 1";

            $return = $db->get_row($sql);            
        }       
        return $return;
    }

    public static function getClientInvoicesNoTTReceipt( $ClientProfileID = false ){
        if( !$ClientProfileID ){
            $userinfo = User::info();            
            $userID = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $ClientProfileID = self::clientInfo( 'ClientProfileID', $userID );
        }

        $return = 0;
        $db = new DB();
        if( $ClientProfileID ){
            $sql = "SELECT count(`ClientID`) as total FROM `invoices` WHERE `ClientID` = '$ClientProfileID' AND `TTReceipt` IS NULL OR `TTReceipt` = 0 LIMIT 1";

            $countdata = $db->get_row($sql);
            $return = isset( $countdata ) ? $countdata->total : 0;
        }       
        return $return;
    }

    public static function fileHasFileItem($id) {
        $return = false;
        $db = new DB();
        if($id) {
            $sql = "SELECT FileItemID FROM `file_items` WHERE Active = 1 AND FileID = '".$id."' LIMIT 1";
            $ret = $db->get_row($sql); 
            $return = isset($ret->FileItemID) ? true : false;
        }       
        return $return;
    }

    /**
     * Get profile completeness
     * 
     * @access public | static
     * @param (int) $userID : (required) : specified userid
     * @param (int) $viewee : (optional) : if logged user is admin or has access to view
     * @return int
     */
    public static function profileCompleteness( $id, $viewee = '' ){

        $complete = 0;

        if( User::is( 'Client' ) || User::is( 'Assistants' ) || $viewee == 'Client' ){
            $complete = 0;
            $profile = self::getClientProfileInfo( $id );

            // if( isset( $profile->CompanyReleaseFileID  ) ){
            //     if( $profile->CompanyReleaseFileID  != 0 && self::fileHasFileItem( $profile->CompanyReleaseFileID  ) ){ 
            //         $complete = $complete + .5; 
            //     }
            // }

            // if( isset( $profile->CompanyResearchFileID ) ){
            //     if( $profile->CompanyResearchFileID != 0 && self::fileHasFileItem( $profile->CompanyResearchFileID ) ){ 
            //         $complete = $complete + .5; 
            //     }
            // }

            // if( isset( $profile->CompanyDueDiligenceFileID ) ){
            //     if( $profile->CompanyDueDiligenceFileID != 0 && self::fileHasFileItem( $profile->CompanyDueDiligenceFileID ) ){ 
            //         $complete = $complete + .5; 
            //     }
            // }

            switch( $profile->Documents ){
                case 0: break;
                case 1: $complete = $complete + .5; break;                
                case 2: $complete = $complete + 1; break;                
                default: $complete = $complete + 1.5; break;
            }

            if( isset( $profile->CompanyPhoto ) ){
                if( $profile->CompanyPhoto != 0 && self::fileHasFileItem( $profile->CompanyPhoto ) ){ 
                    $complete = $complete + .5; 
                }
            }
            
            if( isset( $profile->FinancialSummary  ) && $profile->FinancialSummary  != "" ){ $complete = $complete + .5; }
            if( isset( $profile->Video ) && $profile->Video != "" ){ $complete = $complete + .5; }
            if( isset( $profile->VideoDescription  ) && $profile->VideoDescription != "" ){ $complete = $complete + .5; }
            if( isset( $profile->ExecutiveSummary  ) && $profile->ExecutiveSummary  != "" ){ $complete = $complete + .5; }
            if( isset( $profile->OfferOverview ) && $profile->OfferOverview != "" ){ $complete = $complete + .5; }

            if( isset( $profile->OfferOpening ) && $profile->OfferOpening != "" ){ $complete = $complete + .5; }
            if( isset( $profile->OfferClosing ) && $profile->OfferClosing != "" ){ $complete = $complete + .5; }
            if( isset( $profile->Price ) && $profile->Price != "" ){ $complete = $complete + .5; }
            if( isset( $profile->TargetGoal ) && $profile->TargetGoal  != "" ){ $complete = $complete + .5; }
            if( isset( $profile->MinimumBid ) && $profile->MinimumBid  != "" ){ $complete = $complete + .5; }
            if( isset( $profile->MaximumBid ) && $profile->MaximumBid  != "" ){ $complete = $complete + .5; }
        }  

        if( User::is( 'User' ) || $viewee == 'User' ){ 
            $complete = 0;
            $profile = self::getUserProfileInfo( $id );

            if( isset( $profile->IdPhoto ) ){
                if( $profile->IdPhoto != 0 && self::fileHasFileItem( $profile->IdPhoto ) ){ 
                    $complete = $complete + 3; 
                }
            }
            if( isset( $profile->AddressPhoto ) ){
                if( $profile->AddressPhoto != 0 && self::fileHasFileItem( $profile->AddressPhoto ) ){ 
                    $complete = $complete + 2.75; 
                }
            }

            if( isset( $profile->Address ) && $profile->Address != "" ){ $complete = $complete + .25; }
            if( isset( $profile->City ) && $profile->City != "" ){ $complete = $complete + .25; }
            if( isset( $profile->State ) && $profile->State != "" ){ $complete = $complete + .25; }
            if( isset( $profile->Country ) && $profile->Country != "" ){ $complete = $complete + .25; }
            if( isset( $profile->PostalCode ) && $profile->PostalCode != "" ){ $complete = $complete + .25; }
        }

        if( isset( $profile->FirstName ) && $profile->FirstName != "" ){ $complete = $complete + .5; }
        if( isset( $profile->LastName ) && $profile->LastName != "" ){ $complete = $complete + .5; }
        if( isset( $profile->Email ) && $profile->Email != "" ){ $complete = $complete + 1; }
        if( isset( $profile->Phone ) && $profile->Phone != "" ){ $complete = $complete + 1; }        

        return $complete * 10;
    }

    public static function uniqueRandomNumbers($min, $max) {
        $numbers = range($min, $max);
        shuffle($numbers);
        $id = array_slice($numbers, 0, 1);
        return $id[0];
    }
    
    public static function newUserID() {
        $newID = self::uniqueRandomNumbers(100000,999999);
        
        $theID = User::info('UserID',$newID);
        
        if($theID == '') {
            return $newID;
        } else {
            return self::newUserID();
        }
    }
    
    /**
     * Get commision rate User ID if exists
     * 
     * @access public | static
     * @param (string) $userID : (required) : specified userid
     * @return boolean
     */
    public static function hasCommissionRate( $userID )
    {
        $db = new DB();

        $sql = "SELECT * FROM `user_referrer` WHERE `UserID` = $userID LIMIT 1";
        $query = $db->prepare($sql);
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        $return = isset( $data ) && count( $data ) ? true : false;

        return $return;
    }
    
    /**
     * Update datetime of current project view
     * 
     * @access public | static
     * @param (string) $user      : (required) : string value to be evaluated
     * @param (string) $client    : (required) : string value to be evaluated
     * @return false
     */
    public static function updateLastView( $user, $client )
    {
        $data = [];
        $common = new Common();
        if( isset( $client->ClientProfileID ) ) {
            if( !isset( $user->Code ) || $user->Code == 'USR' ){
                $data['LastViewed'] = date( 'Y-m-d H:i:s' );
                $where = array( 'ClientProfileID' => $client->ClientProfileID );                
                $common->db->update( "client_profiles", $data, $where );
            }
        }

        return false;
    }
    
    /**
     * Get no invoice count
     * 
     * @access public | static
     * @return int
     */
    public static function getNoInvoiceCount()
    {
        $return = 0;
        $db = new DB();
        $sql = "SELECT COUNT(*) as total FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `InvoiceStatus` <> 1";

        $countdata = $db->get_row($sql);
        $return = isset( $countdata ) ? $countdata->total : 0;

        return $return;
    }
    
    /**
     * Get unpaid invoice count
     * 
     * @access public | static
     * @return int
     */
    public static function getUnpaidInvoiceCount()
    {
        $return = 0;
        $db = new DB();
        $sql = "SELECT COUNT(*) as total FROM `invoices` WHERE `Status` <> 'Paid'";

        $countdata = $db->get_row($sql);
        $return = isset( $countdata ) ? $countdata->total : 0;

        return $return;
    }
    
    /**
     * Get pending withdrawal count
     * 
     * @access public | static
     * @return int
     */
    public static function getPendingWithdrawalCount()
    {
        $return = 0;
        $db = new DB();
        $sql = "SELECT COUNT(*) AS total FROM `referral_earnings` WHERE `Status` = 'Pending'";

        $countdata = $db->get_row($sql);
        $return = isset( $countdata ) ? $countdata->total : 0;

        return $return;
    }

    /**
     * Get data count for a given condition
     * 
     * @access public | static
     * @param (object) $array    : (required) : data table input object
     * @param (string) $colname  : (required) : column name on data table
     * @param (string) $operator : (required) : programming operator
     * @param (string) $ref      : (required) : string value to be evaluated
     * @return int
     */
    public static function countConditional( $array, $colname, $operator, $ref )
    {
        $i = 0;
        foreach( $array as $k => $v ){
            eval( 'if( $v->$colname '."$operator".' $ref ){ $i++; }' );
        }
        return $i;
    }

    /**
     * Get specified column sum for a given condition
     * 
     * @access public | static
     * @param (object) $array    : (required) : data table input object
     * @param (string) $colname  : (required) : column name on data table
     * @param (string) $operator : (required) : programming operator
     * @param (string) $ref      : (required) : string value to be evaluated
     * @return int
     */
    public static function sumConditional( $array, $colname, $operator, $ref )
    {
        $sum = 0;
        foreach( $array as $k => $v ){
            eval( 'if( $v->$colname '."$operator".' $ref ){ $sum += $v->$colname; }' );
        }
        return $sum;
    }

    /**
     * Get unique from specified column for a given condition
     * 
     * @package
     * @access public | static
     * @param (array) $array    : (required) : data table input array
     * @param (string) $colname : (required) : column name on data table
     * @return array
     */
    public static function removeDuplicate( $array, $colname )
    {
        $unique = array();
        if( isset( $array ) && count( $array ) ) {
            foreach ( $array as $k => $v ){
                array_push( $unique, $v->$colname );
            };
        }

        return isset( $unique ) ? array_unique( $unique ) : array();
    }

    /**
     * Get color for a specified percent range
     * 
     * @package
     * @access public | static
     * @param (int) $percent    : (required) : percentage value
     * @return string
     */
    public static function getPercentageColor( $percent ){
        $color = '#da2865';
        if( $percent <= 75 ){ $color = '#eb6090'; }
        if( $percent <= 35 ){ $color = '#f3a6be'; }
        
        return $color;
    }
    
    /**
     * Check user code in specified array of string
     * 
     * @package
     * @access public | static
     * @param (array) $levels : (required)                : a string array of defined level code
     * @param (int) $id       : (optional) default[false] : userid of current user. if not supplied, session userid will be use.
     * @return boolean
     */
    public static function codein( $levels, $id = false )
    {
        $return = false;
        $level = User::info( 'Code', $id );

        foreach( $levels as $v ){
            if( $level == $v ){
                $return = true;
            }
        }
        
        return $return;
    }

    /**
     * Check user code in specified array of string
     * 
     * @package
     * @access public | static
     * @param (string) $controller : (required) : a controller name of the caller, use '$this->controller'.
     * @param (array) $segment     : (required) : a string array of url segments.
     * @param (string) $title      : (required) : a title of the page.
     * @return string
     */
    public static function getBreadcrumb( $controller, $segment, $title ){
        $return = '';
        $common = new Common();
        $lister = $common->getSession('lister') == NULL ? array( $controller, $controller ) : $common->getSession('lister');

        if( isset( $segment[1] ) ){
            $return = '<li><a href="'.$segment[0].'">'.$segment[0].'</a></li>';
            if( $title ){
                $return .= '<li class="fa fa-angle-right"></li>';
                $return .= '<li>'.$title.'</li>';
            }
        } else {
            $return = '<li>'.$segment[0].'</li>';
        }

        if( isset( $lister[0] ) ){
            if( $lister[0] != $controller ){
                if( isset ( $segment[1] ) ){
                    // $href = $lister[1] == 'dashboard' ? User::dashboardLink() : str_ireplace( '/main', '', '/'.$lister[0].'/'.$lister[1] );
                    // $return = '<li><a href="'.$href.'">'.( isset( $lister[2] ) ? $lister[2] : $lister[1] ).'</a></li>';
                    // if( $title ){
                    //     $return .= '<li class="fa fa-angle-right"></li>';
                    //     $return .= '<li>'.$title.'</li>';
                    // }
                } else {
                    $return = '<li>'.$lister[1].'</li>';
                }
            } else {
                $return = str_ireplace( 'href="', 'href="/', $return );
            }
        }

        return $return;        
    }
    
    /**
     * Get status class style
     * 
     * @package
     * @access public | static
     * @return array
     * 
     * @todo use appropriate method name getStatusClass()
     */
    public static function getStatusCSS() 
    {        
        return array(
            'Pending' => array( 'warning', 'si si-user-follow' ),
            'Approved' => array( 'success', 'si si-user-following' ),
            'Verification' => array( 'info', 'si si-user-follow' ),
            'Incomplete' => array( 'danger', 'si si-user-unfollow' )
        );
    }
    
    /**
     * Process clients data
     * 
     * @package
     * @access public | static
     * @param (object) $clientdata  : (required) : contains raw data
     * @param (object) $dataheaders : (required) : contains headers for new data
     * @return object
     */
    public static function processClientsData( $clientdata, $dataheaders )
    {        
        if( isset( $clientdata ) ){
            if( count( $clientdata ) ){
                foreach( $clientdata as $client ){
                    $client->Data = $dataheaders;

                    foreach( $client->Data as $key ){
                        switch( $key ){
                            case 'Percentage':
                                $percentage = 0;
                                if( isset( $client->Funded ) ){
                                    $percentage = $client->Funded > 0 && $client->TargetGoal > 0 ? (int)( ( $client->Funded / $client->TargetGoal ) * 100 + 0.5 ) : 0;
                                }
                                if( isset( $client->TotalRaisedAmt ) ){
                                    $percentage = $client->TotalRaisedAmt > 0 && $client->TargetGoal > 0 ? (int)( ( $client->TotalRaisedAmt / $client->TargetGoal ) * 100 + 0.5 ) : 0;
                                }
                                $client->Data[$key] = $percentage > 100 ? 100 : $percentage;
                            break;

                            case 'BarColor':
                                $percentage = isset($client->Data['Percentage']) ? $client->Data['Percentage'] : 0;
                                $client->Data[$key] = self::getPercentageColor( $percentage );
                            break;
                            
                            case 'ProjectClass':
                                $client->Data[$key] = $client->OfferOpening > date('Y-m-d') ? 'upcoming' : 'currentoffer';
                            break;
                            
                            case 'BookHTML':
                                $blink = 'book';
                                $label = 'Book';
                                $today = new DateTime( "now" );
                                $opening = new DateTime( $client->OfferOpening );

                                //change to Register button if the project target goal is already 100% and the type of offer is limited 
                                if( ( $client->TotalBookingAmounts >= $client->TargetGoal && $client->TypeOfOffer == 'Limited' ) ||
                                    ( isset( $opening ) && $opening > $today ) ){
                                        $label = 'Register';
                                        $blink = 'register';
                                }

                                //add Again on book button if the user was booked already
                                if( isset( $client->CurrentUserBookedCount ) && $client->CurrentUserBookedCount > 0 && $label == 'Book' ){
                                    $label .= ' Again';
                                }

                                //add Again on register button if the user was registered already
                                // if( isset( $client->CurrentUserRegisterCount ) && $client->CurrentUserRegisterCount > 0 && $bookreg == 'Register' ){
                                //     $bookreg .= ' Again';                                
                                // }

                                $client->Data['BookButton'] = $label;
                                $client->Data['BookButtonLink'] = '/bookings/'.$blink.'/'.$client->ClientProfileID;
                                $client->Data[$key] = '<a href="/bookings/'.$blink.'/'.$client->ClientProfileID.'" class="btn btn-success item_button2">'.$label.'</a>';
                            break;
                            
                            case 'Avatar':
                                $client->Data[$key] = View::common()->getUploadedFiles( $client->Avatar );
                                $slug = View::asset( isset( $client->Data[$key][0]->FileSlug ) ? 'files'.$client->Data[$key][0]->FileSlug : 'images/user.png' );
                                $link = $slug != '' ? $slug : View::asset( 'images/user.png' );
                                $client->Data[$key.'Link'] = '<img src="'.$link.'">';
                            break;
                            
                            case 'Photo':
                                $client->Data[$key] = View::common()->getUploadedFiles( $client->Photo );
                                $slug = View::asset( isset( $client->Data[$key][0]->FileSlug ) ? 'files'.$client->Data[$key][0]->FileSlug : 'images/user.png' );
                                $link = $slug != '' ? $slug : View::asset( 'images/user.png' );
                                $client->Data[$key.'Link'] = '<img src="'.$link.'">';
                            break;
                            
                            case 'SliderImage':
                                $client->Data[$key] = View::common()->getUploadedFiles( $client->ImageID );
                                $slug = View::asset( isset( $client->Data[$key][0]->FileSlug ) ? 'files'.$client->Data[$key][0]->FileSlug : 'images/slide-3.jpg' );
                                $link = $slug != '' ? $slug : View::asset( 'images/slide-3.jpg' );
                                $client->Data[$key.'Link'] = '<img src="'.$link.'">';
                            break;
                            
                            case 'CompanyLogo':
                                $client->Data[$key] = View::common()->getUploadedFiles( $client->CompanyLogo );
                                $slug = View::asset( isset( $client->Data[$key][0]->FileSlug ) ? 'files'.$client->Data[$key][0]->FileSlug : 'images/placeholder.png' );
                                $link = $slug != '' ? $slug : View::asset( 'images/placeholder.png' );
                                $client->Data[$key.'Link'] = '<img src="'.$link.'">';
                            break;
                            
                            case 'CompanyPhoto':
                                // $companyPhoto = View::common()->getUploadedFiles( $client->CompanyPhoto );
                                // $file = isset( $companyPhoto[0]->FileSlug ) ? 'files'.$companyPhoto[0]->FileSlug : false;
                                // $client->Data[$key] = $companyPhoto;
                                // $fileUrl = ( View::asset( $file ) ) ? View::asset( $file ) : View::asset( 'images/placeholder-proj.jpg' );
                                // $client->Data[$key.'Link'] = '<img src="'.$fileUrl.'">';

                                $client->Data[$key] = View::common()->getUploadedFiles( $client->CompanyPhoto );
                                $slug = View::asset( isset( $client->Data[$key][0]->FileSlug ) ? 'files'.$client->Data[$key][0]->FileSlug : 'images/placeholder-proj.jpg' );
                                $link = $slug != '' ? $slug : View::asset( 'images/placeholder-proj.jpg' );
                                $client->Data[$key.'Link'] = '<img src="'.$link.'">';
                            break;
                            
                            default: break;
                        }
                    }
                }
            }
        }

        return $clientdata;
    }

    /**
     * Get project's funded amount
     * 
     * @package
     * @access public | static
     * @param (int) $profileID  : (required) : client's profile ID
     * @return integer
     * @author Mo
     */
    public static function getProjectAmountFunded( $profileID )
    {
        $return = 0;
        $db = new DB();
        $sql = "SELECT SUM(TotalAmountAttached) as TotalAmountFunded FROM `bookings` WHERE ClientProfileID = ".$profileID." AND BookingStatus = 'Approved'";

        $countdata = $db->get_row($sql);
        $return = isset( $countdata->TotalAmountFunded ) ? $countdata->TotalAmountFunded : 0;

        return $return;
    }
     
    
    /**
     * Get redirect page
     * 
     * @package
     * @access public | static
     * @param (string) $message : (optional) : for message info on page
     * @return string
     */
    public static function redirect( $message = '' )
    {
        $common = new Common();
        $common->setSession( 'message', $message );
        $redirect = $common->getSession('redirect') !== NULL ? $common->getSession('redirect') : '/';
        
        return $redirect;
    }
    
    /**
     * Get set breadcumb segmetns
     * 
     * @package
     * @access public | static
     * @param (string) $message : (optional) : for message info on page
     * @return string
     */
    public static function getCustomBreadcrumb( $level, $main, $sub1, $sub2 = false )
    {
        $settings = array(
        );
        
        return $breadcrumb;
    }
    
    /**
     * Compute commission
     * 
     * @package
     * @access public | static
     * @param (string) $amount : (required) : investment amount
     * @param (string) $rate   : (required) : commission/subscription rate in percent value
     * @return float
     */
    public static function computeCommission( $amount, $rate )
    {
        $amount = isset( $amount ) ? str_replace( ',', '', $amount ) : 0.00;
        $rate = isset( $rate ) ? str_replace( ',', '', $rate ) : 0.50;
        $commission = $amount > 0 && $rate > 0 ? $amount * ( $rate / 100 ) : 0.00;
        
        return number_format( $commission, 2 );
    }
    
    /**
     * Get monthly earning
     * 
     * @package
     * @access public | static
     * @param (string) $earning : (required) : monthly earning
     * @return array
     */
    public static function processMonthlyEarning( $earnings )
    {
        $months = self::getMonth();
        unset( $months[0] );
        foreach( $months  as $key => $value ){
            $months[$key] = explode( ':', $value )[1];
        }

        $monthlyEarnings = array();
        foreach( $months as $month ){
            if( isset( $earnings ) && count( $earnings ) ){
                foreach ( $earnings as $earning ){
                    if( isset( $earning->Months ) && $earning->Months ==  $month ){
                        $monthlyEarnings[$month] = number_format( $earning->TotalReferralCommission, 2 );
                    } else {
                        $monthlyEarnings[$month] = '0.00';
                    }
                }
            } else { $monthlyEarnings[$month] = '0.00'; }
        }
        
        return $monthlyEarnings;
    }

    /**
     * Get company name
     * 
     * @package
     * @access public | static
     * @param (string) $userid : (required) : Client userid
     * @return array
     */
    public static function getCompanyName( $userid )
    {
        $return = false;
        $db = new DB();

        $sql = "SELECT CompanyName FROM `client_profiles` WHERE `UserID` = '".$userid."'";
        
        if(User::role() == 'Client') {
            $sql = "SELECT CompanyName FROM `client_profiles` WHERE `UserID` = '".$userid."'";
        }

        if(User::role() == 'Assistant') {
            $sql = "SELECT cp.CompanyName FROM `client_profiles` WHERE `UserID` = (SELECT ReferrerUserID FROM users WHERE UserID = '".$userid."')";
        }

        $result = $db->get_row($sql);
        $return = isset( $result->CompanyName ) ? $result->CompanyName : false;

        return $return;
    }

    /**
     * Validate if user has access in selected project forum. If forum is not selected yet, the most recent project that was visited will be the reference.
     * 
     * @package
     * @access public | static
     * @return object
     */
    public static function validateForumAccess()
    {
        $client = array();
        $clientUserID = false;
        $clientProfileID = false;
        $userinfo = User::info();
        $common = new Common();
        switch( $userinfo->Code ){
            case 'CLN': $clientUserID = $userinfo->UserID; break;
            case 'ASST': $clientUserID = $userinfo->ReferrerUserID; break;            
            default:
                $cookie = View::clientinfo() ? View::clientinfo() : View::redirect( 'forums' );
                $client = (object) array( 'CompanyName' => $cookie[0], 'ClientProfileID' => $cookie[1] );

                //redirect if no booking
                if( User::can( 'Administer All' ) || User::can( 'Administer Forums' ) ){} else {
                    if( self::checkBookingByProject( $userinfo->UserID, $client->ClientProfileID ) == 0 ) { View::redirect( 'forums' ); }
                }
                break;
        }

        if( $clientUserID ){
            $client = self::getClientProfileInfo( $clientUserID );
            if( !isset( $client->ClientProfileID ) ) { View::redirect( 'forums' ); }
        }

        return $client;
    }

    /**
     * Validate if user has access in selected project blogs. If project is not selected yet, the most recent project that was visited will be the reference.
     * 
     * @package
     * @access public | static
     * @return object
     */
    public static function validateBlogAccess()
    {
        $common = new Common();
        $userinfo = User::info();
        $userinfo->Seeder = false;
        switch( $userinfo->Code ){
            case 'CLN':
            case 'ASST':
                $client = self::getClientProfileInfo( $userinfo->Code == 'CLN' ? $userinfo->UserID : $userinfo->ReferrerUserID );
                $userinfo->Bookings = 1000;
                $userinfo->CompanyName = isset( $client->CompanyName ) ? $client->CompanyName : '';
                $userinfo->ClientProfileID = isset( $client->ClientProfileID ) ? $client->ClientProfileID : false;
                break;

            default:
                $userinfo->Seeder = true;
                $cookie = View::clientinfo() ? View::clientinfo() : false;
                if( $cookie ){
                    $userinfo->CompanyName = $cookie[0];
                    $userinfo->ClientProfileID = $cookie[1];
                }

                //check booking
                $userinfo->Bookings = (int) isset( $userinfo->ClientProfileID ) ? self::checkBookingByProject( $userinfo->UserID, $userinfo->ClientProfileID ) : 0;
                break;
        }

        return $userinfo;
    }

    /**
     * Check if user has booking in particular project.
     * 
     * @package
     * @access public | static
     * @param (int) $userID          : (required) : current user id
     * @param (int) $clientProfileID : (required) : referrence client profile id
     * @return int
     */
    public static function checkBookingByProject( $userID, $clientProfileID )
    {
        $return = 0;
        if( Option::get( 'global_promo' ) == 1 ){
            $return = 1;
        } else {
            if( strtolower( $clientProfileID ) == 'so' ){
                $return = 1;
            } else {
                $db = new DB();
                $where = $clientProfileID ? " AND `ClientProfileID` = $clientProfileID" : "";
                $sql = "SELECT COUNT(*) `Total` FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `UserID` = $userID  AND `ClientProfileID` = '$clientProfileID';";

                $data = $db->get_row( $sql );
                $return = isset( $data->Total ) ? $data->Total : 0;
            }
        }

        return (int) $return;
    }

    /**
     * Gete client id if user is assistants manager.
     * 
     * @package
     * @access public | static
     * @param (object) $userinfo : (optional) : current user info or user id
     * @return int
     */
    public static function getClientUserID( $userinfo = false )
    {
        if( $userinfo ){
            if( !is_object( $userinfo ) ){
                $userinfo = User::info( false, $userinfo );
            }
        } else $userinfo = User::info();

        $return = $userinfo->UserID;
        if( isset( $userinfo->Code ) && $userinfo->Code == 'ASST' ){
            $return = $userinfo->ReferrerUserID;
        }

        return $return;
    }

    /**
     * Check if current user has investment in selected client
     * 
     * @package
     * @access public | static
     * @param (int) $ClientProfileID : (required) : selected client id
     * @param (object) $userinfo     : (optional) : current user info
     * @return int
     */
    public static function myInvestments( $ClientProfileID, $userinfo = false )
    {
        if( !$userinfo ) $userinfo = User::info();

        $sql = "SELECT COUNT(UserID) AS Total FROM bookings WHERE BookingType = 'Booking' AND BookingStatus = 'Approved' AND ClientProfileID = $ClientProfileID AND UserID = $userinfo->UserID";

        $db = new DB();
        $data = $db->get_row( $sql );

        $return = isset( $data->Total ) ? $data->Total : 0;

        if( (int)$return == 0 ){
            $return = User::is( 'Client' ) || User::can( 'Manage Clients' ) ? 1 : 0;
        }

        return (int)$return;
    }

    /**
     * Get Wallet Total balance
     * 
     * @package
     * @access public | static
     * @param (int) $UserID : (optional) : user wallet balance
     * @return decimal
     */
    public static function getwalletbalance( $UserID = false )
    {
        $db = new DB();

        $where = ($UserID) ? $UserID : User::info('UserID');

        // $sql = "SELECT * 
        //         FROM `wallet` w 
        //         WHERE w.`UserID` = ".$where." AND w.`TransactionStatus` IN ('Approved', 'Verified') AND NOT (w.`TransactionStatus` = 'Verified' AND w.`Method` = 1)";
        $sql = "SELECT * 
                FROM `wallet` w 
                WHERE w.`UserID` = ".$where." AND w.`TransactionStatus` IN ('Approved') ORDER BY w.`ApproveDate` DESC";
        $query = $db->prepare($sql);
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        $runbal = 0;
        $ctr = 0;
        $remainval = 0;
        if(isset($data)){
            foreach($data as $wallet) { $ctr++;
                switch ($wallet->TransactionType) {
                    case 'Credit':
                        $runbal = $remainval+$wallet->TransactionAmount;
                        break;
                    case 'Debit':
                        $runbal = $remainval-$wallet->TransactionAmount;
                        break;
                }

                $remainval = $runbal;
            } 
        }

        return $runbal;
    }

    /**
     * Delete also data in `file_groups` if deletion reference in `file_items` is `FileItemID`
     * 
     * @package
     * @access public | static
     * @param (int) $FileItemID : (required) : FileItemID <- please verify thouroughly the ID to be passed
     * @return NA
     */
    public static function deleteFileGroupsData( $FileItemID )
    {
        $db = new DB();
        $query = "DELETE FROM `file_groups` WHERE `FileID` = ( SELECT DICTINCT `FileID` FROM `file_items` WHERE `FileItemID` = $FileItemID LIMIT 1 )";
        $db->query( $query );

        return true;
    }

    /**
     * Get Core Bank Accounts As Options
     * 
     * @package
     * @access public | static
     * @return decimal
     */
    public static function getCoreBankAccountsAsOption()
    {
        $db = new DB();

        $sql = "SELECT cba.*, ul.Code as AddedBy_RoleCode, um.FirstName as AddedBy_FirstName, um.LastName as AddedBy_LastName  
                FROM core_bank_accounts cba 
                LEFT JOIN users u ON cba.UserID = u.UserID
                LEFT JOIN user_meta um ON cba.UserID = um.UserID 
                LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID";
        $query = $db->prepare($sql);
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[$row->CoreBankAccountID] = $row->Name.' - '.$row->AccountName.' ('.$row->AccountNumber.')';
        }
        unset($query);

        return $data;
    }

    /**
     * Get Primary Core Bank Account Details
     * 
     * @package
     * @access public | static
     * @return decimal
     */
    public static function getPrimaryBankAccount($ID = false)
    {
        $db = new DB();

        $bankID = $ID ? $ID : Option::get( 'primary_bank_account' );

        $sql = "SELECT cba.*, ul.Code as AddedBy_RoleCode, um.FirstName as AddedBy_FirstName, um.LastName as AddedBy_LastName  
                FROM core_bank_accounts cba 
                LEFT JOIN users u ON cba.UserID = u.UserID
                LEFT JOIN user_meta um ON cba.UserID = um.UserID 
                LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
                WHERE cba.`CoreBankAccountID` = ".$bankID." LIMIT 1";

        $data = $db->get_row( $sql );

        return $data;
    }

    /**
     * Get file groups by reference id
     * 
     * @package
     * @access public | static
     * @param (int) $ID : (required) : reference id (user id or client profile id)
     * @return object
     */
    public static function getFileGroupsByReferenceID( $ID )
    {
        $data = [];
        $db = new DB();

        $sql = "SELECT DISTINCT `FileID` FROM `file_groups` WHERE `ReferenceID` = $ID";
        
        $query = $db->prepare($sql);
        $query->execute();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Delete file by FileID and all associated tables
     * 
     * @package
     * @access public | static
     * @param (int) $fileID : (required) 
     * @return NA
     */
    public static function deleteFileAndAssociates( $fileID )
    {
        $db = new DB();

        foreach( View::common()->getUploadedFiles( $fileID ) as $f ){
           unlink( $f->FilePath );
        }
        
        $where = array( 'FileID' => $fileID );
        $db->delete( "files", $where );
        $db->delete( "file_items", $where );
        $db->delete( "file_groups", $where );
        
        $msg = "Permanently deleted files and associated ids. FileID#$fileID.";

        if( isset( $fileID ) && $fileID != '' ){
            View::common()->setSession( "message", $msg );
            App::activityLog( $msg );
        }

        return true;
    }

    /**
     * Delete all files on user deletion
     * 
     * @package
     * @access public | static
     * @param (int) $userID : (required) : selected user id
     * @return NA
     */
    public static function deleteFilesOnDeleteUser( $userID )
    {
        $user = self::getUserProfileInfo( $userID );
        $client = self::getClientProfileInfo( $userID );
        $userfgs = self::getFileGroupsByReferenceID( $userID );
        $cpid = isset( $client->ClientProfileID ) && $client->ClientProfileID != NULL ? $client->ClientProfileID : 0;
        $clientfgs = self::getFileGroupsByReferenceID( $cpid );

        // delete user's avatar file
        try{
            if( isset( $user ) && count( $user ) ){
                $av = $user->Avatar == NULL ? 0 : $user->Avatar;
                $ip = $user->IdPhoto == NULL ? 0 : $user->IdPhoto;
                $ap = $user->AddressPhoto == NULL ? 0 : $user->AddressPhoto;
                self::deleteFileAndAssociates( $av );
                self::deleteFileAndAssociates( $ip );
                self::deleteFileAndAssociates( $ap );
            }
        } catch( Exception $e ){}

        // delete client's document files
        try{
            if( isset( $client ) && count( $client ) ){
                $logo = $client->CompanyLogo == NULL ? 0 : $client->CompanyLogo;
                $photo = $client->CompanyPhoto == NULL ? 0 : $client->CompanyPhoto;
                $release = $client->CompanyReleaseFileID == NULL ? 0 : $client->CompanyReleaseFileID;
                $research = $client->CompanyResearchFileID == NULL ? 0 : $client->CompanyResearchFileID;
                $diligence = $client->CompanyDueDiligenceFileID == NULL ? 0 : $client->CompanyDueDiligenceFileID;
                self::deleteFileAndAssociates( $logo );
                self::deleteFileAndAssociates( $photo );
                self::deleteFileAndAssociates( $release );
                self::deleteFileAndAssociates( $research );
                self::deleteFileAndAssociates( $diligence );
            }
        } catch( Exception $e ){}

        // delete user's multiple document files
        try{
            if( isset( $userfgs ) && count( $userfgs ) ){
                foreach( $userfgs as $fg ){
                    $fid = $fg->FileID == NULL ? 0 : $fg->FileID;
                    self::deleteFileAndAssociates( $fid );
                }
            }
        } catch( Exception $e ){}

        // delete client's multiple document files
        try{
            if( isset( $clientfgs ) && count( $clientfgs ) ){
                foreach( $clientfgs as $fg ){
                    $fid = $fg->FileID == NULL ? 0 : $fg->FileID;
                    self::deleteFileAndAssociates( $fid );
                }
            }
        } catch( Exception $e ){}

        return true;
    }
    
    /**
     * Get icons list
     * 
     * @access public | static
     * @param (string) $keyword : (optional) : keyword
     * @return object
     */
    public static function getIconList( $keyword = false )
    {
        $db = new DB();
        $where = $keyword ? " WHERE `Description` LIKE '%$keyword%' " : "";

        $sql = "SELECT DISTINCT * FROM `icons` $where";
        $query = $db->prepare($sql);
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Clean String Removes special chars
     * 
     * @access public | static
     * @param (string)
     */
    public static function cleanstring($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * Encode hashkey
     * 
     * @access public | static
     * @param (q)
     */
    public static function encodeHash( $q ){
        $rumble = '';
        $hpctr = 0;
        $inctr = 0;
        $inlen = strlen( $q );
        $hplen = strlen( HASHPHRASE );
        while( $hplen > $hpctr ){
            $rumble .= substr( HASHPHRASE, $hpctr, 1 );
            if( $hpctr > 7 && $inlen > $inctr ){
                $rumble .= substr( $q, $inctr, 1 );
                $inctr++;
            }
            $hpctr++;
        }

        return base64_encode( $rumble );
    }

    /**
     * Decode hashkey
     * 
     * @access public | static
     * @param (q)
     */
    public static function decodeHash( $q ){
        $ret = '';
        $ctr = 0;
        $rumble = base64_decode( $q );
        $rulen = strlen( $rumble );
        $hplen = strlen( HASHPHRASE );
        $inlen = $rulen - $hplen;
        while( $rulen > $ctr ){
            if( $ctr > 8 && strlen( $ret ) < $inlen ){
                $ret .= substr( $rumble, $ctr, 1 );
                $ctr++;
            }
            $ctr++;
        }

        return $ret;
    }

    public static function strReplaceLast( $search, $replace, $str ){
        if( ( $pos = strrpos( $str, $search ) ) !== false ){
            $search_length  = strlen( $search );
            $str = substr_replace( $str, $replace, $pos, $search_length );
        }
        return $str;
    }

    /**
     * Sanitize ending br tags
     * 
     * @access public | static
     * @param (q)
     */
    public static function sanitizeEndingBRTags( $q ){
        $q = trim( $q );
        $qLength = strlen( $q );
        $pattern = '<br />';
        $pLength = strlen( $pattern );
        $suf = '';
        if( substr( $q, 0, 3 ) == '<p>' ){
            $suf = '</p>';
        }
        $sLength = strlen( $suf );

        if( substr( $q, $qLength - ($pLength+$sLength), $pLength ) == $pattern ){
            $q = substr( $q, 0, $qLength - ($pLength+$sLength) );
        }

        $q = trim( $q.$suf );
        $qLength = strlen( $q );
        if( substr( $q, $qLength - ($pLength+$sLength), $pLength ) == $pattern ){
            $q = self::sanitizeEndingBRTags( $q );
        }

        return $q;
    }

    /**
     * Sanitize ending p tags
     * 
     * @access public | static
     * @param (q)
     */
    public static function sanitizeEndingPTags( $q ){
        $q = trim( $q );
        $qLength = strlen( $q );
        $pattern = '<p>&nbsp;</p>';
        $first = $pattern;
        $pLength = strlen( $pattern );
        $fLength = $pLength;

        if( substr( $q, $qLength - $pLength, $pLength ) == $pattern ){
            $q = substr( $q, 0, $qLength - $pLength );
        }

        $q = trim( $q );
        $qLength = strlen( $q );
        $pattern = '<p> </p>';
        $pLength = strlen( $pattern );

        if( substr( $q, $qLength - $pLength, $pLength ) == $pattern ){
            $q = substr( $q, 0, $qLength - $pLength );
        }

        $q = trim( $q );
        $qLength = strlen( $q );
        $pattern = '<p></p>';
        $pLength = strlen( $pattern );

        if( substr( $q, $qLength - $pLength, $pLength ) == $pattern ){
            $q = substr( $q, 0, $qLength - $pLength );
        }

        $q = trim( $q );
        $qLength = strlen( $q );
        if( substr( $q, $qLength - $fLength, $fLength ) == $first ){
            $q = self::sanitizeEndingPTags( $q );
        }

        return $q;
    }
}
?>