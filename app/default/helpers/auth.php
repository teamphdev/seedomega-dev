<?php
/**
 * Auth Class
 *
 * @category   Authentication
 * @package    AppAuth
 * @author     Mo <mogol@gmail.com>
 * @copyright  (c) 2018 TechHon Web Solutions
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0.0
 * @link       http://www.techhon.com
 * @since      Class available since Release 1.2.0
 */
class Auth {     
    static public function userSession($redirect='users/login') {
        if(!User::isLoggedIn()) {
            View::redirect($redirect);
        }
    }

    static public function noUserSession($redirect=false) {
        if(User::isLoggedIn()) {
            if($redirect) {
                View::redirect($redirect);
            } else {
                $referrer = (View::common()->getCookie(Config::get('REFERRER'))) ? View::common()->getCookie(Config::get('REFERRER')) : User::dashboardLink(true);

                View::redirect( $referrer );
            }
        }
    }

    static public function isLoggedIn() {
        return User::isLoggedIn();
    }

    static public function continueFor($role = 'Seeder', $redirect='') {
        if(!User::is($role)) {
            View::redirect($redirect);
        }
    }
}

?>