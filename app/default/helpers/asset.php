<?php  
/**
 * Asset Class
 *
 * @category   Asset
 * @package    AppAsset
 * @author     Mo <mogol@gmail.com>
 * @copyright  (c) 2018 TechHon Web Solutions
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: 1.0.0
 * @link       http://www.techhon.com
 * @since      Class available since Release 2.0.0
 */
class Asset {     
    static public function init()
    {
        self::registerCSS();
        self::registerJS();
    }

    static public function registerJS()
    {
    	// Clear assets list incase it was previously registered
        Assets::clear('js');
        
    	// jQuery (necessary for JavaScript plugins)
        Assets::js( "jquery",         "vendor/jquery/dist/jquery.min.js", true );
        Assets::js( "jqueryui",       "assets/js/jquery-ui.js", true );
        Assets::js( "migrate",        "assets/js/migrate.js", true );
        Assets::js( "popper",         "assets/js/popper.min.js", true );

        // Bootstrap Javascript 
        Assets::js( "bootstrap",      "assets/js/bootstrap.min.js", true );
        Assets::js( "bootstrap2",      "assets/js/bootstrap.min.js", true );

        // EXTENSION PLUGINS
        Assets::js( "rainbow",        "assets/js/pieprogress/scripts/rainbow.min.js", true );
        Assets::js( "jq-pie",         "assets/js/pieprogress/scripts/jquery-asPieProgress.js", true );
        Assets::js( "sr-plugin",      "assets/js/slider-revolution/rs-plugin/js/jquery.themepunch.plugins.min.js", true );
        Assets::js( "sliderrev",      "assets/js/slider-revolution/rs-plugin/js/jquery.themepunch.revolution.min.js", true );
        Assets::js( "bxslider",       "assets/js/bxslider/jquery.bxslider.min.js", true );
        Assets::js( "maps",           "assets/js/maps.js", true );
        Assets::js( "maps-key",       "assets/js/maps.js?key=AIzaSyAZKyQ-_fy5S1FGZ7cYkmbieIRFjzu1jvk", true ); // use by controller : options
        Assets::js( "jqmaps",         "assets/js/maps/scripts/jquery.gmap.min.js", true );
        Assets::js( "scroll",         "assets/js/jquery.scroll.js", true );
        Assets::js( "hoverizr",       "assets/js/jquery.hoverizr.min.js", true );
        Assets::js( "placeholder",    "assets/js/jquery.placeholder.min.js", true );

        Assets::js( "datatables",     "vendor/datatables.net/js/jquery.dataTables.min.js", true );
        Assets::js( "dt-bootstrap",   "vendor/datatables.net-bs/js/dataTables.bootstrap.min.js", true );
        Assets::js( "dt-fixedheader", "vendor/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js", true );
        Assets::js( "dt-keytable",    "vendor/datatables.net-keytable/js/dataTables.keyTable.min.js", true );
        Assets::js( "dt-responsive",  "vendor/datatables.net-responsive/js/dataTables.responsive.min.js", true );
        Assets::js( "dt-responsive-b","vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js", true );
        Assets::js( "dt-scroller",    "vendor/datatables.net-scroller/js/datatables.scroller.min.js", true );
        Assets::js( "dt-checkboxes",  "vendor/datatables.net-checkboxes/js/dataTables.checkboxes.min.js", true );
        Assets::js( "auto-complete",  "vendor/devbridge-autocomplete/dist/jquery.autocomplete.min.js", true );
        Assets::js( "paginate",       "vendor/jquery.easyPaginate-master/lib/jquery.easyPaginate.js", true );
        Assets::js( "progress",       "vendor/bootstrap-progressbar/bootstrap-progressbar.min.js", true );
        Assets::js( "bs-wizard",      "vendor/bootstrap-wizard/jquery.bootstrap.wizard.min.js", true );
        Assets::js( "yadcf",          "vendor/yadcf-master/src/jquery.dataTables.yadcf.js", true );
        Assets::js( "validate",       "vendor/jquery-validation/jquery.validate.min.js", true );
        Assets::js( "mask",           "vendor/jQuery-Mask-Plugin/src/jquery.mask.js", true );
        Assets::js( "richtext",       "vendor/Rich-Text-Editor/jquery.richtext.js", true );
        Assets::js( "switchery",      "vendor/switchery/dist/switchery.min.js", true );
        Assets::js( "lightbox",       "vendor/html5lightbox/html5lightbox.js", true );
        Assets::js( "validator",      "vendor/validator/validator.min.js", true );
        Assets::js( "icheck",         "vendor/iCheck/icheck.min.js", true );
        Assets::js( "chart",          "vendor/chartjs/Chart.min.js", true );
        Assets::js( "polyfiller",     "assets/js/polyfiller.min.js", true );
        Assets::js( "moment",         "assets/js/moment/moment.min.js", true );
        Assets::js( "daterange",      "assets/js/datepicker/daterangepicker.js", true );
        Assets::js( "introducing",    "assets/js/jquery.introducing.js", true );
        Assets::js( "generatepw",     "assets/js/plugins/generatepassword.js", true );
        Assets::js( "isotope",        "assets/js/plugins/isotope.pkgd.min.js", true );
        Assets::js( "fileinput",      "assets/js/fileinput.js", true );
        Assets::js( "profile",        "assets/js/profile.js", true );
        Assets::js( "table",          "assets/js/table.js", true );
        Assets::js( "pw",             "assets/js/pw.js", true );

        Assets::js( "ui-widget",      "assets/js/fileupload/jquery.ui.widget.js", true );
        Assets::js( "transport",      "assets/js/fileupload/jquery.iframe-transport.js", true );
        Assets::js( "fu",             "assets/js/fileupload/jquery.fileupload.js", true );
        Assets::js( "fu-process",     "assets/js/fileupload/jquery.fileupload-process.js", true );
        Assets::js( "fu-validate",    "assets/js/fileupload/jquery.fileupload-validate.js", true );

        Assets::js( "dt-base",        "assets/js/base_tables_datatables.js", true );
        Assets::js( "scroll-to",      "assets/js/aSimpleTour/jquery.scrollTo.js", true );
        Assets::js( "asimple-tour",   "assets/js/aSimpleTour/jquery.aSimpleTour.js", true );

        Assets::js( "tinymce-min",    "assets/js/tinymce/tinymce.min.js", true );
        Assets::js( "tinymce-hidden", "assets/js/hiddenmce.js", true );
        Assets::js( "tinymce",        "assets/js/tinymce.js", true );

        // Custom Settings
        Assets::js( "custom",         "assets/js/custom.js", true );
        Assets::js( "clients",        "assets/js/clients.js", true );
        Assets::js( "clients1",       "assets/js/clients.js", true );
        Assets::js( "clients18",      "assets/js/clients.js", true );
        Assets::js( "invoices",       "assets/js/invoices.js", true );
        Assets::js( "manage",         "assets/js/manage.js", true );
        Assets::js( "support",        "assets/js/support.js", true );
        Assets::js( "wallet",         "assets/js/pages/wallet.js", true );

        Assets::js( "plugin",         "assets/js/plugin.js", true );
        Assets::js( "countdown",      "assets/js/countdown.js", true );
        Assets::js( "retina",         "assets/js/retina.min.js", true );
        
        // Assets::js( "clients1",       "assets/js/clients.js?1", true );
        // Assets::js( "clients18",      "assets/js/clients.js?18", true );
        // Assets::js( "invoices",       "assets/js/invoices.js?v=2", true );
        // Assets::js( "support",        "assets/js/support.js?v=2", true );
    }

    static public function registerCSS()
    {
        // Clear assets list incase it was previously registered
        Assets::clear('css');
        
        // Bootstrap CSS 
        Assets::css( "bootstrap",         "assets/css/bootstrap.min.css" );

        // Basic Stylesheets 
        Assets::css( "w3",                "assets/css/w3.css" );
        Assets::css( "style",             "assets/css/style.css" );
        Assets::css( "custom",            "assets/css/custom-style.css" );
        Assets::css( "responsive",        "assets/css/responsive.css" );

        // EXTENSION STYLES
        Assets::css( "rainbow",           "assets/js/pieprogress/css/rainbow.css" );
        Assets::css( "progress",          "assets/js/pieprogress/css/progress.css" );
        Assets::css( "sr-style",          "assets/js/slider-revolution/css/style.css" );
        Assets::css( "sr-plugin",         "assets/js/slider-revolution/rs-plugin/css/settings.css" );
        Assets::css( "bxslider",          "assets/js/bxslider/jquery.bxslider.css" );
        Assets::css( "circle",            "vendor/css-progress-circle/circle.css" );

        // DEFAULT CSS
        Assets::css( "fa",                "assets/css/font-awesome.min.css" );
        Assets::css( "icons",             "assets/css/icons.css" );
        Assets::css( "fileinput",         "assets/css/fileinput.css" );

        Assets::css( "dt-bootstrap",      "vendor/datatables.net-bs/css/dataTables.bootstrap.min.css" );
        Assets::css( "dt-buttons",        "vendor/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" );
        Assets::css( "dt-fixedheader",    "vendor/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" );
        Assets::css( "dt-responsive-b",   "vendor/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" );
        Assets::css( "dt-scroller",       "vendor/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" );
        Assets::css( "dt-checkboxes",     "vendor/datatables.net-checkboxes/css/dataTables.checkboxes.css" );
        Assets::css( "richtext",          "vendor/Rich-Text-Editor/richtext.min.css" );
        Assets::css( "lightbox",          "vendor/html5lightbox/html5lightbox.css" );
        Assets::css( "yadcf",             "vendor/yadcf-master/jquery.dataTables.yadcf.css" );
        Assets::css( "icheck",            "vendor/iCheck/skins/flat/green.css" );
        Assets::css( "daterange",         "assets/js/datepicker/daterangepicker.css" );
        Assets::css( "skin",              "assets/js/tinymce/skins/lightgray/skin.min.css" );

        // Your Custom Styles 
        Assets::css( "my-styles",         "assets/css/my-styles.css" );

        // Support 
        Assets::css( "framework",         "assets/css/support/app.framework.css" );
    }

    static public function admin()
    {
        Assets::enqueue('css', array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "fa", "icons", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "my-styles"
        ));

        Assets::enqueue('js', array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "dt-base", "custom",
            "plugin", "countdown", "retina"
        ), true );
    }

    static public function affiliates()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "w3", "style", "custom", "responsive", "bxslider", "fa", "icons",
            "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", "dt-scroller", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery",  "migrate", "bootstrap",
            "bxslider", "maps", "jqmaps", "scroll", "hoverizr", "placeholder",
            "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "bs-wizard", "validate", "dt-base", "custom",
            "plugin", "countdown", "retina"
        ), true );
    }

    static public function blogs()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "richtext", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "richtext","tinymce-min", "tinymce-hidden",
            "tinymce",
            "fileinput", "dt-base", "custom", "plugin", "countdown", "retina"
        ), true );
    }

    static public function bookings()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "circle", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "lightbox", "daterange", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "lightbox", "moment",
            "daterange", "fileinput", "dt-base", "custom", "plugin", "countdown", "retina"
        ), true );
    }

    static public function client()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "w3", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin",
            "bxslider", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "richtext", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "jqueryui", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "dt-base", "paginate", "bs-wizard", "validate", "richtext", "lightbox", 
            "chart", "polyfiller", "moment", "daterange", "introducing", "fileinput", "tinymce-min", "tinymce-hidden",
            "tinymce", "custom", "clients", "plugin", "countdown", "retina",
        ), true );
    }

    static public function common()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin",
            "bxslider", "fa", "icons", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "my-styles", "lightbox"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "bs-wizard", "validate", "dt-base", "custom",
            "plugin", "countdown", "retina", "lightbox"
        ), true );
    }

    static public function faq()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin",
            "bxslider", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "richtext", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "richtext", "lightbox", 
            "moment", "daterange", "fileinput", "tinymce-min", "tinymce-hidden", "tinymce", "dt-base", "custom",
            "clients1", "plugin", "countdown", "retina"
        ), true );
    }

    static public function forum()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive",
            "bxslider", "circle", "fa", "icons", "fileinput",
            "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", "dt-scroller",
            "lightbox", "richtext", "daterange", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "jqueryui", "bootstrap", "migrate",
            "bxslider", "maps", "jqmaps", "scroll", "hoverizr", "placeholder",
            "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable",
            "dt-responsive", "dt-responsive-b", "dt-scroller",
            "bs-wizard", "validate", "richtext", "lightbox", "moment", "daterange", "fileinput",
            "tinymce-min", "tinymce-hidden", "tinymce", "dt-base", "custom", "plugin", "retina"
        ), true );
    }

    static public function hubcare()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin",
            "bxslider", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "richtext", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "richtext", "lightbox", 
            "moment", "daterange", "fileinput",
            // "ui-widget", "transport", "fu", "fu-process", "fu-validate",
            "dt-base", "custom", "clients18", "plugin", "countdown", "retina"
        ), true );
    }

    static public function invoices()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "w3", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin",
            "bxslider", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "richtext", "yadcf", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "richtext", "lightbox", 
            "moment", "daterange", "fileinput", "yadcf",
            // "ui-widget", "transport", "fu", "fu-process", "fu-validate",
            "dt-base", "custom", "invoices", "plugin", "countdown", "retina"
        ), true );
    }

    static public function login()
    {
        Assets::enqueue('css', array(
            "bootstrap", "style", "custom", "responsive", "fa", "icons", "my-styles"
        ));

        Assets::enqueue('js', array(
            "jquery", "jqueryui", "migrate", "bootstrap", "fundz", "custom"
        ), true );
    }

    static public function main()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "fa", "icons", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", "dt-scroller", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery",  "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "bs-wizard", "validate", "paginate", "isotope", "dt-base", "tinymce-min", "tinymce",
             "custom", "plugin", "countdown", "retina"
        ), true );
    }

    static public function manage()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "circle", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "lightbox", "skin", "daterange", "my-styles"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "paginate", "bs-wizard", "validate", "lightbox", 
            "moment", "daterange", "fileinput", "dt-base", "tinymce-min", "tinymce", "tinymce-hidden", "custom",
            "manage", "plugin", "countdown", "retina"
        ), true );
    }

    static public function media()
    {
        Assets::enqueue('css',  array(
            "icheck", "fileinput", "lightbox", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "lightbox", "yadcf"
        ));

        Assets::enqueue('js',  array(
            "progress", "switchery", "icheck", "tinymce-min", "tinymce", "fileinput", "auto-complete", "lightbox",
            "validator", "custom", "profile", "pw", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable",
            "dt-responsive", "dt-responsive-b", "dt-scroller", "yadcf", "moment", "daterange", "table"
        ), true );
    }

    static public function support()
    {
        Assets::enqueue('css',  array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin",
            "bxslider", "fa", "icons", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "my-styles", "fileinput", "lightbox"
        ));

        Assets::enqueue('js',  array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "bs-wizard", "validate", "dt-base", "custom", "support",
            "plugin", "countdown", "retina", "fileinput", "lightbox"
        ), true );
    }

    static public function user()
    {
        Assets::enqueue('css', array(
            "bootstrap", "w3", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "circle", "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "dt-checkboxes", "lightbox", "daterange", "my-styles"
        ));

        Assets::enqueue('js', array(
            "jquery", "jqueryui", "migrate", "popper", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "dt-checkboxes", "bs-wizard", "validate", "lightbox", "moment", "daterange",
            "paginate", "fileinput", "generatepw", "isotope", "dt-base", "scroll-to", "asimple-tour", "custom",
            "plugin", "countdown", "retina"
        ), true );
    }

    static public function wallet()
    {
        Assets::enqueue('css', array(
            "bootstrap", "style", "custom", "responsive", "rainbow", "progress", "sr-style", "sr-plugin", "bxslider", 
            "fa", "icons", "fileinput", "dt-bootstrap", "dt-buttons", "dt-fixedheader", "dt-responsive-b", 
            "dt-scroller", "lightbox", "daterange", "my-styles"
        ));

        Assets::enqueue('js', array(
            "jquery", "migrate", "bootstrap",
            "rainbow", "jq-pie", "sr-plugin", "sliderrev", "bxslider", "maps", "jqmaps", "scroll", "hoverizr", 
            "placeholder", "datatables", "dt-bootstrap", "dt-fixedheader", "dt-keytable", "dt-responsive", 
            "dt-responsive-b", "dt-scroller", "bs-wizard", "validate", "lightbox", "mask", "moment", "daterange",
            "fileinput", "dt-base", "custom", "wallet", "plugin", "countdown", "retina"
        ), true );
    }
}

Asset::init();
?>