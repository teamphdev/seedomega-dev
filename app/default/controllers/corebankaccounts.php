<?php
class CoreBankAccounts extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::common();
    }

    public function index()
    {
        $userinfo = User::info();
        if( User::can( 'Administer All' ) || User::can( 'Manage Core Bank Accounts' ) ){
            $bankaccounts = $this->model->getAllBankAccounts();
            View::page( 'core_bankaccounts/list', get_defined_vars() );
        } else {
            View::redirect( '' );
        }
    }
    
    public function edit()
    {
        $userinfo = User::info();
        if( User::can( 'Administer All' ) || User::can( 'Manage Core Bank Accounts' ) ){
            $this->model->doSave();
            $bankdata = $this->model->getBankAccountID( $this->segment[2] );
            View::page( 'core_bankaccounts/edit', get_defined_vars() );
        } else {
            View::redirect( 'corebankaccounts' );
        }
    }

    public function add()
    {
        $userinfo = User::info();
        if( User::can( 'Administer All' ) || User::can( 'Manage Core Bank Accounts' ) ){
            $this->model->doSave();
            View::page( 'core_bankaccounts/add', get_defined_vars() );
        } else {
            View::redirect( 'blogs/lists' );
        }
    }
        
    public function delete()
    {
        $userinfo = User::info();
        if( User::can( 'Administer All' ) || User::can( 'Delete Core Bank Accounts' ) ){
            $data = $this->model->doDelete( $this->segment[2] );
            View::redirect( 'corebankaccounts' );
        } else {
            View::redirect( 'corebankaccounts' );
        }
    }
}