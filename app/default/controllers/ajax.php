<?php
class Ajax extends Controller 
{
    public function __construct()
    {
        parent::__construct();

        // Load helper class
        $this->load->helper('apputility');
        $this->load->helper('auth');  
    }

    public function index()
    {   
        // Protected yes        
        if(!Auth::isLoggedIn() && $this->segment[0] != 'assets') {
            //View::redirect('users/login');
            echo '{}';
        }
    }
    
    public function userInfo()
    {
        $users = $this->load->model('users',true, true);
        echo $users->getUserByCode($this->post['ID']);
    }

    public function getuserInfo()
    {
        $users = $this->load->model('users',true, true);
        echo $users->getUserBySearch($this->post['ID']);
    }

    // public function userInfo()
    // {
    //     $users = $this->load->model('users',true, true);
    //     $search = $this->load->model('search',true, true);
    //     $data = $users->getUserByCode($this->post['ID']);
    //     $data2 = json_decode($data);

    //     $comp = $search->getUserParents($this->post['UserID']);
    //     $data2->CompanyName = $comp[0]->CompanyName;

    //     echo json_encode($data2);
    // }
    
    public function checkemail()
    {
        $id = User::infoByEmail('UserID',$this->post['email']);
        echo ($id) ? true : false;
    }

    public function checkemail2()
    {
        $users = $this->load->model('users',true, true);
        
        $validate = $users->validateUniqueEmail($this->segment[2], $this->segment[3]);

        echo count($validate) ? true : false;
    }
    
    public function checkclientemail()
    {
        $users = $this->load->model('users',true, true);
        
        $info = $users->getUserByClientEmail($this->post['email']);

        echo count($info) ? true : false;
    }
    
    public function changelanguage()
    {
        $users = $this->load->model('users', true, true);
        $this->model->updateLanguage($this->post['ID']);
        
        $users->updateUserInfo();
        Lang::init();
    }

    public function filesUpload()
    {
        $filedata = $this->fileUpload($this->file, User::info('UserID'));
        $ret = 0;
        foreach ($filedata as $key => $value) {
            $ret = $value;
        }
        echo $ret;
    }

    public function sendChatMessage() 
    { 
        $userinfo = User::info();

        if(User::is('Administrator')) {
            $agencyroom = isset($this->segment[2]) ? $this->segment[2] : 'admin';
            $file = "./app/ga/chat/".$agencyroom.".html";
            if(!file_exists($file)) {
               $handle = fopen($file, "w"); 
            }
            
        } elseif(User::is('Agency')) {
            $file = "./app/ga/chat/message_".$userinfo->UserID.".html";
            if(!file_exists($file)) {
               $handle = fopen($file, "w"); 
            }
        } else {
            $agencyID = AppUtility::getAgencyOf($userinfo->UserID);
            $file = "./app/ga/chat/message_".$agencyID.".html";

            if(!file_exists($file)) {
               $handle = fopen($file, "w"); 
            }
        }
        
        $avatar = View::common()->getUploadedFiles($userinfo->Avatar);
        if(Auth::isLoggedIn()) {
            $text = $this->post['text'];//View::url()
            $fp = fopen($file, 'a');
            fwrite($fp, '<li class="clearfix"><div class="chat-avatar"><img src="'.(isset($avatar[0]) ? './app/ga/views/assets/files'.$avatar[0]->FileSlug : './app/ga/views/assets/images/user.png').'" alt="Female" /><i>'.date("g:i A").'</i></div><div class="conversation-text"><div class="ctext-wrap"><i>'.$userinfo->FirstName.' '.$userinfo->LastName.'</i><p>'.stripslashes(htmlspecialchars($text)).'</p></div></div></li>');
            fclose($fp);
        }
    }
    
    public function getChatMessages() 
    { 
        $userinfo = User::info();

        if(User::is('Administrator')) {
            $agencyroom = isset($this->segment[2]) ? $this->segment[2] : 'admin';
            $file = "./app/ga/chat/".$agencyroom.".html";
            if(!file_exists($file)) {
               $handle = fopen($file, "w"); 
            }
            
        } elseif(User::is('Agency')) {
            $file = "./app/ga/chat/message_".$userinfo->UserID.".html";
            if(!file_exists($file)) {
               $handle = fopen($file, "w"); 
            }
        } else {
            $agencyID = AppUtility::getAgencyOf($userinfo->UserID);
            $file = "./app/ga/chat/message_".$agencyID.".html";

            if(!file_exists($file)) {
               $handle = fopen($file, "w"); 
            }
        }

        //$file = "./app/ga/chat/messages.html";
        if(file_exists($file) && filesize($file) > 0){            
            $handle = fopen($file, "r");
            $contents = fread($handle, filesize($file));
            fclose($handle);
            echo $contents;
        }
    }

    public function updateSoaTable() 
    { 
        $transactions = file_get_contents('https://ga.weetec.com/api.asmx/GetTransactionInformation?Key=7d74328f-1c21-4aeb-bff6-bd0513c8ca2b&ClientID=&PolicyNumber=&FromDate=&ToDate=');
        $transaction = json_decode($transactions);

        // echo '<pre>'; print_r($transaction);
        if(is_array($transaction->Transactions) && count($transaction->Transactions)) {
            $this->model->clearSoaTable();
            foreach($transaction->Transactions as $client) {
                $data = array();

                $data['UserID'] = $client->ClientID;
                $data['AccountID'] = $client->PolicyNumber;
                $data['TransactionDate'] = date('Y-m-d H:i:s',strtotime($client->TransactionDate));
                $data['TransactionType'] = $client->TransactionType;
                $data['Point'] = $client->Point;

                $this->model->updateSoa($data);
            }
        }
    }

    public function updateSoa() 
    { 
        $clients = $this->model->getClientIDs();
        foreach($clients as $client) {
            $soadata = file_get_contents('https://ga.weetec.com/api.asmx/CheckClientIDInformation?key=7d74328f-1c21-4aeb-bff6-bd0513c8ca2b&ClientID='.$client->UserID);
            $soa = json_decode($soadata);

            if(is_array($soa)) {
                $sdata = array();
                $sdata['CashAmount'] = isset($soa->CashAmount) ? $soa->CashAmount : 0;            
                $sdata['LastWithdrawalDate'] = isset($soa->LastWithdrawalDate) ? date('Y-m-d H:i:s',strtotime($soa->LastWithdrawalDate)) : '';
                $sdata['LastWithdrawalAmount'] = isset($soa->LastWithdrawalAmount) ? $soa->LastWithdrawalAmount : 0;

                $this->model->updateSoainfo($client->UserID,$sdata);
            }
        }
    }
}