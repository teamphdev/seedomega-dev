<?php
class Contactus extends Controller 
{
    public function __construct()
    {
        parent::__construct();            
        
        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        // Auth::userSession(); // Continue if user has session
        Asset::common();
    }

    public function index()
    {
        $currvw = 'contactus/form';
        $inquiryID = $this->model->doSave();
        $viewkey = isset( $this->segment[2] ) ? $this->segment[2] : false;
        if( $viewkey ){
            $inquiry = $this->model->getInquiryByViewkey( $viewkey );
            $currvw = isset( $inquiry ) ? 'contactus/view' : $currvw;
        }
        View::page( $currvw, get_defined_vars() );

        // echo $inquiryID;
        if( $inquiryID ){
            $inqrydata = $this->model->getInquiryByID( $inquiryID );
            //print_r($inqrydata);

            // Start Email sending
            $email = $this->load->model( 'emailengine', true );
            $email->sendNewInquiry( $inquiryID, $inqrydata );
        }
    }

    public function lists()
    {
        $userinfo = User::info();
        if( User::can( 'Manage Inquiries' ) ){
            $inquiries = $this->model->getAllInquiries();
            View::page( 'contactus/list', get_defined_vars() );
        } else {
            View::redirect( 'blogs/lists' );
        }
    }
    
    public function edit()
    {
        $userinfo = User::info();
        if( User::can( 'Manage Inquiries' ) ){
            $inquiryID = $this->model->doSave();
            $inquiry = $this->model->getInquiryByID( $this->segment[2] );
            if( $inquiryID ){
                // Start Email sending
                $email = $this->load->model( 'emailengine', true );
                $email->sendReplyInquiry( $inquiry );
            }
            View::page( 'contactus/edit', get_defined_vars() );
        } else {
            View::redirect( 'blogs/lists' );
        }
    }
        
    public function delete()
    {
        $userinfo = User::info();
        if( User::can( 'Administer All' ) ){
            $data = $this->model->doDelete( $this->segment[2] );
            View::redirect( 'contactus/list' );
        } else {
            View::redirect( 'blogs/lists' );
        }
    }
}