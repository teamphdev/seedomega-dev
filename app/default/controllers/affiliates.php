<?php
class Affiliates extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model( 'users', false, true );

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::affiliates();
    }

    /**
     * Admin clients page
     *
     * @access private
     * @return na
     */
    public function index()
    {
        $userinfo = User::info();
        if( User::can( 'Manage Affiliates' ) ){
            View::redirect( 'affiliates/dashboard' );
        } else {
            View::redirect( 'main/frontpage' );
        }
    }

    /**
     * Admin clients page
     *
     * @access private
     * @return na
     */
    public function dashboard()
    {
        $userinfo = User::info();
        if( User::can( 'Manage Affiliates' ) ){
            $this->model->doSave();
            
            $status = AppUtility::getStatusCSS();
            $affiliates = $this->model->getAffiliates();
            $levels = array( 'Client', 'User' );

            // if( isset( $affiliates ) && count ( $affiliates ) ){
            //     foreach( $status as $ks => $vs ){
            //         foreach( $affiliates[$ks] as $k => $v ){
            //             if( $v->Name == 'User' ) { $v->Name = 'Seeder'; }
            //             $v->Data = array( 'LClass' => $vs[0], 'IClass' => $vs[1], 'StatusHTML' => '', 'Avatar' => '', 'AvatarLink' => '' );
            //             //$v->Data['Avatar'] = View::common()->getUploadedFiles( $v->Avatar );
            //             $v->Data['AvatarLink'] = '<img src="'.View::asset( isset( $v->FileSlug ) ? 'files'.$v->FileSlug : 'images/user.png' ).'" style="border-radius:50%; height:30px; width:30px;" />';
            //             $v->Data['StatusHTML'] = '<span class="label label-'.$vs[0].'"><i class="'.$vs[1].'"></i>'.$v->Status.'</span>';
            //         };
            //     }
            // }

            if( isset( $affiliates ) && count ( $affiliates ) ){
                foreach( $levels as $level ){
                    foreach( $affiliates[$level] as $k => $v ){
                        if( $v->Name == 'User' ) { $v->Name = 'Seeder'; }
                        $v->Data = array( 'LClass' => '', 'IClass' => '', 'StatusHTML' => '', 'Avatar' => '', 'AvatarLink' => '' );
                        $v->Data['AvatarLink'] = '<img class="img-avatar" src="'.View::asset( isset( $v->FileSlug ) ? 'files'.$v->FileSlug : 'images/user.png' ).'" style="height:30px;width:30px;" />';
                    }
                }
            }
            $clientcount = isset( $affiliates['Client'] ) && count( $affiliates['Client'] ) ? count( $affiliates['Client'] ) : 0;
            $seederscount = isset( $affiliates['User'] ) && count( $affiliates['User'] ) ? count( $affiliates['User'] ) : 0;

            // echo '<pre>';
            // print_r( $affiliates );
            // echo '</pre>';

            // $pendingCount = isset( $affiliates ) && count( $affiliates ) ? count( $affiliates['Pending'] ) : 0;
            // $approvedCount = isset( $affiliates ) && count( $affiliates ) ? count( $affiliates['Approved'] ) : 0;
            // $incompleteCount = isset( $affiliates ) && count( $affiliates ) ? count( $affiliates['Incomplete'] ) : 0;
            // $verificationCount = isset( $affiliates ) && count( $affiliates ) ? count( $affiliates['Verification'] ) : 0;
            // $allCount = $pendingCount + $approvedCount + $incompleteCount + $verificationCount;

            $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'dashboard' );

            View::page( 'affiliates/index', get_defined_vars() );

        } else { View::redirect(); }
    }
}