<?php
class Support extends Controller 
{
    public function __construct()
    {
        parent::__construct();     
        
        $this->load->model('users', false, true);

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::support();
    }

    public function index()
    {
        View::redirect('support/dashboard');
    }

    /**
     * Support index page for ticket list
     *
     * @access private
     * @return na
     */
    public function dashboard()
    {
        $segment2 = isset($this->segment[2]) ? $this->segment[2] : '';

        switch ($segment2){
            case 'urgent':
                $tickets = $this->model->getTickets('', 'URGENT');

                $closedTickets = $this->model->getTickets('CLOSED', '');
                $opentickets = $this->model->getTickets('OPEN', '');
                $closedTicketsCount = count($closedTickets);

                $urgentTicketsCount = count($tickets);
                $openTicketsCount = count($opentickets);
                $totalTicketsCount = $closedTicketsCount + $openTicketsCount;
            break;

            case 'open':
                if( User::is( 'Administrator' ) || User::is( 'Customer Service' )) {
                    $tickets = $this->model->getTickets('OPEN', '');
                    $closedTickets = $this->model->getTickets('CLOSED', '');
                } else {
                    $userId = User::info('UserID');

                    $tickets = $this->model->getClientTickets('OPEN',$userId);
                    $closedTickets = $this->model->getClientTickets('CLOSED',$userId);
                }
                $urgentTickets = $this->model->getTickets('', 'URGENT');
                $urgentTicketsCount = count($urgentTickets);
                $closedTicketsCount = count($closedTickets);
                $openTicketsCount = count($tickets);
                $totalTicketsCount = $closedTicketsCount + $openTicketsCount;
                break;

            case 'closed':
                if( User::is( 'Administrator' ) || User::is( 'Customer Service' )) {
                    $tickets = $this->model->getTickets('CLOSED', '');
                    $closedTickets = $this->model->getTickets('CLOSED', '');

                    $opentickets = $this->model->getTickets('OPEN', '');
                    $urgentTickets = $this->model->getTickets('', 'URGENT');
                    $urgentTicketsCount = count($urgentTickets);
                } else {
                    $userId = User::info('UserID');

                    $tickets = $this->model->getClientTickets('CLOSED',$userId);
                    $closedTickets = $this->model->getClientTickets('CLOSED',$userId);
                    $opentickets = $this->model->getClientTickets('OPEN',$userId);
                }
                $closedTicketsCount = count($closedTickets);
                $openTicketsCount = count($opentickets);
                $totalTicketsCount = $closedTicketsCount + $openTicketsCount;
                break;

            default:

                if( User::is( 'Administrator' ) || User::is( 'Customer Service' )) {
                    //admin or CS view
                    $tickets = $this->model->getTickets();
                    $closedTickets = $this->model->getTickets('CLOSED', '');
                    $opentickets = $this->model->getTickets('OPEN', '');

                    $urgentTickets = $this->model->getTickets('', 'URGENT');
                    $urgentTicketsCount = count($urgentTickets);
                } else {
                    //client view
                    $userId = User::info('UserID');

                    $tickets = $this->model->getClientTickets('OPEN',$userId);
                    $opentickets = $tickets;
                    $closedTickets = $this->model->getClientTickets('CLOSED',$userId);
                }

                $closedTicketsCount = count($closedTickets);
                $openTicketsCount = count($opentickets);
                $totalTicketsCount = $closedTicketsCount + $openTicketsCount;
                break;
        }

        $totalResponses = $this->model->getTotalResponses();
        View::page( 'support/dashboard', get_defined_vars() );
    }

    /**
     * Save a reply
     *
     * @access private
     * @return na
     */
    public function savereply()
    {
        $this->model->doSave();
    }

    /**
     * Viewing ticket
     *
     * @access private
     * @return na
     */
    public function view()
    {
        $this->model->doSave();
        $ticket_id= isset($this->segment[2]) ? $this->segment[2] : '';
        $ticket = $this->model->getTicketDetails($ticket_id);
        $replies = $this->model->getTicketReplies($ticket_id);
        $level = User::info('UserLevel');

        if( User::is( 'Administrator' ) || User::is( 'Customer Service' ) || $ticket->UserId == User::info('UserID') ) {
            View::page( 'support/view_ticket', get_defined_vars() );
        }else{
            View::redirect('support');
        }

        //View::page( 'support/view', get_defined_vars() );
    }

    /**
     * Get ticket details via AJAX call
     *
     * @access private
     * @return na
     */
    public function getticket()
    {
        $ticket_id= isset($this->segment[2]) ? $this->segment[2] : '';
        $ticket = $this->model->getTicketDetails($ticket_id);
        $replies = $this->model->getTicketReplies($ticket_id);
        $level = User::info('UserLevel');

        View::page( 'support/view_modalresult', get_defined_vars() );
    }

    /**
     * Create new ticket for Client or Investor/User
     *
     * @access private
     * @return na
     */
    public function create()
    {
        $level = User::info('UserLevel');

        if( $level == 'Administrator' || $level == 'Customer Service' ||  $level == 'Client' || $level == 'User' ) {

            $this->model->doSave();

            $userinfo = User::info();

            View::page('support/create', get_defined_vars());

        } else {
            View::redirect('/users/dashboard');
        }
    }

    public function mark()
    {
        $ticket_id = isset($this->segment[2]) ? $this->segment[2] : '';
        $action = isset($this->segment[3]) ? $this->segment[3] : '';
        $this->model->markTicket($ticket_id, $action);

        View::redirect('support/dashboard');
    }
}