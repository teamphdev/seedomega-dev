<?php
class Forums extends Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::forum();
    }

    /**
     * Get user list page
     * 
     * @access private | Admin
     * @return rows
     */
    public function index()
    {
        $users = $this->model->getSeeders();
        $userinfo = User::info();

        if( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ){
            View::redirect( 'forums/dashboard' );

        } else {
            $clients = $this->model->getClientDataByUser( $userinfo->UserID );
            if( isset( $clients ) ){
                if( count( $clients ) ){
                    foreach( $clients as $client ){
                        $client->Data['AvatarLink'] = '<img src="'.View::asset( isset( $client->AvatarSlug ) ? 'files'.$client->AvatarSlug : 'images/user.png' ).'">';
                        $client->Data['CompanyPhotoLink'] = '<img src="'.View::asset( isset( $client->PhotoSlug ) ? 'files'.$client->PhotoSlug : 'images/funder_1.jpg' ).'">';
                        if( isset( $client->LastPost ) ){
                            $lp = explode( '|', $client->LastPost );
                            $client->Data['PostedBy'] = isset($lp[0]) ? $lp[0] : '';
                            $client->Data['DatePosted'] = isset($lp[1]) ? $lp[1] : '';
                        }
                    }
                }
            }

            View::page( 'forums/grid_list', get_defined_vars() );
        }
    }

    /**
     * Get user list page
     * 
     * @access private | Admin
     * @return rows
     */
    public function dashboard()
    {
        $userinfo = USer::info();

        if( isset( $this->segment[2] ) ){
            $client = $this->model->getClientDataByClientID( $this->segment[2] );
            $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );
        }
        $client = AppUtility::validateForumAccess();
        $forums = $this->model->getForumSummary( $client->ClientProfileID );
        $title = isset( $client->CompanyName ) ? $client->CompanyName : '';
        if( isset( $forums ) && count( $forums ) ){
            foreach( $forums as $k => $v ){
                foreach( $v as $post ){
                    if( !isset( $post->SubCatNameSlug ) || $post->SubCatNameSlug == '' ) { $post->SubCatNameSlug = str_ireplace( " ", "-", strtolower( $post->SubCatName ) ); }
                    if( isset( $post->LastPost ) ){
                        $lp = explode( '|', $post->LastPost );
                        $post->Data = array( 'PostedBy' => $lp[0], 'DatePosted' => $lp[1], 'FileSlug' => $lp[2], 'AvatarLink' => '' );
                        $post->Data['AvatarLink'] = '<img src="'.View::asset( isset( $post->Data['FileSlug'] ) ? 'files'.$post->Data['FileSlug'] : 'images/slide-3.jpg' ).'">';
                    }
                }
            }
        }

        $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );
        View::page( 'forums/dashboard', get_defined_vars() );
    }

    /**
     * Get forum dashboard page
     * 
     * @access private
     * @return contents
     */
    public function category()
    {
        $this->setSession( 'lister', array( 'forums', 'category' ) );
        $usersModel = $this->load->model( 'users', true, true );

        $userinfo = User::info();
        $client = AppUtility::validateForumAccess();
        $title = isset( $client->CompanyName ) ? $client->CompanyName : '';

        $forum = array();
        $forum['subcatid'] = isset( $this->segment[2] ) ? $this->segment[2] : 0;
        $forum['scname'] = isset( $this->segment[3] ) ? $this->segment[3] : '';
        $forum['forumid'] = isset( $this->segment[4] ) ? $this->segment[4] : false;
        $forum['pinval'] = isset( $this->segment[5] ) ? $this->segment[5] : false;

        if( $forum['forumid'] && $forum['pinval'] ){
            $this->model->doSavePin( $forum );
        }

        $forum['scname'] = ucwords( str_ireplace( '-', ' ', $forum['scname'] ) );
        $categories = $this->model->getForumByCategory( false, $forum['subcatid'] );
        if( isset( $categories ) && count( $categories ) ){
            foreach( $categories as $k => $v ){
                foreach( $v as $category ){
                    $category->AuthorData['AvatarLink'] = '<img src="'.View::asset( isset( $category->AuthorFileSlug ) ? 'files'.$category->AuthorFileSlug : 'images/slide-3.jpg' ).'">';
                    $category->PosterData['AvatarLink'] = '<img src="'.View::asset( isset( $category->LastPosterFileSlug ) ? 'files'.$category->LastPosterFileSlug : 'images/slide-3.jpg' ).'">';
                }
            }
            if( isset( $categories[0][0]->SubCategory ) ){
                $forum['scname'] = $categories[0][0]->SubCategory;
            }
        }
        
        $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );
        View::page( 'forums/category', get_defined_vars() );
    }

    /**
     * Get topic page
     * 
     * @access private
     * @return contents
     */
    public function topic()
    {
        $this->setSession( 'lister', array( 'forums', 'category' ) );
        $usersModel = $this->load->model( 'users', true, true );
        $userinfo = User::info();
        $client = AppUtility::validateForumAccess();
        $title = isset( $client->CompanyName ) ? $client->CompanyName : '';

        $this->model->doSaveTopic();

        $forum = array();
        $forum['subcatid'] = isset( $this->segment[2] ) ? $this->segment[2] : 0;
        $forum['forumid'] = isset( $this->segment[3] ) ? $this->segment[3] : 0;
        $forum['title'] = isset( $this->segment[4] ) ? $this->segment[4] : '';
        $forum['title'] = ucwords( str_ireplace( '-', ' ', $forum['title'] ) );
        
        $username = isset( $userinfo->FirstName ) ? $userinfo->FirstName.' ' : '';
        $username .= isset( $userinfo->LastName ) ? $userinfo->LastName : '';

        $avatar = View::common()->getUploadedFiles( $userinfo->Avatar );
        $avatarlink = '<img class="img-avatar" src="'.View::asset( isset( $avatar[0]->FileSlug ) ? 'files'.$avatar[0]->FileSlug : 'images/slide-3.jpg' ).'">';

        $total = $this->model->getForumTotalPostOwn( $userinfo->UserID );
        $total = isset( $total->Total ) ? $total->Total.( $total->Total > 1 ? ' Posts' : ' Post' ) : '0 Post';

        $epigram = '';
        $topics = $this->model->getForumTopic( $forum['forumid'] );
        if( isset( $topics ) && count( $topics ) ){
            foreach( $topics as $topic ){
                if( isset( $topic->Title ) && $topic->Title != '' ){ $forum['title'] = $topic->Title; }
                $topic->Data = array( 'Name' => '', 'AvatarLink' => '' );
                $topic->Data['Name'] = isset( $topic->FirstName ) ? $topic->FirstName.' ' : '';
                $topic->Data['Name'] .= isset( $topic->LastName ) ? $topic->LastName : '';
                $topic->Data['AvatarLink'] = '<img class="img-avatar" src="'.View::asset( isset( $topic->FileSlug ) ? 'files'.$topic->FileSlug : 'images/slide-3.jpg' ).'">';
                $topic->TotalPost = isset( $topic->TotalPost ) ? $topic->TotalPost.( $topic->TotalPost > 1 ? ' Posts' : ' Post' ) : '0 Post';
                if( $userinfo->UserID == $topic->UserID ){ $epigram = $topic->Epigram; }
            }
        }
        $forum['scname'] = isset( $topics[0]->SubCatName ) ? $topics[0]->SubCatName : '';
        $forum['scslug'] = str_ireplace( ' ', '-', strtolower( $forum['scname'] ) );
        $forum['scslug'] = isset( $topics[0]->SubCatNameSlug )  && $topics[0]->SubCatNameSlug != '' ? $topics[0]->SubCatNameSlug : $forum['scslug'];
        //$this->model->doSaveTopicView( $forum['forumid'] );
            // echo '<pre>'; print_r( $topics ); echo '</pre>';
        $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );
        View::page('forums/topic', get_defined_vars());
    }

    /**
     * Get add topic page
     * 
     * @access private
     * @return contents
     */
    public function newtopic()
    {
        // if( !User::can( 'Add Topics' ) ){
        //     View::redirect('forums/dashboard');
        // }

        $this->setSession( 'lister', array( 'forums', 'category' ) );
        $usersModel = $this->load->model( 'users', true, true );
        $client = AppUtility::validateForumAccess();

        $this->model->doSaveTopic();
        $options = $this->model->getForumCategoryForOptions( $client->ClientProfileID );
        $title = isset( $client->CompanyName ) ? $client->CompanyName : '';

        $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );
        View::page( 'forums/newtopic', get_defined_vars());
    }

    /**
     * Forum list of topics
     *
     * @access private
     * @return na
     */
    public function topics()
    {
        if( !User::can( 'Manage Forums' ) ){
            View::redirect();
        }

        $userinfo = User::info();
        $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
        $client = $this->model->getClientInfo( $cuid );

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        $forumID = isset( $this->segment[3] ) ? $this->segment[3] : '0';

        $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );
        switch( $segment2 ){
            case 'add':
                // $this->model->doSaveTopic();
                View::page( 'forums/newtopic', get_defined_vars() );
                break;

            case 'edit':
                $this->model->doSaveTopic();
                $topic = $this->model->getForumTopics( $client->ClientProfileID, $forumID );
                $options = $this->model->getForumCategoryForOptions( $client->ClientProfileID );

                if( isset( $topic ) ){
                    View::page( 'forums/topics/topic_edit', get_defined_vars() );
                } else {
                    View::redirect( 'forums/topics' );
                }
                break;

            case 'delete':
                if( User::can( 'Delete Topics' ) ){
                    $this->model->doDeleteTopic( $forumID );
                }
                View::redirect( 'forums/topics' );
                break;

            default:
                $topics = $this->model->getForumTopics( $client->ClientProfileID );
                if( isset( $topics ) && count( $topics ) ){
                    foreach( $topics as $topic ){
                        $jdata[$topic->ForumID] = array( 'Title' => $topic->Title, 'Content' => $topic->Content );
                    }
                }
                $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );

                View::page( 'forums/topics/topic_list', get_defined_vars() );
                break;
        }
    }

    /**
     * Forum list of categories
     *
     * @access private
     * @return na
     */
    public function categories()
    {
        if( !User::can( 'Manage Forums' ) ){
            View::redirect();
        }

        $userinfo = User::info();

        $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
        $client = $this->model->getClientInfo( $cuid );

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        $catID = isset( $this->segment[3] ) ? $this->segment[3] : '0';

        switch( $segment2 ){
            case 'add':
                $this->model->doSaveCategory( $client->ClientProfileID );
                View::page( 'forums/categories/cat_add', get_defined_vars() );
                break;

            case 'edit':
                $this->model->doSaveCategory();
                $catdata = $this->model->getForumCategory( $client->ClientProfileID, $catID );
                if( $catdata ){
                    View::page( 'forums/categories/cat_edit', get_defined_vars() );
                } else {
                    View::redirect( 'forums/categories' );
                }                
                break;

            case 'delete':
                $category = $this->model->doDeleteCategory( 'forum_categories', 'CatID', $catID );
                View::redirect( 'forums/categories' );
                break;

            default:
                $categories = $this->model->getForumCategory( $client->ClientProfileID );
                View::page( 'forums/categories/cat_list', get_defined_vars() );
                break;
        }
    }

    /**
     * Forum list of sub-categories
     *
     * @access private
     * @return na
     */
    public function subcategories()
    {
        if( !User::can( 'Manage Forums' ) ){
            View::redirect();
        }

        $userinfo = User::info();

        $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
        $client = $this->model->getClientInfo( $cuid );
        $iclasses = [];
        $icons = AppUtility::getIconList();
        if( isset( $icons ) && count( $icons ) ){
            foreach( $icons as $icon ){
                $suf = explode( '-', $icon->Description );
                $full = $suf[0].' '.$icon->Description;
                $iclasses[$full] = $full;
                // $iclasses[$full] = '<i class="'.$full.' fa-2x"></i> '.$full;
            }
        }

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';

        switch( $segment2 ){
            case 'add':
                $this->model->doSaveSubCategory();
                $catid = isset( $this->segment[3] ) ? $this->segment[3] : 0;
                $catname = isset( $this->segment[4] ) ? $this->segment[4] : '';
                $options = $this->model->getForumCategoryOnlyForOptions( $client->ClientProfileID );

                View::page( 'forums/subcategories/subcat_add', get_defined_vars() );
                break;

            case 'edit':
                $this->model->doSaveSubCategory();
                $subcatid = isset( $this->segment[3] ) ? $this->segment[3] : 0;
                $catname = isset( $this->segment[4] ) ? $this->segment[4] : '';

                $catdata = $this->model->getForumSubCategory( $subcatid );
                if( $catdata ){
                    View::page( 'forums/subcategories/subcat_edit', get_defined_vars() );
                } else {
                    View::redirect( 'forums/subcategories' );
                }
                break;

            case 'delete':
                $category = $this->model->doDeleteCategory( 'forum_sub_categories', 'SubCatID', $this->segment[3] );

                View::redirect( 'forums/subcategories' );
                break;

            default:
                $categories = $this->model->getForumCategory( $client->ClientProfileID );
                $subcategories = $this->model->getForumSubCategory();

                View::page( 'forums/subcategories/subcat_list', get_defined_vars() );
                break;
        }
    }

    /**
     * Trash the current segment user id
     * 
     * @access private
     * @return na
     */
    public function trash()
    {
        $user = $this->model->doTrash($this->segment[2]);
        View::redirect('seeders/list');
    }

    /**
     * Restore trashed user
     * 
     * @access private
     * @return na
     */
    public function restore()
    {
        $user = $this->model->doRestore($this->segment[2]);
        View::redirect('seeders/list');
    }

    /**
     * Delete permanently the current segment user id
     * 
     * @access private
     * @return na
     */
    public function delete()
    {
        $user = $this->model->doDelete($this->segment[2]);
        View::redirect('seeders/trashbin/');
    }

    /**
     * Trash a user
     * 
     * @access private
     * @return na
     */
    public function trashbin()
    {
        if( User::can( 'Delete Seeders' ) ){

            if( isset( $this->post['uids'] ) ){
                $users = $this->model->doEmptyTrash( $this->post['uids'] );
            }

            $users = $this->model->getTrashedSeeders();
            View::page('seeders/trashbin', get_defined_vars());
        }
    }

    /**
     * Clear trashbin
     * 
     * @access private
     * @return na
     */
    public function emptytrashbin()
    {
        $user = $this->model->doEmptyTrash();
        View::redirect('seeders/trashbin/');
    }

    /**
     * Add a user
     * 
     * @access private
     * @return na
     */
    public function add()
    {
        $userID = $this->model->doSave();

        $levels = $this->model->getSeederLevels();
        View::page('seeders/add', get_defined_vars());

        if($userID) {
            $userInfo = $this->model->getSeeder($userID);

            // Start Email sending
            $email = $this->load->model('emailengine',true);
            $email->sendSeederRegistration($userID,$userInfo);

            View::redirect('seeders');
        }
    }

    /**
     * Edit/Update user
     * 
     * @access private
     * @return na
     */
    public function edit()
    {
        $rolesModel = $this->load->model('roles', true, true);
        $usersModel = $this->load->model('users', true, true);

        $this->model->doSave();
        if(isset($this->segment[2])) {
            $user = $this->model->getSeeder($this->segment[2]);
            $levels = $usersModel->getUserLevels();
            $roles = $rolesModel->getLevels();
            $loadedcapa = isset($this->segment[3]) ? Level::capabilities($this->segment[3]) : false;
            $capabilities =  $usersModel->getCapabilities();
            View::page('seeders/edit', get_defined_vars());
        } else {
            View::redirect('seeders/login');
        }
    }

    /**
     * Load footer script for user role
     * 
     * @access private
     * @return na
     */
    public function userRole()
    {
        if( User::isLoggedIn() ){
            View::$footerscripts = array('vendor/jquery/dist/jquery.min.js','vendor/bootstrap/dist/js/bootstrap.min.js','vendor/fastclick/lib/fastclick.js','vendor/nprogress/nprogress.js','vendor/raphael/raphael.min.js','vendor/morris.js/morris.min.js','vendor/bootstrap-progressbar/bootstrap-progressbar.min.js','assets/js/moment/moment.min.js','assets/js/datepicker/daterangepicker.js','assets/js/custom.js');
            View::page( 'user/userrole', get_defined_vars() );
        } else {
            View::redirect();
        }
    }

    /**
     * Save topic view
     * 
     * @access public
     * @return na
     */
    public function saveTopicView()
    {
        $forumid = isset( $_POST['forumid'] ) ? $_POST['forumid'] : 0;
        $this->model->doSaveTopicView( $forumid );
    }

    /**
     * Get sub-categories by category id
     * 
     * @access Public
     * @return json
     */
    function getsubcategories()
    {
        $catid = $this->segment[2];
        $subcategories = $this->model->getForumSubCategoryForOptions( $catid );
        header( "Content-type:application/json" );
        echo json_encode( $subcategories );
    }
}