<?php
class Packages extends Controller
{
    public function __construct()
    {
        parent::__construct();
        // Load model Auth
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        if( User::is( 'Administrator' ) || User::can( 'Manage Invoices' ) ){} else View::redirect();
        Asset::common();
    }

    public function index()
    {   
        $userinfo = User::info();

        $packages = $this->model->getPackages();
        View::page('packages/list', get_defined_vars());
    }
   
    public function delete()
    {
        $userinfo = User::info();

        $user = $this->model->doDelete($this->segment[2]);
        View::redirect('packages');
    }
    
    public function edit()
    {
        $userinfo = User::info();

        $this->model->doSave();
        $package = $this->model->getPackage($this->segment[2]);
        
        View::page('packages/edit', get_defined_vars());
    }    

    public function add()
    {
        $userinfo = User::info();
        
        $pdata = $this->model->doSave();
        View::page('packages/add', get_defined_vars());
    }
}