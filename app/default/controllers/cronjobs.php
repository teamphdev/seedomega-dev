<?php
class CronJobs extends Controller
{
    public function __construct()
    {
        parent::__construct();     
        
        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
    }

     /**
     * Admin clients page
     * 
     * @access private
     * @return na
     */
    public function updateBooking()
    {          
        echo $this->model->deleteExpiredBookings();
    }
}