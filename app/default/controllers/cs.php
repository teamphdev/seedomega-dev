<?php
class Cs extends Controller
{
    public function __construct()
    {
        parent::__construct();     
        
        $this->load->model( $this->controller, false, true );

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::hubcare();
    }

    /**
     * Admin clients page
     * 
     * @access private
     * @return na
     */
    public function index()
    {
        View::referrer();
        $userinfo = User::info();
        if( User::can( 'Administer All' ) || User::can( 'Manage Site' ) || $userinfo->Code == "CSR" ){
        } else View::redirect();

        $allPendings = $this->model->getAllPendingsCount();

        View::page( 'cs/dashboard', get_defined_vars() );
    }

    /**
     * Client profiles list
     *
     * @access private
     * @return na
     */
    public function clientprofiles()
    {
        View::referrer();

        if( User::can( "Manage Profiles" ) ){
            $userinfo = User::info();
            $this->setSession( 'lister', array( $this->controller, $this->method, 'client profiles' ) );
            
            $segment2 = ( isset( $this->segment[2] ) ) ? $this->segment[2] : '';
            if( $segment2 == 'view' ){
                $ID = isset( $this->segment[3] ) ? $this->segment[3] : false;
                if ( !$ID ){
                    View::redirect( 'cs/clientprofiles' );
                }
                $clientdata = $this->model->getClientProfileData( $ID );

                if( User::can( 'Manage Profiles' ) ){
                    $this->model->doSave( $userinfo->UserID, $clientdata );
                }

                $clientdata = $this->model->getClientProfileData( $ID );
                
                $comments = $this->model->getComments( $clientdata->ClientProfileID );
                if( isset( $comments ) && count( $comments ) ){
                    foreach( $comments as $comment ){
                        $comment->AvatarLink = '<img src="'.View::asset( isset( $comment->FileSlug ) ? 'files'.$comment->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';
                    }
                }

                //$this->setSession( 'redirect', 'cs/clientprofiles/view' );
                View::page( 'cs/clientprofiles/view', get_defined_vars() );

            } elseif ( $segment2 == 'approve' ){
                $ID = ( isset( $this->segment[3] ) ) ? $this->segment[3] : '';
                $clientdata = $this->model->getClientProfileData( $ID );
                $result = $this->model->updateClientAsApprove( $ID, $clientdata );

                //$this->setSession( 'redirect', 'cs/clientprofiles' );
                View::redirect( 'cs/clientprofiles' );

            } else {
                View::referrer();
                $allClients = $this->model->getAllClients();
                $pendingClients = $this->model->getPendingClients();
                $pendingClientRequests = $this->model->getClientChangeRequests( 'client' );

                // $tempClientRequests = $this->model->getPendingClients( true );
                // if( isset( $tempClientRequests ) && count( $tempClientRequests ) ){
                //     foreach( $tempClientRequests as $key => $requests ){
                //         $requests->ChangeRequest = json_decode( $requests->ChangeRequest, true );
                //         if( isset( $requests->ChangeRequest['client'] ) ){
                //             array_push( $pendingClientRequests, $tempClientRequests[$key] );
                //         }
                //     }
                // }
                
                $allClientsCount = isset( $allClients ) ? count( $allClients ) : 0;
                $pendingClientsCount = isset( $pendingClients ) ? count( $pendingClients ) : 0;
                $pendingClientRequestsCount = isset( $pendingClientRequests ) ? count( $pendingClientRequests ) : 0;
                $approvedClientsCount = AppUtility::countConditional( $allClients, 'Status', '==', 'Approved' );

                //$this->setSession( 'redirect', 'cs/clientprofiles' );
                View::page( 'cs/clientprofiles', get_defined_vars() );
            }
            
        } else { View::redirect( 'main/frontpage' ); }
    }

    /**
     * Investor Profiles List
     * 
     * @access private
     * @return na
     */
    public function investorprofiles()
    {
        View::referrer();
        $userinfo = User::info();
        if( User::can( 'Administer All' ) || User::can( 'Manage Site' ) || $userinfo->Code == "CSR" ){
        } else View::redirect();

        $this->setSession( 'lister', array( $this->controller, $this->method, 'investor profiles' ) );
        $usersModel = $this->load->model( 'users', true, true );

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        if( $segment2 == 'view' ){
            $ID = isset( $this->segment[3] ) ? $this->segment[3] : '';
            if( !$ID ){ View::redirect( 'cs/investorprofiles' ); }

            $profiles = $usersModel->getProfile( $ID, true );
            $investordata = $this->model->getInverstorProfileData( $ID );
            $investorFiles = '';
            $ctr = 0;
            if( isset( $profiles ) && count( $profiles ) ){
                foreach( $profiles as $profile ){
                    $fgID = isset( $profile->FileGroupID ) ? $profile->FileGroupID : '0';
                    $upload = $profile->FileID == NULL || strlen( $profile->FileID ) == 0 ? true : false;
                    if( !$upload ){
                        $investorFiles .= AppUtility::getCustomFileList( $fgID.'-SeederProfileDocs'.$ctr, $profile, $upload );
                    }
                    $ctr++;
                }
            }

            $avatar = isset( $investordata->Avatar ) ? View::common()->getUploadedFiles( $investordata->Avatar ) : false;
            $IDPhotoStatus = isset( $investordata->IDPhotoStatus ) ? $investordata->IDPhotoStatus : 0;
            $AddressPhotoStatus = isset( $investordata->AddressPhotoStatus ) ? $investordata->AddressPhotoStatus : 0;
            
            $iData = array( 'UserID' => '', 'Status' => '', 'Disabled' => '', 'AvatarLink' => '' );
            $iData['UserID'] = isset( $investordata->UserID ) ? $investordata->UserID : '';
            $iData['Status'] = Apputility::investorinfo( 'InvestorStatus', $iData['UserID'] );
            $iData['Status'] = isset( $iData['Status'] ) ? $iData['Status'] : '';
            $iData['Disabled'] = ( $IDPhotoStatus == 1 && $AddressPhotoStatus == 1 ) ? '' : ' disabled';

            $this->setSession( 'redirect', 'cs/investorprofiles/view' );
            View::page( 'cs/investorprofiles/view', get_defined_vars() );

        } elseif( $segment2 =='approve' ){
            $ID = isset( $this->segment[3] ) ? $this->segment[3] : '';
            $investordata = $this->model->getInverstorProfileData( $ID );
            if( $investordata->IDPhotoStatus == '1' && $investordata->AddressPhotoStatus == '1' ){
                $result = $this->model->updateInvestorAsApprove( $ID, $investordata );
            }

            $this->setSession( 'redirect', 'cs/investorprofiles' );
            View::redirect('cs/investorprofiles');

        } else {
            $status = AppUtility::getStatusCSS();
            $profiles = $this->model->getAllInvestors();
            $profileChangeRequests = $this->model->getSeedersRequest();

            if( isset( $profiles ) && count ( $profiles ) ){
                foreach( $status as $ks => $vs ){
                    if( $ks != 'Pending' ){
                        foreach( $profiles[$ks] as $k => $v ){
                           $v->Data['LabelClass'] = $vs[0];
                           $v->Data['IClass'] = $vs[1];
                           $v->Data['StatusHTML'] = '<span class="label label-'.$vs[0].'"><i class="'.$vs[1].'"></i>'.$v->Status.'</span>';
                        }
                    }
                }
            }
            
            $approvedCount = isset( $profiles ) ? count( $profiles['Approved'] ) : 0;
            $incompleteCount = isset( $profiles ) ? count( $profiles['Incomplete'] ) : 0;
            $verificationCount = isset( $profiles ) ? count( $profiles['Verification'] ) : 0;
            $requestCount = isset( $profileChangeRequests ) ? count( $profileChangeRequests ) : 0;
            $allCount = $approvedCount + $incompleteCount + $verificationCount;

            $this->setSession( 'redirect', 'cs/investorprofiles' );
            View::page( 'cs/investorprofiles', get_defined_vars() );
        }
    }

    /**
     * Affiliates List
     * 
     * @access private
     * @return na
     */
    public function affiliates()
    {
        $this->setSession( 'lister', array( $this->controller, $this->method ) );
        View::redirect( 'affiliates' );
    }

    /**
     * Investor Booking List
     * 
     * @access private
     * @return na
     */
    public function bookings()
    {
        $userinfo = User::info();
        $this->setSession( 'lister', array( $this->controller, $this->method ) );
        if( User::can( 'Manage TT Receipt' ) || $userinfo->Code == "CSR" ){
            $bookings = $this->model->getAllBookings();
            View::page('cs/bookings', get_defined_vars());

        } else { View::redirect(); }
    }

    /**
     * Pending uploaded documents
     * 
     * @access private
     * @return na
     */
    public function documents()
    {
        $userinfo = User::info();
        $this->setSession( 'lister', array( $this->controller, $this->method ) );
        if( User::can( 'Manage Uploaded Documents' ) || $userinfo->Code == "CSR" ){
            $documents = $this->model->getPendingDocuments();
            
            View::page( 'cs/documents', get_defined_vars() );

        } else { View::redirect(); }
    }

    public function rejectfile()
    {
        if( User::can( 'Manage Uploaded Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doRejectFile( $fileID, $this->segment[3] );

            $referrer = str_ireplace( '/', '', View::getReferrer() );
            if( preg_match( '/invoicesreceipt/', $referrer ) ){
                $invoiceID = str_ireplace( 'invoicesreceipt', '', $referrer );
                $this->model->updateInvoiceAsPaid( $fileID, $invoiceID, 'TT Receipt' );
            }

            View::redirect( false, true );
        }
    }
    
    public function approvefile()
    {
        if( User::can( 'Manage Uploaded Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doApproveFile( $fileID, $this->segment[3] );

            $referrer = str_ireplace( '/', '', View::getReferrer() );
            if( preg_match( '/invoicesreceipt/', $referrer ) ){
                $invoiceID = str_ireplace( 'invoicesreceipt', '', $referrer );
                $this->model->updateInvoiceAsPaid( $fileID, $invoiceID );
            }

            View::redirect( false, true );
        }
    }
    
    public function deletefile()
    {
        if( User::can( 'Manage Uploaded Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doDeleteFile( $fileID, $this->segment[3] );
            View::redirect( false, true );
            //View::redirect('users/profile/documents/'.$this->segment[3].'/'); 
        }
    }
}