<?php
class Capabilitygroups extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::common();
    }

    public function index()
    {
        $groups = $this->model->getCapabilityGroups();        
        View::page('capabilitygroups/list', get_defined_vars());
    }
        
    public function delete()
    {
        $group = $this->model->doDelete($this->segment[2]);
        View::redirect('capabilitygroups');
    }
    
    public function edit()
    {
        $this->model->doSave();
        $group = $this->model->getCapabilityGroup($this->segment[2]);
        View::page('capabilitygroups/edit', get_defined_vars());  
    }
    
    public function add()
    {
        $group = $this->model->doSave();
        View::page('capabilitygroups/add', get_defined_vars());   
    }
}