<?php
class Wallet extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::wallet();
    }

    public function index()
    {   
        View::redirect( 'wallet/dashboard/' );

        // $userinfo = User::info();
        // $mybalance = apputility::getwalletbalance($userinfo->UserID);
        // $pendings = $this->model->countPendingTransactions($userinfo->UserID);
        // $historylog = $this->model->getTransactionLog($userinfo->UserID, 10);

        // $this->model->indexAssets();
        // View::page('wallet/dashboard', get_defined_vars());
    }

    public function dashboard()
    {
        if( User::can( 'View Wallet' ) ){   
            $userinfo = User::info();
            $mybalance = apputility::getwalletbalance($userinfo->UserID);
            $pendings = $this->model->countPendingTransactions($userinfo->UserID);
            $historylog = $this->model->getTransactionLog($userinfo->UserID, 10);

            View::page('wallet/dashboard', get_defined_vars());
        }else{
            View::redirect();
        }
    }

    public function deposit()
    {
        if( User::can( 'View Wallet' ) ){    
            $userinfo = User::info();

            $dosave = $this->model->doSave();

            $segment2 = isset($this->segment[2]) ? $this->segment[2] : '';
            $verifyData = $this->model->verifyIfOwn($segment2, " 'Pending' ");
            if($verifyData) {

                if($dosave){ View::redirect( 'wallet/transactions/pending' ); }  

                $walletData = $this->model->getCashInPendingByID($segment2);
                $coreBank = AppUtility::getPrimaryBankAccount($walletData->CoreBankAccountID); 
                View::page('wallet/cash_in_view', get_defined_vars());
            }else{

                if($dosave){ View::redirect( 'wallet/transactions/' ); }  

                $coreBank = AppUtility::getPrimaryBankAccount(); 

                View::page('wallet/cash_in', get_defined_vars());
            }
        }        
    }

    public function withdraw()
    {
        if( User::can( 'View Wallet' ) ){    
            $userinfo = User::info();
            $dosave = $this->model->doSave();
            $mybalance = apputility::getwalletbalance($userinfo->UserID);

            if($dosave){
                View::redirect( 'wallet/transactions/' );
            } 

            $bankaccounts = $this->model->getBankAccounts();        

            View::page('wallet/cash_out', get_defined_vars());
        }else{
            View::redirect();
        }
    }

    public function transfer()
    {
        View::redirect('wallet');
        // if( User::can( 'View Wallet' ) ){
        //     $userinfo = User::info();
        //     $dosave = $this->model->doSave();
        //     $mybalance = apputility::getwalletbalance($userinfo->UserID);
            
        //     View::page('wallet/cash_transfer', get_defined_vars());
        // }else{
        //     View::redirect();
        // }
    }

    public function transactions()
    {
        if( User::can( 'View Wallet' ) ){    
            $userinfo = User::info();
            $mybalance = apputility::getwalletbalance($userinfo->UserID);
            $pendings = $this->model->countPendingTransactions($userinfo->UserID);
            $historylog = $this->model->getTransactionLog($userinfo->UserID);

            $segment2 = isset($this->segment[2]) ? $this->segment[2] : '';
            switch ($segment2) {
                case 'pending':
                    $transactions = $this->model->getPendingTransactions($userinfo->UserID);
                    View::page('wallet/transactions_pending', get_defined_vars());
                    break;
                
                default:
                    View::page('wallet/transactions_list', get_defined_vars());
                    break;
            } 
        }else{
            View::redirect();
        }
    }

    public function estatement()
    {
        if( User::can( 'View Wallet' ) ){    
            $userinfo = User::info();
            $mybalance = apputility::getwalletbalance($userinfo->UserID);
            $pendings = $this->model->countPendingTransactions($userinfo->UserID);

            $datevalue = isset( $this->post['filterdate'] ) ? $this->post['filterdate'] : false;
            $transtype = isset( $this->post['transtype'] ) ? $this->post['transtype'] : false;

            $statements = $this->model->getAccountStatements($userinfo->UserID, $datevalue, $transtype);

            $segment2 = isset($this->segment[2]) ? $this->segment[2] : '';

            View::page('wallet/transactions_statements', get_defined_vars());
        }else{
            View::redirect();
        }
        
    }

    public function cashins()
    {
        if( User::can( 'Manage Wallet' ) ){
            $userinfo = User::info();
            View::referrer();
            $update = $this->model->manageDoSave();

            $segment2 = isset($this->segment[2]) ? $this->segment[2] : '';
            $segment3 = isset($this->segment[3]) ? $this->segment[3] : '';

            $walletData = $this->model->getTransactionByID($segment3);
            $mybalance = isset($walletData->UserID) ? AppUtility::getwalletbalance($walletData->UserID) : '';

            switch ($segment2) {
                case 'pending':                
                    if($walletData){
                        $coreBank = AppUtility::getPrimaryBankAccount($walletData->CoreBankAccountID); 
                        View::page('wallet/manage/cash_in_view', get_defined_vars());
                    }else{
                        $transdata = $this->model->getTransactionsByStatus(" 1 ", " 'Verified', 'Pending' ");
                        View::page('wallet/manage/cashin_pending', get_defined_vars());
                    }
                    
                    break;
                case 'approved':
                    if($walletData){
                        $coreBank = AppUtility::getPrimaryBankAccount($walletData->CoreBankAccountID);
                        View::page('wallet/manage/cash_in_view', get_defined_vars());
                    }else{
                        $transdata = $this->model->getTransactionsByStatus(" 1 ", " 'Approved' ");
                        View::page('wallet/manage/cashin_approved', get_defined_vars());
                    }
                    break;
            }
        }else{
            View::redirect( 'wallet' );
        }
    }

    public function cashouts()
    {
        if( User::can( 'Manage Wallet' ) ){   
            $userinfo = User::info();
            View::referrer();
            $update = $this->model->manageDoSave();

            $segment2 = isset($this->segment[2]) ? $this->segment[2] : '';
            $segment3 = isset($this->segment[3]) ? $this->segment[3] : '';

            $walletData = $this->model->getTransactionByID($segment3);
            $mybalance = isset($walletData->UserID) ? AppUtility::getwalletbalance($walletData->UserID) : '';

            switch ($segment2) {
                case 'pending':                
                    if($walletData){
                        View::page('wallet/manage/cash_out_view', get_defined_vars());
                    }else{
                        $transdata = $this->model->getTransactionsByStatus(" 2,4 ", " 'Verified', 'Pending' ");
                        View::page('wallet/manage/cashout_pending', get_defined_vars());
                    }
                    
                    break;
                case 'approved':
                    if($walletData){
                        View::page('wallet/manage/cash_out_view', get_defined_vars());
                    }else{
                        $transdata = $this->model->getTransactionsByStatus(" 2,4 ", " 'Approved' ");
                        View::page('wallet/manage/cashout_approved', get_defined_vars());
                    }
                    break;
            }
        }else{
            View::redirect( 'wallet' );
        }
    }
        
    // public function delete()
    // {
    //     $userinfo = User::info();
    //     if( User::can( 'Administer All' ) ){
    //         $data = $this->model->doDelete( $this->segment[2] );
    //         View::redirect( 'contactus/list' );
    //     } else {
    //         View::redirect( 'blogs/lists' );
    //     }
    // }

    public function monthlystatement()
    {
        $email = $this->load->model( 'emailengine', true );

        // Select clients by level
        $clients = $this->model->getUsersByRoles(4);
        
        // FOR CRON JOBS JUST LOOP CLIENT ID
        // $userID = User::info( 'UserID', 101228 );
        // $datevalue = date("m/1/Y").' - '.date("m/t/Y",strtotime('today'));
        // $email->sendStatementOfAccount( $userID, $datevalue );

        $datevalue = date("m/1/Y").' - '.date("m/t/Y",strtotime('today'));
        foreach ($clients as $client) {
            $email->sendStatementOfAccount( $client->UserID, $datevalue );
        }
    }

    public function rejectfile()
    {
        if( User::can( 'Manage Uploaded Wallet Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doRejectFile( $fileID, $this->segment[3] );
            View::redirect( false, true );
            //View::redirect('users/profile/documents/'.$this->segment[3].'/');
        }
    }
    
    public function approvefile()
    {
        if( User::can( 'Manage Uploaded Wallet Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doApproveFile( $fileID, $this->segment[3] );
            //$this->model->doRedirect( $this->segment[3] );
            View::redirect( false, true );
        }
    }
    
    public function deletefile()
    {
        if( User::can( 'Manage Uploaded Wallet Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doDeleteFile( $fileID, $this->segment[3] );
            View::redirect( false, true );
            //View::redirect('users/profile/documents/'.$this->segment[3].'/'); 
        }
    }
}