<?php
class Options extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::common();
    }

    /**
     * Options dashboard page
     * 
     * @access private
     * @return rows
     */
    public function index()
    {   
        $this->model->doSave();
        $optionLists = $this->model->getOptionsList();
        View::page('options/index', get_defined_vars());
    }

    /**
     * Add options page
     * 
     * @access private
     * @return rows
     */
    public function add()
    {
        if(!User::can('Administer All')){
            View::redirect('admin'); 
        }
        $this->model->doSave();
        $groups = $this->model->getOptionGroups();
        View::page('options/add', get_defined_vars());
    }

    /**
     * Edit options page
     * 
     * @access private
     * @return rows
     */
    public function edit()
    {
        if(!User::can('Administer All')){
            View::redirect('admin'); 
        }

        $this->model->doSave();
        $groups = $this->model->getOptionGroups();
        $options = $this->model->getOption($this->segment[2]);
        View::page('options/edit', get_defined_vars());
    }

    /**
     * Delete options page
     * 
     * @access private
     * @return rows
     */
    public function delete()
    {
        $option = $this->model->doDeleteOption($this->segment[1]);
        View::redirect('options');
    }  
}