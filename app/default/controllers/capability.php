<?php
class Capability extends Controller 
{
    public function __construct()
    {
        parent::__construct();

        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::common();
    }

    public function index()
    {
        $capabilities = $this->model->getCapabilities();        
        View::page( 'capability/list', get_defined_vars() );
    }
    
    public function capabilities()
    {
        $capabilities = $this->model->getCapabilities();        
        View::page( 'capability/list', get_defined_vars() );
    }
    
    public function delete()
    {
        $user = $this->model->doDelete( $this->segment[2] );
        View::redirect( 'capability' );
    }
    
    public function edit()
    {
        $this->model->doSave();
        $capgroup = $this->model->getCapabilityGroups();
        $capability = $this->model->getCapability( $this->segment[2] );
        View::page( 'capability/edit', get_defined_vars() );
    }
    
    public function add()
    {
        $pdata = $this->model->doSave();
        $capgroup = $this->model->getCapabilityGroups();
        View::page( 'capability/add', get_defined_vars() );
    }
}