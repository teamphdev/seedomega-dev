<?php
class Blogs extends Controller 
{
    public function __construct()
    {
        parent::__construct();

        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        // Auth::userSession(); // Continue if user has session
        Asset::blogs();
    }

    public function index()
    {
        $clientProfileID = 0;
        $searchString = isset( $_GET['s'] ) ? $_GET['s'] : '';
        $segment1 = isset ( $this->segment[1] ) ? $this->segment[1] : false;

       /* if (strcspn($segment1, '0123456789') != strlen($segment1)){// pull blogs by clientprofileid
            $clientProfileID = $segment1;
            $blogs = $this->model->getBlogs( 'Published', false, false, false, false, null, $clientProfileID );

        } elseif(is_string($segment1)){ //category
            $category = $segment1;
            $blogs = $this->model->getBlogs( 'Published', false, $category );

        } else {
            $blogs = $this->model->getBlogs( 'Published',10 );
        }
        if( isset( $blogs ) && count( $blogs ) ){
            foreach( $blogs as $blog ){
                $titleslug = str_ireplace( ' ', '-', strtolower( $blog->BlogTitle) );
                $blog->Data['FImage'] = View::common()->getUploadedFiles( $blog->FeaturedImage );
                $blog->Link = View::url( 'blogs/view/'.$titleslug.'/'.$blog->BlogID );
            }
        }*/

        $clients = [];
        $publiconly = true;
        if( Auth::isLoggedIn() ){
            $userinfo = User::info();
            $clients = $this->model->getMyClients( $userinfo->UserID );
            $publiconly = false;
        }
        if( $segment1 =='*' ){
            View::redirect( 'blogs' );
        }

        if( is_string( $segment1 ) ){ //category
            $category = $segment1;
            $SOblogs = $this->model->getBlogs( 'Published', false, $category, 'so', $publiconly );
        } else {
            $SOblogs = $this->model->getBlogs( 'Published', false, false, 'so', $publiconly );
        }

        if( $searchString ){
            $SOblogs = $this->model->getBlogs( 'Published', false, false, 'so', $publiconly, $searchString );
        }

        if( isset( $SOblogs ) && count( $SOblogs ) ){
            foreach( $SOblogs as $bl ){
                $titleslug = str_ireplace( ' ', '-', strtolower( $bl->BlogSlug ) );
                $titleslug = str_ireplace( '?', '', $titleslug );
                $bl->Data['FImage'] = View::common()->getUploadedFiles( $bl->FeaturedImage );
                $bl->Link = View::url( 'blogs/view/'.$titleslug.'/'.$bl->BlogID );
            }
        }

        //$categories = $this->model->getCategories( 0, 'CategoryName', $publiconly );
        $categories = $this->model->getAllCategories();

        View::page( 'blogs/publiclist', get_defined_vars() );
    }

    public function lists()
    {
        if( !Auth::isLoggedIn() ){
            View::redirect( 'users/login' );
        }

        if( User::can( 'Administer All' ) || User::can( 'Manage All Blogs' ) ){
            $blogs = $this->model->getBlogs( false, false, false, 'both', false, null, null );
        } else { View::redirect( 'blogs' ); }

        View::page( 'blogs/list', get_defined_vars() );
    }
        
    public function delete()
    {
        $userinfo = User::info();
        if( $userinfo->UserLevel != "Administrator" || !User::can( 'Manage All Blogs' ) ){
            View::redirect( 'blogs/lists' );
        } else {
            $data = $this->model->doDelete( $this->segment[2] );
            View::redirect( 'blogs/lists' );
        }
    }
    
    public function edit()
    {
        $userinfo = User::info();
        switch( $userinfo->UserLevel ){
            case 'Administrator':
                $update = $this->model->doSave();
                $blogdata = $this->model->getBlog($this->segment[2]);
                $categories = $this->model->getCategories();
                View::page( 'blogs/edit', get_defined_vars() );
                break;

            case 'Customer Service':
                $update = $this->model->doSave();
                $blogdata = $this->model->getBlog( $this->segment[2] );
                $categories = $this->model->getCategories();
                View::page( 'blogs/edit', get_defined_vars() );
                break;

            default : 
                View::redirect('blogs');
                break;
        }
    }

    public function view()
    {
        $public = false;
        $update = $this->model->doSave();

        if( Auth::isLoggedIn() ){
            $userinfo = User::info();
            $blogs = $this->model->getBlogs( false, 5 );
            $blogdata = $this->model->getBlog( $this->segment[3] );

            // check if current user has access on this blog post
            if( $blogdata->ClientProfileID == 0 && $blogdata->PublicView == 0 ){
                $access = AppUtility::checkBookingByProject( $userinfo->UserID, 'so' );
                if( $access == 0 ){
                    $blogdata = NULL; // restrict non-bookers to view client's private blogs
                    $public = 1;
                }
            }
        } else {
            $public = 1;
            $blogdata = $this->model->getBlog( $this->segment[3], true );
            $blogs = $this->model->getBlogs( 'Published', false, false, 'so', true );
            if( $blogdata->PublicView == 0 ) $blogdata = NULL; // restrict non-loggin to view private post
        }

        if( isset( $blogs ) && count( $blogs ) ){
            foreach( $blogs as $blog ){
                $titleslug = str_ireplace( ' ', '-', strtolower( $blog->BlogTitle) );
                $titleslug = str_ireplace( '?', '', $titleslug );
                $blog->Data['FImage'] = View::common()->getUploadedFiles( $blog->FeaturedImage );
                $blog->Link = View::url( 'blogs/view/'.$titleslug.'/'.$blog->BlogID );
            }
        }
        if( isset( $blogdata ) ){
            $titleslug = str_ireplace( ' ', '-', strtolower( $blogdata->BlogTitle) );
                $titleslug = str_ireplace( '?', '', $titleslug );
            $blogdata->Data['FImage'] = View::common()->getUploadedFiles( $blogdata->FeaturedImage );
            $blogdata->Link = View::url( 'blogs/view/'.$titleslug.'/'.$blogdata->BlogID );
        }
        if( $blogdata == NULL ){
            View::redirect( 'blogs' );
        }
        // $categories = $this->model->getCategories( 0, 'CategoryName', $public );
        $categories = $this->model->getCategories();

        View::page( 'blogs/view', get_defined_vars() );
    }
    
    public function add()
    {
        $userinfo = User::info();
        if( !User::can( "Manage Categories" ) ){
            View::redirect( 'blogs/lists' );
        } else {
            $update = $this->model->doSave();
            $categories = $this->model->getCategories();
            View::page( 'blogs/add', get_defined_vars() );
        }
    }

    public function categories()
    {
        $userinfo = User::info();
        if( !User::can( "Manage Categories" ) ){
            View::redirect( 'blogs/lists' );
        } else {
            $segment = isset( $this->segment[2] ) ? $this->segment[2] : '';
            $link = 'blogs/listCategory';    
            $cuid = AppUtility::getClientUserID();
            $client = AppUtility::getClientProfileInfo( $cuid );
            $cpid = isset( $client->ClientProfileID ) ? $client->ClientProfileID : 0;
            switch( $segment ){
                case 'add':
                    $link = 'blogs/addCategory';
                    $update = $this->model->doSave();
                    break;

                case 'edit':
                    $link = 'blogs/editCategory';
                    $update = $this->model->doSave();
                    $catdata = $this->model->getCategory( $this->segment[3] );
                    break;

                case 'delete':
                    $data = $this->model->doCatDelete( $this->segment[3] );                    
                    $categories = $this->model->getCategories( $cpid );
                    break;
                
                default:
                    $update = $this->model->doSave();
                    $categories = $this->model->getCategories( $cpid );
                    break;
            }
            View::page( $link, get_defined_vars() );
        }
    }
}