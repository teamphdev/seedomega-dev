<?php
class Manage extends Controller
{
    public function __construct()
    {
        parent::__construct();
            
        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::manage();
    }

    public function index()
    {
        // if( !User::is( 'Administrator' ) ){
        //     View::redirect();
        // }
        View::redirect();

        // View::page( 'admin_manage/dashboard', get_defined_vars() );
    }

    /**
     * Manage Investor Users
     * 
     * @access private
     * @return contents
     */
    public function Investors()
    {
        $userinfo = User::info();
        if( !User::is( 'Administrator' ) ){
            View::redirect();
        }

        $InvestorsCounts = $this->model->getAllInvestorsByStatus();
        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        $currvw = 'admin_manage/investor/investor_dashboard';
        switch( $segment2 ){

            case 'verification':
                $currvw = 'admin_manage/investor/investor_verification';
                break;

            case 'approved':
                $currvw = 'admin_manage/investor/investor_approved';
                break;

            case 'incomplete':
                $currvw = 'admin_manage/investor/investor_incomplete';
                break;

            case 'trashbin':
                if( User::can( 'Administer All' ) ){
                    if( isset( $this->post['uids'] ) ){
                        $users = $this->model->doEmptyTrash( $this->post['uids'] );
                    }
                    $allusers = $this->model->getAllTrashUsers();
                    $currvw = 'admin_manage/investor/investor_trashbin';

                } else {
                    View::redirect( 'manage/investors' );
                } break;
            
            default:
                $allinvestors = $this->model->getAllInvestors( 10 );
                break;
        }
        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Manage Customer Service
     * 
     * @access private
     * @return contents
     */
    public function cs()
    {
        if( User::is( 'Administrator' ) || User::is( 'Manager' ) ){} else View::redirect();
        $userinfo = User::info();
        $rolesModel = $this->load->model( 'roles', true, true );

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';

        switch( $segment2 ){
            case 'edit':
                $this->model->doSave();
                $user = $this->model->getUser( $this->segment[3] );
                $levels = $this->model->getUserLevels();
                $roles = $rolesModel->getLevels();
                $loadedcapa = isset( $this->segment[3] ) ? Level::capabilities( $this->segment[3] ) : false;
                $capabilities =  $this->model->getCapabilities();

                View::page( 'admin_manage/cs/cs_edit', get_defined_vars() );
                break;

            case 'add':
                $userID = $this->model->doSave();

                $levels = $this->model->getUserLevels();
                View::page( 'admin_manage/cs/cs_add', get_defined_vars() );

                if( $userID ){
                    $userInfo = $this->model->getUser( $userID );

                    // Start Email sending
                    $email = $this->load->model( 'emailengine', true );
                    $email->sendUserRegistration( $userID, $userInfo );

                    View::redirect( 'manage/cs' );
                }

                View::page( 'admin_manage/cs/cs_add', get_defined_vars() );
                break;
            
            default:
                $users = $this->model->getUsers();
                View::page( 'admin_manage/cs/cs_list', get_defined_vars() );
                break;
        }
    }

    /**
     * Manage Accounting
     * 
     * @access private
     * @return contents
     */
    public function accounting()
    {
        if( User::is( 'Administrator' ) || User::is( 'Manager' ) ){} else View::redirect();
        $userinfo = User::info();
        $rolesModel = $this->load->model('roles', true, true);

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';

        switch( $segment2 ){
            case 'edit':
                $this->model->doSave();
                $user = $this->model->getUser( $this->segment[3] );
                $levels = $this->model->getUserLevels();
                $roles = $rolesModel->getLevels();
                $loadedcapa = isset( $this->segment[3] ) ? Level::capabilities( $this->segment[3] ) : false;
                $capabilities =  $this->model->getCapabilities();

                View::page( 'admin_manage/accounting/accnt_edit', get_defined_vars() );
                break;
                
            case 'add':
                $userID = $this->model->doSave();
                $levels = $this->model->getUserLevels();

                if( $userID ){
                    $userInfo = $this->model->getUser( $userID );

                    // Start Email sending
                    $email = $this->load->model( 'emailengine', true );
                    $email->sendUserRegistration( $userID, $userInfo );

                    View::redirect( 'manage/accounting' );
                }

                View::page( 'admin_manage/accounting/accnt_add', get_defined_vars() );
                break;
            
            default:
                $users = $this->model->getUsers();
                View::page( 'admin_manage/accounting/accnt_list', get_defined_vars() );
                break;
        }
    }

    /**
     * Manage Accounting : Invoice Type
     * 
     * @access private
     * @return contents
     */
    public function invoice_type()
    {
        if( !User::can( 'Manage Invoices' ) ){
            View::redirect();
        }
        $userinfo = User::info();
        $rolesModel = $this->load->model('roles', true, true);

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        $vw_link = 'admin_manage/accounting/invoice_type/invoice_type_list';

        switch( $segment2 ){
                
            case 'add':
                $this->model->doSaveInvoiceType();
                $vw_link = 'admin_manage/accounting/invoice_type/invoice_type_add';
                break;

            case 'edit':
                $this->model->doSaveInvoiceType();
                $typeID = isset( $this->segment[3] ) ? $this->segment[3] : false;
                if( !$typeID ) { View::redirect( $vw_link ); }

                $itype = $this->model->getInvoiceTypes( $typeID );
                $vw_link = 'admin_manage/accounting/invoice_type/invoice_type_edit';
                break;

            case 'delete':
                $typeID = isset( $this->segment[3] ) ? $this->segment[3] : false;

                $where = array( 'InvoiceTypeID' => $typeID );        
                $this->db->delete( "invoice_type", $where );
                $this->setSession( 'message', "Permanently deleted invoice type ID#$typeID." );
                App::activityLog( "Permanently deleted invoice type ID#$typeID." );

                $itypes = $this->model->getInvoiceTypes();
                break;
            
            default:
                $itypes = $this->model->getInvoiceTypes();
                break;
        }
        
        View::page( $vw_link, get_defined_vars() );
    }

    /**
     * Manage Clients
     * 
     * @access private
     * @return contents
     */
    public function clients()
    {
        if( User::is( 'Administrator' ) || User::can( 'Manage Clients' ) ){
        } else View::redirect();
 
        $userinfo = User::info();
        $rolesModel = $this->load->model( 'roles', true, true );
        $this->setSession( 'lister', array( $this->controller, $this->model, 'client profiles' ) );

        $segment2 = isset( $this->segment[2] ) && $this->segment[2] != NULL ? $this->segment[2] : '';
        $segment3 = isset( $this->segment[3] ) && $this->segment[3] != NULL ? $this->segment[3] : 0;
        $currvw = 'admin_manage/client/client_list';

        switch( $segment2 ){
            case 'add':
                $userID = $this->model->doSave();
                $levels = $this->model->getUserLevels();

                if( $userID ){
                    $userInfo = $this->model->getUser( $userID );

                    // Start Email sending
                    $email = $this->load->model( 'emailengine', true );
                    $email->sendUserRegistration( $userID, $userInfo );
                    
                    View::redirect( false, true );
                    //View::redirect( 'manage/client' );
                }
                
                $currvw = 'admin_manage/client/client_add';
                break;

            case 'edit':
                $this->model->doSave();
                $user = $this->model->getUser( $segment3 );
                $levels = $this->model->getUserLevels();
                $roles = $rolesModel->getLevels();
                $loadedcapa = $segment3 ? Level::capabilities( $segment3 ) : false;
                $capabilities =  $this->model->getCapabilities();

                $currvw = 'admin_manage/client/client_edit';
                break;

            case 'pending':
                View::referrer();
                $allClients = $this->model->getAllClients( 'Pending' );
                $currvw = 'admin_manage/client/client_pending';
                break;

            case 'delete':
                $client = $this->model->getClientData( $segment3 );
                $uid = isset( $client ) && $client->UserID != NULL ? $client->UserID : 0;
                $success = $this->model->doDelete( $uid );
                if( $success ){
                    $msg = "Permanently deleted client ".$client->CompanyName." (ID#".$client->ClientProfileID.").";
                    $this->setSession( "message", $msg );
                    App::activityLog( $msg );
                }
                $allClients = $this->model->getAllClients();
                break;
            
            default:
                View::referrer();
                $allClients = $this->model->getAllClients();
                break;
        }

        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Manage Slider
     * 
     * @access private
     * @return contents
     */
    public function slider()
    {
        $userinfo = User::info();
        if( !User::is( 'Administrator' ) ){
            View::redirect();
        }

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        $currvw = 'admin_manage/slider/slider_list';
        switch( $segment2 ){

            case 'add':
                $this->model->doSave();
                $currvw = 'admin_manage/slider/slider_add';
                $clients = $this->model->getCompleteClientData();
                $optionsname = $this->model->getCompleteClientData( 'slider' );
                break;
            
            case 'edit':
                $this->model->doSave();
                $currvw = 'admin_manage/slider/slider_edit';
                $sliders = $this->model->getSliders( isset( $this->segment[3] ) ? $this->segment[3] : false );
                if( isset( $sliders ) && count( $sliders ) ){
                    foreach ($sliders as $slider ) { };
                }
                break;

            default:
                $sliders = $this->model->getSliders();
                break;
        }

        if( isset( $sliders ) && count( $sliders ) ){
            foreach( $sliders as $slider ){
                $jdata[$slider->SlideID] = array( 'Title' => $slider->Title, 'Desc' => $slider->Description );
            }
        }
        $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );

        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Manage Core Team
     * 
     * @access private
     * @return contents
     */
    public function team()
    {
        if( User::can( 'Administer All' ) || User::can( 'Manage Teams' ) ){

            // if( empty( $this->segment[2] ) ){
            //     View::redirect();
            // }

            $this->setSession( 'lister', array( $this->controller, $this->model, 'Manage Core Team Members' ) );
            
            $userinfo = User::info();
            $action = isset( $this->segment[2] ) ? $this->segment[2] : 'manage';
            $teamid = isset( $this->segment[3] ) ? $this->segment[3] : false;
            $referrer = isset( $this->segment[4] ) ? $this->segment[4] : '';
            $currvw = 'admin_manage/team/team_list';

            switch( strtolower( $action ) ){
                case 'add':
                    $currvw = 'admin_manage/team/team_add';
                    $userID = $this->model->doSaveTeam( $userinfo->UserID );

                    // if( $userID ){
                    //     $userInfo = AppUtility::getUser( $userID );
                    //     // Start Email sending
                    //     $email = $this->load->model( 'emailengine', true );
                    //     $email->sendUserRegistration( $userID, $userInfo );
                    // }
                    
                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Add Core Team Member' );
                    break;

                case 'edit':
                    $currvw = 'admin_manage/team/team_edit';
                    $this->model->doSaveTeam( $userinfo->UserID, $teamid );
                    $teams = $this->model->getTeamsByMemberID( $teamid );
                    $dataheaders = array( 'Photo' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $teams[0]->Name );
                    break;

                case 'trash':
                    $this->model->doTrashTeam( $teamid );
                    $teams = $this->model->getTeams( true );
                    $dataheaders = array( 'Photo' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    break;

                case 'delete':
                    $currvw = 'admin_manage/team/team_trashbin';
                    $this->model->doDeleteTeam( $teamid );
                    $teams = $this->model->getTeams( true );
                    $dataheaders = array( 'Photo' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    break;

                case 'restore':
                    $this->model->doRestoreTeam( $teamid );
                    $teams = $this->model->getTeams( true );
                    $dataheaders = array( 'Photo' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    break;

                case 'trashbin':
                    if( User::can( 'Delete Team Members') ){
                        $currvw = 'admin_manage/team/team_trashbin';
                        if( isset( $this->post['uids'] ) ){
                            $this->model->doEmptyTrashTeam( $this->post['uids'] );
                        }                    
                        $teams = $this->model->getTrashedTeams();
                        $dataheaders = array( 'Photo' );
                        AppUtility::processClientsData( $teams, $dataheaders );
                    } else {
                        $teams = $this->model->getTeams( true );
                        $dataheaders = array( 'Photo' );
                        AppUtility::processClientsData( $teams, $dataheaders );

                        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Manage Core Team Members' );
                    }
                    break;

                default:
                    $teams = $this->model->getTeams( true );
                    $dataheaders = array( 'Photo' );
                    AppUtility::processClientsData( $teams, $dataheaders );

                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Manage Core Team Members' );
                    break;
            }

            if( isset( $teams ) && count( $teams ) ){
                foreach( $teams as $team ){
                    $jdata[$team->CoreTeamID] = array( 'FullName' => $team->Name, 'Bio' => $team->Bio );
                }
            }
            $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );

            View::page( $currvw, get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Manage Testimonials
     * 
     * @access private
     * @return contents
     */
    public function testimonial()
    {
        $userinfo = User::info();
        if( !User::is( 'Administrator' ) ){
            View::redirect();
        }

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        $currvw = 'admin_manage/testimonial/testi_list';

        switch( $segment2 ){

            case 'add':
                $this->model->doSave();
                $clients = $this->model->getCompleteClientData();
                $optionsname = $this->model->getCompleteClientData( 'testimonial' );
                $currvw = 'admin_manage/testimonial/testi_add';
                break;
            
            case 'edit':
                $this->model->doSave();
                $testimonials = $this->model->getTestimonials( isset( $this->segment[3] ) ? $this->segment[3] : false );
                if( isset( $testimonials ) && count( $testimonials ) ){
                    foreach ($testimonials as $testimonial ) { };
                }
                $currvw = 'admin_manage/testimonial/testi_edit';
                break;

            default:
                $testimonials = $this->model->getTestimonials();
                break;
        }

        if( isset( $testimonials ) && count( $testimonials ) ){
            foreach( $testimonials as $testi ){
                $jdata[$testi->TestimonialID] = array( 'FullName' => $testi->Name, 'Message' => $testi->Message );
            }
        }
        $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );

        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Manage Support
     * 
     * @access public
     * @return contents
     */
    public function support()
    {
        if( User::is( 'Administrator' ) ){
            $userinfo = User::info();

            View::page( 'admin_manage/support/view', get_defined_vars() );
        } else { View::redirect(); }
    }

    public function trash()
    {
        if( User::can( 'Administer All' ) ){
            $useraccount = $this->model->doTrash( $this->segment[2] );
            $link = $this->segment[3].'/'.$this->segment[4];
            View::redirect( 'manage/'.$link );
        }
    }
    
    public function restore()
    {
        if( User::can( 'Administer All' ) ){
            $useraccount = $this->model->doRestore( $this->segment[2] );
            $link = $this->segment[3].'/'.$this->segment[4];
            View::redirect( 'manage/'.$link );
        }
    }
        
    public function delete()
    {
        if( User::can( 'Administer All' ) ){
            $userID = isset( $this->segment[2] ) ? $this->segment[2] : 0;
            $useraccount = $this->model->doDelete( $userID );
            $link = $this->segment[3].'/'.$this->segment[4];
            View::redirect( 'manage/'.$link );
        }
    }

    public function managedelete()
    {
        if( User::can( 'Administer All' ) ){
            $i = 0;
            $deletea = array();
            switch( $this->segment[2] ){
                case 'testimonial':
                    $deletea[$i]['ID'] = $this->segment[3]; // value
                    $deletea[$i]['TN'] = 'testimonials'; // table name
                    $deletea[$i++]['CN'] = 'TestimonialID'; // column name
                    break;
                case 'slider':
                    $deletea[$i]['ID'] = $this->segment[3];
                    $deletea[$i]['TN'] = 'slides';
                    $deletea[$i++]['CN'] = 'SlideID';
                    break;
                default: break;
            }
            $useraccount = $this->model->doManageDelete( $deletea );
            if( isset( $this->segment[4] ) ){
                AppUtility::deleteFileAndAssociates( $this->segment[4] );
            }
            $link = $this->segment[2];
            View::redirect( 'manage/'.$link );
        }
    }

    public function rejectfile()
    {
        if( User::can( 'Manage Uploaded Documents' ) ){
            $fileID = $this->segment[2];
            $this->model->doRejectFile( $fileID );
            View::redirect( false, true );
            // View::redirect( 'bookings/info/'.$this->segment[3] );
        }
    }
    
    public function approvefile()
    {
        if( User::can( 'Manage Uploaded Documents' ) ){
            $bookID = $this->segment[3];
            $this->model->doApproveFile( $this->segment[2], $bookID );
            View::redirect( false, true );
            // View::redirect( 'bookings/info/'.$this->segment[3] );
        }
    }
    
    public function deletefile()
    {
        if( User::can( 'Manage Uploaded Documents' ) ){
            $bookID = $this->segment[3];
            $this->model->doDeleteFile( $this->segment[2], $bookID );
            View::redirect( false, true );
            // View::redirect( 'bookings/info/'.$this->segment[3] ); 
        }
    }

    public function getselecteduser()
    {
        $userid = $this->segment[2];
        $userinfos = $this->model->getCompleteClientData( false, $userid );
        header( "Content-type:application/json" );
        echo json_encode( $userinfos );
    }
}