<?php
class Api extends Controller 
{
        
    public function __construct()
    {
        parent::__construct();
         
        // Load helper class
        $this->load->helper('auth'); 
        $this->load->helper('apputility');        
    }

    public function index()
    {    
        if(!Auth::isLoggedIn()) {
            View::redirect('users/login');
        }
    }
    
    public function get()
    {  
         
        $id  = isset($this->segment[2]) ? $this->segment[2] : 'none';
    	$key = isset($this->segment[3]) ? $this->segment[3] : 'none';
    	$out = isset($this->segment[4]) ? $this->segment[4] : 'object';
        
        if($this->model->apiConnect($id,$key)) {
            switch(strtolower($out)) {
                case 'json': 
                    echo json_encode($this->model->getAccounts(), JSON_UNESCAPED_UNICODE);
                break;
                case 'array': 
                    header('Content-Type: text/html; charset=utf-8'); 
                    //echo '<pre>';
                    print_r($this->model->getAccounts($out));
                break;
				case 'cleanarray': 
                    header('Content-Type: text/html; charset=utf-8'); 
                    echo '<pre>';
                    print_r($this->model->getAccounts($out));
                break;
                default: 
                    header('Content-Type: text/html; charset=utf-8'); 
                    //echo '<pre>';
                    print_r($this->model->getAccounts());
                break;
            }
        } else {
            echo 'Error: Please check the user id and api key is correct, or contact the administrator.';
        }
    }
    
    public function post()
    {  
        $return = false;

        $id  = isset($this->segment[2]) ? $this->segment[2] : 'none';
        $key = isset($this->segment[3]) ? $this->segment[3] : 'none';
        $cid = isset($this->segment[4]) ? $this->segment[4] : false;
        $table = isset($this->segment[5]) ? $this->segment[5] : false;
        
        if($this->model->apiConnect($id,$key)) {
            $return = $this->model->updateAccounts($cid, $table);
        } else {
            $return = 'Error: Please check the user id and api key is correct, or contact the administrator.';
        }

        echo $return;
    }
}