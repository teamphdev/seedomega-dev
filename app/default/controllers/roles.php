<?php
class Roles extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        // Load model Auth
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::continueFor( "Administrator" );
        Asset::common();
    }

    public function index()
    {   
        $userinfo = User::info();

        $roles = $this->model->getLevels();
        View::page('roles/list', get_defined_vars());
    }
   
    public function delete()
    {
        $userinfo = User::info();

        $user = $this->model->doDelete($this->segment[2]);
        View::redirect('roles');
    }
    
    public function edit()
    {
        $userinfo = User::info();

        $this->model->doSave();
        $role = $this->model->getLevel($this->segment[2]);
        $capabilities = $this->model->getCapabilities();
        
        View::page('roles/edit', get_defined_vars());
    }
    
    public function apply()
    {
        $userinfo = User::info();

        $this->model->doApplyToUsers($this->segment[2]); 
        View::redirect('roles/edit/'.$this->segment[2]);
    }
    
    public function add()
    {
        $userinfo = User::info();
        
        $pdata = $this->model->doSave();
        View::page('roles/add', get_defined_vars());
    }
}