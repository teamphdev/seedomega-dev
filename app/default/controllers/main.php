<?php
class Main extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Asset::main();
    }

    public function index()
    {
        if( Auth::isLoggedIn() ){
            $referrer = ($this->getCookie(Config::get('REFERRER'))) ? $this->getCookie(Config::get('REFERRER')) : User::dashboardLink(true);
            View::redirect( $referrer );
        } else {
            //$this->model->setPw();

            //get recent sliders
            $sliders = $this->model->getRecentSliders();
            $dataheaders = array( 'Avatar', 'BarColor', 'Percentage', 'SliderImage' );
            AppUtility::processClientsData( $sliders, $dataheaders );

            //get currently fundings (active projects)
            $currentlyfundings = $this->model->getCurrentlyFundings(3);
            $dataheaders = array( 'Avatar', 'BarColor', 'Percentage', 'CompanyPhoto', 'CompanyLogo' );
            AppUtility::processClientsData( $currentlyfundings, $dataheaders );
            
            //get our backers
            $backers = $this->model->getOurBackers();
            $dataheaders = array( 'Avatar' );
            AppUtility::processClientsData( $backers, $dataheaders );

            $upcomings = $this->model->getUpcomingEvents(); //get upcoming projects

            View::page('main/homepage', get_defined_vars());
        }
    }

    public function projects()
    {
        $segment1 = ( isset( $this->segment[1] ) ) ? $this->segment[1] : '';
        $clientID = ( isset( $this->segment[2] ) ) ? $this->segment[2] : '';

        $enabledTabs = (Option::get('global_promo') == 1 && User::isLoggedIn() || AppUtility::investorinfo( 'InvestorStatus' ) == 'Approved' || User::is( 'Administrator' ) ) ? true : false;

        switch ( $segment1 ){
            case 'view':
                if( $clientID ){
                    $userinfo = User::info();

                    $clientdata = $this->model->getClientData($clientID);
                    if( !$clientdata ){
                        View::redirect('clients');
                    }
                    
                    $faqs = $this->model->getFaqs( $clientdata->ClientProfileID, NULL );

                    $blog = $this->model->getBlogInfo( $clientdata->ClientProfileID );
                    if( $blog ){
                        $recentPosts = $this->model->getRecentPosts( $clientdata->ClientProfileID );
                    }
                    
                    $comments = $this->model->getComments( $clientdata->ClientProfileID );

                    $communities = $this->model->getCommunities( $clientdata->ClientProfileID );

                    AppUtility::updateLastView( $userinfo, $clientdata );

                    View::page( 'main/client_view', get_defined_vars() );

                }else{
                    View::redirect( 'projects' );
                }
                break;
            
            default:
                $projects = $this->model->getActiveProjects();
                $testimonials = $this->model->getRecentTestimonials();
                View::page( 'projects', get_defined_vars() );
                View::redirect( 'projects' );
                break;
        }
    }

    public function search()
    {
        $searchresults = $this->model->getCompanies();
        View::page('main/search', get_defined_vars());
    }

    public function aboutus()
    {
        View::page('main/aboutus', get_defined_vars());
    }

    public function ipoplacement()
    {
        View::page('main/ipoplacement', get_defined_vars());
    }

    public function seeders()
    {
        View::page('main/seeders', get_defined_vars());
    }

    public function oursolution()
    {
        View::page('main/solution', get_defined_vars());
    }

    public function ourteam()
    {
        $manageModel = $this->load->model( 'manage', true, true );
        $teams = $manageModel->getTeams( true );
        if( isset( $teams ) && count( $teams ) ){
            foreach( $teams as $team ){
                $team->PhotoLink = '<img src="'.View::asset( isset( $team->FileSlug ) && $team->FileSlug != '' ? 'files'.$team->FileSlug : 'images/user.png' ).'">';
                $team->AvatarLink = '<img src="'.View::asset( isset( $team->FileSlug ) && $team->FileSlug != '' ? 'files'.$team->FileSlug : 'images/user.png' ).'">';
                $data = array(
                    'Name' => isset( $team->Name ) ? $team->Name : '',
                    'Position' => isset( $team->Position ) ? $team->Position : '',
                    'Bio' => isset( $team->Bio ) ? $team->Bio : '',
                    'Avatar' => $team->AvatarLink,
                    'Photo' => isset( $team->Photo ) ? $team->Photo : '0',
                    'PhotoLink' => $team->PhotoLink
                );
                $team->Data = str_ireplace( '"', '&quot;', json_encode( $data ) );
            }
        }

        View::page('main/team', get_defined_vars());
    }

    public function privacypolicy()
    {
        View::page('main/privacyPolicy', get_defined_vars());
    }

    public function termsandcondition()
    {
        $this->model->doSave();
        $tac = $this->model->getTAC();
        View::page( 'main/termsCondition', get_defined_vars() );
    }

    public function footer()
    {
        $segment1 = (isset($this->segment[1])) ? $this->segment[1] : '';
        //View::page(isset($segment1) ? 'footer/'.$segment1 : '#', get_defined_vars());
        isset($segment1) ? View::page('footer/'.$segment1, get_defined_vars()) : View::redirect('/') ;
    }
    
    public function ajax()
    {
        echo $this->model->getUserByCode($this->post['ID']);
    }
    
    public function checkemail()
    {
        echo User::infoByEmail('UserID',$this->post['email']);
    }
    
    public function changelanguage()
    {
        $u = $this->load->model('users', true, true);
        $this->model->updateLanguage($this->post['ID']);
        
        $u->updateUserInfo();
        Lang::init();
    }  

    public function login()
    {
        View::redirect('users/login');        
    } 

    public function signup()
    {
        View::redirect('users/signup');        
    } 

    public function register()
    {
        View::redirect('users/signup');        
    } 

    //====================================================================
    //      READY MADE TEMPLATE
    //      When finalized the mvc just drag/copy specific template to use
    //====================================================================
    public function join()
    {   
        $old = array('http://www','http://');
        $new = array('','');
        $domain = str_replace($old, $new, View::url());

        $affiliateID = isset($this->segment[1]) ? $this->segment[1] : false;

        if($affiliateID) {
            $userdata = (User::info(false,$affiliateID)) ? User::info(false,$affiliateID) : false;
            if($userdata && isset($userdata->UserID)) {
                setcookie(Config::get('AFFILIATEKEY'),$affiliateID,time()+ (6 * 30 * 24 * 3600),'/',$domain);
            }
        }   
             
        $exemptedID = isset($this->segment[2]) ? $this->segment[2] : false;
        
        if($exemptedID) {
            $profiledata = (AppUtility::getClientProfileInfoByID($exemptedID)) ? AppUtility::getClientProfileInfoByID($exemptedID) : false;
            if($profiledata && isset($profiledata->ClientProfileID)) {
                
                setcookie(Config::get('EXEMPTIONKEY'),$exemptedID,time()+ (6 * 30 * 24 * 3600),'/',$domain);
                setcookie(Config::get('REFERRER'),'bookings/book/'.$exemptedID.'/',time()+ (24 * 3600),'/',$domain);
            }
        } 

        if(Auth::isLoggedIn()) {
            if($exemptedID) {
                View::redirect('bookings/book/'.$exemptedID.'/'); 
            } else {
                User::dashboardRedirect();
            }
        } else {
            View::redirect('users/signup'); 
        }
    } 

    public function invest()
    {   
        $old = array('http://www','http://');
        $new = array('','');
        $domain = str_replace($old, $new, View::url());

        $affiliateID = isset($this->segment[1]) ? $this->segment[1] : false;

        if($affiliateID) {
            $userdata = (User::info(false,$affiliateID)) ? User::info(false,$affiliateID) : false;
            if($userdata && isset($userdata->UserID)) {
                setcookie(Config::get('AFFILIATEKEY'),$affiliateID,time()+ (6 * 30 * 24 * 3600),'/',$domain);
            }
        }   
             
        $exemptedID = isset($this->segment[2]) ? $this->segment[2] : false;
        
        if($exemptedID) {
            $profiledata = (AppUtility::getClientProfileInfoByID($exemptedID)) ? AppUtility::getClientProfileInfoByID($exemptedID) : false;
            if($profiledata && isset($profiledata->ClientProfileID)) {
                
                setcookie(Config::get('EXEMPTIONKEY'),$exemptedID,time()+ (6 * 30 * 24 * 3600),'/',$domain);
                setcookie(Config::get('REFERRER'),'bookings/book/'.$exemptedID.'/',time()+ (24 * 3600),'/',$domain);
            }
        } 

        if(Auth::isLoggedIn()) {
            if($exemptedID) {
                View::redirect('bookings/book/'.$exemptedID.'/'); 
            } else {
                User::dashboardRedirect();
            }
        } else {
            View::redirect('users/signup'); 
        }
    }

    public function clearreferralcache()
    {  
        $old = array('http://www','http://');
        $new = array('','');
        $domain = str_replace($old, $new, View::url());

        setcookie(Config::get('AFFILIATEKEY'),false,time() - 3600,'/',$domain);
        setcookie(Config::get('EXEMPTIONKEY'),false,time()- 3600,'/',$domain);
        setcookie(Config::get('REFERRER'),false,time() - 3600,'/',$domain);

        if(Auth::isLoggedIn()) {
            User::dashboardRedirect();
        } else {
            View::redirect('users/login'); 
        }
    }    

    public function projectsingle()
    {
        View::page('templates/projectsingle', get_defined_vars());
    }

    public function userprofile()
    {
        View::page('templates/userprofile', get_defined_vars());
    }

    public function tablesstyles()
    {
        View::page('templates/tables_styles', get_defined_vars());
    }

    public function tablesresponsive()
    {
        View::page('templates/tables_responsive', get_defined_vars());
    }

    public function tablesdatatables()
    {
        View::page('templates/tables_datatables', get_defined_vars());
    }
    public function formswizard()
    {
        View::page('templates/forms_wizard', get_defined_vars());
    }
    public function projectwizard()
    {
        View::page('templates/projectwizard', get_defined_vars());
    }

    public function formelements()
    {
        View::page('templates/forms_elements', get_defined_vars());
    }

    public function formelementsfundz()
    {
        View::page('templates/form_elements_fundz', get_defined_vars());
    }

    public function uibuttons()
    {
        View::page('templates/ui_buttons', get_defined_vars());
    }

    public function modals()
    {
        View::page('templates/modals', get_defined_vars());
    }
}