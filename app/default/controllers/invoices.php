<?php
class Invoices extends Controller
{
    public function __construct()
    {
        parent::__construct();     
        
        $this->load->model( 'users', false, true );
        $this->load->model( 'clients', false, true );

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::invoices();
    }

    /**
     * User invoices page
     * 
     * @access private
     * @return na
     */
    public function index()
    {
        View::referrer();
        if( !User::can( 'Manage Invoices' ) ){
            View::redirect();
        }

        //this will pull invoices for user/investor
        $invoices = $this->model->getAllInvoices();
        
        // $invoicesTTReceiptAll = $this->model->getUserInvoicesByStatus( 'TT Receipt' );
        // if( isset( $invoicesTTReceiptAll ) && count( $invoicesTTReceiptAll )){
        //     foreach( $invoicesTTReceiptAll as $tt ){
        //         # code...
        //     }
        // }

        $invoicesTTReceiptAll = $this->model->getUserInvoicesByStatus( 'TT Receipt' );        
        $invoicesPaid = $this->model->getUserInvoicesByStatus( 'Paid' );
        $invoicesOutstanding = $this->model->getUserInvoicesByStatus( 'Outstanding' );
        $invoicesOverdue = $this->model->getInvoicesOverdue();
        // $outstandingCount = AppUtility::countConditional( $invoicesOutstanding, 'Status', '=', 'Outstanding' );

        if( isset( $invoicesPaid ) && count( $invoicesPaid ) ){
            foreach( $invoicesPaid as $inv ){
                if( $inv->Invested != NULL ){ $inv->InvestmentAmount = $inv->Invested; }
            }
        }

        if( isset( $invoicesTTReceiptAll ) && count( $invoicesTTReceiptAll ) ){
            foreach( $invoicesTTReceiptAll as $inv ){
                if( $inv->Invested != NULL ){ $inv->InvestmentAmount = $inv->Invested; }
            }
        }

        if( isset( $invoicesOutstanding ) && count( $invoicesOutstanding ) ){
            foreach( $invoicesOutstanding as $inv ){
                if( $inv->Invested != NULL ){ $inv->InvestmentAmount = $inv->Invested; }
            }
        }

        if( isset( $invoicesOverdue ) && count( $invoicesOverdue ) ){
            foreach( $invoicesOverdue as $inv ){
                if( $inv->Invested != NULL ){ $inv->InvestmentAmount = $inv->Invested; }
            }
        }
        
        View::page( 'invoices/index', get_defined_vars() );
    }

    /**
     * User invoices page
     * 
     * @access private
     * @return na
     */
    public function dashboard()
    {
        if( !User::can( 'Manage Invoices' ) ){
            View::redirect();
        }

        $allPendings = $this->model->getAllPendingsCount();
        
        View::page( 'invoices/dashboard', get_defined_vars() );
    }

    /**
     * Pending uploaded documents
     * 
     * @access private
     * @return na
     */
    public function documents()
    {
        $userinfo = User::info();
        $this->setSession( 'lister', array( $this->controller, $this->method ) );
        if( User::can( 'Manage Invoices' ) ){
            $documents = $this->model->getPendingDocuments();
            View::page( 'invoices/documents', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * User commissions page
     * 
     * @access private
     * @return na
     */
    public function withdrawal()
    {
        if( !User::can( 'Manage Withdrawals' ) ){
            View::redirect();
        }

        $earningID = $this->model->doSave();
        if( $earningID ){
            $withdrawaldata = $this->model->getAllWithdrawals( false, $earningID['EarningID'] );
            $email = $this->load->model( 'emailengine', true );
            if( $earningID['Status'] == 'Approved' ){
                $email->sendWithdrawalApprovalEmail( $withdrawaldata );
            } else {
                $email->sendWithdrawalPaymentEmail( $withdrawaldata );
            }
        }

        //this will pull commission withdrawal request for investors/clients
        $allWithdrawals = $this->model->getAllWithdrawals();

        $pendingWithdrawals = isset( $allWithdrawals['Pending'] ) && count( $allWithdrawals['Pending'] ) ? $allWithdrawals['Pending'] : array();
        if( isset( $pendingWithdrawals ) && count( $pendingWithdrawals ) ){
            foreach( $pendingWithdrawals as $keys => $pendings ){
                foreach( $pendings as $key => $pending ){
                    if( $key != 'Data' ){
                        $pendings->Data[$key] = $pending;
                    }
                }
                $pendings->JSON = json_encode( $pendings->Data );
            };
        }

        $approvedWithdrawals = isset( $allWithdrawals['Approved'] ) && count( $allWithdrawals['Approved'] ) ? $allWithdrawals['Approved'] : array();
        if( isset( $approvedWithdrawals ) && count( $approvedWithdrawals ) ){
            foreach( $approvedWithdrawals as $keys => $approves ){
                foreach( $approves as $key => $approve ){
                    if( $key != 'Data' ){
                        $approves->Data[$key] = $approve;
                    }
                }
                $approves->JSON = json_encode( $approves->Data );
            };
        }

        $paidWithdrawals = isset( $allWithdrawals['Paid'] ) && count( $allWithdrawals['Paid'] ) ? $allWithdrawals['Paid'] : array();
        // if( isset( $paidWithdrawals ) && count( $paidWithdrawals ) ){
        //     foreach( $paidWithdrawals as $keys => $paids ){
        //         foreach( $paids as $key => $paid ){
        //             if( $key != 'Data' ){
        //                 $paids->Data[$key] = $paid;
        //             }
        //         }
        //         $paids->JSON = json_encode( $paids->Data );
        //     };
        // }

        $pendingCount = isset( $pendingWithdrawals ) && count( $pendingWithdrawals ) ? count( $pendingWithdrawals ) : '0';
        $approvedCount = isset( $approvedWithdrawals ) && count( $approvedWithdrawals ) ? count( $approvedWithdrawals ) : '0';
        $paidCount = isset( $paidWithdrawals ) && count( $paidWithdrawals ) ? count( $paidWithdrawals ) : '0';
        $allWithdrawalCount = (int)$pendingCount + (int)$approvedCount + (int)$paidCount;

        View::page( 'invoices/withdrawal', get_defined_vars() );
    }

    /**
     * Client's bank accounts page
     * 
     * @access private
     * @return na
     */
    public function banks()
    {
        if( User::can( "Manage Bank Accounts" ) ){
            $request = '';
            $allclient = '';
            $userinfo = User::info();
            $this->setSession( 'lister', array( $this->controller, $this->method, 'bank Accounts' ) );
            $csModel = $this->load->model( 'cs', true, true );
            
            if( isset( $this->segment[2] ) && $this->segment[2] == '1' ){
                $request = 'active';
            } else {                
                $allclient = 'active';
            }
            $allClients = $csModel->getAllClients();
            $pendingClients = $csModel->getPendingClients();
            $pendingClientRequests = $csModel->getClientChangeRequests( 'bank' );

            // $tempClientRequests = $csModel->getPendingClients( true );
            // if( isset( $tempClientRequests ) && count( $tempClientRequests ) ){
            //     foreach( $tempClientRequests as $key => $requests ){
            //         $requests->ChangeRequest = json_decode( $requests->ChangeRequest, true );
            //         if( isset( $requests->ChangeRequest['bank'] ) ){
            //             array_push( $pendingClientRequests, $tempClientRequests[$key] );
            //         }
            //     }
            // }

            $allClientsCount = isset( $allClients ) ? count( $allClients ) : 0;
            $pendingClientsCount = isset( $pendingClients ) ? count( $pendingClients ) : 0;
            $pendingClientRequestsCount = isset( $pendingClientRequests ) ? count( $pendingClientRequests ) : 0;
            $approvedClientsCount = AppUtility::countConditional( $allClients, 'Status', '==', 'Approved' );

            View::page( 'invoices/bank_accounts', get_defined_vars() );
            
        } else { View::redirect(); }
    }

    /**
     * Create invoice page
     * 
     * @access private
     * @return na
     **/
    public function createold()
    {
        //accessible by Accounting only
        if( !User::can( "Manage Invoices" ) ){
            View::redirect();
        }

        $userinfo = User::info();

        $this->model->doSave();

        $segment2 = ( isset( $this->segment[2] ) ) ? $this->segment[2] : '';
        $segment3 = ( isset( $this->segment[3] ) ) ? $this->segment[3] : '';
        if( $segment2 ){
            $clientId = $segment2;
            $investmentBookingID = $segment3;

            // $client = $this->model->getClientDetails( $clientId );
            $booking = $this->model->getBookedData( $investmentBookingID );

            //$totalInvoiceAmount = $booking->TotalAmountAttached * 0.05;
            View::page( 'invoices/create-2', get_defined_vars() );
        } else {
            $clients = $this->model->getClients();
            View::page( 'invoices/create', get_defined_vars() );
        }
    }

    /**
     * Create blance invoice page
     * 
     * @access private
     * @return na
     **/
    public function create()
    {
        //accessible by Accounting only
        if( !User::can( "Manage Invoices" ) ){
            View::redirect();
        }

        $all =  isset( $this->segment[2] ) && $this->segment[2] == '1' ? false : true;

        $userinfo = User::info();
        $data = $this->post;
        $this->model->doSave();

        $invoiceTypes = $this->model->getInvoiceTypes();

        $clients = $this->model->getClients( $all );
        $logo = '<img src="'.View::asset( 'images/omegalogo.png' ).'">';
        View::page( 'invoices/createnew', get_defined_vars() );
    }

    /**
     * View Invoice details
     *
     * @access private
     * @return na
     */
    public function view()
    {
        //accessible by Accounting only
        if( !User::can( "View Invoices" ) ){
            View::redirect( 'invoices' );
        }

        $id = $this->segment[2];
        if( empty( $id ) ){
            View::redirect( 'invoices' );
        }

        $hidepay = ' hidden ';
        $userinfo = User::info();
        if( isset( $userinfo->Code ) && ( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ) ){
            $hidepay = '';
            $clientModel = $this->load->model( 'clients', true, true );
            $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $clientModel->doSaveInvoicePayment( $cuid );
        }

        $invoice = $this->model->getDetails( $id );
        $investorData = User::info( false, $invoice[0]->UserID );
        $mybalance = apputility::getwalletbalance( $invoice[0]->UserID );
        $logo = '<img src="'.View::asset( 'images/omegalogo.png' ).'">';
        if( isset( $invoice[0]->Status ) && $invoice[0]->Status == 'Paid' ){ $hidepay = ' hidden '; }
        if( isset( $invoice[0]->TTReceipt ) ){ $hidepay = ' hidden '; }

        View::page( 'invoices/view', get_defined_vars() );
    }

    /**
     * View Invoice details
     *
     * @access private
     * @return na
     */
    public function viewold()
    {
        //accessible by Accounting only
        if( !User::can( "View Invoices" ) ){
            View::redirect( 'invoices' );
        }

        $id = $this->segment[2];
        if( empty( $id ) ){
            View::redirect( 'invoices' );
        }

        $hidepay = ' hidden ';
        $userinfo = User::info();
        if( isset( $userinfo->Code ) && ( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ) ){
            $hidepay = '';
            $clientModel = $this->load->model( 'clients', true, true );
            $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $clientModel->doSaveInvoicePayment( $cuid );
        }

        $invoice = $this->model->getDetails( $id );
        $investorData = User::info( false, $invoice[0]->UserID );
        $mybalance = apputility::getwalletbalance( $invoice[0]->UserID );
        $invoice->Logo = '<img src="'.View::asset( 'images/omegalogo.png' ).'">';
        if( isset( $invoice[0]->Status ) && $invoice[0]->Status == 'Paid' ){ $hidepay = ' hidden '; }
        if( isset( $invoice[0]->TTReceipt ) ){ $hidepay = ' hidden '; }

        View::page( 'invoices/viewold', get_defined_vars() );
    }

    /**
     * View Client's TT Confirmation
     *
     * @access private
     * @return na
     */
    public function receipt()
    {
        View::referrer();
        
        //accessible by Accounting only
        if( !User::can( "Manage Invoices" ) ){
            View::redirect();
        }
        
        $id = $this->segment[2];
        if( empty( $id ) ){
            View::redirect( 'invoices' );
        }

        $invoice = $this->model->getDetails( $id );
        $investorData = User::info( false, $invoice[0]->UserID );

        View::page( 'invoices/receipt', get_defined_vars() );
    }

    public function goprint()
    {
        $id = $this->segment[2];
        if( empty( $id ) ){
            View::redirect( 'invoices' );
        }

        $invoice = $this->model->getDetails( $id );
        $investorData = User::info( false, $invoice[0]->UserID );
        $logo = '<img src="'.View::asset( 'images/omegalogo.png' ).'">';

        View::page( 'invoices/print_view', get_defined_vars() );
    }

    public function goprintold()
    {
        $id = $this->segment[2];
        if( empty( $id ) ){
            View::redirect( 'invoices' );
        }

        $invoice = $this->model->getDetails( $id );
        $investorData = User::info( false, $invoice[0]->UserID );
        $invoice->Logo = '<img src="'.View::asset( 'images/omegalogo.png' ).'">';

        View::page( 'invoices/print_viewold', get_defined_vars() );
    }

    public function getbookings()
    {
        $clientid = $this->segment[2];
        $bookings = $this->model->getBookingsOwn( $clientid );
        header( "Content-type:application/json" );
        echo json_encode( $bookings );
    }

    public function getclient()
    {
        $clientid = $this->segment[2];
        $details = $this->model->getClientDetails( $clientid );
        header( "Content-type:application/json" );
        echo json_encode( $details );
        exit;
    }

    public function getbookdata()
    {
        $data = [];
        $investmentBookingID = $this->segment[2];
        $booking = $this->model->getBookedData( $investmentBookingID );
        $subsrate = ( isset( $booking->SubscriptionRate ) && floatval( $booking->SubscriptionRate ) > 0.00 ) ? number_format( $booking->SubscriptionRate, 2 ) : '0.5';
        // $data['commission'] = AppUtility::computeCommission( $booking->TotalAmountAttached, $subsrate );
        $data['commission'] = isset( $booking->TotalInvoiceAmount ) ? $booking->TotalInvoiceAmount : '0.00';
        $data['commission'] = str_ireplace( ',', '', $data['commission'] );
        $data['HTML'] = '<input type="hidden" class="commission" value="'.$data['commission'].'">';
        if( isset( $booking ) ){
            $data['HTML'] .= "<table>";
                $data['HTML'] .= "<tr>";
                    $data['HTML'] .= "<td>Investment Booking ID:</td>";
                    $data['HTML'] .= "<td style=\"padding-left:20px;\">";
                        $data['HTML'] .= "<input type=\"hidden\" class=\"booking-id\" name=\"group[InvestmentBookingID][]\" value=\"$investmentBookingID\">";
                        $data['HTML'] .= "$investmentBookingID";
                    $data['HTML'] .= "</td>";
                $data['HTML'] .= "</tr>";
                if( isset( $booking->Investor ) ){
                    $data['HTML'] .= "<tr>";
                        $data['HTML'] .= "<td>Investor Name:</td>";
                        $data['HTML'] .= "<td style=\"padding-left:20px;\">$booking->Investor</td>";
                    $data['HTML'] .= "</tr>";
                }
                if( isset( $booking->TotalAmountAttached ) ){
                    $data['HTML'] .= "<tr>";
                        $data['HTML'] .= "<td>Investment Amount:</td>";
                        $data['HTML'] .= "<td style=\"padding-left:20px;\">$".number_format( $booking->TotalAmountAttached, 2 )."</td>";
                    $data['HTML'] .= "</tr>";
                }
                $data['HTML'] .= "<tr>";
                    $data['HTML'] .= "<td>Subscription Rate: </td>";
                    $data['HTML'] .= "<td style=\"padding-left:20px;\">$subsrate%</td>";
                $data['HTML'] .= "</tr>";
            $data['HTML'] .= "</table>";
        }
        header( "Content-type:application/json" );
        echo json_encode( $data );
    }
}