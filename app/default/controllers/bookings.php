<?php
class Bookings extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::bookings();
    }

    public function index()
    {
        $this->setSession( 'lister', array( $this->controller, $this->controller ) );
        $userinfo = User::info();

        $status = isset( $this->segment[1] ) ? $this->segment[1] : false;

        $bookings = $this->model->getBookingsOwn( $userinfo->UserID );
        $totals = $this->model->getTotalsBookingsOwn( $userinfo->UserID );
        $projects = $this->model->getBookedCompanies( $userinfo->UserID );
        //$dataheaders = array( 'Avatar', 'BarColor', 'BookHTML', 'Percentage', 'ProjectClass', 'CompanyPhoto' );
        //AppUtility::processClientsData( $projects, $dataheaders );
        //$enabled = Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || User::is( 'Administrator' ) ? true : false;

        if( User::is( 'Administrator' ) ){
            $bookings = $this->model->getBookings();
            $totals = $this->model->getTotalsBookings();
        }

        $groupedbookings = AppUtility::groupBookingByStatus( $bookings );
        $projects = $this->model->getBookedCompanies();
        
        switch(strtolower($status)) {
            case "pending":
                $bookings = $groupedbookings['Pending'];
                View::page('bookings/pending', get_defined_vars());
            break;
            case "verified":
                $bookings = $groupedbookings['Verified'];
                View::page('bookings/verified', get_defined_vars());
            break;
            case "registered":
                $bookings = $groupedbookings['Interest'];
                View::page('bookings/registered', get_defined_vars());
            break;
            case "approved":
                $bookingss = $groupedbookings['Approved'];
                View::page('bookings/approved', get_defined_vars());
            break;
            default:
                $bookingss = $groupedbookings['Approved'];
                View::page('bookings/dashboard', get_defined_vars());
            break;
        }
    }

    /**
     * Get Booked Thank You Page
     * 
     * @access private
     * @return contents
     */
    public function thankyou()
    {
        $bookID = $this->segment[2];
        $bookdata = $this->model->getBookedData($bookID); 
        if( $bookdata ) {            
            View::page('bookings/thankyou', get_defined_vars());
        } else {
            View::redirect('users/dashboard');
        }    
    }

    public function downloadbookingdemandnote() 
    {
        $bookID = $this->segment[2];
        $bookdata = $this->model->getBookedData($bookID);
        
        // Start PDF
        $pdf = $this->load->model('pdfengine',true);
            
            // Demand Note PDF name
            $demandNoteName = $bookdata->UserID.'_'.$bookID.'_demandnote.pdf';
            $demandNotePdf = $pdf->buildBookingDemandNote($bookID,$demandNoteName,$bookdata);                
        // End PDF
            
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"$demandNoteName\""); 
        echo readfile($demandNotePdf);
    }
        
    /**
     * Get Booked page
     * 
     * @access private
     * @return contents
     */
    public function company()
    {
        View::referrer();
        $this->model->doSave();

        $bookdata = $this->model->getBookedData( $this->segment[2] );

        if( $bookdata->BookingStatus == 'Pending' && $bookdata->BookingType == 'Interest' ){
            View::redirect('users/dashboard');
        }

        $title = $bookdata->CompanyName;
        View::$title = 'View Booking';
        if( $bookdata->BookingUserID != User::info( 'UserID' ) ){
            $title = $bookdata->Name;
            View::$title = $bookdata->Name;
        }
        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $title );

        View::page('bookings/view_book', get_defined_vars());
    }

    /**
     * Get booking info page
     * 
     * @access private
     * @return contents
     */
    public function info()
    {
        View::referrer();        
        $this->model->doSave();

        $bookdata = $this->model->getBookedData( $this->segment[2] );

        if( $bookdata->BookingStatus == 'Pending' && $bookdata->BookingType == 'Interest' ){
            View::redirect('users/interest/'.$this->segment[2]);
        }

        $title = $bookdata->CompanyName;
        View::$title = 'View Booking';
        if( $bookdata->BookingUserID != User::info( 'UserID' ) ){
            $title = $bookdata->Name;
            View::$title = $bookdata->Name;
        }
        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $title );

        View::page('bookings/info', get_defined_vars());
    }

    /**
     * print a booking
     *
     * @access private
     * @return contents
     */
    public function goprint()
    {
        $id = $this->segment[2];

        if( empty( $id ) ){
            View::redirect( 'bookings' );
        }
        $bookdata = $this->model->getBookedData( $this->segment[2] );

        View::page( 'bookings/print_view', get_defined_vars() );
    }

    /**
     * Get booking interest page
     * 
     * @access private
     * @return contents
     */
    public function interest()
    {   
        View::referrer();
        $this->model->doSave();

        $bookdata = $this->model->getBookedData( $this->segment[2] );

        $title = $bookdata->CompanyName;
        View::$title = 'View Booking';
        if( $bookdata->BookingUserID != User::info( 'UserID' ) ){
            $title = $bookdata->Name;
            View::$title = $bookdata->Name;
        }
        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $title );

        if( $bookdata->BookingStatus == 'Pending' && $bookdata->BookingType == 'Interest' ){
            View::page('bookings/interest', get_defined_vars());
        }
    }

    /**
     * Company booking page
     * 
     * @access private
     * @return na
     */
    public function book()
    {
        if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || Option::get('global_promo') == 1 ) {

            $exemption = 0;
            $userinfo = User::info();
            $fullname = isset( $userinfo->FirstName ) ? $userinfo->FirstName.' ' : '';
            $fullname .= isset( $userinfo->LastName ) ? $userinfo->LastName : '';
            $clientdata = $this->model->getClientData( $this->segment[2] );

            if( $clientdata ) {
                $this->setSession( 'CLIENTINFO', array( $clientdata->CompanyName, $clientdata->ClientProfileID ) ); //use for forums

                $today = new DateTime( "now" );
                $opening = new DateTime( $clientdata->OfferOpening );
                $closing = new DateTime( $clientdata->OfferClosing );
                
                if( ( intval($clientdata->TotalBookingAmounts) >= $clientdata->TargetGoal && $clientdata->TypeOfOffer == 'Limited' ) || ( $opening > $today ||  $closing < $today ) ) {
                        View::redirect( 'bookings/register/'.$this->segment[2] );
                }

                $referrer = $this->getCookie( Config::get('EXEMPTIONKEY') ) ? $this->getCookie( Config::get('EXEMPTIONKEY') ) : 0;
                if( $referrer == $clientdata->ClientProfileID ){
                    $exemption = 1;
                }

                self::setCompanyLogoLink( $clientdata );
                
            } else { View::redirect( 'users/dashboard' ); }

            $bookID = $this->model->doSaveBooking();
            
            // Process redirection after submission
            if( $bookID ){
                $bookdata = $this->model->getBookedData( $bookID );
                $email = $this->load->model( 'emailengine', true );
                if( $bookdata->BookingType == 'Booking' ){
                    $email->sendBookingEmail( $clientdata, $bookdata );
                }

                View::redirect( 'bookings/thankyou/'.$bookID );
            }

            View::page( 'bookings/book', get_defined_vars() );

        } else { View::redirect( 'users/dashboard/' ); }
    }

    /**
     * Company booking register page
     * 
     * @access private
     * @return na
     */
    public function register()
    {
        if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || Option::get('global_promo') == 1 ){
            $exemption = 0;
            $userinfo = User::info();
            $fullname = isset( $userinfo->FirstName ) ? $userinfo->FirstName.' ' : '';
            $fullname .= isset( $userinfo->LastName ) ? $userinfo->LastName : '';
            $clientdata = $this->model->getClientData( $this->segment[2] );

            if( $clientdata ){
                $this->setSession( 'CLIENTINFO', array( $clientdata->CompanyName, $clientdata->ClientProfileID ) ); //use for forums

                $today = new DateTime( "now" );
                $closing = new DateTime( $clientdata->OfferClosing );
                $opening = new DateTime( $clientdata->OfferOpening );

                if( ( ( intval($clientdata->TotalBookingAmounts) < $clientdata->TargetGoal && $clientdata->TypeOfOffer == 'Limited' ) && $today > $opening && $today < $closing ) || ( $clientdata->TypeOfOffer == 'Open' && $today > $opening && $today < $closing ) ) {
                        View::redirect( 'bookings/book/'.$this->segment[2] );
                }

                $referrer = $this->getCookie( Config::get('EXEMPTIONKEY') ) ? $this->getCookie( Config::get('EXEMPTIONKEY') ) : 0;
                if( $referrer == $clientdata->ClientProfileID ){
                    $exemption = 1;
                }

                self::setCompanyLogoLink( $clientdata );

            } else { View::redirect( 'users/dashboard' ); }

            $bookID = $this->model->doSaveInterest();
            
            // Process redirection after submission
            if( $bookID ){
                $bookdata = $this->model->getBookedData( $bookID );
                $email    = $this->load->model( 'emailengine', true );
                $email->sendInterestEmail( $clientdata, $bookdata );
                View::redirect( 'bookings/interest/'.$bookID );                  
            }

            View::page( 'bookings/register', get_defined_vars() );

        } else { View::redirect( 'users/dashboard' ); }
    }

    /**
     * booking register page
     * 
     * @access private
     * @return na
     */
    public function book_register()
    {
        if( Apputility::investorinfo( 'InvestorStatus' ) != 'Approved' && Option::get('global_promo') == 0 && User::isLoggedIn()){
            View::redirect( 'users/dashboard' );
        }

        $bookID = $this->model->doSave();

        $clientdata = $this->model->getClientData( $this->segment[2] );
        if( $clientdata ){
            $this->setSession( 'CLIENTINFO', array( $clientdata->CompanyName, $clientdata->ClientProfileID ) ); //use for forums

            $today = new DateTime( "now" );
            $opening = new DateTime( $clientdata->OfferOpening );
            $clientdata->Button = $clientdata->TotalBookingAmounts >= $clientdata->TargetGoal && $clientdata->TypeOfOffer == 'Limited' ? "Register" : "Book Now";
            
            //change to Register button if the project is not open yet
            if( isset( $opening ) && $opening > $today ){
                $clientdata->Button = 'Register';
            }

            switch( $clientdata->Button ){
                case 'Book Now':
                    if( isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ){
                        $clientdata->Button = 'Book Again';                        
                    }
                    $clientdata->BookingType = 'Booking';
                    break;

                case 'Register':
                    if( isset( $clientdata->CurrentUserRegisterCount ) && $clientdata->CurrentUserRegisterCount > 0 ){
                        $clientdata->Button .= ' Again';                        
                    }
                    $clientdata->BookingType = 'Interest';
                    break;
                
                default: break;
            }
        } else {
            View::redirect( 'users/dashboard' );
        }
        
        // Process redirection after submission
        if( $bookID ){
            $book       = $this->post['book'];
            $bookdata   = $this->model->getBookedData( $bookID );
            $email      = $this->load->model( 'emailengine', true );
            switch( $book['BookingType'] ){
                case 'Booking':
                    $email->sendBookingEmail( $clientdata, $bookdata );
                    View::redirect( 'bookings/thankyou/'.$bookID.'/' );
                    break;
                case 'Interest':
                    $email->sendInterestEmail( $clientdata, $bookdata );
                    View::redirect( 'bookings/info/'.$bookID.'/' );
                    break;
            }
        }
        
        $exemption = 0;
        $referrer = ($this->getCookie(Config::get('EXEMPTIONKEY'))) ? $this->getCookie(Config::get('EXEMPTIONKEY')) : 0;
        if($referrer == $clientdata->ClientProfileID) {
            $exemption = 1;
        }

        View::page('bookings/register', get_defined_vars());        
    }

    public function rejectfile()
    {
        if(User::can('Manage Uploaded Documents')) {
            $fileID = $this->segment[2];
            $this->model->doRejectFile($fileID);
            View::redirect('bookings/info/'.$this->segment[3].'/');
        }
    }
    
    public function approvefile()
    {
        if(User::can('Manage Uploaded Documents')) {
            $bookID = $this->segment[3];
            $this->model->doApproveFile($this->segment[2], $bookID);
            View::redirect('bookings/info/'.$this->segment[3].'/');  
        }
    }
    
    public function deletefile()
    {
        if(User::can('Manage Uploaded Documents')) {
            $bookID = $this->segment[3];
            $this->model->doDeleteFile($this->segment[2], $bookID);
            View::redirect('bookings/info/'.$this->segment[3].'/'); 
        }
    }

    function setCompanyLogoLink( $cd )
    {
        $cd->Data['CompanyLogo'] = View::common()->getUploadedFiles( $cd->CompanyLogo );
        $slug = View::asset( isset( $cd->Data['CompanyLogo'][0]->FileSlug ) ? 'files'.$cd->Data['CompanyLogo'][0]->FileSlug : 'images/placeholder.png' );
        $link = $slug != '' ? $slug : View::asset( 'images/placeholder.png' );
        $cd->Data['CompanyLogoLink'] = '<img class="company-logo" src="'.$link.'">';

        return $cd;
    }

    function getbookingsbyproject()
    {
        $clientprofileid = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $userinfo = User::info(); // currently logged on - attempting to view someone's profile

        $bookings = $this->model->getBookingsOwnByProject( $clientprofileid, $userinfo->UserID );
        $companyname = isset( $bookings[0]->CompanyName ) ? $bookings[0]->CompanyName : '';

        echo '<div class="project-desc" style="height: 30px;">';
            echo '<h5><a href="'.View::url( 'project/view/'.$clientprofileid ).'"><b>'.$companyname.'</b> <span class="line-green"></span></a></h5>';
        echo '</div>';

        echo '<table id="tablebookings" class="table table-vcenter" width="100%">';
            echo '<thead>';
                echo '<tr>';
                    echo '<th class="text-center">Date</th>';
                    echo '<th class="text-center">Amount</th>';
                    echo '<th class="text-center">Action</th>';
                echo '</tr>';
            echo '</thead>';
            echo '<tbody>';

            if( isset( $bookings ) && count( $bookings ) ){
                foreach( $bookings as $booking ){
                    $createdat = isset( $booking->CreatedAt ) && $booking->CreatedAt != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $booking->CreatedAt ) ) : '-';
                    $amount = isset( $booking->TotalAmountAttached ) ? number_format( $booking->TotalAmountAttached, 2 ) : '0.00';
                    $url = View::url( 'bookings/info/'.$booking->InvestmentBookingID );
                    echo '<tr>';
                        echo '<td class="text-center">'.$createdat.'</td>';
                        echo '<td class="text-right">$'.$amount.'</td>';
                        echo '<td class="text-center"><a href="'.$url.'" class="dropdown-toggle btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="">View Booking</a></td>';
                    echo '</tr>';
                }
            } else {
                echo '<tr>';
                    echo '<td colspan="3">No data</td>';
                    echo '<td class="hidden"></td>';
                    echo '<td class="hidden"></td>';
                echo '</tr>';
            }
            echo '</tbody>';
        echo '</table>';

        echo '<script type="text/javascript">';
            echo '$( "#tablebookings" ).DataTable({ pageLength: 10, bSort: false, searching: false, lengthChange: false, pagingType: "full_numbers" })';
        echo '</script>';
    }
}