<?php
class Clients extends Controller 
{
    public function __construct()
    {
        parent::__construct();     
        
        $this->load->model('users', false, true);

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session

        Asset::client();
    }

    /**
     * Admin clients page
     * 
     * @access private
     * @return na
     */
    public function index()
    {
        //View::page('clients/grid_list', get_defined_vars());
        View::redirect();
    }
    
    /**
     * Activity logs page
     * 
     * @access private
     * @return rows
     */
    public function activitylogs()
    {
        if( User::can( 'View Logs' ) ){            
            $logs = $this->model->getActivityLogs();
            View::page( 'clients/activitylogs', get_defined_vars() );
        } else {
            View::redirect();
        }
    }

    /**
     * Add clients page
     * 
     * @access private
     * @return na
     */
    public function add()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){
            $userinfo = User::info();            
            View::page( 'clients/add_client', get_defined_vars() );
        } else { View::redirect(); }
    } 

    /**
     * Clients list of booked campaign
     *
     * @access private
     * @return na
     */
    public function booked()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){

            $userinfo = User::info();
            
            $this->model->doSave();
            $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $client = $this->model->getInfo( $cuid );
            $status = isset( $this->segment[2] ) ? $this->segment[2] : '';
            $title = 'Booked Users';

            switch( strtolower( $status ) ){
                case "pending":
                    $title .= ' (Pending)';
                    $bookings = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Pending' );
                    $bookingtotals = $this->model->getBookingsTotalsByStatus( $client->ClientProfileID, 'Pending' );
                    View::redirect('clients/booked');
                break;

                case "registeredinterest":
                    $title .= ' (Registered Interest)';
                    $bookings = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Pending', 'Interest' );
                    $bookingtotals = $this->model->getBookingsTotalsByStatus( $client->ClientProfileID, 'Pending', 'Interest' );
                break;

                default:
                    $title .= ' (Approved)';
                    $bookings = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Approved' );
                    $bookingtotals = $this->model->getBookingsTotalsByStatus( $client->ClientProfileID, 'Approved' );
                    if( isset( $bookings ) && count( $bookings ) ){
                        foreach( $bookings as $booking ){
                            $booking->TTReceiptURL = View::asset( 'files'.$booking->TTReceiptFileSlug );
                            switch( $booking->ReceiptOption ){
                                case 'TTPhoto':
                                    $booking->ReceiptBTN = '<a href="'.$booking->TTReceiptURL.'" class="html5lightbox text-muted"><i class="si si-picture fa-2x"></i></a>';
                                    break;
                                case 'Cheque':
                                    $booking->ReceiptBTN = '<a href="javascript:void();" class="text-muted" data-toggle="modal" data-target="#cheque-modal" BookingID="'.$booking->InvestmentBookingID.'" ChequeDrawer="'.$booking->ChequeDrawer.'" ChequeBank="'.$booking->ChequeBank.'" ChequeBranch="'.$booking->ChequeBranch.'" ChequeAmount="'.$booking->ChequeAmount.'" onclick="getCheque(this)"><i class="si si-credit-card fa-2x"></i></a>';
                                    break;
                            }
                        }
                    }
                break;
            }

            View::page('clients/booked', get_defined_vars());

        } else { View::redirect(); }
    }

    /**
     * Clients list of categories
     *
     * @access private
     * @return na
     */
    public function categories()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Categories' ) ){

            $userinfo = User::info();
            $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';

            switch( $segment2 ){
                case 'add':
                    $this->model->doSave();
                    View::page( 'clients/categories/add', get_defined_vars() );
                    break;

                case 'edit':
                    $this->model->doSave();
                    $catdata = $this->model->getClientCategoryByID( $this->segment[3] );
                    if( $catdata ){
                        View::page( 'clients/categories/edit', get_defined_vars() );
                    } else {
                        View::redirect( 'clients/categories' );
                    }                
                    break;

                case 'delete':
                    $category = $this->model->doClientCatDelete( $this->segment[3] );
                    View::redirect( 'clients/categories' );
                    break;

                default:
                    $categories = $this->model->getClientCategories();
                    View::page( 'clients/categories/list', get_defined_vars() );
                    break;
            }

        } else { View::redirect(); }
    }

    /**
     * Clients View Page
     * 
     * @access private
     * @return na
     */
    public function view()
    {
        $userinfo = User::info();
        $cuid = AppUtility::getClientUserID( $userinfo );
        $loggedClient = $this->model->getInfo( $cuid );
        $forumModel = $this->load->model( 'forums', true, true );
        $clientdata = $this->model->getClientData( $this->segment[2] );
        
        $enabledTabs = AppUtility::investorinfo( 'InvestorStatus' ) == 'Approved' || User::is( 'Administrator' ) || $loggedClient->ClientProfileID == $clientdata->ClientProfileID ? true : false;

        $disabled = isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ? '' : ' disabled ';
        $hidden = $disabled == '' ? '' : 'hidden';

        if( $clientdata ){
            $this->setSession( 'CLIENTINFO', array( $clientdata->CompanyName, $clientdata->ClientProfileID ) ); //use for forums

            $today = new DateTime( "now" );
            $opening = new DateTime( $clientdata->OfferOpening );
            $clientdata->Data = array( 'title' => '', 'button' => '', 'disabled' => '' );
            $title = ( $clientdata->TotalBookingAmounts >= $clientdata->TargetGoal && $clientdata->TypeOfOffer == 'Limited' ) ? "Register" : "Book";
            $title = ( isset( $opening ) && $opening > $today ) ? "Register" : $title;

            switch( $title ){
                case 'Book':
                    if( isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ){
                        $title = 'Book Again';
                    } else { $title = 'Book Now'; }
                    break;

                case 'Register':
                    if( isset( $clientdata->CurrentUserRegisterCount ) && $clientdata->CurrentUserRegisterCount > 0 ){
                        $title .= ' Again';
                    } else { $title = 'Register Now'; }
                    break;
                
                default: break;
            }

            $clientdata->Data['title'] = 'Click '.$title.' to Invest!';
            $clientdata->Data['button'] = $title;

            $documents = $this->model->getClientFiles($clientdata);

        } else {
            View::redirect( 'clients' );
        }
		
		$faqs = $this->model->getFaqs( $clientdata->ClientProfileID, NULL, $userinfo->UserID );
        $blog = $this->model->getBlogInfo( $clientdata->ClientProfileID );
        if( $blog ){
            $recentPosts = $this->model->getRecentPosts( $clientdata->ClientProfileID, $userinfo->UserID );
        }
        
        $this->model->doSave( $disabled );
        $newcomments = [];
		$comments = $this->model->getComments( $clientdata->ClientProfileID, $userinfo->UserID );
        if( isset( $comments ) && count( $comments ) ){
            foreach( $comments as $comment ){
                $comment->AvatarLink = '<img src="'.View::asset( isset( $comment->FileSlug ) ? 'files'.$comment->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';

                if( $comment->ParentID == 0 ){
                    $newcomments[$comment->CommentID] = $comment;
                    $newcomments[$comment->CommentID]->Children = [];
                } else {
                    array_push( $newcomments[$comment->ParentID]->Children, $comment );
                }
            }
        }

        //set the current user comment indicator
        $currUser = (object) array();
        $fi = View::common()->getUploadedFiles( $userinfo->Avatar );
        $currUser->AvatarLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';
        $currUser->AvatarReplyLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="25" height="25">';
        $currUser->AvatarLinkConverted = str_ireplace( '"', '&#34;', $currUser->AvatarLink );
        $currUser->Name = isset( $userinfo->FirstName ) ? $userinfo->FirstName : '';
        $currUser->Name .= isset( $userinfo->LastName ) ? ' '.$userinfo->LastName : '';
        $currUser->Level = isset( $userinfo->Level ) ? $userinfo->Level : '';
        $currUser->Indicator = '';
        switch( $userinfo->Level ){
            case '4': $currUser->Indicator = '<i class=&#34;fa clnt-icon fa-star&#34;></i>'; break;
            case '6': $currUser->Indicator = '<i class=&#34;fa ast-icon fa-circle text-warning&#34;></i>'; break;
            default: break;
        }

        $forums = $forumModel->getForumSummary( $clientdata->ClientProfileID );
        if( isset( $forums ) ){
            if( count( $forums ) ){
                foreach( $forums as $k => $v ){
                    foreach( $v as $post ){
                        if( isset( $post->LastPost ) ){
                            $lp = explode( '|', $post->LastPost );
                            $post->Data = array( 'PostedBy' => $lp[0], 'DatePosted' => $lp[1], 'FileSlug' => $lp[2], 'AvatarLink' => '' );
                            $post->Data['AvatarLink'] = '<img src="'.View::asset( isset( $post->Data['FileSlug'] ) ? 'files'.$post->Data['FileSlug'] : 'images/slide-3.jpg' ).'">';
                        }
                    }
                }
            }
        }

        $communities = $this->model->getCommunities( $clientdata->ClientProfileID );
        $teams = $this->model->getTeams( $clientdata->UserID, true );
        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $clientdata->CompanyName );

        AppUtility::updateLastView( $userinfo, $clientdata );

        View::page( 'clients/view', get_defined_vars() );
    }

    /**
     * Forums category page
     * 
     * @access private
     * @return na
     */
    public function category()
    {
        $userinfo = User::info();
        $forumModel = $this->load->model( 'forums', true, true );

        $clientdata = $this->model->getClientData( $this->segment[2] );

        $disabled = isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ? '' : ' disabled ';
        $hidden = $disabled == '' ? '' : 'hidden';

        $forum = array( 'catid' => '', 'subcatid' => '', 'scname' => '', 'forumid' => '' );
        $forum['catid'] = isset( $this->segment[2] ) ? $this->segment[2] : 0;
        $forum['subcatid'] = isset( $this->segment[3] ) ? $this->segment[3] : 0;
        $forum['scname'] = isset( $this->segment[4] ) ? $this->segment[4] : '';
        $forum['forumid'] = isset( $this->segment[5] ) ? $this->segment[5] : false;
        $forum['pinval'] = isset( $this->segment[6] ) ? $this->segment[6] : 0;

        if( $forum['forumid'] ){
            $forumModel->doSavePin( $forum );
        }

        $catid = isset( $this->segment[2] ) ? $this->segment[2] : 0;
        $subcatid = isset( $this->segment[3] ) ? $this->segment[3] : 0;
        $subcatname = isset( $this->segment[4] ) ? $this->segment[4] : '';
        $categories = $forumModel->getForumByCategory( $catid, $subcatid );

        if( isset( $categories ) && count( $categories ) ){
            foreach( $categories as $k => $v ){
                foreach( $v as $category ){
                    $category->AuthorData['AvatarLink'] = '<img src="'.View::asset( isset( $category->AuthorFileSlug ) ? 'files'.$category->AuthorFileSlug : 'images/slide-3.jpg' ).'">';
                    $category->PosterData['AvatarLink'] = '<img src="'.View::asset( isset( $category->LastPosterFileSlug ) ? 'files'.$category->LastPosterFileSlug : 'images/slide-3.jpg' ).'">';
                }
            }
        }

        //View::page( 'clients/view', get_defined_vars() );

        header( "Content-type:application/json" );
        echo json_encode( $categories );
    }

    /**
     * Clients dashboard page
     * 
     * @access private
     * @return na
     */
    public function dashboard()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){

            View::referrer();
             
            $userinfo = User::info();
            $cuid = AppUtility::getClientUserID( $userinfo );
            $client = $this->model->getInfo( $cuid );
            $projects =$this->model->getUserProjects();
            $forumModel = $this->load->model( 'forums', true, true );

            $totalFunds = $this->model->getTotalFundsReceived( $client->ClientProfileID );

            //$offer = filter_var($client->SizeOfOffer, FILTER_SANITIZE_NUMBER_INT);
            
            $percentFund = ( $client->TargetGoal > 0 ) ? floor( ( $totalFunds->TotalFundsReceived / $client->TargetGoal ) * 100 ) : 0;
            $percentFundFinal = ( $percentFund > 100 ) ? 100 : $percentFund;

            $pendingBooking = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Pending' );
            $pendingCount = count( $pendingBooking );

            $interests = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Pending', 'Interest' );
            $interestCount = count( $interests );

            $investors = $this->model->getInvestorsByStatus( $client->ClientProfileID, 'Approved' );
            $investorsCount = count( $investors );

            $verifiedFunds = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Verified' );
            $interestFunds = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Pending', 'Interest' );
            $verifiedFundsCount = isset( $verifiedFunds ) ? count( $verifiedFunds ) : '0';
            $interestFundsCount = isset( $interestFunds ) ? count( $interestFunds ) : '0';

            $invoicesNoTTReceipt = $this->model->getClientInvoicesWithNoTTReceipt( $client->ClientProfileID );
            $invoicesOverdue = $this->model->getClientInvoicesOverdue( $client->ClientProfileID );
            $invoicesNoTTReceiptCount = isset( $invoicesNoTTReceipt ) ? count( $invoicesNoTTReceipt ) : '0';
            $invoicesOverdueCount = isset( $invoicesOverdue ) ? count( $invoicesOverdue ) : '0';

            $affiliates = $this->model->getClientAffiliates( $userinfo->UserID );
            $totalAffiliates = count( $affiliates );

            $confirmedAffiliates = $this->model->getClientActiveAffiliates( $client->UserID );
            $totalConfirmed = count( $confirmedAffiliates );

            $booked = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Approved' );
            $totalBooked = count($booked);

            $this->model->doSave();
            $clientcomments = [];
            $clientcomment = $this->model->getComments( $client->ClientProfileID );
            if( isset( $clientcomment ) && count( $clientcomment ) ){
                foreach( $clientcomment as $comment ){
                    $comment->AvatarLink = '<img src="'.View::asset( isset( $comment->FileSlug ) ? 'files'.$comment->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';

                    if( $comment->ParentID == 0 ){
                        $clientcomments[$comment->CommentID] = $comment;
                        $clientcomments[$comment->CommentID]->Children = [];
                    } else {
                        array_push( $clientcomments[$comment->ParentID]->Children, $comment );
                    }
                }
            }

            //set the current user comment indicator
            $currUser = (object) array();
            $fi = View::common()->getUploadedFiles( $userinfo->Avatar );
            $currUser->AvatarLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';
            $currUser->AvatarReplyLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="25" height="25">';
            $currUser->AvatarLinkConverted = str_ireplace( '"', '&#34;', $currUser->AvatarLink );
            $currUser->Name = isset( $userinfo->FirstName ) ? $userinfo->FirstName : '';
            $currUser->Name .= isset( $userinfo->LastName ) ? ' '.$userinfo->LastName : '';
            $currUser->Level = isset( $userinfo->Level ) ? $userinfo->Level : '';
            $currUser->Indicator = '';
            switch( $userinfo->Level ){
                case '4': $currUser->Indicator = '<i class=&#34;fa clnt-icon fa-star&#34;></i>'; break;
                case '6': $currUser->Indicator = '<i class=&#34;fa ast-icon fa-circle text-warning&#34;></i>'; break;
                default: break;
            }

            //latest activities
            $activities = $this->model->getLatestActivitiesByTransaction( $client->ClientProfileID );

            //quick posting for forum
            $forumModel->doSaveTopic();
            $epigram = $forumModel->getLatestEpigram( $userinfo->UserID );
            $options = $forumModel->getForumCategoryForOptions( $client->ClientProfileID );
            $epigram = isset( $epigram->epigram ) ? $epigram->Epigram : '';
            $disabled = isset( $options ) && count( $options ) > 0 ? '' : 'disabled';

            View::page( 'clients/dashboard', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Clients profile page
     * 
     * @access private
     * @return na
     */
    public function profile()
    {
        View::referrer();

        if( User::is( 'Client' )
            || User::is( 'Assistants' )
            || User::can( 'Manage Profiles' )
            || User::can( 'Manage Bank Accounts' ) ){
             
            $userinfo = User::info(); // Load logged in User info
            $clientID = false;
            $ClientProfileID = '';
            switch( $userinfo->Code ){
                case 'CLN':
                case 'ASST':
                    $cuid = AppUtility::getClientUserID( $userinfo );
                    $client = $this->model->getInfo( $cuid );
                    $clientID = $client->ClientProfileID;
                    break;
                
                default:
                    $clientID = isset( $this->segment[3] ) ? $this->segment[3] : false;
                    $ClientProfileID = !$clientID ? '' : '/'.$clientID;
                    break;
            }

            // capture posted data
            if( $this->post ){
                //$this->model->doSaveAjax();
                $this->model->doSave();
                $this->setSession( "preview", $this->post['preview'] );
                View::redirect( 'clients/profile/'.$this->post['next'].$ClientProfileID );
            }
            $preview = View::getPreview();
            $viewee = $clientID ? 'Client' : '';
            $tab = isset( $this->segment[2] ) ? $this->segment[2] : 'company-info';
            //$tab  = User::can( 'Manage Bank Accounts' ) ? 'bank-accounts' : $tab;
            $client = $this->model->getInfo( false, $clientID ); // get client info
            $profiles = $this->model->getProfile( $client->UserID, true ); // get client profile info            
            $percentcomplete = AppUtility::profileCompleteness( $client->UserID, $viewee );

            $client->Disabled = ( isset( $client ) && $client->Status == 'Approved' ) ? ' disabled ' : '';
            $client->Disabled = $client->Disabled == '' ? '' : (User::can('Manage Profiles')?'':$client->Disabled);
            $client->ACCTDisabled = ( $userinfo->Code == 'ACCT' || $userinfo->Code == 'ADM' || User::can( 'Manage Bank Accounts' )) ? '' : ' disabled ';
            
            if( isset( $profiles ) ){
                foreach( $profiles as $profile ){
                    $profile->Disabled = $client->Disabled;
                }
            }

            $teams = $this->model->getTeams( $client->UserID, true );
            if( isset( $teams ) && count( $teams ) ){
                foreach( $teams as $team ){
                    $team->PhotoLink = '<img src="'.View::asset( isset( $team->FileSlug ) ? 'files'.$team->FileSlug : 'images/user.png' ).'">';
                    $team->AvatarLink = '<img class="img-avatar img-avatar96" src="'.View::asset( isset( $team->FileSlug ) ? 'files'.$team->FileSlug : 'images/user.png' ).'">';
                    $data = array(
                        'ClientTeamID' => isset( $team->ClientTeamID ) ? $team->ClientTeamID : '0',
                        'ClientUserID' => isset( $team->ClientUserID ) ? $team->ClientUserID : '0',
                        'Name' => isset( $team->Name ) ? $team->Name : '',
                        'Position' => isset( $team->Position ) ? $team->Position : '',
                        'Avatar' => isset( $team->AvatarLink ) ? $team->AvatarLink : '',
                        'Bio' => isset( $team->Bio ) ? $team->Bio : '',
                        'Photo' => isset( $team->Photo ) ? $team->Photo : '0',
                        'PhotoLink' => $team->PhotoLink
                    );
                    $team->Data = str_ireplace( '"', '&quot;', json_encode( $data ) );
                }
            }

            $info = '';
            $notice = '';
            $approvehide = '';
            $changelink = View::url( 'clients/changeprofile/'.$tab );
            $title = 'Company Profile<br><small class="text-muted">('.$client->CompanyName.')</small>';
            if( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ){
                $title = View::$title;
                if( $client->Status == 'Approved' && $tab != 'teams' ){
                    $approvehide = ' hidden ';
                    $info = '<span class="text-warning">Your account is already</span> <span class="green"><b>APPROVED</b></span><span class="text-warning">. Any changes will not be saved except for Team Members! Click "Request Profile Update" button to request for Profile Update.</span>';

                    $info = '<div class="alert alert-warning fade in">';
                        $info .= 'Your account is already</span> <span class="green"><b>APPROVED</b></span><span class="text-warning">. Any changes will not be saved except for Team Members! Click <a href="'.$changelink.'" class="text-uppercase" data-toggle="tooltip">"Request Profile Update"</a> button to request for Profile Update.';
                    $info .= '</div>';
                }
                if( $client->Status != 'Approved' && ( $tab == 'company-info' || $tab == 'summary' ) ){
                    $notice = '<div class="alert alert-danger fade in">';
                        // $notice .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                        $notice .= 'NOTICE: Please limit your image file size to smallest as possible to avoid saving of incomplete base64 image code. Or the image might not be saved at all. If possible also to lessen the number of images.';
                    $notice .= '</div>';
                }
            }

            $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $client->CompanyName );
            View::page( 'clients/profile', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Clients change profile request page
     * 
     * @access private
     * @return na
     */
    public function changeprofile()
    {
        View::referrer();

        if( User::is( 'Client' )
            || User::is( 'Assistants' )
            || User::can( 'Manage Profiles' )
            || User::can( 'Manage Bank Accounts' ) ){
             
            $userinfo = User::info(); // Load logged in User info

            $client = [];
            $redirect = false;
            $clientID = false;
            $ClientProfileID = '';
            switch( $userinfo->Code ){
                case 'CLN':
                case 'ASST':
                    $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
                    $client = $this->model->getInfo( $cuid );
                    $clientID = $client->ClientProfileID;
                    $redirect = true;
                    break;
                
                default:
                    $clientID = isset( $this->segment[3] ) ? $this->segment[3] : false;
                    $ClientProfileID = !$clientID ? '' : '/'.$clientID;
                    break;
            }

            // capture posted data
            $next = false;
            if( $this->post ){
                $client = $this->model->getInfo( false, $clientID ); // get client info
                $next = isset( $this->post['next'] ) ? $this->post['next'] : false;
                $next = $next == 'teams' ? false : $next;
                $requesta = isset( $client->ChangeRequest ) && strlen( $client->ChangeRequest ) > 0 ? json_decode( $client->ChangeRequest, true ) : array( 'UpdatedAt' => date('Y-m-d H:i:s') );
                $this->model->doSaveChangeProfile( $requesta, $client->ClientProfileID );
                if( $redirect ) View::redirect( 'clients/changeprofile/'.$next.$ClientProfileID );
            }

            $tab  = isset( $this->segment[2] ) ? $this->segment[2] : 'company-info';
            $tab = $tab == 'teams' ? 'company-info' : $tab;
            // $tab  = $next ? $next : $tab;
            //$tab  = User::can( 'Manage Bank Accounts' ) ? 'bank-accounts' : $tab;
            $client = $this->model->getInfo( false, $clientID ); // get client info
            $profiles = $this->model->getProfile( $client->UserID, true ); // get client profile info          
            $client->Disabled = '';
            $client->ACCTDisabled = '';

            $changeRequests = $this->model->getProfileChangeRequest( $client->ClientProfileID ); // get data from request table
            if( isset( $changeRequests ) ){
                foreach( $changeRequests as $k => $v ){
                    $client->$k = $v->FieldValue;
                }
            }

            // $requesta = isset( $client->ChangeRequest ) && strlen( $client->ChangeRequest ) > 0 ? json_decode( $client->ChangeRequest ) : false;
            // if( $requesta ){
            //     foreach( $requesta as $key => $sections ){
            //         if( is_object( $sections ) ){
            //             foreach( $sections as $k => $v ){
            //                 $client->$k = $v;
            //             }
            //         } else { $client->$key = $sections; }
            //     }
            // }
            // if( isset( $requesta ) ){
            //     foreach( $requesta as $k => $v ){
            //         $client->$k = $v;
            //     }
            // }

            if( isset( $profiles ) ){
                foreach( $profiles as $profile ){
                    $profile->Disabled = $client->Disabled;
                }
            }

            $notice = '';
            $title = 'Company Profile<br><small class="text-muted">('.$client->CompanyName.')</small>';
            if( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ){
                $title = View::$title;
                if( $tab == 'company-info' || $tab == 'summary' ){
                    $notice = '<div class="alert alert-danger fade in">';
                        // $notice .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                        $notice .= 'NOTICE: Please limit your image file size to smallest as possible to avoid saving of incomplete base64 image code. Or the image might not be saved at all. If possible also to lessen the number of images.';
                    $notice .= '</div>';
                }
            }
            
            $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $client->CompanyName );
            View::page( 'clients/changeprofile', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Client profile upload method
     * 
     * @access private
     * @return NULL
     */
    public function profileFileUpload()
    {
        $this->model->doFileUploadAjax();
        return;
    }

    /**
     * Clients investors page
     *
     * @access private
     * @return na
     */
    public function investors()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){
             
            $userinfo = User::info(); // Load logged in User info
            $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $clientProfile = $this->model->getInfo( $cuid );

            //get investors
            $investors = $this->model->getClientInvestors( $clientProfile->UserID );

            View::page( 'clients/investors', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Clients Invoices page
     *
     * @access private
     * @return na
     */
    public function invoices()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){

            $userinfo = User::info(); // Load logged in User info             

            $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $client = $this->model->getInfo( $cuid );

            $segment2 = ( isset( $this->segment[2] ) ) ? $this->segment[2] : '';

            // if( $segment2 == 'uploadtt' ){
            //     $this->model->doSave();

            //     $invoiceID = $this->segment[3];
            //     $invoice = $this->model->getClientInvoiceDetails( $invoiceID );
            //     View::page( 'clients/invoices_uploadtt', get_defined_vars() );
            // } else {
            //     //get invoices
            //     $invoices = $this->model->getClientInvoices( $client->ClientProfileID );
            //     $invoicesOutstanding = $this->model->getClientInvoicesByStatus( $client->ClientProfileID,'Outstanding' );
            //     $invoicesPaid = $this->model->getClientInvoicesByStatus( $client->ClientProfileID,'Paid' );
            //     $invoicesOverdue = $this->model->getClientInvoicesOverdue( $client->ClientProfileID,'Paid' );
            //     $invoicesTTReceipt = $this->model->getClientInvoicesByStatus( $client->ClientProfileID,'TT Receipt' );

            //     $invoicesTTReceiptAll = $this->model->getClientInvoicesWithTTReceipt( $client->ClientProfileID );

            //     $totalPaid = 0;
            //     $totalTTReceipt = 0;

            //     View::page( 'clients/invoices', get_defined_vars() );
            // }

            $title = 'Invoices';
            switch( strtolower( $segment2 ) ){
                case "uploadtt":
                    $title .= ' (Upload TT Receipt)';
                    $this->model->doSave();

                    $invoiceID = $this->segment[3];
                    $invoice = $this->model->getClientInvoiceDetails( $invoiceID );
                    View::page( 'clients/invoices_uploadtt', get_defined_vars() );
                break;

                case "nott":
                    $title .= ' (No TT Receipt)';
                    $invoices = $this->model->getClientInvoicesWithNoTTReceipt( $client->ClientProfileID );
                    View::page( 'clients/invoices', get_defined_vars() );
                break;
                
                default:
                    //get invoices
                    $this->model->doSaveInvoicePayment( $cuid );
                    $mybalance = apputility::getwalletbalance( $cuid );
                    $invoices = $this->model->getClientInvoices( $client->ClientProfileID );
                    $invoicesOutstanding = $this->model->getClientInvoicesByStatus( $client->ClientProfileID, 'Outstanding' );
                    $invoicesPaid = $this->model->getClientInvoicesByStatus( $client->ClientProfileID, 'Paid' );
                    $invoicesOverdue = $this->model->getClientInvoicesOverdue( $client->ClientProfileID, 'Paid' );
                    $invoicesTTReceipt = $this->model->getClientInvoicesByStatus( $client->ClientProfileID, 'TT Receipt' );

                    if( isset( $invoicesOutstanding ) && count( $invoicesOutstanding )){
                        foreach( $invoicesOutstanding as $out ){
                            $data = array(
                                'InvoiceID' => isset( $out->InvoiceID ) ? $out->InvoiceID : '0',
                                'InvoiceType' => isset( $out->InvoiceType ) ? $out->InvoiceType : '',
                                'InvoiceDate' => isset( $out->InvoiceDate ) && $out->InvoiceDate != "0000-00-00" ? date( 'j M Y', strtotime( $out->InvoiceDate ) ) : "-",
                                'DueDate' => isset( $out->DueDate ) && $out->DueDate != "0000-00-00" ? date( 'j M Y', strtotime( $out->DueDate ) ) : "-",
                                'Investor' => isset( $out->Investor ) ? $out->Investor : '',
                                'InvestmentBookingID' => isset( $out->InvestmentBookingID ) ? $out->InvestmentBookingID : '0',
                                'InvestmentAmount' => isset( $out->InvestmentAmount ) ? number_format( $out->InvestmentAmount, 2 ) : '0.00', 
                                'Total' =>  isset( $out->Total ) ? number_format( $out->Total, 2 ) : '0.00'
                            );
                            $out->PayData = str_ireplace( '"', '&quot;', json_encode( $data ) );
                        }
                    }

                    if( isset( $invoicesOverdue ) && count( $invoicesOverdue )){
                        foreach( $invoicesOverdue as $over ){
                            $data = array(
                                'InvoiceID' => isset( $over->InvoiceID ) ? $over->InvoiceID : '0',
                                'InvoiceType' => isset( $over->InvoiceType ) ? $over->InvoiceType : '',
                                'InvoiceDate' => isset( $over->InvoiceDate ) && $over->InvoiceDate != "0000-00-00" ? date( 'j M Y', strtotime( $over->InvoiceDate ) ) : "-",
                                'DueDate' => isset( $over->DueDate ) && $over->DueDate != "0000-00-00" ? date( 'j M Y', strtotime( $over->DueDate ) ) : "-",
                                'Investor' => isset( $over->Investor ) ? $over->Investor : '',
                                'InvestmentBookingID' => isset( $over->InvestmentBookingID ) ? $over->InvestmentBookingID : '0',
                                'InvestmentAmount' => isset( $over->InvestmentAmount ) ? number_format( $over->InvestmentAmount, 2 ) : '0.00', 
                                'Total' =>  isset( $over->Total ) ? number_format( $over->Total, 2 ) : '0.00'
                            );
                            $over->PayData = str_ireplace( '"', '&quot;', json_encode( $data ) );
                        }
                    }

                    $invoicesTTReceiptAll = $this->model->getClientInvoicesWithTTReceipt( $client->ClientProfileID );
                    $totalPaid = 0;
                    $totalTTReceipt = 0;

                    View::page( 'clients/invoices', get_defined_vars() );
                break;
            }

        } else { View::redirect(); }
    }

    /**
     * View TT Receipt page
     *
     * @access private
     * @return na
     */
    public function viewttreceipt()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){

            $userinfo = User::info(); // Load logged in User info             

            $investmentBookingID = $this->segment[2];
            if( empty( $investmentBookingID ) ){
                View::redirect( '/clients/investors' );
            }

            $TTReceiptPhotoID = $this->segment[3];
            if( empty( $TTReceiptPhotoID ) ){
                View::redirect( '/clients/investors' );
            }

            $fileReceipt = $this->getUploadedFiles( $TTReceiptPhotoID );

            View::page( 'clients/view_ttreceipt', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Clients Funds page
     * list of booked fund
     * @access private
     * @return na
     */
    public function funds()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){
        
            $userinfo = User::info(); // Load logged in User info             

            $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
            $client = $this->model->getInfo( $cuid );

            $segment2 = ( isset( $this->segment[2] ) ) ? $this->segment[2] : '';

            switch( $segment2 ){
                case 'view':
                    $bookID = $this->segment[3];
                    $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
                    if( !$bookdata ){
                        View::redirect( 'clients/funds' );
                    }
                    $hide = '';
                    $bookdata->Data['SOComm'] = AppUtility::computeCommission( $bookdata->TotalAmountAttached, $bookdata->SubscriptionRate );
                    $exempted = isset( $bookdata->ExemptedCommission ) ? $bookdata->ExemptedCommission : '';
                    $exempted = $exempted == 1 ? 'hidden' : '';
                    View::page( 'clients/funds_view', get_defined_vars() );
                    break;
                
                case 'acknowledge':
                    $bookID = $this->segment[3];
                    $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
                    if( !$bookdata ){
                        View::redirect( 'clients/funds' );
                    }
                    $result = $this->model->markBookedData( $bookID, $client->ClientProfileID );

                    if( $result ){
                        $bookdata = $this->model->getBookedData( $bookID );
                        $bookeddata = $this->model->getBookedItemData( $bookID );

                        // exempted investor is determined by SubscriptionRate = 0
                        if( isset( $bookeddata ) && $bookdata->ExemptedCommission == 0 ){
                            $createInvoice = $this->model->createAcknowledgeInvoice( $bookdata );
                        }

                        $email = $this->load->model( 'emailengine', true );
                        $email->sendBookingAknowledgeEmail( $bookeddata );
                        if( $createInvoice ){
                            $email->sendBookingAknowledgeInvoiceEmail( $createInvoice, $bookdata, false );
                        }
                    }
                    View::redirect( 'clients/funds' );
                    break;
                
                case 'interestaccept':
                    $bookID = $this->segment[3];
                    $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
                    if( !$bookdata ){
                        View::redirect( 'clients/booked' );
                    }
                    $result = $this->model->markInterestBookedData( $bookID, $client->ClientProfileID );

                    if( $result ){
                        $bookeddata = $this->model->getBookedItemData( $bookID );
                        $email = $this->load->model( 'emailengine', true );
                        $email->sendInterestAcceptedEmail( $bookeddata );
                    }
                    View::redirect('clients/booked');
                    break;
                
                case 'pdf':
                    $bookID = $this->segment[3];
                    $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
                    if( !$bookdata ){
                        View::redirect( 'clients/funds' );
                    }
                    $hide = '';
                    $bookdata->Data['SOComm'] = AppUtility::computeCommission( $bookdata->TotalAmountAttached, $bookdata->SubscriptionRate );
                    View::page( 'clients/funds_view_pdf', get_defined_vars() );
                    break;
                
                case 'goprint':
                    $bookID = $this->segment[3];
                    $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
                    if( !$bookdata ){
                        View::redirect( 'clients/funds' );
                    }
                    $hide = '';
                    $bookdata->Data['SOComm'] = AppUtility::computeCommission( $bookdata->TotalAmountAttached, $bookdata->SubscriptionRate );
                    View::page( 'clients/funds_view_print', get_defined_vars() );
                    break;
                
                default:
                    //get bookings
                    //$bookedFunds = $this->model->getBookingsOwn($client->ClientProfileID);
                    //$bookingsApproved = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Approved' );
                    $bookings = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Verified' );
                    $totals = $this->model->getTotalsBookingsOwn( $client->ClientProfileID );
                    if( isset( $bookings ) && count( $bookings ) ){
                        foreach( $bookings as $booking ){
                            $booking->Data['SOComm'] = AppUtility::computeCommission( $booking->TotalAmountAttached, $booking->SubscriptionRate );
                        }
                    }
                    View::page( 'clients/funds', get_defined_vars() );
                    break;
            }

            // if( $segment2 == 'view' ){
            //     $bookID = $this->segment[3];
            //     $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
            //     if( !$bookdata ){
            //         View::redirect( 'clients/funds' );
            //     }
            //     $hide = '';
            //     $bookdata->Data['SOComm'] = AppUtility::computeCommission( $bookdata->TotalAmountAttached, $bookdata->SubscriptionRate );

            //     View::page( 'clients/funds_view', get_defined_vars() );

            // } elseif( $segment2 == 'acknowledge' ){
            //     $bookID = $this->segment[3];
            //     $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
            //     if( !$bookdata ){
            //         View::redirect( 'clients/funds' );
            //     }
            //     $result = $this->model->markBookedData( $bookID, $client->ClientProfileID );

            //     if( $result ){
            //         $bookdata = $this->model->getBookedData( $bookID );
            //         $bookeddata = $this->model->getBookedItemData( $bookID );

            //         // exempted investor is determined by SubscriptionRate = 0
            //         if( isset( $bookeddata ) && $bookdata->SubscriptionRate > 0 ){
            //             $createInvoice = $this->model->createAcknowledgeInvoice( $bookdata );
            //         }

            //         $email = $this->load->model( 'emailengine', true );
            //         $email->sendBookingAknowledgeEmail( $bookeddata );
            //         if( $createInvoice ){
            //             $email->sendBookingAknowledgeInvoiceEmail( $createInvoice, $bookdata );
            //         }
            //     }

            //     View::redirect( 'clients/funds' );

            // } elseif( $segment2 == 'interestaccept' ){
            //     $bookID = $this->segment[3];
            //     $bookdata = $this->model->getBookedData( $bookID, $client->ClientProfileID );
            //     if( !$bookdata ){
            //         View::redirect( 'clients/booked' );
            //     }
            //     $result = $this->model->markInterestBookedData( $bookID, $client->ClientProfileID );

            //     if( $result ){
            //         $bookeddata = $this->model->getBookedItemData( $bookID );
            //         $email = $this->load->model( 'emailengine', true );
            //         $email->sendInterestAcceptedEmail( $bookeddata );
            //     }

            //     View::redirect('clients/booked');

            // } else {
            //     //get bookings
            //     //$bookedFunds = $this->model->getBookingsOwn($client->ClientProfileID);
            //     //$bookingsApproved = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Approved' );
            //     $bookings = $this->model->getBookingsByStatus( $client->ClientProfileID, 'Verified' );
            //     $totals = $this->model->getTotalsBookingsOwn( $client->ClientProfileID );

            //     if( isset( $bookings ) && count( $bookings ) ){
            //         foreach( $bookings as $booking ){
            //             $booking->Data['SOComm'] = AppUtility::computeCommission( $booking->TotalAmountAttached, $booking->SubscriptionRate );
            //         }
            //     }

            //     View::page( 'clients/funds', get_defined_vars() );
            // }

        } else { View::redirect(); }
    }

    /**
     * Clients Affiliates page
     *
     * @access private
     * @return na
     */
    public function affiliates()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Clients' ) ){
             
            $userinfo = User::info(); // Load logged in User info

            $segment2 = ( isset( $this->segment[2] ) ) ? $this->segment[2] : '';
            $segment3 = ( isset( $this->segment[3] ) ) ? $this->segment[3] : '0';

            if( $segment2 == 'view' ){
                //$update = $this->model->doSave();
               // $affiliate = $this->model->getAffiliateDetails($segment3);
                View::redirect( 'clients/affiliates' );
            } else {
                //list page
                //get Affiliates
                $affiliates = $this->model->getClientAffiliates( $userinfo->UserID );
            }

            View::page( 'clients/affiliates', get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Clients Forums
     * This is for public view
     *
     * @access private
     * @return na
     */
    public function forums()
    {
        View::redirect( 'forums/dashboard' );
    }

    /**
     * Clients Blog
     * This is for public view
     *
     * @access private
     * @return na
     */
    public function blog()
    {
        $userinfo = AppUtility::validateBlogAccess();        
        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $link = 'clients/blog/publiclist';
        $title = 'Blog';
        if( $segment2 ){
            $cuid = AppUtility::getClientUserID();
            $client = $this->model->getInfo( $cuid );
            $this->setSession( 'CLIENTINFO', array( $userinfo->CompanyName, $userinfo->ClientProfileID ) );
            switch( $segment2 ){
                case 'view':
                    //show single article
                    $title .= ': '.$userinfo->CompanyName;   
                    $link = 'clients/blog/view';
                    $blogID = isset( $this->segment[4] ) ? $this->segment[4] : 0;

                    //get latest articles
                    $blogdata = $this->model->getBlogPostInfo( $blogID );
                    $categories = $this->model->getCategories( $blogdata->ClientProfileID );
                    $recentPosts = $this->model->getRecentPosts( $blogdata->ClientProfileID );
                    $client = AppUtility::getClientProfileInfoByClientProfileID( $blogdata->ClientProfileID );
                    $owner = ( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ) ? true : false;

                    // restrict non-bookers to access client's private blogs
                    if( $userinfo->Bookings == 0 && $blogdata->PublicView == 0 ) View::redirect( 'clients/blog' );
                    break;

                case 'preview':
                    $link = 'clients/blog/preview';
                    $blogID = isset( $this->segment[3] ) ? $this->segment[3] : 0;

                    //get latest articles
                    $blogdata = $this->model->getBlogPostInfo( $blogID );
                    $previewa = isset( $blogdata->Preview ) && strlen( $blogdata->Preview ) > 0 ? json_decode( $blogdata->Preview, true ) : false;
                    if( $previewa ){
                        foreach( $previewa as $k => $v ){
                            $blogdata->$k = $v;
                        }
                    }
                    $this->model->doSavePreview( $previewa );

                    $categories = $this->model->getCategories( $blogdata->ClientProfileID );
                    $recentPosts = $this->model->getRecentPosts( $blogdata->ClientProfileID );
                    $client = AppUtility::getClientProfileInfoByClientProfileID( $blogdata->ClientProfileID );
                    $owner = ( $userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ) ? true : false;
                    break;

                case 'manage':
                    $link = 'clients/blog/list';
                    $blogPosts = $this->model->getBlogs( $userinfo->ClientProfileID, 'Published' );
                    break;

                case 'add':
                    if( !User::can( 'Manage Clients' ) ){ View::redirect(); }
                    $link = 'clients/blog/add';
                    $update = $this->model->doSave();
                    $categories = $this->model->getCategories( $userinfo->ClientProfileID );
                    break;

                case 'edit':
                    $link = 'clients/blog/edit';
                    $blogID = isset( $this->segment[3] ) ? $this->segment[3] : 0;
                    $update = $this->model->doSave();
                    $blogdata = $this->model->getBlogPostInfo( $blogID );
                    $categories = $this->model->getCategories( $userinfo->ClientProfileID );
                    break;

                case 'delete':
                    if( User::is( 'Client' ) ){
                        $blogPostID = $this->segment[3];                        
                        $blogdata = $this->model->getBlogPostInfo( $blogPostID );
                        $res = $this->model->doDeleteBlogPost( $blogPostID );
                        AppUtility::deleteFileAndAssociates( $blogdata->FeaturedImage );
                        $this->setSession( 'message', "Blog post ID#$blogPostID has been deleted!" );
                    }
                    View::redirect( '/clients/blog/manage' );
                    break;
                
                default:
                    //show whole blog of client
                    if( View::clientinfo() ){
                        $category = isset ( $this->segment[2] ) ? $this->segment[2] : false;
                        $access = $userinfo->Bookings > 0 ? false : '1';
                        if( isset( $category ) ){
                            $blogs = $this->model->getBlogs( View::clientinfo()[1], 'Published', $access, $category );
                        } else {
                            $blogs = $this->model->getBlogs( View::clientinfo()[1], 'Published', $access );
                        }
                        if( isset( $blogs ) && count( $blogs ) ){
                            foreach( $blogs as $blog ){
                                $titleslug = str_ireplace( ' ', '-', strtolower( $blog->BlogTitle) );
                                $titleslug = str_ireplace( '?', '', $titleslug );
                                $blog->Data['FImage'] = View::common()->getUploadedFiles( $blog->FeaturedImage );
                                $blog->Link = View::url( 'clients/blog/view/'.$titleslug.'/'.$blog->BlogID );
                            }
                        }

                    } else { View::redirect(); }
                    break;
            }
            View::page( $link, get_defined_vars() );

        } else {
            //show whole blog of client
            if( View::clientinfo() ){
                $access = $userinfo->Bookings > 0 ? false : '1';
                $blogs = $this->model->getBlogs( View::clientinfo()[1], 'Published', $access );
                if( isset( $blogs ) && count( $blogs ) ){
                    foreach( $blogs as $blog ){
                        $titleslug = str_ireplace( ' ', '-', strtolower( $blog->BlogTitle) );
                        $titleslug = str_ireplace( '?', '', $titleslug );
                        $blog->Data['FImage'] = View::common()->getUploadedFiles( $blog->FeaturedImage );
                        $blog->Link = View::url( 'clients/blog/view/'.$titleslug.'/'.$blog->BlogID );
                    }
                }
                View::page( $link, get_defined_vars() );

            } else { View::redirect(); }
        }
    }

    /**
     * Clients Faq
     * This is for public view
     *
     * @access private
     * @return na
     */
    public function faq()
    {
        if( empty( $this->segment[2] ) ){
            View::redirect( User::dashboardLink( true ) );
        }
        
        $userinfo = User::info();
        $action = isset( $this->segment[2] ) ? $this->segment[2] : 'manage';
        $faqsid = isset( $this->segment[3] ) ? $this->segment[3] : false;
        $cuid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
        $currvw = 'clients/faq/list';
        $client = $this->model->getInfo( $cuid );

        $update = $this->model->doSaveFaq();

        switch( strtolower( $action ) ){
            case 'add':
                $currvw = 'clients/faq/add';
                break;

            case 'edit':                    
                $faqPost = $this->model->getFaq( $faqsid );            
                $currvw = 'clients/faq/edit';
                break;

            case 'delete':
                if( $faqsid ){
                    if( User::is( 'Client' ) ){
                        $this->model->doDeleteFaqPost( $faqsid );
                        $this->setSession( 'message', "FAQ has been deleted!" );
                    }
                    View::redirect( 'clients/faq/list' );
                } break;

            default:
                $jdata = [];
                $faqPosts = $this->model->getFaqs( $client->ClientProfileID );
                if( isset( $faqPosts ) && count( $faqPosts ) ){
                    foreach( $faqPosts as $faq ){
                        $jdata[$faq->FaqID] = array( 'Question' => $faq->FaqTitle, 'Answer' => $faq->FaqContent );
                    }
                }
                $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );
                break;
        }
        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Clients Assistant
     * This is for public view
     *
     * @access public
     * @return na
     */
    public function assistant()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Assistants' ) ){
        
            if( empty( $this->segment[2] ) ){
                View::redirect( User::dashboardLink( true ) );
            }

            $this->setSession( 'lister', array( $this->controller, $this->model, 'Manage Assistants' ) );
            
            $userinfo = User::info();
            Auth::userSession(); // Continue if user has session
            $action = isset( $this->segment[2] ) ? $this->segment[2] : 'manage';
            $asstid = isset( $this->segment[3] ) ? $this->segment[3] : false;
            $currvw = 'clients/assistant/asst_list';
            $client = $this->model->getInfo( $userinfo->UserID );

            //$update = $this->model->doSaveFaq();

            switch( strtolower( $action ) ){
                case 'add':
                    $currvw = 'clients/assistant/asst_add';
                    $userID = $this->model->doSaveAssistant( $client->UserID );

                    if( $userID ){
                        $userInfo = AppUtility::getUser( $userID );

                        // Start Email sending
                        $email = $this->load->model( 'emailengine', true );
                        $email->sendUserRegistration( $userID, $userInfo );
                    }
                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Add Assistant Manager' );
                    break;

                case 'edit':
                    $currvw = 'clients/assistant/asst_edit';
                    $this->model->doSaveAssistant( $client->UserID );
                    $asst = AppUtility::getUser( $asstid, true );
                    $dataheaders = array( 'Avatar', 'AvatarLink' );
                    AppUtility::processClientsData( $asst, $dataheaders );

                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $asst[0]->FirstName.' '.$asst[0]->LastName );
                    break;

                case 'trash':
                    $this->model->doTrashAssistant( $asstid );
                    $assistants = $this->model->getAssistants( $client->UserID );
                    $dataheaders = array( 'Avatar', 'AvatarLink' );
                    AppUtility::processClientsData( $assistants, $dataheaders );
                    break;

                case 'delete':
                    $currvw = 'clients/assistant/asst_trashbin';
                    $this->model->doDeleteAssistant( $asstid );
                    $assistants = $this->model->getAssistants( $client->UserID );
                    $dataheaders = array( 'Avatar', 'AvatarLink' );
                    AppUtility::processClientsData( $assistants, $dataheaders );
                    break;

                case 'restore':
                    $this->model->doRestoreAssistant( $asstid );
                    $assistants = $this->model->getAssistants( $client->UserID );
                    $dataheaders = array( 'Avatar', 'AvatarLink' );
                    AppUtility::processClientsData( $assistants, $dataheaders );
                    break;

                case 'trashbin':
                    $currvw = 'clients/assistant/asst_trashbin';
                    if( User::can( 'Delete Assistant') ){
                        if( isset( $this->post['uids'] ) ){
                            $this->model->doEmptyTrashAssistant( $this->post['uids'] );
                        }                    
                        $users = $this->model->getTrashedAssistants( $client->UserID );
                        $dataheaders = array( 'Avatar' );
                        AppUtility::processClientsData( $users, $dataheaders );
                    }
                    break;

                default:
                    $assistants = $this->model->getAssistants( $client->UserID );
                    $dataheaders = array( 'Avatar', 'AvatarLink' );
                    AppUtility::processClientsData( $assistants, $dataheaders );
                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Manage Assistant Managers' );
                    break;
            }
            View::page( $currvw, get_defined_vars() );
            
        } else { View::redirect(); }
    }

    /**
     * Clients Teams
     * This is for public view
     *
     * @access public
     * @return na
     */
    public function team()
    {
        if( User::is( 'Client' ) || User::can( 'Manage Teams' ) ){

            if( empty( $this->segment[2] ) ){
                View::redirect( User::dashboardLink( true ) );
            }

            $this->setSession( 'lister', array( $this->controller, $this->model, 'Manage Team Members' ) );
            
            $userinfo = User::info();
            Auth::userSession(); // Continue if user has session
            $cuid = AppUtility::getClientUserID( $userinfo );
            $action = isset( $this->segment[2] ) ? $this->segment[2] : 'manage';
            $teamid = isset( $this->segment[3] ) ? $this->segment[3] : false;
            $referrer = isset( $this->segment[4] ) ? $this->segment[4] : '';
            $currvw = 'clients/team/team_list';
            $client = $this->model->getInfo( $cuid );

            //$update = $this->model->doSaveFaq();

            switch( strtolower( $action ) ){
                case 'add':
                    $currvw = 'clients/team/team_add';
                    $userID = $this->model->doSaveTeam( $client->UserID, $userinfo->UserID );

                    // if( $userID ){
                    //     $userInfo = AppUtility::getUser( $userID );
                    //     // Start Email sending
                    //     $email = $this->load->model( 'emailengine', true );
                    //     $email->sendUserRegistration( $userID, $userInfo );
                    // }
                    
                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Add Team Member' );
                    break;

                case 'edit':
                    $currvw = 'clients/team/team_edit';
                    $this->model->doSaveTeam( $teamid, $userinfo->UserID );
                    $teams = $this->model->getTeamsByMemberID( $client->UserID, $teamid );
                    $dataheaders = array( 'Photo', 'PhotoLink' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $teams[0]->Name );
                    break;

                case 'trash':
                    $this->model->doTrashTeam( $teamid );
                    if( $referrer == 'profile' ){ View::redirect( 'clients/profile/teams' ); }
                    $teams = $this->model->getTeams( $client->UserID );
                    $dataheaders = array( 'Photo', 'PhotoLink' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    break;

                case 'delete':
                    $currvw = 'clients/team/team_trashbin';
                    $this->model->doDeleteTeam( $teamid );
                    $teams = $this->model->getTeams( $client->UserID );
                    $dataheaders = array( 'Photo', 'PhotoLink' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    break;

                case 'restore':
                    $this->model->doRestoreTeam( $teamid );
                    $teams = $this->model->getTeams( $client->UserID );
                    $dataheaders = array( 'Photo', 'PhotoLink' );
                    AppUtility::processClientsData( $teams, $dataheaders );
                    break;

                case 'trashbin':
                    if( User::can( 'Delete Team Members') ){
                        $currvw = 'clients/team/team_trashbin';
                        if( isset( $this->post['uids'] ) ){
                            $this->model->doEmptyTrashTeam( $this->post['uids'] );
                        }                    
                        $teams = $this->model->getTrashedTeams( $client->UserID );
                        $dataheaders = array( 'Photo', 'PhotoLink' );
                        AppUtility::processClientsData( $teams, $dataheaders );
                    } else {
                        $teams = $this->model->getTeams( $client->UserID );
                        $dataheaders = array( 'Photo' );
                        AppUtility::processClientsData( $teams, $dataheaders );

                        if( isset( $teams ) && count( $teams ) ){
                            foreach( $teams as $team ){
                                $jdata[$team->ClientTeamID] = array( 'FullName' => $team->Name, 'Bio' => $team->Bio );
                            }
                        }
                        $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );

                        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Manage Team Members' );
                    }
                    break;

                default:
                    $teams = $this->model->getTeams( $client->UserID );
                    $dataheaders = array( 'Photo' );
                    AppUtility::processClientsData( $teams, $dataheaders );

                    if( isset( $teams ) && count( $teams ) ){
                        foreach( $teams as $team ){
                            $jdata[$team->ClientTeamID] = array( 'FullName' => $team->Name, 'Bio' => $team->Bio );
                        }
                    }
                    $jdata = isset( $jdata ) ? str_ireplace( '"', '&quot;', json_encode( $jdata ) ) : json_encode( new stdClass );

                    $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Manage Team Members' );
                    break;
            }

            View::page( $currvw, get_defined_vars() );

        } else { View::redirect(); }
    }

    /**
     * Clear trashbin
     * 
     * @access private
     * @return na
     */
    public function emptytrashbin()
    {
        Auth::userSession(); // Continue if user has session

        $user = $this->model->doEmptyTrashAssistant();
        
        View::redirect('clients/assistant/asst_trashbin/');
    }

    /**
     * Clients dashboard page
     * 
     * @access private
     * @return na
     */
    public function comments()
    {
        $refid = isset( $this->segment[2] ) ? $this->segment[2] : false;

        $this->model->doSave( $refid );

        // if( AppUtility::myInvestments( $clientid ) > 0 ){
        //     $this->model->doSave();
        // } else { View::redirect('/'); }

        // if( User::is( 'Client' ) || User::can( 'Manage Clients' ) || (AppUtility::myInvestments( $clientid ) > 0) ){
        //     $this->model->doSave();
        // } else { View::redirect('/'); }
    }

    /**
     * Clients dashboard page
     * 
     * @access private
     * @return na
     */
    public function delcomm()
    {
        $where = [];
        $userinfo = User::info();
        $refid = isset( $this->segment[2] ) ? $this->segment[2] : false;
        
        if( strpos( $refid, 'REFID' ) !== false ){
            $where = array( 'ReferenceID' => $refid );
            $comment = $this->model->getComment( 'ReferenceID', $refid );
        } else {
            $where = array( 'CommentID' => $refid );
            $comment = $this->model->getComment( 'CommentID', $refid );
        }

        if( isset( $comment ) ){
            $commentID = isset( $comment->CommentID ) ? $comment->CommentID : '0';
            if( User::can( 'Administer All' )
            || $userinfo->UserID == $comment->ClientUserID // project owner
            || ( $userinfo->ReferrerUserID == $comment->ClientUserID && $userinfo->Code == 'ASST' ) // project assistant
            || $userinfo->UserID == $comment->UserID ){ // commenter
                $this->db->delete( "comments", $where ); // delete comment

                $where = array( 'ParentID' => $commentID );
                $this->db->delete( "comments", $where ); // delete replies

                App::activityLog( "Comment and associated replies has been deleted - ID#$refid" );
            }
        }
    }

    /**
     * Clients download approved booking page
     * 
     * @access private
     * @return na
     */
    public function downloadPDF()
    {
        $bookingID = isset( $this->segment[2] ) ? $this->segment[2] : 0;

        App::vendor('pdfcrowd');
        
        $pdfId = Option::get( 'pdf_id' ) ? Option::get( 'pdf_id' ) : Config::get( 'PDFCROWDID' ); 
        $pdfKey = Option::get( 'pdf_key' ) ? Option::get( 'pdf_key' ) : Config::get( 'PDFCROWDKEY' );
        $filename = "ApprovedBooking_".$bookingID.".pdf";
        $urltopdf = View::url( 'clients/funds/pdf/'.$bookingID );

        // try
        // {   
        //     // create an API client instance
        //     $crowd = new Pdfcrowd( $pdfId, $pdfKey );

        //     // $crowd->setPageWidth($opt['width']);
        //     // $crowd->setPageHeight($opt['height']);

        //     // if($opt['header']) {
        //     //     $crowd->setHeaderHtml($opt['header']);
        //     // }

        //     // if($opt['footer']) {
        //     //     $crowd->setFooterHtml($opt['footer']);
        //     // }
            
        //     // $crowd->setPageMargins($opt['top'], $opt['right'], $opt['bottom'], $opt['right']);
        //     // $crowd->usePrintMedia(true);
        //     // $crowd->enableImages(true);
            
        //     // $pdf = $crowd->convertHtml($opt['html']);
        //     // file_put_contents($path.$filename, $pdf); 

            
        //     $pdf = $crowd->convertURI( $urltopdf );
        //     // $pdf = $crowd->convertUrlToFile( $urltopdf, $filename );

        //     // $output_stream = fopen( $filename, "wb" );
        //     // $crowd->convertUrlToStream( $urltopdf, $output_stream );

        //     header( "Content-Type: application/pdf" );
        //     header( "Cache-Control: max-age=0" );
        //     header( "Accept-Ranges: none" );
        //     header( "Content-Disposition: attachment; filename=\"".$filename."\"" );

        //     // send the generated PDF 
        //     echo $pdf;
        // }
        // catch( PdfcrowdException $why )
        // {
        //     return "Pdfcrowd Error: " . $why;
        // }

        $crowd = new Pdfcrowd( $pdfId, $pdfKey );
        $pdf = $crowd->convertURI( $urltopdf );
        header( "Content-Type: application/pdf" );
        header( "Cache-Control: max-age=0" );
        header( "Accept-Ranges: none" );
        header( "Content-Disposition: attachment; filename=\"".$filename."\"" );
        echo $pdf;
    }

    public function createFileForm()
    {
        $dname = $this->segment[2];
        header( "Content-type:application/json" );
        echo json_encode( AppUtility::getCustomFileList( $dname, false, true, true ) );
    }

    public function savepreview()
    {
        $blogID = $this->model->doSavePreview();
        header( "Content-type:application/json" );
        echo json_encode( $blogID );
    }

    public function discardpreview()
    {
        $fimage = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $microtime = isset( $this->segment[3] ) ? $this->segment[3] : '0';

        // delete file uploaded from preview
        if( $fimage ){
            AppUtility::deleteFileAndAssociates( $fimage );
            $this->setSession( 'message', "" );
        }

        // delete data saved from preview
        $where = array( 'BlogSlug' => 'microtime:'.$microtime );
        $this->db->delete( "blogs", $where );

        header( "Content-type:application/json" );
        echo json_encode( $fimage );
    }

    function teammemberdetails()
    {
        $userid = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $user = User::info( false, $userid );
        $avatar = View::common()->getUploadedFiles( $user->Avatar );
        $usersModel = $this->load->model( 'users', true, true );

        echo '<div>';
            echo '<div class="col-md-6"> ';
                echo '<p><strong>Name:</strong> '.  Apputility::maskText( $user->FirstName, 'x', 'x' ).' '.$user->LastName.'</p>';
                echo '<p><strong>Job Title:</strong> '.  $user->JobTitle .'</p>';
                echo '<p><strong>Occupation:</strong> '.  $user->Occupation.'</p>';
                echo '<p><strong>Phone:</strong> '.  $user->Phone.'</p>';
            echo '</div>';
            echo '<div class="col-md-6">'.View::photo( ( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'), "Avatar", "img-avatar",false, false,false ).'';
            echo '<p></p></div>';

            echo '<div class="col-md-12"><p><strong>Address:</strong> '.$user->Address.'</p></div>';

            echo '<div class="col-md-6">';
                echo '<p><strong>City:</strong> '.  $user->City .'</p>';
                echo '<p><strong>Country:</strong> '.  $user->Country .'</p>';
            echo '</div>';
            echo '<div class="col-md-6">';
                echo '<p><strong>State:</strong> '.  $user->State.'</p>';
                echo '<p><strong>Postal Code:</strong> '.  $user->PostalCode .'</p>';
            echo '</div>';

        $profile = $usersModel->getProfile( $userid );
        $investorFiles = AppUtility::getInvestorFileList( $profile, false );
        echo '<div class="col-md-12">'.$investorFiles.'</div>';

        echo '</div><!--end main div-->';

        echo '<div class="clearfix"></div>';
    }

    function seederinfo()
    {
        $userid = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $userinfo = User::info(); // currently logged on - attempting to view someone's profile
        $uid = $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID;
        $client = AppUtility::getClientProfileInfo( $uid );
        $user = User::info( false, $userid );
        $avatar = View::common()->getUploadedFiles( $user->Avatar );
        $usersModel = $this->load->model( 'users', true, true );
        $profiles = $usersModel->getProfile( $userid, false );
        $investorFiles = AppUtility::getInvestorFileList( $profiles, false );
        
        // $investorFiles = '';
        // $ctr = 0;
        // if( isset( $profiles ) && count( $profiles ) ){
        //     foreach( $profiles as $profile ){
        //         $fgID = isset( $profile->FileGroupID ) ? $profile->FileGroupID : '0';
        //         $upload = $profile->FileID == NULL || strlen( $profile->FileID ) == 0 ? true : false;
        //         if( !$upload ){
        //             $investorFiles .= AppUtility::getCustomFileList( $fgID.'-SeederProfileDocs'.$ctr, $profile, $upload );
        //         }
        //         $ctr++;
        //     }
        // }

        if( User::can( 'Administer All' )
            || $userinfo->UserID == $client->UserID
            || ( $userinfo->ReferrerUserID == $client->UserID && $userinfo->Code == 'ASST' )
            || AppUtility::checkBookingByProject( $userid, $client->ClientProfileID ) > 0 ){
            
            // echo '<div>';
            //     echo '<div class="col-md-6"> ';
            //         echo '<p><strong>Name:</strong> '. $user->FirstName.' '.$user->LastName.'</p>';
            //         echo '<p><strong>Job Title:</strong> '. $user->JobTitle .'</p>';
            //         echo '<p><strong>Occupation:</strong> '. $user->Occupation.'</p>';
            //         echo '<p><strong>Phone:</strong> '. $user->Phone.'</p>';
            //     echo '</div>';
            //     echo '<div class="col-md-6">'.View::photo( ( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'), "Avatar", "img-avatar", false, false, false );
            //     echo '<p></p></div>';
            //     echo '<div class="col-md-12"><p><strong>Address:</strong> '.$user->Address.'</p></div>';
            //     echo '<div class="col-md-6">';
            //         echo '<p><strong>City:</strong> '.$user->City.'</p>';
            //         echo '<p><strong>Country:</strong> '.$user->Country.'</p>';
            //     echo '</div>';
            //     echo '<div class="col-md-6">';
            //         echo '<p><strong>State:</strong> '.$user->State.'</p>';
            //         echo '<p><strong>Postal Code:</strong> '.$user->PostalCode.'</p>';
            //     echo '</div>';
            // echo '<div class="col-md-12">'.$investorFiles.'</div>';
            // echo '</div><!--end main div-->';
            // echo '<div class="clear"></div>';


            echo '<div class="popup-info form-horizontal form-ui">';
              echo '<div class="row">';
                echo '<div class="col-md-6">';

                  echo '<div class="form-group">';
                    echo '<label>Name</label>';
                    echo '<div class="form-control">'. $user->FirstName.' '.$user->LastName.'</div>';
                  echo '</div>';

                  // echo '<div class="form-group">';
                  //   echo '<label>Job Title</label>';
                  //   echo '<div class="form-control">'. $user->JobTitle .'</div>';
                  // echo '</div>';

                  // echo '<div class="form-group">';
                  //   echo '<label>Occupation</label>';
                  //   echo '<div class="form-control">'. $user->Occupation.'</div>';
                  // echo '</div>';

                  echo '<div class="form-group">';
                    echo '<label>Phone</label>';
                    echo '<div class="form-control">'. $user->Phone.'</div>';
                  echo '</div>';

                echo '</div>';
                echo '<div class="col-md-6 text-center">'.View::photo( ( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'), "Avatar", "img-avatar img-avatar158 img-avatar-thumb", false, false, false ).'</div>';
                echo '<div class="clear"></div>';

                echo '<div class="col-md-12">';

                  echo '<div class="form-group">';
                    echo '<div class="form-left">';
                      echo '<label>Job Title</label>';
                      echo '<div class="form-control">'.$user->JobTitle.'</div>';
                    echo '</div>';

                    echo '<div class="form-right">';
                      echo '<label>Occupation</label>';
                      echo '<div class="form-control">'.$user->Occupation.'</div>';
                    echo '</div>';
                  echo '</div>';
                  
                  echo '<div class="form-group">';
                    echo '<label>Address</label>';
                    echo '<div class="form-control">'.$user->Address.'</div>';
                  echo '</div>';

                  echo '<div class="form-group">';
                    echo '<div class="form-left">';
                      echo '<label>City</label>';
                      echo '<div class="form-control">'.$user->City.'</div>';
                    echo '</div>';

                    echo '<div class="form-right">';
                      echo '<label>Country</label>';
                      echo '<div class="form-control">'.$user->Country.'</div>';
                    echo '</div>';
                  echo '</div>';

                  echo '<div class="form-group">';
                    echo '<div class="form-left">';
                      echo '<label>State</label>';
                      echo '<div class="form-control">'.$user->State.'</div>';
                    echo '</div>';

                    echo '<div class="form-right">';
                      echo '<label>Postal Code</label>';
                      echo '<div class="form-control">'.$user->PostalCode.'</div>';
                    echo '</div>';
                  echo '</div>';

                echo '</div>';
                echo '<div class="col-md-12">'.$investorFiles.'</div>';
                echo '<div class="clear"></div>';

              echo '</div>';
            echo '</div>';
            
        } else {
            echo '<div>';
                echo '<h1 class="font-s128 font-w300 text-amethyst animated fadeInDown text-center">401</h1>';
                echo '<h2 class="h3 font-w300 push-50 animated fadeInUp">We are sorry but you are not authorized to access this page...</h2>';
            echo '</div>';
        }
    }
}