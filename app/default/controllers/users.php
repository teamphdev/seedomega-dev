<?php
class Users extends Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        // Load vendor class
        $this->load->vendor('facebook/autoload');

        Asset::user();
    }

    /**
     * Get user list page
     * 
     * @access private | Admin
     * @return rows
     */
    public function index()
    {
        Auth::userSession(); // Continue if user has session

        if( !User::can('Administer All') ){
            View::redirect('/');
        }

         // Load assets
        $users = $this->model->getUsers();
        $dataheaders = array( 'Avatar' );
        AppUtility::processClientsData( $users, $dataheaders );
        // if( isset( $users ) ){
        //     if( count( $users ) ){
        //         foreach( $users as $user ){
        //             $user->Data['AvatarLink'] = '<img src="'.View::asset( isset( $user->FileSlug ) ? 'files'.$user->FileSlug : 'images/user.png' ).'">';
        //         }
        //     }
        // }
        
        View::page( 'users/list', get_defined_vars() );
    }

    /**
     * Get user login page
     * 
     * @access public
     * @return content form
     */
    public function login()
    {
        Auth::noUserSession(); // Continue if user doesn't have session

        Asset::login(); // Load assets
        $this->model->doLogin(); // Process login
        $segment = '';

        if( isset( $this->segment[2] ) ){
            $segment = $this->segment[2];
        }

        View::page( 'users/login', get_defined_vars() );
    }

    /**
     * Login using Facebook
     * 
     * @access public
     * @return na
     */
    public function fbLogin()
    {
        $user = App::fbLogin('users/login');
        $this->model->doFBLogin($user);
    }

    /**
     * Get user dashboard page
     * 
     * @access private
     * @return contents
     */
    public function dashboard()
    {
        Auth::userSession(); // Continue if user has session        
        $this->setSession( 'lister', array( $this->controller, $this->method ) ); // Use for breadcrumb

        //$this->model->doSave();
        //$this->model->updateUserInfo();
         // Load assets

        $userinfo = User::info();
        $levels = $this->model->getUserLevels();

        $recentview = $this->model->getRecentlyViewedProjects( 4 );
        $recentview = AppUtility::processClientsData( $recentview, array( 'CompanyPhoto' ) );

        $projects = $this->model->getActiveProjects( $userinfo->UserID );
        $dataheaders = array(
            'Avatar', 'BarColor', 
            'BookHTML', 'Percentage',
            'ProjectClass', 'CompanyPhoto', 
            'CompanyLogo'
        );
        AppUtility::processClientsData( $projects, $dataheaders );
        $enabled = Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || User::is( 'Administrator' ) ? true : false;

        $incomplete = false;
        if( user::info( 'UserLevel' ) == 'User' && Option::get('global_promo') == 0 ) { 
            switch( Apputility::investorinfo( 'InvestorStatus' ) ) {
                case 'Verification':
                    echo '<div class="alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your profile is awaiting for verification! <a href="/users/profile"> Check Here.</a></div>';

                    $this->setSession('notice', 'Your profile is awaiting for verification! <a href="/users/profile"> Check Here.</a>');
                break;
                case 'Incomplete':
                    $this->setSession('notice', 'Welcome to SeedOmega! You are one step away to get started. Please complete your profile <a href="/users/profile"> <b>HERE</b></a>.');                    
                    $incomplete = true;
                break;
            }
        }

        View::page('users/dashboard', get_defined_vars());
    }

    /**
     * Get user recently viewed page
     * 
     * @access private
     * @return contents
     */
    public function recentlyviewed()
    {
        Auth::userSession(); // Continue if user has session
        $this->setSession( 'lister', array( $this->controller, $this->method, 'recently viewed' ) );

         // Load assets
        $levels = $this->model->getUserLevels();
        $projects =$this->model->getRecentlyViewedProjects();
        $dataheaders = array(
            'Avatar', 'AvatarLink',
            'BarColor', 'BookHTML',
            'Percentage', 'ProjectClass',
            'CompanyPhoto', 'CompanyPhotoLink'
        );
        AppUtility::processClientsData( $projects, $dataheaders );

        $pendingcount = 0;
        if( isset( $projects ) ){
            if( count( $projects ) ){
                foreach( $projects as $p ){ if( $p->Status == 'Pending' ){ $pendingcount++; } }
            }
        }

        View::page('users/recently_viewed', get_defined_vars());
    }

    /**
     * Get user personal page
     * 
     * @access private
     * @return contents
     */
    public function personal()
    {
        Auth::userSession(); // Continue if user has session
        
        $this->model->updatePersonalInformation(); // Update data
         // Load assets

        $thisid = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $userinfo = User::info( false, $thisid ); // Load logged in User info

        View::page( 'users/personal', get_defined_vars() );
    }
    
    /**
     * Get user profile page
     * 
     * @access private
     * @return contents
     */
    public function profile()
    {
        $info = '';
        $status = '';
        $approvehide = '';
        $changeStatusHTML = '';
        Auth::userSession(); // Continue if user has session

        // if(!User::is('User') && $this->segment[3] == ''){
        //     View::redirect('users/personal');
        // }

        View::referrer();

        $this->model->doSave(); // Save/Update data
        $currentuser = User::info();

        $thispage = isset( $this->segment[2] ) ? $this->segment[2] : 'documents';
        $thisid = isset( $this->segment[3] ) ? $this->segment[3] : false;
        $thisrequest = isset( $this->segment[4] ) ? $this->segment[4] : false;
        $userinfo = $thisid ? User::info( false, $thisid ) : $currentuser; // Load logged in User info

        // $fname = isset( $this->segment[3] ) ? User::info( 'FirstName', $this->segment[3] ) : '';
        // $lname = isset( $this->segment[3] ) ? User::info( 'LastName', $this->segment[3] ) : '';

        switch( $currentuser->Code ){
            case 'CLN':
            case 'ASST':
                // prevent client to view seeder's editable profile
                // redirect to his/her own profile instead
                View::redirect( 'clients/profile' );
                break;

            case 'USR':
                if( $currentuser->UserID != $userinfo->UserID ){
                    // prevent user to view other user's profile
                    // just load his/her own profile instead
                    $userinfo = $currentuser;
                } break;
            
            default: break;
        }

        if( isset( $this->post['acct'] ) ){
            $acnt = $this->post['acct'];
            if( isset( $acnt['Status'] ) && $acnt['Status'] == "Approved" && $acnt['Status'] != $this->post['oldstatus'] ){
                $userID = $this->post['userid'];
                $userInfo = $this->model->getUser( $userID );

                // Start Email sending
                $email = $this->load->model( 'emailengine', true );
                $email->sendUserProfileApproved( $userID, $userInfo );
            }
        }

        $name = isset( $userinfo->FirstName ) ? $userinfo->FirstName.' ' : '';
        $name .= isset( $userinfo->LastName ) ? $userinfo->LastName : '';
        $UserID = isset( $userinfo->UserID ) ? $userinfo->UserID : 0;
        $viewee = $thisid ? 'User' : ''; // use for profile completeness

        $profiles = $this->model->getProfile( $UserID, true ); // Get profile info from accounts table
        $proofs = $this->model->getProfile( $UserID, true, 'address' ); // Get address proof data from accounts table
        $request = $this->model->getProfileChangeRequest( $UserID ); // get change request profile info
        $percentcomplete = AppUtility::profileCompleteness( $UserID, $viewee );
        
        $disabled = ( isset( $profiles[0] ) && $profiles[0]->Status == 'Approved' ) ? ' disabled ' : '';
        $disabled = $disabled == '' ? '' : (User::can('Manage Profiles')?'':$disabled);
        if( isset( $profiles ) ){
            foreach( $profiles as $profile ){
                $profile->Disabled = '';
            }
        }
    
        $requestCount = 1;
        if( $request == NULL ){
            $request = new stdClass;
            $requestCount = 0;
        }

        if( $thisrequest == 1 && $requestCount = 1 ){ // load profile request data
            if( isset( $request->FirstName ) ){ $userinfo->FirstName = $request->FirstName; }
            if( isset( $request->LastName ) ){ $userinfo->LastName = $request->LastName; }
            if( isset( $request->Occupation ) ){ $userinfo->Occupation = $request->Occupation; }
            if( isset( $request->JobTitle ) ){ $userinfo->JobTitle = $request->JobTitle; }
            if( isset( $request->Address ) ){ $userinfo->Address = $request->Address; }
            if( isset( $request->City ) ){ $userinfo->City = $request->City; }
            if( isset( $request->State ) ){ $userinfo->State = $request->State; }
            if( isset( $request->Country ) ){ $userinfo->Country = $request->Country; }
            if( isset( $request->PostalCode ) ){ $userinfo->PostalCode = $request->PostalCode; }
            if( isset( $request->Avatar ) && $request->Avatar > 0 ){ $userinfo->Avatar = $request->Avatar; }
            if( isset( $request->AddressPhoto ) && $request->AddressPhoto > 0 ){ $profiles[0]->AddressPhoto = $request->AddressPhoto; }
            $approvehide = ' hidden ';
        }

        $request->RequestAvatar = isset( $request->Avatar ) ? $request->Avatar : '0';
        if( isset( $userinfo ) ){
            foreach( $userinfo as $colname => $value ){
                if( !isset( $request->$colname ) || $request->$colname == '' || $request->$colname == '0' ){
                    $request->$colname = $userinfo->$colname;
                }
            }
        }
        $request->AvatarData = View::common()->getUploadedFiles( $request->RequestAvatar == '0' ? $request->Avatar : $request->RequestAvatar );
        $request = (object)$request;

        if( $approvehide == '' ){ $changeStatusHTML = self::createChangeStatusElements( $profiles[0]->Status ); }
        $requestHTML = self::createRequestOrApproveElements( $currentuser, $profiles[0], $requestCount, $thisrequest );
        $requestHTML = $thispage == 'documents' || $thispage == 'more-about-me' ? '' : $requestHTML;

        $mytitle = "My Profile";
        if( isset( $profiles[0] ) && $profiles[0] != NULL ){
            $status = $profiles[0]->Status ? '<br><small>('.$profiles[0]->AccountType.' - '.$profiles[0]->Status.')</small>' : '';
            if( $profiles[0]->UserID == $currentuser->UserID ){
                if( $profiles[0]->Status == 'Approved' && $thispage != 'more-about-me' && $thispage != 'documents' ){
                    $approvehide = ' hidden ';
                    $info = '<span class="text-warning">Your account is already</span> <span class="green"><b>APPROVED</b></span><span class="text-warning">. Any changes in this section will not be saved! Click <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-updaterequest" class="text-uppercase">"Request Profile Update"</a> button to request for Profile Update.</span>';
                }
            } else {
                $mytitle = $userinfo->FirstName.' '.$userinfo->LastName;
            }
        }

        if( !isset( $userinfo->UserID ) ){
            View::redirect( AppUtility::redirect( 'No profile data was found for this user!' ) );
        }

        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $name );
        View::page('users/profile', get_defined_vars()); // Load profile view
    }

    /**
     * Get user password change page
     * 
     * @access private
     * @return contents form
     */
    public function security()
    {
        Auth::userSession(); // Continue if user has session

        $this->model->updateLoginInformation(); // Save/Update data
         // Load assets            
        $this->model->updateUserInfo(); // Update logged in info            
        $userinfo = User::info(); // Load logged in User info

        View::page('users/security', get_defined_vars());
    }

    /**
     * Manage Referrals
     * 
     * @access private
     * @return na
     */
    public function referrals()
    {
        Auth::userSession(); // Continue if user has session
         // Load assets
        $userinfo = User::info();

        $mytitle = 'My';
        if( isset( $userinfo->Code ) && $userinfo->Code == 'ASST' ){
            $mytitle = 'Company';
            $userinfo = User::info( false, $userinfo->ReferrerUserID );
        }

        $monthlyEarning = $this->model->getMReferralEarningMonthly();
        $monthlyEarning = AppUtility::processMonthlyEarning( $monthlyEarning );

        $myCommRate = $this->model->getCommissionRateOwn( $userinfo );

        $invited = $this->model->getAffiliatesOwn( $userinfo );
        $totalinvited = count( AppUtility::removeDuplicate( $invited, 'UserID' ) );
        if( isset( $invited ) && count ( $invited ) ){
            foreach( $invited as $k => $v ){
                $v->Data = array( 'Name', 'AvatarLink' );
                $v->Data['Name'] = isset( $v->FirstName ) ? ucwords( $v->FirstName ) : '';
                $v->Data['Name'] .= isset( $v->LastName ) ? ' '.ucwords( $v->LastName ) : '';
                $v->Data['AvatarLink'] = '<img src="'.View::asset( isset( $v->FileSlug ) ? 'files'.$v->FileSlug : 'images/user.png' ).'">';
            };
        }

        $result = $this->model->doSaveWithdraw();
        // if( $result ){
        //     $bookeddata = $this->model->getBookedItemData( $bookID );
        //     $email = $this->load->model( 'emailengine', true );
        //     $email->sendRequestWithdrawalEmail( $bookeddata );
        // }

        $converted = $this->model->getAffiliatesOwn( $userinfo, true );
        $totalconverted = count( AppUtility::removeDuplicate( $converted, 'InvestorUserID' ) );
        $totalwithdrawable = '0';
        $totalcommission = 0;
        if( isset( $converted ) && count ( $converted ) ){
            foreach( $converted as $k => $v ){
                $v->Data = array( 'Name', 'AvatarLink' );
                $v->Data['Name'] = isset( $v->InvestorName ) ? ucwords( $v->InvestorName ) : '';
                $v->Data['AvatarLink'] = '<img src="'.View::asset( isset( $v->FileSlug ) ? 'files'.$v->FileSlug : 'images/user.png' ).'">';
                $v->Commission = AppUtility::computeCommission( $v->SOCommission, $v->CommissionRate );
                $v->InvestmentAmount = isset( $v->InvestmentAmount ) ? number_format( $v->InvestmentAmount, 2 ) : '0.00';
                $totalcommission += str_replace( ',', '', $v->Commission );
            };
            $totalwithdrawable = count ( $converted );
        }
        $totalcommission = number_format( $totalcommission, 2 );
        
        //$totalcommission = AppUtility::sumConditional( $converted, 'Commission', '>', 0 );

        View::page( 'users/affiliates_list', get_defined_vars() );
    }

    /**
     * Trash the current segment user id
     * 
     * @access private
     * @return na
     */
    public function trash()
    {
        Auth::userSession(); // Continue if user has session

        $user = $this->model->doTrash( $this->segment[2] );
        $dataheaders = array( 'Avatar', 'AvatarLink' );
        AppUtility::processClientsData( $users, $dataheaders );

        View::redirect('users/list');
    }

    /**
     * Restore trashed user
     * 
     * @access private
     * @return na
     */
    public function restore()
    {
        Auth::userSession(); // Continue if user has session

        $user = $this->model->doRestore($this->segment[2]);
        $dataheaders = array( 'Avatar', 'AvatarLink' );
        AppUtility::processClientsData( $users, $dataheaders );

        View::redirect('users/list');
    }

    /**
     * Delete permanently the current segment user id
     * 
     * @access private
     * @return na
     */
    public function delete()
    {
        Auth::userSession(); // Continue if user has session
        if( User::can( 'Delete Users' ) ){
            $user = $this->model->doDelete( $this->segment[2] );
            $dataheaders = array( 'Avatar' );
            AppUtility::processClientsData( $user, $dataheaders );

            View::redirect( 'users/trashbin/' );
        } else View::redirect();
    }

    /**
     * Trash a user
     * 
     * @access private
     * @return na
     */
    public function trashbin()
    {
        Auth::userSession(); // Continue if user has session

        if( User::can( 'Delete Users' ) ){
            if( isset( $this->post['uids'] ) ){
                $users = $this->model->doEmptyTrash( $this->post['uids'] );
            }
            
            $users = $this->model->getTrashedUsers();
            $dataheaders = array( 'Avatar' );
            AppUtility::processClientsData( $users, $dataheaders );

            View::page( 'users/trashbin', get_defined_vars());
        }
    }

    /**
     * Clear trashbin
     * 
     * @access private
     * @return na
     */
    public function emptytrashbin()
    {
        Auth::userSession(); // Continue if user has session

        $user = $this->model->doEmptyTrash();
        
        View::redirect('users/trashbin/');
    }

    /**
     * Add a user
     * 
     * @access private
     * @return na
     */
    public function add()
    {
        Auth::userSession(); // Continue if user has session        
        
        $userID = $this->model->doSave();

        $levels = $this->model->getUserLevels();
        $optionclients = $this->model->getClientList();
        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, 'Add User' );

        View::page('users/add', get_defined_vars());

        if( $userID ){
            $userInfo = $this->model->getUser( $userID );

            // Start Email sending
            $email = $this->load->model( 'emailengine', true );
            $email->sendUserRegistration( $userID, $userInfo );

            View::redirect('users');
        }
    }

    /**
     * Edit/Update user
     * 
     * @access private
     * @return na
     */
    public function edit()
    {
        $pw = '**********';
        $rolesModel = $this->load->model('roles', true, true);        
        Auth::userSession(); // Continue if user has session
        $this->model->doSave();

        $userinfo = User::info();
        $seg2 = isset( $this->segment[2] ) ? $this->segment[2] : false;
        if( $seg2 ){
            if( User::can( 'Edit Clients' ) || User::can( 'Edit Users' ) || $seg2 == $userinfo->UserID ){
                // do nothing 
            } else {
                $seg2 = $userinfo->UserID;
            }
            $user = $this->model->getUser( $seg2 );
            $levels = $this->model->getUserLevels();
            $roles = $rolesModel->getLevels();
            $loadedcapa = isset( $this->segment[3] ) ? Level::capabilities( $this->segment[3] ) : false;

            $capabilities =  $this->model->getCapabilities();
            $optionclients = $this->model->getClientList();
            $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $user->FirstName.' '.$user->LastName );
            
            if( User::can( 'Administer All' ) || $user->UserID == $userinfo->UserID ){
                $pw = AppUtility::decodeHash( $user->HashKey );
            }

            View::page('users/edit', get_defined_vars());
        } else {
            View::redirect('users/login');
        }
    }

    /**
     * Signup new user
     * 
     * @access public
     * @return signup form
     */
    public function signup()
    {
        if( !User::can( 'Administer All' ) ){
            Auth::noUserSession(); // Continue if user has no session
        }

        $userID = $this->model->doSignupUser(); 
        $levels = $this->model->getUserLevels();
        
        $affiliateUserID = ($this->getCookie(Config::get('AFFILIATEKEY'))) ? $this->getCookie(Config::get('AFFILIATEKEY')) : 0;
        $isExempted = ($this->getCookie(Config::get('EXEMPTIONKEY'))) ? $this->getCookie(Config::get('EXEMPTIONKEY')) : 0;
        $ReferrerUserID = (User::info('UserID',$affiliateUserID) != '') ? $affiliateUserID : 100000;

        View::page( 'users/signup', get_defined_vars() );
        if( $userID ){
            $userInfo = $this->model->getUser($userID);

            // Start Email sending
            $email = $this->load->model('emailengine',true);
            $email->sendUserRegistration($userID,$userInfo);
        }
    }

    /**
     * Signup using Facebook
     * 
     * @access public
     * @return na
     */
    public function fbSignup()
    {
        $user = App::fbLogin('users/signup');
        $userID = $this->model->doFBSignup($user);

        if($userID) {
            $userInfo = $this->model->getUser($userID);

            // Start Email sending
            $email = $this->load->model('emailengine',true);
            $email->sendUserRegistration($userID,$userInfo);
        }
    }

    /**
     * Signup using Google - callback
     *
     * @access public
     * @return na
     */
    public function gcallback()
    {
        $user = App::googleLogin('users/signup');
        $userID = $this->model->doGoogleSignup($user);

        if($userID) {
            $userInfo = $this->model->getUser($userID);

            // Start Email sending
            $email = $this->load->model('emailengine',true);
            $email->sendUserRegistration($userID,$userInfo);
        }
    }

    /**
     * Load footer script for user role
     * 
     * @access private
     * @return na
     */
    public function userRole()
    {
        if(User::isLoggedIn()) {
            View::$footerscripts = array('vendor/jquery/dist/jquery.min.js','vendor/bootstrap/dist/js/bootstrap.min.js','vendor/fastclick/lib/fastclick.js','vendor/nprogress/nprogress.js','vendor/raphael/raphael.min.js','vendor/morris.js/morris.min.js','vendor/bootstrap-progressbar/bootstrap-progressbar.min.js','assets/js/moment/moment.min.js','assets/js/datepicker/daterangepicker.js','assets/js/custom.js');
            View::page('user/userrole', get_defined_vars());
        } else {
            View::redirect();
        }
    }

    /**
     * Load load password request page
     * 
     * @access public
     * @return na
     */
    public function requestResetPassword()
    {
        Auth::noUserSession(); // Continue if user doesn't have session

        $uinfo = $this->model->doRequest();
        if($uinfo) {
            $email = $this->load->model('emailengine',true);
            $email->sendLostPasswordEmail($uinfo);
            $this->setSession('message', 'Email sent!');
        }
        $segment = '';
        if(isset($this->segment[2])){
            $segment = $this->segment[2];
        }

        $this->load->view('users/request', get_defined_vars());
    }

    /**
     * Load load password request page
     * 
     * @access public
     * @return na
     */
    public function resetPassword()
    {
        $this->model->doRequestReset();
        $segment = $this->segment[2];
        if($this->model->checkRequestResetKey($segment)){
            $this->load->view('users/requestform', get_defined_vars());
        } else {
            View::redirect('users/login');
        }        
    }

    /**
     * Logout user
     * 
     * @access private
     * @return na
     */
    public function logout()
    {
        $this->model->doLogout();
    }

    public function testemail()
    {
        $email = $this->load->model('emailengine',true);
        $email->sendTest('moises.goloyugo@gmail.com','Mo','Test from smtp','test content');
    }

    function createChangeStatusElements( $status )
    {
        $html = '';
        if( User::info( 'UserLevel' ) == "Administrator" || User::can( 'Manage Profiles' ) ){
            $html .= '<div class="form-group push-15-t">';
                $html .= '<label>Change Status</label>';
                $html .= '<select name="acct[Status]" class="form-control">';
                    $html .= '<option value="Incomplete" '.($status == 'Incomplete' ? 'selected' : '').'>Incomplete</option>';
                    $html .= '<option value="Verification" '.($status == 'Verification' ? 'selected' : '').'>Verification</option>';
                    $html .= '<option value="Approved" '.($status == 'Approved' ? 'selected' : '').'>Approved</option>';
                $html .= '</select>';
                $html .= '<input type="hidden" value="'.$status.'" name="oldstatus">';
            $html .= '</div>';
        }

        return $html;
    }

    function createRequestOrApproveElements( $userinfo, $profile, $requestcount, $thisrequest )
    {
        $html = '<li>';
        if( $userinfo->Code == 'USR' && $profile->Status == 'Approved' ){
            $html .= '<button type="button" data-toggle="modal" data-target="#modal-updaterequest" class="btn btn-rounded btn-default green text-uppercase"><i class="fa fa-edit"></i> Request Profile Update</button>';

        } elseif( User::info( 'UserLevel' ) == "Administrator" || User::can( 'Manage Profiles' ) ){
            if( $thisrequest == 1 && $requestcount > 0 ){
                $html .= '<form enctype="multipart/form-data" method="post">';
                    $html .= '<input type="hidden" name="action" value="approveseederchangerequest" />';
                    $html .= '<input type="hidden" name="UserID" value="'.$profile->UserID.'" />';
                    $html .= '<li><button type="submit" class="btn btn-sm btn-info text-uppercase">Click To Approve</button></li>';
                $html .= '</form>';
            }
        }
        $html .= '</li>';

        return $html;
    }
}