<?php
class Admin extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::admin();
    }

    /**
     * Admin index page
     * 
     * @access private
     * @return rows
     */
    public function index()
    { 
        // $mainModel = $this->load->model( 'main', true, true );

        // //get recent sliders
        // $sliders = $mainModel->getRecentSliders();
        // $dataheaders = array( 'Avatar', 'BarColor', 'Percentage', 'SliderImage' );
        // AppUtility::processClientsData( $sliders, $dataheaders );

        // //get currently fundings (active projects)
        // $currentlyfundings = $mainModel->getCurrentlyFundings();
        // $dataheaders = array( 'Avatar', 'BarColor', 'Percentage', 'CompanyLogo', 'CompanyPhoto' );
        // AppUtility::processClientsData( $currentlyfundings, $dataheaders );

        // //get our backers
        // $backers = $mainModel->getOurBackers();
        // $dataheaders = array( 'Avatar' );
        // AppUtility::processClientsData( $backers, $dataheaders );

        // $upcomings = $mainModel->getUpcomingEvents(); //get upcoming projects
        
        // View::page( 'main/homepage', get_defined_vars() );

        View::redirect( 'admin/dashboard' );
    }
    
    /**
     * Activity logs page
     * 
     * @access private
     * @return
     */
    public function activitylogs()
    {
        if( User::can( 'Administer All' ) ){
            $logs = $this->model->getActivityLogs();
            View::page( 'admin/activitylogs', get_defined_vars() );
        } else {
            View::redirect( 'admin' );
        }
    }
    
    /**
     * Dashboard
     * 
     * @access private
     * @return 
     */
    public function dashboard()
    {
        $manageModel = $this->load->model( 'manage', true, true );
        $csModel = $this->load->model( 'cs', true, true );
        $acctModel = $this->load->model( 'invoices', true, true );
        if( User::can( 'Administer All' ) ){
            $csPendings = $csModel->getAllPendingsCount();
            $acctPendings = $acctModel->getAllPendingsCount();

            // echo '<pre>'; print_r( count($csPendings) ); echo '</pre>';
            View::page( 'admin/dashboard', get_defined_vars() );
        } else {
            View::redirect();
        }
    }

    /**
     * Settings page
     * 
     * @access private
     * @return rows
     */
    public function settings()
    {
        if(!User::can('Administer All')){
            View::redirect('admin'); 
        }
                    
        View::page('admin/settings');
    }

    /**
     * Options page
     * 
     * @access private
     * @return rows
     */
    public function options()
    {
        if(!User::can('Administer All')){
            View::redirect(''); 
        }
        $this->model->doSave();
        
        $levels = $this->model->getUserLevels();
        $options = Option::getAll();
        $optionLists = $this->model->getOptionsList();

        View::page('admin/options', get_defined_vars());
    }

    /**
     * Import/Export page
     * 
     * @access private
     * @return rows
     */
    public function importexport()
    {        
        View::page('admin/importexport', get_defined_vars());
    }
    
    /**
     * Do Export
     * 
     * @access private
     * @return rows
     */
    public function export()
    {
        if(!User::can('Administer All')){
            View::redirect('admin'); 
        }
        App::exportTables();        
    }  
}