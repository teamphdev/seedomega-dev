<?php
class Faq extends Controller 
{
    var $action = NULL;
    var $faqsid = false;
    var $currvw = false;

    public function __construct()
    {
        parent::__construct();

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        // Auth::userSession(); // Continue if user has session
        Asset::faq();

        $this->action = isset($this->post['action']) ? $this->post['action'] : 'manage';
        $this->faqsid = isset($this->segment[2]) ? $this->segment[2] : false;        
    }

    /**
     * Non-logged in users
     * 
     * @access public
     * @return faq accordion
     */
    public function index()
    {
        $userinfo = User::info();
        $viewing = Auth::isLoggedIn() ? $userinfo->Level : 1;
        $currvw = 'faq/index';
        $faqPosts = $this->model->getFaqs( $viewing );
        
        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Managing list
     * 
     * @access public
     * @return array of object 
     */
    public function manage()
    {
        $userinfo = User::info();
        $currvw = 'faq/list';
        $faqPosts = $this->model->getFaqs();
        
        View::page($currvw, get_defined_vars());
    }

    /**
     * Adding new faq post
     * 
     * @access public
     * @return na 
     */
    public function add()
    {
        $this->model->doSave();
        $userinfo = User::info();
        $currvw = 'faq/add';

        View::page($currvw, get_defined_vars());
    }

    /**
     * Managing list
     * 
     * @access public
     * @return array of object 
     */
    public function edit()
    {
        $this->model->doSave();

        $userinfo = User::info();
        $faqsid = $this->faqsid;
        $currvw = 'faq/edit';
        $faqPost = $this->model->getFaq( $faqsid );
        View::page( $currvw, get_defined_vars() );
    }

    /**
     * Managing list
     * 
     * @access public
     * @return array of object 
     */
    public function delete()
    {
        $userinfo = User::info();
        $action = $this->action;
        $faqsid = $this->faqsid;
        $currvw = 'faq/list';
        $client = false;
        
        View::page($currvw, get_defined_vars());
    }
        
    public function faq()
    {
        if( empty( $this->segment[2] ) ){
            View::redirect( User::dashboardLink( true ) );
        }

        $userinfo = User::info();
        $action = isset( $this->segment[2] ) ? $this->segment[2] : 'manage';
        $faqsid = isset( $this->segment[3] ) ? $this->segment[3] : false;
        $cpid = $userinfo->Code == 'ASST' ? $userinfo->ClientID : false;
        $client = $this->model->getInfo( $cpid );
        $currvw = 'clients/faq/list';

        $update = $this->model->doSaveFaq();

        switch( strtolower( $action ) ){
            case 'add':
                    $currvw = 'clients/faq/add';
                break;

            case 'edit':                    
                    $faqPost = $this->model->getFaq( $faqsid );            
                    $currvw = 'clients/faq/edit';
                break;

            case 'delete':
                if( $faqsid ){
                    if( $userinfo->Code != 'ASST' ){
                        $this->model->doDeleteFaqPost( $faqsid );
                        $this->setSession( 'message', 'FAQ has been deleted!' );
                    }
                    View::redirect( 'clients/faq/list' );
                } break;

            default:
                $faqPosts = $this->model->getFaqs( $client->ClientProfileID );
                break;
        }

        View::page( $currvw, get_defined_vars() );
    }
}