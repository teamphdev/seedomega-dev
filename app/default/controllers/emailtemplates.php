<?php
class EmailTemplates extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');

        Auth::userSession(); // Continue if user has session
        Asset::common();
    }

    public function index()
    {
        $emTemplates = $this->model->getEmailTemplates();        
        View::page('emailtemplates/list', get_defined_vars());
    }
    
    public function delete()
    {
        //if(User::can('Delete EmailTemplate')) {
            $product = $this->model->doDelete($this->segment[2]);
            View::redirect('emailtemplates');
        //}
    }
    
    public function edit()
    {
        $etpl = $this->model->getEmailTemplate($this->segment[2]);        
        $fileContent = App::getFileContents('emails/'. $etpl->filename);         
        View::page('emailtemplates/edit', get_defined_vars());  
    }

    public function save()
    {
        $this->model->doSave();
    }
    
    public function add()
    {
        //if(User::can('Add EmailTemplate')) {
            $pdata = $this->model->doSave();
            View::page('emailtemplates/add', get_defined_vars());
        //}
    }
}