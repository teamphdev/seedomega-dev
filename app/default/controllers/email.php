<?php
class Email extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load helper class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');

        // Load vendor class    
        $this->load->model('emailengine'); 
    }

    public function index()
    {   
        $attachment =array(
            'path' => 'C:\Bitnami\wamp\apache2\htdocs\odeonco\app\views\odeon\assets\pdf\2016\11\11\pdf_78882855_commencement.pdf',
            'name' => 'pdf_78882855_commencement.pdf'
        );
        
        if($this->emailengine->send($attachment)) {
            echo 'Email sent!';
        } else {
            echo 'Sending failed!';
        }
    }
    
    public function send_cron()
    {   
        $this->emailengine->sendMTACron();
    }    
}