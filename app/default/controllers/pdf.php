<?php
class Pdf extends Controller 
{
    public function __construct()
    {
        parent::__construct();
            
        // Load model Auth
        $this->load->model('auth');        
        $this->load->model('pdfengine');   
        $this->load->helper('appmenu');
    }

    public function index()
    {    
        $pdf = isset($this->segment[1]) ? $this->segment[1] : false;
        
        if( $pdf ){
            $opt = array(
                'filename' => $pdf.'.pdf',
                'html' => App::getFileContents('emails'.DS.'pdfcontent'.DS.$pdf.'.eml'),
                'top' =>"0pt",
                'right' => "0pt",
                'bottom' =>"0pt",
                'left' => "0pt",
            );

            echo $this->pdfengine->generate($opt);
        } else {
            echo 'Please set a file name e.g. http://globalasset.loc/pdf/traditional';
        }
    }

    public function download()
    {    
        $ID = isset($this->segment[2]) ? $this->segment[2] : false;
        
        if( $ID ){
            $opt = array(
                'filename' => 'ApprovedBooking_'.$ID.'.pdf',
                'html' => View::url( '/clients/funds/pdf/'.$ID ),
                'top' =>"0pt",
                'right' => "0pt",
                'bottom' =>"0pt",
                'left' => "0pt",
            );

            echo $this->pdfengine->generate( $opt );
        } else {
            echo 'Please set a file name e.g. http://globalasset.loc/pdf/traditional';
        }
    }
}