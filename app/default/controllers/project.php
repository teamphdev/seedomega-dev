<?php
class Project extends Controller 
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('users', false, true);

        // Load vendor class
        $this->load->helper('auth');
        $this->load->helper('apputility');
        $this->load->helper('appmenu');
        $this->load->helper('asset');
        
        //Auth::userSession(); // Continue if user has session
        Asset::client();
    }

    /**
     * Project View Page
     * 
     * @access private
     * @return na
     */
    public function view()
    {
        $searchString = '';
        $previewonly = '';
        $nocomments = '';
        $csModel = $this->load->model( 'cs', true, true );
        $blogModel = $this->load->model( 'blogs', true, true );
        $forumModel = $this->load->model( 'forums', true, true );
        $clientModel = $this->load->model( 'clients', true, true );

        if( User::isLoggedIn() ){
            $userinfo = User::info();
            $cuid = AppUtility::getClientUserID( $userinfo );
            $loggedClient = $clientModel->getInfo( $cuid );
        }
        $userID = User::isLoggedIn() ? User::info('UserID') : false;

        // if current user is a client, force him/her to view his/her own project page
        $cpid = isset( $loggedClient->ClientProfileID ) ? $loggedClient->ClientProfileID : $this->segment[2];
        $clientdata = $clientModel->getClientData( $cpid, $userID ); // get client data
        if( User::can( 'Manage Profiles' ) ){
            // approve/update client for hub care / admins
            $csModel->doSave( $userinfo->UserID, $clientdata );
            $clientdata = $clientModel->getClientData( $cpid, $userID ); // re-get updated client data
        }
        
        $enabledTabs = (
            ( Option::get( 'global_promo' ) == 1 
            || AppUtility::investorinfo( 'InvestorStatus' ) == 'Approved'
            || User::can( 'Manage Clients' )
            || isset( $loggedClient->ClientProfileID ) )
            && User::isLoggedIn()
        ) ? true : false;

        $disabled = isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ? '' : ' disabled ';
        $hidden = $disabled == '' ? '' : ' hidden ';
        $hidden = isset( $loggedClient->ClientProfileID ) ? '' : $hidden;
        if( User::can( 'Manage Profiles' ) ){ $hidden = ''; }

        if( $clientdata ){
            if( !User::isLoggedIn() ){
                $old = array('http://www','http://');
                $new = array('','');
                $domain = str_replace($old, $new, View::url());
               
                setcookie(Config::get('REFERRER'),'bookings/book/'.$clientdata->ClientProfileID.'/',time()+ 3600,'/',$domain);
            }

            $this->setSession( 'CLIENTINFO', array( $clientdata->CompanyName, $clientdata->ClientProfileID ) ); //use for forums

            $today = new DateTime( "now" );
            $opening = new DateTime( $clientdata->OfferOpening );
            $clientdata->Data = array( 'title' => '', 'button' => '', 'disabled' => '' );
            $title = ( $clientdata->TotalBookingAmounts >= $clientdata->TargetGoal && $clientdata->TypeOfOffer == 'Limited' ) ? "Register" : "Book";
            $title = ( isset( $opening ) && $opening > $today ) ? "Register" : $title;

            switch( $title ){
                case 'Book':
                    if( isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ){
                        $title = 'Book Again';
                    } else { $title = 'Book Now'; }
                    break;

                case 'Register':
                    if( isset( $clientdata->CurrentUserRegisterCount ) && $clientdata->CurrentUserRegisterCount > 0 ){
                        $title .= ' Again';
                    } else { $title = 'Register Now'; }
                    break;
                
                default: break;
            }

            $clientdata->Data['title'] = 'Click '.$title.' to Invest!';
            $clientdata->Data['button'] = $title;
        } else {
            // View::redirect();
        }

        if( User::isLoggedIn() ){
    		$faqs = $clientModel->getFaqs( $clientdata->ClientProfileID, NULL, $userinfo->UserID );
            $blog = $clientModel->getBlogInfo( $clientdata->ClientProfileID );
            if( $blog ){
                $category = isset ( $this->segment[4] ) ? $this->segment[4] : false;
                $searchString = (isset($_GET['s'])) ? $_GET['s'] : '';
                if( $category == '*' ){
                    View::redirect( 'project/view/'.$clientdata->ClientProfileID.'/client-blogs' );
                }
                $uid = $userinfo->UserID;
                if( $clientdata->UserID == $userinfo->UserID // client 
                    || ( $clientdata->UserID == $userinfo->ReferrerUserID && $userinfo->Code == 'ASST' ) // client's assistant
                    || User::can( 'Administer All' )
                    || User::can( 'Access Blogs' ) ){ // set $uid = NULL if current user is the owner or admins
                    $uid = NULL;
                }
                if( $category && !$searchString ){
                    $categoryID = $clientModel->getCategoryIDBySlug( $category, $clientdata->ClientProfileID );
                    $recentPosts = $clientModel->getRecentPosts( $clientdata->ClientProfileID, $uid, $categoryID );
                } elseif( $searchString ){
                    $recentPosts = $clientModel->getRecentPosts( $clientdata->ClientProfileID, $uid, null, $searchString );
                } else {
                    $recentPosts = $clientModel->getRecentPosts( $clientdata->ClientProfileID, $uid );
                }
                if( isset( $recentPosts ) && count( $recentPosts ) ){
                    foreach( $recentPosts as $recent ){
                        $recent->FILink = strlen( $recent->FISlug ) ? 'files'.$recent->FISlug : '/images/blank-icon.png';
                    };
                }
            }
            
            $clientModel->doSave( $disabled );
            $newcomments = [];
    		$comments = $clientModel->getComments( $clientdata->ClientProfileID, $userinfo->UserID );
            if( isset( $comments ) && count( $comments ) ){
                foreach( $comments as $comment ){
                    $comment->AvatarLink = '<img src="'.View::asset( isset( $comment->FileSlug ) ? 'files'.$comment->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';

                    if( $comment->ParentID == 0 ){
                        $newcomments[$comment->CommentID] = $comment;
                        $newcomments[$comment->CommentID]->Children = [];
                    } else {
                        array_push( $newcomments[$comment->ParentID]->Children, $comment );
                    }
                }
            }

            //set the current user comment indicator
            $currUser = (object) array();
            $fi = View::common()->getUploadedFiles( $userinfo->Avatar );
            $currUser->AvatarLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';
            $currUser->AvatarReplyLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="25" height="25">';
            $currUser->AvatarLinkConverted = str_ireplace( '"', '&#34;', $currUser->AvatarLink );
            $currUser->Name = isset( $userinfo->FirstName ) ? $userinfo->FirstName : '';
            $currUser->Name .= isset( $userinfo->LastName ) ? ' '.$userinfo->LastName : '';
            $currUser->Level = isset( $userinfo->Level ) ? $userinfo->Level : '';
            $currUser->Indicator = '';
            switch( $userinfo->Level ){
                case '4': $currUser->Indicator = '<i class=&#34;fa clnt-icon fa-star&#34;></i>'; break;
                case '6': $currUser->Indicator = '<i class=&#34;fa ast-icon fa-circle text-warning&#34;></i>'; break;
                default: break;
            }

            $test = $forumModel->getForumSubCategory( 5 );

            $forums = $forumModel->getForumSummary( $clientdata->ClientProfileID );
            if( isset( $forums ) ){
                if( count( $forums ) ){
                    foreach( $forums as $k => $v ){
                        foreach( $v as $post ){
                            if( isset( $post->LastPost ) ){
                                $lp = explode( '|', $post->LastPost );
                                $post->Data = array( 'PostedBy' => $lp[0], 'DatePosted' => $lp[1], 'FileSlug' => $lp[2], 'AvatarLink' => '' );
                                $post->Data['AvatarLink'] = '<img src="'.View::asset( isset( $post->Data['FileSlug'] ) ? 'files'.$post->Data['FileSlug'] : 'images/slide-3.jpg' ).'">';
                            }
                        }
                    }
                }
            }

            $communities = $clientModel->getCommunities( $clientdata->ClientProfileID );

            $teams = $clientModel->getTeams( $clientdata->UserID, true );
            if( isset( $teams ) && count( $teams ) ){
                foreach( $teams as $team ){
                    $team->PhotoLink = '<img src="'.View::asset( isset( $team->FileSlug ) && $team->FileSlug != '' ? 'files'.$team->FileSlug : 'images/user.png' ).'">';
                    $team->AvatarLink = '<img src="'.View::asset( isset( $team->FileSlug ) && $team->FileSlug != '' ? 'files'.$team->FileSlug : 'images/user.png' ).'">';
                    $data = array(
                        'Name' => isset( $team->Name ) ? $team->Name : '',
                        'Position' => isset( $team->Position ) ? $team->Position : '',
                        'Bio' => isset( $team->Bio ) ? $team->Bio : '',
                        'Avatar' => $team->AvatarLink,
                        'Photo' => isset( $team->Photo ) ? $team->Photo : '0',
                        'PhotoLink' => $team->PhotoLink
                    );
                    $team->Data = str_ireplace( '"', '&quot;', json_encode( $data ) );
                };
            }

            $documents = $clientModel->getClientFiles( $clientdata );
            if( isset( $documents ) && count( $documents ) ){
                foreach( $documents as $doc ){
                    $doc->Link = View::asset( isset( $doc->FileSlug ) ? 'files'.$doc->FileSlug : '' );
                };
            }

            $categories = $blogModel->getAllCategories();

            AppUtility::updateLastView( $userinfo, $clientdata );
        } else { $nocomments = 'hidden'; }

        $tabid = isset( $this->segment[3] ) ? $this->segment[3] : 'client-info';
        if( $searchString ){ $tabid = 'client-blogs'; }

        $percentage = 0;
        if( $clientdata->TargetGoal > 0){
            $percentage = (int)( ( $clientdata->TotalRaisedAmt / $clientdata->TargetGoal ) * 100 + .5 );
            $percentage = $percentage > 100 ? 100 : $percentage;
        }

        $description = $clientdata->VideoDescription ? $clientdata->VideoDescription : "No Description";
        $description = preg_replace( "/<img[^>]+\>/i", "", $description );
        $logo = strlen( $clientdata->LogoSlug ) ? 'files'.$clientdata->LogoSlug : '/images/placeholder.png';
        $banner = $clientdata->PhotoSlug ? 'style="background-image:linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('.View::asset( 'files'.$clientdata->PhotoSlug ).');"' : '/images/default-banner.jpg';

        // social media links
        $void = 'javascript:void(0)';
        $SocialMedia = (object) array(
            'Google' => isset( $clientdata->Google ) && strlen( $clientdata->Google ) ? $clientdata->Google : $void,
            'Website' => isset( $clientdata->Website ) && strlen( $clientdata->Website ) ? $clientdata->Website : $void,
            'Facebook' => isset( $clientdata->Facebook ) && strlen( $clientdata->Facebook ) ? $clientdata->Facebook : $void,
            'Instagram' => isset( $clientdata->Instagram ) && strlen( $clientdata->Instagram ) ? $clientdata->Instagram : $void
        );

        // button html
        $htmlButton = '<div class="sidebar-item">';
            // $htmlButton .= '<div class="w-title">'.$clientdata->Data['title'].'</div>';
            $htmlButton .= '<div class="view-proj-btn">';
            if( isset( $userinfo ) ){
                if( $userinfo->UserLevel == 'User' && User::isLoggedIn() ){
                    if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || Option::get( 'global_promo' ) == 1 ){
                        $htmlButton .= '<a href="'.View::url( 'bookings/book/'.$clientdata->ClientProfileID ).'" class="btn btn-success text-uppercase btn-rounded push-15 item_button2 '.$clientdata->Data['disabled'].'">'.$clientdata->Data['button'].'</a>';
                    } else {
                        $htmlButton .= '<a href="'.View::url( 'bookings/book/'.$clientdata->ClientProfileID ).'" class="btn btn-success btn-rounded push-15 item_button2 '.$clientdata->Data['disabled'].'" style="margin:10px 0 5px 0;">BOOK NOW</a>';
                    }
                }
            } else {
                $htmlButton .= '<a href="'.View::url( 'users/signup' ).'" class="btn btn-success btn-rounded push-15 item_button2" style="margin:10px 0 5px 0;">JOIN NOW</a>';
            }
            $htmlButton .= '</div>';
        $htmlButton .= '</div>';

        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $clientdata->CompanyName );

        View::page( 'project/view', get_defined_vars() );
    }

    /**
     * Project Preview Page
     * 
     * @access private
     * @return na
     */
    public function preview()
    {
        $this->setSession( "preview", "" );
        $forumModel = $this->load->model( 'forums', true, true );
        $clientModel = $this->load->model( 'clients', true, true );
        $cpid = isset( $this->segment[2] ) ? $this->segment[2] : false;
        $userinfo = User::info();

        if( $cpid ){
            $cd = AppUtility::getClientProfileInfoByID( $cpid );            
            $cuid = $cd->UserID;
        } else {
            $cuid = AppUtility::getClientUserID( $userinfo );
        }
        $loggedClient = $clientModel->getInfo( $cuid );

        $clientdata = $clientModel->getClientData( $loggedClient->ClientProfileID );
        
        $previewonly = 'hidden';
        $enabledTabs = true;
        $disabled = '';
        $hidden = '';

        // if( $clientdata ){
        //     $this->setSession( 'CLIENTINFO', array( $clientdata->CompanyName, $clientdata->ClientProfileID ) ); //use for forums

        //     $today = new DateTime( "now" );
        //     $opening = new DateTime( $clientdata->OfferOpening );
        //     $clientdata->Data = array( 'title' => '', 'button' => '', 'disabled' => '' );
        //     $title = ( $clientdata->TotalBookingAmounts >= $clientdata->TargetGoal && $clientdata->TypeOfOffer == 'Limited' ) ? "Register" : "Book";
        //     $title = ( isset( $opening ) && $opening > $today ) ? "Register" : $title;

        //     switch( $title ){
        //         case 'Book':
        //             if( isset( $clientdata->CurrentUserBookedCount ) && $clientdata->CurrentUserBookedCount > 0 ){
        //                 $title = 'Book Again';
        //             } else { $title = 'Book Now'; }
        //             break;

        //         case 'Register':
        //             if( isset( $clientdata->CurrentUserRegisterCount ) && $clientdata->CurrentUserRegisterCount > 0 ){
        //                 $title .= ' Again';
        //             } else { $title = 'Register Now'; }
        //             break;
                
        //         default: break;
        //     }

        //     $clientdata->Data['title'] = 'Click '.$title.' to Invest!';
        //     $clientdata->Data['button'] = $title;

        // } else {
        //     View::redirect( 'clients' );
        // }
        
        $faqs = $clientModel->getFaqs( $clientdata->ClientProfileID, NULL, $userinfo->UserID );
        $blog = $clientModel->getBlogInfo( $clientdata->ClientProfileID );
        if( $blog ){
            $recentPosts = $clientModel->getRecentPosts( $clientdata->ClientProfileID, $userinfo->UserID );
        }
        
        //$clientModel->doSave( $disabled );
        $newcomments = [];
        $comments = $clientModel->getComments( $clientdata->ClientProfileID, $userinfo->UserID );
        if( isset( $comments ) && count( $comments ) ){
            foreach( $comments as $comment ){
                $comment->AvatarLink = '<img src="'.View::asset( isset( $comment->FileSlug ) ? 'files'.$comment->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';

                if( $comment->ParentID == 0 ){
                    $newcomments[$comment->CommentID] = $comment;
                    $newcomments[$comment->CommentID]->Children = [];
                } else {
                    array_push( $newcomments[$comment->ParentID]->Children, $comment );
                }
            }
        }

        //set the current user comment indicator
        $currUser = (object) array();
        $fi = View::common()->getUploadedFiles( $userinfo->Avatar );
        $currUser->AvatarLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="35" height="35">';
        $currUser->AvatarReplyLink = '<img src="'.View::asset( isset( $fi[0]->FileSlug ) ? 'files'.$fi[0]->FileSlug : 'images/user.png' ).'" alt="Avatar" width="25" height="25">';
        $currUser->AvatarLinkConverted = str_ireplace( '"', '&#34;', $currUser->AvatarLink );
        $currUser->Name = isset( $userinfo->FirstName ) ? $userinfo->FirstName : '';
        $currUser->Name .= isset( $userinfo->LastName ) ? ' '.$userinfo->LastName : '';
        $currUser->Level = isset( $userinfo->Level ) ? $userinfo->Level : '';
        $currUser->Indicator = '';
        switch( $userinfo->Level ){
            case '4': $currUser->Indicator = '<i class=&#34;fa clnt-icon fa-star&#34;></i>'; break;
            case '6': $currUser->Indicator = '<i class=&#34;fa ast-icon fa-circle text-warning&#34;></i>'; break;
            default: break;
        }

        $forums = $forumModel->getForumSummary( $clientdata->ClientProfileID );
        if( isset( $forums ) ){
            if( count( $forums ) ){
                foreach( $forums as $k => $v ){
                    foreach( $v as $post ){
                        if( isset( $post->LastPost ) ){
                            $lp = explode( '|', $post->LastPost );
                            $post->Data = array( 'PostedBy' => $lp[0], 'DatePosted' => $lp[1], 'FileSlug' => $lp[2], 'AvatarLink' => '' );
                            $post->Data['AvatarLink'] = '<img src="'.View::asset( isset( $post->Data['FileSlug'] ) ? 'files'.$post->Data['FileSlug'] : 'images/slide-3.jpg' ).'">';
                        }
                    }
                }
            }
        }

        $communities = $clientModel->getCommunities( $clientdata->ClientProfileID );
        $teams = $clientModel->getTeams( $clientdata->UserID, true );
        $documents = $clientModel->getClientFiles( $clientdata, true );
        if( isset( $documents ) && count( $documents ) ){
            foreach( $documents as $doc ){
                $doc->Link = View::asset( isset( $doc->FileSlug ) ? 'files'.$doc->FileSlug : '' );
            };
        }

        $breadcrumb = AppUtility::getBreadcrumb( $this->controller, $this->segment, $clientdata->CompanyName );
        
        AppUtility::updateLastView( $userinfo, $clientdata );
        
        $tabid = 'client-info';
        View::page( 'project/view', get_defined_vars() );
    }

    /**
     * Project Forum Ajax Call
     * 
     * @access private
     * @return na
     */
    public function forums()
    {
        $html = '';
        $usersModel = $this->load->model( 'users', true, true );
        $forumModel = $this->load->model( 'forums', true, true );

        $this->setSession( 'lister', array( 'forums', 'category' ) );

        $userinfo = User::info();
        $client = AppUtility::validateForumAccess();
        $title = isset( $client->CompanyName ) ? $client->CompanyName : '';
        $forum = array();

        $segment2 = isset( $this->segment[2] ) ? $this->segment[2] : '';
        switch( $segment2 ){
            case 'category':
                $forum['subcatid'] = isset( $this->segment[3] ) ? $this->segment[3] : 0;
                $forum['forumid'] = isset( $this->segment[4] ) ? $this->segment[4] : false;
                $forum['pinval'] = isset( $this->segment[5] ) ? $this->segment[5] : false;

                if( $forum['forumid'] && $forum['pinval'] ){
                    $forumModel->doSavePin( $forum );
                }

                $categories = $forumModel->getForumByCategory( false, $forum['subcatid'] );
                if( isset( $categories ) && count( $categories ) ){
                    foreach( $categories as $k => $v ){
                        foreach( $v as $category ){
                            $category->AuthorData['AvatarLink'] = '<img src="'.View::asset( isset( $category->AuthorFileSlug ) ? 'files'.$category->AuthorFileSlug : 'images/slide-3.jpg' ).'">';
                            $category->PosterData['AvatarLink'] = '<img src="'.View::asset( isset( $category->LastPosterFileSlug ) ? 'files'.$category->LastPosterFileSlug : 'images/slide-3.jpg' ).'">';
                        }
                    }
                    if( isset( $categories[0][0]->SubCategory ) ){
                        $forum['scname'] = $categories[0][0]->SubCategory;
                    }
                }
                $html = self::getOutputForumCategory( $forum, $categories );
                break;

            case 'topic':
                $forum['forumid'] = isset( $this->segment[3] ) ? $this->segment[3] : 0;
                
                $username = isset( $userinfo->FirstName ) ? $userinfo->FirstName.' ' : '';
                $username .= isset( $userinfo->LastName ) ? $userinfo->LastName : '';
                $forum['username'] = $username;
                $forum['userid'] = isset( $userinfo->UserID ) ? $userinfo->UserID : '0';

                $avatar = View::common()->getUploadedFiles( $userinfo->Avatar );
                $forum['avatarlink'] = '<img class="img-avatar" src="'.View::asset( isset( $avatar[0]->FileSlug ) ? 'files'.$avatar[0]->FileSlug : 'images/slide-3.jpg' ).'">';

                $total = $forumModel->getForumTotalPostOwn( $userinfo->UserID );
                $forum['total'] = isset( $total->Total ) ? $total->Total.( $total->Total > 1 ? ' Posts' : ' Post' ) : '0 Post';

                $forum['epigram'] = '';
                $topics = $forumModel->getForumTopic( $forum['forumid'] );
                if( isset( $topics ) && count( $topics ) ){
                    foreach( $topics as $topic ){
                        if( isset( $topic->Title ) && $topic->Title != '' ){ $forum['title'] = $topic->Title; }
                        if( isset( $topic->CatID ) && $topic->CatID != '0' ){ $forum['catid'] = $topic->CatID; }
                        if( isset( $topic->SubCatID ) && $topic->SubCatID != '0' ){ $forum['subcatid'] = $topic->SubCatID; }
                        $topic->Data = array( 'Name' => '', 'AvatarLink' => '' );
                        $topic->Data['Name'] = isset( $topic->FirstName ) ? $topic->FirstName.' ' : '';
                        $topic->Data['Name'] .= isset( $topic->LastName ) ? $topic->LastName : '';
                        $topic->Data['AvatarLink'] = '<img class="img-avatar" src="'.View::asset( isset( $topic->FileSlug ) ? 'files'.$topic->FileSlug : 'images/slide-3.jpg' ).'">';
                        $topic->TotalPost = isset( $topic->TotalPost ) ? $topic->TotalPost.( $topic->TotalPost > 1 ? ' Posts' : ' Post' ) : '0 Post';
                        if( $userinfo->UserID == $topic->UserID ){ $forum['epigram'] = $topic->Epigram; }
                    }
                }

                $scdata = $forumModel->getForumSubCategory( $forum['subcatid'] );
                $forum['scname'] = isset( $scdata[0]->SubCatName ) ? $scdata[0]->SubCatName : '';
                
                $forumModel->doSaveTopicView( $forum['forumid'] );
                $html = self::getOutputForumTopic( $forum, $topics );
                break;

            case 'newtopic':
                $options = $forumModel->getForumCategoryForOptions( $client->ClientProfileID );
                $forums = $forumModel->getForumSummary( $client->ClientProfileID );
                $userID = isset( $userinfo->UserID ) ? $userinfo->UserID : '0';
                $html = self::getOutputForumNewTopic( $userID, $options );
                break;

            default:
                $forums = $forumModel->getForumSummary( $client->ClientProfileID );
                if( isset( $forums ) && count( $forums ) ){
                    foreach( $forums as $k => $v ){
                        foreach( $v as $post ){
                            if( isset( $post->LastPost ) ){
                                $lp = explode( '|', $post->LastPost );
                                $post->Data = array( 'PostedBy' => $lp[0], 'DatePosted' => $lp[1], 'FileSlug' => $lp[2], 'AvatarLink' => '' );
                                $post->Data['AvatarLink'] = '<img src="'.View::asset( isset( $post->Data['FileSlug'] ) ? 'files'.$post->Data['FileSlug'] : 'images/slide-3.jpg' ).'">';
                            }
                        }
                    }
                }
                $html = self::getOutputForumDashboard( $forums );
                break;
        }

        $this->setSession( 'CLIENTINFO', array( $client->CompanyName, $client->ClientProfileID ) );

        header( "Content-type:application/json" );
        echo json_encode( $html );
    }

    public function submittopic()
    {
        $forumModel = $this->load->model( 'forums', true, true );
        $replyID = $forumModel->doSaveTopic();
        header( "Content-type:application/json" );
        echo json_encode( $replyID );;
    }

    /**
     * Construct forum dashboard html table
     * @package
     * @access Private
     * @param (array) $foruma : (required) forum query results
     * @return (string)
     **/
    function getOutputForumDashboard( $foruma )
    {
        $html = '';

        $dashLink = View::url( 'project/forums/dashboard' );
        $dashLink = str_ireplace( '"', '&quot;', $dashLink );

        $newLink = View::url( 'project/forums/newtopic' );
        $newLink = str_ireplace( '"', '&quot;', $newLink );

        $html .= '<section class="breadcrumb">';
            $html .= '<article class="container">';
                $html .= '<div class="row">';
                    $html .= '<div class="col-lg-9">';
                        $html .= '<ul>';
                            $html .= '<li><span class="fa fa-vcard"></span>&nbsp; You are here:</li>';
                            $html .= '<li>Dashboard</li>';
                        $html .= '</ul>';
                    $html .= '</div>';
                    $html .= '<div class="col-lg-3">';
                        $html .= '<a class="pull-right" href="javascript:void(0);" onclick="getForums( \''.$newLink.'\' );"><i class="fa fa-plus"></i> New Topic</a>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</article>';
        $html .= '</section>';

        $html .= '<table class="table table-striped table-bordered table-vcenter table-header-bg">';
        if( isset( $foruma ) ){
            if( count( $foruma ) ){
                foreach( $foruma as $k => $v ){
                    $catname = isset( $v[0] ) ? $v[0]->CatName : '';
            $html .= '<thead>';
                $html .= '<tr>';
                    $html .= '<th colspan="2">'.$catname.'</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:100px;">Topics</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:100px;">Posts</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:200px;">Last Post</th>';
                $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';

                    if( isset( $v ) ){
                        foreach( $v as $f ){
                        $scname = isset( $f->SubCatName ) ? $f->SubCatName : '';
                        $scdesc = isset( $f->SubCatDescription ) ? $f->SubCatDescription : '';
                        $sciconclass = isset( $f->SubCatIconClass ) ? $f->SubCatIconClass : '';
                        $topics = isset( $f->Topics ) ? $f->Topics : '0';
                        $posts = isset( $f->Posts ) ? $f->Posts : '0';
                        $postedby = isset( $f->Data['PostedBy'] ) ? '<i class="text-muted">by</i> '.$f->Data['PostedBy'] : '-';
                        $dateposted = isset( $f->Data['DatePosted'] ) ? '<br>'.date( 'F j, Y', strtotime( $f->Data['DatePosted'] ) ) : '';
                        $cateLink = View::url( "project/forums/category/$f->SubCatID" );
                        $cateLink = str_ireplace( '"', '&quot;', $cateLink );

                $html .= '<tr>';
                    $html .= '<td class="text-center" style="width:75px;">';
                        $html .= '<i class="'.$sciconclass.'"></i>';
                    $html .= '</td>';
                    $html .= '<td class="text-left">';
                        $html .= '<h4 class="h5 font-w600 push-5">';
                            $html .= '<a href="javascript:void(0);" onclick="getForums( \''.$cateLink.'\' )">'.$scname.'</a>';
                        $html .= '</h4>';
                        $html .= '<div class="font-s13 text-muted">'.$scdesc.'</div>';
                    $html .= '</td>';
                    $html .= '<td class="text-center hidden-xs hidden-sm">';
                        $html .= '<a class="font-w600" href="javascript:void(0)">'.$topics.'</a>';
                    $html .= '</td>';
                    $html .= '<td class="text-center hidden-xs hidden-sm">';
                        $html .= '<a class="font-w600" href="javascript:void(0)">'.$posts.'</a>';
                    $html .= '</td>';
                    $html .= '<td class="hidden-xs hidden-sm text-left">';
                        $html .= '<span class="font-s13">';
                            $html .= $postedby.$dateposted;
                        $html .= '</span>';
                    $html .= '</td>';
                $html .= '</tr>';
                        }
                    }
            $html .= '</tbody>';
                }
            } else {
            $html .= '<thead>';
                $html .= '<tr>';
                    $html .= '<th colspan="2"></th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:100px;">Topics</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:100px;">Posts</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:200px;">Last Post</th>';
                $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
                $html .= '<tr>';
                    $html .= '<td colspan="5">';
                        $html .= '<div class="h5 hidden-xs text-left">No data found! Contact the administrator to create Categories and Sub-Categories.</div>';
                    $html .= '</td>';
                    $html .= '<td class="hidden"></td>';
                    $html .= '<td class="hidden"></td>';
                    $html .= '<td class="hidden"></td>';
                    $html .= '<td class="hidden"></td>';
                $html .= '</tr>';
            $html .= '</tbody>';
            }
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * Construct forum category html table
     * @package
     * @access Private
     * @param (array) $foruma    : (required) forum variables
     * @param (array) $categorya : (required) categories query results
     * @return (string)
     **/
    function getOutputForumCategory( $foruma, $categorya )
    {
        $html = '';

        $dashLink = View::url( 'project/forums/dashboard' );
        $dashLink = str_ireplace( '"', '&quot;', $dashLink );

        $newLink = View::url( 'project/forums/newtopic' );
        $newLink = str_ireplace( '"', '&quot;', $newLink );

        $html .= '<section class="breadcrumb">';
            $html .= '<article class="container">';
                $html .= '<div class="row">';
                    $html .= '<div class="col-lg-9">';
                        $html .= '<ul>';
                            $html .= '<li><span class="fa fa-vcard"></span>&nbsp; You are here:</li>';
                            $html .= '<li><a class="link-effect" href="javascript:void(0);" onclick="getForums( \''.$dashLink.'\' );">Dashboard</a></li>';
                            $html .= '<li class="fa fa-angle-right"></li>';
                            $html .= '<li>'.$foruma['scname'].'</li>';
                        $html .= '</ul>';
                    $html .= '</div>';
                    $html .= '<div class="col-lg-3">';
                        $html .= '<a class="pull-right" href="javascript:void(0);" onclick="getForums( \''.$newLink.'\' );"><i class="fa fa-plus"></i> New Topic</a>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</article>';
        $html .= '</section>';

        $html .= '<table id="tableforum" class="table table-striped table-bordered table-vcenter table-header-bg">';

            $html .= '<thead>';
                $html .= '<tr>'; 
                    $html .= '<th class="text-left">'.$foruma['scname'].'</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:100px;">Replies</th>'; 
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:100px;">Views</th>';
                    $html .= '<th class="text-center hidden-xs hidden-sm" style="width:200px;">Last Post</th>';
                $html .= '</tr>';
            $html .= '</thead>';

            $html .= '<tbody>';
                if( isset( $categorya ) && count( $categorya ) ){
                    if( isset( $categorya[1] ) ){
                        foreach( $categorya[1] as $cat ):
                            $title = isset( $cat->Title ) ? $cat->Title : '';
                            $author = isset( $cat->Author ) ? $cat->Author : '';
                            $views = isset( $cat->Views ) ? $cat->Views : '0';
                            $replies = isset( $cat->Replies ) ? $cat->Replies : '0';
                            $lastposter = isset( $cat->LastPoster ) ? '<i class="text-muted">by</i> '.$cat->LastPoster : '-';
                            $lastposterdate = isset( $cat->LastPosterDate ) ? '<br>'.date( 'F j, Y', strtotime( $cat->LastPosterDate ) ) : '';
                            $dateposted = isset( $cat->DatePosted ) ? date('F j, Y', strtotime( $cat->DatePosted ) ) : '-';
                            $pinLink = View::url( "project/forums/category/$cat->SubCatID/$cat->ForumID/2" );
                            $pinLink = str_ireplace( '"', '&quot;', $pinLink );
                            $topicLink = View::url( "project/forums/topic/$cat->ForumID" );
                            $topicLink = str_ireplace( '"', '&quot;', $topicLink );

                $html .= '<tr>'; 
                    $html .= '<td class="text-left no-wrap">';
                        $html .= '<div class="h5 font-w600 push-5 inline-block text-top push-5-t">';
                            if( User::can( 'Administer Forums' ) ){
                            $html .= '<a href="javascript:void(0);" onclick="getForums( \''.$pinLink.'\' );" title="Unpin this"><i class="si si-pin fa-2x text-gray"></i></a>';
                            } else {
                            $html .= '<i class="si si-pin fa-2x"></i>';
                            }
                        $html .= '</div>&nbsp;';
                        $html .= '<div class="inline-block">';
                            $html .= '<a href="javascript:void(0);" onclick="getForums( \''.$topicLink.'\' );">'.$title.'</a>'; 
                            $html .= '<div class="font-s13">'; 
                                $html .= $author.' <i class="text-muted">on '.$dateposted.'</i>';
                            $html .= '</div>';
                        $html .= '</div>';
                    $html .= '</td>';
                    $html .= '<td class="text-center hidden-xs hidden-sm">'; 
                        $html .= '<a class="font-w600" href="javascript:void(0);">'.$replies.'</a>';
                    $html .= '</td>'; 
                    $html .= '<td class="text-center hidden-xs hidden-sm">';
                        $html .= '<a class="font-w600" href="javascript:void(0);">'.$views.'</a>'; 
                    $html .= '</td>';
                    $html .= '<td class="hidden-xs hidden-sm text-left">';
                        $html .= '<span class="font-s13">';
                            $html .= $lastposter.$lastposterdate;
                        $html .= '</span>';
                    $html .= '</td>';
                $html .= '</tr>';
                        endforeach;
                    }
            
                    if( isset( $categorya[0] ) ){
                        foreach( $categorya[0] as $cat ):
                            $title = isset( $cat->Title ) ? $cat->Title : '';
                            $author = isset( $cat->Author ) ? $cat->Author : '';
                            $views = isset( $cat->Views ) ? $cat->Views : '0';
                            $replies = isset( $cat->Replies ) ? $cat->Replies : '0';
                            $lastposter = isset( $cat->LastPoster ) ? '<i class="text-muted">by</i> '.$cat->LastPoster : '-';
                            $lastposterdate = isset( $cat->LastPosterDate ) ? '<br>'.date('F j, Y', strtotime( $cat->LastPosterDate ) ) : '';
                            $dateposted = isset( $cat->DatePosted ) ? date('F j, Y', strtotime( $cat->DatePosted ) ) : '-';
                            $pinLink = View::url( "project/forums/category/$cat->SubCatID/$cat->ForumID/1" );
                            $pinLink = str_ireplace( '"', '&quot;', $pinLink );
                            $topicLink = View::url( "project/forums/topic/$cat->ForumID" );
                            $topicLink = str_ireplace( '"', '&quot;', $topicLink );

                $html .= '<tr>';
                    $html .= '<td class="text-left">';
                            if( User::can( 'Administer Forums' ) && count( $categorya[0] ) > 10 ){
                                if( !isset( $categorya[1] ) || count( $categorya[1] ) < 3 ){
                        $html .= '<a href="javascript:void(0);" onclick="getForums( \''.$pinLink.'\' );" title="Pin this to top"><i class="si si-pin text-gray"></i>&nbsp;</a>';
                                }
                            }
                        $html .= '<a href="javascript:void(0);" onclick="getForums( \''.$topicLink.'\' );">'.$title.'</a>';
                        $html .= '<div class="font-s13">';
                            $html .= $author.' <i class="text-muted">on '.$dateposted.'</i>';
                        $html .= '</div>';
                    $html .= '</td>';
                    $html .= '<td class="text-center hidden-xs hidden-sm">';
                        $html .= '<a class="font-w600" href="javascript:void(0);">'.$replies.'</a>';
                    $html .= '</td>';
                    $html .= '<td class="text-center hidden-xs hidden-sm">';
                        $html .= '<a class="font-w600" href="javascript:void(0);">'.$views.'</a>';
                    $html .= '</td>';
                    $html .= '<td class="hidden-xs hidden-sm text-left">';
                        $html .= '<span class="font-s13">';
                            $html .= $lastposter.$lastposterdate;
                        $html .= '</span>';
                    $html .= '</td>';
                $html .= '</tr>';
                        endforeach;
                    }
                } else {
                $html .= '<tr>';
                    $html .= '<td colspan="4" class="text-left">Be the first one to reply</td>';
                    $html .= '<td class="hidden"></td>';
                    $html .= '<td class="hidden"></td>';
                    $html .= '<td class="hidden"></td>';
                $html .= '</tr>';
                }
            $html .= '</tbody>';

        $html .= '</table>';

        return $html;
    }

    /**
     * Construct forum category html table
     * @package
     * @access Private
     * @param (array) $foruma   : (required) forum variables
     * @param (array) $topica   : (required) topic query results
     * @return (string)
     **/
    function getOutputForumTopic( $foruma, $topica )
    {
        $html = '';

        $dashLink = View::url( 'project/forums/dashboard' );
        $dashLink = str_ireplace( '"', '&quot;', $dashLink );

        $newLink = View::url( 'project/forums/newtopic' );
        $newLink = str_ireplace( '"', '&quot;', $newLink );

        $cateLink = View::url( 'project/forums/category/'.$foruma['subcatid'] );
        $cateLink = str_ireplace( '"', '&quot;', $cateLink );

        $submitLink = View::url( 'project/submittopic' );
        $submitLink = str_ireplace( '"', '&quot;', $submitLink );

        $redirectLink = View::url( 'project/forums/topic/'.$foruma['forumid'] );

        $html .= '<section class="breadcrumb">';
            $html .= '<article class="container">';
                $html .= '<div class="row">';

                    $html .= '<div class="col-lg-9">';
                        $html .= '<ul>';
                            $html .= '<li><span class="fa fa-vcard"></span>&nbsp; You are here:</li>';
                            $html .= '<li><a class="link-effect" href="javascript:void(0);" onclick="getForums( \''.$dashLink.'\' );">Dashboard</a></li>';
                            $html .= '<li class="fa fa-angle-right"></li>';
                            $html .= '<li><a class="link-effect" href="javascript:void(0);" onclick="getForums( \''.$cateLink.'\' );">'.$foruma['scname'].'</a></li>';
                            $html .= '<li class="fa fa-angle-right"></li>';
                            $html .= '<li>'.$foruma['title'].'</li>';
                        $html .= '</ul>';
                    $html .= '</div>';

                    $html .= '<div class="col-lg-3">';
                        $html .= '<a class="pull-right" href="javascript:void(0);" onclick="getForums( \''.$newLink.'\' );"><i class="fa fa-plus"></i> New Topic</a>';
                    $html .= '</div>';

                $html .= '</div>';
            $html .= '</article>';
        $html .= '</section>';

        $html .= '<div class="project-view-forum project-view-forum-topic">';
            $html .= '<input name="image" type="file" id="upload" class="hidden" onchange="">';
            $html .= '<table id="tableforum" class="table table-striped table-borderless table-vcenter table-header-bg push-10-t">';
                
                $html .= '<thead>';
                    $html .= '<tr>';
                        $html .= '<th class="text-left">';
                            $html .= '<h2 class="block-title"> <i class="fa fa-hand-o-right"></i> '.$foruma['title'].'</h2>';
                        $html .= '</th>';
                    $html .= '</tr>';
                $html .= '</thead>';

                $html .= '<tbody>';
                    if( isset( $topica ) && count( $topica ) ){
                        $ctr = 0;
                        foreach( $topica as $topic ){ $ctr++;
                            $fid = isset( $topic->ForumID ) ? $topic->ForumID : '';
                            $author = isset( $topic->Data['Name'] ) ? $topic->Data['Name'] : '';
                            $avatarLink = isset( $topic->Data['AvatarLink'] ) ? $topic->Data['AvatarLink'] : '';
                            $dateposted = isset( $topic->DatePosted ) ? date( 'F j, Y - H:i:s', strtotime( $topic->DatePosted ) ) : '';
                            $totalpost = isset( $topic->TotalPost ) ? $topic->TotalPost : '0';
                            $content = isset( $topic->Content ) ? $topic->Content : '';
                            $epigram = isset( $topic->Epigram ) ? $topic->Epigram : '';

                    $html .= '<tr>';
                        $html .= '<td>';
                            $html .= '<div class="font-s13 text-left">';
                                $html .= $author.' <i class="text-muted">on '.$dateposted.'</i>';
                            $html .= '</div>';
                            $html .= '<div class="clear"></div>';
                            $html .= '<div class="block push-5-t">';
                                $html .= '<div class="wlessp form-left text-center inline-block">';
                                    $html .= '<div class="push-10 push-5-t">';
                                        $html .= '<a href="javascript:void(0)">'.$avatarLink.'</a>';
                                    $html .= '</div>';
                                    $html .= '<small>'.$totalpost.'</small>';
                                $html .= '</div>';
                                $html .= '<div class="wmorep form-right">';
                                    $html .= '<div class="text-left">';
                                        $html .= $content;
                                        $html .= '<div id="form-edit-wrapper" class="hidden">';
                                            $html .= '<form id="formForumEdit" class="form-horizontal input_mask" enctype="multipart/form-data" method="post">';
                                                $html .= '<input type="hidden" name="action" value="edittopic">';
                                                $html .= '<input type="hidden" name="topic[UserID]" value="'.$foruma['userid'].'">';
                                                $html .= '<input type="hidden" name="topic[ForumID]" value="'.$fid.'">';
                                                $html .= '<textarea id="textContentEdit" class="form-control full tinyMCE" name="topic[Content]" cols="30" rows="20" style="width:100%;">'.html_entity_decode( $content ).'</textarea>';
                                                $html .= '<a href="javascript:void(0);" class="btn btn-rounded btn-primary btn-sm push-5-t" onclick="doSubmitEdit( \''.$submitLink.'\', '.'\''.$redirectLink.'\' );"><i class="fa fa-save"></i> SUBMIT</a>&nbsp;';
                                                $html .= '<a href="javascript:void(0);" class="btn btn-rounded btn-danger btn-sm push-5-t" onclick="cancelMyEditedTopic();"><i class="fa fa-close"></i> CANCEL</a>';
                                            $html .= '</form>';
                                        $html .= '</div>';
                                        $html .= $ctr == 1 && ($topic->UserID == $foruma['userid'] || $topic->UserID == '0') ? '<br><div id="btn-edit-wrapper"><a href="javascript:void(0);" class="btn btn-rounded btn-primary btn-sm" onclick="editMyTopic( \''.$fid.'\' );"><i class="fa fa-edit "></i> EDIT</a></div>' : '';
                                        $html .= '<hr>';
                                        $html .= '<p class="font-s13 text-muted hidden">'.$epigram.'</p>';
                                    $html .= '</div>';
                                $html .= '</div>';
                            $html .= '</div>';
                        $html .= '</td>';
                    $html .= '</tr>';
                        }

                    } else {
                    $html .= '<tr>';
                        $html .= '<td>';
                            $html .= '<div class="h5 hidden-xs text-left">No data found!</div>';
                        $html .= '</td>';
                    $html .= '</tr>';
                    }
                $html .= '</tbody';

                $html .= '<tfoot';
                    $html .= '<tr id="forum-reply-form">';

                        $html .= '<td>';
                            $html .= '<div class="font-s13 text-left">';
                                $html .= $foruma['username'].' <i class="text-muted">just now</i>';
                            $html .= '</div>';
                            $html .= '<div class="clear"></div>';
                            $html .= '<div class="block push-5-t">';
                                $html .= '<div class="wlessp form-left text-center inline-block">';
                                    $html .= '<div class="push-10 push-5-t">';
                                        $html .= '<a href="javascript:void(0)">'.$foruma['avatarlink'].'</a>';
                                    $html .= '</div>';
                                    $html .= '<small>'.$foruma['total'].'</small>';
                                $html .= '</div>';
                                $html .= '<div class="wmorep form-right">';
                                    $html .= '<div class="text-left">';
                                        $html .= '<form id="formForumReply" class="form-horizontal input_mask" enctype="multipart/form-data" method="post">';
                                            $html .= '<input type="hidden" name="action" value="reply">';
                                            $html .= '<input type="hidden" name="topic[UserID]" value="'.$foruma['userid'].'">';
                                            $html .= '<input type="hidden" name="topic[ForumID]" value="'.$foruma['forumid'].'" id="ForumID">';
                                            $html .= '<div class="text-left">';
                                                $html .= '<div class="hidden">';
                                                    $html .= '<label for="topic-description">'.Lang::get('FORUM_FRM_WTP').'</label>';
                                                    $html .= '<input class="form-control" type="text" name="topic[Epigram]" value="'.$foruma['epigram'].'" placeholder="It\'s nice to have any words to ponder" style="width:100%">';
                                                $html .= '</div>';
                                                $html .= '<div class="">';
                                                    $html .= '<textarea id="textContent" class="form-control full tinyMCE" name="topic[Content]" cols="30" rows="10" style="width:100%;"></textarea>';
                                                $html .= '</div><br>';
                                                $html .= '<div class="">';
                                                    $html .= '<a href="javascript:void(0)" class="btn btn-primary btn-rounded" onclick="doSubmitReply( \''.$submitLink.'\', '.'\''.$redirectLink.'\' );"><i class="fa fa-reply"></i> Reply</a>';
                                                $html .= '</div>';
                                            $html .= '</div>';
                                        $html .= '</form>';
                                    $html .= '</div>';
                                $html .= '</div>';
                            $html .= '</div>';
                        $html .= '</td>';

                    $html .= '</tr>';
                $html .= '</tfoot>';

            $html .= '</table>';

        $html .= '</div>';

        return $html;
    }

    /**
     * Construct forum new topic html table
     * @package
     * @access Private
     * @param (int) $userID   : (required) current user id
     * @param (array) $options: (required) subcategory list
     * @return (string)
     **/
    function getOutputForumNewTopic( $userID, $options )
    {
        $html = '';

        $dashLink = View::url( 'project/forums/dashboard' );
        $dashLink = str_ireplace( '"', '&quot;', $dashLink );

        $submitLink = View::url( 'project/submittopic' );
        $submitLink = str_ireplace( '"', '&quot;', $submitLink );

        $disabled = isset( $options ) && count( $options ) > 0 ? '' : 'disabled';

        $html .= '<section class="breadcrumb">';
            $html .= '<article class="container">';
                $html .= '<div class="row">';
                    $html .= '<div class="col-lg-9">';
                        $html .= '<ul>';
                            $html .= '<li><span class="fa fa-vcard"></span>&nbsp; You are here:</li>';
                            $html .= '<li><a class="link-effect" href="javascript:void(0);" onclick="getForums( \''.$dashLink.'\' );">Dashboard</a></li>';
                            $html .= '<li class="fa fa-angle-right"></li>';
                            $html .= '<li>Add Topic</li>';
                        $html .= '</ul>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</article>';
        $html .= '</section>';

        $html .= '<div class="block-content">';
            $html .= '<input name="image" type="file" id="upload" class="hidden" onchange="">';
            $html .= '<form id="formForumNewTopic" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">';
                $html .= '<input type="hidden" name="action" value="addtopic">';
                $html .= '<input type="hidden" name="topic[UserID]" value="'.$userID.'">';

                $html .= '<div class="form-group w50p">';
                    $html .= '<div class="form-left inline-block">';
                        $html .= '<div class="text-divider font-w600 push-20-t text-left text-uppercase"><span>Forum Category </span><span class="text-danger">*</span></div>';

                            $html .= '<select class="js-select2 form-control select2-hidden-accessible-show" id="topic[IDs]" name="topic[IDs]" style="width:100%;" data-placeholder="Please select a category" tabindex="-1" aria-hidden="true">';
                                $html .= '<option></option>';

                        if( isset( $options ) && count( $options ) ){
                            foreach( $options as $k => $v){
                                $val = isset( $v ) ? $v : '';
                                $key = isset( $k ) ? $k : '';
                                if( strpos( $k, 'disabled' ) !== false ){
                                $html .= '<option value="" disabled="">'.$val.'</option>';
                                } else {
                                $html .= '<option value="'.$key.'">'.$val.'</option>';    
                                }
                            }
                        }
                            $html .= '</select>';

                    $html .= '</div>';
                    $html .= '<div class="form-right inline-block">';
                        $html .= '<div class="text-divider font-w600 push-20-t text-left text-uppercase"><span>Topic Title </span><span class="text-danger">*</span></div>';
                        $html .= '<input class="form-control" type="text" id="topic-title" name="topic[Title]" placeholder="Please enter a title">';
                    $html .= '</div>';
                $html .= '</div>';
                $html .= '<div class="form-group">';
                    $html .= '<div class="text-divider font-w600 push-20-t text-left text-uppercase"><span>Topic Description </span><span class="text-danger">*</span></div>';
                    $html .= '<input class="form-control" type="text" id="topic-description" name="topic[Description]" placeholder="Please enter a description">';
                $html .= '</div>';
                $html .= '<div class="form-group hidden">';
                    $html .= '<div class="text-divider font-w600 push-20-t text-left text-uppercase"><span>'.Lang::get('FORUM_FRM_WTP').' </span><span class="text-danger">*</span></div>';
                    $html .= '<input class="form-control" type="text" id="topic-epigram" name="topic[Epigram]" placeholder="It\'s nice to have any words to ponder">';
                $html .= '</div>';
                $html .= '<div class="form-group">';
                    $html .= '<textarea class="form-control tinyMCE" name="topic[Content]" id="textContent" cols="30" rows="10"></textarea>';
                $html .= '</div>';
                $html .= '<div class="form-group">';
                    $html .= '<div class="col-xs-12">';
                        $html .= '<a href="javascript:void(0)" class="btn btn-rounded btn-primary" onclick="doSubmitTopic( \''.$submitLink.'\', '.'\''.$dashLink.'\' );"><i class="fa fa-plus"></i> Add Topic</a>';
                        $html .= '<button class="btn btn-rounded btn-default hidden" type="button"><i class="fa fa-eye"></i> Preview</button>';
                        $html .= '<button class="btn btn-rounded btn-default hidden" type="button"><i class="fa fa-floppy-o"></i> Draft</button>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</form>';
        $html .= '</div>';

        return $html;
    }
}