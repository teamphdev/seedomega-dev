/**
 * English localized messages.
 */
var signField_I18N = {
    'sketch.label': "Draw Signature",
    'sketch.clear': "Clear",
    'file.label': "Upload image",
    'file.error.maxSize': "Max size exceeded: {0} > {1}"    
};
