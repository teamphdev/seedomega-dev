
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    $('#client-comments').hide();
    $( document ).ready( function(){
        $('#client-comments').removeClass( 'hidden' );
    });

    $( document ).keypress( function(e){
        if( e.keyCode == 13 ){
            e.preventDefault();
            var reply = $('#ReplyContent').val();
            var comment = $('#CommentContent').val();
            refid = 'REFID'+new Date().getTime();

            if( comment.trim().length > 0 ){
                saveComment( '#formComments', refid );
                appendHTML( comment, 'comment', refid );
            }
            if( reply.trim().length > 0 ){
                saveComment( '#formCommentsReply', refid );
                appendHTML( reply, 'reply', refid );
            }
        }
    });

    $( '#minmax' ).click(function(){
        toggleComments();
    });

    $( '.block-header' ).click(function(){
        toggleComments();
    });

    $( '.comment-reply' ).click( function(e){
        e.preventDefault();
        $( '#formCommentsReply' ).remove(); //remove first the previous form
        $( 'li' ).removeClass( 'referenceComment' ); //remove first the previous classname
        $( this ).closest( 'li' ).find( 'ul li' ).last().addClass( 'referenceComment' );
        commentid = $(this).attr( 'data-id' );
        replyWrapper = '<form id="formCommentsReply" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">';
        replyWrapper += '<input type="hidden" name="action" value="sendcommentdashboard" />';
        replyWrapper += '<input type="hidden" name="ParentID" value="'+commentid+'">';
        replyWrapper += '<input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">';
        replyWrapper += '<input type="hidden" name="ClientProfileID" value="<?php echo $client->ClientProfileID; ?>">';
        replyWrapper += '<div class="form-group role<?php echo $currUser->Level; ?>">';
        replyWrapper += '   <div class="write-comment-image inline-block">';
        replyWrapper += '       <?php echo $currUser->AvatarReplyLink; ?>';
        replyWrapper += '       <?php echo $currUser->Indicator; ?>';
        replyWrapper += '   </div>';
        replyWrapper += '   <div class="write-comment inline-block">';
        replyWrapper += '       <input id="ReplyContent" type="text" value="" class="form-control" placeholder="Write a reply..." name="CommentContent" required="required">';
        replyWrapper += '       <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>';
        replyWrapper += '   </div>';
        replyWrapper += '   <div class="clear"></div>';
        replyWrapper += '</div>';
        replyWrapper += '</form>';

        if( $( this ).closest( 'li' ).find( 'ul li' ) ){
            $( this ).closest( 'li' ).find( 'ul li' ).last().after( replyWrapper );
        } else {
            $( this ).parent().append( replyWrapper );
        }
        $( '#ReplyContent' ).focus();
    });

    $( '.comment-delete' ).click( function(e){
        e.preventDefault();
        var id = $( this ).attr( 'data-id' );
        var cid = $( this ).attr( 'data-cid' );
        deleteComment( id, cid );
    });

    function toggleComments(){
        $('#client-comments').slideToggle();
        autoScroll();
    }

    //save comment to database
    function saveComment( formid, refid ){
        $.ajax({
            type: 'POST',
            url: '/clients/comments/'+refid,
            data: $( formid ).serialize(),
            success: function(){}
        });
    }

    //delete comment from database
    function deleteComment( id, cid ){
        var checkstr =  confirm('are you sure you want to delete this comment?');
        if( checkstr == true ){
            $.ajax({
                type: 'POST',
                url: '/clients/delcomm/'+id,
                data: '',
                success: function(){}
            });
            $( '#'+cid ).remove();
        } else { return false; }
    }

    //append comment/reply for comment box
    function appendHTML( msg, type, refid ){
        var now = new Date();
        now = monthNames[ now.getMonth() ] + ' ' + getGetOrdinal( now.getDate() ) + ', ' + now.getFullYear();

        var name = $('#currName').val();
        var leve = $('#Level').val();
        var link = $('#AvatarLink').val();
        var indi = $('#Indicator').val();
        var html = '<li class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <span class="date sub-text">on '+now+'</span></div></li>';
        html = '<li id="'+refid+'" class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <a href="#" data-id="'+refid+'" class="append-delete">Delete</a> <span>·</span> '+now+'</div></li>';

        if( type == 'comment' ){
            $( '.actionBox > ul' ).append( html );
            $( '#CommentContent' ).val( '' );
        } else {
            $( '.referenceComment' ).removeClass( 'referenceComment' ).after( html );
            $( '#ReplyContent' ).val( '' );
        }

        $( '.append-delete' ).click( function(e){
            e.preventDefault();
            var id = $( this ).attr( 'data-id' );
            deleteComment( id, id );
        });
        autoScroll();
    }

    function autoScroll(){
        $( '#client-comments-content' ).scrollTop( $('#client-comments-content')[0].scrollHeight );
    }

    function getGetOrdinal( n ){
        var s = ["th","st","nd","rd"], v = n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }