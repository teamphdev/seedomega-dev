jQuery(document).ready( function( $ ){
    if( jQuery('#profile_wizard')[0] ){
        if( selectedTab ){
            console.log( selectedTab );
            $( '#profileTabs li' ).each( function(){
                $(this).removeClass( 'current' );
            });
            $( '#profile_wizard' ).find( 'li#tab-'+selectedTab ).addClass( 'current' );
            var id = selectedTab;
            $( '.form-wizard' ).each( function(){
                thisid = $( this ).attr('id');
                if( thisid == id ){ $( this ).addClass( "active" ); }else{ $( this ).removeClass( 'active' ); }
            });
        }
        $( '#modalWizardSuccess' ).modal( {show: false} );
        $( '#btnFirstNext' ).click( function(){
            submitForm( '#formCompanyInfo' );
            return false;
        });
        $( '#btnSecondNext' ).click( function(){
            submitForm( '#formOffer' );
            return false;
        });
        $( '#btnThirdNext' ).click( function(){
            submitForm( '#formDocuments' );
            return false;
        })
        $( '#btnFourthNext' ).click( function(){
            submitForm( '#formBankAccounts' );
            return false;
        })
        $( '#btnFifthNext' ).click( function(){
            submitForm( '#formSummary' );
            return false;
        })
        $( '#btnSubmitData' ).click( function( e ) {
            submitForm( '#formTeams' );
        });
        function submitForm( curForm ){
            var datastring = $( curForm ).serialize();
            $( curForm ).submit();
        }
    }/*end if profile wizrd*/

    if( jQuery('#modal-teamMember')[0] ) {
        $('.showDetails').click(function(e){
           e.preventDefault();
            $('#modal-teamMember').modal('show');
            $('#modal-teamMember').find(".block-content").html('');
            $('#modal-teamMember').find(".block-content").load($(this).attr("href"));
        });
    }
});