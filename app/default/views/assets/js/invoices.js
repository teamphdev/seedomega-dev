jQuery( document ).ready( function( $ ){
    $( '#selClient' ).change( function(){
        if( $( this ).val() != '' ){
            var clientid = $( this ).val();
            $.ajax({
                type: "GET",
                url: '/invoices/getbookings/'+clientid,
                dataType: 'json',
                success: function( response ){
                    $( '#selBooking' ).empty();
                    $.each( response, function( key, value ){
                        $( "#selBooking" ).append( "<option value='"+value.InvestmentBookingID+"'>" + value.BookingDate + " - $" + value.Amount + "</option>");
                    });
                    console.log(response);
                }
            });
        }
    });

    $( '#frmCreateInvoice' ).submit( function( e ){
        e.preventDefault();

        var clientid = $( '#selClient' ).val();
        var bookid = $( '#selBooking' ).val();
        window.location = '/invoices/create/' + clientid + '/'+ bookid;
    });

    $( '.priceInput' ).keypress( function(){
        var $this = $( this );
        window.setTimeout( function(){
            $( '#subtotal' ).val( $this.val() );
            $( '#total' ).val( $this.val() );
        }, 0);
    });
});