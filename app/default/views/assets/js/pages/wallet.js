jQuery(document).ready( function( $ ){
    var userChecker = function(ref, output, loading, carry, wrapper, self) {
        var $url = ref.attr('rel');
        var $cid = ref.attr('data-value');
        var $id = ref.val();
        
        $(loading).show();
        $('#submit-btn').attr('disabled','disabled');
        if($cid != $id) {
            if($id && $id != 0) {
                
                $.ajax({
                    url: $url,
                    method: 'POST',
                    data: {ID:$id},
                    success: function(txt) {
                        var data = jQuery.parseJSON(txt);
                        
                        if(data.UserID != undefined && data.UserID != 0) {
                            $(output).text(data.FirstName+' '+data.LastName);
                            carry.val(data.UserID);
                            carry.attr('value',data.UserID);

                            wrapper.removeClass('has-error');
                            wrapper.addClass('has-success');
                            $('#submit-btn').removeAttr('disabled');
                        } else {
                            wrapper.removeClass('has-success');
                            wrapper.addClass('has-error');

                            $(output).text('Not found, please check User ID or Email Address.'); 
                            carry.val('');
                            carry.attr('value','');
                        }
                        $(loading).hide();                        
                    }
                });
            } else {
                wrapper.removeClass('has-success');
                wrapper.addClass('has-error');

                $(output).text('Please enter User ID or Email Address!'); 
                carry.val('');
                carry.attr('value','');
                ref.val('');
                ref.attr('value','');
                $(loading).hide();
            }
        } else {
            wrapper.removeClass('has-success');
            wrapper.addClass('has-error');
            $(output).text('Not found, please check User ID or Email Address.'); 
            carry.val('');
            carry.attr('value','');
            ref.val('');
            ref.attr('value','');
            $(loading).hide();
        }

        return false;
    };

    $('#UserSearch').change(function(){
        userChecker($('#UserSearch'), $('#userdataSearch'), $('#searchloading'),$('#ToUserID'), $('#wrapchecker'), $(this));
        return false;
    });

});