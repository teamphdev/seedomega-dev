tinymce.init({
  selector: ".tinyMCE",
  branding: false,
  theme: "modern",
  height: '300px',
  paste_data_images: true,
  media_live_embeds: true,
  toolbar1: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
  toolbar2: "print link image preview emoticons",
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace visualblocks visualchars code fullscreen",
    "insertdatetime nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern"
  ],
  image_advtab: true,
  file_picker_callback: function( callback, value, meta ){
    if( meta.filetype == 'image' ){
      $( '#upload' ).trigger( 'click' );
        $( '#upload' ).on( 'change', function(){
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e){
            callback( e.target.result, { alt: '' });
        };
        reader.readAsDataURL(file);
      });
    }
  },
  video_template_callback: function( data ){
   return '<video width="' + data.width
    + '" height="' + data.height + '"'
    + (data.poster ? ' poster="' + data.poster + '"' : '')
    + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n'
    + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
  },
  setup: function( editor ){
    editor.on( 'change', function(){
      tinymce.triggerSave();
    });
  },
  media_scripts: [
   {filter: 'http://media1.tinymce.com'},
   {filter: 'http://media2.tinymce.com', width: 100, height: 200}
  ]
});

tinymce.init({
  selector: ".profileMCE",
  branding: false,
  theme: "modern",
  height: '300px',
  image_advtab: true,
  paste_data_images: true,

  forced_root_block : "",
  force_br_newlines : false,
  force_p_newlines : false,

  toolbar1: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
  toolbar2: "print link image preview emoticons",
  plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace visualblocks visualchars code fullscreen",
    "insertdatetime nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern"
  ],
  file_picker_callback: function( callback, value, meta ){
    if( meta.filetype == 'image' ){
      $( '#upload' ).trigger( 'click' );
        $( '#upload' ).on( 'change', function(){
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e){
            callback( e.target.result, { alt: '' });
        };
        reader.readAsDataURL(file);
      });
    }
  },
  // file_picker_callback: function( callback, value, meta ){
  //   var input = document.createElement( 'input' );
  //   input.setAttribute( 'type', 'file' );
  //   input.setAttribute( 'accept', 'image/*' );
  //   // input.addClass( 'hidden' );
  //   input.onchange = function(){
  //     var file = this.files[0];      
  //     var reader = new FileReader();
  //     reader.onload = function(){
  //       var id = 'blobid'+(new Date()).getTime();
  //       var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
  //       var base64 = reader.result.split(',')[1];
  //       var blobInfo = blobCache.create( id, file, base64 );
  //       blobCache.add( blobInfo );
        
  //       // call the callback and populate the Title field with the file name
  //       callback( blobInfo.blobUri(), { title: file.name } );
  //     };
  //     reader.readAsDataURL(file);
  //   };    
  //   input.click();
  // },
  setup: function( editor ){
    editor.on( 'change', function(){
      tinymce.triggerSave();
    });
  }
});