jQuery( document ).ready( function( $ ){
    $( '#selNames' ).change( function(){
        $( '#Company' ).val('');
        $( '#Position' ).val('');
        $( '#UserID' ).val('');
        $( '#Name' ).val('');
        if( $( this ).val() != '' ){
            var userid = $( this ).val();
            $.ajax({
                type: "GET",
                url: '/manage/getselecteduser/'+userid,
                dataType: 'json',
                success: function( response ){
                    $.each( response, function( key, value ){
                        $( '#Company' ).val( value.CompanyName );
                        $( '#Position' ).val( value.JobTitle );
                        $( '#UserID' ).val( value.UserID );
                        $( '#Name' ).val( value.FirstName + ' ' + value.LastName );
                    });
                    console.log(response);
                }
            });
        }
    });

    $( '#selCompanies' ).change( function(){
        $( '#ClientProfileID' ).val('');
        if( $( this ).val() != '' ){
            var clientid = $( this ).val();
            $( '#ClientProfileID' ).val( clientid );
        }
    });

    // $( '#frmCreateInvoice' ).submit( function( e ){
    //     e.preventDefault();

    //     var clientid = $( '#selClient' ).val();
    //     var bookid = $( '#selBooking' ).val();
    //     window.location = '/invoices/create/' + clientid + '/'+ bookid;
    // });

    // $( '.priceInput' ).keypress( function(){
    //     var $this = $( this );
    //     window.setTimeout( function(){
    //         $( '#subtotal' ).val( $this.val() );
    //         $( '#total' ).val( $this.val() );
    //     }, 0);
    // });
});