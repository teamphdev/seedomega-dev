jQuery( document ).ready( function( $ ){
    $('.viewticket').click(function(el){

        el.preventDefault();
        var ticket_id = $(this).data('id');
        console.log('opening modal');

        $('#modal-ticket').modal('show');
        $( "#modal-ticket" ).find('.modal-content .block').html('<div style="padding:5px;">loading...</div>');
        $( "#modal-ticket" ).find('.modal-content .block').load( '/support/getticket/' + ticket_id);

    });


});