// JavaScript Document

// THESE SCRIPT IS FOR LOADING ICON
window.onbeforeunload = function($) {
  jQuery("#loader").show();
}

window.onload = function($) {
  jQuery("#loader").hide();
}

jQuery(document).ready(function($){
	
	function checkDevice()
	{
		if(jQuery(".nav-header-right").hasClass("mobile")){return 1;}else{return 0;}}
	
	/***********************************************************
	Initialize Custom Scroll Bar
	************************************************************/
	var screenWidth = jQuery( window ).width();
	if(screenWidth<1024){jQuery(".nav-header-right").niceScroll();}
	
	
	/***********************************************************
	Dropdown Menu
	************************************************************/
	if (jQuery(".main-nav ul li")[0]) {
		jQuery('.main-nav ul li').hover(function (){
			jQuery('ul', this).stop(true,false).slideDown(100);
			jQuery('ul li ul', this).css("display","none");
		},function () {
			if(checkDevice()==0){jQuery('ul', this).stop(true,false).slideUp(100);}
		});
	}
	
	

	/***********************************************************
	Responsive Menu
	************************************************************/
	jQuery("#navbar-toggle").click(function(){
		if(jQuery(this).hasClass("collapsed"))
		{
			jQuery(this).removeClass("collapsed");
			jQuery(".nav-header-right").addClass("mobile");	
			jQuery("#navbar-toggle i").removeClass("fa-bars");	
			jQuery("#navbar-toggle i").addClass("fa-minus");
		}
		else
		{
			jQuery(this).addClass("collapsed");
			jQuery(".nav-header-right").removeClass("mobile");	
			jQuery("#navbar-toggle i").removeClass("fa-minus");
			jQuery("#navbar-toggle i").addClass("fa-bars");
		}
		jQuery(".nav-header-right").toggle("slow");
	});
	
	
	/***********************************************************
	Responsive Menu
	************************************************************/
	jQuery(".login-panel a").click(function(){
		if(jQuery(this).hasClass("open"))
		{
			jQuery(this).removeClass("open");
			jQuery(".dropdown-login").fadeOut("fast");
		}
		else
		{
			jQuery(this).addClass("open");
			jQuery(".dropdown-login").fadeIn("fast");
			
			/* Hide search box when login panel open */
			jQuery('.search-box input[type="text"]').stop().fadeOut(100);
			jQuery('.search-box .icon').removeClass("open");
			//jQuery(".search-box").css("border-right","solid 1px #ebebeb");
		}
		return false;
	});
	jQuery("body").click(function() {
		if(checkDevice()==0){
			jQuery(".login-panel a").removeClass("open");
			jQuery(".dropdown-login").fadeOut("fast");
		}
	});
	jQuery('.dropdown-login').click(function(e) {
		e.stopPropagation();
	});
	
	// jQuery(".dropdown-login form input[type='text']" || ".dropdown-login form input[type='password']").focus(function(){
	// 	if(jQuery(this).val()=="Enter User ID"){jQuery(this).val('');}
	// 	if(jQuery(this).val()=="Your password"){jQuery(this).val(''); jQuery(this).attr("type","password");}
	// });
	
	// jQuery(".dropdown-login form input#password").blur(function(){
	// 	if(jQuery(this).val()==""){jQuery(this).attr("type","text"); jQuery(this).val('Your password');}
	// });
	
	// jQuery(".dropdown-login form input[type='text']").blur(function(){
	// 	if(jQuery(this).val()==""){jQuery(this).val('Enter User ID');}
	// });
	
	/* Search box */
	jQuery(".search-box .icon").click(function(){
		if(jQuery(this).hasClass("open"))
		{
			jQuery(this).removeClass("open");
			jQuery(this).parent('form').submit();
		}
		else
		{
			jQuery(this).addClass("open");
			jQuery(".search-box").css("border-right","solid 1px transparent");
			jQuery('.search-box input[type="text"]').stop().fadeIn(100, function(){jQuery(this).css("display","block");});
			
			/* Hide login panel when search box open */
			jQuery(".login-panel a").removeClass("open");
			jQuery(".dropdown-login").fadeOut("fast");
		}
	});
	jQuery("body").click(function() {
		if(checkDevice()==0){
			jQuery('.search-box input[type="text"]').stop().fadeOut(100);
			jQuery('.search-box .icon').removeClass("open");
			//jQuery(".search-box").css("border-right","solid 1px #ebebeb");
		}
	});
	jQuery('.search-box').click(function(e) {
		e.stopPropagation();
	});
	jQuery(".search-box input[type='text']").focus(function(){
		if(jQuery(this).val()=="Search Here"){jQuery(this).val('');}
	});
	jQuery(".search-box input[type='text']").blur(function(){
		if(jQuery(this).val()==""){jQuery(this).val('Search Here');}
	});
	
	
	/***********************************************************
	Revolution Slider Initialization
	************************************************************/
	if (jQuery(".tp-banner")[0]) {
		var revSlider = jQuery('.tp-banner').show().revolution(
		{
			dottedOverlay:"none",
			delay:16000,
			startwidth:1170,
			startheight:632,
			hideThumbs:200,
			
			thumbWidth:100,
			thumbHeight:50,
			thumbAmount:5,
			
			navigationType:"bullet",
			navigationArrows:"solo",
			navigationStyle:"round",
			
			touchenabled:"on",
			onHoverStop:"on",
			
			swipe_velocity: 0.7,
			swipe_min_touches: 1,
			swipe_max_touches: 1,
			drag_block_vertical: false,
									
			parallax:"mouse",
			parallaxBgFreeze:"on",
			parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
									
			keyboardNavigation:"off",
			
			navigationHAlign:"center",
			navigationVAlign:"bottom",
			navigationHOffset:0,
			navigationVOffset:20,
	
			soloArrowLeftHalign:"left",
			soloArrowLeftValign:"center",
			soloArrowLeftHOffset:20,
			soloArrowLeftVOffset:0,
	
			soloArrowRightHalign:"right",
			soloArrowRightValign:"center",
			soloArrowRightHOffset:20,
			soloArrowRightVOffset:0,
					
			shadow:0,
			fullWidth:"on",
			fullScreen:"off",
	
			spinner:"spinner4",
			
			stopLoop:"off",
			stopAfterLoops:-1,
			stopAtSlide:-1,
	
			shuffle:"off",
			
			autoHeight:"off",						
			forceFullWidth:"off",				
									
			hideThumbsOnMobile:"off",
			hideNavDelayOnMobile:1500,						
			hideBulletsOnMobile:"off",
			hideArrowsOnMobile:"off",
			hideThumbsUnderResolution:0,
			
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			startWithSlide:0,
			videoJsPath:"rs-plugin/videojs/",
			fullScreenOffsetContainer: ""	
		});
		revSlider.bind('revolution.slide.onloaded', function() {
			jQuery(".main-slider .btn").css("display","block");
			jQuery(".main-slider .tp-banner .progress-section").css("display","block");
		});
	}
	
	
	/***********************************************************
	Google Map
	************************************************************/
	if (jQuery("#projects-map")[0]) {
		jQuery("#projects-map").gMap({
			 maptype: google.maps.MapTypeId.ROADMAP, 
			 zoom: 14, 
			 markers: [{
				 latitude: 51.52056138207458, 
				 longitude: -0.6469490528106, 
				 html: "<div class='map-info'><img src='assets/images/map_img_4.jpg' width='135' height='70' /><div><strong>New Animal Lab Campaign</strong> <br /> by Erin Bagwell <a href='#'>More about the project</a> </div><div class='clear'></div></div>", 
				 popup: false,
				 flat: true,
				 icon: { 
					 image: "assets/js/maps/icons/animals.png", 
					 iconsize: [58, 67], 
					 iconanchor: [15, 30], 
					 shadow: "assets/js/maps/icons/icon-shadow.png", 
					 shadowsize: [32, 37], 
					 shadowanchor: null}
					},
					{
				 latitude: 51.51997072273623, 
				 longitude: -0.6651669788360596, 
				 html: "<div class='map-info'><img src='assets/images/map_img_3.jpg' width='135' height='70' /><div><strong>Charity hospital</strong> <br /> by Erin Bagwell <a href='#'>More about the project</a> </div><div class='clear'></div></div>",
				 popup: false, 
				 flat: true, 
				 icon: { 
					 image: "assets/js/maps/icons/health.png", 
					 iconsize: [58, 67], 
					 iconanchor: [15, 30], 
					 shadow: "assets/js/maps/icons/icon-shadow.png", 
					 shadowsize: [32, 37], 
					 shadowanchor: null}
					},
					{
				 latitude: 51.5157072273623, 
				 longitude: -0.6901669788360596, 
				 html: "<div class='map-info'><img src='assets/images/map_img_2.jpg' width='135' height='70' /><div><strong>Help us to save the nature</strong> <br /> by Erin Bagwell <a href='#'>More about the project</a> </div><div class='clear'></div></div>",
				 popup: false, 
				 flat: true, 
				 icon: { 
					 image: "assets/js/maps/icons/home.png", 
					 iconsize: [58, 67], 
					 iconanchor: [15, 30], 
					 shadow: "assets/js/maps/icons/icon-shadow.png", 
					 shadowsize: [32, 37], 
					 shadowanchor: null}
					},
					{
				 latitude: 51.51650228812931, 
				 longitude: -0.6114151477813721, 
				 html: "<div class='map-info'><img src='assets/images/map_img_1.jpg' width='135' height='70' /><div><strong>We Can Build Our Church</strong> <br /> by Erin Bagwell <a href='#'>More about the project</a> </div><div class='clear'></div></div>",
				 popup: true, 
				 flat: true, 
				 icon: { 
					 image: "assets/js/maps/icons/community.png", 
					 iconsize: [58, 67], 
					 iconanchor: [15, 30], 
					 shadow: "assets/js/maps/icons/icon-shadow.png", 
					 shadowsize: [32, 37], 
					 shadowanchor: null}
					} 
				], 
			 panControl: false,
			 'infoWindow': null,
			 zoomControl: true, 
			 mapTypeControl: false,
			 scaleControl: false, 
			 streetViewControl: false, 
			 scrollwheel: false, 
			 styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#b8b8b8"},{"saturation":-100},{"gamma":1.50},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}], 
			 onComplete: function() {
				 // Resize and re-center the map on window resize event
				 var gmap = jQuery("#projects-map").data('gmap').gmap;
				 window.onresize = function(){
					 google.maps.event.trigger(gmap, 'resize');
					 jQuery("#projects-map").gMap('fixAfterResize');
				 };
			}
		});
	}
	
	if (jQuery("#contact-map")[0]) {
		jQuery("#contact-map").gMap({
			 maptype: google.maps.MapTypeId.ROADMAP, 
			 zoom: 14, 
			 markers: [{
				 latitude: 51.52056138207458, 
				 longitude: -0.6469490528106, 
				 html: "<div class='map-info'><div><strong>2 Abc Road City, London UK AB12CD</strong> <br /> +44 1234 567890 <a href='#'>demo@stackthemes.net</a> </div><div class='clear'></div></div>", 
				 popup: false,
				 flat: true,
				 icon: { 
					 image: "assets/js/maps/icons/location.png", 
					 iconsize: [58, 67], 
					 iconanchor: [15, 30], 
					 shadow: "assets/js/maps/icons/icon-shadow.png", 
					 shadowsize: [32, 37], 
					 shadowanchor: null}
					}
				], 
			 panControl: false,
			 'infoWindow': null,
			 zoomControl: true, 
			 mapTypeControl: false,
			 scaleControl: false, 
			 streetViewControl: false, 
			 scrollwheel: false, 
			 styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#b8b8b8"},{"saturation":-100},{"gamma":1.50},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}], 
			 onComplete: function() {
				 // Resize and re-center the map on window resize event
				 var gmap = jQuery("#contact-map").data('gmap').gmap;
				 window.onresize = function(){
					 google.maps.event.trigger(gmap, 'resize');
					 jQuery("#contact-map").gMap('fixAfterResize');
				 };
			}
		});
	}
	
	
	/***********************************************************
	Pie Progress Initialization
	************************************************************/
	if (jQuery(".pie_progress")[0]) {
		jQuery('.pie_progress').asPieProgress({
			'namespace': 'pie_progress'
		});
		jQuery(window).on("load scroll", function(d,h) {
			jQuery(".pie_progress").each(function(i) {
				a = jQuery(this).offset().top + jQuery(this).height();
				b = jQuery(window).scrollTop() + (jQuery(window).height()+250);
				if (a < b) {
					jQuery(this).asPieProgress('start');
				}
			});
		});
		/*if(jQuery(window).scrollTop()>300)
		{
			jQuery('.pie_progress').asPieProgress('start');
		}
		else
		{
			jQuery('.tp-banner .pie_progress').asPieProgress('start');
			jQuery(window).scroll(function() {jQuery('.pie_progress').asPieProgress('start');});
		}*/
	}
	
	
	/***********************************************************
	Parallax Section
	************************************************************/
	if (jQuery(".parallax")[0]) {
		var parallax = document.querySelectorAll(".parallax"),speed = 0.5;
		window.onscroll = function() {
			[].slice.call(parallax).forEach(function(el,i) {
				var windowYOffset = window.pageYOffset,elBackgrounPos = "50% " + (windowYOffset * speed) + "px";
				el.style.backgroundPosition = elBackgrounPos;
			});
		};
	}
	
	
	/***********************************************************
	Logo Carousels
	************************************************************/
	if (jQuery(".logo-carousels")[0]) {
		jQuery('.logo-carousels').bxSlider({
			slideWidth: 175,
			minSlides: 5,
			maxSlides: 5,
			moveSlides: 1,
			slideMargin: 15,
			infiniteLoop: true,
			pager: false,
			auto: true
		});
	}
	
	
	/***********************************************************
	Newsletter
	************************************************************/
	if (jQuery(".newsletter")[0]) {
		jQuery(".newsletter form input[type='text']").focus(function(){
			if(jQuery(this).val()=="Enter your e-mail"){jQuery(this).val('');}
		});
		jQuery(".newsletter form input[type='text']").blur(function(){
			if(jQuery(this).val()==""){jQuery(this).val('Enter your e-mail');}
		});
	}
	
	
	/***********************************************************
	Convert images to grayscale for IE only
	************************************************************/
	jQuery(window).load(function() {
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE");
		if (msie > 0) 
		{
			if (jQuery(".backers-avatar")[0]) {jQuery('.backers-avatar ul li img').hoverizr({effect:"grayscale",speedIn:"slow"});}
		}
	});
	
	
	/***********************************************************
	ToolTip
	************************************************************/
	if (jQuery(".backers-avatar")[0]) {
		jQuery('.backers-avatar ul li').tooltip({placement:'auto', viewport:{ "selector": ".backers-avatar ul", "padding": 5 }});
	}
	
	
	/***********************************************************
	Event Count Down
	************************************************************/
	if (jQuery(".time-countdown")[0]) {
		jQuery(".time-countdown").each(function() {
			var year = parseFloat(jQuery(this).attr("data-year"));
	  		var month = parseFloat(jQuery(this).attr("data-month"));
	  		var day = parseFloat(jQuery(this).attr("data-day"));
	  		var final_date = new Date();
			final_date = new Date(year, month-1, day);		  
			jQuery(this).countdown({until: final_date, format: 'DHMS', padZeroes:true});
		});
	}
	
	
	/***********************************************************
	Carousels
	************************************************************/
	if (jQuery(".project-slider-cnt")[0]) {
		jQuery('.project-slider-cnt').bxSlider({
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideMargin: 15,
			infiniteLoop: true,
			pager: false,
			auto: false,
			nextSelector: '#slider-next',
			prevSelector: '#slider-prev',
			nextText: '<i class="fa fa-angle-right"></i>',
			prevText: '<i class="fa fa-angle-left"></i>'
		});
	}
	
	if (jQuery(".project-images")[0]) {
		jQuery('.project-images').bxSlider({
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideMargin: 15,
			mode: 'fade',
			autoHover: true,
			infiniteLoop: true,
			pager: false,
			auto: true
		});
	}
	  
	if (jQuery(".blog-posts-slider")[0]) {
		var screenWidth = window.innerWidth;
		var slidewidth, maxslides;
		var cntWidth = jQuery(".blog-posts").css("width");
		cntWidth = parseFloat(cntWidth.replace("px", ""));
		if(screenWidth<=450){maxslides=1; slidewidth=(cntWidth-30);}
		else if(screenWidth<=1199){maxslides=2; slidewidth=((cntWidth-60)/2);}
		else{maxslides=3; slidewidth=((cntWidth-90)/3);}
		
		var blogSlider = jQuery('.blog-posts-slider').bxSlider({
			minSlides: 1,
			maxSlides: maxslides,
			moveSlides: 1,
			slideMargin: 0,
			slideWidth: slidewidth,
			infiniteLoop: true,
			pager: false,
			auto: false,
			nextSelector: '#blog-next',
			prevSelector: '#blog-prev',
			nextText: '<i class="fa fa-angle-right"></i>',
			prevText: '<i class="fa fa-angle-left"></i>'
		});
		
		jQuery(window).resize(function() {
			var screenWidth = window.innerWidth;
			var slidewidth, maxslides;
			var cntWidth = jQuery(".blog-posts").css("width");
			cntWidth = parseFloat(cntWidth.replace("px", ""));
			if(screenWidth<=450){maxslides=1; slidewidth=(cntWidth-30);}
			else if(screenWidth<=1199){maxslides=2; slidewidth=((cntWidth-60)/2);}
			else{maxslides=3; slidewidth=((cntWidth-90)/3);}
			
			blogSlider.reloadSlider({
				minSlides: 1,
				maxSlides: maxslides,
				moveSlides: 1,
				slideMargin: 0,
				slideWidth: slidewidth,
				infiniteLoop: true,
				pager: false,
				auto: false,
				nextSelector: '#blog-next',
				prevSelector: '#blog-prev',
				nextText: '<i class="fa fa-angle-right"></i>',
				prevText: '<i class="fa fa-angle-left"></i>'
			});
		});
	}
	
	if (jQuery(".tweets-slider")[0]) {
		var tweetSlider = jQuery('.tweets-slider').bxSlider({
			minSlides: 3,
			maxSlides: 3,
			moveSlides: 1,
			slideMargin: 0,
			mode: 'vertical',
			infiniteLoop: true,
			pager: false,
			auto: true
		});
		jQuery(window).resize(function() {
			tweetSlider.reloadSlider({
				minSlides: 3,
				maxSlides: 3,
				moveSlides: 1,
				slideMargin: 0,
				mode: 'vertical',
				infiniteLoop: true,
				pager: false,
				auto: true
			});
		});
	}
	
	if (jQuery(".stories-slider")[0]) {
		jQuery('.stories-slider').bxSlider({
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideMargin: 15,
			mode: 'fade',
			autoHover: true,
			infiniteLoop: true,
			pager: false,
			auto: true,
			speed: 5000,
			pause: 10000
		});
	}
	
	if (jQuery(".pslider")[0]) {
		jQuery('.pslider').bxSlider({
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideMargin: 15,
			mode: 'fade',
			autoHover: true,
			infiniteLoop: true,
			pager: false,
			auto: true
		});
	}
	
	
	/***********************************************************
	Scroll to an element
	************************************************************/
	if (jQuery(".sub-menu ul li a")[0]) {
		jQuery(".sub-menu ul li a").click(function (e){
			var element = jQuery(this).attr("href");
			if (element.indexOf("#") >= 0)
			{
				e.preventDefault();
				jQuery('html, body').animate({scrollTop: jQuery(element).offset().top}, 500);
			}
		});
	}
	
	
	/***********************************************************
	Animated Numbers
	************************************************************/
	var statsDone = true;
	jQuery(window).on("load scroll", function() {
		jQuery(".facts-figures ul li span").each(function(i) {
			a = jQuery(this).offset().top + jQuery(this).height();
			b = jQuery(window).scrollTop() + jQuery(window).height();
			statSep = jQuery.animateNumber.numberStepFactories.separator(',');
			attrStat = jQuery(this).attr('data-count');
			if (a < b) {
				jQuery(this).animateNumber({ 
						number: attrStat,
						numberStep: statSep
					}, 2000
				);
			}
		});
	});
	
	
	/***********************************************************
	Select Box, Radio inputs and checkbox
	************************************************************/
	// jQuery("select").each(function() {
	// 	jQuery(this).selectbox();
	// });
	// jQuery('input').iCheck({
	// 	checkboxClass: 'icheckbox_minimal-blue',
	// 	radioClass: 'iradio_minimal-blue'
	// });

	// Material inputs helper
    var uiForms = function() {
        jQuery('.form-material.floating > .form-control').each(function(){
            var $input  = jQuery(this);
            var $parent = $input.parent('.form-material');

            setTimeout(function() {
                if ($input.val() ) {
                    $parent.addClass('open');
                }
            }, 150);

            $input.on('change', function(){
                if ($input.val()) {
                    $parent.addClass('open');
                } else {
                    $parent.removeClass('open');
                }
            });
        });
    };
	 
	 
	/***********************************************************
	Toggle Search Projects
	************************************************************/
	if (jQuery("#filter-toggle")[0]) {
		jQuery("#filter-toggle").click(function(e){
			if($('section.filter').css('display') == 'none')
		 	{
			 	jQuery('section.filter').stop(true,false).slideDown(300);
		 	}
			 else
		 	{
			 	jQuery('section.filter').stop(true,false).slideUp(300);
		 	}
	 	});
	}
	
	
	/***********************************************************
	Content Tabs
	************************************************************/
	jQuery('.pstabs li a').click(function (e) {
	  e.preventDefault();
	  jQuery(this).tab('show');
	});
	
	
	/***********************************************************
	Costs Bar Chart
	************************************************************/
	if (jQuery(".costs-detail")[0]) {
		var totalCost = 0;
		var costsArray = [];
		var colorArray = [];
		var barChart = "";
		jQuery(".costs-detail ul li").each(function() {
			var iColor = jQuery(this).attr("data-color");
			var cost = jQuery(this).attr("data-cost");
			var iTag = document.createElement('i');
			jQuery(iTag).css("background","#"+iColor);
			jQuery(iTag).prependTo(this);
			
			colorArray.push(iColor);
			costsArray.push(parseFloat(cost));
			totalCost = totalCost+parseFloat(cost);
		});
		for(var i=0; i<=(costsArray.length-1); i++)
		{
			var width = (costsArray[i]/totalCost)*100;
			barChart = barChart+'<div style="background-color:#'+colorArray[i]+'; float:left; width:'+width+'%; height:40px;"></div>';
		}
		barChart = barChart+'<div style="clear:both;"></div>';
		jQuery("#total-barChart").css({"border-radius":"5px", "overflow":"hidden"});
		jQuery("#total-barChart").html(barChart);
	}
	
	/***********************************************************
	Bootstrap Collapse
	************************************************************/
	$('.collapse').collapse({ toggle: false });
	
	$(document).on('click', '.accordion-toggle', function(e) {
		event.preventDefault();
		
		var parent_1 = $(this).parent('div');
		var parent_2 = $(parent_1).parent('div');
		var parent_3 = $(parent_2).parent('div');
		var parent_id = $(parent_3).attr('id');
		
		$('#'+parent_id+'.accordion').find('.accordion-body').collapse('hide');
		$(this).parent().next().collapse('show');
	});

	
	
	/***********************************************************
	On Click Return False
	************************************************************/
	$(document).on('click', '.perk-wrapper ul li.perk-disabled a', function(e) {
		return false;
	});
	
	/***********************************************************
	Start Project PlaceHolder & Values on focus and blur
	************************************************************/
	// $('.start-content form input[type="text"], .start-content form textarea, .form-ui input[type="text"], .form-ui textarea').each(function() {
	// 	var inputValue = $(this).attr('placeholder');
	// 	var input = $(this);
	// 	$(this).val(inputValue);
	// });
	
	// $(document).on('focus', '.start-content form input[type="text"], .start-content form textarea, .form-ui input[type="text"], .form-ui textarea', function(e) {
	// 	var inputValue = $(this).attr('placeholder');
	// 	if($(this).val()==inputValue)
	// 	{
	// 		$(this).val('');
	// 	}
	// });
	
	// $(document).on('blur', '.start-content form input[type="text"], .start-content form textarea, .form-ui input[type="text"], .form-ui textarea', function(e) {
	// 	var inputValue = $(this).attr('placeholder');
	// 	if($(this).val()=='')
	// 	{
	// 		$(this).val(inputValue);
	// 	}
	// });
	
	
	/***********************************************************
	Project Gallery Type
	************************************************************/
	$(document).on('click', '.select-gallery-type ul li', function(e) {
		var galtype = $(this).attr('data-galerytype');
		$(".select-gallery-type ul li").removeClass('active');
		$(this).addClass('active');
		if(galtype=='vid')
		{
			$("#images-inputs").css("display","none");
			$("#video-inputs").css("display","block");
		}
		else if(galtype=='img')
		{
			$("#images-inputs").css("display","block");
			$("#video-inputs").css("display","none");
		}
	});
	/***********************************************************
	Project Gallery File Selection
	************************************************************/
	$(document).on('click', '.selectimage input[type="text"], .selectimage button', function(e) {
		var parentcnt = $(this).parent('div').attr('id');
		$('#'+parentcnt+' input[type="file"]').click();
		$(this).blur();
	});
	$(document).on('change', 'input[type="file"]', function(e) {
		var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
		var parentcnt = $(this).parent('div').attr('id');
		$('#'+parentcnt+' input[type="text"]').val(filename);
	});
	
	
	
	/***********************************************************
	Contact Form Submition
	************************************************************/	
	
	$('#contact-form').submit(function(e){
        //stop the form from being submitted
        e.preventDefault();
        
        /* declare the variables, var error is the variable that we use on the end
        to determine if there was an error or not */
        var error = false;
        var name = $('#name').val();
        var email = $('#email').val();
		
		var phone = $('#number').val();
        
        var message = $('#message').val();
        
        if(name.length == 0 || email.length == 0 || phone.length == 0 || message.length == 0){
            var error = true;
            $('#form-message').fadeIn(500);
			$('#form-message').removeClass("hide");
			$('#form-message').addClass("alert-danger");
			$('#form-message').html("<strong>Error!</strong> Please fill all fields");
        }
        
        //now when the validation is done we check if the error variable is false (no errors)
        if(error == false){
            
            /* using the jquery's post(ajax) function and a lifesaver
            function serialize() which gets all the data from the form
            we submit it to send_email.php */
            $.post("send_mail.php", $("#contact-form").serialize(),function(result){
                //and after the ajax request ends we check the text returned
				
                if(result == 'sent'){
                    //if the mail is sent remove the submit paragraph
                     $('#form-message').removeClass("hide");
                     $('#form-message').removeClass("alert-danger");
                    //and show the mail success div with fadeIn
					$('#form-message').addClass("alert-success");
					$('#form-message').html("Thank you. Your message has been sent.");
                    $('#form-message').fadeIn(500);
                }else{
                    //show the mail failed div
                     $('#form-message').removeClass("hide");
                     $('#form-message').removeClass("alert-success");
					$('#form-message').addClass("alert-danger");
					$('#form-message').html("Sorry, don't know what happened. Try later.");
                    $('#form-message').fadeIn(500);
                }
            });
        }
    });

    /***********************************************************
	Datatables
	************************************************************/
	jQuery('.pstabs li a').click(function (e) {
	  e.preventDefault();
	  jQuery(this).tab('show');
	});	


	/***********************************************************
	Signup
	************************************************************/
	jQuery('.CompleteInfo').click(function (e) {
	  //e.preventDefault();
	  var docomplete = jQuery(this).val();
	  if(docomplete == 'Now') {
	  	jQuery('.CompleteInformation').slideDown();
	  } else {
	  	jQuery('.CompleteInformation').slideUp();
	  }
	});	
	
	/***********************************************************
	Email Duplicate checker
	************************************************************/
	var validateEmail = function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    
	var emailChecker = function(ref) {
        var $url = ref.attr('rel');
        var $msg = ref.attr('checkmessage');
        var $inv = ref.attr('invalidmessage');
        var $id = ref.val();

        if($id != '') {
            if(validateEmail($id)) {
                jQuery('#emailloading').show();
                jQuery.ajax({
                    url: $url,
                    method: 'POST',
                    data: {email:$id},
                    success: function(txt) {
                        jQuery('#emailloading').hide();
                        if(txt != '') {
                            jQuery('#emailcheck-modal-slideup .block-content').html($msg);
                        	jQuery('#emailcheck-modal-slideup').modal('show');
                            //alert($msg);
                            ref.val('');
                        }
                    }
                });
            } else {
                jQuery('#emailcheck-modal-slideup .block-content').html($inv);
	        	jQuery('#emailcheck-modal-slideup').modal('show');
	            ref.val('');
            }
        }
    }

    // While on edit page validate email
    var emailChecker2 = function(ref) {
        var $url = ref.attr('rel');
        var $msg = ref.attr('checkmessage');
        var $inv = ref.attr('invalidmessage');
        var $id = ref.val();
        var $thelink = $url+$id;

        if($id != '') {
            if(validateEmail($id)) {
                jQuery('#emailloading').show();
                jQuery.ajax({
                    url: $thelink,
                    method: 'POST',
                    success: function(txt) {
                        jQuery('#emailloading').hide();
                        if(txt == true) {
                        	jQuery('#emailcheck-modal-slideup .block-content').html($msg);
                        	jQuery('#emailcheck-modal-slideup').modal('show');
                            //alert($msg);
                            ref.val('');
                        }
                    }
                });
            } else {
                //alert($inv);
                //ref.val('');
                jQuery('#emailcheck-modal-slideup .block-content').html($inv);
	        	jQuery('#emailcheck-modal-slideup').modal('show');
	            ref.val('');
            }
        }
    }
    
    jQuery('#EmailChecker').change(function(){
        emailChecker(jQuery(this));
    });

    jQuery('#EmailChecker2').change(function(){
        emailChecker2(jQuery(this));
    });

    if(jQuery( "input" ).hasClass( "money-mask" )){
        jQuery('.money-mask').mask('000,000,000,000,000.00', {reverse: true})
    }

    if(jQuery( "input" ).hasClass( "daterange-filter" )){
        jQuery(".daterange-filter").daterangepicker({
	        showDropdowns: true,
	        linkedCalendars: false,
	        autoUpdateInput: true,
	        locale: {
	          format: 'YYYY-MM-DD',
	          "cancelLabel": "Clear"
	        },
	        ranges: {
	           'This Week': [moment().startOf('week'), moment().endOf('week')],
	           'Last Week': [moment().startOf('week').subtract(7, 'days'), moment().startOf('week').subtract(1, 'days')],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    });
    }

    if(jQuery( "input" ).hasClass( "wallet-datefilter" )){
        jQuery(".wallet-datefilter").daterangepicker({
	        showDropdowns: true,
	        linkedCalendars: false,
	        autoUpdateInput: true,
	        locale: {
	          format: 'YYYY-MM-DD',
	          "cancelLabel": "Clear"
	        },
	        ranges: {	           	           
	           'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],	           
	           'Last 2 Months': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
	           'Last 1 Months': [moment().subtract(1, 'month').startOf('month'), moment().endOf('month')],
	           'Last Week': [moment().startOf('week').subtract(7, 'days'), moment().startOf('week').subtract(1, 'days')],
	           'This Week': [moment().startOf('week'), moment().endOf('week')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
	           'This Month': [moment().startOf('month'), moment().endOf('month')]
	        }
	    });
    }
});

/***********************************************************
Form Wizard Next Back Function
************************************************************/

function moveform(btn, id)
{
	$('.form-wizard').each(function() {
		thisid = $(this).attr('id');
		if(thisid==id){$(this).css("display","block");}else{$(this).css("display","none");}
	});
	
	thisDataLink = $(btn).attr('data-link');
	
	var formstep = 0;
	
	$('.start-project .title ul li').each(function() {
		if(formstep==0)
		{
			if(thisDataLink==$(this).attr('data-link'))
			{
				formstep = 1;
				$(this).addClass("current");
			}
			else
			{
				$(this).removeClass("current");
				$(this).addClass("done");
			}
		}
		else
		{
			$(this).removeClass("done");
			$(this).removeClass("current");
		}
	});
}

/***********************************************************
Add More Perks
************************************************************/

function addperk()
{
	var perkelEments = $("#perk-elements").html();
	$("#add-more-perks").append("<div class='moreperks'>"+perkelEments+"</div>");
}

function addimage()
{
	var imageNumber = parseFloat($('#imgNumber').val());
	$('#imgNumber').val(imageNumber+1);
	var imageEments = '<div class="form-left selectimage image-fld-add" id="imgupload-'+(imageNumber+1)+'"> <input type="text" value="" class="form-control" placeholder="Upload Image"> <button type="button" class="imageUploadBtn">Choose File</button> <input type="file" name="galleryimg[]" /> </div>';
	$("#add-image-field-cnt").append(imageEments);
}


/***********************************************************
Animated Numbers Plugin
************************************************************/
(function(d){var p=function(b){return b.split("").reverse().join("")},l={numberStep:function(b,a){var e=Math.floor(b);d(a.elem).text(e)}},h=function(b){var a=b.elem;a.nodeType&&a.parentNode&&(a=a._animateNumberSetter,a||(a=l.numberStep),a(b.now,b))};d.Tween&&d.Tween.propHooks?d.Tween.propHooks.number={set:h}:d.fx.step.number=h;d.animateNumber={numberStepFactories:{append:function(b){return function(a,e){var k=Math.floor(a);d(e.elem).prop("number",a).text(k+b)}},separator:function(b,a){b=b||" ";a=
a||3;return function(e,k){var c=Math.floor(e).toString(),s=d(k.elem);if(c.length>a){for(var f=c,g=a,l=f.split("").reverse(),c=[],m,q,n,r=0,h=Math.ceil(f.length/g);r<h;r++){m="";for(n=0;n<g;n++){q=r*g+n;if(q===f.length)break;m+=l[q]}c.push(m)}f=c.length-1;g=p(c[f]);c[f]=p(parseInt(g,10).toString());c=c.join(b);c=p(c)}s.prop("number",e).text(c)}}}};d.fn.animateNumber=function(){for(var b=arguments[0],a=d.extend({},l,b),e=d(this),k=[a],c=1,h=arguments.length;c<h;c++)k.push(arguments[c]);if(b.numberStep){var f=
this.each(function(){this._animateNumberSetter=b.numberStep}),g=a.complete;a.complete=function(){f.each(function(){delete this._animateNumberSetter});g&&g.apply(this,arguments)}}return e.animate.apply(e,k)}})(jQuery);


/*
 *  Document   : base_forms_wizard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Form Wizard Page
 */

var BaseFormWizard = function() {
// Init simple wizard, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
var initWizardSimple = function(){
    jQuery('.js-wizard-simple').bootstrapWizard({
        'tabClass': '',
        'firstSelector': '.wizard-first',
        'previousSelector': '.wizard-prev',
        'nextSelector': '.wizard-next',
        'lastSelector': '.wizard-last',
        'onTabShow': function($tab, $navigation, $index) {
	var $total      = $navigation.find('li').length;
	var $current    = $index + 1;
	var $percent    = ($current/$total) * 100;

    // Get vital wizard elements
    var $wizard     = $navigation.parents('.block');
    var $progress   = $wizard.find('.wizard-progress > .progress-bar');
    var $btnPrev    = $wizard.find('.wizard-prev');
    var $btnNext    = $wizard.find('.wizard-next');
    var $btnFinish  = $wizard.find('.wizard-finish');

    // Update progress bar if there is one
	if ($progress) {
        $progress.css({ width: $percent + '%' });
    }

	// If it's the last tab then hide the last button and show the finish instead
	if($current >= $total) {
                $btnNext.hide();
                $btnFinish.show();
	} else {
                $btnNext.show();
                $btnFinish.hide();
	}
        }
    });
};

// Init wizards with validation, for more examples you can check out http://vadimg.com/twitter-bootstrap-wizard-example/
var initWizardValidation = function(){
    // Get forms
    var $form1 = jQuery('.js-form1');
    var $form2 = jQuery('.js-form2');

    // Prevent forms from submitting on enter key press
    $form1.add($form2).on('keyup keypress', function (e) {
        var code = e.keyCode || e.which;

        if (code === 13) {
            e.preventDefault();
            return false;
        }
    });

    // Init form validation on classic wizard form
    var $validator1 = $form1.validate({
        errorClass: 'help-block animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        rules: {
            'validation-classic-firstname': {
                required: true,
                minlength: 2
            },
            'validation-classic-lastname': {
                required: true,
                minlength: 2
            },
            'validation-classic-email': {
                required: true,
                email: true
            },
            'validation-classic-details': {
                required: true,
                minlength: 5
            },
            'validation-classic-city': {
                required: true
            },
            'validation-classic-skills': {
                required: true
            },
            'validation-classic-terms': {
                required: true
            }
        },
        messages: {
            'validation-classic-firstname': {
                required: 'Please enter a firstname',
                minlength: 'Your firtname must consist of at least 2 characters'
            },
            'validation-classic-lastname': {
                required: 'Please enter a lastname',
                minlength: 'Your lastname must consist of at least 2 characters'
            },
            'validation-classic-email': 'Please enter a valid email address',
            'validation-classic-details': 'Let us know a few thing about yourself',
            'validation-classic-skills': 'Please select a skill!',
            'validation-classic-terms': 'You must agree to the service terms!'
        }
    });

    // Init form validation on the other wizard form
    var $validator2 = $form2.validate({
        errorClass: 'help-block text-right animated fadeInDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
            jQuery(e).parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error').addClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        success: function(e) {
            jQuery(e).closest('.form-group').removeClass('has-error');
            jQuery(e).closest('.help-block').remove();
        },
        rules: {
            'validation-firstname': {
                required: true,
                minlength: 2
            },
            'validation-lastname': {
                required: true,
                minlength: 2
            },
            'validation-email': {
                required: true,
                email: true
            },
            'validation-details': {
                required: true,
                minlength: 5
            },
            'validation-city': {
                required: true
            },
            'validation-skills': {
                required: true
            },
            'validation-terms': {
                required: true
            }
        },
        messages: {
            'validation-firstname': {
                required: 'Please enter a firstname',
                minlength: 'Your firtname must consist of at least 2 characters'
            },
            'validation-lastname': {
                required: 'Please enter a lastname',
                minlength: 'Your lastname must consist of at least 2 characters'
            },
            'validation-email': 'Please enter a valid email address',
            'validation-details': 'Let us know a few thing about yourself',
            'validation-skills': 'Please select a skill!',
            'validation-terms': 'You must agree to the service terms!'
        }
    });

    // Init classic wizard with validation
    jQuery('.js-wizard-classic-validation').bootstrapWizard({
        'tabClass': '',
        'previousSelector': '.wizard-prev',
        'nextSelector': '.wizard-next',
        'onTabShow': function($tab, $nav, $index) {
	var $total      = $nav.find('li').length;
	var $current    = $index + 1;

            // Get vital wizard elements
            var $wizard     = $nav.parents('.block');
            var $btnNext    = $wizard.find('.wizard-next');
            var $btnFinish  = $wizard.find('.wizard-finish');

	// If it's the last tab then hide the last button and show the finish instead
	if($current >= $total) {
                $btnNext.hide();
                $btnFinish.show();
	} else {
                $btnNext.show();
                $btnFinish.hide();
	}
        },
        'onNext': function($tab, $navigation, $index) {
            var $valid = $form1.valid();

            if(!$valid) {
                $validator1.focusInvalid();

                return false;
            }
        },
        onTabClick: function($tab, $navigation, $index) {
	return false;
        }
    });

    // Init wizard with validation
    jQuery('.js-wizard-validation').bootstrapWizard({
        'tabClass': '',
        'previousSelector': '.wizard-prev',
        'nextSelector': '.wizard-next',
        'onTabShow': function($tab, $nav, $index) {
	var $total      = $nav.find('li').length;
	var $current    = $index + 1;

            // Get vital wizard elements
            var $wizard     = $nav.parents('.block');
            var $btnNext    = $wizard.find('.wizard-next');
            var $btnFinish  = $wizard.find('.wizard-finish');

	// If it's the last tab then hide the last button and show the finish instead
	if($current >= $total) {
                $btnNext.hide();
                $btnFinish.show();
	} else {
                $btnNext.show();
                $btnFinish.hide();
	}
        },
        'onNext': function($tab, $navigation, $index) {
            var $valid = $form2.valid();

            if(!$valid) {
                $validator2.focusInvalid();

                return false;
            }
        },
        onTabClick: function($tab, $navigation, $index) {
	return false;
        }
    });
};

return {
    init: function () {
        // Init simple wizard
        initWizardSimple();

        // Init wizards with validation
        initWizardValidation();
    }
};
}();

// Initialize when page loads
jQuery(function(){ BaseFormWizard.init(); });

// Initialize Tooltips
jQuery('[data-toggle="tooltip"], .js-tooltip').tooltip({
container: 'body',
animation: false
});

// Initialize Popovers
jQuery('[data-toggle="popover"], .js-popover').popover({
container: 'body',
animation: true,
trigger: 'hover'
});

// Initialize Tabs
jQuery('[data-toggle="tabs"] a, .js-tabs a').click(function(e){
e.preventDefault();
jQuery(this).tab('show');
});

// Init form placeholder (for IE9)
jQuery('.form-control').placeholder();

// Blocks options functionality
var uiBlocks = function() {
    // Init default icons fullscreen and content toggle buttons
    uiBlocksApi(false, 'init');

    // Call blocks API on option button click
    jQuery('[data-toggle="block-option"]').on('click', function(){
        uiBlocksApi(jQuery(this).closest('.block'), jQuery(this).data('action'));
    });
};

// Blocks API
var uiBlocksApi = function($block, $mode) {
    // Set default icons for fullscreen and content toggle buttons
    var $iconFullscreen         = 'si si-size-fullscreen';
    var $iconFullscreenActive   = 'si si-size-actual';
    var $iconContent            = 'si si-arrow-up';
    var $iconContentActive      = 'si si-arrow-down';

    if ($mode === 'init') {
        // Auto add the default toggle icons to fullscreen and content toggle buttons
        jQuery('[data-toggle="block-option"][data-action="fullscreen_toggle"]').each(function(){
            var $this = jQuery(this);

            $this.html('<i class="' + (jQuery(this).closest('.block').hasClass('block-opt-fullscreen') ? $iconFullscreenActive : $iconFullscreen) + '"></i>');
        });

        jQuery('[data-toggle="block-option"][data-action="content_toggle"]').each(function(){
            var $this = jQuery(this);

            $this.html('<i class="' + ($this.closest('.block').hasClass('block-opt-hidden') ? $iconContentActive : $iconContent) + '"></i>');
        });
    } else {
        // Get block element
        var $elBlock = ($block instanceof jQuery) ? $block : jQuery($block);

        // If element exists, procceed with blocks functionality
        if ($elBlock.length) {
            // Get block option buttons if exist (need them to update their icons)
            var $btnFullscreen  = jQuery('[data-toggle="block-option"][data-action="fullscreen_toggle"]', $elBlock);
            var $btnToggle      = jQuery('[data-toggle="block-option"][data-action="content_toggle"]', $elBlock);

            // Mode selection
            switch($mode) {
                case 'fullscreen_toggle':
                    $elBlock.toggleClass('block-opt-fullscreen');

                    // Enable/disable scroll lock to block
                    if ($elBlock.hasClass('block-opt-fullscreen')) {
                        jQuery($elBlock).scrollLock('enable');
                    } else {
                        jQuery($elBlock).scrollLock('disable');
                    }

                    // Update block option icon
                    if ($btnFullscreen.length) {
                        if ($elBlock.hasClass('block-opt-fullscreen')) {
                            jQuery('i', $btnFullscreen)
                                .removeClass($iconFullscreen)
                                .addClass($iconFullscreenActive);
                        } else {
                            jQuery('i', $btnFullscreen)
                                .removeClass($iconFullscreenActive)
                                .addClass($iconFullscreen);
                        }
                    }
                    break;
                case 'fullscreen_on':
                    $elBlock.addClass('block-opt-fullscreen');

                    // Enable scroll lock to block
                    jQuery($elBlock).scrollLock('enable');

                    // Update block option icon
                    if ($btnFullscreen.length) {
                        jQuery('i', $btnFullscreen)
                            .removeClass($iconFullscreen)
                            .addClass($iconFullscreenActive);
                    }
                    break;
                case 'fullscreen_off':
                    $elBlock.removeClass('block-opt-fullscreen');

                    // Disable scroll lock to block
                    jQuery($elBlock).scrollLock('disable');

                    // Update block option icon
                    if ($btnFullscreen.length) {
                        jQuery('i', $btnFullscreen)
                            .removeClass($iconFullscreenActive)
                            .addClass($iconFullscreen);
                    }
                    break;
                case 'content_toggle':
                    $elBlock.toggleClass('block-opt-hidden');

                    // Update block option icon
                    if ($btnToggle.length) {
                        if ($elBlock.hasClass('block-opt-hidden')) {
                            jQuery('i', $btnToggle)
                                .removeClass($iconContent)
                                .addClass($iconContentActive);
                        } else {
                            jQuery('i', $btnToggle)
                                .removeClass($iconContentActive)
                                .addClass($iconContent);
                        }
                    }
                    break;
                case 'content_hide':
                    $elBlock.addClass('block-opt-hidden');

                    // Update block option icon
                    if ($btnToggle.length) {
                        jQuery('i', $btnToggle)
                            .removeClass($iconContent)
                            .addClass($iconContentActive);
                    }
                    break;
                case 'content_show':
                    $elBlock.removeClass('block-opt-hidden');

                    // Update block option icon
                    if ($btnToggle.length) {
                        jQuery('i', $btnToggle)
                            .removeClass($iconContentActive)
                            .addClass($iconContent);
                    }
                    break;
                case 'refresh_toggle':
                    $elBlock.toggleClass('block-opt-refresh');

                    // Return block to normal state if the demostration mode is on in the refresh option button - data-action-mode="demo"
                    if (jQuery('[data-toggle="block-option"][data-action="refresh_toggle"][data-action-mode="demo"]', $elBlock).length) {
                        setTimeout(function(){
                            $elBlock.removeClass('block-opt-refresh');
                        }, 2000);
                    }
                    break;
                case 'state_loading':
                    $elBlock.addClass('block-opt-refresh');
                    break;
                case 'state_normal':
                    $elBlock.removeClass('block-opt-refresh');
                    break;
                case 'close':
                    $elBlock.hide();
                    break;
                case 'open':
                    $elBlock.show();
                    break;
                default:
                    return false;
            }
        }
    }
};

jQuery('#check-toggle').click(function() {
    var c = this.checked;
    jQuery('.capabilities').prop('checked',c);
});

jQuery('.capabilities').click(function(){
    if(!jQuery(this).attr('checked')){
        jQuery('#check-toggle').attr('checked',false);
    }
});

jQuery('#GAAall').click(function() {
    var c = this.checked;
    jQuery('.GAmedia').prop('checked',c);
});

jQuery('.GAmedia').click(function(){
    if(!jQuery(this).attr('checked')){
        jQuery('#GAAall').attr('checked',false);
    }
});

if(jQuery('.blog-editor')[0]){
	jQuery('.blog-editor').richText();
}


if(jQuery('.jsdatetime')[0]){
	jQuery('.jsdatetime').daterangepicker({
	    "singleDatePicker": true,
	    "showDropdowns": true,
	    "timePicker": true,
	    "timePickerSeconds": true,
	    timePickerIncrement: 1,
	    timePicker24Hour: true,
	    "startDate": new Date(),
	    format: 'YYYY-MM-DD HH:mm:ss',
	});
}

if(jQuery('.jsdate')[0]){
	jQuery('.jsdate').daterangepicker({
	    "singleDatePicker": true,
	    "showDropdowns": true,
	    "startDate": new Date(),
	    format: 'YYYY-MM-DD'
	});
}

function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
}

// Booking total calculation by shares
// if(jQuery('#SharesToApply')[0]){

// 	//jQuery('#TotalAmountAttached').val(0);

// 	jQuery('#SharesToApply').change(function(){
// 		total = 0;
// 		var shareprice = jQuery('#shareprice').val();
// 		var SharesToApply = jQuery('#SharesToApply').val();
// 		var minimumbid = jQuery('#minimumbid').val();

// 		total =+ shareprice * SharesToApply;
// 		jQuery('#TotalAmountAttached').val(total);

// 		if(minimumbid > total){
// 			alert('The Minimum Bid is '+commaSeparateNumber(minimumbid));
// 			//jQuery('#SharesToApply').val('');
// 		}
// 	});
// }


jQuery('#TotalAmountAttached').change(function(){


	var shareprice = parseInt(jQuery('#shareprice').val());
	var minimumbid = parseInt(jQuery('#minimumbid').val());
	var maximumbid = parseInt(jQuery('#maximumbid').val());
	var totalamount = parseInt(jQuery(this).val());
	
	if(totalamount < minimumbid) {
		alert('The Minimum Bid is '+commaSeparateNumber(minimumbid));
		var sharetotal = Math.floor(minimumbid / shareprice);
		jQuery(this).val(minimumbid);
		jQuery('#SharesToApply').val(sharetotal);
		jQuery('#calculator').html('<span>'+sharetotal+'<br><small>Shares</small></span> <span>@</span> <span>'+shareprice+'<br><small>Per Share</small></span> <span>=</span> <span>'+minimumbid+'<br><small>Total Value</small></span>');
	} else if(totalamount > maximumbid) {
		alert('The Maximum Bid is '+commaSeparateNumber(maximumbid));
		var sharetotal = Math.floor(maximumbid / shareprice);
		jQuery(this).val(maximumbid);
		jQuery('#SharesToApply').val(sharetotal);
		jQuery('#calculator').html('<span>'+sharetotal+'<br><small>Shares</small></span> <span>@</span> <span>'+shareprice+'<br><small>Per Share</small></span> <span>=</span> <span>'+maximumbid+'<br><small>Total Value</small></span>');
	} else {
		var sharetotal = Math.floor(totalamount / shareprice);
		jQuery('#SharesToApply').val(sharetotal);
		jQuery('#calculator').html('<span>'+sharetotal+'<br><small>Shares</small></span> <span>@</span> <span>'+shareprice+'<br><small>Per Share</small></span> <span>=</span> <span>'+totalamount+'<br><small>Total Value</small></span>');
	}
});

jQuery('#SharesToApply').change(function(){

    var minimumshare = parseInt(jQuery('#minimumShare').val());
    var maxshare = parseInt(jQuery('#maxShare').val());
    var shareprice = parseInt(jQuery('#shareprice').val()).toFixed(2);
    var minimumbid = parseInt(jQuery('#minimumbid').val());
    var maximumbid = parseInt(jQuery('#maximumbid').val());
    var totalamount = parseInt(jQuery('#TotalAmountAttached').val());
    var sharestoapply = parseInt(jQuery('#SharesToApply').val());

    if(sharestoapply < minimumshare) {
        alert('The Minimum Share is '+ minimumshare);
        var sharetotal = Math.floor(minimumbid / shareprice);
        jQuery(this).val(minimumbid);
        jQuery('#SharesToApply').val(sharetotal);
        jQuery('#calculator').html('<span>'+sharetotal+'<br><small>Shares</small></span> <span>@</span> <span>'+shareprice+'<br><small>Per Share</small></span> <span>=</span> <span>'+minimumbid+'<br><small>Total Value</small></span>');
     } else if(sharestoapply > maxshare) {
        alert('The Maximum Share allowed is '+maxshare);
        var sharetotal = Math.floor(maximumbid / shareprice);
        jQuery(this).val(maximumbid);
        jQuery('#SharesToApply').val(minimumshare);
        jQuery('#calculator').html('<span>'+minimumshare+'<br><small>Shares</small></span> <span>@</span> <span>'+shareprice+'<br><small>Per Share</small></span> <span>=</span> <span>'+maximumbid+'<br><small>Total Value</small></span>');
    } else {
        var totalamount = Math.floor(sharestoapply * shareprice);
        jQuery('#TotalAmountAttached').val(totalamount);
        jQuery('#calculator').html('<span>'+sharestoapply+'<br><small>Shares</small></span> <span>@</span> <span>'+shareprice+'<br><small>Per Share</small></span> <span>=</span> <span>'+totalamount+'<br><small>Total Value</small></span>');
    }
    console.log('changed'  + sharestoapply + ' '+ minimumshare);

});

jQuery('#MinimumBid').change(function(){
	var $minbid = parseInt(jQuery(this).val());
	var $target = parseInt(jQuery('#TargetGoal').val());

	if($minbid >= $target) {
		alert('Minimum Bid should not be greater than Target Amount! Please enter amount less than '+$target);
		jQuery(this).val($target * 0.1);
		jQuery(this).focus();
	}

});

jQuery('#MaximumBid').change(function(){
	var $maxbid = parseInt(jQuery(this).val());
	var $minbid = parseInt(jQuery('#MinimumBid').val());
	var $target = parseInt(jQuery('#TargetGoal').val());

	if($maxbid >= $target || $maxbid < $minbid) {
		alert('Maximum Bid should not be greater than Target Amount and lesser than Minimum Bid! Please enter amount between '+$minbid+'-'+ ($target-1));
		jQuery(this).val(parseInt($target * 0.1)+parseInt($minbid));
		jQuery(this).focus();
	}

});

// (parseFloat($(inp).val()) / parseFloat(reserveDispatcher.params.price_per_share))
// recalculate: function () {
// var inp = $('form#allocation_reserve_form #allocation_reserve_form_amount, #allocation_reserve_form_amount2:visible');
// var value = Math.floor(parseFloat((parseFloat($(inp).val()) / parseFloat(reserveDispatcher.params.price_per_share)).toFixed(4)));
// if (value >= reserveDispatcher.params.upper) {
//   value = reserveDispatcher.params.upper;
//   $(inp).val(Number(value * reserveDispatcher.params.price_per_share).formatMoney(4, '.', ','));
// }
// $('#allocation_reserve_form span#amount-limit.control-element span.sharescnt, #share2').html(value);
// }

// Switching show/hide fields on client section
var doclientfieldsswitch = function(ref){
	switch( ref ){
		case '4': //for client
			$("#userclient").slideUp();
        	$(".client-profiles-section").slideDown();
        	break;

		case '6':
    		$("#userclient").slideDown();
        	$(".client-profiles-section").slideUp();
			break;

		default:
        	$("#userclient").slideUp();
        	$(".client-profiles-section").slideUp();
			break;
	}
};

if(jQuery('#userlevelopt')[0]){
	doclientfieldsswitch(jQuery('#userlevelopt').val());
	jQuery('#userlevelopt').change(function () {        
	    doclientfieldsswitch(jQuery(this).val());
	});
}

// Switching show/hide fields on booking section
var doreceiptswitch = function(ref){
    if (ref == "Cheque") 
    {
        $(".cheque-opt").slideDown();
        $(".ttreceipt-opt").slideUp();
    } 
    else 
    {
        $(".cheque-opt").slideUp();
        $(".ttreceipt-opt").slideDown();
    }
};

//Disable radio buttons function on Check Disable radio button 
if(jQuery('.receiptopt')[0]){

	jQuery('.receiptopt').change(function () {        
        doreceiptswitch(jQuery(this).val());
    });

	jQuery('.receiptopt').each(function(){
        if(this.checked === true) {
            doreceiptswitch(jQuery(this).val());
        }
    });
}

// Table tr double click redirect
// $('.table.focusRow tr').dblclick(function () { 
//     window.location.href = $(this).data('link');
// });
jQuery('.table.focusRow tr').click(function () { 
    window.location.href = jQuery(this).data('link');
    ///window.open(jQuery(this).data('link'),'_blank');
});
jQuery('.table.focusRow tr').click(function () {
    jQuery('.table tr').removeClass('highlight');
    jQuery(this).addClass('highlight');
});

//Scroll to Bottom/Top function on div tags
function autoScroll(el){
	var h = $(el).css("height").replace("px","");
	$(el).scrollTop(h);
	// $(el).animate({ scrollTop : h }, 2000);
	//$(id).animate({scrollTop:$(id)[0].scrollHeight},2000);
}
function scrollToBottom(id) {
	var div = document.getElementById(id);
	div.scrollTop = div.scrollHeight - div.clientHeight;
}
function scrollToTop(id) {
	var div = document.getElementById(id);
	div.scrollTop = 0;
}

if( jQuery( '#modal-global' )[0] ){
    $( '.showDetails' ).click( function( e ){
       e.preventDefault();
        $( '#modal-global' ).modal( 'show' );
        $( '#modal-global' ).find( ".block-content" ).html( '' );
        $( '#modal-global' ).find( ".block-content" ).load( $( this ).attr( "href" ) );
    });
}