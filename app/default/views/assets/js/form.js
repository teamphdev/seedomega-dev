$(document).ready(function() {

    $('#CommentID').keypress(function (e) {
        var key = e.which;
        var comment = $('#CommentID').val().trim();
        if(key == 13) { // the enter key code
            if(comment.length > 0) {
                //alert(comment);

                //var $baseurl = $(this).attr('rel');
                //var $id = $(this).val();
                //var $thelink = $baseurl+$id;

                alert($(this).attr('rel'));

                $.ajax({
                    url: $thelink,
                    method: 'POST',
                    type: 'JSON',
                    success: function(msg){
                        //alert(msg);
                        var $htmlOut = '<option selected="selected">Please select option</option>';
                        var $res = JSON.parse(msg);
                        for($i = 0; $i < $res.length; $i++) {
                            if($res[$i]['Code'] == "USD"){
                                $month = "months";
                            }else{
                                $month = "月";
                            }

                            // Remove B7 Product Item with ID 10462
                            if($res[$i]['ProductItemID'] != '10462'){
                                $htmlOut += '<option value="'+$res[$i]['ProductItemID']+'|'+$res[$i]['StepUp']+'|'+$res[$i]['InvestmentAmount']+'|'+$res[$i]['TenureMonths']+'">'+$res[$i]['ProductItemTitle']+' '+commaSeparateNumber($res[$i]['InvestmentAmount'])+' / '+$res[$i]['TenureMonths']+' '+$month+'</option>';
                            }
                            
                        }
                        $('#couponAmount').html('');
                        $('#ProductItemID').html($htmlOut);
                    }
                });
                
            }
            return false;
        }
    });
    
     var doaccountswitch = function(ref){
        if (ref == "Individual") 
        {
            $(".acounttype_corporate").slideUp();
            $(".acounttype_individual").slideDown();
        } 
        else 
        {
            $(".acounttype_corporate").slideDown();
            $(".acounttype_individual").slideUp();
        }
    };

    //Disable radio buttons function on Check Disable radio button 
    $('.naccount').change(function () {        
        doaccountswitch($(this).val());
    });
    
    $('.naccount').each(function(){
        if(this.ckecked === true) {
            doaccountswitch($(this).val());
        }
    });
    
    doaccountswitch($('#theAccountType, .naccount').val());
    resizeSign();

    function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
    }

    $('#ProductID').change(function(){
        
        var $baseurl = $(this).attr('rel');
        var $id = $(this).val();
        var $thelink = $baseurl+$id;
        $.ajax({
            url: $thelink,
            method: 'POST',
            type: 'JSON',
            success: function(msg){
                //alert(msg);
                var $htmlOut = '<option selected="selected">Please select option</option>';
                var $res = JSON.parse(msg);
                for($i = 0; $i < $res.length; $i++) {
                    if($res[$i]['Code'] == "USD"){
                        $month = "months";
                    }else{
                        $month = "月";
                    }

                    // Remove B7 Product Item with ID 10462
                    if($res[$i]['ProductItemID'] != '10462'){
                        $htmlOut += '<option value="'+$res[$i]['ProductItemID']+'|'+$res[$i]['StepUp']+'|'+$res[$i]['InvestmentAmount']+'|'+$res[$i]['TenureMonths']+'">'+$res[$i]['ProductItemTitle']+' '+commaSeparateNumber($res[$i]['InvestmentAmount'])+' / '+$res[$i]['TenureMonths']+' '+$month+'</option>';
                    }
                    
                }
                $('#couponAmount').html('');
                $('#ProductItemID').html($htmlOut);
            }
        });
           
    });
  
    var $baseurl = $('#ProductID').attr('rel');
    var $id = $('#ProductID').val();
    var $ProductItemID = $('#theProductItemID').val();
    var $thelink = $baseurl+$id;
    $.ajax({
        url: $thelink,
        method: 'POST',
        type: 'JSON',
        success: function(msg){
            //alert(msg);
            var $htmlOut = '<option selected="selected">Please select option</option>';
            var $res = JSON.parse(msg);
            for($i = 0; $i < $res.length; $i++) {
                if($res[$i]['Code'] == "USD"){
                    $month = "Months";
                }else{
                    $month = "月";
                }

                if($res[$i]['ProductItemID'] == $ProductItemID){
                    $htmlOut += '<option value="'+$res[$i]['ProductItemID']+'|'+$res[$i]['StepUp']+'|'+$res[$i]['InvestmentAmount']+'|'+$res[$i]['TenureMonths']+'" selected>'+$res[$i]['ProductItemTitle']+' '+commaSeparateNumber($res[$i]['InvestmentAmount'])+' / '+$res[$i]['TenureMonths']+' '+$month+'</option>';
                }else{
                    // Remove B7 Product Item with ID 10462
                    if($res[$i]['ProductItemID'] != '10462'){
                        $htmlOut += '<option value="'+$res[$i]['ProductItemID']+'|'+$res[$i]['StepUp']+'|'+$res[$i]['InvestmentAmount']+'|'+$res[$i]['TenureMonths']+'">'+$res[$i]['ProductItemTitle']+' '+commaSeparateNumber($res[$i]['InvestmentAmount'])+' / '+$res[$i]['TenureMonths']+' '+$month+'</option>';
                    }
                }
            }
            $('#ProductItemID').html($htmlOut);
        
        }
    });

    var $basePrdurl = $('#ProductItemID').attr('rel');
    var $thePrdlink = $basePrdurl+$ProductItemID;
    $.ajax({
        url: $thePrdlink,
        method: 'POST',
        type: 'JSON',
        success: function(msg){
            //alert(msg);
            var $ress = JSON.parse(msg);
            for($ii = 0; $ii < $ress.length; $ii++) {
                if($ress[$ii]['CouponAmount'] != ''){
                    $htmlCouponAmt = '<strong>Coupon Amount :</strong> '+$ress[$ii]['CouponAmount'];
                    $TotalMonths = $ress[$ii]['TenureMonths'];
                }else{
                    $htmlCouponAmt = '';
                    $TotalMonths = '';
                }
            }

            $('#couponAmount').html($htmlCouponAmt);
            $('#totalmonths').val($TotalMonths);

            // setMaturityByCommence($comDateID, $totalMonths, $MaturityID)
            
            setMaturityByCommence('#commencedate', '#totalmonths', '#MatuDate');

        }
    });
   
    $('#ProductItemID').change(function(){
        
        var $step = $(this).val();
        var $vals = $step.split('|');
        $('#theProductItemID').val($vals[0]);
        $('#StepUp').val($vals[1]);
        $('#Invested').val($vals[2]);
        $('#totalmonths').val($vals[3]);

        var $basePrdurl = $(this).attr('rel');
        var $prid = $vals[0];
        var $thePrdlink = $basePrdurl+$prid;
        $.ajax({
            url: $thePrdlink,
            method: 'POST',
            type: 'JSON',
            success: function(msg){
                //alert(msg);
                var $ress = JSON.parse(msg);
                for($ii = 0; $ii < $ress.length; $ii++) {
                    if($ress[$ii]['CouponAmount'] != ''){
                        $htmlCouponAmt = '<strong>Coupon Amount :</strong> '+$ress[$ii]['CouponAmount'];
                        $TotalMonths = $ress[$ii]['TenureMonths'];
                    }else{
                        $htmlCouponAmt = '';
                        $TotalMonths = '';
                    }
                }

                $('#couponAmount').html($htmlCouponAmt);
                $('#totalmonths').val($TotalMonths);

                // setMaturityByCommence($comDateID, $totalMonths, $MaturityID)
                setFieldName("#commencedate", 'data-name');
                setFieldName("#MatuDate", 'data-name');
                setMaturityByCommence('#commencedate', '#totalmonths', '#MatuDate');
            }
        });
  
    }); 

    $('#commencedate').on('change', function() {
        // setMaturityByCommence($comDateID, $totalMonths, $MaturityID)
        setFieldName("#commencedate", 'data-name');
        setFieldName("#MatuDate", 'data-name');
        setMaturityByCommence('#commencedate', '#totalmonths', '#MatuDate');
    });

    $('#accPaymentReceived').on('change', function() {
        setFieldName("#accPaymentReceived", 'data-name');
    });

    $('#DepositedAmount').change(function(){
        var $thisVal  = $(this).val();
        var $stepVal  = $('#StepUp').val();
        var $Invested = $('#Invested').val();
    
        if($thisVal % $stepVal) {
            // if($Invested == 20000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 20,000, 30,000, 40,000...");
            // }else if($Invested == 50000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 50,000, 60,000, 70,000...");
            // }else if($Invested == 100000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 100,000, 150,000, 200,000...");  
            // }else if($Invested == 300000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 300,000, 350,000, 400,000..");  
            // }else if($Invested == 500000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 500,000, 600,000, 700,000...");  
            // }else if($Invested == 1000000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 1,00,000, 1,100,000, 1,200,000..");  
            // }else if($Invested > 1000000){
            //     alert("Cumulative amount must follow by minimum "+commaSeparateNumber($stepVal)+" USD. E.g.: 1,100,000; 1,200,000; 1,300,000...");  
            // }

            if($Invested == 20000 || $Invested == 30000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 2萬, 3萬, 4萬...");
                $(this).val("");
            }else if($Invested == 50000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 5萬, 6萬, 7萬...");
                $(this).val("");
            }else if($Invested == 100000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 10萬, 15萬, 20萬...");  
                $(this).val("");
            }else if($Invested == 300000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 30萬, 35萬, 40萬..."); 
                $(this).val(""); 
            }else if($Invested == 500000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 50萬, 60萬, 70萬...");
                $(this).val("");
            }else if($Invested == 1000000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 100萬, 110萬, 120萬...");
                $(this).val("");
            }else if($Invested > 1000000){
                alert("累積金額必須至少 "+commaSeparateNumber($stepVal)+" USD. 例: 100萬, 110萬, 120萬...");
                $(this).val("");
            }

            
        }
    });

    
   
        
    //$(".acounttype_corporate").slideUp();
    //$(".acounttype_individual").slideDown();
    
    $('.cneedcoolingoff').change(function() {
        if($(this).val() == 'Y') {
            $('#WaiveCoolingOffE1').prop('checked', true);
        } else {
            $('#WaiveCoolingOffE2').prop('checked', true);
        }
    });
    $('.eneedcoolingoff').change(function() {
        if($(this).val() == 'Y') {
            $('#WaiveCoolingOffC1').prop('checked', true);
        } else {
            $('#WaiveCoolingOffC2').prop('checked', true);
        }
    });
    
    $('#UBODeclarationConfirm').change(function() {
        if($(this).val() == 'Y') {
            $('#UBODeclarationConfirm1').prop('checked', true);
        } else {
            $('#UBODeclarationConfirm').prop('checked', false);
        }
    });
    $('#UBODeclarationConfirm1').change(function() {
        if($(this).val() == 'Y') {
            $('#UBODeclarationConfirm').prop('checked', true);
        } else {
            $('#UBODeclarationConfirm1').prop('checked', true);
        }
    });
    
    $('.uboswitch').click(function() {
        var $id = $(this).attr('rel');
        
        if(this.checked == true) {
            $($id).slideDown();
        } else {
            $($id).slideUp();
        }
    });
    
    $('.uboswitch').each(function(){
        var $id = $(this).attr('rel');
        
        if(this.checked == true) {
            $($id).slideDown();
        } else {
            $($id).slideUp();
        }
    });

    
    $('.isdatepicker').daterangepicker({
        format: 'YYYY-MM-DD',
        singleDatePicker: true,
        calender_style: "picker_3",
        showDropdowns: true
    }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
    });
    

    // initialize the validator function
    // validator.message.date = 'not a real date';

    //   // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    // $('form')
    //     .on('blur', 'input[required], input.optional, select.required', validator.checkField)
    //     .on('change', 'select.required', validator.checkField)
    //     .on('keypress', 'input[required][pattern]', validator.keypress);

    // $('.multi.required').on('keyup blur', 'input', function() {
    //     validator.checkField.apply($(this).siblings().last()[0]);
    // });

});