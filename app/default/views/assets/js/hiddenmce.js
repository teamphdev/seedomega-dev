var hiddenMCEconfig = {
    selector: '.hiddenMCE',
    branding: false,
    menubar: false,
    toolbar: false,
    statusbar: false,
    setup: function( editor ){
        editor.on( 'change', function(){
            tinymce.triggerSave();
        });
    }
};