<?php 
View::$title = 'Manage Inquiries';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<!-- <section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section> -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>">Contact Us</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <!-- <div class="block-header">
                <h3 class="block-title">All Capability Groups <small></small><a class="btn btn-success pull-right" href="<?php echo View::url('capabilitygroups/add'); ?>">Add Group</a></h3>
            </div> -->
            <div class="block">
                <?php echo View::getMessage(); ?>                
                <table class="table table-divide js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter">
                    <thead>
                        <tr class="headings">
                            <th class="text-center">ID</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Content</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if(count($inquiries)) {
                            foreach($inquiries as $data) { $cntr++;
                                switch ($data->Status) {
                                    case 'Pending':
                                        $status = '<span class="text-warning"><i class="fa fa-clock-o push-5-r"></i> Pending</span>';
                                        break;
                                    case 'Closed':
                                        $status = '<span class="text-muted"><i class="fa fa-check push-5-r"></i> Closed</span>';
                                        break;
                                    case 'In Progress':
                                        $status = '<span class="text-info"><i class="fa fa-cogs push-5-r"></i> In Progress</span>';
                                        break;
                                }
                            ?>
                            <tr>
                                <td class="text-center text-muted">
                                    #<?php echo $data->InquiryID; ?>
                                </td>
                                <td class="font-w600"><?php echo isset($data->FullName) ? ucwords($data->FullName) : "-" ?></td>
                                <td>
                                    <a class="font-w600" data-toggle="modal" data-target="#modal-message" href="#"><?php echo substr(strip_tags($data->Subject),0,52) . ""; ?></a>
                                    <div class="text-muted push-5-t"><?php echo substr(strip_tags($data->Message),0,60) . "..."; ?></div>
                                </td>
                                <td class="visible-lg font-w600">
                                   <?php echo isset($status) ? $status : ""; ?>
                                </td>
                                <td class="text-muted hidden-xs">
                                    <em><?php echo AppUtility::time_elapsed_string($data->DateAdded); ?></em>
                                </td>
                                <td class="text-center">
                                    <?php if( User::can( 'Administer All' ) ){ ?>
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo View::url( 'contactus/edit/'.$data->InquiryID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                    <li><a href="<?php echo View::url( 'contactus/delete/'.$data->InquiryID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this inquiry?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <a href="<?php echo View::url( 'contactus/edit/'.$data->InquiryID ); ?>" class="btn btn-sm btn-rounded btn-default" title="" data-toggle="tooltip">Edit</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php }
                        }else{ ?>
                            <tr>
                                <td class="text-center text-muted" colspan="6">
                                    Currently No Inquiries
                                </td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        
                        <!-- <tr>
                            <td class="text-center">
                                <label class="css-input css-checkbox css-checkbox-primary">
                                    <input type="checkbox"><span></span>
                                </label>
                            </td>
                            <td class="font-w600">Susan Elliott</td>
                            <td>
                                <a class="font-w600" data-toggle="modal" data-target="#modal-message" href="#">Your subscription was updated</a>
                                <div class="text-muted push-5-t">We are glad you decided to go with a vip..</div>
                            </td>
                            <td class="font-w600">
                                <span class="text-muted"><i class="fa fa-check push-5-r"></i> Closed</span>
                            </td>
                            <td class="text-muted">
                                <em>10 min ago</em>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <label class="css-input css-checkbox css-checkbox-primary">
                                    <input type="checkbox"><span></span>
                                </label>
                            </td>
                            <td class="font-w600">George Stone</td>
                            <td>
                                <a class="font-w600" data-toggle="modal" data-target="#modal-message" href="#">Update is available</a>
                                <div class="text-muted push-5-t">An update is under way for your app..</div>
                            </td>
                            <td class="text-info font-w600">
                                <i class="fa fa-cogs push-5-r"></i> In Progress
                            </td>
                            <td class="text-muted">
                                <em>25 min ago</em>
                            </td>
                            <td>
                                <a href="#" title="Edit" class="btn btn-xs btn-default" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                <a href="#" title="Delete" class="btn btn-xs btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete data?');"><i class="fa fa-times"></i></a>
                            </td>
                        </tr> -->
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>