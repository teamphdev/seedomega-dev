<?php 
View::$title = 'Inquiry';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?> #<?php echo $inquiry->InquiryID; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>">Contact Us</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li>My Inquiry</li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="white">
   <article class="container contactpage">
      <div class="row row-header">
         <div class="form-inq col-lg-8">
            <?php echo View::getMessage(); ?>
          <form class="form-ui" method="post">
            <div class="contact-sec">
              <label>Date: <?php echo date('F j, Y', strtotime($inquiry->DateAdded)); ?></label>
                <?php  
                switch ($inquiry->Status) {
                    case 'Pending':
                        $status = '<i class="fa fa-clock-o text-warning"> Pending</i>';
                        break;
                    case 'Closed':
                        $status = '<i class="fa fa-check text-muted"> Closed</i>';
                        break;
                    case 'In Progress':
                        $status = '<i class="fa fa-cogs text-info"> In Progress</i>';
                        break;
                } echo $status; ?>
            </div>
            <div id="basic-data" class="form-wizard active">
              <div class="form-group">
                 <div class="form-left">
                    <label>Full Name</label>
                    <input type="text" value="<?php echo isset($inquiry->FullName) ? $inquiry->FullName : ""; ?>" class="form-control" readonly>
                 </div>
                 <div class="form-right">
                    <label>Email</label>
                    <input type="email" value="<?php echo isset($inquiry->Email) ? $inquiry->Email : ""; ?>" class="form-control" readonly>
                 </div>
                 <div class="clear"></div>
              </div>
              <div class="form-group">
                 <div class="form-left">
                    <label>Subject</label>
                    <input type="text" value="<?php echo isset($inquiry->Subject) ? $inquiry->Subject : ""; ?>" class="form-control" readonly>
                 </div>
                 <div class="form-right">
                    <label>Phone No.</label>
                    <input type="text" value="<?php echo isset($inquiry->Phonenumber) ? $inquiry->Phonenumber : ""; ?>" class="form-control" readonly>
                 </div>
                 <div class="clear"></div>
              </div>
              <div class="form-group">
                 <label>Message</label>
                 <textarea class="form-control" readonly><?php echo isset( $inquiry->Message ) ? $inquiry->Message : ""; ?></textarea>
              </div>
              <?php if( isset( $inquiry->Reply ) ){ ?>
                <div class="form-group">
                   <textarea class="form-control" readonly><?php echo $inquiry->Reply; ?></textarea>
                </div>
              <?php } ?>
            </div>
          </form>
         </div>
      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>