<?php 
View::$title = 'Contact Us';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<!-- section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li>Contact Us</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section -->

<!-- ************************ Page Content ************************ -->
<section class="gray">
  <article class="container contact-section">
    <div class="row">
      <h3>Contact Us</h3>
      <div class="col-lg-12 contact-details">
        <div class="col-lg-6 contact-text">
          <p>Let us know what's on your mind at the moment, just fill up the form below.</p>
        </div>
        <div class="col-lg-6 contact-info">
          <div class="col-lg-6 col-sm-6 c-address">
            <i class="fa fa-map-marker green"></i> 18 Marina Boulevard <br />#35-08 Singapore 018980
          </div>
          <div class="col-lg-6 col-sm-6 c-address">
            <i class="fa fa-phone yellow"></i> +65 6651 4886 <br />
            <i class="fa fa-envelope red"></i> info@seedomega.com
          </div>
        </div>
      </div>

      <div class="col-lg-12 contact-form">
        <div class="col-lg-6">
          <?php echo View::getMessage(); ?>
          <form class="form-ui" method="post">
            <input type="hidden" name="action" value="contactus">
                <div id="basic-data" class="form-wizard active">
                
                  <div class="form-group">
                    <div class="form-block">
                      <label>Full Name:</label>
                      <input type="text" value="" class="form-control" name="mail[FullName]" required="required">
                    </div>
                    <div class="form-block">
                      <label>Email:</label>
                      <input type="email" value="" class="form-control" name="mail[Email]" required="required">
                    </div>
                    <div class="form-block">
                      <label>Contact Number:</label>
                      <input type="text" value="" class="form-control" name="mail[Phonenumber]" id="number">
                    </div>
                    <div class="form-block">                        
                      <label>Subject:</label>
                      <input type="text" value="" class="form-control" name="mail[Subject]" required="required">
                    </div>
                    <div class="form-block"> 
                      <label>Your Message:</label>
                      <textarea name="mail[Message]" class="form-control"></textarea>
                    </div>
                  </div>

                  <div class="form-group text-center">
                    <!--label class="css-input css-checkbox css-checkbox-info">
                        <input type="checkbox" value="" required="required"> <span></span> I/We Read and Understood the above
                    </label>
                    <br-->
                     <button type="submit" class="btn btn-rounded btn-primary">Send</button>
                  </div>
               </div>
            </form>
        </div>
        <div class="col-lg-6 contact-map">
          <div class="map-container">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d838.5467857441427!2d103.8545847345918!3d1.279689250166237!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da190fe6a6865f%3A0xeb5008402f55a4f2!2s18+Marina+Blvd%2C+35%2C+Singapore+018980!5e0!3m2!1sen!2sph!4v1532499265394" width="100%" height="475" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>