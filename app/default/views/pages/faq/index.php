<?php 
View::$title = 'Frequently Asked Questions';
View::$bodyclass = '';
View::header(); 
$param = array();
$param['view'] = 'index';
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<!-- <section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li>FAQ</li>
          </ul>
        </div>
      </div>
    </article>
</section> -->

<section class="gray">
    <!-- Page Content -->
    <article class="container">
        <div class="block-header"><h5 class="text-uppercase"><?php echo View::$title; ?></h5></div>
        <div class="block-content">
        
          <div class="row">
              <div class="accordion" id="accordion-faq">
                 <?php 
                 $cntr = 0;
                 if( count( $faqPosts ) ){
                 foreach( $faqPosts as $article ){ $cntr++; ?>
                 <div class="accordion-group push-5-l push-10-t">
                    <div class="accordion-heading">
                       <a class="accordion-toggle" data-toggle="collapse" href="#collapse<?php echo $article->FaqID; ?>">
                         <?php echo $article->FaqTitle; ?>
                         <span class="updown-cion"></span>
                       </a>
                    </div>
                    <div id="collapse<?php echo $article->FaqID; ?>" class="accordion-body collapse">
                       <div class="push-20 push-20-l pad-60-r">
                          <i class="fa fa-caret-right"></i> <?php echo $article->FaqContent; ?>
                       </div>
                    </div>
                 </div>
                 <hr>
                 <?php } 
                 } ?>               
              </div>
          </div>

        </div>        
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>
