<?php 
    View::$title = 'FAQ';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
    $param = array();
    $param['view'] = 'list';
    $userinfo = User::info(); 
?>

<?php View::page('faq/head'); ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block">
                <?php echo View::getMessage(); ?> 
                <table class="table table-divide js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('faq/add'); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Add Faq</a>'>
                    <thead>
                        <tr class="headings">
                            <th class="text-center">ID</th>
                            <th class="">Info</th>
                            <th class="text-center">Status</th>
                            <th class="text-center sort-this">Public</th>
                            <th class="text-center no-sorting" style="width: 5%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if(count($faqPosts)) {
                            foreach($faqPosts as $article) { $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td><?php echo $article->FaqID; ?></td>
                            <td><strong><?php echo $article->FaqTitle; ?></strong><div class="clearfix"></div>
                                <div class="col-md-10">
                                    <?php echo $article->FaqContent; ?>
                                </div><div class="clearfix"></div>
                            </td>

                            <td><?php echo $article->FaqStatus; ?></td>
                            <td>
                                <?php switch($article->PublicView) { case 1: echo 'General'; break; case 4: echo 'Clients'; break; case 7: echo 'Backers'; break; default: echo 'No'; break; } ?>
                            </td>

                            <td class="text-center">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'faq/edit/'.$article->FaqID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'faq/delete/'.$article->FaqID ); ?>" title="Delete FAQ" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this FAQ?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'faq/edit/'.$article->FaqID ); ?>" class="btn btn-sm btn-default btn-rounded" title="" data-toggle="tooltip">Edit</a>
                                    </div>                                    
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                            } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="5">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>