<?php 
$s0 = isset(View::$segments[0]) ? View::$segments[0] : false; 
$s1 = isset(View::$segments[1]) ? View::$segments[1] : false; 
$s2 = isset(View::$segments[2]) ? View::$segments[2] : false; 
?>

<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
                    <?php if($s1 != 'manage') { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url('faq/manage'); ?>">FAQ</a></li>
                    <?php } ?>
                    <li class="fa fa-angle-right"></li>
                    <li><?php echo View::$title; ?></li>                    
                </ul>
            </div>
            <?php if(isset($view) && $view == 'list') { ?>
            <div class="col-lg-6 align-right sub-menu">
                
            </div>
            <?php } ?>
        </div>
    </article>
</section>
*/ ?>