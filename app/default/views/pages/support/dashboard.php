<?php 
    View::$title = 'Support Dashboard';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1>We are always here to Help!</h1>
            <p>The new way to invest in innovative business. We want to ensure our clients have the best opportunity for success, so we offer support before, during and after the offer for companies and investors.</p>
        </div>
    </article>
</section>

<section class="gray">
    <article class="container projects-page animated bounceIn">
        <?php echo View::getMessage(); ?>      
            
        <?php if( User::is( 'Client' ) || User::is( 'User' )): ?>
            <ul class="push-20 no-padding" style="list-style: none;">
                <li>
                    <a href="/support/create" class="btn btn-rounded btn-default green text-uppercase">
                        <i class="fa fa-fw fa-plus-circle push-5-r"></i> Create New Ticket
                    </a>
                </li>
            </ul>
        <?php endif; ?>

        <div class="block">
            <ul class="nav nav-tabs tabs-bg" >
                <?php
                    $urgentMenuItem = '';
                    $openMenuItem = '';
                    $closedMenuItem = '';
                    switch($segment2){
                        case 'urgent': $urgentMenuItem = 'active'; $openMenuItem = '';break;
                        case 'open': default: $urgentMenuItem = ''; $openMenuItem = 'active';break;
                        case 'closed': $closedMenuItem = 'active';$openMenuItem = ''; $urgentMenuItem='';break;
                    }
                ?>

                <?php if( User::is( 'Administrator' ) || User::is( 'Customer Service' ) ): ?>
                <li class="<?php echo $urgentMenuItem;?>">
                    <a href="/support/dashboard/urgent">
                        <span class="badge pull-right"><?php echo $urgentTicketsCount;?></span><i class="fa fa-fw fa-warning push-5-r"></i> Urgent
                    </a>
                </li>
                <?php endif; ?>

                <li class="<?php echo $openMenuItem;?>">
                    <a href="/support/dashboard/open">
                        <span class="badge pull-right"><?php echo $openTicketsCount;?></span><i class="fa fa-fw fa-folder-open-o push-5-r"></i> Open
                    </a>
                </li>
                <li class="<?php echo $closedMenuItem;?>">
                    <a href="/support/dashboard/closed">
                        <span class="badge pull-right"><?php echo $closedTicketsCount;?></span><i class="fa fa-fw fa-folder-o push-5-r"></i> Closed
                    </a>
                </li>
                

            </ul>
            <div class="tab-content">
                <div class="tab-pane fade fade-up in active cleanfilter">
                    <table class="table table-hover table-divide table-vcenter focusRow dataTable-cleanfilter no-header">
                        <thead class="hidden">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php if ($tickets): foreach($tickets as $tic): ?>
                            <tr class="" data-id="<?php echo $tic->TicketId;?>" data-link="<?php echo View::url('support/view/'.$tic->TicketId); ?>">
                                <td class="h5 text-center" style="width: 150px;">#SO-TCK-<?php echo $tic->TicketId;?></td>
                                <td class="hidden-xs hidden-sm hidden-md text-center" style="width: 100px;">
                                    <?php
                                    switch($tic->Status){
                                        case 'OPEN': $StatClass = 'success'; break;
                                        case 'CLOSED': $StatClass = 'default'; break;
                                    }
                                    ?>
                                    <span class="label label-<?php echo $StatClass;?>"><?php echo $tic->Status;?></span>
                                </td>
                                <td>
                                    <div class="h4"><?php echo ($tic->Title) ? $tic->Title : '';?></div>
                                    <div class="text-muted">
                                        <small>
                                            <?php echo AppUtility::time_elapsed_string($tic->CreatedAt);?> by <a href="javascript:void(0)"><?php echo ($tic->FirstName) ? $tic->FirstName : '';?> <?php echo ($tic->LastName) ? $tic->LastName : '';?></a>
                                        </small>
                                    </div>
                                </td>
                                <td class="hidden-xs hidden-sm hidden-md text-muted" style="width: 150px;">
                                    <?php echo $tic->Priority;?>
                                </td>
                                <td class="hidden-xs hidden-sm hidden-md text-muted" style="width: 150px;">
                                    <?php if ($tic->FileAttch): ?>
                                        <i class="fa fa-fw fa-2x fa-paperclip"></i>
                                    <?php endif; ?>
                                </td>
                                <td class="hidden-xs hidden-sm hidden-md text-center" style="width: 60px;">
                                    <span class="badge badge-primary"><i class="fa fa-comments-o"></i> <?php echo $tic->TotalReplies;?></span>
                                </td>
                            </tr>
                            <?php endforeach; else: ?>
                            <tr>
                                <td>No tickets found.</td>
                            </tr>
                            <?php endif; ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<!-- <div class="modal fade" id="modal-ticket" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">

            </div>
        </div>
    </div>
</div> -->