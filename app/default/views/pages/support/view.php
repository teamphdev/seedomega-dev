<?php 
    View::$title = 'View Ticket';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo View::url(); ?>">Home</a></li>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]); ?>/"><?php echo View::$segments[0]; ?></a></li>
                    <?php if( isset(View::$segments[1]) ) { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-6 align-right">
                <!-- <ul>
                    <li><a href="<?php echo View::url('users/add'); ?>" class="btn btn-sm btn-rounded btn-success" data-toggle="tooltip" title="Add User"><i class="si si-user-follow"></i> Add User</a></li>
                    <li><a href="<?php echo View::url('users/trashbin'); ?>" class="btn btn-sm btn-rounded btn-warning" data-toggle="tooltip" title="Trash Bin"><i class="si si-trash"></i> Trash Bin</a></li>
                </ul> -->
            </div>
        </div>
    </article>
</section>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <span class="label label-success">Open</span>
                        </li>
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">#TCK0014</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="nav-users">
                                <li>
                                    <a href="base_pages_profile_v2.html">
                                        <img class="img-avatar" src="assets/img/avatars/avatar2.jpg" alt="">
                                        <i class="fa fa-circle text-success"></i> Helen Silva
                                        <div class="font-w400 text-muted"><small><i class="fa fa-user"></i> Client</small></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="nav-users">
                                <li>
                                    <a href="base_pages_profile_v2.html">
                                        <img class="img-avatar" src="assets/img/avatars/avatar9.jpg" alt="">
                                        <i class="fa fa-circle text-success"></i> <span class="text-amethyst">Roger Hart</span>
                                        <div class="font-w400 text-muted"><small><i class="fa fa-support"></i> Support</small></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="block-content block-content-full block-content-mini bg-gray-light">
                    <span class="text-muted pull-right"><em>40 min ago</em></span>
                    <span class="font-w600">Payment issue with Paypal</span> by
                    <a href="javascript:void(0)">Helen Silva</a>
                </div>
                <div class="block-content">
                    <p>Hi there, I'm getting the following error when trying to buy the product. Could you please help me out?</p>
                    <p>
                        <code>Error code 589: Please contact support</code>
                    </p>
                </div>
                <div class="block-content block-content-full block-content-mini bg-gray-light">
                    <span class="text-muted pull-right"><em>10 min ago</em></span>
                    <span class="font-w600">Re: Payment issue with Paypal</span> by
                    <a class="text-amethyst" href="javascript:void(0)">Roger Hart</a>
                </div>
                <div class="block-content">
                    <p>Hi there, thanks for contacting support!</p>
                    <p>We are really sorry about the inconvenience, there was an issue with our payment getaway. It is now resolved, so could you please try again one more time?</p>
                    <p>Thank you</p>
                </div>
                <div class="block-content block-content-full block-content-mini bg-gray-light">
                    <i class="fa fa-fw fa-plus"></i> <span class="font-w600">New Reply</span>
                </div>
                <div class="block-content">
                    <form class="form-horizontal" action="base_pages_tickets.html" method="post" onsubmit="return false;">
                        <div class="form-group push-10">
                            <div class="col-xs-12">
                                <textarea class="form-control" rows="4" placeholder="Your answer.."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button class="btn btn-sm btn-default" type="submit">
                                    <i class="fa fa-fw fa-reply text-success"></i> Reply
                                </button>
                                <button class="btn btn-sm btn-default" type="reset">
                                    <i class="fa fa-fw fa-repeat text-danger"></i> Reset
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="block-content block-content-full bg-gray-lighter clearfix">
                    <button class="pull-right btn btn-sm btn-rounded btn-noborder btn-primary" type="button">
                        <i class="fa fa-fw fa-check"></i> Mark as resolved
                    </button>
                    <button class="btn btn-sm btn-rounded btn-noborder btn-warning" type="button">
                        <i class="fa fa-fw fa-warning"></i> Mark as urgent
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>