<?php 
    View::$title = 'View Ticket';
    View::$bodyclass = User::info('Sidebar');
View::header( false,'header_ajaxcontent' );
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<div class="support-popup">

    <div class="block-header bg-primary-dark">
        <?php $labelClass = ($ticket->Status =='OPEN') ? 'success':'default';?>
        <ul class="block-options">
            <li>
                <span class="label label-<?php echo $labelClass;?>"><?php echo $ticket->Status;?></span>
            </li>
            <li>
                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
            </li>
        </ul>
        <h3 class="block-title">#SO-TCK-<?php echo $ticket_id;?></h3>
    </div>

    <div class="block-content block-content-full">

        <ul class="nav-users sp-avatar push-20">
            <li>
                <?php View::photo((isset($ticket->avatarImg) ? 'files/'.$ticket->avatarImg : '/images/user.png'),"Avatar","img-avatar img-avatar-thumb"); ?>
                <i class="fa fa-circle text-success"></i> <?php echo ($ticket->FirstName)? $ticket->FirstName : '';?> <?php echo ($ticket->LastName)? $ticket->LastName : '';?>
            </li>
        </ul>

        <div class="sp-header">
            <h4><?php echo $ticket->Title;?></h4>
            <div class="sp-desc">
                <?php echo nl2br($ticket->Description);?><br>
                <small class=""><?php echo AppUtility::time_elapsed_string($ticket->CreatedAt);?></small>
            </div>            
        </div>

        <!--attachments-->
            <?php if ($ticket->FileAttch):
                $file = View::common()->getUploadedFiles( $ticket->FileAttch );
                ?>
                <div class="block-content block-content-full block-content-mini bg-success-light">
                    <?php $fileURL = View::asset( ( isset( $file[0] ) ? 'files'.$file[0]->FileSlug : '#'), "" ); ?>
                    <i class="fa fa-fw fa-paperclip"></i> <span><a href="<?php echo $fileURL;?>" target="_Blank" class="html5lightbox text-muted"><?php echo $file[0]->FileName;?></a></span>
                </div>
            <?php endif; ?>
        <!--end attachments-->

        <div class="sp-replies">
            <ul class="commentList">
                <?php if ($replies) : foreach($replies as $reply): ?>

                    <li class="clear">
                        <div class="commenterImage">
                           <?php View::photo((isset($reply->avatarImg) ? 'files/'.$reply->avatarImg : '/images/user.png'),"Avatar","avatar img-avatar-thumb"); ?>                               
                        </div>
                        <div class="commentText">
                            <div class="commentName"><?php echo $reply->FirstName . ' '.$reply->LastName; ?> <br><span><?php echo date('M jS, Y', strtotime( $reply->EventDate ) ) ?></span></div>
                            <div class="commentReply"><?php echo nl2br($reply->Content);?></div>
                        </div>
                    </li>

                <?php endforeach; endif; ?>                
            </ul>

        </div>

        <?php if ($ticket->Status == 'OPEN') : ?>

            <form class="form-horizontal" action="/support/savereply" method="post">
                <input type="hidden" name="action" value="savereply">
                <input type="hidden" name="reply[UserId]" value="<?php echo $userinfo->UserID;?>">
                <input type="hidden" name="reply[TicketId]" value="<?php echo $ticket_id;?>">
                <input type="hidden" name="reply[UserLevel]" value="<?php echo $level;?>">

                <div class="form-group push-20">
                    <div class="">
                        <textarea class="form-control" rows="4" name="reply[Content]" placeholder="Your answer.."></textarea>
                    </div>
                </div>

                <div class="row push-20">
                    <div class="col-lg-8 col-xs-8">                        
                        <a class="btn btn-sm btn-rounded btn-noborder btn-warning" href="/support/mark/<?php echo $ticket_id;?>/urgent">
                            <i class="fa fa-fw fa-warning"></i> Mark as urgent
                        </a>
                        <a class=" btn btn-sm btn-rounded btn-noborder btn-primary" href="/support/mark/<?php echo $ticket_id;?>/resolved">
                            <i class="fa fa-fw fa-check"></i> Mark as resolved
                        </a>
                    </div>
                    <div class="col-lg-4 col-xs-4 text-right">
                        <button class="btn btn-sm btn-rounded btn-default" type="submit">
                            <i class="fa fa-fw fa-reply text-success"></i> Reply
                        </button>
                        <button class="btn btn-sm btn-rounded btn-default" type="reset">
                            <i class="fa fa-fw fa-repeat text-danger"></i> Reset
                        </button>
                    </div>
                </div>
            </form>
            
        <?php else: ?>
            <div class="block-content block-content-full block-content-mini bg-gray-light">

            </div>
        <?php endif; ?>

    </div>

</div>


<!-- /page content -->