<?php
View::$title = 'View Ticket';
View::$bodyclass = User::info('Sidebar');
View::header();
?>
<?php $userinfo = User::info(); ?>
    <!-- page content -->

    <section class="breadcrumb">
        <article class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul>
                        <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                        <li><a href="<?php echo View::url(); ?>">Home</a></li>
                        <li class="fa fa-angle-right"></li>
                        <li><a href="<?php echo View::url(View::$segments[0]); ?>/"><?php echo View::$segments[0]; ?></a></li>
                        <?php if( isset(View::$segments[1]) ) { ?>
                            <li class="fa fa-angle-right"></li>
                            <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-lg-6 align-right">
                    <!-- <ul>
                    <li><a href="<?php echo View::url('users/add'); ?>" class="btn btn-sm btn-rounded btn-success" data-toggle="tooltip" title="Add User"><i class="si si-user-follow"></i> Add User</a></li>
                    <li><a href="<?php echo View::url('users/trashbin'); ?>" class="btn btn-sm btn-rounded btn-warning" data-toggle="tooltip" title="Trash Bin"><i class="si si-trash"></i> Trash Bin</a></li>
                </ul> -->
                </div>
            </div>
        </article>
    </section>
    <section class="gray">
        <!-- Page Content -->
        <div class="container animated fadeInDown">
            <div class="modal-content sp-popup">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <div class="label label-success">Open</div>
                            </li>
                        </ul>
                        <h3 class="block-title">Open New Ticket</h3>
                    </div>

                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="saveticket">
                    <input type="hidden" name="ticket[UserId]" value="<?php echo $userinfo->UserID;?>">
                    <input type="hidden" name="ticket[Status]" value="OPEN">

                        <div class="block-content block-content-full">
                            <div class="row">
                                <div class="col-sm-8">
                                    <ul class="nav-users sp-avatar push-20">
                                        <li>
                                            <div class="ticket-avatar">
                                                <?php $avatar = View::common()->getUploadedFiles( $userinfo->Avatar ); ?>
                                                <?php echo View::photo( ( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'), "Avatar","img-avatar img-avatar-thumb" ); ?>

                                                <i class="fa fa-circle text-success"></i> <?php echo ($userinfo->FirstName!='')? $userinfo->FirstName : '';?> <?php echo ($userinfo->LastName!='')? $userinfo->LastName : '';?>
                                                <div class="font-w400 text-muted"><small><i class="fa fa-user"></i> <?php echo $userinfo->UserLevel;?></small></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group push-10">
                                        <div class="col-xs-12">
                                            <label>Urgency</label>
                                            <select name="ticket[Priority]" class="form-control">
                                                <option value="LOW">Low</option>
                                                <option value="MEDIUM">Medium</option>
                                                <option value="URGENT">Urgent</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block-content">                        
                            <div class="form-group push-10">
                                <div class="col-xs-12">
                                    <input type="text" name="ticket[Title]" value="" class="form-control" placeholder="Title..." required>
                                </div>
                            </div>
                            <div class="form-group push-10">
                                <div class="col-xs-12">
                                    <textarea class="form-control" rows="4" placeholder="Your message.." name="ticket[Description]" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label>Attachment</label>
                                    <input id="attachment" class="file form-control" type="file" data-min-file-count="0" name="FileAttch" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12 text-center">
                                    <button class="btn btn-success btn-rounded" type="submit" value="create">Submit New Ticket</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
    <!-- /page content -->
<?php View::footer(); ?>