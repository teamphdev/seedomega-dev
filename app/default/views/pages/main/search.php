<?php 
View::$title = 'Welcome to '.Config::get('SITE_TITLE');
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
  
<section class="main-slider">
  <article class="container">

    <main id="main-container" style="min-height: 877px;">
      <div class="content bg-gray-lighter">
        <div class="row items-push">
          <div class="col-sm-7">
            <h1 class="page-heading">Search Results
              <!-- <small>Vital page found in most projects.</small> -->

              <div class="search-results border-right">
                <form id="formSearch" action="/search" enctype="multipart/form-data" method="post" onsubmit="if(document.getElementById('thesearchkey2').value == ''){ return false; } else { return true; }">
                  <input type="hidden" name="action" value="sendsearch" />
                  <div>
                    <input id="thesearchkey2" type="text" value="" name="keyword" placeholder="Search projects...">
                    <span class="icon2 fa fa-search transition-color" title="Search Projects" onclick="doSearch();"></span>
                  </div>
                </form>
              </div>

            </h1>
          </div>
          <div class="col-sm-5 text-right hidden-xs">
            <ol class="push-10-t">
              <!-- <li>SeedOmega</li> -->
              <!-- <li><a class="link-effect" href="">Search Results</a></li> -->
            </ol>
          </div>
        </div>
      </div>
      <div class="content">
        <div class="block">

          <?php
            $ctr = 0;
            $count = 0;
            if( isset( $searchresults ) ){ $count = count( $searchresults ) ? count( $searchresults ) : 0; } ?>

          <div class="block-content tab-content bg-white">
            <div class="tab-pane fade fade-up active in" id="search-projects">
              <div class="border-b push-30">
                <h2 class="push-10"><?php echo $count; ?> <span class="h5 font-w400 text-muted"><?php echo $count < 2 ? 'Project' : 'Projects'; ?> Found</span></h2>
              </div>

              <?php if( $count > 0 ){ ?>
                <table class="table table-hover table-header-bg table-vcenter table-bordered js-table-checkable js-dataTable-simple" style="width: 100%;">
                  <thead>
                    <tr>
                      <th><i class="fa fa-suitcase text-gray"></i> Project Name</th>
                      <th class="text-center hidden-xs" style="width: 17%;"><i class="fa fa-share-alt text-gray"></i> Type of Offer</th>
                      <th class="text-center hidden-xs" style="width: 15%;"><i class="si si-users text-gray"></i> Investors</th>
                      <th class="text-center" style="width: 20%;"><i class="fa fa-money text-gray"></i> Total Funds</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if( count( $searchresults ) ){
                      foreach( $searchresults as $result ){
                        switch( $result->Status ){
                          case 'Approved':
                              $classicon = 'fa fa-check';
                              $classbg = 'success';
                              break;
                          case 'Pending':
                              $classbg = 'warning';
                              $classicon = 'si si-clock';
                              break;
                          default: break;
                        } ?>
                      <tr>
                        <td>
                          <h3 class="h5 font-w600 push-10">
                            <a class="link-effect" href="<?php echo View::url( 'project/view/'.$result->ClientProfileID ); ?>"><?php echo isset( $result->CompanyName ) ? ucwords( $result->CompanyName ) : ""; ?></a>
                          </h3>
                          <div class="push-10">
                            <span class="label label-<?php echo $classbg;?>"><i class="<?php echo $classicon; ?>"></i> <?php echo $result->Status; ?></span>
                          </div>
                          <div class="font-s13 text-muted hidden-xs">
                            <p><?php echo AppUtility::excerptText( $result->OfferOverview, 200, ' <a href="'.View::url( 'project/view/'.$result->ClientProfileID ).'" class="item_button1">...read more</a>'); ?></p>
                          </div>
                        </td>
                        <td class="h5 text-center text-info font-w600 hidden-xs">
                          <?php echo isset( $result->TypeOfOffer ) ? $result->TypeOfOffer : "-"; ?>
                        </td>
                        <td class="h5 text-center text-info font-w600 hidden-xs">
                          <?php echo $result->Status == 'Pending' ? '-' : ( isset( $result->TotalInvestor ) ? $result->TotalInvestor : "-" ); ?>
                        </td>
                        <td class="h5 text-center text-info font-w600 hidden-xs">
                          <?php echo isset( $result->TotalFunds ) ? '$'.number_format( $result->TotalFunds ) : "-"; ?>
                        </td>
                      </tr>
                    <?php } } ?>
                  </tbody>
                </table>
              <?php } ?>
              
          </div>
        </div>
      </div>
    </main>

  </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        // $('#search-results').easyPaginate({
        //     paginateElement : 'tr',
        //     elementsPerPage: 10,
        //     effect: 'fade',
        //     slideOffset : 100,
        //     nextButtonText : "Next",
        //     prevButtonText : "Prev",
        //     lastButtonText : "Last",
        //     firstButtonText: "First"
        // });

        //$('#DataTables_Table_0_wrapper div:visible').first().hide();
    });
        
    function doSearch(){
      $( '#formSearch' ).submit();
    }
</script>