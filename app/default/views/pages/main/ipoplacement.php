<?php 
View::$title = 'IPO & Placements';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom heading-title-v2" style="background-image: url(<?php echo View::url('app/default/views/assets/images/aboutus/about-us-header.jpg'); ?>);"></section>
<section class="heading-title-text">
  <div class="heading-line"></div>
  <h1 class="aus-heading-title"><?php echo View::$title; ?></h1>
</section>

<section class="ipo-about">
  <div class="container-fluid">

    <div class="ipolistitems">
      <div class="flex">
        <div class="left">
          <div class="sec-block-holder sec-img">
            <img src="<?php echo View::url('app/default/views/assets/images/ipo/ipo-about.jpg'); ?>" alt="">
          </div>
        </div>
        <div class="right gray-bg pad-50-t">
          <div class="sec-block-holder">
            <div class="sec-block-content">
              <h1 class="sec-block-title">Not Raising Enough Money? </h1>
              <p>As experienced IPO Consultants and Fund Managers we understand you want a no hassle, simple approach that will help get you the required funding you need to grow and develop – to get you to the next stage of your business. </p>
            </div>
          </div>
        </div>
        
      </div>

      <div class="ipo-regbtn">
        <a href="/users/signup" class="btn btn-primary btn-rounded btn-lg text-uppercase">Register here for <strong>FREE</strong></a>
      </div>
    </div>

  </div>
</section>

<section class="ipo-allin">
  <div class="container-fluid">
    
    <div class="sec-block-title pad-10">SeedOmega delivers a proprietary, all-in-one <br> technology platform giving companies full<br> access to:</div>

    <div class="sec-block-content xs-pad-0-t">
      <ul class="frontlist-check pad-20-l">
        <li><i class="si si-check"></i> Our global network of seeders  </li>
        <li><i class="si si-check"></i> Your own online presence for investors (seeders, venture capitalists, and angel investors) to discover and learn</li>
        <li><i class="si si-check"></i> Streamlined and enhanced QnA for open-door communications with your potential seeders</li>
        <li><i class="si si-check"></i> Enhanced efficiency and transparency on everything – with no surprises down the road.</li>
      </ul>
    </div>

  </div>
</section>

<section class="ipo-fullaccess xs-pad-0-t">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-5 no-padding">
        <div class="sec-block-holder sec-img">
          <img src="<?php echo View::url('app/default/views/assets/images/ipo/ipo-network.jpg'); ?>" alt="">
        </div>
      </div>
      <div class="col-lg-7">
        <div class="sec-block-holder">
          <div class="sec-block-title xs-pad-50-t h3">Full Access to Multiple Communication Platforms with Potential Seeders (Investors)</div>
          <div class="sec-block-content">
            <p>Our proprietary platform delivers a unique, and robust forum-style community allowing seeders to collaborate on their backed projects.  </p>
            <p>In addition, we’ve created a DIRECT communication platform, allowing you to answer any seeder questions/concerns to accelerate the funding process. </p>
            <p>This allows you to increase the velocity of acquiring new funds, and to increase the amount of seeded funds by quickly and directly addressing any concerns or questions your seeders will likely have. </p>
            <p>With seeders who want to contribute in making a difference, you will have full access to communicate and to acquire collaborative information beyond just financial funding. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="block-section ipo-benefits no-padding nopadd-xs">
  <div class="container-fluid no-padding">
    <div class="flex">      

      <div class="left purple-bg no-padding">
        <div class="sec-block-holder pad-50-t">
          <div class="sec-block-title librebaskerville text-white h3">IPO & Private Placement Benefits:</div>
          <div class="sec-block-content">

            <ul class="ipo-benefitslist pad-10-l">
              <li>
                <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/ipo/ipo-document.png'); ?>" alt=""></div> 
                No hidden fees (no surprises) – fully transparent for marketing and accounting purposes
              </li>
              <li>
                <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/ipo/ipo-investment.png'); ?>" alt=""></div> 
                Full robust, communications platforms to handle communications with seeders – we take care of it for you
              </li>
              <li class="pad-20-t">
                <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/ipo/ipo-global.png'); ?>" alt=""></div> 
                Access to our global network of seeders
              </li>
            </ul>

          </div>
        </div>
      </div>  

      <div class="right gray-bg">
        <div class="sec-block-holder pad-100-t">
          <div class="sec-block-content">
            <p>Acquire the funding you need, through a simple and robust platform where the end-to-end communications are handled 100% by us. </p>
            <p>We deliver a fully transparent, proprietary platform, with no surprises – because no business likes surprises. </p>
            <p>Through our simple end-to-end one-stop solution you will able to get your business to the next stage seamlessly. </p>
            <a href="/users/signup" class="btn btn-primary btn-rounded btn-lg font-20 push-20-t text-uppercase">Register here for <strong>FREE</strong></a>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</section>


<!-- /page content -->
<?php View::footer(); ?>