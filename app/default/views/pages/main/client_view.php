<?php 
View::$title = isset( $clientdata->CompanyName ) ? $clientdata->CompanyName : "No Title";
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <li class="fa fa-angle-right"></li>
              <li><?php echo View::$title; ?></li>
            <?php }else{ ?>
              <li><?php echo View::$segments[0]; ?></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      <?php echo View::getMessage();  ?>
      <!-- ************************ Left Side Area ************************ -->
      
      <div class="col-lg-8">
        <div class="embed-responsive embed-responsive-16by9">
          <?php echo $clientdata->Video; ?>
        </div>

          <div class="block" style="margin-top: 20px;">

            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active">
                    <a href="#client-info">Info</a>
                </li>
                <?php if( $enabledTabs ){ ?>
                  <li class="">
                      <a href="#client-faq">FAQ</a>
                  </li>
                  <li class="">
                      <a href="#client-blogs">Blogs</a>
                  </li>
                  <li class="" id="client-comments-tab">
                      <a href="#client-comments">Comments</a>
                  </li>
                  <li class="">
                      <a href="#client-community">Community</a>
                  </li>
                <?php } ?>

            </ul>
            <div class="block-content tab-content bg-white">

              <div class="tab-pane fade fade-up in active" id="client-info">
                <p class="text-muted"><i><?php echo isset($clientdata->VideoDescription) ? $clientdata->VideoDescription : "No Description"; ?></i></p>
                <h5>Offer Overview</h5>
                <p><?php echo isset($clientdata->OfferOverview) ? $clientdata->OfferOverview : ""; ?></p>

                <h5>Business Model</h5>
                <p><?php echo isset($clientdata->BusinessModel) ? $clientdata->BusinessModel : ""; ?></p>
                  <?php if( $enabledTabs ) { ?>
                  <h5>Key Invest Highlights</h5>
                  <p><?php echo isset($clientdata->KeyInvestHighlights) ? $clientdata->KeyInvestHighlights : ""; ?></p>

                  <h5>Strategy Vision</h5>
                  <p><?php echo isset($clientdata->StrategyVision) ? $clientdata->StrategyVision : ""; ?></p>

                  <h5>Market Demand</h5>
                  <p><?php echo isset($clientdata->MarketDemand) ? $clientdata->MarketDemand : ""; ?></p>

                  <h5>Board Management</h5>
                  <p><?php echo isset($clientdata->BoardManagement) ? $clientdata->BoardManagement : ""; ?></p>

                  <h5>Usage Of Funds</h5>
                  <p><?php echo isset($clientdata->UsageOfFunds) ? $clientdata->UsageOfFunds : ""; ?></p>

                  <h5>Financial Summary</h5>
                  <p><?php echo isset($clientdata->FinancialSummary) ? $clientdata->FinancialSummary : ""; ?></p>

                  <h5>Press Coverage</h5>
                  <p><?php echo isset($clientdata->PressCoverage) ? $clientdata->PressCoverage : ""; ?></p>

                  <h5>Disclosure</h5>
                  <p><?php echo isset($clientdata->Disclosure) ? $clientdata->Disclosure : ""; ?></p>
                <?php }else{ $premessage = AppUtility::investorinfo('InvestorStatus') == 'Pending' ? 'Please complete your investor profile' : 'You need to <a href="'.View::url('users/signup').'">signup</a> completely'; ?>
                  <br><br>                  
                  <h4 class="text-divider text-muted text-left"><span><?php echo $premessage; ?> to view this company's full information!</span></h4>
                  <br>
                  <h5>Strategy Vision</h5>
                  <p>....</p>

                  <h5>Market Demand</h5>
                  <p>....</p>

                  <h5>Board Management</h5>
                  <p>....</p>

                  <h5>Usage Of Funds</h5>
                  <p>....</p>

                  <h5>Financial Summary</h5>
                  <p>....</p>

                  <h5>Press Coverage</h5>
                  <p>....</p>

                  <h5>Disclosure</h5>
                  <p>....</p>
                <?php } ?>
              </div>

              <?php if( $enabledTabs ){ ?>
                <div class="tab-pane fade fade-up" id="client-blogs">
                  <!--<h4>Blogs</h4> <br><br>-->
                  <!--blog section-->
                  <div class="blog-page">
                      <!-- Dynamic Table Full Pagination -->
                      <ul id="bloglist" class="blog-list row">
                          <?php
                          $cntr = 0;
                            if( isset( $recentPosts ) ){
                              if( count( $recentPosts ) ){
                                foreach( $recentPosts as $article ){ $cntr++;
                                    ?>
                                    <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="blog-item">
                                            <figure class="blog-image blog-user-view-l">
                                                <figcaption><a href="<?php echo View::url('clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID); ?>">Read Article</a></figcaption>
                                                <?php if ($article->FeaturedImage):?>
                                                    <?php $featuredImage = View::common()->getUploadedFiles($article->FeaturedImage); ?>
                                                    <?php echo View::photo((isset($featuredImage[0]) ? 'files'.$featuredImage[0]->FileSlug : '/images/user.png'),$article->BlogTitle,"articleFeaturedImage"); ?>

                                                <?php else: ?>
                                                    <img src="<?php echo View::url('assets/images'); ?>/blog/blog-3.jpg" alt="img" />
                                                <?php endif; ?>
                                            </figure>
                                            <div class="popular-content blog-user-view-r">
                                                <div class="blog-desc" style="margin-left: 0;">
                                                    <h5 style="margin-top: 0;"><a href="<?php echo View::url('clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID); ?>"><?php echo mb_strimwidth(isset($article->BlogTitle) ? $article->BlogTitle: "No Category Title", 0, 55, ""); ?></a><span class="line-green"></span></h5>
                                                    <?php echo AppUtility::excerptText($article->BlogContent,200,'...'); ?>
                                                </div>
                                            </div>
                                            <div class="blog-byDate clear">
                                                <div class="pmeta">
                                                    <div class="met"><i class="fa fa-user"></i> <?php echo (isset($article->FirstName)) ? $article->FirstName : ""; ?> <?php echo (isset($article->LastName)) ? $article->LastName : ""; ?></div>
                                                    <div class="met"><i class="fa fa-calendar"></i> <?php echo (isset($article->BlogDatePublished) && $article->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime($article->BlogDatePublished)) : '-' ?></div>
                                                    <div class="met"><i class="fa fa-tags"></i> <?php echo (isset($article->CategoryName)) ? $article->CategoryName : ""; ?></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </li>
                                <?php }
                              }
                            }
                            ?>
                      </ul>
                  </div>
                  <!--end blog section-->
                </div>

                <div class="tab-pane fade fade-up" id="client-faq">
                    <!--<h4>FAQ</h4>-->
                    <p class="text-muted">
                    <?php 
          				  	$ctr = 0;
          				  	if (isset($faqs)) {
            						if (count($faqs)) {
            							foreach($faqs as $faq) {
                            $ctr++; ?>
                            <div class="clearfix"></div>
                            <div class="panel-group accordion" id="caq-a" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab">
                                        <h4 class="panel-title">
                                            <a role="button" class="collapsed" data-toggle="collapse" data-parent="#caq-a" href="#caq-a-<?php echo $ctr; ?>" aria-expanded="true">
                                                <i class="fa fa-plus toggle-icon"></i>
                                                <i class="fa fa-minus toggle-icon"></i>
                                                Q<?php echo $ctr.'. '.$faq->FaqTitle; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="caq-a-<?php echo $ctr; ?>" class="panel-collapse collapse in" role="tabpanel">
                                        <div class="panel-body faq">
                                            <p><?php echo $faq->FaqContent; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
            				<?php	} }	} ?>
                    </p>
                </div>

                <div class="tab-pane fade fade-up" id="client-comments">
                    <!--<h4>Comments</h4>-->
                    <div id="client-comments-content" class="comments-tab">
                      <div class="actionBox">
                        <ul class="commentList">
                        <?php 
              				  	$ctr = 0;
              				  	if (isset($comments)) {
                						if (count($comments)) {
                							foreach($comments as $comment) { $ctr++;
                                ?>
                                <li class="">
                                  <div class="commenterImage">
                                    <?php 
                                      $avatar = View::common()->getUploadedFiles(User::info('Avatar', $comment->UserID));
                                    ?>
                                    <img src="<?php echo isset($avatar[0]) ? View::url('assets/files/'.$avatar[0]->FileSlug) : View::url('assets/images/user.png'); ?>" alt="Avatar" width="35" height="35">
                                  </div>
                                  <div class="commentText">
                                    <p class=""><b><?php echo $comment->FirstName.' '.$comment->LastName; ?></b>:&nbsp; <?php echo $comment->CommentContent; ?></p> <span class="date sub-text">on <?php echo date('M jS, Y', strtotime($comment->CommentDate)) ?></span>
                                  </div>
                                </li>
                                <?php
              							  }
              						  }
              					  }
              				  ?>
                        </ul>
                      </div>
                    </div>
                    <div class="text-center">
                      <a href="<?php echo View::url('users/signup'); ?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;">Signup to comment</a>
                    </div>
                    <br>
                </div>

                <div class="tab-pane fade fade-up" id="client-community">
                  <!-- <h4>Community</h4> -->
                  <h2 class="content-heading text-center push-0-t">Our Backers</h2>
                  <?php
                    $ctr = 0;
                    if( isset( $communities ) ){
                      if( count( $communities ) ){
                        foreach( $communities as $community ){ $ctr++;
                          $avatar = View::common()->getUploadedFiles( $community->Avatar );
                          ?>
                          <div class="col-sm-6 col-lg-4">
                            <a class="block block-rounded block-link-hover3" href="javascript:void(0)">
                              <div class="block-content block-content-full clearfix">
                              <div class="pull-right">
                                <?php echo View::photo( ( $avatar ? 'files'.$avatar[0]->FileSlug : 'images/user.png' ), false, 'img-avatar', false, false ); ?>
                              </div>
                              <div class="pull-left push-10-t">
                                <?php $backer = Apputility::maskText( $community->FirstName, 'x', 'x' ).' '.$community->LastName; ?>
                                <div class="font-w600 push-5"><?php echo AppUtility::excerptAsNeeded( $backer, 11, '..' ); ?></div>
                                <div class="text-muted"><?php echo Apputility::excerptAsNeeded( $community->JobTitle, 13, '..' ); ?></div>
                              </div>
                              </div>
                            </a>
                          </div>
                  <?php } } } ?>
                </div>
              <?php } ?>

            </div><!--end block-content-->

            <?php if( $enabledTabs ){ ?>
              <div class="content">
                <h2 class="content-heading text-center push-0-t">Company Team</h2>
                <div class="row">

                  <div class="col-sm-6 col-lg-4">
                    <a class="block block-rounded block-link-hover3" href="javascript:void(0)">
                      <div class="block-content block-content-full clearfix">
                      <div class="pull-right">
                        <img class="img-avatar" src="<?php echo View::url('assets/images/funder_1.jpg'); ?>" alt="">
                      </div>
                      <div class="pull-left push-10-t">
                        <div class="font-w600 push-5">Tiffany Kim</div>
                        <div class="text-muted">Web Designer</div>
                      </div>
                      </div>
                    </a>
                  </div>

                  <div class="col-sm-6 col-lg-4">
                    <a class="block block-rounded block-link-hover3" href="javascript:void(0)">
                      <div class="block-content block-content-full clearfix">
                        <div class="pull-right">
                          <img class="img-avatar" src="<?php echo View::url('assets/images/funder_2.jpg'); ?>" alt="">
                        </div>
                        <div class="pull-left push-10-t">
                          <div class="font-w600 push-5">Judy Alvarez</div>
                          <div class="text-muted">Copywriter</div>
                        </div>
                      </div>
                    </a>
                  </div>

                  <div class="col-sm-6 col-lg-4">
                    <a class="block block-rounded block-link-hover3" href="javascript:void(0)">
                      <div class="block-content block-content-full clearfix">
                        <div class="pull-right">
                          <img class="img-avatar" src="<?php echo View::url('assets/images/funder_3.jpg'); ?>" alt="">
                        </div>
                        <div class="pull-left push-10-t">
                          <div class="font-w600 push-5">Judy Alvarez</div>
                          <div class="text-muted">Copywriter</div>
                        </div>
                      </div>
                    </a>
                  </div>

                </div>
              </div>
            <?php } ?>

          </div><!--end block-->
      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4">
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="project-progress">
              <?php 
              $percentage = 0;
              if($clientdata->TargetGoal > 0){
                $percentage = (int)(($clientdata->TotalRaisedAmt / $clientdata->TargetGoal) * 100+.5);
              ?>

              <div class="popular-data data-single"> 
                <!-- <img src="<?php echo View::url() ?>/assets/images/funder_1.jpg" alt="Funder" /> -->
                
                <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>

              <?php } ?>              

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($clientdata->TotalRaisedAmt) ? "$".number_format($clientdata->TotalRaisedAmt) : "0";?><br>
                        <small class="text-muted">Raised</small>
                    </div>                                        
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($clientdata->TotalInvestor) ? number_format($clientdata->TotalInvestor) : "0";?><br>
                        <small class="text-muted">Investors</small>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset($clientdata->TypeOfOffer) ? $clientdata->TypeOfOffer : "-"; ?><small>Type of Offer</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data"><?php echo $clientdata->DaysLeft;?><small>Days Left</small></div>
                </div>

                <!-- <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo ($clientdata->OfferOpening != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferOpening)) : "-"; ?><small>Offer Open</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo ($clientdata->OfferClosing != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferClosing)) : "-"; ?><small>Offer Closing</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->TargetGoal) ? number_format($clientdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    <?php echo isset($clientdata->SizeOfOffer) ? $clientdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?><small>Price</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->MinimumBid) ? number_format($clientdata->MinimumBid) : "-"; ?><small>Minimum Investment</small>
                  </div>
                </div> -->
                <div class="row">
                  <div class="col-sm-12 offer-lead offer-data text-center">
                    <a href="<?php echo View::url('users/signup'); ?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;">Signup to Invest</a>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>
          
          <!-- We Need Volunteers -->     
          
        </div>
      </div>             
    </div>
  </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
  $(document).ready(function() {

    $( '#client-comments-tab' ).click( function( e ) {
      e.preventDefault();
      setTimeout(function(){ autoScroll(); }, 800);
    });

    function autoScroll(){
      $( '#client-comments-content' ).animate( { scrollTop: $( '.commentList' )[0].scrollHeight }, 2000);
    }

  });
</script>