<?php 
View::$title = 'Terms & Conditions';
View::$bodyclass = '';
View::header(); 
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<!-- <section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( View::$segments[0] ); ?>"><?php echo View::$title; ?></a></li>
              <?php if( isset( View::$segments[1] ) ){ ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url( View::$segments[0].'/'.View::$segments[1] ); ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section> -->

<section class="gray">
    <!-- Page Content -->
    <article class="container animated zoomIn">
        <div class="block-header"><h5 class="text-uppercase"><?php echo View::$title; ?></h5></div>
        <div class="block-content">
          <?php echo isset( $tac->Content ) ? $tac->Content : ''; ?>
        </div>
        
        <?php if( User::can( 'Manage Documents' ) ) { ?>    
          <div class="block-content">        
            <form id="formTAC" class="tac hidden" enctype="multipart/form-data" method="post">
                <input type="hidden" name="action" value="saveTAC" />
                <input type="hidden" name="ID" value="<?php echo isset( $tac->ID ) ? $tac->ID : '0'; ?>" />
                <div class="form-group">
                  <textarea class="form-control tinyMCE" name="Content" id="tacContent"><?php echo isset( $tac->Content ) ? $tac->Content : ''; ?></textarea>
                </div>
                <button type="submit" class="btn btn-rounded btn-primary pull-right"><i class="fa fa-save"></i> SAVE</button>
                <div class="clear"></div>
            </form>
            <button id="editTAC" class="btn btn-info btn-rounded pull-right" type="button" data-dismiss="modal"><i class="fa fa-edit"></i> EDIT</button>
            <div class="clear"></div>
          </div>
        <?php } ?>
        
        <div class="block-content">&nbsp;</div>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
  $( '#editTAC' ).click( function(){
    $( '#formTAC' ).removeClass( 'hidden' );
    $( this ).addClass( 'hidden' );
  });
</script>