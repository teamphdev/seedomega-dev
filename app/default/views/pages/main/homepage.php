<?php 
View::$title = 'Welcome to '.Config::get('SITE_TITLE');
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
  
  <section class="home-banner no-padding" style="background:url(<?php echo View::asset('images/home-slide-1.jpg'); ?>) no-repeat center 0; background-size: cover; ">
    <img src="" alt="" class="img-responsive banner-img">
    <div class="banner-holder">
      <div class="container-fluid">
        <h2 class="banner-title">Simplified<br> Investment Process<br> Delivering Returns<br> And Creating Change</h2>
        <div class="banner-subtitle">Providing Well-vetted Private & IPOs Companies<br> for the smart investor – made easy….</div>
      </div>
      <div class="container-fluid banner-subbtn">
        <div class="banner-subbtn-holder">
          <div class="row">
            <div class="col-lg-6 left-area">
              <div class="banner-wrap">
                <div class="bsubbtn-text pull-left">
                  Do you want higher investment returns?
                </div>
                <a href="<?php echo View::url('users/signup'); ?>" class="btn btn-primary btn-rounded btn-lg text-uppercase">Register here for <strong>FREE</strong></a>
              </div>
            </div>
            <div class="col-lg-6 hidden-xs hidden-sm"></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="block-section pad-100-t nopadd-xs">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-5 no-padding">
          <div class="sec-block-holder sec-img">
            <img src="<?php echo View::asset('images/homepage/home-public-1.jpg'); ?>" alt="">
          </div>
        </div>
        <div class="col-lg-7">
          <div class="sec-block-holder">
            <div class="sec-block-title xs-pad-50-t h3">SeedOmega provides exclusive, quality investments in BOTH Private and in IPO companies</div>
            <div class="sec-block-content">
              <p>Our proprietary technology integrates the first supportive and active community for seeders to collaborate and support companies they are passionate about.  </p>
              <p>This allows the seeders (you) to be both financially and socially involved in the company's growth and development affecting REAL change to DIRECTLY impact their product development. </p>
              <p>And you know what? These companies really do WANT to hear from you – and we've made that finally happen. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="block-section nopadd-xs">
    <div class="container-fluid">
      <div class="row">

        <div class="col-lg-7">
          <div class="sec-block-holder">
            <div class="sec-block-title h3">1st to Deliver Direct Messaging</div>
            <div class="sec-block-content">
              <p>Through our sophisticated chat platform you can now communicate DIRECTLY to IPO and Private companies on concerns and suggestions.</p>
              <p>Gone are the old days of email and Annual General Meetings (AGM's) TRADITIONALLY used to communicate investors' thoughts and requests.  </p>
              <p>Through this proprietary communication platform we've made communication between seeders and companies lightning fast to effect real change faster. </p>
              <p>Our seeders who are passionate and want to be involved in their seeded projects bring immense value that our companies value and encourage. </p>
            </div>
          </div>
        </div>

        <div class="col-lg-5 no-padding">
          <div class="sec-block-holder sec-img">
            <img src="<?php echo View::asset('images/homepage/home-public-2.jpg'); ?>" alt="">
          </div>
        </div>
        
      </div>
    </div>
  </section>

  <?php
  if( isset( $currentlyfundings ) ){
    if( count( $currentlyfundings ) ){ ?>
      <section class="">
        <article class="container-fluid main" id="popular">
          <h3 class="animated fadeInDown">Currently Funding</h3>
          <div class="row">
            <?php
            $ctr = 0;
            foreach( $currentlyfundings as $funding ){ $ctr++; ?>

              <div class="col-lg-4 animated flipInY">                

                <div class="project-item ribbon ribbon-left">

                    <div class="ft-img">
                        <?php echo $funding->Data['CompanyPhotoLink']; ?>

                        <figcaption><a href="<?php echo View::url('project/view/'.$funding->ClientProfileID); ?>">View Details</a></figcaption>
                    </div>
                    <div class="project-content">
                        <h5><a href="<?php echo View::url('project/view/'.$funding->ClientProfileID); ?>"><?php echo $funding->CompanyName; ?></a></h5>
                        <p class="project-text"><?php echo isset($funding->OfferOverview) ? AppUtility::excerptText( $funding->OfferOverview, 150, ' <a href="/projects/view/'.$funding->ClientProfileID.'" class="item_button1">...read more</a>' ) : 'No Description'; ?></p>

                        <div class="project-progress">
                            <div class="project-data row">                             
                                <div class="col-lg-6">
                                    <div class="pie_progress" role="progressbar" data-goal="<?php echo $funding->Data['Percentage']; ?>" data-barcolor="#7390d6" data-barsize="5" data-size="96">
                                      <?php echo $funding->Data['CompanyLogoLink']; ?>
                                    </div>                                
                                </div>
                                <div class="col-lg-6">
                                    <div class="progress_number"><?php echo $funding->Data['Percentage']; ?>%</div>
                                    <div class="progress_label">Completed</div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="project-summary">
                            <div class="">
                                <div class="col-lg-6 col-xs-6 no-padding daysback">
                                    <span><?php echo isset( $funding->DaysLeft ) ? number_format( $funding->DaysLeft ) : '0'; ?></span> DAYS LEFT
                                </div>
                                <div class="col-lg-6 col-xs-6 no-padding text-right daysback">
                                    <span><?php echo isset( $funding->Backers ) ? number_format( $funding->Backers ) : '0'; ?></span> BACKERS
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="text-center project-funded">
                                <span>$<?php echo number_format( $funding->Funded, 2 ); ?></span><br>
                                Funded
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="project-btn">
                            <a href="/project/view/<?php echo $funding->ClientProfileID; ?>" class="btn btn-success">View Details</a>
                        </div>
                    </div>
                </div>

              </div>

            <?php } ?>

          </div>
          <div class="popular-btn green"><a href="<?php echo View::url( 'projects' ); ?>">See More</a> </div>
        </article>
      </section>
  <?php } } ?>

  <section class="zerofee top-line-r nopadd-xs no-topline-xs">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4">
          <div class="nfz-holder">
            <div class="block content">
              <div class="sec-block-title h3">We charge NO FEE's – ZERO</div>
              <div class="sec-block-content">
                <p>We make it 100% FREE for seeders to participate socially and financially in well-vetted, high quality IPO and Private companies via our secure and advanced platform.</p>
                <a href="<?php echo View::url('users/signup'); ?>" class="btn btn-primary home-signup-btn btn-rounded font-20 btn-lg text-uppercase">Register here for <strong>FREE</strong></a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-8">

          <div class="nfz-dr-access">
            <div class="block content">
              <div class="sec-block-title text-center h3">Through SeedOmega's proprietary platform the seeder gets DIRECT access to our:</div>
              <div class="sec-block-content">
                <ul class="nfz-accesslist row">
                  <li class="col-lg-6">
                    <div class="nfzlist-holder">
                      <span>1</span>
                      <div class="nfzlist-content">
                        Well-vetted, quality, and exclusive IPO & private companies before trading on the open market.
                      </div>
                    </div>
                  </li>
                  <li class="col-lg-6">
                    <div class="nfzlist-holder">
                      <span>2</span>
                      <div class="nfzlist-content">
                        Company data beyond the standard prospectus to provide investors with a greater in-depth analysis
                      </div>
                    </div>
                  </li>
                  <li class="col-lg-6">
                    <div class="nfzlist-holder">
                      <span>3</span>
                      <div class="nfzlist-content">
                        <strong>PLUS</strong> a supportive community-forum for seeders to collaborate on their seeded projects 
                      </div>
                    </div>
                  </li>
                  <li class="col-lg-6">
                    <div class="nfzlist-holder">
                      <span>4</span>
                      <div class="nfzlist-content">
                        A Sophisticated Chat System for enhanced communication between private and IPO companies to affect REAL change to make a REAL difference  
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section class="white">
    <div class="container-fluid">
      <div class="row">

        <div class="col-lg-6">
          <div class="sec-block-content">
            <div class="sec-block-title text-center min-height-120">Through SeedOmega's Proprietary platform the Company acquires DIRECT access to:</div>
            <div class="sec-block-content">
              <ul class="frontlist-check">
                <li><i class="si si-check"></i> Our global network of seeders</li>
                <li><i class="si si-check"></i> Your own online presence for investors to discover, and learn about you and your company</li>
                <li><i class="si si-check"></i> Streamlined and enhanced messaging, with QnA for open-door communications with your potential seeders</li>
                <li><i class="si si-check"></i> Enhanced efficiency and transparency on everything – no surprises down the road – Simple. </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="sec-block-content">
            <div class="sec-block-title text-center min-height-100">The First Complete All-In-One Solution <br>&nbsp;</div>
            <div class="sec-block-content">
              <ul class="frontlist-check">
                <li><i class="si si-check"></i> The latest up-to-date company information is sent regularly to seeders and easily accessible 24/7. </li>
                <li><i class="si si-check"></i> Increased and enhanced communication between seeders and companies through the most advanced chat platforms for crowdfunding.</li>
                <li><i class="si si-check"></i> 100% safe and seamless process that is fully assisted by our dedicated support staff to ensure a positive experience. </li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="seeding-proccess text-center top-line-l no-padding">
    <div class="container-fluid no-padding">
      <img src="<?php echo View::asset('images/seeding-proccess.jpg'); ?>" alt="" class="img-responsive">
    </div>
  </section>

  <section class="block-section top-line-r pad-15-t no-padding pad-50">
    <div class="container-fluid">
      <div class="sec-block-title pad-50-t pad-10  text-center">Our Advanced Technology Platform Includes: </div>
      <div class="row">
        <div class="col-lg-6 no-padding">
          <div class="sec-block-holder sec-img">
            <img src="<?php echo View::asset('images/homepage/home-public-3.jpg'); ?>" alt="">
          </div>
        </div>
        <div class="col-lg-6">
          <div class="sec-block-content xs-pad-50-t">
            <ul class="frontlist-check pad-20-l">
              <li><i class="si si-check"></i> 1st direct access portal to Australian IPOs and private companies (global access). </li>
              <li><i class="si si-check"></i> Community Forum to track IPO projects they've backed.</li>
              <li><i class="si si-check"></i> QnA for open-door communications with IPO companies. </li>
              <li><i class="si si-check"></i> Constant updates on information DIRECTLY FROM IPO & Private companies, with notifications so you're always in-the-know. </li>
              <li><i class="si si-check"></i> Simple, easy booking & tracking process. </li>
              <li><i class="si si-check"></i> Multi-channel communication tools for seeders and companies. </li>
            </ul>
            <div class="text-center">
              <a href="<?php echo View::url('users/signup'); ?>" class="btn btn-primary home-signup-btn btn-rounded btn-lg font-20 text-uppercase">Register here for <strong>FREE</strong></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
<?php View::footer(); ?>