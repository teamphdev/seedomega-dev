<?php 
View::$title = 'Our Team';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><?php echo View::$title;?></li>
          </ul>
        </div>
      </div>
    </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="sobg">
  <article class="container team-members">
    <?php if( isset( $teams ) && count( $teams ) ){ ?>

      <!-- <h4 class="text-center text-white text-uppercase push-40">Company Team Members</h4> -->
      <div class="max900 centered">
        <div class="row">

          <?php foreach( $teams as $team ){ ?>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 project-cnt">

              <div class="project-item">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-team-member" data-locations="<?php echo $team->Data; ?>" onclick="biomember( this )">
                  <div class="ft-img">
                      <?php echo View::photo( isset( $team->FileSlug ) && $team->FileSlug != '' ? 'files'.$team->FileSlug : 'images/user.png', false, '', false, false ); ?>
                  </div>
                  <div class="project-content push-0 push-0-l push-0-r push-0-t">
                      <h5 class="min-height-60"><?php echo isset( $team->Name ) ? Apputility::excerptAsNeeded( $team->Name, 18, '..' ) : ''; ?></h5>
                      <p class="project-text-recent min-height-20"><?php echo isset( $team->Position ) ? Apputility::excerptAsNeeded( $team->Position, 20, '..' ) : ''; ?></p>
                      
                  </div>
                </a>
              </div>

          </div>

          <?php } ?>

        </div>
      </div>

    <?php } ?>
  </article>
</section>

<!-- team members -->
<div class="modal fade" id="modal-team-member" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
  <div class="modal-dialog modal-dialog-popout">
    <div class="modal-content">
      <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title"><span id="modal-title"></span></h3>
        </div>
        <div class="block-content">
          <div id="modal-header" class="block text-center">
            <div class="animated fadeInDown">
              <div id="avatar" class="img-wrap img-avatar img-avatar128 img-avatar-thumb"></div>
              <div id="position" class="text-uppercase push-5-t text-info"></div>
            </div>
          </div>
          <div class="animatedx fadeInUp">
            <div id="content" class="backer-view"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
      </div>
    </div>
  </div>
</div>
<!-- end team members -->

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
  // popout member info
  function biomember( el ){
    var json = $( el ).attr( 'data-locations' );
    var data = jQuery.parseJSON( json );
    
    $( '#modal-header' ).removeClass( 'hidden' );
    $( '#modal-title' ).text( data.Name );
    $( '#avatar' ).html( data.Avatar );
    $( '#position' ).html( '<b>'+data.Position+'</b>' );
    $( '#content' ).html( '<div class="text-divider font-w600 push-5-t text-left text-uppercase"><span>Bio</span></div>'+data.Bio );

    return true;
  }
</script>