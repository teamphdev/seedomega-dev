<?php 
View::$title = 'About Us';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom heading-title-v2" style="background-image: url(<?php echo View::url('app/default/views/assets/images/aboutus/about-us-header.jpg'); ?>);"></section>
<section class="heading-title-text">
  <div class="heading-line"></div>
  <h1 class="aus-heading-title"><?php echo View::$title; ?></h1>
</section>

<section class="block-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-5">
        <div class="sec-block-holder sec-img">
          <img src="<?php echo View::url('app/default/views/assets/images/aboutus/about-us-1.jpg'); ?>" alt="">
        </div>
      </div>
      <div class="col-lg-7">
        <div class="sec-block-holder">
          <div class="sec-block-content">
            <p>As the Director of a listed company providing IPO Consultancy, Corporate Governance, and Portfolio Fund Management, we discovered there was tremendous financial opportunities in the IPO and Private placement markets compared to the traditional stock market. </p>
            <p>We found prices tended to be lower, so they were cheaper to buy and also provided substantially higher returns compared to the stock market.</p>
            <p>However, the vast majority of the retail investors are unable to take advantage of these wonderful financial opportunities. The large banks and large institutional fund managers participate in these type of securities mainly because they have direct access to these opportunities that provide a high, wealth generating opportunities and diversification to their clients’ portfolios. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="two-reason">
  <div class="container-fluid">
    <div class="sec-block-title h4">The reasons why the general investment public aren’t <br> using these commonly used investment securities are: </div>
    <div class="tworeas-holder">
      <div class="row">
        <div class="col-md-6 col-xs-12">
          <div class="trnum">01</div>
          <div class="trcontent">
            IPOs and Private placements are not publicly advertised – as you usually need to be within the ‘right’ circle of investors. 
          </div>
        </div>
        <div class="col-md-6 col-xs-12">
          <div class="trnum">02</div>
          <div class="trcontent">
            Most retail investors do not understand IPOs and Private placements opportunities - so they aren’t often sought after or used compared to traditional stocks
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="block-section about-bridge push-30-t nopadd-xs">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-7">
        <div class="sec-block-holder no-padding">
          <div class="sec-block-title h3">Bridging the Gap for the retail investor to ALSO have DIRECT access to wonderful investment opportunities:</div>
          <div class="sec-block-content">
            <p>Based on historical growth and performance compared to the traditional stock/equity securities, we decided to provide the investor with direct access, giving the retail the same investment opportunities as the Big Banks, and the Large Institutional Portfolio Managers. </p>
            <p>In short, we bridge the gap in the lack of available IPOs and private placements of these wonderful opportunities that exist, but the regular investor unfortunately does not have access to them. In addition, we provide the company knowledge beyond the prospectus to aid the investor in greater analysis. </p>
            <p>Real change – traditional communication channels between investors and companies in these types of investments rely on email, and AGM’s (annual general meetings – one per year). This process is slow and dated. The transfer and processing of information from this traditional (old) process tedious and takes a long time and so change is slow. </p>
          </div>
        </div>
      </div>

      <div class="col-lg-5 no-padding">
        <div class="sec-block-holder sec-img">
          <img src="<?php echo View::url('app/default/views/assets/images/aboutus/about-us-2.jpg'); ?>" alt="">
          <div class="bimg-content">
            With our advanced communication platforms, the investors (seeders) can now communicate DIRECTLY and QUICKLY to companies who genuinely WANT to hear your voice and opinions in affecting positive change on the development of a producing quality product that will be highly successful. 
          </div>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="block-section about-perform nopadd-xs">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-5 left no-padding">
        <div class="sec-block-holder sec-img">
          <img src="<?php echo View::url('app/default/views/assets/images/aboutus/about-us-3.jpg'); ?>" alt="">
        </div>
      </div>

      <div class="col-lg-7 right xs-pad-40-t">
        <div class="sec-block-holder">
          <div class="sec-block-title h3">Expected Type of Performance</div>
          <div class="sec-block-content">
            <p>Performance results will provide ‘value-like’ investor returns (buying at a discount), and letting the company grow to increase ROI. </p>
            <p>We say value-like as value investors buy companies at their low price points and usually hold onto these wonderful companies for the long-term; however, the opportunity exists to also realize quick profits on high demand post-IPO, then selling the stock to realize a quick capital gain profit. </p>
          </div>
        </div>
      </div>        
      
    </div>
  </div>
</section>

<section class="block-section about-seedomega nopadd-xs">
  <div class="container-fluid">
    <div class="row flex">      

      <div class="left ">
        <div class="sec-block-holder">
          <div class="sec-block-title h3">SEEDOMEGA</div>
          <div class="sec-block-content">
            <p>We at SeedOmega strongly believe it is our #1 priority to provide the seeders’ with exclusive, and well-vetted quality IPOs and private placements companies that have the ability to provide you with strong performance returns.  </p>
            <p>When you invest in a company, you’re investing in the capability of the people who are able to deliver you results. Likewise, we at SeedOmega are founded based on our expertise in Corporate Consultancy, Corporate Governance, and Portfolio Management. </p>
          </div>
        </div>
      </div>  

      <div class="no-padding right purple-bg">
        <div class="sec-block-holder sec-img text-center">
          <img src="<?php echo View::url('app/default/views/assets/images/aboutus/logo-so.png'); ?>" class="img-responsive" alt="">
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="block-section about-vision">
  <div class="container-fluid">
    <div class="flex">      

      <div class="visionbox vborder-b text-center">
        <div class="vision-block-title">Vision</div>
        <div class="sec-block-content">
          <p>One-Stop platform where seeders connect with projects</p>
        </div>
      </div>

      <div class="visionbox vborder-t text-center">
        <div class="vision-block-title">Mission</div>
        <div class="sec-block-content">
          <p>To build a community to match seeders and projects together</p>
        </div>
      </div>

      <div class="visionbox vborder-b text-center">
        <div class="vision-block-title">Core Values</div>
        <div class="sec-block-content">
          <p>• Authentic <br>• Transparency <br>• Experiences</p>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="block-section about-management nopadd-xs">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-5 left">
        <div class="sec-block-holder sec-img text-center">
          <img src="<?php echo View::url('app/default/views/assets/images/aboutus/seamless-elipses.png'); ?>" class="img-responsive" alt="">
        </div>
      </div>

      <div class="col-lg-7 right ">
        <div class="sec-block-title h3">Our Management & Founders</div>
          <div class="sec-block-content">
            <p>Management fuels start-ups to turn investments (seed funds) into profits. </p>
            <p>Which is the reason why our background is deeply rooted in IPO Consultancy, in Corporate Advisory and in Fund Portfolio Management for High Net Worth Clients. </p>
            <p>Through our highly qualified, diverse experience and network to exclusive IPOs and Private placements SeedOmega is able to confidently deliver:</p>
          </div>
      </div>
      
    </div>
  </div>
</section>

<section class="block-section about-newage nopadd-xs">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-5 left">
        <div class="sec-block-title h3">It’s a New Age of Corporate Management, <br> Operations & Investing: </div>
      </div>

      <div class="col-lg-7 right">
        <div class="sec-block-holder no-padding">
          
          <div class="sec-block-content">
            <p>Your social involvement on seeding IPOs and Private companies assists in<br> delivering these high quality products/services (solutions) to the market,<br> that are genuinely seeking your valued input.  </p>
          </div>  
        </div>
      </div>
      
    </div>

    <div class="aimdeliver">
      <h4>We aim to deliver:</h4>
      <div class="row xs-pad-20">
        <div class="col-lg-3">
          <div class="aimd-numb">01.</div>
          <div class="aimd-content">A seamless, global, transparent platform - that you can trust for both seeders and companies</div>
        </div>
        <div class="col-lg-3">
          <img src="<?php echo View::url('app/default/views/assets/images/aboutus/about-us-4.jpg'); ?>" class="img-responsive" alt="">
          <div class="aimd-numb">02.</div>
          <div class="aimd-content">Well-vetted, quality IPOs </div>
        </div>
        <div class="col-lg-3">
          <div class="aimd-numb">03.</div>
          <div class="aimd-content">Constant, up-to-date knowledge on the companies to make accurate investment decisions </div>
        </div>
        <div class="col-lg-3">
          <img src="<?php echo View::url('app/default/views/assets/images/demo/rawpixel-369788-unsplash.jpg'); ?>" class="img-responsive" alt="">
          <div class="aimd-numb">04.</div>
          <div class="aimd-content">A community forum for seeders to collaborate on their seeded (backed) projects </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about-teambio" style="background-image: url(<?php echo View::url('app/default/views/assets/images/aboutus/about-us-5.jpg'); ?>);">
  <div class="container-fluid">
    <div class="sec-block-title h3">Team Bios and Their Experiences</div>
    <div class="row teambio">

      <div class="col-lg-4">
        <div class="teambio-item">
          <div class="teamimg">
            <div class="team-img">
              <img src="<?php echo View::url('app/default/views/assets/images/coreteam/chris.jpg'); ?>" alt="no img">
            </div>
            <a href="javascript:void();" class="teamlearn-more" data-target="#team-modal-1" data-toggle="modal">learn more</a>
          </div>
          <div class="team-name">
            <h4>Chris Chua <span>Partner</span></h4>
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="teambio-item">
          <div class="teamimg">
            <div class="team-img">
              <img src="<?php echo View::url('app/default/views/assets/images/coreteam/damien.jpg'); ?>" alt="no img">
            </div>
            <a href="javascript:void();" class="teamlearn-more" data-target="#team-modal-2" data-toggle="modal">learn more</a>
          </div>
          <div class="team-name">
            <h4>Damien Lee <span>Partner</span></h4>
          </div>
        </div>
      </div>

      <!--div class="col-lg-4">
        <div class="teambio-item">
          <div class="teamimg">
            <div class="team-img">
              <img src="<?php echo View::url('app/default/views/assets/images/coreteam/moises.jpg'); ?>" alt="no img">
            </div>
            <a href="javascript:void();" class="teamlearn-more" data-target="#team-modal-3" data-toggle="modal">learn more</a>
          </div>
          <div class="team-name">
            <h4>Moises Goloyugo <span>Lead Programmer</span></h4>
          </div>
        </div>
      </div-->

    </div>
  </div>
</section>


<div class="modal" id="team-modal-1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content nobg">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Person Info</h3>
                </div>
                <div class="block-content">
                  <div class="block text-center">
                    <div class="animated fadeInDown">
                      <div class="img-wrap img-avatar img-avatar128 img-avatar-thumb"><img src="<?php echo View::url('app/default/views/assets/images/coreteam/chris.jpg'); ?>"></div>
                      <div class="text-uppercase push-5-t text-info"><strong>Chris Chua</strong> <br><span>Partner</span></div>
                    </div>
                  </div>
                  <div class="animatedx fadeInUp">
                    <div class="backer-view">
                      <div class="text-divider font-w600 push-5-t text-left text-uppercase">
                        <span>Bio</span>
                      </div>

                      <p>Is a Co-founder of SeedOmega. Chris has experienced handling high networth clienteles and is active in the Finance industry. In 2017, He was promoted to the position of Partner at a Australia listed company which specializes in IPO Advisory and Corporate Finance.</p>
                      <p>Chris brings to SeedOmega his strong network of Australian partners to manage structure growth of the business and provide solutions to develop the business capacities both locally and abroad.</p>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer bg-white">                
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="team-modal-2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content nobg">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Person Info</h3>
                </div>
                <div class="block-content">
                  <div class="block text-center">
                    <div class="animated fadeInDown">
                      <div class="img-wrap img-avatar img-avatar128 img-avatar-thumb"><img src="<?php echo View::url('app/default/views/assets/images/coreteam/damien.jpg'); ?>"></div>
                      <div class="text-uppercase push-5-t text-info"><strong>Damien Lee</strong> <br> <span>Partner</span></div>
                    </div>
                  </div>
                  <div class="animatedx fadeInUp">
                    <div class="backer-view">
                      <div class="text-divider font-w600 push-5-t text-left text-uppercase">
                        <span>Bio</span>
                      </div>

                      <p>Is a Co-founder of SeedOmega. Damien holds a bachelor of Science in Economics and Management and comes from the Asset management background. In 2017, he was appointed Fund Director to a Licensed Fund for Professional Investors.</p>
                      <p>Damien brings to SeedOmega his in-depth knowledge when it comes to Corporate Advisory and performing Due Diligence on businesses. In addition, He has network of Funds Managers ready to seed the projects.</p>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer bg-white">                
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<!--div class="modal" id="team-modal-3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content nobg">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Person Info</h3>
                </div>
                <div class="block-content">
                  <div class="block text-center">
                    <div class="animated fadeInDown">
                      <div class="img-wrap img-avatar img-avatar128 img-avatar-thumb"><img src="<?php echo View::url('app/default/views/assets/images/coreteam/moises.jpg'); ?>"></div>
                      <div class="text-uppercase push-5-t text-info"><strong>Moises Goloyugo</strong> <br> <span>Lead Programmer</span></div>
                    </div>
                  </div>
                  <div class="animatedx fadeInUp">
                    <div class="backer-view">
                      <div class="text-divider font-w600 push-5-t text-left text-uppercase">
                        <span>Bio</span>
                      </div>

                      <p>Is a seasoned programmer with more than 10 years of experience in Object Oriented Programming in PHP, MySQL, JavaScript and Ajax.</p>
                      <p>Moises brings to SeedOmega his constant creative and highly efficient team of passionate programmers to constantly enhance the platform’s user experiences.</p>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer bg-white">                
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> Close</button>
            </div>
        </div>
    </div>
</div-->
<!-- /page content -->
<?php View::footer(); ?>