<?php 
View::$title = 'Privacy Policy';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container">
        <h1>SeedOmega Privacy Policy</h1>
        <p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>
    </div>
    </article>
</section>

<!--section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul class="list-unstyled">
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$title; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section -->

<section class="halfgray">
    <!-- Page Content -->
    <article class="container about-section">

        <div class="body-longtext">
            <div class="bl-title">What personal information do we collect from the people that visit our blog, website or app?</div>
            <div class="bl-text"><p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number or other details to help you with your experience.</p>
            </div>

            <div class="bl-title">When do we collect information?</div>
            <div class="bl-text"><p>We collect information from you when you register on our site or enter information on our site.</p>
            </div>

            <div class="bl-title">How do we use your information?</div>
            <div class="bl-text"><p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
            <div class="bodyp">
                <ul class="list-unstyled">
              <li><i class="si si-check"></i> To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
              <li><i class="si si-check"></i> To improve our website in order to better serve you.</li>
              <li><i class="si si-check"></i> To allow us to better service you in responding to your customer service requests.</li>
              <li><i class="si si-check"></i> To quickly process your transactions.</li>
              <li><i class="si si-check"></i> To send periodic emails regarding your order or other products and services.</li>
            </ul></div>
            </div>

            <div class="bl-title">How do we protect your information?</div>
            <div class="bl-text"><p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.</p>
            <p>We do not use Malware Scanning.</p>
            <p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>
            <p>We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.</p>
            <p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
            </div>

            <div class="bl-title">Do we use 'cookies'?</div>
            <div class="bl-text"><p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>
            </div>

            <div class="bl-title">We use cookies to:</div>
            <div class="bl-text"><div class="bodyp">
                <ul class="list-unstyled">
                  <li><i class="si si-check"></i> Understand and save user's preferences for future visits.</li>
                </ul>
            </div>
            <p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>
            </div>

            <div class="bl-title">If users disable cookies in their browser:</div>
            <div class="bl-text"><p>If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.Some of the features that make your site experience more efficient and may not function properly.</p>
            </div>

            <div class="bl-title">Third-party disclosure</div>
            <div class="bl-text"><p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information.</p>
            </div>

            <div class="bl-title">Third-party links</div>
            <div class="bl-text"><p>We do not include or offer third-party products or services on our website.</p>
            </div>

            <div class="bl-title">Google</div>
            <div class="bl-text"><p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en" target="_BLANK">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a> </p>
            <p>We have not enabled Google AdSense on our site but we may do so in the future.</p>
            </div>

            <div class="bl-title">California Online Privacy Protection Act</div>
            <div class="bl-text"><p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf</p>
            </div>

            <div class="bl-title">According to CalOPPA, we agree to the following:</div>
            <div class="bl-text"><p>Users can visit our site anonymously.</p>
            <p>Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website. Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.</p>
            <p>You will be notified of any Privacy Policy changes:</p>
            <div class="bodyp">
                <ul class="list-unstyled">
                  <li><i class="si si-check"></i> On our Privacy Policy Page</li>
                </ul>
            </div>
            <p>Can change your personal information:</p>
            <div class="bodyp">
                <ul class="list-unstyled">
                  <li><i class="si si-check"></i> By logging in to your account</li>
                </ul>
            </div>
            </div>

            <div class="bl-title">How does our site handle Do Not Track signals?</div>
            <div class="bl-text"><p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.</p>
            </div>

            <div class="bl-title">Does our site allow third-party behavioral tracking?</div>
            <div class="bl-text"><p>It's also important to note that we do not allow third-party behavioral tracking</p>
            </div>

            <div class="bl-title">COPPA (Children Online Privacy Protection Act)</div>
            <div class="bl-text"><p>When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.</p>
            <p>We do not specifically market to children under the age of 13 years old. Do we let third-parties, including ad networks or plug-ins collect PII from children under 13?</p>
            </div>

            <div class="bl-title">Fair Information Practices</div>
            <div class="bl-text"><p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
            </div>

            <div class="bl-title">In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</div>
            <div class="bl-text"><p>We will notify you via email</p>
            <div class="bodyp"><ul class="list-unstyled"><li><i class="si si-check"></i> Within 1 business day</li></ul></div>
            <p>We will notify you via phone call</p>
            <div class="bodyp"><ul class="list-unstyled"><li><i class="si si-check"></i> Within 1 business day</li></ul></div>
            <p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>
            </div>

            <div class="bl-title">CAN SPAM Act</div>
            <div class="bl-text"><p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>
            </div>

            <div class="bl-title">We collect your email address in order to:</div>
            <div class="bl-text"><div class="bodyp"><ul class="list-unstyled"><li><i class="si si-check"></i> Send information, respond to inquiries, and/or other requests or questions</li></ul></div>
            </div>

            <div class="bl-title">To be in accordance with CANSPAM, we agree to the following:</div>
            <div class="bl-text"><div class="bodyp"><ul class="list-unstyled"><li><i class="si si-check"></i> Not use false or misleading subjects or email addresses</li></ul></div>
            </div>

            <div class="bl-title">If at any time you would like to unsubscribe from receiving future emails, you can email us at</div>
            <div class="bl-text"><p>and we will promptly remove you from <b>ALL</b> correspondence.</p>
            </div>

            <div class="bl-title">Contacting Us</div>
            <div class="bl-text"><p>If there are any questions regarding this privacy policy, you may contact us using the information below.</p>
            <p>SeedOmega support@seedomega.com</p>
            </div>
        </div>


    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>
