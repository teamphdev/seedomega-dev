<?php 
View::$title = 'Our Solution';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>">Contact us</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="white">
   <article class="container contactpage">
      <div class="row">
         <div class="col-lg-12">
            <h3 style="text-align: left;">Solutions</h3>

            <p style="text-align: left;">SeedOmega strive to transforms the traditional pathway to early stage investment opportunities in companies by connecting investors and companies online though our revolutionary platform. The platform aims to empower investors by providing them with access to a unique selection of well vetted private and IPO deals.</p>
            <p style="text-align: left;">SeedOmega’s Revolutionary platform opens the gateway for investors to access a digital marketspace in which capital market relationships would greatly enhanced both efficiency and transparency.</p>
            
         </div>
      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>