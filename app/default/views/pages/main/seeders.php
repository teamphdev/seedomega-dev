<?php 
View::$title = 'Seeders';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom heading-title-v2" style="background-image: url(<?php echo View::url('app/default/views/assets/images/aboutus/about-us-header.jpg'); ?>);"></section>
<section class="heading-title-text">
  <div class="heading-line"></div>
  <h1 class="aus-heading-title"><?php echo View::$title; ?></h1>
</section>

<section class="ipo-about seeders-about">
  <div class="container-fluid">

    <div class="ipolistitems">
      <div class="flex">
        <div class="left">
          <div class="sec-block-holder sec-img">
            <img src="<?php echo View::url('app/default/views/assets/images/ipo/ipo-about.jpg'); ?>" alt="">
          </div>
        </div>
        <div class="right gray-bg pad-50-t">
          <div class="sec-block-holder">
            <div class="sec-block-content">
              <h2 class="sec-block-title librebaskerville">You want global <br> opportunities with <br> strong returns that have outperformed <br> the market…</h2>
            </div>
          </div>
        </div>
        
      </div>

      <div class="ipo-regbtn">
        <a href="/users/signup" class="btn btn-primary btn-rounded btn-lg text-uppercase">Register here for <strong>FREE</strong></a>
      </div>
    </div>

  </div>
</section>

<section class="seeder-transferring">
  <div class="container-fluid">
    <h1 class="sec-block-title text-center">Transferring wealth generating opportunities ONCE RESERVED for wealthy and large institutional investors to the small retail investors</h1>

    <div class="row">
      <div class="col-lg-6 left">
        <div class="sec-block-content">
          <p>Previously, the regular investor unfortunately did not have access to these wonderful investment opportunities – so we set out to change that.</p>

          <p>We decided to provide the regular investor with direct access, giving them the same investment opportunities as the Big Banks, and the Large Institutional Portfolio Managers. </p>

          <p>In short, we bridged the gap in the lack of available IPOs and private placements of these wonderful opportunities that exist to the regular investor. In addition, we developed a process to provide the latest company knowledge beyond the prospectus to aid the investor in greater analysis. </p>

          <p>We did this by creating the most advanced and the most simplified platform to connect seeders to quality companies that would provide high quality investment opportunities, and create real change in the product development process. </p>
        </div>
      </div>
      <div class="col-lg-6 right">
        <div class="sec-block-content">
          <p>Real change – traditional communication channels between investors and companies in these types of investments rely on email, and AGM’s (annual general meetings – one per year). This process is slow and dated. The transfer and processing of information from this traditional (old) process tedious and takes a long time and so change is slow. </p>

          <p>With our advanced communication platforms, the investors (seeders) can now communicate DIRECTLY and QUICKLY to companies who genuinely WANT to hear your voice and opinions in affecting positive change on the development of a producing quality product that will be highly successful. </p>          
        </div>
      </div>
    </div>
  </div>
</section>

<section class="seeder-performance">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-5 no-padding">
        <div class="sec-block-holder sec-img">
          <img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-expected-performance.jpg'); ?>" alt="">
        </div>
      </div>
      <div class="col-lg-7">
        <div class="sec-block-holder">
          <div class="sec-block-title xs-pad-50-t h3">Expected Type of Performance</div>
          <div class="sec-block-content">
            <p>Expected Type of Performance</p>
            <p>We say value-like as value investors buy companies at their low price points and usually hold onto these wonderful companies for the long-term; however, the opportunity exists to also realize quick profits on high demand post-IPO, then selling the stock to realize a quick capital gain profit. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="block-section seeders-aimdeliver no-padding nopadd-xs">
  <div class="container-fluid no-padding">
    <div class="flex">      

      <div class="left lightgray-bg no-padding">
        <div class="sec-block-holder pad-50-t">
          <div class="sec-block-title librebaskerville text-center h3">SeedOmega Aims to Deliver: </div>
          <div class="sec-block-content">

            <ul class="ipo-benefitslist pad-10-l">
              <li>
                <div class="img-ipob">01</div> 
                Long-term investment opportunities
              </li>
              <li>
                <div class="img-ipob">02</div> 
                Short-term profit gains
              </li>
              <li class="">
                <div class="img-ipob">03</div> 
                Diversification and convenience
              </li>
            </ul>

          </div>
        </div>
      </div>  

      <div class="right gray-bg">
        <div class="sec-block-holder pad-50-t">
          <div class="sec-block-content">
            <div class="sec-block-title librebaskerville text-center h3">Highlights: </div>
            <ul class="frontlist-check">
              <li><i class="si si-check"></i> SeedOmega is 100% FREE for Seeders</li>
              <li><i class="si si-check"></i> 1st direct access portal for Initial Public Offerings and private companies with a global reach</li>
              <li><i class="si si-check"></i> Dedicated support team to assist the entire process from setup to seeding</li>
              <li><i class="si si-check"></i> Multi-channel communication platform for enhanced communications with backed companies</li>
            </ul>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="seeders-experience text-center no-padding">
  <div class="container-fluid no-padding">
    <img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-experience-whole.jpg'); ?>" alt="" class="img-responsive hidden-xs">
    <img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-experience-whole-xs.jpg'); ?>" alt="" class="img-responsive visible-xs">
  </div>
</section>

<section class="seeders-why">
  <div class="container-fluid">
    <div class="sec-block-title librebaskerville text-center h3">Why Crowdfunding & Why ASX IPOs? </div>
    <div class="row push-50-t">

      <div class="col-lg-6 left">
        <ul class="ipo-benefitslist pad-10-l">
          <li>
            <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-investment.png'); ?>" alt=""></div> 
            Crowdfunding uses the Internet to gather a large quantity of people to pool smaller amounts of money to seed an investing goal. 
          </li>
          <li>
            <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-crowdfunding.png'); ?>" alt=""></div> 
            This allows the regular investor (seeder) to optionally invest a smaller investment amount. 
          </li>
          <li class="">
            <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-money.png'); ?>" alt=""></div> 
            Seeders support projects they like socially, and financially. Ie. They like the vision behind the company and wish to support it as it aligns with their passion and socially impacts their shared values. 
          </li>
        </ul>
      </div>

      <div class="col-lg-6 right">
        <ul class="ipo-benefitslist pad-10-l">
          <li>
            <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-management.png'); ?>" alt=""></div> 
            IPOs and private projects are able to obtain the investments needed instead of going through large institutional investment firms requiring higher sums of money. 
          </li>
          <li>
            <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-teamwork.png'); ?>" alt=""></div> 
            Crowdfunding minimizes the individual investor’s risk (less investment per investor), and spreads the financial risk out among the crowd. 
          </li>
          <li class="">
            <div class="img-ipob"><img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-feedback.png'); ?>" alt=""></div> 
            In addition, the IPO’s and private companies gain access to not just financial investment but gain invaluable feedback from the backers via our embedded social community forum and via  our direct communication tools with the companies they back. 
          </li>
        </ul>
      </div>

    </div>
  </div>
</section>

<section class="gray seeders-advantages">
  <div class="container-fluid">
    <div class="sec-block-title librebaskerville text-center">Advantages of ASX IPO shares over Australian stock shares <br> <span class="circularStdmedium">2017 ASX IPO Performance Report</span></div>

    <div class="sec-block-title librebaskerville subtitle push-50-t font-18">Price Performance <br> <span class="circularStdmedium">2017 IPO Average Returns</span></div>

    <div class="row">
      <div class="col-lg-6 left">
        <img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-advantages-graph1.jpg'); ?>" alt="" class="img-responsive">
      </div>
      <div class="col-lg-6 right xs-pad-20-t">
        <p>The average IPO returns outperformed the ASX200 performance return of 7%, by 780% (nearly 8x) providing value-investing opportunities in a high P/E, or high priced market environment. </p>
        <p><strong>In addition: </strong></p>
        <p>Over the last 3 years, Smaller IPO companies (less than $50 million market capitalization) have outperformed large cap IPO companies (<strong>greater</strong> than $50 million market capitalization) by <strong>197.02%</strong>. </p>
      </div>
    </div>

    <p class="push-20 push-40-t"><strong>The little ones making the big prices performance at the end of the year!</strong></p>

    <div class="row">
      <div class="col-lg-6 left">
        <img src="<?php echo View::url('app/default/views/assets/images/seeders/seeders-advantages-graph2.jpg'); ?>" alt="" class="img-responsive">
      </div>
      <div class="col-lg-6 right xs-pad-20-t">
        <p><strong>“The smaller companies have consistently returned more than large cap listings over the past 3 years.”</strong> </p>
        <p>For the benefit our seeders financial gain, it makes sense to provide both opportunities to invest in large cap and in small cap IPO companies, as well as private placements.</p>
      </div>
    </div>
    
  </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>