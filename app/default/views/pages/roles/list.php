<?php 
View::$title = 'Roles';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">All Roles <small></small></h3>                
            </div>
            <div class="">
                <?php echo View::getMessage(); ?> 
                <table id="role-table" class="table table-divide js-dataTable-full-pagination" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-primary" href="<?php echo View::url("roles/add"); ?>">Add Role</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th width="30" class="text-center">ID</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th class="no-wrap">Dashboard Link</th>
                            <th class="no-sorting text-center" style="max-width:150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        foreach($roles as $role) { $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="a-center"><input type="checkbox" name="table_records" value="<?php echo $role->UserLevelID; ?>" class="flat"></td> -->                            
                            <td class="text-center"><?php echo $role->UserLevelID; ?></td>
                            <td><?php echo $role->Code; ?></td>
                            <td><?php echo $role->Name; ?></td>
                            <td><?php echo $role->Description; ?></td>
                            <td><?php echo $role->Link; ?></td>
                            <td class="text-center">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'roles/edit/'.$role->UserLevelID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'roles/delete/'.$role->UserLevelID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this role?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    // $(document).ready(function() {

    // });
</script>