<?php 
View::$title = 'Edit Role';
View::$bodyclass = User::info('Sidebar').' client-profile';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?>   
                <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="updaterole" />
                    <input type="hidden" name="roleid" value="<?php echo $role->UserLevelID; ?>" />
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Code <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <input type="text" value="<?php echo isset($role->Code) ? $role->Code : ''; ?>" id="name" name="Code" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Role Name <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <input type="text" value="<?php echo isset($role->Name) ? $role->Name : ''; ?>" id="name" name="Name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Dashboard Link <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <input type="text" value="<?php echo isset($role->Link) ? $role->Link : ''; ?>" id="link" name="Link" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <textarea class="form-control dowysiwyg" name="Description"><?php echo isset($role->Description) ? $role->Description : ''; ?></textarea>
                        </div>
                    </div>

                    <div class="col-md-7 col-sm-9 col-xs-12"><div class="ln_solid"></div></div>
                    <div class="clearfix"></div>
                    <?php $userCapa = isset($role->Capability) ? View::common()->stringToArray($role->Capability) : array(); ?>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">Capabilities</label>

                        <div class="col-md-7 col-sm-9 col-xs-9">
                            <div class="col-md-4 col-sm-6 col-xs-12 caplist">
                                <label class="css-input css-checkbox css-checkbox-sm css-checkbox-default"><input type="checkbox" id="check-toggle" value=""><span></span> Check All / Uncheck All</label>
                            </div>
                        </div>
                    </div>

                    <?php 
                    foreach($capabilities as $ckey => $cval){ 
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3"><?php echo $ckey; ?></label>
                            <div class="col-md-7 col-sm-9 col-xs-9">
                            <?php if(is_array($cval)) { 
                                foreach($cval as $citem){ ?>
                                    <div class="col-md-4 col-sm-6 col-xs-12 caplist">
                                        <label class="css-input css-radio css-radio-sm css-radio-default"><input type="checkbox" id="capability-<?php echo $citem->UserCapabilityID; ?>" name="capabilities[<?php echo $citem->UserCapabilityID; ?>]" <?php echo isset($userCapa[$citem->UserCapabilityID]) ? 'checked' : ''; ?> class="capabilities" value="<?php echo $citem->Name; ?>"><span></span> <?php echo $citem->Name; ?></label>
                                    </div>
                                <?php };
                            }; ?>

                            </div>
                        </div>
                        <?php }                                    
                    ?>

                    <div class="ln_solid"></div>
                    
                    <div class="clearfix"></div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3">On Save</label>
                            <div class="col-md-7 col-sm-9 col-xs-9">
                           
                                    <div class="col-md-12 col-sm-12 col-xs-12 caplist">
                                        <label class="css-input css-checkbox css-checkbox-sm css-checkbox-default"><input type="checkbox" name="applyRoleToUSsers"><span></span> Apply to all users in this role</label>
                                    </div>
                               
                            </div>
                        </div>
                        

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('roles'); ?>" class="btn btn-danger btn-rounded">Back</a>       
                            <a href="<?php echo View::url('roles/apply/'.$role->UserLevelID); ?>" class="btn btn-info btn-rounded">Apply Current Settings to Users</a>
                            <button id="send" type="submit" class="btn btn-primary btn-rounded">Save Role</button>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>