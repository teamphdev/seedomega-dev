<?php 
View::$title = 'Edit Package';
View::$bodyclass = User::info('Sidebar').' client-profile';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?>   
                <form class="form-horizontal form-label-left input_mask"  method="post">
                    <input type="hidden" name="action" value="updatepackage" />
                    <input type="hidden" name="packageid" value="<?php echo $package->PackageID; ?>" />

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Name <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <input type="text" value="<?php echo isset($package->Name) ? $package->Name : ''; ?>" id="name" name="Name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Amount <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <input type="text" value="<?php echo isset($package->Amount) ? $package->Amount : ''; ?>" id="name" name="Amount" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>


                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-7 col-sm-9 col-xs-12">
                            <textarea class="form-control dowysiwyg" name="Description"><?php echo isset($package->Description) ? $package->Description : ''; ?></textarea>
                        </div>
                    </div>

                    <div class="col-md-7 col-sm-9 col-xs-12"><div class="ln_solid"></div></div>

                        

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('packages'); ?>" class="btn btn-rounded btn-danger">Back</a>
                            <button id="send" type="submit" class="btn btn-rounded btn-primary">Save Package</button>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>