<?php 
View::$title = 'Capability Groups';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>

<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>">Capability Groups</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">All Capability Groups <small></small></h3>
            </div>
            <div class="block">
                <?php echo View::getMessage(); ?> 
                <table id="role-table" class="table table-divide js-dataTable-full-pagination" width="100%" addbutton='<a class="btn btn-rounded btn-primary" href="<?php echo View::url("capabilitygroups/add"); ?>">Add Group</a>'>
                    <thead>
                        <tr class="headings">
                            <th width="150" class="text-center no-wrap">Groups ID</th>
                            <th>Name</th>
                            <th width="200" class="no-sorting text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if(count($groups)) {
                            foreach($groups as $group) { $cntr++;
                            ?>
                            <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                <td class="text-center"><?php echo $group->UserCapabilityGroupID; ?></td>
                                <td><?php echo $group->Name; ?></td>
                                <td class="text-center">
                                    <?php if( User::can( 'Administer All' ) ){ ?>
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo View::url( 'capabilitygroups/edit/'.$group->UserCapabilityGroupID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                    <li><a href="<?php echo View::url( 'capabilitygroups/delete/'.$group->UserCapabilityGroupID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this capability group?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="btn-group view-info">
                                            <a href="<?php echo View::url( 'capabilitygroups/edit/'.$group->UserCapabilityGroupID ); ?>" class="btn btn-sm btn-default" title="" data-toggle="tooltip">Edit</a>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php }
                        }else{ ?>
                            <tr class="odd pointer">
                                <!--td class="a-center"><input type="checkbox" name="table_records" value="<?php //echo $user->ProductID; ?>" class="flat"></td-->
                                <td colspan="3">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>