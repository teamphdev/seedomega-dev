<?php 
View::$title = 'Add Capability Group';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <h3 class="page-title"><?php echo View::$title; ?></h3>
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" novalidate>
                    <input type="hidden" name="action" value="addgroup" />
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Capability Group Name <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" value="<?php echo isset($group->Name) ? $group->Name : ''; ?>" name="Name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-7 col-md-offset-3">
                            <a href="<?php echo View::url('capabilitygroups'); ?>" class="btn btn-rounded btn-danger">Cancel</a>
                            <button id="send" type="submit" class="btn btn-rounded btn-primary">Add Group</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>