<?php 
View::$title = 'Add Option';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" novalidate>
                    <input type="hidden" name="action" value="addoption" />
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Group <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php
                                View::form(
                                    'selecta',
                                    array(
                                        'name'=>'OptionGroupID',
                                        'options'=>$groups,
                                        'value'=>isset($groups->OptionGroupID) ? $groups->OptionGroupID : '',
                                        'class'=>'form-control',
                                        'custom' => 'required'
                                    )
                                ); 
                            ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Form Type <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="FormType" class="form-control" required="">
                                <option value="text">Text</option>
                                <option value="select">Select</option>
                                <option value="upload">Upload</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Option Key</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" value="" placeholder="Ex: option_key, logo_title, field_key" name="OptionKey" id="text" class="form-control" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Option Label</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" value="" name="OptionLabel" class="form-control" required="">
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('options'); ?>" class="btn btn-rounded btn-danger">Back</a>
                            <button type="submit" class="btn btn-rounded btn-primary">Add Option</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>