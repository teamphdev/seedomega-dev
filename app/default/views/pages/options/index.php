<?php 
View::$title = 'Manage Options';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
$env = Config::get('ENVIRONMENT');
?>
<?php $userinfo = User::info(); ?>

<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Options Manager <small></small></h3>
            </div>
            <div class="block">
                <?php echo View::getMessage(); ?> 
                <table class="table table-divide table-striped js-dataTable-full-pagination" width="100%" addbutton='<a class="btn btn-rounded btn-primary" href="<?php echo View::url("options/add"); ?>">Add Option</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th class="text-center no-wrap" width="100">Option ID</th>
                            <th class="text-center no-wrap">Group Name</th>
                            <th class="text-center no-wrap">Form Type</th> 
                            <th class="text-center no-wrap">Option Key</th> 
                            <th class="text-center no-wrap">Option Label</th>
                            <th class="no-sorting text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cntr = 0;
                        if(count($optionLists)) {
                        foreach($optionLists as $optionList) { $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="a-center"><input type="checkbox" name="table_records" value="<?php echo $item->ProductItemID; ?>" class="flat"></td> -->
                            <td class="text-center"><?php echo $optionList->OptionID; ?></td>
                            <td class="text-center"><?php echo ucwords($optionList->GroupName); ?></td>
                            <td class="text-center"><?php echo ucwords($optionList->FormType); ?></td>
                            <td class="text-center"><?php echo $optionList->OptionKey; ?></td>
                            <td class="text-center"><?php echo $optionList->OptionLabel; ?></td>
                            <td width="120px" class="text-center">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'options/edit/'.$optionList->OptionID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'options/delete/'.$optionList->OptionID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this option?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!--td class="a-center"><input type="checkbox" name="table_records" value="<?php //echo $user->ProductID; ?>" class="flat"></td-->
                            <td class="text-center" colspan="6">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<?php View::footer(); ?>