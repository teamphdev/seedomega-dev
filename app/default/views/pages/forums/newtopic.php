<?php 
  View::$title = $title.': New Topic';
  View::$bodyclass = User::info('Sidebar').' forum-new-topic';
  View::header();
?>

<?php /*
  <section class="breadcrumb">
      <article class="container">
        <div class="row">
          <div class="col-lg-6">
            <ul>
              <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                <li><a class="link-effect" href="<?php echo View::url(); ?>">Home</a></li>
                <li class="fa fa-angle-right"></li>
                <li><a class="link-effect" href="<?php echo View::url( 'forums'); ?>">Forums</a></li>
                <li class="fa fa-angle-right"></li>
                <li><a class="link-effect" href="<?php echo View::url( 'forums/dashboard'); ?>">Dashboard</a></li>
                <li class="fa fa-angle-right"></li>
                <li class="text-muted">New Topic</li>
            </ul>
          </div>
        </div>
      </article>
  </section>
*/ ?>
  <!-- Page Content -->
  <section class="gray">
    <article class="container" id="intor-section">
        <div class="block" style="min-height: 446px;">
            <div class="block-header">
                <h4 class="block-title text-left"><i class="fa fa-plus"></i> New Topic</h4>
            </div>
            

            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <input name="image" type="file" id="upload" class="hidden" onchange="">
                <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="addtopic">
                    <input type="hidden" name="topic[UserID]" value="<?php echo isset( $userinfo->UserID ) ? $userinfo->UserID : ''; ?>">
                    <div class="form-group">
                        <div class="form-material form-material-primary">
                            <select class="js-select2 form-control select2-hidden-accessible-show" id="topic[IDs]" name="topic[IDs]" style="width: 100%;" data-placeholder="Please select a category" tabindex="-1" aria-hidden="true">
                                <option></option>
                                <?php if( isset( $options ) && count( $options ) ){
                                foreach( $options as $k => $v){ ?>
                                    <?php if( strpos( $k, 'disabled' ) !== false ){ ?>
                                            <option value="" disabled=""><?php echo isset( $v ) ? $v : ''; ?></option>
                                    <?php } else { ?>                                                    
                                            <option value="<?php echo isset( $k ) ? $k : ''; ?>"><?php echo isset( $v ) ? $v : ''; ?></option>
                                    <?php } ?>
                                <?php } } ?>
                            </select>
                            <!-- <span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;">
                                <span class="selection">
                                    <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-topic[IDs]-container">
                                        <span class="select2-selection__rendered" id="select2-topic[IDs]-container">
                                            <span class="select2-selection__placeholder">Please select a category</span>
                                        </span>
                                        <span class="select2-selection__arrow" role="presentation">
                                            <b role="presentation"></b>
                                        </span>
                                    </span>
                                </span>
                                <span class="dropdown-wrapper" aria-hidden="true"></span>
                            </span> -->
                            <label for="topic-forum">Forum Category <span class="text-danger">*</span></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material form-material-primary">
                            <input class="form-control" type="text" id="topic-title" name="topic[Title]" placeholder="Please enter a title">
                            <label for="topic-title">Topic Title <span class="text-danger">*</span></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-material form-material-primary">
                            <input class="form-control" type="text" id="topic-description" name="topic[Description]" placeholder="Please enter a description">
                            <label for="topic-description">Topic Description</label>
                        </div>
                    </div>

                    <div class="form-group hidden">
                        <div class="form-material form-material-primary">
                            <input class="form-control" type="text" id="topic-epigram" name="topic[Epigram]" placeholder="It's nice to have any words to ponder">
                            <label for="topic-description"><?php echo Lang::get('FORUM_FRM_WTP'); ?></label>
                        </div>
                    </div>

<!--                             <div class="form-group">
                        <div class="form-material form-material-primary">
                            <input class="js-tags-input form-control" type="text" id="topic-tags" name="topic-tags" value="Question" data-tagsinput-init="true" style="display: none;">
                            <div id="topic-tags_tagsinput" class="tagsinput" style="width: 100%; min-height: 36px; height: 36px;">
                                <div id="topic-tags_addTag">
                                    <input id="topic-tags_tag" value="" data-default="Add tag" style="color: rgb(102, 102, 102); width: 68px;">
                                </div>
                                <div class="tags_clear"></div>
                            </div>
                            <label for="topic-tags">Topic Tags <span class="text-danger">*</span></label>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <textarea class="form-control tinyMCE" name="topic[Content]" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <button class="btn btn-rounded btn-primary" type="submit"><i class="fa fa-plus"></i> Add Topic</button>
                            <button class="btn btn-rounded btn-warning hidden" type="button"><i class="fa fa-eye"></i> Preview</button>
                            <button class="btn btn-rounded btn-primary hidden" type="button"><i class="fa fa-floppy-o"></i> Draft</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </article>
  </section>
  
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        //$(".js-select2").select2();
    });
</script>