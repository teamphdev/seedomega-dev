<?php 
View::$title = 'Add Category';
View::$bodyclass = '';
View::header();
$s = View::$segments;
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( $s[0] ); ?>"><?php echo $s[0]; ?></a></li>
                <?php if( isset( $s[1] ) ){ ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url( $s[0].'/'.$s[1] ); ?>"><?php echo $s[1]; ?></a>
                    <?php if( isset( $s[2] ) ){ ?></li>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo $s[2]; ?></li>
                    <?php } else { ?>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo View::$title; ?></li>
                <?php } } else { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><?php echo View::$title; ?></li>
                <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">Add New Category</div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>   
                <form class="form-horizontal form-ui form-label-left input_mask" method="post">
                    <input type="hidden" name="action" value="add" />
                    
                    <div class="form-group">
                        <label class="" for="name">
                            Category Name <span class="required">*</span>
                        </label>
                        <input type="text" value="" name="category[CatName]" required="required" class="form-control">
                    </div>                    
                    
                    <div class="form-group">
                        <label class="">Description</label>
                        <textarea class="form-control dowysiwyg" name="category[CatDescription]"></textarea>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group text-center">
                        <a href="<?php echo View::url('forums/categories'); ?>" class="btn btn-info btn-rounded"><i class="si si-action-undo"></i> Back</a>
                        <button id="send" type="submit" class="btn btn-rounded btn-primary">Add Category</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>