<?php 
View::$title = 'Categories';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1>Forum <?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( View::$segments[0] ); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><?php echo View::$title; ?></li>
              <?php } ?>
          </ul>
        </div>
        <div class="col-lg-6">
            
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?>
                <table id="role-table" class="table table-divide table-header-bg table-vcenter js-dataTable-full-pagination" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('forums/categories/add'); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Add Category</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th width="200" class="text-center"><i class="fa fa-calendar text-gray"></i> Date Added</th>
                            <th><i class="fa fa-contao text-gray"></i> Category</th>
                            <th><i class="fa fa-book text-gray"></i> Description</th>
                            <th class="no-sorting text-center" style="width:15%;min-width:110px;"><i class="fa fa-edit text-gray"></i> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if(count($categories)){
                            foreach( $categories as $cat ){ $cntr++;
                            ?>
                            <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> category-id-<?php echo $cat->ClientCatID; ?>">                            
                                <td class="text-center">
                                    <?php echo isset( $cat->CatDateAdded ) && $cat->CatDateAdded != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $cat->CatDateAdded ) ) : '-'; ?>
                                </td>
                                <td><?php echo isset( $cat->CatName ) ? ucwords( $cat->CatName ) : '-'; ?></td>
                                <td><?php echo isset( $cat->CatDescription ) ? ucwords( $cat->CatDescription ) : '-'; ?></td>
                                <td class="text-center">
                                    <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                    <div class="">
                                        <div class="dropdown more-opt">
                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?php echo View::url( 'forums/categories/edit/'.$cat->CatID ); ?>" title="Edit Category" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                <li><a href="<?php echo View::url( 'forums/categories/delete/'.$cat->CatID ); ?>" title="Delete Category" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this category?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                        <div class="">
                                            <a href="<?php echo View::url( 'forums/categories/edit/'.$cat->CatID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit Category" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                        </div>                                    
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } 
                        } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    // $(document).ready(function() {


    // });
</script>