<?php 
  View::$title = $title.' Forums';
  View::$bodyclass = User::info('Sidebar').' forum-dashboard';
  View::header();
?>
<?php /*
  <section class="breadcrumb">
      <article class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul>
              <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
              <li><a href="<?php echo View::url(); ?>">Home</a></li>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url( 'forums' ); ?>">Forums</a></li>
              <li class="fa fa-angle-right"></li>
              <li>Dashboard</li>
            </ul>
          </div>
        </div>
      </article>
  </section>
*/ ?>
  <!-- Page Content -->
  <section class="gray">
    <article class="container projects style-2" id="intor-section">
      <div class="row animated fadeInDown">
          <div class="col-md-6 col-sm-6 col-xs-6"><h3 class="page-title" style="text-align: left"><?php echo View::$title; ?></h3></div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="pull-right">
              <a class="btn btn-info btn-rounded" href="<?php echo view::url( 'forums/newtopic' ); ?>"><i class="fa fa-plus"></i> New Topic</a>
              <?php if( $userinfo->Code == 'CLN' || User::can( 'Manage Categories' ) ){ ?>
                <a class="btn btn-rounded btn-success" href="<?php echo view::url( 'forums/topics' ); ?>">Topics</a>
                <a class="btn btn-rounded btn-success" href="<?php echo view::url( 'forums/categories' ); ?>">Categories</a>
                <a class="btn btn-rounded btn-success" href="<?php echo view::url( 'forums/subcategories' ); ?>">Sub Categories</a>
              <?php } ?>
            </div>
          </div>
      </div>
      <div class="block" style="min-height: 446px;">
        <div class="block-content-full">
          <table class="table table-bordered table-hover table-vcenter table-header-bg">
          <?php if( isset( $forums ) ){
          if( count( $forums ) ){
          foreach( $forums as $k => $v ){ ?>
            
            <thead>
              <tr>
                <th colspan="2"><?php echo isset( $v[0] ) ? $v[0]->CatName : ''; ?></th>
                <th class="text-center hidden-sm" style="width: 100px;">Topics</th>
                <th class="text-center hidden-sm" style="width: 100px;">Posts</th>
                <th class="text-center hidden-sm" style="width: 200px;">Last Post</th>
              </tr>
            </thead>
            <tbody>
              <?php if( isset( $v ) ){ foreach( $v as $f ){ ?>
              <tr>
                <td class="text-center" style="width: 75px;">
                  <i class="<?php echo isset( $f->SubCatIconClass ) ? $f->SubCatIconClass : ''; ?>"></i>
                </td>
                <td style="text-align: left;">
                  <h4 class="h5 font-w600 push-5">
                    <?php $scname = str_ireplace( '?', '', htmlentities( $f->SubCatNameSlug ) ); ?>
                    <a href="<?php echo View::url( 'forums/category/'.$f->SubCatID.'/'.$scname ); ?>"><?php echo isset( $f->SubCatName ) ? $f->SubCatName : ''; ?></a>
                  </h4>
                  <div class="font-s13 text-muted"><?php echo isset( $f->SubCatDescription ) ? $f->SubCatDescription : ''; ?></div>
                </td>
                <td class="text-center hidden-xs hidden-sm">
                  <a class="font-w600" href="javascript:void(0)"><?php echo isset( $f->Topics ) ? $f->Topics : 0; ?></a>
                </td>
                <td class="text-center hidden-xs hidden-sm">
                  <a class="font-w600" href="javascript:void(0)"><?php echo isset( $f->Posts ) ? $f->Posts : 0; ?></a>
                </td>
                <td class="hidden-xs hidden-sm text-left">
                  <span class="font-s13">
                    <?php 
                    echo isset( $f->Data['PostedBy'] ) ? '<i class="text-muted">by</i> '.$f->Data['PostedBy'] : '-';
                    echo isset( $f->Data['DatePosted'] ) ? '<br>'.date('F j, Y', strtotime( $f->Data['DatePosted'] ) ) : ''; 
                    ?>
                  </span>
                </td>
              </tr>
              <?php } } ?>
            </tbody>

          <?php } } else { ?>

            <thead>
              <tr>
                <th colspan="2"></th>
                <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Topics</th>
                <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Posts</th>
                <th class="hidden-xs hidden-sm" style="width: 200px;">Last Post</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colspan="99">
                  <div class="h5 hidden-xs text-left">No data found! Contact the administrator to create Categories and Sub-Categories.</div>
                </td>
              </tr>
            </tbody>

          <?php } } ?>

          </table>
        </div>
      </div>
    </article>
  </section>
  
<?php View::footer(); ?>