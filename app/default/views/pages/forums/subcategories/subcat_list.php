<?php
    View::$title = 'Sub-Categories';
    View::$bodyclass = 'dev';
    View::header();
?>
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1>Forum <?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( View::$segments[0] ); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset( View::$segments[1] ) ){ ?>
                <li class="fa fa-angle-right"></li>
                <li><?php echo View::$title; ?></li>
              <?php } ?>
          </ul>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
    </article>
</section>
*/ ?>

<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page">
        <?php echo View::getMessage(); ?>
        <!-- <a href="<?php echo View::url( 'forums/subcategories/add'.'/'.$category->CatName.'/'.$category->CatID ); ?>"><i class="fa fa-plus"></i></a> -->
        <div class="block">
            <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
            <?php $ctr = 0;
            if( isset( $categories ) ){
                if( count( $categories ) ){
                foreach( $categories as $category ){ $ctr++; ?>
                <li class="<?php echo $ctr == 1 ? 'active' : ''; ?>">
                    <a href="#category-<?php echo $category->CatID; ?>"><?php echo isset( $category->CatName ) ? $category->CatName : '-'; ?> <span class="badge badge-success"></span></a>
                </li>
            <?php } } } ?>
            </ul>
            <div class="tab-content">

            <?php $ctr = 0; $nodata = '<div class="h5 hidden-xs">No category data found! Click <a href="'.View::url( 'forums/categories/add' ).'">here</a> to add category.</div><br>';
            if( isset( $categories ) ){
                if( count( $categories ) ){
                    foreach( $categories as $category ){ $ctr++; ?>
                <div class="subcategory tab-pane fade fade-up in <?php echo $ctr == 1 ? 'active' : ''; ?>" id="category-<?php echo $category->CatID; ?>" data-catid="<?php echo $category->CatID; ?>" data-catname="<?php echo $category->CatName; ?>">
                    <table id="scdatatable-<?php echo $category->CatID; ?>" class="table table-hover table-vcenter table-divide js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center no-wrap"><i class="fa fa-calendar text-gray"></i> Date Added</th>
                            <th class="text-center no-wrap"><i class="fa fa-subscript text-gray"></i> Sub-Category</th>
                            <th class="text-center"><i class="fa fa-book text-gray"></i> Description</th>
                            <th class="text-center no-wrap"><i class="fa fa-image text-gray"></i> Icon Class</th>
                            <th class="text-center" style="max-width:155px;"><i class="fa fa-edit text-gray"></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( isset( $subcategories[$category->CatID] ) ){
                            foreach( $subcategories[$category->CatID] as $sub ): ?>
                                <tr>
                                    <td class="text-center">
                                        <?php echo isset( $sub->SubCatDateAdded ) && $sub->SubCatDateAdded != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $sub->SubCatDateAdded ) ) : '-'; ?>
                                    </td>
                                    <td><?php echo $sub->SubCatName; ?></td>
                                    <td><?php echo $sub->SubCatDescription; ?></td>
                                    <td><span class="span-icon"><i class="<?php echo str_ireplace( '2x', '1x', $sub->SubCatIconClass ); ?> absolute td-icon"></i> <?php echo $sub->SubCatIconClass; ?></span></td>
                                    <td class="text-center">
                                        <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo View::url( 'forums/subcategories/edit/'.$sub->SubCatID.'/'.$category->CatName ); ?>" title="Edit Sub-Category" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                    <li><a href="<?php echo View::url( 'forums/subcategories/delete/'.$sub->SubCatID ); ?>" title="Delete Sub-Category" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this sub-category?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php } else { ?>
                                            <div class="">
                                                <a href="<?php echo View::url( 'forums/subcategories/edit/'.$sub->SubCatID.'/'.$category->CatName ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit Sub-Category" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                            </div>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
            <?php } else { echo $nodata; } ?>
            <?php } else { echo $nodata; } ?>

            </div>
        </div>
        <input type="hidden" id="uriadd" value="<?php echo View::url( 'forums/subcategories/add' ); ?>" />

    </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    var added = false;
    var uriadd = $( '#uriadd' ).val();
    $( document ).ready( function(){

        $('.subcategory').each( function(){
            var catid = $( this ).attr( 'data-catid' );
            var catname = $( this ).attr( 'data-catname' );
            var href = uriadd + '/' + catid + '/' + catname;
            var button = '&nbsp;&nbsp;<a class="btn btn-info btn-rounded" href="' + href + '">Add Sub-Category</a>';
            $( this ).find( '.dataTables_filter' ).append( button );
            $( ".duplicate" ).remove();
            added = true;
        });

    });
    if( !added ){
        $('.subcategory').each( function(){
            var catid = $( this ).attr( 'data-catid' );
            var catname = $( this ).attr( 'data-catname' );
            var href = uriadd + '/' + catid + '/' + catname;
            var button = '<div class="col-lg-12 text-right duplicate" style="padding-bottom:10px;"><a class="btn btn-info btn-rounded" href="' + href + '">Add Sub-Category</a></div>';
            var tableid = '#scdatatable-' + catid;
            $( this ).find( tableid ).before( button );
        });
    }
</script>