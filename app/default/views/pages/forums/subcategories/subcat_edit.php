<?php 
View::$title = 'Edit Sub-Category';
View::$bodyclass = User::info('Sidebar').' client-profile';
View::header();
$s = View::$segments;
//print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( $s[0] ); ?>"><?php echo $s[0]; ?></a></li>
                <?php if( isset( $s[1] ) ){ ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url( $s[0].'/'.$s[1] ); ?>">Sub-Categories</a>
                    <?php if( isset( $s[2] ) ){ ?></li>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo $s[2]; ?></li>
                    <?php } else { ?>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo View::$title; ?></li>
                <?php } } else { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><?php echo View::$title; ?></li>
                <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header"><?php echo View::$title; ?></div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>   
                <form class="form-horizontal form-ui form-label-left input_mask" method="post">
                    <input type="hidden" name="action" value="edit" />
                    <input type="hidden" name="subcatid" value="<?php echo $catdata[0]->SubCatID; ?>" />
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Category Name</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" value="<?php echo $catname; ?>" disabled class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Sub-Category Name <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" value="<?php echo isset( $catdata[0]->SubCatName ) ? $catdata[0]->SubCatName : ''; ?>" name="category[SubCatName]" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Icon Class</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <!-- <input type="text" value="<?php echo isset( $catdata[0]->SubCatIconClass ) ? $catdata[0]->SubCatIconClass : ''; ?>" name="category[SubCatIconClass]" required="required" class="form-control col-md-7 col-xs-12"> -->
                            <span>
                            <?php 
                                $ivalue = isset( $catdata[0]->SubCatIconClass ) ? str_ireplace( " fa-2x", "", $catdata[0]->SubCatIconClass ) : "";
                                $idisplay = isset( $catdata[0]->SubCatIconClass ) ? $catdata[0]->SubCatIconClass : "fa fa-question fa-2x";
                                
                                View::form(
                                    'selecta',
                                    array(
                                        'name' => 'category[SubCatIconClass]',
                                        'options' => $iclasses,
                                        'value' => $ivalue,
                                        'class' => 'form-control col-md-7 col-xs-12',
                                        'required' => 'required',
                                        'id' => 'iclass'
                                    )
                                );  
                            ?><i id="iclassid" class="<?php echo $idisplay; ?>"></i>
                            </span>
                        </div>
                        <div class="col-md-3 col-sm-7 col-xs-12">
                            <label for="itimestwo" class="pointer css-input css-checkbox css-checkbox-default push-15-t">
                                <input type="checkbox" id="itimestwo" name="category[TimesTwo]" class="pointer " checked="checked" value="fa-2x">
                                <span></span> 2X
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <textarea class="form-control dowysiwyg" name="category[SubCatDescription]"><?php echo isset( $catdata[0]->SubCatDescription ) ? $catdata[0]->SubCatDescription : ''; ?></textarea>
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('forums/subcategories'); ?>" class="btn btn-info btn-rounded"><i class="si si-action-undo"></i> Back</a>
                            <button id="send" type="submit" class="btn btn-rounded btn-primary">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function( event ){        
        $( '#iclass' ).change( function(){
            var optionSelected = $( this ).find( "option:selected" );
            var valueSelected = optionSelected.val();
            var textSelected = optionSelected.text();
            $( '#iclassid' ).removeClass();
            $( '#iclassid' ).addClass( valueSelected+' fa-2x' );
        });
    });
</script>