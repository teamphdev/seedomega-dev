<?php 
View::$title = 'Add Category';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  ?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
        <div class="col-lg-6">
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?>   
                <form class="form-horizontal form-ui form-label-left input_mask" method="post">
                    <input type="hidden" name="action" value="addclientcategory" />
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Category Name <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" value="" name="category[CCatName]" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>                    
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <textarea class="form-control dowysiwyg" name="category[CCatDescription]"></textarea>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('clients/categories'); ?>" class="btn btn-rounded btn-info"><i class="si si-action-undo"></i> Back</a>
                            <button id="send" type="submit" class="btn btn-rounded btn-primary">Add Category</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>