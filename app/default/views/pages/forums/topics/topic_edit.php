<?php 
  View::$title = 'Edit Topic';
  View::$bodyclass = User::info('Sidebar').' forum-new-topic';
  View::header();
?>

    <section class="header-bottom">
        <article>
            <div class="container animated fadeInDown">
                <h1><i class="fa fa-edit"></i> <?php echo View::$title; ?></h1>
            </div>
        </article>
    </section>

    <?php /*
    <section class="breadcrumb">
      <article class="container">
        <div class="row">
          <div class="col-lg-6">
            <ul>
              <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                <li><a class="link-effect" href="<?php echo View::url(); ?>">Home</a></li>
                <li class="fa fa-angle-right"></li>
                <li><a class="link-effect" href="<?php echo View::url( 'forums'); ?>">Forums</a></li>
                <li class="fa fa-angle-right"></li>
                <li><a class="link-effect" href="<?php echo View::url( 'forums/dashboard'); ?>">Dashboard</a></li>
                <li class="fa fa-angle-right"></li>
                <li><a class="link-effect" href="<?php echo View::url( 'forums/topics'); ?>">Topics</a></li>
                <li class="fa fa-angle-right"></li>
                <li class="text-muted">Edit Topic</li>
            </ul>
          </div>
        </div>
      </article>
    </section>
    */ ?>

  <!-- Page Content -->
  <section class="gray">
    <article class="container" id="intor-section">
        <div class="block" style="min-height: 446px;">
            
            <div class="block-header"><h4 class="block-title text-left"><?php echo isset( $topic->Title ) ? $topic->Title : ''; ?></h4></div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <input name="image" type="file" id="upload" class="hidden" onchange="">
                
                <form class="form-horizontal form-label-left form-ui input_mask text-left" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="fulledit">
                    <input type="hidden" name="topic[ForumID]" value="<?php echo isset( $topic->ForumID ) ? $topic->ForumID : ''; ?>">
                    <div class="form-group">
                        <div class="form-left">
                            <label for="topic-forum">Forum Category <span class="text-danger">*</span></label>
                            <select class="js-select2 form-control select2-hidden-accessible-show" id="topic[IDs]" name="topic[IDs]" style="width: 100%;" data-placeholder="Please select a category" tabindex="-1" aria-hidden="true">
                                <option></option>
                                <?php if( isset( $options ) && count( $options ) ){
                                foreach( $options as $k => $v){
                                    $value = isset( $topic->CatID ) ? $topic->CatID : '';
                                    $value .= isset( $topic->SubCatID ) ? '-'.$topic->SubCatID : ''; ?>
                                    <?php if( strpos( $k, 'disabled' ) !== false ){ ?>
                                            <option value="" disabled=""><?php echo isset( $v ) ? $v : ''; ?></option>
                                    <?php } else { ?>                                                    
                                            <option value="<?php echo isset( $k ) ? $k : ''; ?>" <?php echo $k == $value ? 'selected' : ''; ?>><?php echo isset( $v ) ? $v : ''; ?></option>
                                    <?php } ?>
                                <?php } } ?>
                            </select>
                            
                        </div>
                        <div class="form-right">
                            <label>Active</label>
                            <select name="Status" class="form-control">
                                <option value="1" <?php echo $topic->Status == 1 ? 'selected' : ''; ?>>Yes</option> 
                                <option value="0" <?php echo $topic->Status == 0 ? 'selected' : ''; ?>>No</option> 
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="topic-title">Topic Title <span class="text-danger">*</span></label>
                        <input class="form-control" type="text" id="topic-title" name="topic[Title]" placeholder="Please enter a title" value="<?php echo isset( $topic->Title ) ? $topic->Title : ''; ?>">
                    </div>

                    <div class="form-group">
                        <label for="topic-description">Topic Description</label>
                        <input class="form-control" type="text" id="topic-description" name="topic[Description]" placeholder="Please enter a description" value="<?php echo isset( $topic->Description ) ? $topic->Description : ''; ?>">
                    </div>

                    <div class="form-grou hidden">
                        <label for="topic-description"><?php echo Lang::get('FORUM_FRM_WTP'); ?></label>
                        <input class="form-control" type="text" id="topic-epigram" name="topic[Epigram]" placeholder="It's nice to have any words to ponder" value="<?php echo isset( $topic->Epigram ) ? $topic->Epigram : ''; ?>">
                    </div>

                    <div class="form-group">
                        <textarea class="form-control tinyMCE" name="topic[Content]" cols="30" rows="10"><?php echo isset( $topic->Content ) ? $topic->Content : ''; ?></textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-rounded btn-primary" type="submit"><i class="fa fa-save"></i> Save Changes</button>
                            <button class="btn btn-rounded btn-default hidden" type="button"><i class="fa fa-eye"></i> Preview</button>
                            <button class="btn btn-rounded btn-default hidden" type="button"><i class="fa fa-floppy-o"></i> Draft</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </article>
  </section>
  
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        //$(".js-select2").select2();
    });
</script>