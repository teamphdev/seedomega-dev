<?php 
View::$title = 'Topics';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( View::$segments[0] ); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
        <div class="col-lg-6">
            
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?>
                <table class="table table-divide table-header-bg table-vcenter js-dataTable-full-pagination" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url( 'forums/newtopic' ); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Add Topic</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th class="text-center no-wrap">Date Posted</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Author</th>
                            <th class="text-center no-wrap">Sub-Category</th>
                            <th class="text-center">Content</th>
                            <th class="no-sorting text-center" style="width:15%;min-width:110px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if( isset( $topics ) && count( $topics ) ){
                            foreach( $topics as $topic ){ $cntr++;
                                $name = isset( $topic->FirstName ) ? ucwords( $topic->FirstName ).' ' : '';
                                $name .= isset( $topic->LastName ) ? ucwords( $topic->LastName ) : ''; ?>
                            <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> category-id-<?php echo $topic->ForumID; ?>">                            
                                <td class="text-center">
                                    <?php echo isset( $topic->DatePosted ) && $topic->DatePosted != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $topic->DatePosted ) ) : '-'; ?>
                                </td>
                                <td><?php echo isset( $topic->Title ) ? ucwords( $topic->Title ) : '-'; ?></td>
                                <td><?php echo $name; ?></td>
                                <td><?php echo isset( $topic->SubCatName ) ? $topic->SubCatName : '-'; ?></td>
                                <td data-toggle="modal" data-target="#modal-topic-content" onclick="viewContent( '<?php echo $topic->ForumID; ?>' );">                                    
                                    <?php $excerpt = AppUtility::excerptAsNeeded( $topic->Content, 300, '... <span class="text-info">(click to view more)</span>' ); ?>
                                    <?php echo $excerpt; ?>
                                    <?php //echo isset( $topic->Epigram ) ? '<hr><small class="text-muted"><i>'.$topic->Epigram.'</i></small>' : ''; ?>                                    
                                </td>
                                <td class="text-center">
                                    <?php /*
                                    <div class="">
                                        <a href="<?php echo View::url( 'forums/topics/edit/'.$topic->CatID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit Topic" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                    </div>
                                    */ ?>

                                    <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Topics' ) ){ ?>
                                    <div class="">
                                        <div class="dropdown more-opt">
                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?php echo View::url( 'forums/topics/edit/'.$topic->ForumID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                <li><a href="<?php echo View::url( 'forums/topics/delete/'.$topic->ForumID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this topic?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                        <div class="">
                                            <a href="<?php echo View::url( 'forums/topics/edit/'.$topic->CatID ); ?>" class="btn btn-sm btn-default btn-rounded" title="" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } } else { ?>
                            <tr>
                                <td colspan="6">No Topics Found!</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>

                <!-- Content Modal -->
                <div class="modal fade" id="modal-topic-content" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="modal-title"></span></h3>
                          </div>
                          <div class="block-content">
                            <div id="modal-content" class="hiddenMCE"></div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END Cotent Modal -->
            </div>
        </div>

    </div>
</section>
<input type="hidden" id="jcontent" value="<?php echo $jdata; ?>">

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    var content = [];
    $( document ).ready( function(){
        $content = $( '#jcontent' ).val();
        content = jQuery.parseJSON( $content );

        // initialize tinyMCE on contents having .hiddenMCE class
        hiddenMCEconfig.inline = true;
        hiddenMCEconfig.readonly = 1;
        tinymce.init( hiddenMCEconfig ); // hiddenmce.js
    });

    function viewContent( fid ){
        $( '#modal-title' ).text( content[fid]['Title'] );
        $( '#modal-content' ).text( content[fid]['Content'] );
        tinymce.get( 'modal-content' ).setContent( content[fid]['Content'] );
    }
</script>