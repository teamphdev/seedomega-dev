<?php 
View::$title = 'Company Forums';
View::$bodyclass = 'loginpage';
View::header(); 
?>

<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1>Our Community!</h1>
            <p>The new way to invest in innovative business. We want to ensure our clients have the best opportunity for success, so we offer support before, during and after the offer for companies and investors.</p>
        </div>
    </article>
  </section>
<style type="text/css">
  .project-desc { height: auto; }
  .popular-details ul { padding-left: 80px; }
  .popular-data img { margin-top: 4px; }
</style>
<!-- ************************ Page Content ************************ -->
<section class="gray">
  <!-- Page Content -->
  <div class="container blog-page">

      <?php /* ?>
      <!-- Dynamic Table Full Pagination -->
      <ul id="bloglist" class="blog-list row">

      <?php if( isset( $clients ) && count( $clients ) ){ foreach( $clients as $client ){ ?>

      <li class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="blog-item">
            <figure class="blog-image">
                <figcaption><a href="/forums/dashboard/<?php echo $client->ClientProfileID; ?>">View Forum</a></figcaption>
                <?php echo $client->Data['CompanyPhotoLink']; ?>
            </figure>
            <div class="popular-content">
                <div class="blog-desc">
                    <h5><a href="/forums/dashboard/<?php echo $client->ClientProfileID; ?>"><?php echo isset( $client->CompanyName ) ? $client->CompanyName : ''; ?><span class="line-green"></span></a></h5>
                </div>
            </div>
            <div class="blog-byDate">
                <div class="pmeta">
                    <div class="met"><strong><?php echo isset( $client->Topics ) ? number_format( $client->Topics ) : '0'; ?></strong> Topics</div>
                    <div class="met"><strong><?php echo isset( $client->Replies ) ? number_format( $client->Replies ) : '0'; ?></strong> Replies</div>
                    <div class="met"><strong><?php echo isset( $client->Views ) ? number_format( $client->Views ) : '0'; ?></strong> Views</div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
      </li>
      <?php } } ?>        
    </ul>
    <?php */ ?>
    <table class="table js-table-checkable focusRow js-dataTable-full-pagination dt-responsive table-header-bg table-vcenter" cellspacing="0" width="100%">
      <thead>
          <tr class="headings">
              <th class="text-center">Photo</th>
              <th>Main Forums</th>
              <th class="text-center">Topics</th>
              <th class="text-center">Replies</th>
              <th class="text-center">Views</th>
              <th class="text-center no-wrap">Last Post</th>
          </tr>
      </thead>
      <tbody>
          <?php 
              $cntr = 0;
              if(isset( $clients ) && count( $clients )) {
              foreach($clients as $client) { $cntr++;
              ?>
              <tr class="" data-link="<?php echo View::url('forums/dashboard/'.$client->ClientProfileID); ?>">
                  <td><?php echo $client->Data['CompanyPhotoLink']; ?></td>
                  <td>
                    <div class="font-15"><?php echo isset( $client->CompanyName ) ? $client->CompanyName : ''; ?></div>
                    <div class="font-13 text-muted"><?php echo AppUtility::excerptAsNeeded( $client->OfferOverview, 200, '...' ); ?></div>
                  </td>
                  <td class="font-16 text-center"><?php echo isset( $client->Topics ) ? number_format( $client->Topics ) : '0'; ?></td>
                  <td class="font-16 text-center"><?php echo isset( $client->Replies ) ? number_format( $client->Replies ) : '0'; ?></td>
                  <td class="font-16 text-center"><?php echo isset( $client->Views ) ? number_format( $client->Views ) : '0'; ?></td>
                  <td class="font-16 text-center no-wrap">
                    <span class="font-s13">
                      <?php 
                      echo isset( $client->Data['PostedBy'] ) ? 'by '.$client->Data['PostedBy'] : '-';
                      echo isset( $client->Data['DatePosted'] ) ? '<br>'.date('F j, Y', strtotime( $client->Data['DatePosted'] ) ) : ''; 
                      ?>
                    </span>
                  </td>
              </tr>
          <?php } 
              } else {?>
          <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
              <td>No Data</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <?php } ?>
      </tbody>
  </table>  
  </div>
</section>

<?php View::footer(); ?>