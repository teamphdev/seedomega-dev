<?php 
  View::$title = $title.' Forums';
  View::$bodyclass = User::info('Sidebar').' forum-topic';
  View::header();
  // $subslug = str_ireplace( ' ', '-', strtolower( $forum['scname'] ) );
  $forum['scslug'] = str_ireplace( '?', '', $forum['scslug'] );
?>
<?php /*
  <section class="breadcrumb">
      <article class="container">
        <div class="row">
          <div class="col-lg-12">

            <ul>
              <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
              <li><a class="link-effect" href="<?php echo View::url(); ?>">Home</a></li>
              <li class="fa fa-angle-right"></li>
              <li><a class="link-effect" href="<?php echo View::url( 'forums'); ?>">Forums</a></li>
              <li class="fa fa-angle-right"></li>
              <li><a class="link-effect" href="<?php echo View::url( 'forums/dashboard' ); ?>">Dashboard</a></li>
              <li class="fa fa-angle-right"></li>
              <li><a class="link-effect" href="<?php echo View::url( 'forums/category/'.$forum['subcatid'].'/'.$forum['scslug'] ); ?>"><?php echo $forum['scname']; ?></a></li>
              <li class="fa fa-angle-right"></li>
              <li class="text-muted"><?php echo $forum['title']; ?></li>
            </ul>

          </div>
        </div>
      </article>
  </section>
  */ ?>

  <!-- Page Content -->
  <section class="white">
    <article class="container" id="intor-section">

      <div class="block" style="min-height: 446px;">
        <?php echo View::getMessage(); ?>

        <div class="block-content">
          <input name="image" type="file" id="upload" class="hidden" onchange="">
          <table id="tabletopic" class="table table-borderless table-vcenter table-header-bg" width="100%">
            
            <thead>
              <tr>
                  <th class="text-left">
                    <h2 class="block-title"> <i class="fa fa-hand-o-right"></i>  <?php echo isset( $topics[0]->Title ) ? $topics[0]->Title : ''; ?></h2>
                  </th>
              </tr>
            </thead>

            <tbody>

              <?php 
              if( isset( $topics ) && count( $topics ) ){
                $ctr = 0;
                foreach( $topics as $topic ){ $ctr++; ?>
                  <tr>
                    <td class="no-padding">
                      
                      <div class="clear"></div>
                      <div class="block push-5-t">
                        <div class="wlessp form-left text-center inline-block">
                          <div class="push-10 push-5-t">
                            <a href="javascript:void(0)"><?php echo $topic->Data['AvatarLink']; ?></a>
                          </div>
                          <small><?php echo $topic->TotalPost; ?></small>
                        </div>                        
                        <div class="wmorep form-right">
                          <div class="font-s13 text-left pad-10 pad-10-t link">
                            <?php echo $topic->Data['Name']; ?> <i class="text-muted">on <?php echo isset( $topic->DatePosted ) ? date('F j, Y - H:i:s', strtotime( $topic->DatePosted ) ) : ''; ?></i>
                          </div>
                          <div class="text-left">
                            <?php echo isset( $topic->Content ) ? $topic->Content : ''; ?>

                            <div id="form-edit-wrapper" class="hidden">
                              <form id="formForumEdit" class="form-horizontal input_mask" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="action" value="edittopic">
                                <input type="hidden" name="topic[UserID]" value="<?php echo isset( $userinfo->UserID ) ? $userinfo->UserID : '0'; ?>">
                                <input type="hidden" name="topic[ForumID]" value="<?php echo isset( $topics[0]->ForumID ) ? $topics[0]->ForumID : '0'; ?>">
                                <textarea id="textContentEdit" class="form-control full tinyMCE" name="topic[Content]" cols="30" rows="20" style="width:100%;">
                                  <?php echo isset( $topic->Content ) ? $topic->Content : ''; ?>
                                </textarea>
                                <button class="btn btn-info btn-sm push-5-t" type="submit"><i class="fa fa-save"></i> SUBMIT</button>&nbsp;
                                <a href="javascript:void(0);" class="btn btn-default btn-sm push-5-t" onclick="cancelMyEditedTopic();"><i class="fa fa-close"></i> CANCEL</a>
                              </form>
                            </div>

                            <?php if( $ctr == 1 && ($topic->UserID == $userinfo->UserID || $topic->UserID == '0') ){ ?>
                              <div id="btn-edit-wrapper">
                                <a href="javascript:void(0);" class="btn btn-primary btn-rounded btn-sm" onclick="editMyTopic();"><i class="fa fa-edit "></i> EDIT</a>
                              </div>
                            <?php } ?>
                          </div>
                          <br>
                          <hr>
                        </div>
                      </div>

                    </td>
                  </tr>
              <?php } } else { ?>
                <tr>
                  <td><div class="h5 hidden-xs text-left">No data found!</div></td>
                </tr>
              <?php } ?>

            </tbody>

            <tfoot>

              <tr id="forum-reply-form">
                <td class="no-padding">
                  <?php /* ?>
                  <div class="font-s13 text-left">
                    <?php echo $username; ?> <i class="text-muted">just now</i>
                  </div>
                  <div class="clear"></div>
                  */?>

                  <div class="block push-5-t">
                    <div class="wlessp form-left text-center inline-block">
                      <div class="push-10 pad-20-t">
                        <a href="javascript:void(0)"><?php echo $avatarlink; ?></a>
                      </div>
                      <small><?php echo $total; ?></small>
                    </div>                        
                    <div class="wmorep form-right">
                      <div class="text-left">

                        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" id="forum-reply-form">
                          <input type="hidden" name="action" value="reply">
                          <input type="hidden" name="topic[UserID]" value="<?php echo isset( $userinfo->UserID ) ? $userinfo->UserID : '0'; ?>">
                          <input type="hidden" name="topic[ForumID]" value="<?php echo isset( $topics[0]->ForumID ) ? $topics[0]->ForumID : '0'; ?>" id="ForumID">

                          <div class="text-left">
                            <div class="hidden">
                              <label for="topic-description"><?php echo Lang::get('FORUM_FRM_WTP'); ?>:</label>
                              <input class="form-control" type="text" name="topic[Epigram]" value="<?php echo $epigram; ?>" placeholder="It's nice to have any words to ponder" style="width:100%">
                            </div><br>
                            <div class="">
                              <textarea class="form-control tinyMCE" name="topic[Content]" id="textContent" cols="30" rows="10"></textarea>
                            </div><br>
                            <div class="">
                              <button class="btn btn-primary btn-rounded" type="submit" id="submitReply"><i class="fa fa-reply"></i> Reply</button>
                            </div>
                          </div>
                        </form>

                      </div>
                    </div>
                  </div>
                </td>
              </tr>

            </tfoot>

          </table>

        </div>

      </div>

    </article>
  </section>
  
<?php View::footer(); ?>

<script type="text/javascript">
  setTimeout( function(){ savethisview(); }, 5000 );

  function savethisview(){
    var fid = $( '#ForumID' ).val();
    var $url = "<?php echo View::url( 'forums/saveTopicView' ); ?>";
    $.ajax({
        type: "POST",
        url: $url,
        data: { forumid : fid },
        dataType: 'json'
    });
  }

  function editMyTopic(){
    $( '#btn-edit-wrapper' ).addClass( 'hidden' );
    $( '#form-edit-wrapper' ).removeClass( 'hidden' );
  }

  function cancelMyEditedTopic(){
    $( '#btn-edit-wrapper' ).removeClass( 'hidden' );
    $( '#form-edit-wrapper' ).addClass( 'hidden' );
  }

  // $( "#gotoreps" ).click( function(){
  //   $( "html, body" ).animate({ scrollTop: $(document).height() }, "slow" );
  //   return false;
  // });​

  // var content = tinymce.get('tinymceEditor').getContent({format: 'text'});
  // if( $.trim( content ) == '' ){
  //   // alert('0');
  // }

  $( document ).ready( function(){
    var mytable = $( '#tabletopic' ).DataTable({
      "pageLength": 10,
      "ordering": false,
      "searching": false,
      "bLengthChange": false,
      "pagingType": "full_numbers"
    });
  });
</script>