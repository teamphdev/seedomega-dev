<?php 
  View::$title = $title.' Forums';
  View::$bodyclass = User::info('Sidebar').' forum-category';
  View::header();
?>

<?php /*
  <section class="breadcrumb">
      <article class="container">
        <div class="row">
          <div class="col-lg-6">

            <ul>
              <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
              <li><a class="link-effect" href="<?php echo View::url(); ?>">Home</a></li>
              <li class="fa fa-angle-right"></li>
              <li><a class="link-effect" href="<?php echo View::url( 'forums'); ?>">Forums</a></li>
              <li class="fa fa-angle-right"></li>
              <li><a class="link-effect" href="<?php echo View::url( 'forums/dashboard'); ?>">Dashboard</a></li>
              <li class="fa fa-angle-right"></li>
              <li class="text-muted"><?php echo $forum['scname']; ?></li>
            </ul>

          </div>
          <div class="col-lg-6">
            
          </div>
        </div>
      </article>
  </section>
*/ ?>

  <!-- Page Content -->
  <section class="white">
    <article class="container" id="intor-section">
      <div class="block" style="min-height: 446px;">
        <div class="row animated fadeInDown">
          <div class="col-md-6 col-sm-6 col-xs-6"></div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="pull-right">
              <a class="btn btn-rounded btn-primary" href="<?php echo view::url( 'forums/newtopic' ); ?>"><i class="fa fa-plus"></i> New Topic</a>
            </div>
          </div>
      </div>
        
        <div class="">

          <table id="tablecategory" class="table table-bordered table-vcenter table-header-bg">

            <thead>
              <tr>
                <th class="text-left"><?php echo $forum['scname']; ?></th>
                <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Replies</th>
                <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Views</th>
                <th class="text-center hidden-xs hidden-sm" style="width: 200px;">Last Post</th>
              </tr>
            </thead>

            <tbody>

            <?php
            if( isset( $categories ) && count( $categories ) ){
              if( isset( $categories[1] ) ){
                foreach( $categories[1] as $cat ):
                  $subslug = str_ireplace( '?', '', $cat->SubCatNameSlug );
                  $titleslug = str_ireplace( '?', '', $cat->TitleSlug ); ?>
                  <tr>
                    <td class="text-left no-wrap">
                      <div class="h5 font-w600 push-5 inline-block text-top push-5-t">
                        <?php if( User::can( 'Administer Forums' ) ){ ?>
                        <a href="<?php echo View::url( "forums/category/$cat->SubCatID/$cat->ForumID/0" ); ?>" title="Unpin this"><i class="si si-pin fa-2x text-gray"></i></a>
                        <?php } else { ?>
                        <i class="si si-pin fa-2x"></i>
                        <?php } ?>
                      </div>&nbsp;
                      <div class="inline-block">
                        <a href="<?php echo View::url( "forums/topic/$cat->SubCatID/$cat->ForumID/$titleslug" ); ?>"><?php echo isset( $cat->Title ) ? $cat->Title : ''; ?></a>
                        <div class="font-s13">
                          <?php echo isset( $cat->Author ) ? $cat->Author : ''; ?> <i class="text-muted">on <?php echo isset( $cat->DatePosted ) ? date('F j, Y', strtotime( $cat->DatePosted ) ) : ''; ?></i>
                        </div>
                      </div>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                      <a class="font-w600" href="javascript:void(0)"><?php echo isset( $cat->Replies ) ? $cat->Replies : ''; ?></a>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                      <a class="font-w600" href="javascript:void(0)"><?php echo isset( $cat->Views ) ? $cat->Views : '0'; ?></a>
                    </td>
                    <td class="hidden-xs hidden-sm text-left">
                      <span class="font-s13">
                        <?php echo isset( $cat->LastPoster ) ? '<i class="text-muted">by</i> '.$cat->LastPoster : '-';
                        echo isset( $cat->LastPosterDate ) ? '<br>'.date('F j, Y', strtotime( $cat->LastPosterDate ) ) : ''; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
            <?php } ?>
              
            <?php              
              if( isset( $categories[0] ) ){
                foreach( $categories[0] as $cat ):
                  $subslug = str_ireplace( '?', '', $cat->SubCatNameSlug );
                  $titleslug = str_ireplace( '?', '', $cat->TitleSlug ); ?>
                  <tr>
                    <td class="text-left">
                      <?php if( User::can( 'Administer Forums' ) && count( $categories[0] ) > 10 ){
                        if( !isset( $categories[1] ) || count( $categories[1] ) < 3 ){ ?>
                          <a href="<?php echo View::url( "forums/category/$cat->SubCatID/$cat->ForumID/1" ); ?>" title="Pin this to top"><i class="si si-pin text-gray"></i>&nbsp;</a>
                      <?php } } ?>
                        <a href="<?php echo View::url( "forums/topic/$cat->SubCatID/$cat->ForumID/$titleslug" ); ?>"><?php echo isset( $cat->Title ) ? $cat->Title : ''; ?></a>
                        <div class="font-s13">
                          <?php echo isset( $cat->Author ) ? $cat->Author : ''; ?> <i class="text-muted">on <?php echo isset( $cat->DatePosted ) ? date('F j, Y', strtotime( $cat->DatePosted ) ) : ''; ?></i>
                        </div>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                      <a class="font-w600" href="javascript:void(0)"><?php echo isset( $cat->Replies ) ? $cat->Replies : '0'; ?></a>
                    </td>
                    <td class="text-center hidden-xs hidden-sm">
                      <a class="font-w600" href="javascript:void(0)"><?php echo isset( $cat->Views ) ? $cat->Views : '0'; ?></a>
                    </td>
                    <td class="hidden-xs hidden-sm text-left">
                      <span class="font-s13">
                        <?php echo isset( $cat->LastPoster ) ? '<i class="text-muted">by</i> '.$cat->LastPoster : '-';
                        echo isset( $cat->LastPosterDate ) ? '<br>'.date('F j, Y', strtotime( $cat->LastPosterDate ) ) : ''; ?>
                      </span>
                    </td>
                  </tr>
                <?php endforeach; ?>
            <?php } } else { ?>
                <tr>
                  <td colspan="4" class="text-left">Be the first one to reply</td>
                  <td class="hidden"></td>
                  <td class="hidden"></td>
                  <td class="hidden"></td>
                </tr>
            <?php } ?>

            </tbody>
          </table>
        </div>
      </div>
    </article>
  </section>
  
<?php View::footer(); ?>

<script type="text/javascript">
  $( document ).ready( function(){
    var mytable = $( '#tablecategory' ).DataTable({
      "pageLength": 10,
      "ordering": false,
      "searching": false,
      "bLengthChange": false,
      "pagingType": "full_numbers"
    });
  });
</script>