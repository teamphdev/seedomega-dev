 <?php 
  View::$title = isset( $clientdata->CompanyName ) ? $clientdata->CompanyName : "No Title";
  View::$bodyclass = 'project-view';
  View::header();
?> 

<section class="project-header project-banner" <?php echo $banner; ?>>
  <div class="container">
    <div class="row">

      <div class="col-lg-8">
        <h2 class="proj-title animated fadeInDown"><?php echo isset( $clientdata->CompanyName ) ? ucwords( $clientdata->CompanyName ) : "-"; ?></h2>
        <div class="animated fadeInDown"><?php echo $description; ?></div>
        <p class="author push-30-t animated fadeInDown">Lead Manager : <?php echo isset( $clientdata->LeadManager ) ? ucwords( $clientdata->LeadManager ) : "-"; ?>
          <?php echo '<a href="'.$SocialMedia->Website.'" class="push-20-l"><i class="fa fa-globe"></i></a>'; ?>
          <?php echo '<a href="'.$SocialMedia->Facebook.'" class="push-20-l"><i class="fa fa-facebook"></i></a>'; ?>
          <?php echo '<a href="'.$SocialMedia->Google.'" class="push-20-l"><i class="fa fa-google-plus"></i></a>'; ?>
          <?php echo '<a href="'.$SocialMedia->Instagram.'" class="push-20-l"><i class="fa fa-instagram"></i></a>'; ?>
        </p>
        <div class="ribbontag ribbon ribbon-left">
          <div class="ribbon-box pink">Limited</div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="push-20-t push-20">
            <div class="project-data row">
                <div class="col-lg-6 col-xs-6 animated fadeInDown">
                    <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#f75570" data-barsize="5" data-size="148">
                      <?php echo View::photo( $logo, "Avatar", "company-logo" ); ?>
                    </div>                                
                </div>
                <div class="col-lg-6 col-xs-6 animated fadeInDown">
                    <div class="progress_number"><?php echo $percentage; ?>%</div>
                    <div class="progress_label">Completed</div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
      </div>

    </div>

    <div class="about-project bg-white client-vid">
      <?php echo View::getMessage(); ?>

      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-8 no-padding animated fadeIn">
        <div class="embed-responsive embed-responsive-16by9">
          <?php echo $clientdata->Video; ?>
        </div>          
      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4 animated fadeIn">
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="project-progress">

              <div class="offer-details" style="">
                
                <div class="offer-info">
                  <div class="row">
                    <div class="col-xs-12 offer-data">
                        <h2 class="font-32 text-success"><?php echo isset( $clientdata->TotalRaisedAmt ) ? "$".number_format( $clientdata->TotalRaisedAmt, 2 ) : "0.00"; ?><br>
                        <small class="text-muted">Raised</small></h2>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 offer-size offer-data">
                      $<?php echo isset( $clientdata->TargetGoal ) ? number_format( $clientdata->TargetGoal, 2 ) : "-"; ?><small>Target Goal</small>
                    </div>
                    <div class="col-md-12 offer-close offer-data"><?php echo isset( $clientdata->DaysLeft ) ? number_format( $clientdata->DaysLeft ) : 0; ?><small>Days Left</small></div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12 offer-size offer-data">
                      $<?php echo isset( $clientdata->Price ) ? number_format( $clientdata->Price) : "0.00"; ?><small>Price</small>
                    </div>
                    <div class="col-md-12 offer-size offer-data">
                      $<?php echo isset( $clientdata->MinimumBid ) ? number_format( $clientdata->MinimumBid, 2 ) : "-"; ?><small>Minimum Investment</small>
                    </div>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>

          <?php echo $htmlButton; ?>

          <!-- Approve Client-->
          <?php if( User::can( 'Manage Profiles' ) ){ ?>
            <div class="w-title push-0 text-uppercase">Status: <b><?php echo $clientdata->Status; ?></b></div>
            <div class="text-center" style="padding:5px 0 15px;">
                <?php if( $clientdata->Status == 'Pending' ){ ?>
                  Pending project is not visible to the seeders.
                <?php } ?>

                <form class="form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                  <input type="hidden" name="action" value="<?php echo $clientdata->Status == 'Pending' ? 'approveclient' : 'updateclient'; ?>">
                  <input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">

                  <table>
                    <tr>
                      <td><span class="input-group-addon form-control"><?php echo Lang::get('CLN_CS_RATE'); ?> (%)</span></td>    
                      <td><input type="text" value="<?php echo isset( $clientdata->CommRate ) && $clientdata->CommRate > 0 ? number_format( $clientdata->CommRate ) : '25'; ?>" class="form-control csvalue" pattern="^[0-9]\d*(\.\d+)?$" name="CommissionRate" placeholder="25" step="1"></td>
                    </tr>
                    <tr>
                      <td><span class="input-group-addon form-control"><?php echo Lang::get('CLN_SS_RATE'); ?> (%)</span></td>
                      <td><input type="text" value="<?php echo isset( $clientdata->SubsRate ) ? number_format( $clientdata->SubsRate, 2 ) : '0.5'; ?>" class="form-control csvalue" pattern="^[0-9]\d*(\.\d+)?$" name="SubscriptionRate" placeholder="0.00" step=".01"></td>
                    </tr>
                  </table>

                  <small class="text-muted">must be positive(+) value</small>

                  <div class="clear"></div>

                  <input type="submit" class="btn btn-rounded btn-primary" style="margin:10px 0 5px;" value="<?php echo $clientdata->Status == 'Pending' ? 'Approve' : 'Update'; ?> this Client">
                </form>
            </div>
          <?php } ?>
          <!-- END Approve Client-->
          
        </div>
      </div>
      <div class="clear"></div>
    </div>

  </div>
</section>

<section class="bg-white no-padding">
  <article class="container view-company-info animated fadeIn">  

    <div class="row about-project">
      <?php echo View::getMessage(); ?>

      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-12 client-content">        
          <div class="block">

            <ul class="nav nav-tabs main-nav" data-toggle="tabs">
                <li class="<?php echo $tabid == 'client-info' ? 'active' : ''; ?>">
                    <a href="#client-info">Info</a>
                </li>
                <?php if( $enabledTabs ){ ?>
                  <li class="<?php echo $tabid == 'client-documents' ? 'active' : ''; ?>">
                      <a href="#client-documents">Documents</a>
                  </li>
                  <li class="<?php echo $previewonly; ?> <?php echo $tabid == 'client-faq' ? 'active' : ''; ?>">
                      <a href="#client-faq">FAQ</a>
                  </li>
                  <li class="<?php echo $previewonly; ?> <?php echo $tabid == 'client-blogs' ? 'active' : ''; ?>">
                      <a href="#client-blogs">Blogs</a>
                  </li>
                  <li class="hidden <?php echo $tabid == 'client-comments' ? 'active' : ''; ?>" id="client-comments-tab">
                      <a href="#client-comments">Comments</a>
                  </li>
                  <li class="<?php echo $previewonly; ?> <?php echo $tabid == 'client-community' ? 'active' : ''; ?>">
                      <a href="#client-community">Community</a>
                  </li>
                <?php } ?>
            </ul>
            
            <div class="tab-content bg-white">

              <div class="tab-pane body-longtext fade fade-up <?php echo $tabid == 'client-info' ? 'in active' : ''; ?>" id="client-info">

                <?php if(strlen( $clientdata->OfferOverview )) { ?>
                <h4 class="bl-title">Offer Overview</h4>
                <div class=""><?php echo isset( $clientdata->OfferOverview ) ? $clientdata->OfferOverview : ""; ?></div>
                <?php } ?>

                <?php if(strlen( $clientdata->BusinessModel )) { ?>
                <h4 class="bl-title">Business Model</h4>
                <div class="bodyp"><?php echo isset( $clientdata->BusinessModel ) ? $clientdata->BusinessModel : ""; ?></div>
                <?php } ?>

                <?php if(strlen( $clientdata->ExecutiveSummary )) { ?>
                <h4 class="bl-title">Executive Summary</h4>
                <div class="bodyp"><?php echo isset($clientdata->ExecutiveSummary) ? $clientdata->ExecutiveSummary : ""; ?></div>
                <?php } ?>

                <?php if( $enabledTabs ){ ?>
                  <?php if(strlen( $clientdata->KeyInvestHighlights )) { ?>
                  <h4 class="bl-title">Key Invest Highlights</h4>
                  <div class="bodyp"><?php echo isset($clientdata->KeyInvestHighlights) ? $clientdata->KeyInvestHighlights : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->StrategyVision )) { ?>
                  <h4 class="bl-title">Strategy Vision</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->StrategyVision ) ? $clientdata->StrategyVision : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->MarketDemand )) { ?>
                  <h4 class="bl-title">Market Demand</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->MarketDemand ) ? $clientdata->MarketDemand : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->BoardManagement )) { ?>
                  <h4 class="bl-title">Board Management</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->BoardManagement ) ? $clientdata->BoardManagement : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->UsageOfFunds )) { ?>
                  <h4 class="bl-title">Usage Of Funds</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->UsageOfFunds ) ? $clientdata->UsageOfFunds : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->FinancialSummary )) { ?>
                  <h4 class="bl-title">Financial Summary</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->FinancialSummary ) ? $clientdata->FinancialSummary : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->PressCoverage )) { ?>
                  <h4 class="bl-title">Press Coverage</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->PressCoverage ) ? $clientdata->PressCoverage : ""; ?></div>
                  <?php } ?>

                  <?php if(strlen( $clientdata->Disclosure )) { ?>
                  <h4 class="bl-title">Disclosure</h4>
                  <div class="bodyp"><?php echo isset( $clientdata->Disclosure ) ? $clientdata->Disclosure : ""; ?></div>
                  <?php } ?>
                <?php } ?>               
              </div>

              <div class="tab-pane fade fade-up <?php echo $tabid == 'client-documents' ? 'in active' : ''; ?>" id="client-documents">
                <!-- <h4>Documents</h4> -->

                <div class="block <?php echo $hidden; ?>" style="min-height: 446px;">
                  <div class="block-content block-content-full" id="forums-content">
                    <?php if( isset( $documents ) && count( $documents ) ){ ?>
                      <?php /* <ul>
                        <?php foreach( $documents as $doc ){ ?>                     
                            <li><a href="<?php echo isset( $doc->Link ) ? $doc->Link : 'javascript:void(0);'; ?>" target="_blank"><i class="fa fa-download"></i> <?php echo isset( $doc->DocumentName ) ? $doc->DocumentName : ''; ?></a></li>
                        <?php } ?>
                      </ul> */ ?>
                      <div class="listdownload brochure-list row">
                        <?php 
                        foreach( $documents as $doc ){
                          $ext = pathinfo( $doc->FileSlug, PATHINFO_EXTENSION );
                          switch( $ext ){
                              case 'pdf':
                                  $fileSlugUrl = View::asset("images/pdf3.png");
                                  break;
                              case 'docx':
                                  $fileSlugUrl = view::asset("images/word.jpg");
                                  break;
                              case 'doc':
                                  $fileSlugUrl = view::asset("images/word.jpg");
                                  break;
                              default:
                                  $fileSlugUrls = isset( $doc->Link ) ? $doc->Link : '';
                                  $fileSlugUrl = View::asset("images/blank-icon.jpg");
                                  if( $fileSlugUrls != '' ){
                                    $fileSlugUrl = getimagesize( $fileSlugUrls ) !== false ? $fileSlugUrls : View::asset("images/blank-icon.jpg");
                                  }                                  
                                  break;
                          } ?>
                          <div class="col-lg-4 col-sm-6 col-xs-12">
                              <a href="<?php echo isset( $doc->Link ) ? $doc->Link : 'javascript:void(0);'; ?>" class="html5lightbox brochure push-20" download style="min-height: 135px;">
                                  <div class="pull-left ico">
                                      <img src="<?php echo $fileSlugUrl; ?>" alt="icon" width="42" height="60">
                                  </div>
                                  <div class="brochure-body">
                                      <small class="text-muted"><?php echo isset( $doc->DocumentName ) ? $doc->DocumentName : ''; ?></small>
                                      <p><small class="text-muted"><i class="fa fa-eye"></i> Preview</small></p>
                                  </div>
                              </a>
                              <!-- <div class="brochure push-20" download style="min-height: 135px;">
                                  <div class="pull-left ico">
                                      <a href="<?php echo isset( $doc->Link ) ? $doc->Link : 'javascript:void(0);'; ?>" class="html5lightbox"><img src="<?php echo $fileSlugUrl; ?>" alt="icon" width="42" height="60"></a>
                                  </div>
                                  <div class="brochure-body">
                                      <small class="text-muted"><?php echo isset( $doc->DocumentName ) ? $doc->DocumentName : ''; ?></small>
                                      <p><a href="<?php echo isset( $doc->Link ) ? $doc->Link : 'javascript:void(0);'; ?>" class="btn btn-sm text-white btn-info" download><?php /* echo isset( $doc->FileDescription ) ? $doc->FileDescription : ''; */ ?><i class="fa fa-download"></i> Download</a></p>
                                  </div>
                              </div> -->
                          </div>
                        <?php } ?>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <div class="tab-pane fade fade-up <?php echo $tabid == 'client-blogs' ? 'in active' : ''; ?>" id="client-blogs">
                <div class="row sorting-area push-20-t">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <select id="categories" name="categories" class="form-control arrow-down category-box" onchange="window.location.href='/project/view/<?php echo $clientdata->ClientProfileID; ?>/client-blogs/'+this.value">
                                <option value="*" selected="">All Categories</option>
                                <?php
                                foreach ($categories as $cat) {
                                    $selected = (isset($category) && $cat->CategorySlug == $category) ? ' selected ' : ''; ?>
                                    <option value="<?php echo $cat->CategorySlug; ?>" <?php echo $selected;?>><?php echo ucwords($cat->CategoryName); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <form action="" method="get">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="s" placeholder="Search blog" class="form-control category-box" value="<?php echo $searchString; ?>">
                                    <div class="input-group-addon sbox"><span class="icon fa fa-search transition-color"></span></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--blog section-->
                <div class="blog-page">

                  <!-- Dynamic Table Full Pagination -->
                  <ul id="bloglist" class="blog-list row">
                    <?php
                      $cntr = 0; $tdctr = 0;
                      if( isset( $recentPosts ) && count( $recentPosts ) ){
                        foreach( $recentPosts as $article ){ $cntr++; ?>
                          <li class="col-md-4 col-sm-6 col-xs-12">
                            <?php if( $article->PublicView == 0 ){ ?>
                              <div class="corner-box"><div class="cribbon pink"><span>SEEDERS ONLY</span></div></div>
                            <?php } ?>
                            <div class="blog-item">
                                <figure class="blog-image">
                                    <figcaption><a href="<?php echo View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID ); ?>">Read Article</a></figcaption>
                                    <?php if( $article->FeaturedImage ): ?>
                                        <?php echo View::photo( $article->FILink, $article->BlogTitle, "articleFeaturedImage" ); ?>
                                    <?php else: ?>
                                        <img src="<?php echo View::url( 'assets/images' ); ?>/blog/blog-3.jpg" alt="img" />
                                    <?php endif; ?>
                                </figure>
                                <div class="popular-content">
                                    <div class="blog-desc" style="margin-left: 0;">
                                        <h5 style="margin-top: 0;"><a href="<?php echo View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID ); ?>"><?php echo mb_strimwidth(isset( $article->BlogTitle ) ? $article->BlogTitle: "No Category Title", 0, 55, ""); ?></a><span class="line-green"></span></h5>
                                        <?php echo AppUtility::excerptText( $article->BlogContent, 200, ' <a href="'.View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID).'" class="item_button1">...read more</a>' ); ?>
                                    </div>
                                </div>
                                <div class="blog-byDate">
                                    <div class="pmeta">
                                        <div class="met"><i class="fa fa-user"></i> <?php echo isset( $article->FirstName ) ? $article->FirstName : ""; ?> <?php echo isset( $article->LastName ) ? $article->LastName : ""; ?></div>
                                        <div class="met"><i class="fa fa-calendar"></i> <?php echo isset( $article->BlogDatePublished ) && $article->BlogDatePublished != '0000-00-00' ? date( 'j M, Y', strtotime( $article->BlogDatePublished ) ) : '-' ?></div>
                                        <div class="met"><i class="fa fa-tags"></i> <?php echo isset( $article->CategoryName ) ? $article->CategoryName : ""; ?></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                          </li>
                    <?php } } ?>
                  </ul>
                  <?php ?>
                </div>
                <!--end blog section-->
              </div>

              <div class="tab-pane faq fade fade-up <?php echo $tabid == 'client-faq' ? 'in active' : ''; ?>" id="client-faq">
                <!--<h4>FAQ</h4>-->
                <p class="text-muted">
                <?php 
                  $ctr = 0;
                  if ( isset( $faqs ) && count( $faqs ) ){
                    foreach( $faqs as $faq ){ $ctr++; ?>
                      <div class="clearfix"></div>
                      <div class="panel-group accordion" id="caq-a" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                              <a role="button" class="collapsed" data-toggle="collapse" data-parent="#caq-a" href="#caq-a-<?php echo $ctr; ?>" aria-expanded="true">
                                <i class="fa fa-plus toggle-icon"></i>
                                <i class="fa fa-minus toggle-icon"></i>
                                Q<?php echo $ctr.'. '.$faq->FaqTitle; ?>
                              </a>
                            </h4>
                          </div>
                          <div id="caq-a-<?php echo $ctr; ?>" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                              <p><?php echo $faq->FaqContent; ?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                <?php } } ?>
                </p>
              </div>

              <div class="tab-pane fade fade-up <?php echo $tabid == 'client-community' ? 'in active' : ''; ?>" id="client-community">
                <!-- <h4>Community</h4> -->

                <div class="<?php echo $hidden; ?> push-20-t">
                  <!-- modal spinner -->
                  <div class="modal fade spinner-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <span class="fa fa-spinner fa-spin fa-5x"></span>
                        <!-- <img src="<?php echo View::asset( 'images/loading-icon.gif' ); ?>"> -->
                      </div>
                    </div>
                  </div>
                  <!-- END modal spinner -->
                  <div id="resultpane">

                    <section class="breadcrumb">
                      <article class="container">
                        <div class="row">
                          <div class="col-lg-9">
                            <ul>
                              <li><span class="fa fa-vcard"></span>&nbsp; You are here:</li>
                              <li>Dashboard</li>
                            </ul>
                          </div>
                          <div class="col-lg-3">
                            <a class="pull-right" href="javascript:void(0);" onclick="getForums( '<?php echo View::url( "project/forums/newtopic" ); ?>' );"><i class="fa fa-plus"></i> New Topic</a>
                          </div>
                        </div>
                      </article>
                    </section>

                    <table class="table table-vcenter table-header-bg">
                    <?php if( isset( $forums ) ){
                    if( count( $forums ) ){
                    foreach( $forums as $k => $v ){ ?>
                      
                      <thead>
                        <tr>
                          <th colspan="2"><?php echo isset( $v[0] ) ? $v[0]->CatName : ''; ?></th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Topics</th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Posts</th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 200px;">Last Post</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( isset( $v ) ){ foreach( $v as $f ){ ?>
                        <tr>
                          <td class="text-center" style="width: 75px;">
                            <i class="<?php echo isset( $f->SubCatIconClass ) ? $f->SubCatIconClass : ''; ?>"></i>
                          </td>
                          <td class="text-left">
                            <h4 class="h5 font-w600 push-5">
                              <a href="javascript:void(0);" onclick="getForums( '<?php echo View::url( "project/forums/category/$f->SubCatID" ); ?>' )"><?php echo isset( $f->SubCatName ) ? $f->SubCatName : ''; ?></a>
                            </h4>
                            <div class="font-s13 text-muted"><?php echo isset( $f->SubCatDescription ) ? $f->SubCatDescription : ''; ?></div>
                          </td>
                          <td class="text-center hidden-xs hidden-sm">
                            <a class="font-w600" href="javascript:void(0)"><?php echo isset( $f->Topics ) ? $f->Topics : 0; ?></a>
                          </td>
                          <td class="text-center hidden-xs hidden-sm">
                            <a class="font-w600" href="javascript:void(0)"><?php echo isset( $f->Posts ) ? $f->Posts : 0; ?></a>
                          </td>
                          <td class="hidden-xs hidden-sm">
                            <span class="font-s13">
                              <?php 
                              echo isset( $f->Data['PostedBy'] ) ? '<i class="text-muted">by</i> '.$f->Data['PostedBy'] : '-';
                              echo isset( $f->Data['DatePosted'] ) ? '<br>'.date('F j, Y', strtotime( $f->Data['DatePosted'] ) ) : ''; 
                              ?>
                            </span>
                          </td>
                        </tr>
                        <?php } } ?>
                      </tbody>

                    <?php } } else { ?>

                      <thead>
                        <tr>
                          <th colspan="2"></th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Topics</th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Posts</th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 200px;">Last Post</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="5">
                            <div class="h5 hidden-xs text-left">No data found! Contact the administrator to create Categories and Sub-Categories.</div>
                          </td>
                          <td class="hidden"></td>
                          <td class="hidden"></td>
                          <td class="hidden"></td>
                          <td class="hidden"></td>
                        </tr>
                      </tbody>

                    <?php } } ?>

                    </table>
                  </div>
                </div>

                <div class="w-title text-uppercase text-muted">Our Backers</div>
                  <div class="row">
                    <?php
                    $ctr = 0;
                    if( isset( $communities ) && count( $communities ) ){
                      foreach( $communities as $community ){ $ctr++; ?>
                        <?php if( User::can( 'Administer All' )
                          || $clientdata->UserID == User::info('UserID')
                          || ( $userinfo->ReferrerUserID == $clientdata->UserID && $userinfo->Code == 'ASST' ) ){ ?>
                          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

                            <a class="block block-rounded block-link-hover3 showDetails" href="<?php echo View::url( "clients/seederinfo/$community->UserID", true ); ?>">
                              <div class="block-content block-content-full clearfix">
                                <div class="pull-right">
                                  <?php View::photo( $community->FileSlug ? 'files'.$community->FileSlug : 'images/user.png', false, 'img-avatar ', false, true ); ?>
                                </div>
                                <div class="pull-left push-10-t">
                                  <?php $backer = Apputility::maskText( $community->FirstName, 'x', 'x' ).' '.$community->LastName; ?>
                                  <div class="font-w600 push-5"><?php echo Apputility::excerptAsNeeded( $backer, 18, '..' ); ?></div>
                                  <div class="text-muted"><?php echo Apputility::excerptAsNeeded( $community->JobTitle, 20, '..' ); ?></div>
                                </div>
                              </div>
                            </a>

                          </div>
                        <?php }else{ ?>
                          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="block-content block-content-full clearfix">
                            <div class="pull-right">
                              <?php View::photo( $community->FileSlug ? 'files'.$community->FileSlug : 'images/user.png', false, 'img-avatar ', false, true ); ?>
                            </div>
                            <div class="pull-left push-10-t">
                              <?php $backer = Apputility::maskText( $community->FirstName, 'x', 'x' ).' '.$community->LastName; ?>
                              <div class="font-w600 push-5"><?php echo Apputility::excerptAsNeeded( $backer, 18, '..' ); ?></div>
                              <div class="text-muted"><?php echo Apputility::excerptAsNeeded( $community->JobTitle, 20, '..' ); ?></div>
                            </div>
                            </div>
                          </div>
                        <?php } ?>
                          
                  <?php } } ?>
                  </div>
              </div>

            </div><!--end block-content-->

          </div><!--end block-->

      </div>
    </div>
  </article>
</section>

<div class="modal" id="modal-teamMember" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title">Seeder Info</h3>
        </div>
        <div class="block-content">
          
          <!-- <div>Loading...</div> -->

          <!-- modal spinner -->
          <div class="modal fade spinner-modal-pop" data-backdrop="static" data-keyboard="false" tabindex="-1">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <span class="fa fa-spinner fa-spin fa-5x"></span>
              </div>
            </div>
          </div>
          <!-- END modal spinner -->

        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
      </div>
    </div>
  </div>
</div>

<section class="sobg">
  <article class="container team-members">
    <?php if( isset( $teams ) && count( $teams ) ){ ?>

      <h4 class="text-center text-white text-uppercase push-40">Company Team Members</h4>
      <div class="max900 centered">
        <div class="row">

          <?php foreach( $teams as $team ){ ?>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 project-cnt">

              <div class="project-item">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-team-member" data-locations="<?php echo $team->Data; ?>" onclick="biomember( this )">
                  <div class="ft-img">
                      <?php echo View::photo( isset( $team->FileSlug ) && $team->FileSlug != '' ? 'files'.$team->FileSlug : 'images/user.png', false, '', false, false ); ?>
                  </div>
                  <div class="project-content push-0 push-0-l push-0-r push-0-t">
                      <h5 class="min-height-60"><?php echo isset( $team->Name ) ? Apputility::excerptAsNeeded( $team->Name, 18, '..' ) : ''; ?></h5>
                      <p class="project-text-recent min-height-20"><?php echo isset( $team->Position ) ? Apputility::excerptAsNeeded( $team->Position, 20, '..' ) : ''; ?></p>
                      
                  </div>
                </a>
              </div>

          </div>

          <?php } ?>

        </div>
      </div>

    <?php } ?>
  </article>
</section>

<div class="modal" id="modal-teamMember" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title">Seeder Info</h3>
        </div>
        <div class="block-content">
          
          <!-- <div>Loading...</div> -->

          <!-- modal spinner -->
          <div class="modal fade spinner-modal-pop" data-backdrop="static" data-keyboard="false" tabindex="-1">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <span class="fa fa-spinner fa-spin fa-5x"></span>
              </div>
            </div>
          </div>
          <!-- END modal spinner -->

        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
      </div>
    </div>
  </div>
</div>

<!-- team members -->
<div class="modal fade" id="modal-team-member" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
  <div class="modal-dialog modal-dialog-popout">
    <div class="modal-content">
      <div class="block block-themed block-transparent remove-margin-b">
        <div class="block-header bg-primary-dark">
          <ul class="block-options">
            <li>
              <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
            </li>
          </ul>
          <h3 class="block-title"><span id="modal-title"></span></h3>
        </div>
        <div class="block-content">
          <div id="modal-header" class="block text-center">
            <div class="animated fadeInDown">
              <div id="avatar" class="img-wrap img-avatar img-avatar128 img-avatar-thumb"></div>
              <div id="position" class="text-uppercase push-5-t text-info"></div>
            </div>
          </div>
          <div class="animatedx fadeInUp">
            <div id="content" class="backer-view"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
      </div>
    </div>
  </div>
</div>
<!-- end team members -->

<!-- comments start -->     
<div class="block comments <?php echo $nocomments; ?>" id="cln-comments">
    <a href="#" class="minmax" id="minmax"><i class="si si-speech"></i></a>
    <div class="block-header mmbar">
        <div class="block-title">Comments</div>
    </div>
    <div id="client-comment-chat" class="block-content hidden">
        <div id="client-comments-content" class="comments-tab client-page">
            
            <div id="CommentsWrapper" class="actionBox">
              <ul class="commentList">
                <?php $ctr = 0; $cctr = 0;
                if( isset( $newcomments ) && count( $newcomments ) ){
                    foreach( $newcomments as $comment ){ $ctr++; ?>
                    <li id="parent<?php echo $ctr; ?>" class="role<?php echo $comment->Level; ?>">
                        <div class="commenterImage">
                            <?php echo $comment->AvatarLink; ?>
                            <?php echo ($comment->Level == 4) ? '<i class="fa clnt-icon fa-star"></i>' : ''; ?>
                            <?php echo ($comment->Level == 6) ? '<i class="fa ast-icon fa-circle text-warning"></i>' : '';?>
                        </div>
                        <div class="commentText">
                            <div class=""><strong><?php echo $comment->FirstName.' '.$comment->LastName; ?></strong>:&nbsp; <?php echo $comment->CommentContent; ?>
                            </div>
                            <span class="date sub-text">
                                <a href="#" data-id="<?php echo isset( $comment->CommentID ) ? $comment->CommentID : '0'; ?>" class="comment-reply">Reply</a> <span>·</span>                                
                                <?php if( User::can( 'Administer All' )
                                || $userinfo->UserID == $comment->UserID
                                || $userinfo->UserID == $comment->ClientUserID
                                || ($userinfo->ReferrerUserID == $comment->ClientUserID && $userinfo->Code == 'ASST') ){ ?>
                                  <a href="#" data-id="<?php echo isset( $comment->CommentID ) ? $comment->CommentID : '0'; ?>" data-cid="parent<?php echo $ctr; ?>" class="comment-delete">Delete</a> <span>·</span>
                                <?php } ?>
                                <?php echo date('M jS, Y', strtotime( $comment->CommentDate ) ); ?>
                            </span>
                        </div>
                        <ul>
                        <?php $cctr = 0;
                        if( isset( $comment->Children ) && count( $comment->Children ) ){
                        foreach( $comment->Children as $reply ){ $cctr++; ?>
                            <li id="child<?php echo $ctr.'-'.$cctr; ?>" class="role<?php echo $reply->Level; ?>">
                                <div class="commenterImage">
                                    <?php echo $reply->AvatarLink; ?>
                                    <?php echo ($reply->Level == 4) ? '<i class="fa clnt-icon fa-star"></i>' : ''; ?>
                                    <?php echo ($reply->Level == 6) ? '<i class="fa ast-icon fa-circle text-warning"></i>' : '';?>
                                </div>
                                <div class="commentText">
                                    <div class="">
                                        <strong><?php echo $reply->FirstName.' '.$reply->LastName; ?></strong>:&nbsp; <?php echo $reply->CommentContent; ?>
                                    </div>
                                    <?php if( User::can( 'Administer All' )
                                    || $userinfo->UserID == $reply->UserID
                                    || $userinfo->UserID == $reply->ClientUserID
                                    || ( $userinfo->ReferrerUserID == $reply->ClientUserID && $userinfo->Code == 'ASST' ) ){ ?>
                                      <a href="#" data-id="<?php echo isset( $reply->CommentID ) ? $reply->CommentID : '0'; ?>" data-cid="child<?php echo $ctr.'-'.$cctr; ?>" class="comment-delete">Delete</a> <span>·</span>
                                      <?php echo date('M jS, Y', strtotime( $reply->CommentDate ) ); ?>
                                    <?php } else { ?>
                                      <span class="date sub-text">on <?php echo date( 'M jS, Y', strtotime( $reply->CommentDate ) ); ?></span>
                                    <?php } ?>
                                </div>
                            </li>
                        <?php } } else { ?>
                            <li></li>
                        <?php } ?>
                        </ul>
                    </li>
                <?php } } ?>
              </ul>
            </div>
        </div>
        <form id="formComments" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
            <input type="hidden" name="action" value="sendcommentview" />
            <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
            <input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">
            <div class="form-group role<?php echo $currUser->Level; ?>">
                <div class="commenterImage write-comment-image inline-block">
                    <?php echo $currUser->AvatarLink; ?>
                    <?php echo $currUser->Indicator; ?>
                </div>
                <div class="write-comment inline-block">
                    <input id="CommentContent" type="text" value="" class="form-control radiusx" placeholder="Write a comment..." name="CommentContent" required="required">
                    <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>
                </div>
                <div class="clear"></div>
            </div>
        </form>
        <input type="hidden" id="currName" value="<?php echo $currUser->Name; ?>">
        <input type="hidden" id="Level" value="<?php echo $userinfo->Level; ?>">
        <input type="hidden" id="AvatarLink" value="<?php echo $currUser->AvatarLinkConverted; ?>">
        <input type="hidden" id="Indicator" value="<?php echo $currUser->Indicator; ?>">
    </div>
</div>
<!-- comments end --> 

<?php View::footer(); ?>

<script type="text/javascript">
  $('#bloglist').easyPaginate({
      paginateElement : 'li',
      elementsPerPage: 6,
      effect: 'fade',
      slideOffset : 100,
      nextButtonText : "Next",
      prevButtonText : "Prev",
      lastButtonText : "Last",
      firstButtonText: "First"
  });

  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  $( '#client-comment-chat' ).hide();
  $( document ).ready( function(){
    $( "#spinner" ).hide();
    $( '#client-comment-chat' ).removeClass( 'hidden' );
    // $( '#client-comments-tab' ).click( function( e ) {
    //   e.preventDefault();
    //   setTimeout( function(){ autoScroll(); }, 200);
    // });

    $( '.html5lightbox' ).click( function( e ){
      var file = $( this ).attr( "href" );
      createDownloadButton( file );
      // $( '#html5-text' ).append( '<div class="push-right"><i class="fa fa-download"></i> DOWNLOAD</div>' );
      // $.magnificPopup.open({
      //   items: {
      //     src: $('<img src="' + file + '"/><a class="download" href="' + file + '">DOWNLOAD</a>'),
      //     type: 'inline'
      //   },
      //   closeBtnInside: false,
      // });
      e.preventDefault();
    });
  });

  $( document ).keypress( function(e){
    if( e.keyCode == 13 ){
      e.preventDefault();
      var reply = $('#ReplyContent').val();
      var comment = $('#CommentContent').val();
      refid = 'REFID'+new Date().getTime();

      if( comment.trim().length > 0 ){
        saveComment( '#formComments', refid );
        appendHTML( comment, 'comment', refid );
      }
      if( reply.trim().length > 0 ){
        saveComment( '#formCommentsReply', refid );
        appendHTML( reply, 'reply', refid );
      }
    }
  });

  $( '#minmax' ).click(function(){
      toggleComments();
  });

  $( '.mmbar' ).click(function(){
      toggleComments();
  });

  $( "#blogstable" ).DataTable({
      pageLength: 5,
      bSort: false,
      searching: false,
      lengthChange: false,
      pagingType: "full_numbers"
  });

  $( '.comment-reply' ).click( function(e){
    e.preventDefault();
    $( '#formCommentsReply' ).remove(); //remove first the previous form
    $( 'li' ).removeClass( 'referenceComment' ); //remove first the previous classname
    $( this ).closest( 'li' ).find( 'ul li' ).last().addClass( 'referenceComment' );
    commentid = $(this).attr( 'data-id' );
    replyWrapper = '<form id="formCommentsReply" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">';
    replyWrapper += '<input type="hidden" name="action" value="sendcommentview" />';
    replyWrapper += '<input type="hidden" name="ParentID" value="'+commentid+'">';
    replyWrapper += '<input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">';
    replyWrapper += '<input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">';
    replyWrapper += '<div class="form-group role<?php echo $currUser->Level; ?>">';
    replyWrapper += '   <div class="write-comment-image inline-block">';
    replyWrapper += '       <?php echo $currUser->AvatarReplyLink; ?>';
    replyWrapper += '       <?php echo $currUser->Indicator; ?>';
    replyWrapper += '   </div>';
    replyWrapper += '   <div class="write-comment inline-block">';
    replyWrapper += '       <input id="ReplyContent" type="text" value="" class="form-control" placeholder="Write a reply..." name="CommentContent" required="required">';
    replyWrapper += '       <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>';
    replyWrapper += '   </div>';
    replyWrapper += '   <div class="clear"></div>';
    replyWrapper += '</div>';
    replyWrapper += '</form>';

    if( $( this ).closest( 'li' ).find( 'ul li' ) ){
      $( this ).closest( 'li' ).find( 'ul li' ).last().after( replyWrapper );
    } else {
      $( this ).parent().append( replyWrapper );
    }
    $( '#ReplyContent' ).focus();
  });

  $( '.comment-delete' ).click( function(e){
    e.preventDefault();
    var id = $( this ).attr( 'data-id' );
    var cid = $( this ).attr( 'data-cid' );
    deleteComment( id, cid );
  });

  $( '.showDetails' ).click( function(){ $( '.spinner-modal-pop' ).modal( 'show' ); });

  function toggleComments(){
      $('#client-comment-chat').slideToggle();
      autoScroll();
  }

  function createDownloadButton( file ){
    $( '#html5-elem-box' ).append( '<div id="html5-elem-download" class="text-left push-10-l push-5"><a href="'+file+'" download="'+file+'" class="btn btn-sm text-white btn-info"><i class="fa fa-download"></i> DOWNLOAD</a></div>' );
  }

  // edit my topic
  function editMyTopic( fid ){
    // var mycontent = $( '#mycontent' ).text();
    // var tags = '<textarea class="form-control full tinyMCE" name="topic[Content]" id="textContent" cols="30" rows="10" style="width:100%;">'+mycontent+'</textarea>';
    // var saveedittags = '<a href="javascript:void(0);" class="btn btn-info btn-sm push-5-t" onclick="saveMyEditedTopic( '+fid+' );"><i class="fa fa-save"></i> SUBMIT</a>';
    // $( '#mycontent' ).append( tags );
    // $( '#mycontent' ).append( saveedittags );
    // $( '#btn-edit-wrapper' ).html( '' );
    // loadTinyMCE();
    $( '#btn-edit-wrapper' ).addClass( 'hidden' );
    $( '#form-edit-wrapper' ).removeClass( 'hidden' );
    $( '#form-edit-wrapper' ).slideDown();
    // $( '#textContentEdit' ).html( tinymce.get( 'textContentEdit' ).getContent() );
    tinymce.get( 'textContentEdit' ).setContent();
  }

  // cancel my edited topic
  function cancelMyEditedTopic(){
    $( '#btn-edit-wrapper' ).removeClass( 'hidden' );
    $( '#form-edit-wrapper' ).addClass( 'hidden' );
    $( '#form-edit-wrapper' ).slideUp();
  }

  // save comment to database
  function saveComment( formid, refid ){
    $.ajax({
      type: 'POST',
      url: '/clients/comments/'+refid,
      data: $( formid ).serialize(),
      success: function(){}
    });
  }

  //delete comment from database
  function deleteComment( id, cid ){
    var checkstr =  confirm('are you sure you want to delete this comment?');
    if( checkstr == true ){
      $.ajax({
        type: 'POST',
        url: '/clients/delcomm/'+id,
        data: '',
        success: function(){}
      });
      $( '#'+cid ).remove();
    } else { return false; }
  }

  // append comment/reply for comment box
  function appendHTML( msg, type, refid ){
    var now = new Date();
    now = monthNames[ now.getMonth() ] + ' ' + getGetOrdinal( now.getDate() ) + ', ' + now.getFullYear();

    var name = $('#currName').val();
    var leve = $('#Level').val();
    var link = $('#AvatarLink').val();
    var indi = $('#Indicator').val();
    var html = '<li class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <span class="date sub-text">on '+now+'</span></div></li>';
    html = '<li id="'+refid+'" class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <a href="#" data-id="'+refid+'" class="append-delete">Delete</a> <span>·</span> '+now+'</div></li>';

    if( type == 'comment' ){
      $( '.actionBox > ul' ).append( html );
      $( '#CommentContent' ).val( '' );
    } else {
      $( '.referenceComment' ).removeClass( 'referenceComment' ).after( html );
      $( '#ReplyContent' ).val( '' );
    }

    $( '.append-delete' ).click( function(e){
      e.preventDefault();
      var id = $( this ).attr( 'data-id' );
      deleteComment( id, id );
    });
    autoScroll();
  }

  // popout member info
  function biomember( el ){
    var json = $( el ).attr( 'data-locations' );
    var data = jQuery.parseJSON( json );
    
    $( '#modal-header' ).removeClass( 'hidden' );
    $( '#modal-title' ).text( data.Name );
    $( '#avatar' ).html( data.Avatar );
    $( '#position' ).html( '<b>'+data.Position+'</b>' );
    $( '#content' ).html( '<div class="text-divider font-w600 push-5-t text-left text-uppercase"><span>Bio</span></div>'+data.Bio );

    return true;
  }

  // submit new topic
  function doSubmitTopic( url, redirect ){
    // tinyMCE.triggerSave();
    //tinyMCE.get("textContent").save();
    var fdata = new FormData( $( '#formForumNewTopic' )[0] );
    processSubmission( url, redirect, fdata );
  }

  // submit reply
  function doSubmitReply( url, redirect ){
    // tinyMCE.triggerSave();
    //tinyMCE.get("textContent").save();
    var fdata = new FormData( $( '#formForumReply' )[0] );
    processSubmission( url, redirect, fdata );
  }

  // submit reply
  function doSubmitEdit( url, redirect ){
    // tinyMCE.triggerSave();
    // tinyMCE.get("textContent").save();
    var fdata = new FormData( $( '#formForumEdit' )[0] );
    processSubmission( url, redirect, fdata );
  }

  // ajax processing of new topic and reply
  function processSubmission( url, redirect, fdata ){
    $.ajax({
      method: 'POST',
      url: url,
      data: fdata,
      dataType: "json",
      contentType: false,
      cache: false,
      processData: false,
      success: function( data ){
          getForums( redirect );
      },
      error: function( xhr, desc, err ){
          console.log( err );
      }
    });
  }

  // get html results from ajax call for forum dashboard
  function getForums( url ){
    // removeTinyMCE(); // clear first all tinyMCE instances
    $.ajax({
      type: 'POST',
      url: url,
      data: '',
      beforeSend: function(){
        $( '.spinner-modal-lg' ).modal( 'show' );
      },
      success: function( data ){
        $( '#resultpane' ).html( data );
        initTinyMCE(); // load tinyMCE to textareas
        setDataTable(); // set datatable config
        $( '.spinner-modal-lg' ).modal( 'hide' );
      },
      error: function( jqXHR, textStatus, errorThrown ){
        console.log( textStatus, errorThrown );
        $( '.spinner-modal-lg' ).modal( 'hide' );
      }
    });
  }

  // set datatable config from ajax call
  function setDataTable(){
    var mytable = $( '#tableforum' ).DataTable({
      "pageLength": 10,
      "ordering": false,
      "searching": false,
      "bLengthChange": false,
      "pagingType": "full_numbers"
    });
  }

  // load tinyMCE on textarea
  function initTinyMCE(){
    // tinyMCE.editors.length = 0;
    tinymce.init({
      selector: ".tinyMCE",  // change this value according to your HTML
      branding: false,
      mode: "simple",
      setup: function( editor ){
        editor.on( 'change', function(){
          tinymce.triggerSave();
        });
      }
    });
    // tinymce.EditorManager.execCommand( 'mceAddEditor', true, 'textContent' );
    // tinymce.EditorManager.execCommand( 'mceAddEditor', true, 'textContentEdit' );
  }

  // clear any existence of tinyMCE on textarea
  function removeTinyMCE(){
    tinymce.EditorManager.remove( '.tinyMCE' );
    tinymce.EditorManager.length = 0;
    // $( '.tinyMCE' ).each( function(){
    //   $( this ).tinymce().remove();
    //   $( this ).removeClass( 'tinyMCE' );
    // });
    // tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, 'textContent' );
    // tinymce.EditorManager.execCommand( 'mceRemoveEditor', true, 'textContentEdit' );
    // tinymce.EditorManager.editors = [];
  }

  function reloadTinyMCE(){
    tinymce.EditorManager.editors.forEach( function( editor ){
      var old_settings = tinymce.settings;
      tinymce.settings = editor.settings;
      tinymce.EditorManager.execCommand( 'mceRemoveEditor', false, editor.id );
      tinymce.EditorManager.execCommand( 'mceAddEditor', false, editor.id );
      tinymce.settings = old_settings;
    });
  }

  // get ordinal order of a number
  function getGetOrdinal( n ){
    var s = ["th","st","nd","rd"], v = n%100;
    return n+(s[(v-20)%10]||s[v]||s[0]);
  }

  function autoScroll(){
    // $( '#client-comments-content' ).animate( { scrollTop: $( '.commentList' )[0].scrollHeight }, 2000);
    // $( '#client-comments-content' ).scrollTop( $( '.commentList' )[0].scrollHeight );
    $( '#client-comments-content' ).scrollTop( $('#client-comments-content')[0].scrollHeight );
  }
</script>