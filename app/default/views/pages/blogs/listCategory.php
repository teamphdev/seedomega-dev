<?php 
View::$title = 'Blog Categories';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <article class="container projects style-2">

        <div class="block">                
            <div class="tab-content">
                <!-- Page Content -->
                <!-- Dynamic Table Full Pagination -->
                <?php echo View::getMessage(); ?>
                <table class="table table-divide table-hover dt-responsive table-vcenter table-header-bg js-dataTable-full-pagination" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url( 'blogs/categories/add' ); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Add Category</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th width="10"></th> -->
                            <th width="50">Category ID</th>
                            <th width="150">Category Name</th>
                            <th class="text-center" style="max-width:150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if( isset( $categories ) && count( $categories ) ){
                        foreach( $categories as $cat ){ $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="text-center"><label><input type="checkbox" value="<?php echo $cat->BlogCatID; ?>"></label></td> -->
                            <td>BlogCatID-#<?php echo ucwords( $cat->BlogCatID ); ?></td>
                            <td><?php echo $cat->CategoryName ? $cat->CategoryName : ""; ?></td>
                            <td class="text-center">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                    <div class="">
                                        <div class="dropdown more-opt">
                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?php echo View::url( 'blogs/categories/edit/'.$cat->BlogCatID ); ?>" title="Edit Category" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                <li><a href="<?php echo View::url( 'blogs/categories/delete/'.$cat->BlogCatID ); ?>" title="Delete Category" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete data?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'blogs/categories/edit/'.$cat->BlogCatID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit Category"><i class="fa fa-edit"></i> Edit</a>
                                    </div>                                    
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="3">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>

