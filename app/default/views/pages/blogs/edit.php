<?php 
View::$title = 'Edit Post';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url( View::$segments[0] ); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset( View::$segments[1] ) ){ ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url( View::$segments[0].'/lists' ); ?>">Manage Blogs</a></li>
                <li class="fa fa-angle-right"></li>
                <li><?php echo $blogdata->BlogTitle ? $blogdata->BlogTitle : ""; ?></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <article class="container projects-page style-2">
        <h3 class="page-title"><?php echo View::$title; ?></h3>
        <div class="block">                
            <div class="block-content tab-content bg-white">
                <input name="image" type="file" id="upload" class="hidden" onchange="">
                <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="updatepost">
                    <input type="hidden" name="blogid" value="<?php echo $blogdata->BlogID; ?>" />
                    <input type="hidden" name="BlogDatePublished" value="<?php echo $blogdata->BlogDatePublished; ?>" />

                    <div class="block-content">
                        
                        <div class="row">
                            <?php echo View::getMessage();  ?>
                            <div class="col-sm-9">
                                <div class="push-30-r">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" value="<?php echo ($blogdata->BlogTitle) ? $blogdata->BlogTitle : ""; ?>" name="BlogTitle" class="form-control col-xs-12" required>
                                        <div>Permalink: <a href="<?php echo View::url('blogs/view/'.$blogdata->BlogSlug.'/'.$blogdata->BlogID); ?>" target="_blank"><?php echo View::url('view/'.$blogdata->BlogSlug.'/'.$blogdata->BlogID); ?></a></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address"></label>
                                        <textarea class="form-control tinyMCE" name="BlogContent" cols="30" rows="10"><?php echo ($blogdata->BlogContent) ? $blogdata->BlogContent : ""; ?></textarea>
                                    </div>
                                </div>             
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Status</label>                                 
                                    <select class="form-control blog-status" name="BlogStatus">
                                        <option value="Published" <?php echo ($blogdata->BlogStatus == "Published") ? 'selected' : ""; ?>>Published</option>
                                        <option value="Pending" <?php echo ($blogdata->BlogStatus == "Pending") ? 'selected' : ""; ?>>Pending</option>
                                        <option value="Draft" <?php echo ($blogdata->BlogStatus == "Draft") ? 'selected' : ""; ?>>Draft</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Date Published</label>                                 
                                    <input type="text" class="form-control" value="<?php echo date('d M Y', strtotime($blogdata->BlogDatePublished)); ?> @ <?php echo date('h:i:a', strtotime($blogdata->BlogDatePublished)); ?>" readonly="">
                                </div>     

                                <div class="form-group">
                                    <label>Featured Image</label>

                                    <?php if ($blogdata->FeaturedImage): ?>
                                        <?php $featuredImage = View::common()->getUploadedFiles($blogdata->FeaturedImage); ?>
                                        <?php echo View::photo((isset($featuredImage[0]) ? '/files/'.$featuredImage[0]->FileSlug : '/images/user.png'),"img",""); ?>
                                    <?php endif;?>
                                    <br><br>
                                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="FeaturedImage" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                </div>               

                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="BlogCatID" class="form-control">
                                    <?php if ($categories) { ?>
                                        <?php foreach ($categories as $cat) { ?>
                                            <option value="<?php echo $cat->BlogCatID; ?>" <?php echo ($blogdata->BlogCatID == $cat->BlogCatID) ? 'selected' : ""; ?>><?php echo $cat->CategoryName; ?></option>                                
                                        <?php } ?>
                                    <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Accessibility</label>
                                    <?php $publicViewOptions = array( 1 => 'Public', 0 => 'Seeders' ); ?>
                                    <select class="form-control" name="PublicView" required="">
                                        <?php foreach( $publicViewOptions as $key => $val ):
                                            $selected = $key == $blogdata->PublicView ? 'selected': '';
                                            ?>
                                            <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group">                          
                                    <button type="submit" class="btn btn-rounded btn-primary" style="min-width: 120px;">Update</button>
                                </div>                                
                            </div>
                        </div>
                    
                    </div>            

                </form>
            </div>
        </div>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>
