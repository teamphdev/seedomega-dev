<?php
  View::$title = 'View Post';
  View::$ptitle = $blogdata->BlogTitle;
  View::$bodyclass = User::info('Sidebar');
  View::header();
?>

<!-- page content -->
<!-- section class="header-bottom">
    <article>
      <div class="container"><h1><?php //echo View::$title; ?></h1></div>
    </article>
</section -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-12">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <?php if( isset( View::$segments[1] ) ){ ?>
              <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <li class="fa fa-angle-right"></li>
              <li><?php echo $blogdata->BlogTitle ? $blogdata->BlogTitle : ""; ?></li>
            <?php }else{ ?>
              <li><?php echo View::$segments[0]; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
   <article class="container projects-page" id="popular">
      <div class="row">
         <!-- ************************ Left Side Area ************************ -->
         <div class="col-lg-3 sidebar-container">
           <div class="sidebar">
              <div class="sidebar-item">
                  <div class="block-content">
                    <div class="bw-heading push-0">Recent Posts</div>
                  </div>
                  <div class="text-left block-content">
                      <?php
                      $ctrl = 0;
                      if( isset( $blogs ) && count( $blogs ) ){
                          foreach( $blogs as $blog ){ $ctrl++; ?>
                              <div class="blog-item widget">
                                  <a href="<?php echo $blog->Link; ?>">
                                      <div class="row">
                                          <div class="col-lg-4 col-xs-4">
                                              <div class="blog-img">
                                                  <?php View::photo( isset( $blog->Data['FImage'][0] ) ? 'files'.$blog->Data['FImage'][0]->FileSlug : '/images/user.png', $blog->BlogTitle, "articleFeaturedImage" ); ?>
                                              </div>
                                          </div>
                                          <div class="col-lg-8 col-xs-8 no-padding">
                                              <div class="blog-content">
                                                  <?php $excerptTitle = AppUtility::excerptAsNeeded( $blog->BlogTitle, 57, '' ); ?>
                                                  <div class="blog-title"><?php echo mb_strimwidth( isset( $excerptTitle ) ? $excerptTitle : "No Category Title", 0, 55, "" ); ?></div>
                                                  <div class="blog-footer">
                                                      <?php echo $blog->CategoryName ? AppUtility::excerptText( $blog->CategoryName, 30, '' ) : ""; ?>   
                                                      <span class="push-10-r push-10-l">/</span>   
                                                      <span class="text-muted"><?php echo ( $blog->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime( $blog->BlogDatePublished ) ) : '-' ?></span></div>
                                              </div>
                                          </div> 
                                      </div>
                                  </a>
                              </div>
                              
                          <?php }
                      } ?>
                  </div>
              </div>

              <div class="sidebar-item">
                 <div class="block-content">
                    <div class="bw-heading push-0">Categories</div>
                  </div>
                 <div class="block-content block-content-full">
                    <ul class="popular-cat-widget">
                      <?php if( $categories ){ ?>
                          <?php foreach( $categories as $cat ){ ?> 
                              <li><a href="/blogs/<?php echo $cat->CategorySlug; ?>"><?php echo $cat->CategoryName; ?></a></li>
                          <?php } ?>
                      <?php } ?>
                    </ul>
                 </div>
              </div>             

           </div>
         </div>

         <div class="col-lg-9 content-cnts">
            <div class="blog-single">
               <figure>
                  <?php //if( $blogdata->Data['FImage'] && is_readable($blogdata->Data['FImage'][0]->FilePath) ):
                  if( $blogdata->Data['FImage'] && @getimagesize(View::url(APPURI.'/views/assets/files/'.$blogdata->Data['FImage'][0]->FileSlug)) ):
                    View::photo( isset( $blogdata->Data['FImage'][0] ) ? 'files'.$blogdata->Data['FImage'][0]->FileSlug : '/images/user.png', $blogdata->BlogTitle, "articleFeaturedImage" ); ?>
                  <?php endif; ?>
               </figure>
               <div class="blog-byDate">
                  <div class="pmeta">
                     <div class="met"><i class="fa fa-user"></i> <?php echo ($blogdata->FirstName) ? $blogdata->FirstName : ""; ?> <?php echo ($blogdata->LastName) ? $blogdata->LastName : ""; ?></div>
                     <div class="met"><i class="fa fa-calendar"></i> <?php echo ($blogdata->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime($blogdata->BlogDatePublished)) : '-' ?></div>
                     <div class="met"><i class="fa fa-tags"></i> <a href="/blogs/<?php echo $blogdata->CategorySlug; ?>"><?php echo ($blogdata->CategoryName) ? $blogdata->CategoryName : "Uncategorized"; ?></a></div>
                  </div>
                  <div class="clear"></div>
               </div>
               <div class="blog-description ul-content">
                    <h3><?php echo ($blogdata->BlogTitle) ? ucwords($blogdata->BlogTitle) : ""; ?></h3>
                    <?php echo ($blogdata->BlogContent) ? $blogdata->BlogContent : "No Content"; ?>
               </div>
            </div>
         </div>         

      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>