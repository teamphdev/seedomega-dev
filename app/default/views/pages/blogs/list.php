<?php 
    View::$title = 'Manage Blogs';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header();
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?> 
                <table class="table table-divide table-hover dt-responsive table-vcenter table-header-bg js-dataTable-full-pagination" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('blogs/add'); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Create Blog</a>'>
                    <thead>
                        <tr class="headings">
                            <th class="text-center">Date</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Company</th>
                            <th class="text-center">Author</th>
                            <th class="text-center">Category</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if( count( $blogs ) ){
                        foreach( $blogs as $blog ){ $cntr++;
                            switch( $blog->BlogStatus ){
                                case 'Published':
                                    $bstatus = '<span class="label label-success">Published</span>';
                                    break;
                                case 'Pending':
                                    $bstatus = '<span class="label label-warning">Pending</span>';
                                    break;
                                case 'Draft':
                                    $bstatus = '<span class="label label-info">Draft</span>';
                                    break;

                                default: $bstatus = ''; break;
                            }
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?>">
                            <td class="text-center"><?php echo ($blog->BlogDate == '0000-00-00') ? '---' : date('Y/m/d', strtotime($blog->BlogDate)); ?></td>
                            <td><a href="<?php echo View::url('blogs/edit/'.$blog->BlogID); ?>"><b><?php echo ucwords($blog->BlogTitle); ?></b></a></td>
                            <td><?php echo ucwords( $blog->CompanyName ); ?></td>
                            <td><?php echo ucwords( $blog->FirstName ); ?> <?php echo ucwords( $blog->LastName ); ?></td>
                            <td><?php echo $blog->CategoryName; ?></td>
                            <td class="text-center">
                                <?php echo $bstatus; ?>
                            </td>
                            <td class="text-center">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'blogs/view/'.$blog->BlogSlug.'/'.$blog->BlogID ); ?>" title="View Blog Post" data-toggle="tooltip"><i class="fa fa-eye pull-right"></i> View</a></li>
                                            <li><a href="<?php echo View::url( 'blogs/edit/'.$blog->BlogID ); ?>" title="Edit Blog Post" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                            <li><a href="<?php echo View::url( 'blogs/delete/'.$blog->BlogID ); ?>" title="Delete Blog Post" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this blog?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="7">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>