<?php 
    View::$title = 'Blogs';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1>What's New!</h1>
            <p>The new way to invest in innovative business. We want to ensure our clients have the best opportunity for success, so we offer support before, during and after the offer for companies and investors.</p>
        </div>
    </article>
</section>

<section class="whitebg">
    <!-- Page Content -->
    <div class="container blog-listing">
        <div class="row">
            <div class="col-lg-8 animated slideInLeft">
                <div class="bw-heading">
                    <?php echo ($searchString) ? 'Search Results' : 'Latest Articles'; ?>
                </div>

                <div class="row sorting-area">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <select name="categories" class="form-control arrow-down category-box" onchange="window.location.href='/blogs/'+this.value;">
                                <option value="*" selected="">All Categories</option>
                                <?php
                                foreach ($categories as $cat) {
                                    $selected = (isset($category) && $cat->CategorySlug == $category) ? ' selected ' : ''; ?>
                                    <option value="<?php echo $cat->CategorySlug; ?>" <?php echo $selected;?>><?php echo ucwords($cat->CategoryName); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <form action="" method="get">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" name="s" placeholder="Search blog" class="form-control category-box" value="<?php echo $searchString;?>">
                                    <button type="submit" id="hanap" class="hidden"></button>
                                    <div class="input-group-addon sbox pointer"><span class="icon fa fa-search transition-color"></span></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php
                $cntr = 0;
                if( isset( $SOblogs ) && count( $SOblogs ) ){ ?>
                    <table id="blogstable" class="table table-hover no-hover-border dt-responsive row" width="100%">
                     <thead class="hidden"><tr><th></th></tr></thead>
                     <?php foreach( $SOblogs as $soblog ){ $cntr++; ?>
                      <tr><td>
                        <div class="blog-item">
                            <a href="<?php echo $soblog->Link; ?>">
                                <div class="row">
                                    <div class="col-lg-5 col-xs-12">
                                        <div class="blog-img">
                                          <?php 
                                            if( $soblog->Data['FImage'] ) {
                                                $img = isset( $soblog->Data['FImage'][0] ) ? 'files'.$soblog->Data['FImage'][0]->FileSlug : '/images/blog/blog-3.jpg'; 
                                                if( View::photo( $img,false,false,false,false,false ) ) {
                                                    View::photo( $img, $soblog->BlogTitle, "articleFeaturedImage min-height-180", false, 'height:180px;' );
                                                } else { 
                                                    View::photo( 'images/blog/blog-3.jpg', false, false, false, 'height:180px;' );                                             
                                                }
                                            } else {
                                                View::photo( 'images/blog/blog-3.jpg',false, false, false, 'height:180px;' );
                                            }
                                          ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-xs-12">
                                        <div class="blog-content">
                                            <?php $excerptTitle = AppUtility::excerptAsNeeded( $soblog->BlogTitle, 31 ); ?>
                                            <div class="blog-title"><?php echo mb_strimwidth( isset( $excerptTitle ) ? $excerptTitle : "No Category Title", 0, 55, "" ); ?></div>
                                            <div class="blog-desc"><?php echo AppUtility::excerptText( $soblog->BlogContent, 180,' <a href="'.$soblog->Link.'" class="link">...Read More</a>' );?></div>
                                            <div class="blog-footer">
                                                <?php echo $soblog->CategoryName ? AppUtility::excerptText( $soblog->CategoryName, 30, '' ) : ""; ?>   
                                                <span class="push-10-r push-10-l">/</span>   
                                                <span class="text-muted"><?php echo ( $soblog->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime( $soblog->BlogDatePublished ) ) : '-' ?></span>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </a>
                        </div>
                      </td></tr>
                     <?php } ?>
                    </table>
                <?php } else { ?>
                    <p>No results found.</p>
                <?php } ?>

            </div>
            <div class="col-lg-4 animated slideInRight">
                <div class="sidebar">

                    <div class="sidebar-item">
                        <div class="bw-heading">Recent Blogs</div>
                        <div class="text-left ul-content">
                            <?php
                            $ctrl = 0;
                            if( isset( $SOblogs ) && count( $SOblogs ) ){
                                foreach( $SOblogs as $soblog ){ $ctrl++; if ($ctrl <= 3): ?>
                                    <div class="blog-item widget">
                                        <a href="<?php echo $soblog->Link; ?>">
                                            <div class="row">
                                                <div class="col-lg-4 col-xs-4">
                                                    <div class="blog-img">
                                                        <?php 
                                                        if( $soblog->Data['FImage'] ) {
                                                            $img = isset( $soblog->Data['FImage'][0] ) ? 'files'.$soblog->Data['FImage'][0]->FileSlug : '/images/blog/blog-3.jpg'; 
                                                            if( View::photo( $img,false,false,false,false,false ) ) {
                                                                View::photo( $img, $soblog->BlogTitle, "articleFeaturedImage", false, '' );
                                                            } else { 
                                                                View::photo( 'images/blog/blog-3.jpg' );                                             
                                                            }
                                                        } else {
                                                            View::photo( 'images/blog/blog-3.jpg' );
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-xs-8 no-padding">
                                                    <div class="blog-content">
                                                        <?php $excerptTitle = AppUtility::excerptAsNeeded( $soblog->BlogTitle, 57, '' ); ?>
                                                        <div class="blog-title"><?php echo mb_strimwidth( isset( $excerptTitle ) ? $excerptTitle : "No Category Title", 0, 55, "" ); ?></div>
                                                        <div class="blog-footer">
                                                            <?php echo $soblog->CategoryName ? AppUtility::excerptText( $soblog->CategoryName, 30, '' ) : ""; ?>   
                                                            <span class="push-10-r push-10-l">/</span>   
                                                            <span class="text-muted"><?php echo ( $soblog->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime( $soblog->BlogDatePublished ) ) : '-' ?></span></div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </a>
                                    </div>
                                    
                                <?php endif; }
                            } ?>
                        </div>
                    </div>
                    <div class="clear"></div>

                </div>
            </div>
        </div>
    </div>

</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $( '#bloglist' ).easyPaginate({
            paginateElement : 'li',
            elementsPerPage: 8,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });

        $( ".sbox" ).click( function(){
            $( '#hanap' ).click();
        });
    });

    $( "#blogstable" ).DataTable({
        pageLength: 10,
        searching: false,
        lengthChange: false,
        pagingType: "full_numbers"
    });
</script>