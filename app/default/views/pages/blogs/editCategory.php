<?php 
View::$title = 'Edit Category';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <article class="container start-projects style-2">
        <h3 class="page-title"><?php echo View::$title; ?></h3>
        <div class="block">                
            <div class="block-content tab-content bg-white">
                <?php echo View::getMessage(); ?> 
                <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                <input type="hidden" name="action" value="updatecategory">
                <input type="hidden" name="catid" value="<?php echo $catdata->BlogCatID; ?>">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Category Name : 
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" name="CategoryName" class="form-control" value="<?php echo isset($catdata->CategoryName) ? $catdata->CategoryName : ""; ?>" placeholder="Category Name">
                        </div>
                    </div>
<?php /*
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                            Category Slug : 
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <input type="text" name="CategorySlug" class="form-control" value="<?php echo isset($catdata->CategorySlug) ? $catdata->CategorySlug : ""; ?>" placeholder="Category Slug">
                        </div>
                    </div>  
*/ ?>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('blogs/categories'); ?>" class="btn btn-rounded btn-danger"><i class="si si-action-undo"></i> <?php echo Lang::get('AGT_EDIT_BCKBTN'); ?></a>
                            <button type="submit" class="btn btn-rounded btn-primary">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </article>
</section>
<!-- /page content -->
<?php View::footer(); ?>