<?php 
View::$title = 'Core Bank Accounts';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block">
                <?php echo View::getMessage(); ?>                
                <table class="table table-divide js-dataTable-full-pagination table-header-bg table-hover table-vcenter" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="/corebankaccounts/add"><i class="fa fa-bank push-5-r"></i> Create New</a>'>
                    <thead>
                        <tr class="headings">
                            <th class="text-center visible-lg">ID</td>
                            <th class="text-center no-wrap">Account Name</td>
                            <th class="text-center no-wrap">Bank Name</td>
                            <th class="text-center no-wrap">Account Number</td>
                            <th class="text-center no-wrap">Date Added</td>
                            <th class="text-center" style="max-width:150px;">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if(count($bankaccounts)) {
                            foreach($bankaccounts as $data) { $cntr++;
                            ?>
                            <tr>
                                <td class="text-center text-muted visible-lg">
                                    #<?php echo $data->CoreBankAccountID; ?>
                                </td>
                                <td class="font-w600"><?php echo isset($data->AccountName) ? ucwords($data->AccountName) : "-" ?></td>
                                <td>
                                    <?php echo isset($data->Name) ? ucwords($data->Name) : "-" ?>
                                </td>
                                <td class="visible-lg">
                                   <?php echo isset($data->AccountNumber) ? ucwords($data->AccountNumber) : "-" ?>
                                </td>
                                <td class="text-muted visible-lg">
                                    <small><?php echo isset($data->DateAdded) ? date('M d Y', strtotime($data->DateAdded) ) : "-" ?></small>
                                </td>
                                <td class="text-center">
                                    <?php if( User::can( 'Administer All' ) || User::can( 'Delete Core Bank Accounts' ) ){ ?>
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo View::url( 'corebankaccounts/edit/'.$data->CoreBankAccountID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                    <li><a href="<?php echo View::url( 'corebankaccounts/delete/'.$data->CoreBankAccountID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this bank account?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="">
                                            <a href="<?php echo View::url( 'corebankaccounts/edit/'.$data->CoreBankAccountID ); ?>" class="btn btn-sm btn-default btn-rounded" title="" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                        </div>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php }
                        }else{ ?>
                            <tr>
                                <td class="text-muted" colspan="6">
                                    Currently No Inquiries
                                </td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>                        
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>