<?php 
View::$title = 'Contact Us';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li>Contact Us</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="white">
   <article class="container contactpage">
      <div class="row row-header">
         <div class="col-lg-12">
            <h3>Contact Details</h3>
            <p>Hi, we are more than happy to serve, you can call us or email us please see the information below.</p>
         </div>
         <div class="col-lg-4 contact-sec">
            <i class="fa fa-map-marker green"></i>
            <span class="line-green"></span>
            18 Marina Boulevard #35-08<br />Singapore 018980
         </div>
         <div class="col-lg-4 contact-sec">
            <i class="fa fa-phone yellow"></i>
            <span class="line-yellow"></span>
            +65 6651 4886
         </div>
         <div class="col-lg-4 contact-sec">
            <i class="fa fa-envelope red"></i>
            <span class="line-red"></span>
            info@seedomega.com
         </div>
      </div>
   </article>
</section>
<hr />

<section class="gray">

   <article class="container contactpage">
      <div class="row">
         <div class="col-lg-12">
            <?php echo View::getMessage(); ?>
            <h3>Send Your Message</h3>
            <p>Let us know what's on your mind at the moment, just fill up the form below.</p>
            <div class="alert hide" id="form-message">
               <!-- Form Alert Goes Here -->
            </div>            
            <form class="form-ui" method="post">
            <input type="hidden" name="action" value="contactus">
               <div id="basic-data" class="form-wizard active">
                
                  <div class="form-group">
                     <div class="form-left">
                        <input type="text" value="" class="form-control" placeholder="Full Name" name="mail[FullName]" required="required">
                     </div>
                     <div class="form-right">
                        <input type="email" value="" class="form-control" placeholder="Email" name="mail[Email]" required="required">
                     </div>
                     <div class="clear"></div>
                  </div>
                  <div class="form-group">
                     <div class="form-left">
                        <input type="text" value="" class="form-control" placeholder="Subject" name="mail[Subject]" required="required">
                     </div>
                     <div class="form-right">                        
                        <input type="text" value="" class="form-control" placeholder="Contact Number" name="mail[Phonenumber]" id="number">
                     </div>
                     <div class="clear"></div>
                  </div>
                  <div class="form-group">
                     <textarea name="mail[Message]" class="form-control" placeholder="Your Message"></textarea>
                  </div>

                  <div class="form-group text-center">
                    <!--label class="css-input css-checkbox css-checkbox-info">
                        <input type="checkbox" value="" required="required"> <span></span> I/We Read and Understood the above
                    </label>
                    <br-->
                     <button type="submit" class="btn btn-4 blue default">Send</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>