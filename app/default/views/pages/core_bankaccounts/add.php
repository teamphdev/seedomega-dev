<?php 
View::$title = 'Add Bank Account';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>">C-Bank Accounts</a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="white">
   <article class="container contactpage">
      <div class="row row-header">
         <div class="form-inq col-lg-8">
            <?php echo View::getMessage(); ?>
            <form class="form-ui" method="post">
            <input type="hidden" name="action" value="addbank">
            
               <div id="basic-data" class="form-wizard active">

                  <div class="form-group">
                      <label>Bank Name</label>
                      <input type="text" value="" class="form-control" placeholder="-" name="bank[Name]" required="required">
                     <div class="clear"></div>
                  </div>

                  <div class="form-group">
                     <div class="form-left">
                        <label>Account Name</label>
                        <input type="text" value="" class="form-control" placeholder="-" name="bank[AccountName]" required="required">
                     </div>
                     <div class="form-right">  
                        <label>Account Number</label>
                        <input type="text" value="" class="form-control" placeholder="-" name="bank[AccountNumber]">
                     </div>
                     <div class="clear"></div>
                  </div>

                  <div class="form-group">
                     <div class="form-left">
                        <label>Swift Code</label>
                        <input type="text" value="" class="form-control" placeholder="-" name="bank[SwiftCode]">
                     </div>
                     <div class="form-right">  
                        <label>CCV</label>
                        <input type="text" value="" class="form-control" placeholder="-" name="bank[CCV]">
                     </div>
                     <div class="clear"></div>
                  </div>

                  <div class="form-group">
                    <label>Bank Address</label>
                    <textarea name="bank[Address]" class="form-control" placeholder="-"></textarea>
                  </div>

                  <div class="form-group text-center">
                     <button type="submit" class="btn btn-rounded btn-primary">Submit</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>