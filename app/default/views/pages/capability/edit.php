<?php 
View::$title = 'Edit Capability';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <h3 class="page-title"><?php echo View::$title; ?></h3>
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?> 
                <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="updatecapability" />
                    <input type="hidden" name="capabilityid" value="<?php echo $capability->UserCapabilityID; ?>" />
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Capability Name <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <?php View::form('text', array(
                                    'name'=>'Name',
                                    'value'=>isset($capability->Name) ? $capability->Name : '',
                                    'custom' => 'required',
                                    'class'=>'form-control col-md-7 col-xs-12'
                                )
                            ); ?>  
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                            Capability Group <span class="required">*</span>
                        </label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <?php View::form('selecta', array(
                                    'name'=>'UserCapabilityGroupID',
                                    'value'=>isset($capability->UserCapabilityGroupID) ? $capability->UserCapabilityGroupID : '',
                                    'custom' => 'required',
                                    'options' => $capgroup,
                                    'class'=>'form-control col-md-7 col-xs-12'
                                )
                            ); ?>                                        
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <?php View::form('textarea', array(
                                    'name'=>'Description',
                                    'value'=>isset($capability->Description) ? $capability->Description : '',
                                    'class'=>'form-control col-md-7 col-xs-12'
                                )
                            ); ?> 
                            
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="<?php echo View::url('capability'); ?>" class="btn btn-rounded btn-danger">Back</a>                            
                            <button id="send" type="submit" class="btn btn-rounded btn-success">Save Capability</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>