<?php 
View::$title = 'Capabilities';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<!-- page content -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">All Capability <small></small><a class="btn btn-sm btn-info btn-rounded pull-right" href="<?php echo View::url('capability/add'); ?>">Add Capability</a></h3>
            </div>
            <div class="block">
                <?php echo View::getMessage(); ?> 
                <table id="capability-table" class="table table-divide js-dataTable-full-pagination" width="100%">
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th>ID</th>
                            <th class="no-wrap">Group ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th class="no-sorting text-center" style="max-width:150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        foreach($capabilities as $capability) { $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="a-center"><input type="checkbox" name="table_records" value="<?php echo $capability->UserLevelID; ?>" class="flat"></td> -->
                            
                            <td class="text-center"><?php echo $capability->UserCapabilityID; ?></td>
                            <td><?php echo $capability->GroupName; ?></td>
                            <td><?php echo $capability->Name; ?></td>
                            <td><?php echo $capability->Description; ?></td>
                            <td class="text-center">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'capability/edit/'.$capability->UserCapabilityID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'capability/delete/'.$capability->UserCapabilityID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this capability?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                    
                </table>

            </div>
        </div>

    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>
