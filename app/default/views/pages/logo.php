<div id="logo">
    <?php 
        $sitelogo = View::common()->getUploadedFiles(Option::get('site_logo'));
        $sitelogos = View::common()->getUploadedFiles(Option::get('site_logo_small'));
    ?>
    <a href="<?php echo User::dashboardLink(); ?>" class="site_title" title="<?php echo Config::get('SITE_TITLE'); ?>"><?php View::photo((isset($sitelogo[0]) ? 'files'.$sitelogo[0]->FileSlug : 'images/omegalogo.png'),'',''); ?></a>
    <button type="button" class="collapsed" id="navbar-toggle"><i class="icon fa fa-bars"></i></button>
</div>