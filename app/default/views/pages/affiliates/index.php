<?php
    View::$title = 'Manage Affiliates';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header();
    $userinfo = User::info();
?>
    <!-- <section class="header-bottom">
        <article>
          <div class="container"><h1><?php echo View::$title; ?></h1></div>
        </article>
    </section> -->
    <section class="header-bottom">
        <article>
            <div class="container">
                <h1><?php echo View::$title; ?></h1>
            </div>
        </article>
    </section>

    <!-- page content -->
    <section class="gray">
        <!-- Page Content -->
        <div class="container">
            <?php echo View::getMessage(); ?>
            <!-- Stats -->
            <!-- <div class="content bg-white border-b push-20">
                <div class="row items-push text-uppercase">
                    <div class="col-xs-2 col-sm-2">
                        <div class="font-w700 text-gray-darker animated fadeIn">All Affiliates</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-users"></i> Clients | Seeders</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $allCount; ?></a>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-following"></i> Clients | Seeders</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $approvedCount; ?></a>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <div class="font-w700 text-gray-darker animated fadeIn">Pendings</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-follow"></i> Clients</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $pendingCount; ?></a>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <div class="font-w700 text-gray-darker animated fadeIn">Verification</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-follow"></i> Seeders</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $verificationCount; ?></a>
                    </div>
                    <div class="col-xs-2 col-sm-2">
                        <div class="font-w700 text-gray-darker animated fadeIn">Incomplete</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-unfollow"></i> Seeders</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $incompleteCount; ?></a>
                    </div>
                </div>
            </div> -->
            <!-- END Stats -->

            <!-- Dynamic Table Full Pagination -->
            <div class="block">
                <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">

                    <li class="active">
                        <a href="#affiliates-Client">Clients <span class="badge badge-success"><?php echo $clientcount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#affiliates-User">Seeders <span class="badge badge-success"><?php echo $seederscount; ?></span></a>
                    </li>

                    <!-- <li class="active">
                        <a href="#affiliates-Approved">Approved <span class="badge badge-success"><?php echo $approvedCount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#affiliates-Pending">Pending <span class="badge badge-warning"><?php echo $pendingCount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#affiliates-Verification">Verification <span class="badge badge-info"><?php echo $verificationCount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#affiliates-Incomplete">Incomplete <span class="badge badge-danger"><?php echo $incompleteCount; ?></span></a>
                    </li> -->
                </ul>

                <div class="block tab-content">

                    <!-- affiliates -->
                    <?php
                    if( isset( $affiliates ) ){ if( count( $affiliates ) ){
                        foreach( $affiliates as $ks => $affs ){ ?>
                        <div class="tab-pane fade fade-up<?php echo isset( $ks ) && $ks == 'Client' ? ' active in' : ''; ?>" id="affiliates-<?php echo isset( $ks ) ? $ks : ''; ?>">
                            <table class="table table-divide js-dataTable-full-pagination dt-responsive table-hover table-vcenter" width="100%">
                                <thead>
                                <tr class="headings">
                                    <th>Photo</th>
                                    <th class="text-center no-wrap"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                                    <th class="text-center"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                                    <th class="text-center"><?php echo Lang::get('USR_MNG_EML'); ?></th>
                                    <?php if( isset( $ks ) && $ks == 'Client' ){ ?>
                                        <th class="text-center">Company</th>
                                    <?php } ?>
                                    <th class="text-center" name="CommissionRate"><?php echo Lang::get('USR_MNG_COMMI_RATE'); ?></th>
                                    <th class="text-center" name="SubscriptiomRate"><?php echo Lang::get('USR_MNG_SUBS_RATE'); ?></th>
                                    <th class="text-center">Exemption</th>
                                    <?php if( isset( $ks ) && $ks != 'Client' ){ ?>
                                        <th class="text-center"><?php echo Lang::get('USR_MNG_LVL'); ?></th>
                                    <?php } ?>                                    
                                    <th class="no-sorting text-center" style="width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $cntr = 0;
                                if( isset( $affs ) && count( $affs ) ){
                                    foreach( $affs as $user ){ $cntr++; ?>
                                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                            <td class="text-center"><?php echo $user->Data['AvatarLink']; ?></td>
                                            <td><?php echo $user->UserID; ?></td>
                                            <td><?php echo $user->LastName; ?>, <?php echo $user->FirstName; ?></td>
                                            <td class="text-small"><?php echo $user->Email; ?></td>
                                            <?php if( isset( $user->Name ) && $user->Name == 'Client' ){ ?>
                                                <td class="text-center"><?php echo $user->CompanyName; ?></td>
                                            <?php } ?>
                                            <td class="text-center"><?php echo isset( $user->CommissionRate ) ? $user->CommissionRate : '0'; ?></td>
                                            <td class="text-center"><?php echo isset( $user->SubscriptionRate ) ? $user->SubscriptionRate : '0'; ?></td>
                                            <td class="text-center"><?php echo isset( $user->Exemption ) && $user->Exemption == '1' ? 'Yes' : 'No'; ?></td>                                            
                                            <?php if( isset( $user->Name ) && $user->Name == 'Seeder' ){ ?>
                                                <td><?php echo $user->Name; ?></td>
                                            <?php } ?>
                                            <td class="text-center">
                                                <div class="">
                                                    <a href="javascript:void(0);" class="edit-commission-rate btn btn-sm btn-default btn-rounded" type="button" data-toggle="tooltip" data-name="<?php echo $user->FirstName.' '.$user->LastName; ?>" data-userid="<?php echo $user->UserID; ?>" data-level="<?php echo $user->Name; ?>" data-commrate="<?php echo $user->CommissionRate; ?>" data-subsrate="<?php echo $user->SubscriptionRate; ?>" data-exempt="<?php echo $user->Exemption; ?>" data-refid="<?php echo $user->ReferrerUserID; ?>" onclick="editCommissionRate(this)"><i class="fa fa-edit"></i> Edit Rate</a>
                                                </div>
                                                <?php /*
                                                <div title="Edit Commission Rate" class="edit-commission-rate btn btn-xs btn-default" data-toggle="tooltip" data-name="<?php echo $user->FirstName.' '.$user->LastName; ?>" data-userid="<?php echo $user->UserID; ?>" data-level="<?php echo $user->Name; ?>" data-commrate="<?php echo $user->CommissionRate; ?>" data-subsrate="<?php echo $user->SubscriptionRate; ?>" data-exempt="<?php echo $user->Exemption; ?>" data-refid="<?php echo $user->ReferrerUserID; ?>" onclick="editCommissionRate(this)"><i class="fa fa-pencil"></i></div>
                                                */ ?>
                                            </td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                        <td colspan="9">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } } } else { ?>
                        <div class="tab-pane">No Data!</div>
                    <?php } ?>
                    <!-- END affiliates -->

                </div>

            </div>
        </div>

        <div id="dialog" class="w3-modal modal-content">
            <div class="w3-modal-content block block-themed remove-margin-b" style="width:450px;">
                <div id="dialog-form" class="text-center">
                    <button type="button" class="close" aria-label="Close" onclick="document.getElementById('dialog').style.display='none'">
                        <i class="si si-close text-red"></i>
                    </button>
                    <hr>
                    <div class="modal-body heading"><h3 id="Name"></h3></div>
                </div>
                <form enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="editcommrate" />
                    <input type="hidden" name="Level" value="" id="Level" />
                    <input type="hidden" name="rate[UserID]" value="" id="UserID" />
                    <input type="hidden" name="rate[ReferrerUserID]" value="" id="ReferrerUserID" />

                    <div id="dialog-form" class="block-content text-center comm-rate">

                        <div class="form-group">

                            <div class="row push-10">
                                <div class="col-xs-5 col-sm-5 rate-name"><label><?php echo Lang::get('CLN_CS_RATE'); ?>:</label></div>
                                <div class="col-xs-6 col-sm-6 text-left">
                                    <input type="text" value="" id="CommissionRate" name="rate[CommissionRate]" class="form-control" pattern="^[0-9]\d*(\.\d+)?$" step="1" required>
                                </div>
                            </div>

                            <div class="row push-10">
                                <div class="col-xs-5 col-sm-5 rate-name"><label><?php echo Lang::get('CLN_SS_RATE'); ?>:</label></div>
                                <div class="col-xs-6 col-sm-6 text-left">
                                    <input type="text" value="" id="SubscriptionRate" name="rate[SubscriptionRate]" class="form-control" pattern="^[0-9]\d*(\.\d+)?$" step=".01" required>
                                    <small class="text-muted">must be positive(+) value</small>
                                </div><br>
                            </div>

                            <hr class="push-10">

                            <div class="row">
                                <div class="col-xs-5 col-sm-5 rate-name"><label>Exemption:</label></div>
                                <div class="col-xs-6 col-sm-6 text-left">
                                    <?php
                                        $exemptOptions = array( 0 => 'No', 1 => 'Yes' );
                                        View::form( 'selecta', array( 'name' => 'rate[Exemption]', 'options' => $exemptOptions, 'id' => 'Exemption', 'class' => 'form-control' ) );
                                    ?>
                                </div>
                            </div><br>
                        
                            <div class="text-center clear">
                                <button class="btn btn-rounded btn-primary" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Save</button>
                            </div>
                        </div>

                    </div>
                </form>
                <div id="dialog-form" class="text-center">
                    <div class="text-reminder text-white">Update Commission/Subscription Rate</div>
                </div>
            </div>
        </div>

    </section>
    <!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){

        // $('.edit-commission-rate').click( function(){
        //     var name = $( this ).attr( 'data-name' );
        //     var level = $( this ).attr( 'data-level' );
        //     var userid = $( this ).attr( 'data-userid' );
        //     var commrate = $( this ).attr( 'data-commrate' );

        //     $( '#Name' ).text( name );
        //     $( '#Level' ).val( level );
        //     $( '#UserID' ).val( userid );
        //     $( '#CommissionRate' ).val( commrate );

        //     $( '#dialog' ).css( 'display', 'block' );
        //     // var diaHeight = $( '#dialog' ).height();
        //     // var refTop = $( '#popular' ).offset().top;
        //     // $( '#dialog' ).css( 'padding-top', ( refTop - 85 ) + 'px' );
        // });

    });

    function editCommissionRate( el ){
        var name = $( el ).attr( 'data-name' );
        var level = $( el ).attr( 'data-level' );
        var userid = $( el ).attr( 'data-userid' );
        var commrate = $( el ).attr( 'data-commrate' );
        var subsrate = $( el ).attr( 'data-subsrate' );
        var exempt = $( el ).attr( 'data-exempt' );
        var refid = $( el ).attr( 'data-refid' );

        $( '#Name' ).text( name );
        $( '#Level' ).val( level );
        $( '#UserID' ).val( userid );
        $( '#CommissionRate' ).val( commrate == 0 ? 25 : commrate );
        $( '#SubscriptionRate' ).val( subsrate );
        $( '#ReferrerUserID' ).val( refid );
        $( '#Exemption option[ value="' + exempt + '"]' ).prop( "selected", true );

        $( '#Exemption' ).prop( 'disabled', false );
        if( level == 'Client' ){
            $( '#Exemption' ).prop( 'disabled', true );
        }

        $( '#dialog' ).css( 'display', 'block' );
    }
</script>