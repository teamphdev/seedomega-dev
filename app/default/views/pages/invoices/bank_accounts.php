<?php
    View::$title = 'Manage Client Bank Accounts';
    View::$bodyclass = '';
    View::header();
?>
<?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page">
        <?php echo View::getMessage(); ?>
        <!-- Stats -->
<!--         <div class="content bg-white border-b push-20">
            <div class="row items-push text-uppercase">
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">All Withdrawals</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $allWithdrawalCount; ?></a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-clock"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $approvedCount; ?></a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Paid</div>
                    <div class="text-muted animated fadeIn"><small><i class="si si-check"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $paidCount; ?></a>
                </div>
            </div>
        </div> -->
        <!-- END Stats -->

        <div class="block">
            <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                <li class="<?php echo $allclient; ?>">
                    <a href="#clients-all">All Client Profiles <span class="badge"><?php echo $allClientsCount; ?></span></a>
                </li>
                <li class="<?php echo $request; ?>">
                    <a href="#clients-request">Change Request <span class="badge badge-warning"><?php echo $pendingClientRequestsCount; ?></span></a>
                </li>
            </ul>
            <div class="block tab-content">
                <!-- All clients -->
                <div class="tab-pane fade fade-up in <?php echo $allclient; ?>" id="clients-all">
                    <table class="table table-divide table-hover table-vcenter clientprofiles js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                            <tr>
                                <th class="text-center" style="min-width:550px;"><i class="fa fa-suitcase text-gray"></i> Projects</th>
                                <th class="text-center" style="width:15%;"><i class="fa fa-ticket text-gray"></i> Status</th>
                                <th class="text-center" style="width:15%;"><i class="fa fa-money text-gray"></i> Investment</th>
                                <th class="text-center" style="min-width:100px;"><i class="fa fa-gavel text-gray"></i> Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( count( $allClients ) ){
                            foreach( $allClients as $cp ):
                                $startdate = date_create();
                                $closedate = date_create();
                                $interval = 1;
                                switch( $cp->Status ){
                                    case 'Completed':
                                        $statusfield = '<span class="label label-success"><i class="si si-check"></i> '.$cp->Status.'</span>';
                                        break;
                                    case 'Approved':
                                        $statusfield = '<span class="label label-success"><i class="si si-check"></i> '.$cp->Status.'</span>';
                                        break;
                                    case 'Pending':
                                    default:
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$cp->Status.'</span>';
                                        break;
                                    case 'Verified':
                                        $statusfield = '<span class="label label-info"><i class="si si-clock"></i> '.$cp->Status.'</span>';
                                        break;
                                } ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/cs/clientprofiles/view/<?php echo $cp->ClientProfileID;?>"><?php echo isset( $cp->CompanyName ) ? $cp->CompanyName: "-"; ?> </a>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <?php echo $statusfield; ?>
                                        </div>

                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?> <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                </div>
                                                <?php /*
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span>$<?php echo isset( $cp->SizeOfOffer ) ? str_ireplace( '$', '', $cp->SizeOfOffer ) : "-"; ?> <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                </div>
                                                */ ?>
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span><?php echo isset( $cp->TargetGoal ) ? "$".number_format( $cp->TargetGoal ) : "-"; ?> <br><small class="text-muted">TARGET GOAL</small></span>
                                                </div>
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span><?php echo isset( $cp->Price ) ? "$".number_format( $cp->Price, 2 ) : "-"; ?> <br><small class="text-muted">PRICE</small></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td>
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $cp->TotalRaisedAmt ) ? "$".number_format( $cp->TotalRaisedAmt, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-center text-primary hidden-xs">
                                        <div class="">
                                            <a href="/clients/profile/bank-accounts/<?php echo $cp->ClientProfileID; ?>" class="btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="">Edit Bank Profile</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END All clients -->

                <!-- Change Request -->
                <div class="tab-pane fade fade-up in <?php echo $request; ?>" id="clients-request">
                    <table class="table table-divide table-hover table-vcenter dt-responsive js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center hidden-xs" style="width:30%;"><i class="fa fa-suitcase text-gray"></i> Projects</th>
                            <th class="text-center hidden-xs hidden-sm no-wrap"><i class="si si-drawer text-gray"></i> Type Of Offer</th>
                            <th class="text-center hidden-xs hidden-sm"><i class="si si-note text-gray"></i> Notes</th>
                            <th class="text-center hidden-xs hidden-sm no-wrap" style="width:15%;"><i class="fa fa-calendar text-gray"></i> Request Date</th>
                            <th class="text-center hidden-xs hidden-sm" style="max-width:150px;"><i class="fa fa-gavel text-gray"></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( count( $pendingClientRequests ) ){
                            foreach( $pendingClientRequests as $cp ):
                                $statusfield = '<small class="text-danger">Please approve this Bank Account Profile change request!</small>'; ?>

                                <tr>
                                    <td class="">
                                        <span class="font-w600"><?php echo isset( $cp->CompanyName ) ? $cp->CompanyName : "-"; ?></span><br>
                                        <table class="table table-borderless visible-xs visible-sm  font-s13">
                                            <tbody>
                                            <tr>
                                                <td class="font-w600" style="width: 30%;">Type</td>
                                                <td><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="font-w600">Notes</td>
                                                <td><?php echo $statusfield; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="hidden-xs hidden-sm">
                                        <small><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?></small>
                                    </td>
                                    <td class="hidden-xs hidden-sm">
                                        <?php echo $statusfield; ?>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo isset( $cp->RequestDate ) && $cp->RequestDate != "0000-00-00" ? date( 'j M Y', strtotime( $cp->RequestDate ) ) : "-"; ?>
                                    </td>
                                    <td class="text-center hidden-xs hidden-sm">
                                        <div class="">
                                            <a href="/clients/changeprofile/bank-accounts/<?php echo $cp->ClientProfileID; ?>" class="btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="">Approve Bank Profile</a>
                                        </div>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Change Request -->
            </div>
        </div>

    </article>

</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
    });
</script>