<?php
View::$title = "Client's TT Receipt Confirmation";
View::$bodyclass = '';
View::header();
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink();?>">Home</a></li>
            <?php if( isset( View::$segments[1] ) ){ ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url( View::$segments[0]); ?>"><?php echo View::$segments[0];?></a></li>
            <?php } ?>
            <?php if( isset( View::$segments[2] ) ){ ?>
              <li class="fa fa-angle-right"></li>
              <li><?php echo View::$title;?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <div class="content content-boxed">
        <!-- Invoice -->
        <div class="block">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <!-- <a href="/invoices/goprint/<?PHP echo $invoice->InvoiceID;?>" target="_blank"><i class="si si-printer"></i> Print Invoice</a> -->
                    </li>
                </ul>
                <h3 class="block-title">INVOICE # <?PHP echo $invoice[0]->InvoiceID;?></h3>
            </div>

            <!-- Main Content -->
            <div class="start-content">
                <?php echo View::getMessage(); ?>
                <div id="my-profile" class="content bg-white border-b push-20">
                    <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="action" value="updateprofile" />
                        <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                        <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                        <input type="hidden" name="acctid" value="<?php echo $profile->AccountID; ?>" />
                        <input type="hidden" name="IdPhotoID" value="<?php echo $profile->IdPhoto; ?>" />
                        <input type="hidden" name="AddressPhotoID" value="<?php echo $profile->AddressPhoto; ?>" />

                        <div class="form-group">
                            <div class="form-left">
                                <label>Invoice Date</label>
                                <input type="text" value="<?php echo isset( $invoice[0]->InvoiceDate ) ? $invoice[0]->InvoiceDate : ''; ?>" class="form-control" readonly>
                            </div>
                            <div class="form-right">
                                <label>Due Date</label>
                                <input type="text" value="<?php echo isset( $invoice[0]->DueDate ) ? $invoice[0]->DueDate : ''; ?>" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-left">
                                <label>Amount</label>
                                <input type="text" value="$<?php echo isset( $invoice[0]->Total ) ? number_format( $invoice[0]->Total, 2 ) : '0.00'; ?>" class="form-control" readonly>
                            </div>
                            <div class="form-right">
                                <label>Status</label>
                                <input type="text" value="<?php echo isset( $invoice[0]->Status ) ? $invoice[0]->Status : ''; ?>" class="form-control" readonly>
                            </div>
                        </div>

                        <?php echo AppUtility::getInvoiceTTReceipt( $invoice[0] ); ?>

                        <div class="text-center push-30">
                            <?php 
                            switch (User::info('UserLevel')) {
                                case 'Client':
                                    $redirectlink = View::url('clients/invoices');
                                    break;

                                case 'Accounting':
                                    $redirectlink = View::url('invoices');
                                    break;

                                 default:
                                    $redirectlink = View::url('invoices');
                                    break;
                            } ?>
                            <a href="<?php echo $redirectlink; ?>" class="btn btn-rounded btn-danger">Go Back <span class="si si-action-undo"></span></a>
                        </div>

                        <div class="ln_solid"></div>

                    </form>
                </div>
            </div>
            <!-- END Main Content -->

        </div>
        <!-- END Invoice -->
    </div>
</section>

<?php View::footer(); ?>