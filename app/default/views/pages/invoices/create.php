<?php
View::$title = 'Create New Invoice';
View::$bodyclass = '';
View::header();
?>
    <section class="header-bottom">
        <article>
            <div class="container"><h1><?php echo View::$title; ?></h1></div>
        </article>
    </section>

    <section class="breadcrumb">
        <article class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul>
                        <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                        <li><a href="<?php echo View::url(); ?>">Home</a></li>
                        <li class="fa fa-angle-right"></li>
                        <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo View::$title; ?></li>
                    </ul>
                </div>
                <div class="col-lg-6 align-right sub-menu">
                </div>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
    <!-- Page Content -->
    <div class="content">
        <div class="row">

            <div class="col-sm-6 col-md-4 col-lg-4 col-lg-offset-4">
                <div class="block">
                    <div class="block-header">
                        <ul class="block-options">

                        </ul>
                        <h3 class="block-title">Create New Invoice</h3>
                    </div>
                    <div class="block-content block-content-narrow">
                        <form class="form-horizontal" method="get" id="frmCreateInvoice">
                            <div class="form-material">
                                <label>Select a Client:</label>
                                <select class="form-control" id="selClient">
                                    <option value="">-select-</option>
                                    <?php foreach( $clients as $cl ) : ?>
                                        <?php if( $cl->FirstName ) : ?>
                                        <option value="<?php echo $cl->ClientProfileID; ?>"><?php echo $cl->FirstName . ' ' . $cl->LastName; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                                <br/>
                                <br/>
                            </div>

                            <div class="form-material">
                                <label>Select a Booking:</label>
                                <select class="form-control" id="selBooking">
                                    <option value="">-select-</option>
                                </select>
                                <br/>
                            </div>

                            <div class="form-material">
                                <button class="btn btn-primary" type="submit" id="btnCreateInv">Create Invoice</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- END Page Content -->
    </section>

<?php View::footer(); ?>