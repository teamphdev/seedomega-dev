<?php
View::$title = 'Invoices';
View::$bodyclass = ' acct-invoices';
View::header();
?>
<?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page">
        <?php echo View::getMessage(); ?>
        <!-- Stats -->
        <div class="content bg-white border-b push-20">
            <div class="row items-push text-uppercase">
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">All Invoices</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo count( $invoices ); ?></a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Paid</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-check"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo count( $invoicesPaid );?></a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">TT Receipt</div>
                    <div class="text-muted animated fadeIn"><small><i class="si si-clock"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo count( $invoicesTTReceiptAll );?></a>
                </div>
            </div>
        </div>
        <!-- END Stats -->
        
        <div class="block">
            <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                <li class="active">
                    <a href="#invoice-outstanding">Outstanding <span class="badge badge-warning"><?php echo count( $invoicesOutstanding ); ?></span></a>
                </li>
                <li class="">
                    <a href="#invoice-overdue">Overdue <span class="badge badge-danger"><?php echo count( $invoicesOverdue ); ?></span></a>
                </li>
                <li class="">
                    <a href="#invoice-paid">Paid <span class="badge badge-success"><?php echo count( $invoicesPaid ); ?></span></a>
                </li>
            </ul>
            <div class="block tab-content">

                <!-- Invoices Outstanding-->
                <div class="tab-pane fade fade-up in active" id="invoice-outstanding">
                    <?php /*
                    <div class="form-group">
                        <label for="">Company Name:</label>
                        <div class="form-left inline-block">
                            <div id="filter-company-out"></div>
                        </div>
                        <div class="form-right inline-block">
                            <input type="button" onclick="yadcf.exResetAllFilters( outstandingTable );" value="Reset" class="btn btn-default">
                        </div>
                        <div class="clear"></div>
                    </div>
                    */ ?>
                    <table id="tab-outstanding" class="table table-divide table-hover table-vcenter" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center">Invoice</th>
                            <th class="text-center">Project</th>
                            <!-- <th class="text-center">Investor</th> -->
                            <th class="text-center">Invested</th>
                            <th class="text-center no-wrap">Subscription Rate</th>
                            <th class="text-center">Notes</th>
                            <th class="text-center">Amount</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <!-- <th></th> -->
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        if( isset( $invoicesOutstanding ) && count( $invoicesOutstanding ) ){
                            foreach( $invoicesOutstanding as $inv ):

                                $notes = '';
                                $startdate = '';
                                $closedate = '';
                                $interval = '';
                                $uploadtt = false;
                                $docshref = 'invoices/receipt/' . $inv->InvoiceID;

                                if( isset ( $inv->TTReceipt ) && $inv->TTReceipt > 0 ){
                                    $uploadtt = true;
                                    $notes = '<span class="label label-warning"><i class="si si-clock"></i> TT Receipt uploaded</span>';
                                    if( isset( $inv->Active ) ){
                                        if( $inv->Active == 1 ){                                            
                                            $notes = '<span class="label label-success"><i class="fa fa-check"></i> TT Receipt uploaded</span>';
                                        }
                                    }
                                }

                                switch( $inv->Status ){
                                    case 'Paid':
                                        $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Paid</span>';
                                        break;
                                    case 'Outstanding':
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$inv->Status.'</span>';
                                        break;
                                    case 'TT Receipt':
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> Outstanding</span>';
                                        break;
                                } ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?></a>
                                            <br><?php echo $statusfield; ?>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <div class="project-info">
                                                <small><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> - <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></small>
                                            </div>
                                        </div>
                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                                    <span><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> - <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <?php echo isset( $inv->CompanyName ) ? $inv->CompanyName : ''; ?>
                                    </td>
                                    <!-- <td class="h5 text-left hidden-xs">
                                        <?php echo isset( $inv->Investor ) ? $inv->Investor : ''; ?>
                                    </td> -->
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $inv->InvestmentAmount ) ? number_format( $inv->InvestmentAmount, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-right hidden-xs">
                                        <?php echo isset( $inv->SubscriptionRate ) ? number_format( $inv->SubscriptionRate, 2 ) : '0.50'; ?>%
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $notes; ?>
                                    </td>
                                    <!-- <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td> -->
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $inv->Total ) ? '$'.number_format( $inv->Total, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo $uploadtt ? $docshref : 'javascript:void(0);'; ?>" class="<?php echo $uploadtt ? '' : ' disabled'; ?>" data-toggle="tooltip" title=""><i class="fa fa-image<?php echo $uploadtt ? '' : ' text-gray'; ?> fa-1x pull-right"></i> View TT Receipt</a></li>
                                                    <li><a href="<?php echo View::url( 'invoices/view/'.$inv->InvoiceID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-eye pull-right"></i> View Details</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="7">No Data</td>
                                <td class="hidden"></td>
                                <!-- <td class="hidden"></td> -->
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END Invoices Outstanding-->

                <!-- Invoices Overdue -->
                <div class="tab-pane fade fade-up in" id="invoice-overdue">
                    <?php /*
                    <div class="form-group">
                        <label for="">Company Name:</label>
                        <div class="form-left inline-block">
                            <div id="filter-company-over"></div>
                        </div>
                        <div class="form-right inline-block">
                            <input type="button" onclick="yadcf.exResetAllFilters( overdueTable );" value="Reset" class="btn btn-default">
                        </div>
                        <div class="clear"></div>
                    </div>
                    */ ?>
                    <table id="tab-overdue" class="table table-divide table-hover table-vcenter" style="width:100%;">
                        <thead>
                            <tr>
                                <th class="text-center">Invoice</th>
                                <th class="text-center hidden-xs">Project</th>
                                <!-- <th class="text-center hidden-xs">Investor</th> -->
                                <th class="text-center hidden-xs">Invested</th>
                                <th class="text-center no-wrap">Subscription Rate</th>
                                <th class="text-center hidden-xs">Notes</th>
                                <!-- <th class="text-center hidden-xs">Status</th> -->
                                <th class="text-center hidden-xs hidden-sm">Amount</th>
                                <th class="text-center" style="min-width: 90px;">Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <!-- <th></th> -->
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        if( isset( $invoicesOverdue ) && count( $invoicesOverdue ) ){
                            foreach( $invoicesOverdue as $inv ):

                                $notes = '';
                                $startdate = '';
                                $closedate = '';
                                $interval = '';
                                $uploadtt = false;
                                $docshref = 'invoices/receipt/' . $inv->InvoiceID;

                                if( isset ( $inv->TTReceipt ) && $inv->TTReceipt > 0 ){
                                    $uploadtt = true;
                                    $notes = '<span class="label label-warning"><i class="si si-clock"></i> TT Receipt uploaded</span>';
                                    if( isset( $inv->Active ) ){
                                        if( $inv->Active == 1 ){                                            
                                            $notes = '<span class="label label-success"><i class="fa fa-check"></i> TT Receipt uploaded</span>';
                                        }
                                    }
                                }

                                switch( $inv->Status ){
                                    case 'Paid':
                                        $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Paid</span>';
                                        break;
                                    case 'Outstanding':
                                        $statusfield = '<span class="label label-danger"><i class="si si-clock"></i> Overdue</span>';
                                        break;
                                    case 'TT Receipt':
                                        $statusfield = '<span class="label label-danger"><i class="si si-clock"></i> Overdue</span>';
                                        break;
                                }
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?> </a>
                                            <br><?php echo $statusfield; ?>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <div class="project-info">
                                                <small><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> - <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></small>
                                            </div>
                                        </div>
                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                                    <span><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> - <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></span>
                                                </div>

                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <?php echo isset( $inv->CompanyName ) ? $inv->CompanyName : ''; ?>
                                    </td>
                                    <!-- <td class="h5 text-left hidden-xs">
                                        <?php echo isset( $inv->Investor ) ? $inv->Investor : ''; ?>
                                    </td> -->
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $inv->InvestmentAmount ) ? number_format( $inv->InvestmentAmount, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-right hidden-xs">
                                        <?php echo isset( $inv->SubscriptionRate ) ? number_format( $inv->SubscriptionRate, 2 ) : '0.50'; ?>%
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $notes; ?>
                                    </td>
                                    <!-- <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td> -->
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $inv->Total ) ? "$".number_format( $inv->Total, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a href="<?php echo $uploadtt ? $docshref : 'javascript:void(0);'; ?>" class="<?php echo $uploadtt ? '' : ' disabled'; ?>" data-toggle="tooltip" title=""><i class="fa fa-image<?php echo $uploadtt ? '' : ' text-gray'; ?> fa-1x pull-right"></i> View TT Receipt</a></li>
                                                    <li><a href="<?php echo View::url( 'invoices/view/'.$inv->InvoiceID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-eye pull-right"></i> View Details</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="7">No Data</td>8
                                <td class="hidden"></td>
                                <!-- <td class="hidden"></td> -->
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Invoices Overdue-->

                <!-- Invoices Paid -->
                <div class="tab-pane fade fade-up in" id="invoice-paid">
                    <?php /*
                    <div class="form-group">
                        <label for="">Company Name:</label>
                        <div class="form-left inline-block">
                            <div id="filter-company-paid"></div>
                        </div>
                        <div class="form-right inline-block">
                            <input type="button" onclick="yadcf.exResetAllFilters( paidTable );" value="Reset" class="btn btn-default">
                        </div>
                        <div class="clear"></div>
                    </div>
                    */ ?>
                    <table id="tab-paid" class="table table-divide table-hover table-vcenter" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center"><!-- <i class="fa fa-suitcase text-gray"></i> --> Invoice</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bank text-gray"></i> --> Project</th>
                            <!-- <th class="text-center hidden-xs">Investor</th> -->
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> Invested</th>
                            <th class="text-center no-wrap"><!-- <i class="fa fa-bar-chart-o text-gray"></i> --> Subscription Rate</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-ticket text-gray"></i> --> Notes</th>
                            <!-- <th class="text-center hidden-xs"><i class="fa fa-ticket text-gray"></i> Status</th> -->
                            <th class="text-center hidden-xs hidden-sm"><!-- <i class="fa fa-money text-gray"></i> --> Amount</th>
                            <th class="text-center" style="min-width: 90px;"><!-- <i class="fa fa-wrench text-gray"></i> --> Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <!-- <th></th> -->
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        if( isset( $invoicesPaid ) && count( $invoicesPaid ) ){
                            foreach( $invoicesPaid as $inv ):

                                $notes = '';
                                $startdate = '';
                                $closedate = '';
                                $interval = '';

                                if( isset ( $inv->TTReceipt ) && $inv->TTReceipt > 0 ){
                                    $notes = '<span class="label label-warning"><i class="si si-clock"></i> TT Receipt uploaded</span>';
                                    if( isset( $inv->Active ) ){
                                        if( $inv->Active == 1 ){                                            
                                            $notes = '<span class="label label-success"><i class="fa fa-check"></i> TT Receipt uploaded</span>';
                                        }
                                    }
                                }

                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Paid</span>';
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?> </a>
                                            <br><?php echo $statusfield; ?>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <div class="project-info">
                                                <small><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> - <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></small>
                                            </div>
                                        </div>
                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                                    <span><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> - <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></span>
                                                </div>

                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <?php echo isset( $inv->CompanyName ) ? $inv->CompanyName : ''; ?>
                                    </td>
                                    <!-- <td class="h5 text-left hidden-xs">
                                        <?php echo isset( $inv->Investor ) ? $inv->Investor : ''; ?>
                                    </td> -->
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $inv->InvestmentAmount ) ? number_format( $inv->InvestmentAmount, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-right hidden-xs">
                                        <?php echo isset( $inv->SubscriptionRate ) ? number_format( $inv->SubscriptionRate, 2 ) : '0.50'; ?>%
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $notes; ?>
                                    </td>
                                    <!-- <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td> -->
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $inv->Total ) ? "$".number_format( $inv->Total, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="">
                                            <a href="/invoices/view/<?php echo $inv->InvoiceID;?>" class="btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="">View Details</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="7">No Data</td>
                                <td class="hidden"></td>
                                <!-- <td class="hidden"></td> -->
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Invoices Paid-->

            </div>
        </div>

    </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    var outstandingTable;
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });

        outstandingTable = $("#tab-outstanding").dataTable({
           // "lengthMenu": [ 10, [25, 50, 75, 100, -1], [25, 50, 75, 100, "All"] ],
           "pageLength": 10,
           "searchCols": [ null, null, null, null, null, null, null ]
        }).yadcf([
           {column_number : 0, filter_type: 'none'},
           {column_number : 1, filter_type: 'select', filter_container_selector: '#filter-outstanding', filter_default_label: "SELECT PROJECT" },
           {column_number : 2, filter_type: 'none'},
           {column_number : 3, filter_type: 'none'},
           {column_number : 4, filter_type: 'none'},
           {column_number : 5, filter_type: 'none'},
           {column_number : 6, filter_type: 'none'}
       ], 'footer');

        overdueTable = $("#tab-overdue").dataTable({
           "pageLength": 10,
           "searchCols": [ null, null, null, null, null, null, null ]
        }).yadcf([
           {column_number : 0, filter_type: 'none'},
           {column_number : 1, filter_type: 'select', filter_container_selector: '#filter-overdue', filter_default_label: "SELECT PROJECT" },
           {column_number : 2, filter_type: 'none'},
           {column_number : 3, filter_type: 'none'},
           {column_number : 4, filter_type: 'none'},
           {column_number : 5, filter_type: 'none'},
           {column_number : 6, filter_type: 'none'}
       ], 'footer');

        paidTable = $("#tab-paid").dataTable({
           "pageLength": 10,
           "searchCols": [ null, null, null, null, null, null, null ]
        }).yadcf([
           {column_number : 0, filter_type: 'none'},
           {column_number : 1, filter_type: 'select', filter_container_selector: '#filter-paid', filter_default_label: "SELECT PROJECT" },
           {column_number : 2, filter_type: 'none'},
           {column_number : 3, filter_type: 'none'},
           {column_number : 4, filter_type: 'none'},
           {column_number : 5, filter_type: 'none'},
           {column_number : 6, filter_type: 'none'}
       ], 'footer');

        var tags = '<div class="form-group inline-block"><div class="form-left inline-block"><div id="filter-xx"></div></div><div class="form-right inline-block"><input type="button" onclick="yadcf.exResetAllFilters( xxTable );" value="Reset" class="btn btn-default"></div><div class="clear"></div></div>';

        // outstanding invoices
        var outstanding = tags.replace( /xx/g, 'outstanding');
        $( '#tab-outstanding_length' ).addClass( 'no-wrap' );
        $( '#tab-outstanding_length label' ).addClass( 'inline-block' );
        $( '#tab-outstanding_length label' ).after( outstanding );
        yadcf.exResetAllFilters( outstandingTable );

        // overdue invoices
        var overdue = tags.replace( /xx/g, 'overdue');
        $( '#tab-overdue_length' ).addClass( 'no-wrap' );
        $( '#tab-overdue_length label' ).addClass( 'inline-block' );
        $( '#tab-overdue_length label' ).after( overdue );
        yadcf.exResetAllFilters( overdueTable );

        // paid invoices
        var paid = tags.replace( /xx/g, 'paid');
        $( '#tab-paid_length' ).addClass( 'no-wrap' );
        $( '#tab-paid_length label' ).addClass( 'inline-block' );
        $( '#tab-paid_length label' ).after( paid );
        yadcf.exResetAllFilters( paidTable );

    });
</script>