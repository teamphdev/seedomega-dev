<?php
View::$title = 'Invoices';
View::$bodyclass = '';
View::header();
?>

<section class="gray">
    <div class="container view invoice w1024 push-10-t">
        <!-- Invoice -->
        <div class="block centered">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <div>
                            <?php $wallet = isset( $mybalance ) ? '$'.number_format( $mybalance, 2 ) : '$0.00';
                            $wallet = str_ireplace( '$-', '-$', $wallet );
                            if( $hidepay == '' ){ ?>
                            Running Balance: <a href="/wallet"><label id="running-balance" class="block-title font-w300 white animated flipInX pointer"><b><?php echo $wallet; ?></b></label></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <?php } ?>
                            <a href="/invoices/goprint/<?php echo isset( $invoice[0]->InvoiceID ) ? $invoice[0]->InvoiceID : ''; ?>" target="_blank"><i class="si si-printer"></i> Print Invoice</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="block-content">

                <!-- Invoice Info -->
                <div class="h1 text-center push-30-t push-30">
                    <a href="javascript:void();" data-toggle="modal" data-target="#singlepay-modal" class="btn btn-info btn-rounded inline text-uppercase pull-right <?php echo $hidepay; ?>" title="Pay" id="process-pay"><i class="si si-wallet"></i> Pay Now</a>
                </div>

                <div class="row">
                    <!-- Client Info -->
                    <div class="col-xs-6 col-sm-6 col-lg-6">
                        <div class="sologo"><?php echo $logo; ?></div>
                        <div class="push-20-t">
                            <b>Bill To:</b>
                            <p class="h4 push-5-t push-0"><?php echo isset( $invoice[0]->CompanyName ) ? $invoice[0]->CompanyName : ''; ?></p>
                            <!-- <p class="h3 font-w400 push-5"><?php echo isset( $invoice->FirstName ) ? $invoice->FirstName : ''; ?><?php echo isset( $invoice->LastName ) ? ' '.$invoice->LastName : ''; ?></p> -->
                            <address>
                                <i class="fa fa-map-marker"></i> <small class="text-muted"><?php echo ( isset( $invoice[0]->Address ) ? $invoice[0]->Address : '' ) . ( isset( $invoice[0]->Address2 ) ? ' '.$invoice[0]->Address2 : '' ); ?>
                                <?php echo ( isset( $invoice[0]->City ) ? $invoice[0]->City.', ' : '' ) . ( isset( $invoice[0]->State ) ? $invoice[0]->State : '' ); ?></small><br>
                                <div class="push-15-l"><small class="text-muted"><?php echo ( isset( $invoice[0]->Country ) ? $invoice[0]->Country.', ' : '' ) . ( isset( $invoice[0]->PostalCode ) ? $invoice[0]->PostalCode : '' ); ?></small></div>
                                <i class="fa fa-phone"></i> <small class="text-muted"><?php echo isset( $invoice[0]->Phone ) ? $invoice[0]->Phone : ''; ?></small><br>
                                <i class="fa fa-envelope-o"></i> <small class="text-muted"><?php echo isset( $invoice[0]->Email ) ? $invoice[0]->Email : ''; ?></small>
                            </address>
                        </div>
                    </div>
                    <!-- END Client Info -->

                    <div class="col-xs-6 col-sm-6 col-lg-6 text-right">
                        <div class="push-10 border-bottom">
                            <h1 class="font-w600" style="color: #2070bd;">Invoice</h1>
                            <h5>Invoice No : <?PHP echo isset( $invoice[0]->InvoiceID ) ? $invoice[0]->InvoiceID : ''; ?></h5>
                        </div><br>
                        <span><b>Payment Method:</b></span><br>
                        <small class="text-muted">Paypal: payment@seedomega.com</small><br>
                        <small class="text-muted">Card Payment WE Accept: Visa, Mastercard, Payoneer</small>
                    </div>
                </div>
                <!-- END Invoice Info -->

                <div class="row details push-20-t">
                    <div class="col-xs-3 col-sm-3 col-lg-3 div-due">
                        <div class="push-20-t">
                            Date : <?php echo $invoice[0]->CreatedAt != "0000-00-00" ? strtolower( date( 'd M Y', strtotime( $invoice[0]->CreatedAt ) ) ) : "-"; ?>
                        </div>
                        <div class="push-50-t font-w700">
                            <div class="h5 push-10-t"><b>Amount Due</b></div>
                            <div class="h2"><b>$<?php echo isset( $invoice[0]->SubTotal ) ? number_format( $invoice[0]->SubTotal, 2 ) : '0.00'; ?></b></div>
                        </div>
                        <div class="push-50-t">
                            <div class="push-10-t">PAYMENT METHOD</div>
                            <div>payment@seedomega.com</div>
                        </div>
                    </div>

                    <div class="col-xs-9 col-sm-9 col-lg-9 div-table">
                        <div class="pull-right">
                            <!-- Table -->
                            <table class="table table-main">
                                <thead>
                                    <tr>
                                        <th class="text-center">Item Descriptions</th>
                                        <th class="text-center">Details</th>
                                        <th class="text-center">Due Date</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-right" style="min-width:120px;padding-right:20px;">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if( isset( $invoice ) && count( $invoice ) ){
                                        foreach( $invoice as $inv ){ ?>
                                            <tr>
                                                <td>
                                                    <p class="font-w600 push-10"><?php echo isset( $inv->InvoiceType ) && $inv->InvoiceType != '' ? $inv->InvoiceType : $inv->Type; ?></p>
                                                    <table class="table-transparent" style="min-width:155px;">
                                                        <?php if( isset( $inv->SubscriptionRate ) && $inv->Type == 'Booking Commission' ){ ?>
                                                            <tr>
                                                                <td><small class="text-muted text-darkgray">Subscription Rate:</small> </td>
                                                                <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo number_format( $inv->SubscriptionRate, 2 ); ?>%</small></td>
                                                            </tr>
                                                        <?php } ?>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table class="table-transparent" style="min-width:200px;">
                                                        <?php if( isset( $inv->BookingID ) ){ ?>
                                                            <tr>
                                                                <td class="w70"><small class="text-muted text-darkgray">Booking ID:&nbsp;&nbsp;</small></td>
                                                                <td><small class="text-muted text-darkgray"><?php echo $inv->BookingID ?></small></td>
                                                            </tr>
                                                        <?php } ?>

                                                        <?php if( isset( $inv->Investor ) ){ ?>
                                                            <tr>
                                                                <td class="w70"><small class="text-muted text-darkgray">Investor:</small></td>
                                                                <td><small class="text-muted text-darkgray"><?php echo $inv->Investor ?></small></td>
                                                            </tr>
                                                        <?php } ?>

                                                        <?php if( isset( $inv->TotalAmountAttached ) ){ ?>
                                                            <tr>
                                                                <td class="w70"><small class="text-muted text-darkgray">Amount:</small></td>
                                                                <td><small class="text-muted text-darkgray">$<?php echo number_format( $inv->TotalAmountAttached, 2 ); ?></small></td>
                                                            </tr>
                                                        <?php } ?>
                                                    </table>
                                                </td>
                                                <td class="no-wrap"><small class="text-muted text-darkgray"><?php echo $inv->DueDate != "0000-00-00" ? date( 'j M Y', strtotime( $inv->DueDate ) ) : "-"; ?></small></td>
                                                <td class="text-center"><span class="badge badge-primary"><?php echo isset( $inv->Qty ) ? $inv->Qty : '1'; ?></span></td>
                                                <td class="text-right">$<?php echo isset( $inv->ItemAmount ) ? number_format( $inv->ItemAmount, 2 ) : '0.00'; ?></td>
                                            </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                            <!-- END Table -->
                        </div>
                    </div>
                </div>

                <div class="row pull-right push-0-r">
                    <table class="table summary-total">
                        <tbody>
                            <tr>
                                <td class="font-w600 text-right">Sub-Total</td>
                                <td class="text-right">$<?php echo isset( $invoice[0]->SubTotal ) ? number_format( $invoice[0]->SubTotal, 2 ) : '0.00'; ?></td>
                            </tr>
                            <!-- <tr>
                                <td class="font-w600 text-right">Vat Rate</td>
                                <td class="text-right">20%</td>
                            </tr>
                            <tr>
                                <td class="font-w600 text-right">Vat Due</td>
                                <td class="text-right">$ 5.500,00</td>
                            </tr> -->
                            <tr>
                                <td class="font-w700 text-right">Grand Total</td>
                                <td class="font-w700 text-right no-wrap">$<?php echo isset( $invoice[0]->Total ) ? number_format( $invoice[0]->Total, 2 ) : '0.00'; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear"></div>

                <div class="row">
                    <!-- Company Info -->
                    <div class="col-xs-6 col-sm-6 col-lg-6">
                        <address>
                            <div class="border-bottom">
                                <i class="fa fa-map-marker"></i> 18 Marina Boulevard #35-08
                                <div class="push-15-l">Singapore 018980</div>
                            </div>
                            <div class="push-10-t">
                                <i class="fa fa-phone"></i> +65 6651 4886<br>
                                <i class="fa fa-globe"></i> www.seedomega.com<br>
                                <i class="fa fa-envelope-o"></i> info@seedomega.com
                            </div>
                        </address>
                    </div>
                    <!-- END Company Info -->
                </div>

                <hr class="hidden-print"><br>
                <div class="text-center push-20">
                    <?php 
                    switch( User::info( 'UserLevel' ) ){
                        case 'Client':
                            $redirectlink = View::url( 'clients/invoices' );
                            break;

                        case 'Accounting':
                            $redirectlink = View::url( 'invoices' );
                            break;

                         default:
                            $redirectlink = View::url( 'invoices' );
                            break;
                    } ?>
                    <a href="<?php echo $redirectlink; ?>" class="btn btn-rounded btn-info">Go Back <span class="si si-action-undo"></span></a>
                </div>

                <!-- Footer -->
                <hr class="hidden-print">
                <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
                <!-- END Footer -->

                <!-- Single Pay Modal -->
                <div class="modal" id="singlepay-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form enctype="multipart/form-data" method="post">
                                <input type="hidden" name="action" value="paysingle">
                                <input type="hidden" name="InvoiceID" value="<?php echo isset( $invoice->InvoiceID ) ? $invoice->InvoiceID : '0'; ?>">

                                <div class="block block-themed block-transparent remove-margin-b">
                                    <div class="block-header bg-primary-dark">
                                        <ul class="block-options">
                                            <li>
                                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                            </li>
                                        </ul>
                                        <h3 class="block-title">Please Confirm</h3>
                                    </div>
                                    <div class="block-content form-ui">
                                        <p>Are you sure you want to pay this invoice?</p>

                                        <div class="form-group push-20-t">
                                            <div class="col-xs-12 col-lg-6 push-10">
                                                <label class="font-w400 text-muted animated fadeIn text-left"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label>
                                                <input type="text" value="$ <?php echo number_format( $mybalance, 2 ); ?>" class="form-control" readonly="">
                                            </div>
                                            <div class="col-xs-12 col-lg-6 push-10">
                                                <label class="font-w400 text-muted animated fadeIn text-left text-uppercase">AMOUNT DUE</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" value="$<?php echo isset( $invoice->Total ) ? number_format( $invoice->Total, 2 ) : '0.00'; ?>" required="required" class="form-control" placeholder="0.00" readonly="">
                                                </div>
                                            </div>
                                        </div>

                                        <label class="css-input css-checkbox css-checkbox-info text-center">
                                            <input id="cb-confirm" type="checkbox" value="" required="required"> <span></span> I/We read and verified the above data.
                                        </label><br>

                                    </div>
                                </div>
                                <div class="ln-solid"></div>
                                <div class="modal-footer">
                                    <button class="btn btn-rounded btn-default" type="button" data-dismiss="modal">CLOSE</button>
                                    <button type="submit" class="text-success btn btn-info btn-rounded"><i class="fa fa-check"></i> SUBMIT PAYMENT</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END Single Pay Modal -->

            </div>
        </div>
        <!-- END Invoice -->
    </div>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        hideButton();
    });

    function hideButton(){
        var $mybalance = <?php echo isset( $mybalance ) ? number_format( $mybalance, 2 ) : 0; ?>;
        if( parseFloat( $mybalance ) < 0 ){
            $( '#process-pay' ).addClass( 'hidden' );
        }
    }
</script>