<?php
View::$title = 'Dashboard';
View::$bodyclass = User::info('Sidebar').' client-dashboard-page';
View::header();
?>

    <section class="header-bottom">
        <article>
            <div class="container">
                <h1>Accessible. Approachable. Accountable.</h1>
                <p>The knowledge and insights to uncover opportunities and the commitment to see them through. Clarity from complexity, we find truth in numbers.</p>
            </div>
        </article>
    </section>

    <!-- content start -->
    <section class="gray">
        <article class="container projects-page" id="clientdashboard">
            <div class="row">
                <?php echo View::getMessage();  ?>

                <div class="col-md-12">
                    <div class="block">
                        <!-- Booking Summary -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="booking-summary">

                                    <div class="block-header">
                                        <div class="block-title">Tasks To Accomplish</div>
                                    </div>
                                    <div class="block-content text-center">
                                        <div class="row items-push text-center">
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/create/1' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Invoices ) ? number_format( $allPendings->Invoices ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-file-text-o fa-1x"></i></div>
                                                    <label>For Invoicing</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/documents' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Documents ) ? number_format( $allPendings->Documents ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-files-o fa-1x"></i></div>
                                                    <label for="">Pending TT Receipts</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/banks/1' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Clients ) ? number_format( $allPendings->Clients ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-bank fa-1x"></i></div>
                                                    <label>Pending Bank Accounts</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/withdrawal' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Earnings ) ? number_format( $allPendings->Earnings ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="si si-wallet fa-1x"></i></div>
                                                    <label>Pending Withdrawals</label>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END Booking Summary -->

                    </div>
                </div> 

            </div>
        </article>
    </section>
    <!-- content end -->

<?php View::footer(); ?>

<script type="text/javascript">

</script>