<?php
View::$title = 'Invoices';
View::$bodyclass = '';
View::header();
?>


<div class="content content-boxed">
    <!-- Invoice -->
    <div class="block">
        <div class="block-header">
            <ul class="block-options">
                <li>
                    <!-- Print Page functionality is initialized in App() -> uiHelperPrint() -->
                    <button type="button" onclick="App.initHelper('print-page');"><i class="si si-printer"></i> Print Invoice</button>
                </li>
                <li>
                    <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"></button>
                </li>
                <li>
                    <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                </li>
            </ul>
            <!--<h3 class="block-title">#INV0625</h3>-->
        </div>
        <div class="block-content block-content-narrow">
            <!-- Invoice Info -->
            <div class="h1 text-center push-30-t push-30 hidden-print">INVOICE</div>
            <hr class="hidden-print">
            <div class="row items-push-2x">
                <!-- Company Info -->
                <div class="col-xs-6 col-sm-4 col-lg-3">
                    <p class="h2 font-w400 push-5">SeedOmega</p>
                    <address>
                        18 Marina Boulevard #35-08<br/>
                        Singapore 018980 <br/>
                        <i class="si si-call-end"></i>+65 6651 4886
                    </address>
                </div>
                <!-- END Company Info -->

                <!-- Client Info -->
                <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-lg-3 col-lg-offset-6 text-right">
                    <p class="h2 font-w400 push-5"><?php echo isset( $booking->FirstName ) ? $booking->FirstName : '' . isset( $booking->LastName ) ? ' '.$booking->LastName : ''; ?></p>
                    <address>
                        <?php echo isset( $booking->Address ) ? $booking->Address : '' . isset( $booking->Address2 ) ? ' '.$booking->Address2 : ''; ?><br>
                        <?php echo isset( $booking->City ) ? $booking->City : ''; ?><br>
                        <?php echo isset( $booking->State ) ? $booking->State.', ' : '' . isset( $booking->PostalCode ) ? $booking->PostalCode : ''; ?><br>
                        <i class="si si-call-end"></i> <?php echo isset( $booking->Phone ) ? $booking->Phone : ''; ?>
                    </address>
                </div>
                <!-- END Client Info -->
            </div>
            <!-- END Invoice Info -->

            <!-- Table -->
            <div class="table-responsive push-50">
                <form method="post">
                    <input type="hidden" name="action" value="addinvoice">
                    <input type="hidden" name="invoice[UserID]" value="<?php echo isset( $booking->UserID ) ? $booking->UserID : '0'; ?>">
                    <input type="hidden" name="invoice[ClientID]" value="<?php echo isset( $booking->ClientProfileID ) ? $booking->ClientProfileID : '0'; ?>">
                    <input type="hidden" name="invoice[DueDate]" value="<?php echo date( 'Y-m-d', strtotime( "+7 days" ) ); ?>">
                    <input type="hidden" name="invoice[InvoiceDate]" value="<?php echo date( 'Y-m-d' ); ?>">
                    <input type="hidden" name="invoice[InvestmentBookingID]" value="<?php echo isset( $booking->InvestmentBookingID ) ? $booking->InvestmentBookingID : '0'; ?>">
                    <input type="hidden" name="invoice[InvestorUserID]" value="<?php echo isset( $booking->InvestorUserID ) ? $booking->InvestorUserID : '0'; ?>">
                    <input type="hidden" name="invoice[InvestmentAmount]" value="<?php echo isset( $booking->TotalAmountAttached ) ? $booking->TotalAmountAttached : '0.00'; ?>">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;"></th>
                            <th class="text-center">Products</th>
                            <th class="text-center">Details</th>
                            <!-- <th class="text-center">Investment Amount</th>
                            <th class="text-center">Subscription Rate</th> -->
                            <th class="text-center" style="width:100px;">Quantity</th>
                            <th class="text-right" style="width:150px;">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td>
                                <input type="text" style="width:100%;" name="invoice[InvoiceItem]" class="form-control" value="<?php echo isset( $booking->CompanyName ) ? $booking->CompanyName : ''; ?>">
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>Investor Name:</td>
                                        <td style="padding-left:20px;"><?php echo isset( $booking->Investor ) ? $booking->Investor : '' ?></td>
                                    </tr>
                                    <tr>
                                        <td>Investment Amount:</td>
                                        <td style="padding-left:20px;">$<?php echo isset( $booking->TotalAmountAttached ) ? number_format( $booking->TotalAmountAttached, 2 ) : '0.00' ?></td>
                                    </tr>
                                    <tr>
                                        <td>Subscription Rate: </td>
                                        <td style="padding-left:20px;"><?php echo isset( $booking->SubscriptionRate ) ? number_format( $booking->SubscriptionRate, 2 ) : '0.00' ?>%</td>
                                    </tr>
                                </table>
                            </td>
                            <!-- <td class="text-right">$ <?php echo isset( $booking->TotalAmountAttached ) ? number_format( $booking->TotalAmountAttached, 2 ) : '0.00' ?></td>
                            <td class="text-right"><?php echo isset( $booking->SubscriptionRate ) ? number_format( $booking->SubscriptionRate, 2 ) : '0.00' ?> %</td> -->
                            <td class="text-center"><span class="badge badge-primary">1</span></td>
                            <td class="text-right">$ <input type="number" style="width:80%; display:inline-block" name="unit_price" class="form-control priceInput" value="<?php echo isset( $booking->TotalInvoiceAmount ) ? number_format( $booking->TotalInvoiceAmount, 2 ) : '0.00'; ?>"></td>

                        </tr>

                        <tr>
                            <td colspan="4" class="font-w600 text-right">Subtotal</td>
                            <td class="text-right">$ <input type="number" style="width:80%;display: inline-block" name="invoice[SubTotal]" id="subtotal" class="form-control" value="<?php echo isset( $booking->TotalInvoiceAmount ) ? number_format( $booking->TotalInvoiceAmount, 2 ) : '0.00'; ?>"></td>
                        </tr>
                        <!--<tr>
                            <td colspan="4" class="font-w600 text-right">Vat Rate</td>
                            <td class="text-right">20%</td>
                        </tr>-->
                        <!--  <tr>
                            <td colspan="4" class="font-w600 text-right">Vat Due</td>
                            <td class="text-right">$ <input type="text" style="width:80%;" name="subtotal"></td>
                        </tr>-->
                        <tr class="active">
                            <td colspan="4" class="font-w700 text-uppercase text-right">Total Due</td>
                            <td class="font-w700 text-right">$ <input type="number" style="width:80%;display: inline-block" name="invoice[Total]" id="total" class="form-control" value="<?php echo isset( $booking->TotalInvoiceAmount ) ? number_format( $booking->TotalInvoiceAmount, 2 ) : '0.00'; ?>"></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <button type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary">Save Invoice</button>
                    </div>
                </form>
            </div>
            <!-- END Table -->

            <!-- Footer -->
            <hr class="hidden-print">
            <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
            <!-- END Footer -->
        </div>
    </div>
    <!-- END Invoice -->
</div>

<?php View::footer(); ?>