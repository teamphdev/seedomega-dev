<?php
View::$title = 'Invoices';
View::$bodyclass = '';
View::header();
?>

<section class="gray">
    <div class="container push-100-t">
        <!-- Invoice -->
        <div class="block centered">
            <div class="block-header">
                <ul class="block-options">
                    <li>
                        <div>
                            <?php $wallet = isset( $mybalance ) ? '$'.number_format( $mybalance, 2 ) : '$0.00';
                            $wallet = str_ireplace( '$-', '-$', $wallet );
                            if( $hidepay == '' ){ ?>
                            Running Balance: <a href="/wallet"><label id="running-balance" class="block-title font-w300 text-primary animated flipInX pointer"><b><?php echo $wallet; ?></b></label></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <?php } ?>
                            <a href="/invoices/goprint/<?php echo isset( $invoice->InvoiceID ) ? $invoice->InvoiceID : ''; ?>" target="_blank"><i class="si si-printer"></i> Print Invoice</a>
                        </div>
                    </li>
                </ul>
                <h3 class="block-title">INVOICE # <?PHP echo isset( $invoice->InvoiceID ) ? $invoice->InvoiceID : ''; ?></h3>
            </div>
            <div class="block-content">
                <!-- Invoice Info -->
                <div class="sologo"><?php echo $invoice->Logo; ?></div>
                <div class="h1 text-center push-30-t push-30">
                    INVOICE 
                    <a href="javascript:void();" data-toggle="modal" data-target="#singlepay-modal" class="btn btn-info inline text-uppercase pull-right <?php echo $hidepay; ?>" title="Pay" id="process-pay"><i class="si si-wallet"></i> Pay</a>
                </div>
                <hr class="">
                <div class="row items-push-2x">
                    <!-- Company Info -->
                    <div class="col-xs-6 col-sm-4 col-lg-3">
                        <p class="h2 font-w400 push-5">SeedOmega</p>
                        <address>
                            18 Marina Boulevard #35-08<br/>
                            Singapore 018980<br/>
                            <i class="si si-call-end"></i>+65 6651 4886
                        </address>
                    </div>
                    <!-- END Company Info -->

                    <!-- Client Info -->
                    <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-lg-3 col-lg-offset-6 text-right">
                        <p class="h2 font-w400 push-5"><?php echo isset( $invoice->CompanyName ) ? $invoice->CompanyName : ''; ?></p>
                        <!-- <p class="h2 font-w400 push-5"><?php echo isset( $invoice->FirstName ) ? $invoice->FirstName : ''; ?><?php echo isset( $invoice->LastName ) ? ' '.$invoice->LastName : ''; ?></p> -->
                        <address>
                            <?php echo ( isset( $invoice->Address ) ? $invoice->Address : '' ) . ( isset( $invoice->Address2 ) ? ' '.$invoice->Address2 : '' ); ?><br>
                            <?php echo ( isset( $invoice->City ) ? $invoice->City.', ' : '' ) . ( isset( $invoice->State ) ? $invoice->State : '' ); ?><br>
                            <?php echo ( isset( $invoice->Country ) ? $invoice->Country.', ' : '' ) . ( isset( $invoice->PostalCode ) ? $invoice->PostalCode : '' ); ?><br>
                            <i class="si si-call-end"></i> <?php echo isset( $invoice->Phone ) ? $invoice->Phone : ''; ?>
                        </address>
                    </div>
                    <!-- END Client Info -->
                </div>
                <!-- END Invoice Info -->

                <!-- Table -->
                <div class="table-responsive push-20">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;"></th>
                            <th class="text-center">Projects</th>
                            <th class="text-center">Details</th>
                            <th class="text-center">Invoice Date</th>
                            <th class="text-center">Due Date</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center" style="width:100px;">Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td>
                                <p class="font-w600 push-10"><?php echo isset( $invoice->InvoiceItem ) ? $invoice->InvoiceItem : ''; ?></p>

                                <table>
                                <?php if( isset( $invoice->SubscriptionRate ) ){ ?>
                                    <tr>
                                        <td><small class="text-muted text-darkgray">Subscription Rate:</small> </td>
                                        <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo number_format( $invoice->SubscriptionRate, 2 ); ?> %</small></td>
                                    </tr>
                                <?php } ?>
                                </table>
                            </td>
                            <td>
                                <table>
                                <?php if( isset( $invoice->InvoiceType ) && strlen( $invoice->InvoiceType ) ){ ?>
                                    <tr>
                                        <td><small class="text-muted text-darkgray">Invoice Type:</small></td>
                                        <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->InvoiceType ?></small></td>
                                    </tr>
                                <?php } ?>

                                <?php if( isset( $invoice->InvestmentBookingID ) ){ ?>
                                    <tr>
                                        <td><small class="text-muted text-darkgray">Booking ID:</small></td>
                                        <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->InvestmentBookingID ?></small></td>
                                    </tr>
                                <?php } ?>

                                <?php if( isset( $invoice->Investor ) ){ ?>
                                    <tr>
                                        <td><small class="text-muted text-darkgray">Investor Name:</small></td>
                                        <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->Investor ?></small></td>
                                    </tr>
                                <?php } ?>

                                <?php if( isset( $invoice->InvestmentAmount ) ){ ?>
                                    <tr>
                                        <td><small class="text-muted text-darkgray">Investment Amount:</small></td>
                                        <td style="padding-left:20px;"><small class="text-muted text-darkgray">$ <?php echo number_format( $invoice->InvestmentAmount, 2 ); ?></small></td>
                                    </tr>
                                <?php } ?>
                                </table>
                            </td>
                            <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $invoice->CreatedAt ) ) : "-"; ?></small></td>
                            <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->DueDate != "0000-00-00" ? date( 'j M Y', strtotime( $invoice->DueDate ) ) : "-"; ?></small></td>
                            <td class="text-center"><span class="badge badge-primary">1</span></td>
                            <td class="text-right">$ <?php echo isset( $invoice->SubTotal ) ? number_format( $invoice->SubTotal, 2 ) : '0.00'; ?></td>
                        </tr>

                        <tr>
                            <td colspan="6" class="font-w600 text-right">Subtotal</td>
                            <td class="text-right">$ <?php echo isset( $invoice->SubTotal ) ? number_format( $invoice->SubTotal, 2 ) : '0.00'; ?></td>
                        </tr>
                       <!-- <tr>
                            <td colspan="4" class="font-w600 text-right">Vat Rate</td>
                            <td class="text-right">20%</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="font-w600 text-right">Vat Due</td>
                            <td class="text-right">$ 5.500,00</td>
                        </tr>-->
                        <tr class="active">
                            <td colspan="6" class="font-w700 text-uppercase text-right">Total Due</td>
                            <td class="font-w700 text-right">$ <?php echo isset( $invoice->Total ) ? number_format( $invoice->Total, 2 ) : '0.00'; ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <!-- END Table -->

                <div class="text-center push-30">
                    <?php 
                    switch (User::info('UserLevel')) {
                        case 'Client':
                            $redirectlink = View::url('clients/invoices');
                            break;

                        case 'Accounting':
                            $redirectlink = View::url('invoices');
                            break;

                         default:
                            $redirectlink = View::url('invoices');
                            break;
                    } ?>
                    <a href="<?php echo $redirectlink; ?>" class="btn btn-rounded btn-info">Go Back <span class="si si-action-undo"></span></a>
                </div>

                <!-- Footer -->
                <hr class="hidden-print">
                <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
                <!-- END Footer -->

                <!-- Single Pay Modal -->
            <div class="modal" id="singlepay-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="paysingle">
                            <input type="hidden" name="InvoiceID" value="<?php echo isset( $invoice->InvoiceID ) ? $invoice->InvoiceID : '0'; ?>">

                            <div class="block block-themed block-transparent remove-margin-b">
                                <div class="block-header bg-primary-dark">
                                    <ul class="block-options">
                                        <li>
                                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Please Confirm</h3>
                                </div>
                                <div class="block-content form-ui">
                                    <p>Are you sure you want to pay this invoice?</p>

                                    <div class="form-group push-20-t">
                                        <div class="col-xs-12 col-lg-6 push-10">
                                            <label class="font-w400 text-muted animated fadeIn text-left"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label>
                                            <input type="text" value="$ <?php echo number_format( $mybalance, 2 ); ?>" class="form-control" readonly="">
                                        </div>
                                        <div class="col-xs-12 col-lg-6 push-10">
                                            <label class="font-w400 text-muted animated fadeIn text-left text-uppercase">AMOUNT DUE</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="text" value="$<?php echo isset( $invoice->Total ) ? number_format( $invoice->Total, 2 ) : '0.00'; ?>" required="required" class="form-control" placeholder="0.00" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <label class="css-input css-checkbox css-checkbox-info text-center">
                                        <input id="cb-confirm" type="checkbox" value="" required="required"> <span></span> I/We read and verified the above data.
                                    </label><br>

                                </div>
                            </div>
                            <div class="ln-solid"></div>
                            <div class="modal-footer">
                                <button class="btn btn-rounded btn-default" type="button" data-dismiss="modal">CLOSE</button>
                                <button type="submit" class="text-success btn btn-info btn-rounded"><i class="fa fa-check"></i> SUBMIT PAYMENT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <!-- END Single Pay Modal -->

            </div>
        </div>
        <!-- END Invoice -->
    </div>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        hideButton();
    });

    function hideButton(){
        var $mybalance = <?php echo isset( $mybalance ) ? number_format( $mybalance, 2 ) : 0; ?>;
        if( parseFloat( $mybalance ) < 0 ){
            $( '#process-pay' ).addClass( 'hidden' );
        }
    }
</script>