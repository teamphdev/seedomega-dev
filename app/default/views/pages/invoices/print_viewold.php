<?php
View::$title = 'Invoices';
View::$bodyclass = '';
View::header( false,'header_print' );
?>

<div class="content content-boxed">
    <!-- Invoice -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">INVOICE # <?PHP echo isset( $invoice->InvoiceID ) ? $invoice->InvoiceID : ''; ?></h3>
        </div>
        <div class="block-content">
            <!-- Invoice Info -->
            <div class="sologo"><?php echo $invoice->Logo; ?></div>
            <div class="h1 text-center push-30-t push-30 hidden-print">INVOICE</div>
            <hr class="hidden-print">
            <div class="row items-push-2x">
                <!-- Company Info -->
                <div class="col-xs-6 col-sm-4 col-lg-3">
                    <p class="h2 font-w400 push-5">SeedOmega</p>
                    <address>
                        18 Marina Boulevard #35-08<br/>
                        Singapore 018980 <br/>
                        <i class="si si-call-end"></i>+65 6651 4886
                    </address>
                </div>
                <!-- END Company Info -->

                <!-- Client Info -->
                <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-lg-3 col-lg-offset-6 text-right">
                    <p class="h2 font-w400 push-5"><?php echo isset( $invoice->CompanyName ) ? $invoice->CompanyName : ''; ?></p>
                    <!-- <p class="h2 font-w400 push-5"><?php echo ( isset( $invoice->FirstName ) ? $invoice->FirstName : '' ) . ( isset( $invoice->LastName ) ? ' '.$invoice->LastName : '' ); ?></p> -->
                    <address>
                        <?php echo ( isset( $invoice->Address ) ? $invoice->Address : '' ) . ( isset( $invoice->Address2 ) ? ' '.$invoice->Address2 : '' ); ?><br>
                        <?php echo ( isset( $invoice->City ) ? $invoice->City : '' ) . ( isset( $invoice->State ) ? ', '.$invoice->State : '' ); ?><br>
                        <?php echo ( isset( $invoice->Country ) ? $invoice->Country : '' ) . ( isset( $invoice->PostalCode ) ? ', '.$invoice->PostalCode : '' ); ?><br>
                        <i class="si si-call-end"></i> <?php echo isset( $invoice->Phone ) ? $invoice->Phone : ''; ?>
                    </address>
                </div>
                <!-- END Client Info -->
            </div>
            <!-- END Invoice Info -->

            <!-- Table -->
            <div class="table-responsive push-50">
                <table class="table  table-hover">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;"></th>
                        <th class="text-center">Product</th>
                        <th class="text-center">Details</th>
                        <th class="text-center">Invoice Date</th>
                        <th class="text-center">Due Date</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center" style="width: 100px;">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">1</td>
                        <td>
                            <p class="font-w600 push-10"><?php echo isset( $invoice->InvoiceItem ) ? $invoice->InvoiceItem : ''; ?></p>
                            <table>
                            <?php if( isset( $invoice->SubscriptionRate ) ){ ?>
                                <tr>
                                    <td><small class="text-muted text-darkgray">Subscription Rate:</small> </td>
                                    <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo number_format( $invoice->SubscriptionRate, 2 ); ?> %</small></td>
                                </tr>
                            <?php } ?>
                            </table>
                        </td>
                        <td>
                            <table>
                            <?php if( isset( $invoice->InvoiceType ) && strlen( $invoice->InvoiceType ) ){ ?>
                                <tr>
                                    <td><small class="text-muted text-darkgray">Invoice Type:</small></td>
                                    <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->InvoiceType ?></small></td>
                                </tr>
                            <?php } ?>

                            <?php if( isset( $invoice->InvestmentBookingID ) ){ ?>
                                <tr>
                                    <td><small class="text-muted text-darkgray">Booking ID:</small></td>
                                    <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->InvestmentBookingID ?></small></td>
                                </tr>
                            <?php } ?>

                            <?php if( isset( $invoice->Investor ) ){ ?>
                                <tr>
                                    <td><small class="text-muted text-darkgray">Investor Name:</small></td>
                                    <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->Investor ?></small></td>
                                </tr>
                            <?php } ?>

                            <?php if( isset( $invoice->InvestmentAmount ) ){ ?>
                                <tr>
                                    <td><small class="text-muted text-darkgray">Investment Amount:</small></td>
                                    <td style="padding-left:20px;"><small class="text-muted text-darkgray">$ <?php echo number_format( $invoice->InvestmentAmount, 2 ); ?></small></td>
                                </tr>
                            <?php } ?>
                            </table>
                        </td>
                        <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $invoice->CreatedAt ) ) : "-"; ?></small></td>
                        <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo $invoice->DueDate != "0000-00-00" ? date( 'j M Y', strtotime( $invoice->DueDate ) ) : "-"; ?></small></td>
                        <td class="text-center"><span class="badge badge-primary">1</span></td>
                        <td class="text-right">$ <?php echo isset( $invoice->SubTotal ) ? number_format( $invoice->SubTotal, 2 ) : '0.00'; ?></td>
                    </tr>

                    <tr>
                        <td colspan="6" class="font-w600 text-right">Subtotal</td>
                        <td class="text-right">$ <?php echo isset( $invoice->SubTotal ) ? number_format( $invoice->SubTotal, 2 ) : '0.00'; ?></td>
                    </tr>
                   <!-- <tr>
                        <td colspan="4" class="font-w600 text-right">Vat Rate</td>
                        <td class="text-right">20%</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="font-w600 text-right">Vat Due</td>
                        <td class="text-right">$ 5.500,00</td>
                    </tr>-->
                    <tr class="active">
                        <td colspan="6" class="font-w700 text-uppercase text-right">Total Due</td>
                        <td class="font-w700 text-right">$ <?php echo isset( $invoice->Total ) ? number_format( $invoice->Total, 2 ) : '0.00'; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- END Table -->

            <!-- Footer -->
            <hr class="hidden-print">
            <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
            <!-- END Footer -->
        </div>
    </div>
    <!-- END Invoice -->
</div>
