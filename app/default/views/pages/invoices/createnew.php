<?php
View::$title = 'Create Invoice';
View::$bodyclass = '';
View::header();
?>
    <section class="header-bottom">
        <article>
            <div class="container"><h1><?php echo View::$title; ?></h1></div>
        </article>
    </section>

    <section class="breadcrumb">
        <article class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul>
                        <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                        <li><a href="<?php echo View::url(); ?>">Home</a></li>
                        <li class="fa fa-angle-right"></li>
                        <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo View::$title; ?></li>
                    </ul>
                </div>
                <div class="col-lg-6 align-right sub-menu">
                </div>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
    <!-- Page Content -->
    <div class="container create invoice">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title"><?php echo View::$title; ?></h3>
            </div>
            <div class="block-content">
                <!-- Invoice Info -->
                <div class="row">
                    <!-- Client Info -->
                    <div class="col-xs-6 col-sm-6 col-lg-6">
                        <div class="sologo"><?php echo $logo; ?></div>
                        <div class="push-20-t">
                            <b>Bill To:</b>

                            <select class="form-control selClient client-list">
                                <option value="">-select client-</option>
                                <?php foreach( $clients as $cl ) : ?>
                                    <?php if( $cl->FirstName ) : ?>
                                        <option value="<?php echo $cl->ClientProfileID; ?>"><?php echo $cl->FirstName.' '.$cl->LastName.' ('.$cl->CompanyName.')'; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>

                            <br><label class="text-danger push-5-t empty hidden"><i class="fa fa-hand-o-up pull-right"></i> Please select a Client!</label>

                            <a href="#" id="btnSelectClient" class="btn btn-sm btn-default btn-rounded hidden" style="margin-top:-10px;">Select Client</a>
                            <p class="h4 font-w700 push-5-t push-5" id="clientNamePart"></p>
                            <address>
                                <i class="fa fa-map-marker"></i> <span id="clientAddress1"></span>
                                <div class="push-15-l"><span id="clientAddress2"></span></div>
                                <div class="push-15-l"><span id="clientAddress3"></span></div>
                                <span id="clientPhone"></span>
                            </address>
                        </div>
                    </div>
                    <!-- END Client Info -->

                    <div class="col-xs-6 col-sm-6 col-lg-6 text-right">
                        <div class="push-10 border-bottom">
                            <h1 class="font-w600" style="color: #2070bd;">Invoice</h1>
                        </div><br>
                        <span><b>Payment Method:</b></span><br>
                        <small class="text-muted">Paypal: payment@seedomega.com</small><br>
                        <small class="text-muted">Card Payment WE Accept: Visa, Mastercard, Payoneer</small>
                    </div>
                </div>
                <!-- END Invoice Info -->

                <!-- Table -->
                <form method="post">
                    <input type="hidden" name="action" value="createinvoice">
                    <input type="hidden" id="ClientUserID" name="invoice[UserID]" value="">
                    <input type="hidden" id="ClientProfileID" name="invoice[ClientID]" value="">
                    <input type="hidden" id="InvoiceItem" name="invoice[InvoiceItem]" value="">
                    <div class="clear"></div>
                    <div class="table-responsive">
                        <table id="invoice_table" class="table table-hover newinvoice-table">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width:50px;"></th>
                                    <th class="text-center">Item Descriptions</th>
                                    <th class="text-center">Details</th>
                                    <th class="text-center" style="width:100px;">Quantity</th>
                                    <th class="text-center" style="width:150px;">Price</th>
                                    <th class="text-center" style="width:170px;">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="productElement">
                                    <td class="text-center"><button type="button" id="addProduct" class="btn btn-sm btn-primary"><i class="si si-plus fa-1x"></i></button></td>
                                    <td>
                                        <select class="form-control selType">
                                            <option value="">-</option>
                                            <?php foreach( $invoiceTypes as $type ) : ?>
                                                <option value="<?php echo $type->Description; ?>"><?php echo $type->Description; ?></option>
                                            <?php endforeach; ?>
                                            <option value="Other">-blank-</option>
                                        </select>
                                        <input type="text" name="group[Type][]" value="" class="selecttype" readonly placeholder="-select item-">
                                        <select id="selBooking" class="form-control push-5-t selectbooking hidden">
                                            <option value="">-select booking-</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" name="group[Details][]" class="form-control indetails" value="">
                                        <div class="divdetails"></div>
                                    </td>
                                    <td><input type="number" name="group[Quantity][]" value="1" class="form-control text-center calculate" min="1" oninput="validity.valid||(value='1');" placeholder="0.00" required></td>
                                    <td><input type="number" name="group[Price][]" value="" class="form-control text-right calculate price" min="0" oninput="validity.valid||(value='0');" placeholder="0.00" required></td>
                                    <td><input type="text" name="group[Amount][]" value="" class="form-control text-right calculate-sub" placeholder="0.00" disabled></td>
                                </tr>
                                <tr class="footerTr">
                                    <td colspan="5" class="font-w600 text-right">Sub-Total</td>
                                    <td class="text-right">
                                        $<span class="invoice-sub-total">0.00</span>
                                    </td>
                                </tr>
                                <tr class="active">
                                    <td colspan="5" class="font-w700 text-uppercase text-right">Grand Total</td>
                                    <td class="font-w700 text-right">
                                        $<span class="invoice-total-due">0.00</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="clear"></div>
                        <div class="pull-right push-10-t">
                            <button type="submit" name="btnSubmit" id="btnSubmit" onclick="return checkDuplicates();" class="btn btn-primary btn-rounded">Save Invoice</button>
                        </div>
                    </div>
                    <!-- END Table -->

                    <div class="clear"></div>

                    <!-- Company Info -->
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-lg-6">
                            <address>
                                <div class="border-bottom">
                                    <i class="fa fa-map-marker"></i> 18 Marina Boulevard #35-08
                                    <div class="push-15-l">Singapore 018980</div>
                                </div>
                                <div class="push-10-t">
                                    <i class="fa fa-phone"></i> +65 6651 4886<br>
                                    <i class="fa fa-globe"></i> www.seedomega.com<br>
                                    <i class="fa fa-envelope-o"></i> info@seedomega.com
                                </div>
                            </address>
                        </div>
                    </div>
                    <!-- END Company Info -->
                </form>

                <!-- Footer -->
                <hr class="hidden-print">
                <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
                <!-- END Footer -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="clientModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Please select a Client</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control selClient" id="selClient">
                        <option value="">-select-</option>
                        <?php foreach( $clients as $cl ) : ?>
                            <?php if( $cl->FirstName ) : ?>
                                <option value="<?php echo $cl->ClientProfileID; ?>"><?php echo $cl->FirstName.' '.$cl->LastName.' ('.$cl->CompanyName.')'; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-rounded" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- END Page Content -->
    <input id="counter" type="hidden" value="0">
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $( '#btnSelectClient' ).click( function( e ){
            e.preventDefault();
            $( '#clientModal' ).modal( 'show' );
        })

        $( '.selClient' ).change( function(){
            $.ajax( '/invoices/getclient/'+$( this ).val(), {
                success: function( data ){
                    /*console.log(data);*/
                    $( '.empty' ).addClass( 'hidden' );
                    $( '#ClientUserID' ).val( data.UserID );
                    $( '#ClientProfileID' ).val( data.ClientProfileID );
                    $( '#InvoiceItem' ).val( data.CompanyName );
                    $( '#clientNamePart' ).html( data.CompanyName );
                    $( '#clientAddress1' ).html( data.Address + ' ' + data.Address2 );
                    $( '#clientAddress2' ).html( data.City + ', ' + data.State );
                    $( '#clientAddress3' ).html( data.Country + ' '+ data.PostalCode );
                    if( data.Phone == '' ){
                        $( '#clientPhone' ).html( '' );
                    } else {                        
                        $( '#clientPhone' ).html( '<i class="fa fa-phone"></i> ' + data.Phone );
                    }
                    populateBookings( data.ClientProfileID );
                },
                error: function(){}
            });
        })
        
        $( "#addProduct" ).click( function(){
            var ctr = parseInt( $( '#counter' ).val() );
            ctr++;
            $('<tr id="productElement">\
                <td class="text-center"><button type="button" class="btn btn-sm btn-danger remItem"><i class="fa fa-minus-circle fa-1x"></i></button></td>\
                <td>\
                    <select class="form-control selTypes">\
                        <option value="">-</option>\
                        <option value="Platform Fee">Platform Fee</option>\
                        <option value="Subscription Fee">Subscription Fee</option>\
                        <option value="Booking Commission">Booking Commission</option>\
                        <option value="Other">-blank-</option>\
                    </select>\
                    <input type="text" name="group[Type][]" value="" class="selecttype" readonly placeholder="-select item-">\
                    <select id="selBooking'+ctr+'" class="form-control push-5-t selectbooking hidden"></select>\
                </td>\
                <td>\
                    <input type="text" name="group[Details][]" class="form-control indetails" value="">\
                    <div class="divdetails"></div>\
                </td>\
                <td><input type="number" name="group[Quantity][]" value="1" class="form-control text-center calculate" min="1" oninput="validity.valid||(value=\'1\');" placeholder="0.00" required></td>\
                <td><input type="number" name="group[Price][]" value="" class="form-control text-right calculate price" min="0" oninput="validity.valid||(value=\'0\');" placeholder="0.00" required></td>\
                <td><input type="text" name="group[Amount][]" value="" class="form-control text-right calculate-sub" placeholder="0.00" disabled></td>\
            </tr>').insertBefore( ".footerTr" );

            var $options = $( "#selBooking > option" ).clone();
            $( '#selBooking'+ctr ).append( $options );

            $( '.selTypes' ).change( function(){
                var tr = $( this ).closest( 'tr' );
                var optvalue = $( this ).val();
                selectedType( tr, optvalue, 's' );
            });

            $( '.selectbooking' ).change( function(){
                var tr = $( this ).closest( 'tr' );
                var selected = $( "option:selected", this );
                selectedBooking( tr, selected.val(), selected.text() );
                updateTotals( this, '' );
            });
            $( '#counter' ).val( ctr );
        });

        $( "#invoice_table" ).on( 'click', '.remItem', function(){
            $( this ).parent().parent().remove();
        });

        $( '#invoice_table' ).on( 'change keyup paste', '.calculate', function(){
            console.log( 'updating totals' );
            updateTotals( this, '' );
        });
    });

    $( '.selType' ).change( function(){
        var tr = $( this ).closest( 'tr' );
        var optvalue = $( this ).val();
        selectedType( tr, optvalue, '' );
    });

    $( '#selBooking' ).change( function(){
        var tr = $( this ).closest( 'tr' );
        var selected = $( "option:selected", this );
        selectedBooking( tr, selected.val(), selected.text() );
        updateTotals( this, '' );
        if( selected.val() == '' ){
            $( '[name="group[Details][]"]', tr ).prop( 'readonly', false ).removeClass( 'hidden' );
        }
    });

    function selectedType( tr, ov, suf ){
        resetPrice( tr );
        $( '.selecttype', tr ).prop( 'readonly', true ).val( ov );
        $( '.selectbooking', tr ).addClass( 'hidden' );
        $( '[name="group[Price][]"]', tr ).prop( 'readonly', false );
        $( '[name="group[Details][]"]', tr ).prop( 'readonly', false ).removeClass( 'hidden' );
        $( '.divdetails', tr ).addClass( 'hidden' );
        switch( ov ){
            case 'Booking Commission':
                var clientid = $( '#ClientProfileID' ).val();
                $( '.selectbooking', tr ).prop( 'selectedIndex', 0 );
                if( clientid == '' ){
                    $( '.selType'+suf, tr ).prop( 'selectedIndex', 0 );
                    $( '.selecttype', tr ).val( '' );
                    // $( '#clientModal' ).modal( 'show' );
                    // alert( 'Please select first the Client.' );
                    $( '.client-list' ).focus();
                    $( '.empty' ).removeClass( 'hidden' );
                } else {
                    $( '[name="group[Price][]"]', tr ).prop( 'readonly', true );
                    $( '.selectbooking', tr ).removeClass( 'hidden' );
                }
            break;

            case 'Other':
                $( '.selecttype', tr ).val( '' );
                $( '.selecttype', tr ).prop( 'placeholder', 'write description here ...' );
                $( '.selecttype', tr ).prop( 'readonly', false ).focus();
            break;

            default: break;
        }
    }

    function selectedBooking( tr, ov, ot ){
        resetPrice( tr );
        var amount = ot.split( '$' );
        $.ajax({
            type: "GET",
            url: '/invoices/getbookdata/'+ov,
            dataType: 'json',
            success: function( response ){
                $( '.divdetails', tr ).html( response.HTML );
                $( '[name="group[Price][]"]', tr ).val( response.commission );
                $( '[name="group[Details][]"]', tr ).addClass( 'hidden' );
                $( '.divdetails', tr ).removeClass( 'hidden' );
                updateTotals( false, tr );
                // console.log( response );
            }
        });
    }

    function populateBookings( clientid ){
        reset();
        $( '.selectbooking' ).empty();
        $.ajax({
            type: "GET",
            url: '/invoices/getbookings/'+clientid,
            dataType: 'json',
            success: function( response ){
                $( ".selectbooking" ).append( "<option value=''>-select booking-</option>" );
                $.each( response, function( key, value ){
                    $( ".selectbooking" ).append( "<option value='"+value.InvestmentBookingID+"'>" + value.BookingDate + " - $" + value.Amount + "</option>");
                });
                console.log( response );
            }
        });
    }

    function updateTotals( elem, tr ){
        var tr = tr == '' ? $( elem ).closest( 'tr' ) : tr;
        var quantity = $( '[name="group[Quantity][]"]', tr ).val(),
            price = $( '[name="group[Price][]"]', tr ).val(),
            /*percent = $( '[name="invoice_product_discount[]"]', tr ).val(),*/
            subtotal = parseInt( quantity ) * parseFloat( price );
        $( '.calculate-sub', tr ).val( subtotal.toFixed( 2 ) );
        calculateTotal();
    }

    function calculateTotal(){
        var grandTotal = 0.0;
        var totalQuantity = 0;
        $( '.calculate-sub' ).each( function(){
            grandTotal += parseFloat( $( this ).val() );
        });
        grandTotal = parseFloat( grandTotal ).toFixed( 2 );
        $( '.invoice-sub-total' ).text( commaSeparateNumber( grandTotal ) );
        $( '.invoice-total-due' ).text( commaSeparateNumber( grandTotal ) );
    }

    function checkDuplicates(){
        var ret = true;
        var $elems = $( '.booking-id' );
        var values = [];

        $elems.each( function(){
            if( this.value == '' ){ return true; }
            values.push( this.value );
        });

        var unique_list = [];
        var duplicates_list = [];
        $.each( values, function( key, value ){
            if( $.inArray( value, unique_list ) == -1 ){
                unique_list.push( value );
            } else {
                if( $.inArray( value, duplicates_list ) == -1 ){
                    duplicates_list.push( value );
                }
            }
        });

        if( duplicates_list.length > 0 ){
            alert( 'Duplicate found on InvestmentBookingIDs: ' + duplicates_list );
            ret = false;
        }

        if( checkClient() == '0' ){
            $( '.client-list' ).focus();
            $( '.empty' ).removeClass( 'hidden' );
            ret = false;
        }

        return ret;
    }

    function checkClient(){
        var client = $( '#ClientProfileID' ).val();
        return client == '' || client == '0' ? '0' : client;
    }

    function reset(){
        $( '.selecttype' ).each( function(){
            var tr = $( this ).closest('tr')
            if( $( this ).val() == 'Booking Commission' ){
                $( this ).val( '' );
                $( '.price', tr ).val( '0.00' );
                $( '.divdetails', tr ).html( '' );
                $( '.selectbooking' ).addClass( 'hidden' );
                $( 'select', tr ).prop( 'selectedIndex', 0 );
                $( '[name="group[Details][]"]', tr ).removeClass( 'hidden' );
                updateTotals( this );
            }
        });
    }

    function resetPrice( tr ){
        $( '.divdetails', tr ).html( '' );
        $( '[name="group[Price][]"]', tr ).val( '0.00' );
        $( '[name="group[Quantity][]"]', tr ).val( '1' );
        updateTotals( false, tr );
    }

    // function addrow()
    // {
    //     var productElement = $("#productElement").html();
    //     $("<tr class='more-product'>"+productElement+"</tr>").insertBefore(".footerTr");
    //     //$("#productElement").append(""+productElement+"");
    // }
</script>