<?php
View::$title = 'Invoices';
View::$bodyclass = '';
View::header( false,'header_print' );
?>

<div class="content view invoice w1024 content-boxed">
    <!-- Invoice -->
    <div class="block">
        <div class="block-header">
        </div>
        <div class="block-content">
                
            <!-- Invoice Info -->
            <div class="row">
                <!-- Client Info -->
                <div class="col-xs-6 col-sm-6 col-lg-6">
                    <div class="sologo"><?php echo $logo; ?></div>
                    <div class="push-20-t">
                        <b>Bill To:</b>
                        <p class="h4 push-5-t push-0"><?php echo isset( $invoice[0]->CompanyName ) ? $invoice[0]->CompanyName : ''; ?></p>
                        <!-- <p class="h3 font-w400 push-5"><?php echo isset( $invoice->FirstName ) ? $invoice->FirstName : ''; ?><?php echo isset( $invoice->LastName ) ? ' '.$invoice->LastName : ''; ?></p> -->
                        <address>
                            <i class="fa fa-map-marker"></i> <small class="text-muted"><?php echo ( isset( $invoice[0]->Address ) ? $invoice[0]->Address : '' ) . ( isset( $invoice[0]->Address2 ) ? ' '.$invoice[0]->Address2 : '' ); ?>
                            <?php echo ( isset( $invoice[0]->City ) ? $invoice[0]->City.', ' : '' ) . ( isset( $invoice[0]->State ) ? $invoice[0]->State : '' ); ?></small><br>
                            <div class="push-15-l"><small class="text-muted"><?php echo ( isset( $invoice[0]->Country ) ? $invoice[0]->Country.', ' : '' ) . ( isset( $invoice[0]->PostalCode ) ? $invoice[0]->PostalCode : '' ); ?></small></div>
                            <i class="fa fa-phone"></i> <small class="text-muted"><?php echo isset( $invoice[0]->Phone ) ? $invoice[0]->Phone : ''; ?></small><br>
                            <i class="fa fa-envelope-o"></i> <small class="text-muted"><?php echo isset( $invoice[0]->Email ) ? $invoice[0]->Email : ''; ?></small>
                        </address>
                    </div>
                </div>
                <!-- END Client Info -->

                <div class="col-xs-6 col-sm-6 col-lg-6 text-right">
                    <div class="push-10 border-bottom">
                        <h1 class="font-w600" style="color: #2070bd;">Invoice</h1>
                        <h5>Invoice No : <?PHP echo isset( $invoice[0]->InvoiceID ) ? $invoice[0]->InvoiceID : ''; ?></h5>
                    </div><br>
                    <span><b>Payment Method:</b></span><br>
                    <small class="text-muted">Paypal: payment@seedomega.com</small><br>
                    <small class="text-muted">Card Payment WE Accept: Visa, Mastercard, Payoneer</small>
                </div>
            </div>
            <!-- END Invoice Info -->

            <div class="row details push-20-t">
                <?php /*
                <div class="col-xs-3 col-sm-3 col-lg-3 div-due">
                    <div class="push-20-t">
                        Date : <?php echo $invoice[0]->CreatedAt != "0000-00-00" ? strtolower( date( 'd M Y', strtotime( $invoice[0]->CreatedAt ) ) ) : "-"; ?>
                    </div>
                    <div class="push-50-t font-w700">
                        <div class="h5 push-10-t"><b>Amount Due</b></div>
                        <div class="h2"><b>$<?php echo isset( $invoice[0]->SubTotal ) ? number_format( $invoice[0]->SubTotal, 2 ) : '0.00'; ?></b></div>
                    </div>
                    <div class="push-50-t">
                        <div class="push-10-t">PAYMENT METHOD</div>
                        <div>payment@seedomega.com</div>
                    </div>
                </div>
                */ ?>

                <div class="col-xs-12 col-sm-12 col-lg-12 div-table">
                    <div class="text-center">
                        <!-- Table -->
                        <table class="table table-main" style="min-width: 937px; max-width: 937px;">
                            <thead>
                                <tr>
                                    <th class="text-center">Item Descriptions</th>
                                    <th class="text-center">Details</th>
                                    <th class="text-center">Invoice Date</th>
                                    <th class="text-center">Due Date</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-right" style="min-width:100px;">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( isset( $invoice ) && count( $invoice ) ){
                                    foreach( $invoice as $inv ){ ?>
                                        <tr>
                                            <td>
                                                <p class="font-w600 push-10"><?php echo isset( $inv->InvoiceType ) && $inv->InvoiceType != '' ? $inv->InvoiceType : $inv->Type; ?></p>
                                                <table class="table-transparent" style="min-width:200px;">
                                                    <?php if( isset( $inv->SubscriptionRate ) && $inv->Type == 'Booking Commission' ){ ?>
                                                        <tr>
                                                            <td><small class="text-muted text-darkgray">Subscription Rate:</small> </td>
                                                            <td style="padding-left:20px;"><small class="text-muted text-darkgray"><?php echo number_format( $inv->SubscriptionRate, 2 ); ?>%</small></td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="table-transparent" style="min-width:250px;">
                                                    <?php if( isset( $inv->BookingID ) ){ ?>
                                                        <tr>
                                                            <td class="w70"><small class="text-muted text-darkgray">Booking ID:&nbsp;&nbsp;</small></td>
                                                            <td><small class="text-muted text-darkgray"><?php echo $inv->BookingID ?></small></td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php if( isset( $inv->Investor ) ){ ?>
                                                        <tr>
                                                            <td class="w70"><small class="text-muted text-darkgray">Investor:</small></td>
                                                            <td><small class="text-muted text-darkgray"><?php echo $inv->Investor ?></small></td>
                                                        </tr>
                                                    <?php } ?>

                                                    <?php if( isset( $inv->TotalAmountAttached ) ){ ?>
                                                        <tr>
                                                            <td class="w70"><small class="text-muted text-darkgray">Amount:</small></td>
                                                            <td><small class="text-muted text-darkgray">$<?php echo number_format( $inv->TotalAmountAttached, 2 ); ?></small></td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </td>
                                            <td class="no-wrap"><small class="text-muted text-darkgray"><?php echo $inv->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $inv->CreatedAt ) ) : "-"; ?></small></td>
                                            <td class="no-wrap"><small class="text-muted text-darkgray"><?php echo $inv->DueDate != "0000-00-00" ? date( 'j M Y', strtotime( $inv->DueDate ) ) : "-"; ?></small></td>
                                            <td class="text-center"><span class="badge badge-primary"><?php echo isset( $inv->Qty ) ? $inv->Qty : '1'; ?></span></td>
                                            <td class="text-right" style="min-width: 140px;">$<?php echo isset( $inv->ItemAmount ) ? number_format( $inv->ItemAmount, 2 ) : '0.00'; ?></td>
                                        </tr>                                        
                                <?php } } ?>
                            </tbody>
                        </table>
                        <!-- END Table -->
                    </div>
                </div>
            </div>

            <div class="row pull-right push-0-r">
                <table class="table summary-total">
                    <tbody>
                        <tr>
                            <td class="font-w600 text-right">Sub-Total</td>
                            <td class="text-right">$<?php echo isset( $invoice[0]->SubTotal ) ? number_format( $invoice[0]->SubTotal, 2 ) : '0.00'; ?></td>
                        </tr>
                        <!-- <tr>
                            <td class="font-w600 text-right">Vat Rate</td>
                            <td class="text-right">20%</td>
                        </tr>
                        <tr>
                            <td class="font-w600 text-right">Vat Due</td>
                            <td class="text-right">$ 5.500,00</td>
                        </tr> -->
                        <tr>
                            <td class="font-w700 text-right">Grand Total</td>
                            <td class="font-w700 text-right no-wrap">$<?php echo isset( $invoice[0]->Total ) ? number_format( $invoice[0]->Total, 2 ) : '0.00'; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>

            <div class="row">
                <!-- Company Info -->
                <div class="col-xs-6 col-sm-6 col-lg-6">
                    <address>
                        <div class="border-bottom">
                            <i class="fa fa-map-marker"></i> 18 Marina Boulevard #35-08
                            <div class="push-15-l">Singapore 018980</div>
                        </div>
                        <div class="push-10-t">
                            <i class="fa fa-phone"></i> +65 6651 4886<br>
                            <i class="fa fa-globe"></i> www.seedomega.com<br>
                            <i class="fa fa-envelope-o"></i> info@seedomega.com
                        </div>
                    </address>
                </div>
                <!-- END Company Info -->
            </div>

            <!-- Footer -->
            <hr class="hidden-print">
            <p class="text-muted text-center"><small>Thank you very much for doing business with us. We look forward to working with you again!</small></p>
            <!-- END Footer -->
        </div>
    </div>
    <!-- END Invoice -->
</div>
