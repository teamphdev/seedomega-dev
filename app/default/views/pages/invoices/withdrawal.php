<?php
View::$title = 'Referral Commission Withdrawals';
View::$bodyclass = 'dev';
View::header();
?>
<?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page">
        <?php echo View::getMessage(); ?>
        <!-- Stats -->
        <div class="content bg-white border-b push-20">
            <div class="row items-push text-uppercase">
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">All Withdrawals</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $allWithdrawalCount; ?></a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-clock"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $approvedCount; ?></a>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Paid</div>
                    <div class="text-muted animated fadeIn"><small><i class="si si-check"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $paidCount; ?></a>
                </div>
            </div>
        </div>
        <!-- END Stats -->

        <div class="block">
            <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                <li class="active">
                    <a href="#withdrawal-pending">Pending <span class="badge badge-warning"><?php echo $pendingCount; ?></span></a>
                </li>
                <li class="">
                    <a href="#withdrawal-approved">Approved <span class="badge badge-success"><?php echo $approvedCount; ?></span></a>
                </li>
                <li class="">
                    <a href="#withdrawal-paid">Paid <span class="badge badge-success"><?php echo $paidCount; ?></span></a>
                </li>
            </ul>
            <div class="block tab-content">

                <!-- Pending Withdrawal -->
                <div class="tab-pane fade fade-up in active" id="withdrawal-pending">
                    <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center"><!-- <i class="fa fa-suitcase text-gray"></i> --> Withdrawal</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-ticket text-gray"></i> --> Payee</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bar-chart-o text-gray"></i> --> Subscription Rate</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-user text-gray"></i> --> Investor</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bank text-gray"></i> --> Project</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> Invested</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> SO Commission</th>
                            <!-- <th class="text-center hidden-xs"><i class="fa fa-ticket text-gray"></i> Status</th> -->
                            <th class="text-center hidden-xs hidden-sm"><!-- <i class="fa fa-money text-gray"></i> --> Commission Amount</th>
                            <th class="text-center" style="min-width: 90px;"><!-- <i class="fa fa-wrench text-gray"></i> --> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( isset( $pendingWithdrawals ) && count( $pendingWithdrawals ) ){
                            foreach( $pendingWithdrawals as $with ):

                                switch( $with->Status ){
                                    case 'Pending':
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$with->Status.'</span>';
                                        break;
                                    case 'Approved':
                                        $statusfield = '<span class="label label-success"><i class="si si-clock"></i> '.$with->Status.'</span>';
                                        break;
                                    case 'Paid':
                                        $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$with->Status.'</span>';
                                        break;
                                }
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/invoices/withdrawal/view/<?php echo $with->EarningID;?>">Withdrawal #<?php echo isset( $with->EarningID ) ? $with->EarningID: "-"; ?></a>
                                            <br><?php echo $statusfield; ?>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <div class="project-info">
                                                <small><?php echo isset( $with->CreatedAt ) ? date( 'Y-m-d', strtotime( $with->CreatedAt ) ) : "-"; ?></small>
                                            </div>
                                        </div>
                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                                    <span><?php echo isset( $with->CreatedAt ) ? date( 'Y-m-d', strtotime( $with->CreatedAt ) ) : "-"; ?></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <a href="/users/personal/<?php echo $with->ReferrerUserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Personal" target="_blank"><?php echo isset( $with->ReferrerName ) ? $with->ReferrerName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-right hidden-xs">
                                        <?php echo isset( $with->CommissionRate ) ? number_format( $with->CommissionRate, 2 ) : '25'; ?>%
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <a href="/users/personal/<?php echo $with->InvestorUserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Personal" target="_blank"><?php echo isset( $with->InvestorName ) ? $with->InvestorName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <a href="/projects/view/<?php echo $with->ClientProfileID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Information" target="_blank"><?php echo isset( $with->CompanyName ) ? $with->CompanyName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $with->TotalAmountAttached ) ? number_format( $with->TotalAmountAttached, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $with->SOCommission ) ? number_format( $with->SOCommission, 2 ) : '0.00'; ?>
                                    </td>
                                    <!-- <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td> -->
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $with->ReferralCommission ) ? "$".number_format( $with->ReferralCommission, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">

                                        <!-- <div class="btn-group view-info">
                                            <a href="/invoices/withdrawal/view/<?php echo $with->EarningID; ?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="">View Details</a>
                                        </div> -->

                                        <div class="">
                                            <div class="edit-commission-rate btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="" data-locations='<?php echo $with->JSON; ?>' onclick="approveWithdrawal(this)">Approve
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="9">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END Pending Withdrawal -->

                <!-- Approved Withdrawal  -->
                <div class="tab-pane fade fade-up" id="withdrawal-approved">
                    <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center"><!-- <i class="fa fa-suitcase text-gray"></i> --> Withdrawal</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-ticket text-gray"></i> --> Payee</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bar-chart-o text-gray"></i> --> Subscription Rate</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-user text-gray"></i> --> Investor</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bank text-gray"></i> --> Project</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> Invested</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> SO Commission</th>
                            <!-- <th class="text-center hidden-xs"><i class="fa fa-ticket text-gray"></i> Status</th> -->
                            <th class="text-center hidden-xs hidden-sm"><!-- <i class="fa fa-money text-gray"></i> --> Commission Amount</th>
                            <th class="text-center" style="min-width: 90px;"><!-- <i class="fa fa-wrench text-gray"> --></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( isset( $approvedWithdrawals ) && count( $approvedWithdrawals ) ){
                            foreach( $approvedWithdrawals as $with ):

                                switch( $with->Status ){
                                    case 'Pending':
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$with->Status.'</span>';
                                        break;
                                    case 'Approved':
                                        $statusfield = '<span class="label label-success"><i class="si si-clock"></i> '.$with->Status.'</span>';
                                        break;
                                    case 'Paid':
                                        $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$with->Status.'</span>';
                                        break;
                                }
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/invoices/withdrawal/view/<?php echo $with->EarningID;?>">Withdrawal #<?php echo isset( $with->EarningID ) ? $with->EarningID: "-"; ?></a>
                                            <br><?php echo $statusfield; ?>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <div class="project-info">
                                                <small><?php echo isset( $with->CreatedAt ) ? date( 'Y-m-d', strtotime( $with->CreatedAt ) ) : "-"; ?></small>
                                            </div>
                                        </div>
                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                                    <span><?php echo isset( $with->CreatedAt ) ? date( 'Y-m-d', strtotime( $with->CreatedAt ) ) : "-"; ?></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <a href="/users/personal/<?php echo $with->ReferrerUserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Personal" target="_blank"><?php echo isset( $with->ReferrerName ) ? $with->ReferrerName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-right hidden-xs">
                                        <?php echo isset( $with->CommissionRate ) ? number_format( $with->CommissionRate, 2 ) : '25'; ?>%
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <a href="/users/personal/<?php echo $with->InvestorUserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Personal" target="_blank"><?php echo isset( $with->InvestorName ) ? $with->InvestorName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <a href="/projects/view/<?php echo $with->ClientProfileID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Information" target="_blank"><?php echo isset( $with->CompanyName ) ? $with->CompanyName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $with->TotalAmountAttached ) ? number_format( $with->TotalAmountAttached, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $with->SOCommission ) ? number_format( $with->SOCommission, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $with->ReferralCommission ) ? "$".number_format( $with->ReferralCommission, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="">
                                            <div class="edit-commission-rate btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="" data-locations='<?php echo $with->JSON; ?>' onclick="payWithdrawal(this)">Process Payment
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="9">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END Approved Withdrawal -->

                <!-- Paid Withdrawal  -->
                <div class="tab-pane fade fade-up" id="withdrawal-paid">
                    <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center"><!-- <i class="fa fa-suitcase text-gray"></i> --> Withdrawal</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-ticket text-gray"></i> --> Payee</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bar-chart-o text-gray"></i> --> Subscription Rate</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-user text-gray"></i> --> Investor</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-bank text-gray"></i> --> Project</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> Invested</th>
                            <th class="text-center hidden-xs"><!-- <i class="fa fa-dollar text-gray"></i> --> SO Commission</th>
                            <!-- <th class="text-center hidden-xs"><i class="fa fa-ticket text-gray"></i> Status</th> -->
                            <th class="text-center hidden-xs hidden-sm"><!-- <i class="fa fa-money text-gray"></i> --> Commission Amount</th>
                            <!-- <th class="text-center" style="min-width: 90px;"><i class="fa fa-wrench text-gray"></i> Actions</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( isset( $paidWithdrawals ) && count( $paidWithdrawals ) ){
                            foreach( $paidWithdrawals as $with ):

                                switch( $with->Status ){
                                    case 'Pending':
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$with->Status.'</span>';
                                        break;
                                    case 'Approved':
                                        $statusfield = '<span class="label label-success"><i class="si si-clock"></i> '.$with->Status.'</span>';
                                        break;
                                    case 'Paid':
                                        $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$with->Status.'</span>';
                                        break;
                                }
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/invoices/withdrawal/view/<?php echo $with->EarningID;?>">Withdrawal #<?php echo isset( $with->EarningID ) ? $with->EarningID: "-"; ?></a>
                                            <br><?php echo $statusfield; ?>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <div class="project-info">
                                                <small><?php echo isset( $with->CreatedAt ) ? date( 'Y-m-d', strtotime( $with->CreatedAt ) ) : "-"; ?></small>
                                            </div>
                                        </div>
                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-12 col-sm-12 col-md-12 text-left">
                                                    <span><?php echo isset( $with->CreatedAt ) ? date( 'Y-m-d', strtotime( $with->CreatedAt ) ) : "-"; ?></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <a href="/users/personal/<?php echo $with->ReferrerUserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Personal" target="_blank"><?php echo isset( $with->ReferrerName ) ? $with->ReferrerName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-right hidden-xs">
                                        <?php echo isset( $with->CommissionRate ) ? number_format( $with->CommissionRate, 2 ) : '25'; ?>%
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <a href="/users/personal/<?php echo $with->InvestorUserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Personal" target="_blank"><?php echo isset( $with->InvestorName ) ? $with->InvestorName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-left hidden-xs">
                                        <a href="/projects/view/<?php echo $with->ClientProfileID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Information" target="_blank"><?php echo isset( $with->CompanyName ) ? $with->CompanyName : ''; ?></a>
                                    </td>
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $with->TotalAmountAttached ) ? number_format( $with->TotalAmountAttached, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-right hidden-xs no-wrap">
                                        $<?php echo isset( $with->SOCommission ) ? number_format( $with->SOCommission, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h4 text-right text-primary no-wrap">
                                        <?php echo isset( $with->ReferralCommission ) ? "$".number_format( $with->ReferralCommission, 2 ) : "-"; ?> <br>
                                        <div class="push-5-t visible-xs">
                                            <div class="">
                                                <a href="/invoices/withdrawal/view/<?php echo $with->EarningID; ?>" class="btn btn-sm btn-default btn-rounded" data-toggle="tooltip" title="">View Details</a>
                                            </div>
                                        </div>
                                    </td>
                                    <!-- <td class="h5 text-primary text-center hidden-xs">
                                        <div class="btn-group">
                                            <div class="edit-commission-rate btn btn-xs btn-default" data-toggle="tooltip" title="Approve" data-locations='<?php echo $with->JSON; ?>' onclick="editWithdrawal(this)"><i class="fa fa-pencil"></i>
                                            </div>
                                        </div>
                                    </td> -->
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="8">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END Paid Withdrawal -->

            </div>
        </div>

    </article>

    <div id="dialog" class="w3-modal modal-content">
        <div class="w3-modal-content block block-themed remove-margin-b" style="width:500px;">
            <div id="dialog-form" class="text-center">
                <button type="button" class="close" aria-label="Close" onclick="document.getElementById('dialog').style.display='none'">
                    <i class="si si-close text-red"></i>
                </button>
                <hr>
                <div class="modal-body heading"><h3 id="Name"></h3></div>
            </div>
            <form enctype="multipart/form-data" method="post">
                <input type="hidden" name="action" value="approvewithdrawal" />
                <input type="hidden" name="withdraw[EarningID]" value="" id="EarningID" />

                <div id="dialog-form" class="block-content text-center comm-rate">

                    <div class="form-group" id="withdraw-form">

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Subscription Rate</span>
                            <input type="text" id="iSubscriptionRate" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Investor Name</span>
                            <input type="text" id="iInvestorName" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Project</span>
                            <input type="text" id="iProject" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Invested Amount</span>
                            <input type="text" id="iInvestedAmount" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">SO Commission</span>
                            <input type="text" id="iSOCommission" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Commission Amount</span>
                            <input type="text" id="iCommissionAmount" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="clear"></div>

                        <hr>
                    
                        <button class="btn btn-primary clear col-md-4 col-xs-12" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Approve</button>
                    </div>

                </div>
            </form>
            <div id="dialog-form" class="text-center">
                <div class="text-reminder text-white">Update Withdrawal Request</div>
            </div>
        </div>
    </div>

    <div id="dialogp" class="w3-modal modal-content">
        <div class="w3-modal-content block block-themed remove-margin-b" style="width:500px;">
            <div id="dialog-form" class="text-center">
                <button type="button" class="close" aria-label="Close" onclick="document.getElementById('dialogp').style.display='none'">
                    <i class="si si-close text-red"></i>
                </button>
                <hr>
                <div class="modal-body heading"><h3 id="pName"></h3></div>
            </div>
            <form enctype="multipart/form-data" method="post">
                <input type="hidden" name="action" value="paidwithdrawal" />
                <input type="hidden" name="withdraw[EarningID]" value="" id="pEarningID" />

                <div id="dialog-form" class="block-content text-center comm-rate">

                    <div class="form-group" id="withdraw-form">

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Subscription Rate</span>
                            <input type="text" id="pSubscriptionRate" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Investor Name</span>
                            <input type="text" id="pInvestorName" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Project</span>
                            <input type="text" id="pProject" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Invested Amount</span>
                            <input type="text" id="pInvestedAmount" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">SO Commission</span>
                            <input type="text" id="pSOCommission" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group">
                            <span class="input-group-addon">Commission Amount</span>
                            <input type="text" id="pCommissionAmount" value="" class="form-control" readonly="readonly">
                          </div>
                        </div>

                        <div class="clear"></div>

                        <hr>
                    
                        <button class="btn btn-primary clear col-md-4 col-xs-12" type="submit" data-dismiss="modal"><i class="fa fa-check"></i> Paid</button>
                    </div>

                </div>
            </form>
            <div id="dialog-form" class="text-center">
                <div class="text-reminder text-white">Process Payment for Withdrawal Request</div>
            </div>
        </div>
    </div>

</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });

    function approveWithdrawal( el ){
        var data = $( el ).data( 'locations' );
        $( '#Name' ).text( 'PAYEE: ' + data['ReferrerName'] );
        $( '#EarningID' ).val( data['EarningID'] );
        $( '#iSubscriptionRate' ).val( data['CommissionRate'] + '%' );
        $( '#iInvestorName' ).val( data['InvestorName'] );
        $( '#iProject' ).val( data['CompanyName'] );
        $( '#iInvestedAmount' ).val( '$' + commaSeparateNumber( data['TotalAmountAttached'] ) );
        $( '#iSOCommission' ).val( '$' + commaSeparateNumber( data['SOCommission'] ) );
        $( '#iCommissionAmount' ).val( '$' + commaSeparateNumber( data['ReferralCommission'] ) );

        $( '#dialog' ).css( 'display', 'block' );
    }

    function payWithdrawal( el ){
        var data = $( el ).data( 'locations' );
        $( '#pName' ).text( 'PAYEE: ' + data['ReferrerName'] );
        $( '#pEarningID' ).val( data['EarningID'] );
        $( '#pSubscriptionRate' ).val( data['CommissionRate'] + '%' );
        $( '#pInvestorName' ).val( data['InvestorName'] );
        $( '#pProject' ).val( data['CompanyName'] );
        $( '#pInvestedAmount' ).val( '$' + commaSeparateNumber( data['TotalAmountAttached'] ) );
        $( '#pSOCommission' ).val( '$' + commaSeparateNumber( data['SOCommission'] ) );
        $( '#pCommissionAmount' ).val( '$' + commaSeparateNumber( data['ReferralCommission'] ) );

        $( '#dialogp' ).css( 'display', 'block' );
    }
</script>