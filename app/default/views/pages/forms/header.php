<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <link rel="icon" href="<?php echo View::url('assets/images/logo-s.png'); ?>" type="image/png" sizes="16x16"> 
        <title><?php echo View::$title . ' | '. Config::get('SITE_TITLE'); ?></title>
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet"> 
        <?php View::headers(); ?>
    </head>
    <body class="<?php echo View::$bodyclass; ?>">
        <div id="wrapall"> 