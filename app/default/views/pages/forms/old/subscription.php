<?php 
View::$title = 'Odeon & Co. Subscription Form';
View::$bodyclass = 'guest';
View::header('forms'); 
?>

    <!-- page content -->
    <div class="body-wrap">
        <div class="page-header">
            <div class="container">
                <h1>Odeon & Co. Subscription Form</h1>
            </div>
        </div>
        
        <form id="master_form" action="" method="post" class="form-horizontal subscription-form" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo md5(microtime()); ?>">        
        <input type="hidden" name="action" value="addcasefile">
        <input type="hidden" name="acct[FormType]" value="Traditional">
        <div class="section-holder container">
            <?php echo View::getMessage(); ?> 
            
            <!-- Section 1 GENERAL GUIDANCE -->
            <div class="section section-1">
                <div class="s-header">
                    <span><strong>01. 概括指引 GENERAL GUIDANCE</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>您申購的投資信託計劃《傲鼎麒麟投資信託》(<strong>Odeon Investment Trust</strong>)，是由 <strong>Odeon & Co. Pte Ltd</strong> (簡稱“<strong>Odeon</strong>”) 所提供與信託公司 <strong>Zico Allshores Trust</strong> 集團（簡稱“信託公司”）監督監管；以募集資金及投資存款，來提供申購人投資回報。</p>
                        <p>此申購人開戶程序應由申購人親自填寫。本公司的信託申購協議規範了投資信託的條款和條件，請與此申購書一起閱讀。感謝您的理解並希望您能配合本公司要求提供所需之文件。</p>
                        <p>請完整填妥此申購開戶書並按要求簽字，以方便本公司審核您的申請。如未能提供相關資料和證明文件將導致申購開戶有所延誤。附加資料應包括在內以作為本申請的佐證資料。在本申購開戶書非指定地方提供的任何額外資料、備註或指令將不會被執 行。</p>
                        <p><span style="color:red; font-weight:bold;">注:</span> 如本中文版本與英文版本內容有差異，將以英文版本為準。</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p>Odeon Investment Trust is an investment deposit program offered by <strong>Odeon & Co. Pte Ltd</strong> (“<strong>Odeon</strong>”) with Trust Agent <strong>Zico Allshores Trust Pte Ltd</strong> (“Esrow Trustee”) for the purpose of receiving and investing deposits to provide a return for Subscribers. </p>
                        <p>This Account Opening Package should only be completed by the applicant. This form should be read in conjunction with the Subscription Agreement and the Term Sheet. We seek your understanding and cooperation in fur
                        nishing the required documents and appreciate your time and effort in doing so.</p>
                        <p>It is important that you complete this Subscription form in full, and sign as required to enable us to consider your Subscription. Kindly attach necessary documents. Failure to provide all relevant information and supporting documentation will result in a delay to the Subscription being processed. Additional information may be included to support this subscription. Any additional details/notes/instructions or those provided at a  non-designated area of the form may not be executed</p>
                        <p><span style="color:red; font-weight:bold;">Note:</span> If the Chinese version and English version of the content is different, the English version shall prevail..</p>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 2 PRIVACY POLICY -->
            <div class="section section-2">
                <div class="s-header">
                    <span><strong>02. 隱私政策 PRIVACY POLICY</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>除非在《打擊洗錢及恐怖分子資金籌集(金融機構)條例》或《稅務條例》或法庭指令的要求下，否則 <strong>Odeon</strong> 不會公開及洩露客戶任何資料; 在信託行政需要外，不論基於任何目的，<strong>Odeon</strong> 一般不應透露關於信託存款的資料，也不會使用受益人的資料，所有有關信託申購人資料是受到保護的。</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p>The Escrow Trustee and Odeon will not disclose information on the Subscriber and their affairs except as required under applicable laws, including the AntiMoney Laundering and Counter-Terrorist Financing (Financial Institutions) Ordinance (“AML Ordinance “). Odeon shall not disclose information on trust escrow deposits to any third party, except as required under applicable laws, including the AML Ordinance and the Inland Revenue Ordinance or otherwise as directed by a court order.</p>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 3 POWER OF ATTORNEY -->
            <div class="section section-3 POA-area acounttype_corporate">
                <div class="s-header">
                    <span><strong>03. 委託書 POWER OF ATTORNEY <small>(法人機構填寫, 個人申報免填 | for corporate clients only)</small></strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-12 col-xs-12 form-inline">
                        <p>By this Power of Attorney dated <input id="POADate" class="form-control POADate isdatepicker" type="text" name="case[POADate]" placeholder="YYYY-MM-DD">, <input type="text" class="form-control POAFirstName" name="case[POAFirstName]" style="width: 30%;" placeholder="First Name"> <input type="text" class="form-control POALastName" name="case[POALastName]" style="width: 30%;" placeholder="Last Name"></p>
                        <p><input type="text" class="form-control POACompanyName" name="case[POACompanyName]"style="width: 42%;" placeholder="Company Name"> (申購公司 NAME OF SUBSCRIBER COMPANY), (商業登記號碼公司編號 COMPANY NUMBER :</p>
                        <p><input type="text" class="form-control POACompanyNumber" name="case[POACompanyNumber]" style="width: 25%;" placeholder="Company Number">), a Company registered in <select id="Country" name="case[POACompanyCountry]" class="form-control POACompanyCountry" style="width:25%">
                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                <?php } ?>
                            </select> (註冊地 PLACE OF INCORPORATION) and having</p>
                        <p>its registered office at <input type="text" class="form-control POACompanyAddress" name="case[POACompanyAddress]" style="width: 55%;" placeholder="Company Addres"> <input type="text" name="case[POACompanyCity]" class="form-control POACompanyCity" style="width: 25%;" placeholder="Company City"></p>
                        <p><input type="text" class="form-control POACompanyState" name="case[POACompanyState]" style="width: 61%;" placeholder="Company State"> (公司地址 FULL ADDRESS) (hereinafter called the “申購公司</p>
                        <p>SUBSCRIBER COMPANY”) hereby appoint <input type="text" class="form-control POAAppointor" name="case[POAAppointor]" style="width: 36%;" placeholder="Appointor"> (法定代表人 APPOINTOR, 護照號碼 PASSPORT NO.</p>
                        <p><input type="text" class="form-control POAAppointorIdNumber" name="case[POAAppointorIdNumber]" style="width: 30%;" placeholder="Appointor Id Number">) to act as the true and lawful Attorney of the Company and in the name of the Company and on the Company’s
                            behalf to perform all acts and things, including signing and execution of all necessary documents in relation to the Company, including any
                            amendments or modifications and etc, in the best interest of the Company to subscribe Odeon Investment Trust. This Power of Attorney shall be
                            valid and of full effect until revoke by writing.</p><Br>
                        <p>此授權書由申購公司 (SUBCRIBER COMPANY) 商業登記號碼，註冊地，公司地址）於以上日期填寫，委託法定代表人（護照號碼為以上填寫）為本公司法定代表人，並以公司名義及代表公司執行公司之一切事務，包括簽署及執行所有與公司相關及必須之文件，包括任何修訂或修改等，以公司之最大利益為前提，認購《傲鼎投資信託》(Odeon Investment Trust)。此授權書具法律效力，直至以書面通知撤銷。</p><br>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p>申購公司法定代表人 <br>
                        The Company Seal was affixed hereunto in the presence
                        of the Directors (and Secretary, if applicable).</p>                    
                    </div>
                    <div class="col-sm-6 col-xs-12 POA-seals form-group">
                        <div class="g-header-2"><label>申購公司蓋章 Subscriber Company Corporate Seal</label></div>
                        <p>
                            <input class="file" type="file" data-show-upload="false" name="POACorporateSeal[]" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                        </p> 
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group"> 
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group"> 
                        <label>法定代表人 Director Signature</label>
                        <div class="signatureSection" upload-name="POADirectorSign" data-name="signatures[POADirectorSign]">
                            <div class="radio signOptions">
                                <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                            </div>
                            <div class="parentsign">
                                <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                <div class="sign-upload hidetag">
                                    <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                </div>
                            </div>
                        </div>
                        <p><input type="text" class="form-control" name="case[POADirectorName]" placeholder="Director Name"></p>

                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <label>董事 Director / 監察人 Secretary Signature</label>
                        <p><input class="file" type="file" data-show-upload="false" name="POASecretarySign[]" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                        <p><input type="text" class="form-control" name="case[POASecretaryName]" placeholder="Secretary Name"></p>
                    </div>-->
                </div>
                
            </div>
            
            <!-- Section 4 ACCOUNT TYPE -->
            <div class="section section-4">
                <div class="s-header">
                    <span><strong>04. 帳戶類型 ACCOUNT TYPE</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p><label><input type="radio" class="naccount" id="TrustAccountTypeIndividual" name="acct[AccountType]" value="Individual" checked="checked"> <strong>INDIVIDUAL 個人</strong></label><br>
                        (如選擇個人，免填寫第 5 段)<br>
                        (Please Ignore Section 5 If Individual is Selected)</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p><label><input type="radio" class="naccount" id="TrustAccountTypeCorporate" name="acct[AccountType]" value="Corporate"> <strong>CORPORATE 機構</strong></label><Br>
                        (如選擇機構 , 請填寫第 5 段)<Br>
                        (Please Fill Up Section 5 If Corporate is Selected)</p>
                    </div>
                    
                </div>
                
            </div>
            
            <!-- Section 5 CORPORATE CLIENT INFORMATION -->
            <div class="section section-5 acounttype_corporate">
                <div class="s-header">
                    <span><strong>05. 機構客戶資訊 CORPORATE CLIENT INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>機構公司名稱 Company Name</label></p>
                        <p><input type="text" id="CompanyName" name="acct[CompanyName]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>機構公司註冊號碼 Registration No / Certificate of Incorporation No.</label></p>
                        <p><input type="text" id="RegistrationNo" name="acct[RegistrationNo]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>註冊地址 Registered Address</label></p>
                        <p><textarea rows="3" id="RegistrationAddress" name="acct[RegistrationAddress]" class="form-control"></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>機構公司註冊國家 Country of Registration / Incorporation</label></p>
                        <select id="CRINC" name="acct[RegistrationCountry]" class="form-control">
                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>電話號碼 Telephone</label></p>
                        <p><input type="text" id="CRINCTelephone" name="acct[RegistrationTelephone]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>主要經營地址 <small><strong>（如與註冊地址不同）</strong></small> Principal Business Address <small>(If different from Registered Address)</small></label></p>
                        <p><textarea rows="3" id="PBAddress" name="acct[BusinessAddress]" class="form-control" value=""></textarea></p>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 6 APPLICANT’S PERSONAL DETAILS -->
            <div class="section section-6">
                <div class="s-header">
                    <span><strong>06. 申購人的個人資料（個人客戶專用） APPLICANT’S PERSONAL DETAILS</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Agent ID / Referrer ID</label></p>
                        <p><input type="text" id="ReferrerUserChecker" class="form-control" name="user[ReferrerUserID]" value="" required="required" rel="<?php echo View::url('ajax/userinfo'); ?>" ><span id="referrerloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Referrer Name</label></p>
                        <p><input type="text" id="referrerdata" class="form-control" value="" disabled=""></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>稱呼 Title</label></p>
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'meta[Salutation]',
                                    'options'=>AppUtility::getSalutation(),
                                    'value'=>'Mr',  
                                    'class'=>'form-control',
                                    'id'=>'Salutation',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>護照/附相片身分證號碼 Passport / Photo ID Number</label></p>
                        <p><input type="text" id="IdNumber" name="meta[IdNumber]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>名字 First Name</label></p>
                        <p><input type="text" id="FirstName" name="meta[FirstName]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>國籍 Country of Citizenship</label></p>
                        <p>
                        <select id="Country" name="meta[Country]" class="form-control">
                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                            <?php } ?>
                        </select> 
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>姓氏 Last Name</label></p>
                        <p><input type="text" id="LastName" name="meta[LastName]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>稅務居住國 Country of Tax Residency</label></p>
                        <p><select id="Country" name="acct[BusinessCountry]" class="form-control">
                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                <?php } ?>
                            </select>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>出生日期 Date of Birth</label></p>
                        <div class="col-md-3 col-sm-3 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Month]',
                                    'options'=>AppUtility::getMonth(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Month',
                                    'placeholder'=>'Month',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Day]',
                                    'options'=>AppUtility::getDay(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Day',
                                    'placeholder'=>'Day',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Year]',
                                    'options'=>AppUtility::getYear(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Year',
                                    'placeholder'=>'Year',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <p><label>婚姻狀況 Marital Status</label></p>
                        <p><?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'meta[CivilStatus]',
                                    'options'=>AppUtility::getCivilStatus(),
                                    'value'=>'SINGLE',
                                    'class'=>'form-control',
                                    'id'=>'CivilStatus',
                                    'custom'=>'required'
                                )
                            ); 
                        ?></p>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <p><label>性别 Gender</label></p>
                        <p><select id="Gender" name="meta[Gender]" class="form-control" required>
                                <option value="M">Male | 男</option>
                                <option value="F">Female | 女</option>
                        </select></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>職業及職位 <small>(如退休，請註明退休，或退休前的職位)</small> <br> Occupation & Job Title <small>(if retired, state retired and former occupation)</small></label></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><input type="text" id="Occupation" name="meta[Occupation]" class="form-control" placeholder="職業 | Occupation" value="" required="required"></p>
                        <p><input type="text" id="JobTitle" name="meta[JobTitle]" class="form-control" placeholder="職位 | Job Title" value="" required="required"></p>
                    </div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>選擇語言 Prefered Language</label></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p>
                        <select id="Language" name="meta[Language]" class="form-control" required>
                                <option value="en">英語 | English</option>
                                <option value="tcn">繁體中文 Traditional Chinese</option>
                                <option value="scn">簡體中文 Simplified Chinese</option>
                        </select>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>居住地址 Primary Residential Address</label></p>
                        <p><input type="text" id="Address" name="meta[Address]" class="form-control" value="" required="required"></p>
                        <p><input type="text" id="Address2" name="meta[Address2]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>通訊地址 <small>（如與居住地址不同）</small> Correspondence Address</label></p>
                        <p><input type="text" id="CorrespondenceAddress" name="meta[Address3]" class="form-control" value=""></p>
                        <p><input type="text" id="CorrespondenceAddress2" name="meta[Address4]" class="form-control" value=""></p>
                        <p><span class="help-block">(if different from residential address)</span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>手機 Mobile</label></p>
                        <p><input type="text" id="Mobile" name="meta[Mobile]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>電郵 Email</label></p>
                        <p><input type="email" id="EmailChecker" name="acct[AccountEmail]" class="form-control" value="" data-validate-linked="email" required="required" rel="<?php echo View::url('ajax/checkclientemail'); ?>" checkmessage="Email already exists please enter a unique one or contact the administrator. 电子邮件已存在，请输入唯一的或联系管理员。"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></p>
                        <p>將以電子郵件作為與受託人（申購人）基本的聯繫方式 <br>The primary form of correspondence will be via email.</p>
                    </div>
                    
                </div>
                
            </div>
            
            <!-- Section 7 SUBSCRIBER BANK ACCOUNT INFORMATION -->
            <div class="section section-7">
                <div class="s-header">
                    <span><strong>07. 申購人銀行帳號資料 SUBSCRIBER BANK ACCOUNT INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>收款銀行地址 Bank Address</label></p>
                        <p><textarea rows="3" id="BankAddress" name="bank[Address]" class="form-control" value="" required></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>收款銀行名稱 Bank Name</label></p>
                        <p><input type="text" id="BankName" name="bank[Name]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶持有人姓名 Account Name</label></p>
                        <p><input type="text" id="BankAccountName" name="bank[AccountName]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶號碼 Account Number</label></p>
                        <p><input type="text" id="BankAccountNumber" name="bank[AccountNumber]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行國際代碼/城市代碼 SWIFT Code</label></p>
                        <p><input type="text" id="SwiftCode" name="bank[SwiftCode]" class="form-control" value="" required="required"></p>
                    </div>
                </div> 
            </div>
            
            <!-- Section 8 ULTIMATE BENEFICIAL OWNER (UBO) DECLARATION< -->
            <div class="section section-8">
                <div class="s-header">
                    <span><strong>08. 最終受益方聲明 （所指為信託受益人） ULTIMATE BENEFICIAL OWNER (UBO) DECLARATION</strong></span>
                </div>
                
                <div class="panel-body ubo-sect">
                    <p>I, <input type="text" placeholder="(全名，如護照 full name as in passport)" name="case[UBOName]" required="required" /> of <input type="text" name="case[UBOAddress]" placeholder="(詳細住址 full residential address)" required="required" /></p>
                    <p>do hereby declare that my/our source(s) of funds which I/we have are my/our assets derived from:<br> 我，居住於以上填寫住址，特此聲明本人(我們)的資金及本人(我們)的資產來源於 : </p>
                    <section class="row">
                        <h5>請提供詳細資訊 Please tick the source of funds</h5>
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOEmploymentIncome">
                            <p style="margin: -15px 0 0; padding-bottom: 0;">Employment Income (employer, title, period, salary figures)<br>就業收入（任職公司,職務,年資,薪津金額）</p></label>
                            
                            <div id="UBOEmploymentIncome" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOEmploymentIncome[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                        
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOCommission">
                            <p style="margin: -15px 0 0; padding-bottom: 0;">Sales / Commission Income (details of company(ies) and value figures)<br>銷售/佣金收入（公司的詳細情況及相關資料）</p></label>
                            <div id="UBOCommission" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOCommission[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                      
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOBusiness"> 
                            <p style="margin: -15px 0 0; padding-bottom: 0;">Business Income (business particulars and financial statements to determine general turnover and profits per respective years)<br>營業收入（營業相關資料與財務報表，確認每個相應年度的營業收入）</p></label>
                            <div id="UBOBusiness" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOBusiness[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                       
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOInheritance"> 
                            <p style="margin: -15px 0 0; padding-bottom: 0;">Inheritance (inheritance lineage and value)<br>繼承（繼承血統及其價值）</p></label>
                            <div id="UBOInheritance" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOInheritance[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                         
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOGift">                                             
                            <p style="margin: -15px 0 0; padding-bottom: 0;">Gift (the details of giver and details of gift and its value)<br>贈與物（贈與者及贈與物之細節及其價值）</p></label>
                            <div id="UBOGift" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOGift[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                        
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOSales"> 
                            <p style="margin: -15px 0 0; padding-bottom: 0;">Proceeds from the sale of property and/or other assets (description of assets and value and how they were obtained in the first instance) <br>出售房產收益或其他資產（資產價值的描述，以及如何在第一時間取得）</p></label>
                            <div id="UBOSales" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOSales[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                         
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                           <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOOther">                                             
                           <p style="margin: -15px 0 0; padding-bottom: 0;">Any other (please describe)<br>其他補充事項（請進行描述） </p></label>
    
                            <div id="UBOOther" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOOther[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div>
                    </section>
                    
                    <p><label><input type="checkbox" id="UBODeclarationConfirm1" name="UBODeclarationConfirm" value="Y" required="required"> I further declare that :</label></p>
                    <ul style="padding-left: 20px;">
                        <li>I am not involved in money laundering and/or drug trafficking; and</li>
                        <li>any monies and/or securities that have been or will be deposited in any bank account do not originate, either directly or indirectly, from illegal and/or criminal activities.</li>
                    </ul>
                    <br>
                    <p><label>我進一步聲明 :</label></p>
                    <ul style="padding-left: 20px;">
                        <li>我沒有參與洗黑錢或販賣毒品;</li>
                        <li>已經存入或將要存入的公司銀行帳戶、及其任何款項或證券的任何款項，並無直接地或間接地參與非法或犯罪活動。</li>
                    </ul>
                </div> 
            </div>
            
            <!-- Section 9 DECLARATION OF POLITICALLY EXPOSED PERSON (“PEP”) AND TAX STATUS -->
            <div class="section section-9">
                <div class="s-header">
                    <span><strong>09. 稅務及政治人物聲明 DECLARATION OF POLITICALLY EXPOSED PERSON (“PEP”) AND TAX STATUS</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p><strong>我特此聲明：</strong></p>
                        <p>
                        <ul>
                            <li>本人不是政治人物或政治人物的密切關連人士。</li>
                            <li>本人並非美國人士<sup>1</sup>，並且不打算成為美國公民。 </li>
                            <li>本人的稅務居地及稅務識別號為：</li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p><strong>I hereby declare that :</strong> </p>
                        <p>
                        <ul>    
                            <li>I am not a PEP2 or a Closely Connection Person to a PEP3. </li>
                            <li>I am not a U.S. Person and do not intent to be one. </li>
                            <li>My tax residency and tax identification number are:</li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>主要稅務居地 Primary Tax Residency</label></p>
                        <p><textarea name="case[PrimaryTaxResidency]" class="form-control" rows="3" required></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>稅務識別號 Tax ID No.</label></p>
                        <p><textarea class="form-control" name="case[TaxIdNumber]" rows="3" required></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <ul>
                            <li>以上所有資料屬實、完整及正確無誤，並且進一步承諾即時通知 Odeon 及信託公司此資料的任何變化及同意提交申購人所需要的額外文件。 <br> &nbsp;</li>
                            <li>The above information is true, complete and accurate; and further undertake to notify Odeon and the Escrow Trustee promptly of any change in the aforementioned information and agree to submit additional documents to the Escrow Trustee upon its request.</li>
                        </ul><Br>
                        
                        <div class="g-header">
                            <strong><sup>1</sup> 就遵從美國《  海外帳戶納稅法案》而言，“美籍人士”是指：</strong>
                        </div>
                        <ul>
                            <li>個人：美國公民或合法永久居民(綠卡持有者)；或出生在美國；或者有美國的居住地址或滿足國稅局 (www.irs.gov) 定義的實際居留身份。<br>
                                Individuals : U.S. citizen or lawful permanent resident (green card holder); or born in the U.S.; or have a U.S. residential address or meeting the substantial presence test for the calendar year as defined by the IRS (www.irs.gov).<br> &nbsp;
                            </li>
                            <li> 企業：美國的合夥企業或美國公司。 <br>Corporations: a U.S. partnership or U.S. corporation <br> &nbsp;</li>
                            <li>信託：任何信託，例： (a) 美國的法院可對信託的管理行使本監管；及 (b) 一個或多個美籍人士有權控制信託的所有重大決策。 <Br>Trusts : Any trust if (a) a court within U.S. is able to exercise primary supervision over the administration of the trust; and (b) one or more U.S. persons have the authority to control all substantial decisions of the trust. </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="g-header">
                            <strong><sup>2</sup>  政治人物的例子包括： <br> <sup>2</sup>  Examples of Politically Exposed Persons include:</strong>
                        </div>
                        <p>
                        <ul>
                            <li>國家元首或政府首腦 Heads of State or Government </li>
                            <li>資深政治家 Senior Politicians </li>
                            <li>高級政府、司法或軍事官員 Senior Government, Judicial or Military Officials </li>
                            <li>國有企業的高級管理人員  Senior Executives of State-Owned Corporations </li>
                            <li>重要政黨官員 Important Political Party Officials </li>
                            <li>皇室 Royal Family</li>
                        </ul>
                        </p>
                        
                        <div class="g-header">
                            <strong><sup>3</sup> 密切關連人士的例子包括: <br><sup>3</sup> Examples of Closely Connected Persons include:</strong>
                        </div>
                        <p>
                        <ul>
                        <li>家庭成員(配偶、子女、父母、兄弟姐妹，包括姻親) <Br>Family members (spouse, children, parents, brothers and sisters, including in-laws) <br> &nbsp;</li>
                        <li>助手和其他貼身顧問(社交或專業)<br> Aides and Other Close Advisors, socially or professionally <br> &nbsp;</li>
                        <li>業務夥伴／ 戰略合作方 <br>Business associates <br> &nbsp;</li>
                        <li>高級政治人物擁有權益或影響力的公司 <Br>Companies in which a Senior Political Figure has an interest or exercises influence </li>
                        </ul>
                        </p>
                    </div>
        
                </div>
                
            </div>
            
            <!-- Section 11 TRUST ACCOUNT BANKING INFORMATION -->
            <div class="section section-10">
                <div class="s-header">
                    <span><strong>10. 信託帳戶銀行資料  TRUST ACCOUNT BANKING INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-12 col-xs-12">
                        <div class="g-header-1">信託存款，請匯款至 For Remittance into Trust</div><br>
                    </div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶名稱 Account Name:</label> Odeon & Co Pte Ltd</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶名稱 Account Name:</label> 深圳钰鼎一号创业投资企业(有限合伙）</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户名称 Account Name:</label></p>
                        <p><input type="text" id="TrustAccountName" name="trust[AccountName]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶號碼 Account Number:</label></p>
                        <p>
                        373-307-132-8（SGD 新幣）<br>
                        373-901-199-8（USD 美元）
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶號碼 Account Number:</label></p>

                        <p>
                        4000021209200754136(RMB)
                        </p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>收款人户口号码 Account Number:</label></p>
                        <p><input type="text" id="TrustAccountNumber" name="trust[AccountNumber]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行名稱 Bank Name:</label></p>
                        <p>United Overseas Bank Limited</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行名稱 Bank Name:</label></p>
                        <p>中国工商银行</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行名称 Bank Name:</label></p>
                        <p><input type="text" id="TrustName" name="trust[Name]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行國際代碼 Swift Code:</label></p>
                        <p>UOVBSGSG</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行國際代碼 Swift Code:</label></p>
                        <p>ICBKCNBJSZN</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行國際代碼 Swift Code:</label></p>
                        <p><input type="text" id="TrustSwiftCode" name="trust[SwiftCode]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行地址 Bank Address:</label></p>
                        <p>80 Raffles Place, UOB Plaza 1, Singapore 048624</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行地址 Bank Address:</label></p>
                        <p>深圳市罗湖区深南东路5002号地王大厦附楼G层</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行地址 Bank Address: </label></p>
                        <p><input type="text" id="TrustAddress" name="trust[Address]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                </div> 
            </div>
            
            <!-- Section 11 CONTRIBUTION METHODS -->
            <div class="section section-11">
                <div class="s-header">
                    <span><strong>11. 信託存款資訊 CONTRIBUTION METHODS</strong></span>
                </div>
                <!-- <pre>
                    <?php print_r($items); ?>
                </pre> --> 
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p class="hidden-xs"><img src="<?php echo view::url(); ?>/assets/images/qr.jpg"> <span style="display: inline-block; font-size: 20px; color: #78664a; position: absolute; top: 40%; margin: 0 0 0 40px;">信託存款帳戶資訊 <br><br><a href="http://tt.OdeonCo.com" style="color: #78664a; text-decoration: none;">http://tt.OdeonCo.com</a></span></p>
                        <div class="visible-xs text-center">
                            <p><img src="<?php echo view::url(); ?>/assets/images/qr.jpg" class="img-responsive" style="margin: 0 auto;"></p>
                            <p><span style="display: inline-block; font-size: 20px; color: #78664a;">信託存款帳戶資訊 <br><br><a href="http://tt.OdeonCo.com" style="color: #78664a; text-decoration: none;">http://tt.OdeonCo.com</a></span></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="form-group">
                            <p><label>Package Type | 購買商品系列(請選擇)</label></p>
                            <select name="acct[ProductID]" id="ProductID" class="form-control" required rel="<?php echo View::url('form/get/'); ?>">
                                <option selected>Select Option</option>
                                <?php
                                foreach ($prods as $prod) {
                                ?>
                                    <option value="<?php echo $prod->ProductID; ?>"><?php echo $prod->ProductName; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <p><label>Package Terms | 配套條款</label></p>
                            <select id="ProductItemID" class="form-control" required rel="<?php echo View::url('form/getPrItem/'); ?>">
                                <option selected="selected">Select</option>
                                
                            </select>
                            <input type="hidden" id="theProductItemID" name="acct[ProductItemID]" value="">
                            <input type="hidden" id="StepUp" value="">
                            <input type="hidden" id="Invested" value="">
                            <p id="couponAmount" class="help-block" style="margin-bottom: 0;"></p>
                        </div>

                        <div class="form-group">
                            <p><label>Final Amount | 投資金額</label></p>
                            <input type="text" name="acct[DepositedAmount]" class="form-control" id="DepositedAmount">
                        </div>

                    </div>     
                </div> 
            </div>
            
            <!-- Section 12 DOCUMENTATION REQUIREMENTS (INDIVIDUAL) -->
            <div class="section section-12 acounttype_individual">
                <div class="s-header">
                    <span><strong>12. 信託帳戶申請的文件要求 (個人專用) DOCUMENTATION REQUIREMENTS (INDIVIDUAL)</strong></span>
                </div>
                
                <div class="panel-body g-body">                
                    <div class="g-header-1"><strong>個人申購人 INDIVIDUAL SUBSCRIBER</strong></div>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>帶照片的身份證件 Photo ID<br> (如身份證/護照/駕帳)<br> (National ID / Passport / Driver License preferred)<br><br> <input class="file" type="file" name="IAPhotoid[]" data-show-upload="true" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>居住證明（不超過3個月）<br> Proof of Residency (of not more than 3 months old)<br> (如銀行對帳單/信用卡對帳單/保險帳單/水電費帳單)<br> (Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill preferred)<br><br> <input id="ia-proofresidency" class="file" type="file" name="IAProofresidency[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>銀行帳單（不超過3個月）和/或同等文件<br> Bank Statement (of not more than 3 months old) and/or equivalent documents<br> (顯示資產和/或基金多於信託存款金額)<br> (Displaying assets and/or funds more than the Trust Deposit Amount)<br><br> <input id="ia-bankstatement" class="file" type="file" name="IABankstatement[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>簽名樣本 Specimen Signature <br></p>
                                    <div class="signatureSection" upload-name="IASpecimensign" data-name="signatures[IASpecimensign]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder border-bot full">
                            <td>
                                <p>支付費用的證明<br>Proof of Payment of Fees<br><br>
                                <input id="ca-shareholders" class="file" type="file" name="IAProofOfPayment[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                    </tbody></table>
                    
                </div> 
            </div>
            
            <!-- Section 13 DOCUMENTATION REQUIREMENTS (CORPORATE) -->
            <div class="section section-13 acounttype_corporate">
                <div class="s-header">
                    <span><strong>13. 信託帳戶申請的文件要求 (機構專用) DOCUMENTATION REQUIREMENTS (CORPORATE)</strong></span>
                </div>
                
                <div class="panel-body g-body">                
                    <div class="g-header-1"><strong>機構申購人資料（請勾選，並附上附件資料） CORPORATE SUBSCRIBER</strong></div>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%;">
                                <p>公司註冊證 Certificate of Incorporation<br><br>
                                <input id="ca-namechange" class="file" type="file" name="CACertincorporation[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td>
                                <p>名稱變更證明（如適用） Certificate of Name Change (if applicable)<br><br> 
                                <input id="ia-namechange" class="file" type="file" name="CANamechange[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>
                        
                        <tr class="tr-td-withborder">
                            <td style="width: 50%;">
                                <p>存續證明 （如適用） Certificate of Good Standing (if applicable)<br><br>
                                <input id="ca-goodstand" class="file" type="file" name="CAGoodstand[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td>
                                <p>董事及股東名冊 Registry of Director(s) & Officer(s)applicable)<br><br> 
                                <input id="ca-regdirector" class="file" type="file" name="CARegdirector[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>  
                    </tbody></table>

                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder full">
                            <td>
                                <p>如銀行對帳單/信用卡對帳單/保險對帳單/水電費帳單<br>Proof of Business Address (Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill preferred)<br><br>
                                <input id="ca-proofbusadd" class="file" type="file" name="CAProofbusadd[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%;">
                                <p>公司章程及組織大綱<br>Memorandum and Articles of Association<br><br>
                                <input id="ca-memorandumaa" class="file" type="file" name="CAMemorandumaa[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td>
                                <p>近期財務報表 （如適用）<Br>Most Recent Financial Statements (if applicable)<br><br> 
                                <input id="ca-recentfinancialstatement" class="file" type="file" name="CARecentfinancialstatement[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%; border-right:0; border-bottom:0;">
                                <p><label>身份證件 Photo ID</label></p>
                                <p>法定代表人 Director(s)<br><br>
                                <input id="ca-directorsid" class="file" type="file" name="CADirectorsid[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td style="border-left:0; border-bottom:0;">
                                <p>公司簽署人 Company Signatory(ies)</p>
                                <div class="signatureSection" upload-name="CACompanysign" data-name="signatures[CACompanysign]">
                                    <div class="radio signOptions">
                                        <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                        <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                    </div>
                                    <div class="parentsign">
                                        <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                        <div class="sign-upload hidetag">
                                            <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">

                        <tbody>
                        <tr class="tr-td-withborder border-top full">
                            <td>
                                <p>股東持有公司股本 10% 或持有控制權的 （投票權） 股東<br>Shareholer(s) holding 10% or more of Company Share Capital and/or Controlling (Voting) Shareholder<br><br>
                                <input id="ca-shareholders" class="file" type="file" name="CAShareholders[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%; border-right:0; border-bottom:0;">
                                <p><label>居住證明 (3個月內) Proof of Residency (of not more than 3 months old)</label></p>
                                <p>法定代表人 Director(s)<br><br>
                                <input id="ca-directorsproof" class="file" type="file" name="CADirectorsproof[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td style="border-left:0; border-bottom:0;">
                                <p>公司簽署人 Company Signatory(ies)<br></p>
                                <div class="signatureSection" upload-name="CACompanysignproof" data-name="signatures[CACompanysignproof]">
                                    <div class="radio signOptions">
                                        <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                        <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                    </div>
                                    <div class="parentsign">
                                        <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                        <div class="sign-upload hidetag">
                                            <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">
    
                        <tbody>
                        <tr class="tr-td-withborder border-bot border-top full">
                            <td>
                                <p>股東 （持有公司股本 10% 或以上及有實質執行權的股東）<br>Shareholer(s) holding 10% or more of Company Share Capital and/or Controlling (Voting) Shareholder<br><br>
                                <input id="ca-shareholdersproof" class="file" type="file" name="CAShareholdersproof[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                        <tr class="tr-td-withborder border-bot full">
                            <td>
                                <p>支付費用的證明<br>Proof of Payment of Fees<br><br>
                                <input id="ca-shareholders" class="file" type="file" name="CAProofOfPayment[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>  
                            <tr>
                                <td colspan="2">
                                    <p><strong>授權簽署人名單及簽名樣本<br> Authorised Signatory Lists & Specimen Signatures</strong></p>
                                </td>
                            </tr>                            
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <div class="signatureSection" upload-name="CAAuthorizedone" data-name="signatures[CAAuthorizedone]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>                                    
                                    <input type="text" name="case[CAAuthorizedonename]" class="form-control" placeholder="姓名 Name :" /><br>
                                    <input type="text" name="case[CAAuthorizedonetitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                                <td>
                                    <div class="signatureSection" upload-name="CAAuthorizedtwo" data-name="signatures[CAAuthorizedtwo]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <input type="text" name="case[CAAuthorizedtwoname]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedtwotitle]" class="form-control" placeholder="稱謂 Title :" />
                                    </p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <div class="signatureSection" upload-name="CAAuthorizedthree" data-name="signatures[CAAuthorizedthree]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <input type="text" name="case[CAAuthorizedthreename]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedthreetitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                                <td>
                                    <div class="signatureSection" upload-name="CAAuthorizedfour" data-name="signatures[CAAuthorizedfour]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <input type="text" name="case[CAAuthorizedfourname]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedfourtitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
     
                </div> 
            </div>
            
            <!-- Section 14 -->
            <div class="section section-14">
                <div class="s-header">
                    <span><strong>14. 申購協議及重要資料</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>投資信託與多種投資產品相連結，使您的投資信託帳戶具有多元化的投資種類。請詳 細閱讀開戶程序的條款書和申購書，投資信託存有投資風險；除非書面聲明，此投資 沒有保證回報。過往的投資回報並不一定反映未來的表現。</p>
                        <p>在每個條款清單上已有免責聲明特別提示，您需要接受相對的投資風險，並要了解所有 投資都是有風險的。</p>
                        <div class="g-header-2">申請加入信託聲明</div>
                        <p>本人／我們，作為上述指定的個人，已擁有完整的法律權力，申請成為投資信託的申 購人，並證明：</p>
                        <ol type="a">
                            <li>本人／我們在沒有喪失法律行為能力的情況下提示出此申請;</li>
                            <li>本人／我們已遵守所有適用法律去提示出本次申請;</li>
                            <li>本人／我們在此開戶程序中提示供的所有資訊均是完整，真實且正確無誤的。</li>
                            <li style="list-style:none; margin-left: -15px;"><p>本人／我們已閱讀、了解並同意附加的申購人協議的條款;</p></li>
                            <li>作出草簽，以表示本人／我們察覺與了解信託申購人會籍、放棄分散投資、免投 資風險和各項免責聲明所帶來的費用;</li>
                            <li>現申請作為此信託的申購人。</li>
                        </ol><br>
                        <p>本人／我們進一步聲明,本人/我們是一個多元的、專業的、經認可的或是等同上述說
                            明的資深投資者、在我們司法管轄所在地任何適用的說明書及小冊子中得以豁免顯示
                            本人/我們的購買、供款、登記和通知要求。關於該投資信託計畫的發行、銷售或營
                            銷等相關資訊、本人/我們已經獲得或尋求金融和法律專業人士的適當建議來確認此
                            一說法、任何因此而產生的糾紛或爭議、本人/我們在此聲明放棄任何訴訟或司法聽
                            證會之權利。</p><Br>
                        <p>申購人執行日期如下:</p>
                        
                        <div class="g-header-2">基本釋義</div>
                        <p>在這申購協議和整個開戶程序中，下列詞彙具有以下涵義:</p>
                        <ul>
                            <li>“開戶程序”指全套的文件，包括申購協議，及申購人提示交併入檔的任何補充文件或隨附表格;</li>
                            <li>“費用”指信託申購人根據內附的標準需要交付的各項費用，或隨信託計畫不定期的變更而可能產生的其他費用;</li>
                            <li>“託管公司”是 Odeon & Co Pte Ltd (簡稱“Odeon”)</li>
                            <li>“信託公司”是 Zico Allshores Trust (Singapore) Pte Ltd</li>
                            <li>“申購人”指簽署信託之申購人，並獲得信託公司和託管公司Odeon接受。在信託公司按情況制定的合理時間內，申購人必須遵守上述的信託條件，方可獲得信託公司結算其投資回報;</li>
                            <li>“授權核證人”指一個在其所屬司法轄區大於法定年齡的人，在得託管公司允許其 負責核證文件之前，須先由信託公司和託管公司對其進行反洗錢(“AML”) 和公共記錄核實或其他背景調查。</li>
                            <li>“信託契約”指在 2016 年已分別執行的 <信託契約> 和 <信託聲明>和信託公司有任何修訂後存放於管理人及信託公司辦事處的已執行副本。</li>
                        </ul><br>
                        <p>除非文義另有所指或是此申購協議內有更改或另有列明，載於此申購人協定內的文字 應與信託契約內的文字具有相同含義:</p>
                        
                        <ol type="i">
                            <li>單數字詞包含複數字詞，反之亦然，</li>
                            <li>在此申購協議中標題可被忽略。</li>
                            <li>所述及的人應包括法人團體，非法人社團和合夥公司。</li>
                            <li>凡述及的任何文件、協議、法律或法規，都應該理解為目前已生效、經過不定期 修訂、變更、補充、替代或以新代舊。</li>
                        </ol><Br>
                        
                        <p>這申購協議的任何條款和細則/或任何附屬協議，在現在或將來某天，如果可能失去 效力或變成無效，其申購協議和任何所有附屬協定的其餘部分應仍然生效。</p><br>
                        <p>申請程序以新加坡法律為解釋依據<!-- 並受香港法律管轄 --> 。</p><br>
                        
                        <div class="g-header-2">信託認購者向託管公司聲明</div>
                        <p>本開戶書簽字人,已符合資格申請加入成為其信託申購人,現同意並做出如下承諾:</p>
                        <ol type="a">
                            <li>本人／我們聲明，在開戶程序填妥的內容裡，本人／我們提示供的資訊在各方面都 準確無訛。</li>
                            <li>本人／我們聲明在收到此開戶程序時，本人／我們已得到相關建議，內容包括加 入信託後本人／我們將得到的利益，決定這些利益的方法，以及這些利益的相關 條款。</li>
                            <li>本人／我們茲向託管公司確認，已經知悉託管公司建議，並就其所推薦的投資信 託計劃，獨立諮詢與之相關的金融、法律和稅務意見，而本人／我們亦就以上本 人／我們認為需要知道的其他關於該投資信託計劃的可能含義和託管公司的託管 情況作出詢問。</li>
                            <li>本人／我們了解就所推薦的投資信託計劃或該計劃對本人／我們的影響，或該計 劃可能受惠於其他人的可能性，信託公司尚未向本人／我們提示供與之相關的稅務 意見 。</li>
                        </ol>
                        
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="g-header-2">託管公司守則</div>
                        <ol type="a">
                            <li>有關投資信託基金的操作或管理，託管公司的任何決定均是最終決定，各方均同 意由信託公司制定的政策、指引、指令或命令，均必須貫徹始終並嚴格執行。</li>
                            <li>如果任何一方希望改變託管公司制定的政策、指引、指令或命令. 可以書面提示出 要求，而託管公司可以全權決定是否准許。</li>
                        </ol>
                        <div class="g-header-2">放棄分散投資</div>
                        <p>本人／我們同意選擇參與由信託公司建議並得到託管公司批准的相關投資或長期策略 投資。因此，本人／我們放棄了由託管公司管理的分散投資需要。</p>
                        <p>本人／我們接受，當託管公司按本人／我們要求作出投資時:</p>
                        <ol type="a">
                            <li>若因信託公司和託管公司的任何建議、代表、舉動、疏忽或行為而造成損失時， 本人／我們不會向信託公司和託管公司索取賠償。</li>
                        </ol>
                        <div class="g-header-2">投資</div>
                        <p>本人／我們明瞭在本人／我們提示出要求信託公司指示財務顧問作出特定投資時，本 人／我們知悉存在固有風險，市場價值有漲有跌，本人／我們不應為信託公司和託管 公司執行了本人／我們的要求而向其索償。託管公司並沒有向本人／我們提示供財務建 議，亦未有就有關的任何投資作出擔保。就本人／我們已選擇的投資(包括但不限於 專業投資)，本人／我們聲明:</p>
                        <ol type="a">
                            <li>本人／我們有足夠經驗了解與該投資相關的特性和風險;</li>
                            <li>本人／我們在決定投資該項目前，已閱讀並充分理解該銷售檔，尤其當中包括銷 售檔內關於投資各項目的風險和費用的資訊;</li>
                            <li>本人／我們接受有關事項投資的所有風險，尤其本人／我們在專業投資所涉及的 風險，有可能導致部分比例或全部投資的虧損;</li>
                            <li>就本投資結構內的各項投資專案於個人是否合適，本人／我們已聽取過獨立意見;</li>
                            <li>信託公司並沒有進行促銷或提示供任何建議，亦沒有就投資的績效風險、投資的監 管問題或安全性(包括任何明示或暗示的保證)表示任何意見。</li>
                            <li>本人／我們同意賠償信託公司因投資選擇問題引起或與其相關的索償、成本支 出、債務、費用、損害或損失(包括但不限於任何因此而受影響的損失/利潤損失，名譽損失，全部利息罰款，法律及其他專業收費) 。</li>
                        </ol>
                        <div class="g-header-2">標準規定收費</div>
                        <p>通過填妥本申請表，本人／我們同意支付一次性信託帳號開戶費美元 $535 (個人申購 人) / 美元 $842 (機構申購人) 和本人所有受信託管理資產的每年 2％ 管理費。所有 費用提示前支付，不予退還。</p>
                        <p>信託公司和託管公司保留可追回已發放款項與實際支出（如直接從計畫衍生的第三方 收費）的權利。尤其若基金結構複雜或按成員要求而獨有，額外的第三方收費可能會 因管理該基金而產生。各項收費將根據個別具體情況而訂，這些費用通常會事先通知 並商定後方確立收費。</p>
                        
                        <p>為達到“了解客戶”，根據“信託公司條例”，本人／我們同意信託公司和託管公司可於全球範圍內收到本人／我們資產基數的所有必要資訊。本人／我們明白，根據“ 個人資料(私隱)條例”，必須本人／我們授權方可披露本人／我們的個人資料，本 人／我們也明白，在未經本人／我們進一步書面簽署授權下，本人／我們的資料不會披露予信託公司的信託要求以外的任何人士、公司或機構。</p>
                        <div class="g-header-2">放棄冷靜期</div>
                        <p>成員申請之日起有 14 個工作日的冷靜期：在此期間申請將被擱置，若決定退出計畫，信託帳號開戶費和首年的管理費，扣除銀行匯款或轉帳手續費用後可獲退還。由於直到冷靜期過後 Odeon 和信託公司才能開始進行相關作業，所以申購人可以在此簽字放棄冷靜期。在這種情況下，信託基金的設置將在收到申請後隨即開始。在冷靜期後或在申購人放棄冷靜期的情況下，上述費用將不會退還。</p>
                        <table class="table" border="0">
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="cneedcoolingoff pull-left" id="WaiveCoolingOffC1" name="NeedCoolingOff" value="Y"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffC1"> 我選擇 14 天的冷靜期，如我 14 日內無額外通知，到時啟動我的信託帳號。</label></td>
                            </tr>
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="cneedcoolingoff pull-left" id="WaiveCoolingOffC2" name="NeedCoolingOff" value="N"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffC2"> 我選擇放棄冷靜期，立即啟動我的信託帳號。</label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
             
            <!-- Section 15 SUBSCRIPTION AGREEMENT & INFORMATION -->
            <div class="section section-15">
                <div class="s-header">
                    <span><strong>15. SUBSCRIPTION AGREEMENT & INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>Investment Trust is linked to various investment products which may include a wide array
                            of investments received in Escrow Trust for your account. Please see the Term Sheet and
                            Subscription included with this Account Opening Package. Each of the Investment Trust is
                            designed to follow a different investment strategy. Investment Trust carry investment risks.
                            Unless stated otherwise, the returns are not guaranteed. Do note that the past returns are not
                            necessarily indicative of the future performance.</p>
                        <p>Disclaimers specific to Investment Trust are written in each Term Sheet. You acknowledge there is always risk in investments.</p>
                        <div class="g-header-2">Declaration to the Trust</div>
                        <p>I, being the above named individual and having full legal authority to make this Subscription for membership in the investment trust program offered by Odeon, certify that:</p>
                        <ol type="a">
                            <li>I have no legal disability of any description to make this subscription;</li>
                            <li>I have complied with all applicable laws in making this subscription;</li>
                            <li>All information given by me and contained in this Subscription Form is complete, true and correct.</li>
                            <li style="list-style:none; margin-left: -15px;">I have read and understood the Subscription Agreement; and</li>
                            <li>I have taken note of and initialled to signify my awareness and understanding of the fees payable by Subscriber, diversification waiver, investment risk waiver and disclaimers; and</li>
                            <li>hereby make subscription as a Subscriber of the investment trust program.</li>
                        </ol><br>
                        <p>I/we further declare that I am / We are a sophisticated, professional or accredited investor
                            as such terms are defined, or the equivalent thereof, in my home jurisdiction to qualify
                            and exempt my purchase, contribution, or participation in this investment program from
                            any applicable information memorandum, registration and notice requirements for the
                            distribution, sale, or marketing of securities that may apply to this investment trust program,
                            and I have obtained or sought the appropriate advice from financial and legal professionals to
                            confirm this statement, thus waiving my rights to claim otherwise in any dispute or proceeding
                            in law, court or hearing.</p>
                        <p>Executed by Subscriber on the date below:</p>
                        
                        <div class="g-header-2">General Interpretation</div>
                        <p>In this Subscription Agreement and the entire Account Opening Package the following terms shall have the following meanings:</p>
                        <ul>
                            <li>“Account Opening Package” means this entire document, including the Subscription Agreement, and any supplementary or accompanying forms provided and filed by the Subscriber;</li>
                            <li>“Fees” means the amounts payable by a Subscriber pursuant to attached Fee Schedule, or any other fees that may be charged, which are subject to change from time to time;</li>
                            <li>“Management Company” is Odeon & Co Pte Ltd (hereby also known as Odeon)</li>
                            <li>“Escrow Trustee” is Zico Allshores Trust (Singapore) Pte Ltd</li>
                            <li>“Subscriber” is a person who signs this Subscription Form and provides funds to Odeon
                                to become a Subscriber of the investment trust program. Under this program, Odeon
                                will transfer funds received from Subscribers. Any person who becomes a Subscriber
                                must fulfil the conditions of Subscription as set out above in order to be available for
                                settlements at Odeon’s discretion;</li>
                            <li>“Approved Certifier” is a natural person of greater than the age of majority in their home
                                jurisdiction, who has completed an Anti-Money Laundering (“AML”) and public records
                                check or other background reviews at the discretion of Odeon, prior to having been
                                approved by Odeon to certify documents for reliance upon the AML review by the Escrow
                                Trustee and Odeon;</li>
                            <li>“Trust Deed” means the Escrow Trust Deed [and Declaration of Trust respectively]
                                executed in the year of 2016 and any amendments by the Escrow Trustee, executed
                                copies of which are maintained at the offices of the Escrow Trustee.</li>
                        </ul><br>
                        <p>Unless the context otherwise requires and except as varied or otherwise specified in this
                            Subscription Agreement, words and expressions contained in this Subscription Agreement
                            shall bear the same meanings as in the Escrow Trust Deed:</p>
                        
                        <ol type="i">
                            <li>the singular includes the plural and vice versa;</li>
                            <li>headings shall be ignored in the construction of this Subscription Agreement;</li>
                            <li>references to persons shall include body corporates, unincorporated associations and partnerships; and</li>
                            <li>references to any documents or agreements, laws or regulations, are to be construed as
                                references to such documents or agreements, laws or regulations, as is in force for the
                                time being and as amended, varied, supplemented, substituted or novated from time to
                                time.</li>
                        </ol><Br>
                        
                        <p>In the event that any of the clauses, terms and conditions of this Subscription Agreement
                            and/or any of the subsidiary agreements either now or at a future date, should be held to be
                            without force and effect or be otherwise invalid, the rest of the Subscription Agreement and
                            any and all subsidiary agreements shall remain in effects.</p><br>
                        <p>This entire Account Opening Package is construed and shall be governed by and in accordance with the laws of Singapore.</p><br>
                        
                        <div class="g-header-2">Subscriber’s Declaration to Odeon</div>
                        <p>I, the undersigned person, hereby being eligible to apply for Subscription of the trust, agree and undertake as follows:</p>
                        <ol type="a">
                            <li>I declare that to the extent that I have completed the Account Opening Package, the information contained is accurate in every respect.</li>
                            <li>I declare that at the time I was given this Account Opening Package, I was also given advice about the benefits to which I 
                            would be entitled as a Subscriber, the method of determining that entitlement and the conditions relating to those benefits.</li>
                            <li>I confirm to Odeon, that I have been advised by Odeon to take independent financial,
                                legal and taxation advice on the investment trust program offered by Odeon, and that
                                I have made such enquiries and taken such financial, legal, taxation and other advice
                                as I consider necessary concerning all possible implications concerning the proposed
                                program.</li>
                            <li>I acknowledge that the Escrow Trustee has not given me any tax advice concerning the
                            proposed investment trust program on my circumstances or on the circumstances of any
                            other person likely to be affiliated with or benefiting from the plan.</li>
                        </ol>
                        
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="g-header-2">Code of Practice</div>
                        <ol type="a">
                            <li>Any decision by Odeon pertaining to the operation of or administration of the investment
                            trust program is final and the parties all hereby agree that policies, instructions,directions
                            or orders made by Odeon must be followed rigorously and at all times.</li>
                            <li>Should any party wish to vary the policies, instructions, directions or orders made by
                            Odeon, they may request such variance in writing and the approval of such variance shall
                            be at the sole discretion of Odeon.</li>
                        </ol>
                        <div class="g-header-2">Diversification Waiver</div>
                        <p>I have endorsed the Escrow Trustees’ election to participate in underlying investments or a
                            longer term investment strategy as proposed by Odeon and adopted by the Escrow Trustee. I
                            therefore waive the need for diversification of investments by Odeon.</p>
                        <p>I accept that when Odeon invests funds I have provided:</p>
                        <ol type="a">
                            <li>a. no claim will be made by me against Odeon or Escrow Trustee for any advice, representations, acts, omissions, or conduct of Odeon or Escrow Trustee.</li>
                        </ol>
                        <div class="g-header-2">Investments</div>
                        <p>I acknowledge that I am aware of the inherent risks that market values can go down as well as
                            up; and that I shall make no claim on Odeon or Escrow Trustee for carrying out my requests.
                            Odeon has not provided me financial advice and has made no warranty or representation in
                            relation to any investment. Where I choose or have chosen an investment, including but not
                            limited to specialist investment, I declare that:</p>
                        <ol type="a">
                            <li>l am sufficiently experienced to understand the features and risks associated with the investment; and;</li>
                            <li>I have read and fully understood the offering document, including in particular the
                            information on the risks and charges associated with investing in the investments
                            contained in the offering document before deciding to invest into it and;</li>
                            <li>I personally accept all risk associated with the investment, and in particular that my
                            investment in a specialist investment involves risk that could in loss of a significant
                            proportion or all of the sum involved; and;</li>
                            <li>I have taken independent advice on the suitability of the investments within this structure; and;</li>
                            <li>The Escrow Trustee has not promoted the investment or provided any advice, made any
                            recommendation or expressed any opinion whatsoever in respect of the performance
                            risk, regulatory issues or security (including any express or implied guarantees) of the
                            investment.</li>
                            <li>I agree to indemnify and keep the Escrow Trustee indemnified from and against all claims,
                            cost demands, liabilities, expenses, damages or losses (including without limitation any
                            consequential losses/loss of profit and loss of reputation, and all interest penalties and
                            legal and other professional costs) arising out of or in connection with the choice of
                            investment.</li>
                        </ol>
                        <div class="g-header-2">Standard Fee Schedule</div>
                        <p>By completing this Subscription, I agree to pay the one-time account set-up fee of USD535
                        (Individual Subscriber) / USD842 (Corporate Subscriber) (“Set-up Fee “) and the first annual
                        administration fee of 2% of all my Assets Under Management (AUM) (“Management Fee”). I
                        agree to pay subsequent annual Administration Fees. All fees are payable in advance and
                        non-refundable.</p>
                        <p>The Escrow Trustee and Odeon reserve the right to recover disbursements and out-of-pocket
                        expenses, such as third party charges, direct from the plan. Additional third party charges
                        may be incurred in administering the structure, in particular where the structure is complex or
                        unique to the Subscriber’s requirements. Charges will be discussed on a case-by-case basis
                        and such charges will normally be notified and agreed in advance.</p>
                        <div class="g-header-2">Authority for Disclosure of Personal Data</div>
                        <p>I hereby consent for Odeon and Escrow Trustee to receive all necessary information on my
                            asset base globally as it required for the purposes of “Know Your Client”. I understand that
                            my authority is required under the Personal Data (Privacy) Ordinance and I also understand
                            that this information will not be disclosed outside of Odeon and Escrow Trustee’s legal
                            requirements to any person, company or any institution without my further written and signed
                            authority.</p>
                        <div class="g-header-2">Cooling-off Period Waiver</div>
                        <p>There is a 14 working day cooling-off period from the date of this Subscription: during this
                            period, the Subscription will be on hold, and Set-up and first year Administration Fees can be
                            refunded, net of the relevant banking charges. As the Odeon and the escrow trustee cannot
                            commence set-up until after the Cooling-off Period, if the Subscription is time-sensitive, the
                            subscriber may, by opting here, waive the Cooling-off Period. In that event, set-up of the
                            investment trust program will commence upon receipt of the Subscription. After the Coolingoff
                            Period, or in the event that the Cooling-off Period is waived, fees shall be non-refundable.</p>
                        <table class="table" border="0">
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="eneedcoolingoff pull-left" id="WaiveCoolingOffE1" name="case[NeedCoolingOff]" value="Y"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffE1"> I need the cooling-off period. Only activate the account 14 working days thereafter.</label></td>
                            </tr>
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="eneedcoolingoff pull-left" id="WaiveCoolingOffE2" name="case[NeedCoolingOff]" value="N"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffE2"> I opt to waive the cooling-off period. Activate my account immediatel</label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 16 CHECKED BY APPROVED CERTIFIER --> 
            <div class="section section-16">
                <div class="s-header">
                    <span><strong>16. 由適用簽署人見證核實審查（合格審查人員專用） CHECKED BY APPROVED CERTIFIER</strong></span>
                </div>
                
                <div class="panel-body g-body" style="border-bottom: 1px solid #a3947d;">
    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%; border-right:0;;">
                                <div class="row">
                                    <label class="col-sm-4">姓名 Name</label>
                                    <div class="col-sm-8">
                                        <input id="ApprovedBy" type="text" class="form-control" name="acct[ApprovedBy]">
                                    </div> 
                                </div>
                                <div class="clearfix"></div>
                            </td> 
                            <td>
                                <div class="row">
                                    <label class="col-sm-4">日期 Date</label>
                                    <div class="col-sm-8">
                                        <!--<input type="date" class="form-control" value="<?php echo date("d/m/Y");?>" readonly>-->
                                        <input type="text" class="form-control" name="acct[ApprovedDate]" value="<?php echo date("d-M-Y");?>" readonly>
                                    </div>  
                                </div>
                            </td>
                        </tr>
                        
                    </tbody></table>
                </div>
                
                <br>
                <div style="text-align: center;">
                    <p style="color:red;"><input style="margin-top:-3px;"type="checkbox" id="TermsAgree" name="TermsAgree" value="Y" required="required"> <label for="TermsAgree"><strong>我/我們閱讀並理解上面的內容 | I/We Read and Understood the above.</strong></label> </p>
                    <button type="submit" class="btn">Submit | 提交</button>
                </div>
    
        </div>

        </div>
        </form>

    </div>
    <!-- /page content -->
<?php View::footer('forms'); ?>