<?php 
View::$title = 'Odeon & Co. Subscription Form';
View::$bodyclass = 'guest';
View::header('forms'); 
?>

    <!-- page content -->
    <div class="body-wrap">
        <div class="page-header">
            <div class="container">
                <h1>Odeon & Co. Subscription Form</h1>
            </div>
        </div>
        
        <form id="master_form" action="" method="post" class="form-horizontal subscription-form" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo md5(microtime()); ?>">        
        <input type="hidden" name="action" value="addcasefile">
        <input type="hidden" name="acct[FormType]" value="Simplified">
        <div class="section-holder container">
            <?php echo View::getMessage(); ?> 
            
            <!-- Section 1 GENERAL GUIDANCE -->
            <div class="section section-1">
                <div class="s-header">
                    <span><strong>01.  概括指引  GENERAL GUIDANCE</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>您申购的投资信托计划 《傲鼎麒麟投资信托》 (<strong>Odeon
                            Investment Trust</strong>)，是由 <strong>Odeon & Co. Pte Ltd</strong> (简称“<strong>Odeon</strong>”) 所提供与信托公司 <strong>Zico Allshores Trust</strong> 集团（简称“信托公司”）监督监管；以募集资金及投资存款，来提供申购人投资回报。</p>
                        <p>此申购人开户程序应由申购人亲自填写。本公司的信托申购协议规范了投资信托的条款和条件，请与此申购书一起阅读。感谢您的理解并希望您能配合本公司要求提供所需之文件。</p>
                        <p>请完整填妥此申购开户书并按要求签字，以方便本公司审核您的申请。如未能提供相关资料和证明文件将导致申购开户有所延误。附加资料应包括在内以作为本申请的佐证资料。在本申购开户书非指定地方提供的任何额外资料、备注或指令将不会被执
                            行。</p>
                        <p><span style="color:red; font-weight:bold;">注:</span> 如本中文版本与英文版本内容有差异，将以英文版本为准。</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p>Odeon Investment Trust is an investment deposit program offered by <strong>Odeon &
                        Co. Pte Ltd</strong> (“Odeon”) with Trust Agent <strong>Zico Allshores Trust Pte Ltd</strong> (“Esrow
                        Trustee”) for the purpose of receiving and investing deposits to provide a return
                        for Subscribers. </p>
                        <p>This Account Opening Package should only be completed by the applicant. This
                        form should be read in conjunction with the Subscription Agreement and the
                        Term Sheet. We seek your understanding and cooperation in furnishing the
                        required documents and appreciate your time and effort in doing so. </p>
                        <p>It is important that you complete this Subscription form in full, and sign
                        as required to enable us to consider your Subscription. Kindly attach
                        necessary documents. Failure to provide all relevant information and
                        supporting documentation will result in a delay to the Subscription
                        being processed. Additional information may be included to support this
                        subscription. Any additional details/notes/instructions or those provided at a
                        non-designated area of the form may not be executed. </p>
                        <p><span style="color:red; font-weight:bold;">Note:</span> If the Chinese version and English version of the content is different, the English version shall prevail.</p>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 2 PRIVACY POLICY -->
            <div class="section section-2">
                <div class="s-header">
                    <span><strong>02. 隐私政策 PRIVACY POLICY</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>除非在《打击洗钱及恐怖分子资金筹集(金融机构)条例》或《税务条例》或法庭指令的要求下，否则 <strong>Odeon </strong>不会公开及泄露客户任何资料;
                        在信托行政需要外，不论基于任何目的，<strong>Odeon </strong>一般不应透露关于信托存款的资料，也不会使用受益人的资料，所有有关信托申购人资料是受到保护的。</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p>The Escrow Trustee and Odeon will not disclose information on the Subscriber
                        and their affairs except as required under applicable laws, including the AntiMoney
                        Laundering and Counter-Terrorist Financing (Financial Institutions)
                        Ordinance (“AML Ordinance “). Odeon shall not disclose information on trust
                        escrow deposits to any third party, except as required under applicable laws,
                        including the AML Ordinance and the Inland Revenue Ordinance or otherwise as
                        directed by a court order.</p>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 3 POWER OF ATTORNEY -->
            <div class="section section-3 POA-area acounttype_corporate">
                <div class="s-header">
                    <span><strong>03. 委托书 POWER OF ATTORNEY <small>(法人机构填写，个人申办免填|for corporate clients only)</small></strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-12 col-xs-12 form-inline">
                        <p>By this Power of Attorney dated <input id="POADate" class="form-control POADate isdatepicker" type="text" name="case[POADate]" placeholder="YYYY-MM-DD"> <input type="text" class="form-control POAFirstName" name="case[POAFirstName]" style="width: 30%;" placeholder="First Name"> <input type="text" class="form-control POALastName" name="case[POALastName]" style="width: 30%;" placeholder="Last Name"></p>
                        <p><input type="text" class="form-control POACompanyName" name="case[POACompanyName]"style="width: 42%;" placeholder="Company Name"> (申购公司 NAME OF SUBSCRIBER COMPANY), (商业登记号码公司编号 COMPANY NUMBER :</p>
                        <p><input type="text" class="form-control POACompanyNumber" name="case[POACompanyNumber]" style="width: 25%;" placeholder="Company Number">), a Company registered in <select id="Country" name="case[POACompanyCountry]" class="form-control POACompanyCountry" style="width:25%">
                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                <?php } ?>
                            </select> (注册地 PLACE OF INCORPORATION) and having</p>
                        <p>its registered office at <input type="text" class="form-control POACompanyAddress" name="case[POACompanyAddress]" style="width: 55%;" placeholder="Company Addres"> <input type="text" name="case[POACompanyCity]" class="form-control POACompanyCity" style="width: 25%;" placeholder="Company City"></p>
                        <p><input type="text" class="form-control POACompanyState" name="case[POACompanyState]" style="width: 61%;" placeholder="Company State"> (公司地址 FULL ADDRESS) (hereinafter called the “申购公司</p>
                        <p>SUBSCRIBER COMPANY”) hereby appoint <input type="text" class="form-control POAAppointor" name="case[POAAppointor]" style="width: 36%;" placeholder="Appointor"> (法定代表人 APPOINTOR, 护照号码 PASSPORT NO.</p>
                            <p><input type="text" class="form-control POAAppointorIdNumber" name="case[POAAppointorIdNumber]" style="width: 30%;" placeholder="Appointor Id Number">) to act as the true and lawful Attorney of the Company and in the name of the Company and on the Company’s
    behalf to perform all acts and things, including signing and execution of all necessary documents in relation to the Company, including any
    amendments or modifications and etc, in the best interest of the Company to subscribe Odeon Investment Trust. This Power of Attorney shall be
    valid and of full effect until revoke by writing.</p><Br>
                        <p>此授权书由申购公司 (SUBCRIBER COMPANY) (商业登记号码，注册地，公司地址）于以上日期填写，委托法定代表人（护照号码为以上填写）為本公司法定代表人，並以公司名义及代表公司执行公司之一切事务，包括签署及执行所有与公司相关及必须之文件，包括任何修订或修改等，以公司之最大利益为前提，认购 《傲鼎投资信托》(Odeon Investment Trust)。此授权书具法律效力，直至以书面通知撤销。</p><br>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p>申购公司法定代表人 <br>
                        The Company Seal was affixed hereunto in the presence
                        of the Directors (and Secretary, if applicable).</p>                    
                    </div>
                    <div class="col-sm-6 col-xs-12 POA-seals form-group">
                        <div class="g-header-2"><label>申购公司蓋章 Subscriber Company Corporate Seal</label></div>
                        <p>
                            <input class="file" type="file" data-show-upload="false" name="POACorporateSeal[]" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                        </p> 
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group"> 
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group"> 
                        <label>法定代表人 Director Signature</label>

                        <div class="signatureSection" upload-name="POADirectorSign" data-name="signatures[POADirectorSign]">
                            <div class="radio signOptions">
                                <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                            </div>
                            <div class="parentsign">
                                <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                <div class="sign-upload hidetag">
                                    <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                </div>
                            </div>
                        </div>
                        <p><input type="text" class="form-control" name="case[POADirectorName]" placeholder="Director Name"></p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <label>董事 Director / 監察人 Secretary Signature</label>
                        <p><input class="file" type="file" data-show-upload="false" name="POASecretarySign[]" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                        <p><input type="text" class="form-control" name="case[POASecretaryName]" placeholder="Secretary Name"></p>
                    </div>-->
                </div>
                
            </div>
            
            <!-- Section 4 ACCOUNT TYPE -->
            <div class="section section-4">
                <div class="s-header">
                    <span><strong>04. 帐户类型 ACCOUNT TYPE</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p><label><input type="radio" class="naccount" id="TrustAccountTypeIndividual" name="acct[AccountType]" value="Individual" checked="checked"> <strong>INDIVIDUAL 个人</strong></label><br>
                        (如果选择个人,请省略过第5段)<br>
                        (Please Ignore Section 5 If Individual is Selected)</p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p><label><input type="radio" class="naccount" id="TrustAccountTypeCorporate" name="acct[AccountType]" value="Corporate"> <strong>CORPORATE 机构</strong></label><Br>
                        (如选择机构,请填写第5段)<Br>
                        (Please Fill Up Section 5 If Corporate is Selected)</p>
                    </div>
                    
                </div>
                
            </div>
            
            <!-- Section 5 CORPORATE CLIENT INFORMATION -->
            <div class="section section-5 acounttype_corporate">
                <div class="s-header">
                    <span><strong>05. 机构客户资讯  CORPORATE CLIENT INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>机构公司名称 Company Name</label></p>
                        <p><input type="text" id="CompanyName" name="acct[CompanyName]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>机构公司注册号码 Registration No / Certificate of Incorporation No.</label></p>
                        <p><input type="text" id="RegistrationNo" name="acct[RegistrationNo]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>注册地址 Registered Address</label></p>
                        <p><textarea rows="3" id="RegistrationAddress" name="acct[RegistrationAddress]" class="form-control"></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>机构公司注册国家 Country of Registration / Incorporation</label></p>
                        <select id="CRINC" name="acct[RegistrationCountry]" class="form-control">
                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>电话号码 Telephone</label></p>
                        <p><input type="text" id="CRINCTelephone" name="acct[RegistrationTelephone]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>主要经营地址 <small><strong>（如与注册地址不同）</strong></small> Principal Business Address <small>(If different from Registered Address)</small></label></p>
                        <p><textarea rows="3" id="PBAddress" name="acct[BusinessAddress]" class="form-control" value=""></textarea></p>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 6 APPLICANT’S PERSONAL DETAILS -->
            <div class="section section-6">
                <div class="s-header">
                    <span><strong>06. 申购人的个人资料（个人客户专用） APPLICANT’S PERSONAL DETAILS</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Referrer ID</label></p>
                        <p><input type="text" id="ReferrerUserChecker" class="form-control" name="user[ReferrerUserID]" value="" required="required" rel="<?php echo View::url('ajax'); ?>" ><span id="referrerloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Referrer Name</label></p>
                        <p><input type="text" id="referrerdata" class="form-control" value="" disabled=""></p>
                    </div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>稱呼 Title</label></p>
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'meta[Salutation]',
                                    'options'=>AppUtility::getSalutation(),
                                    'value'=>'Mr',  
                                    'class'=>'form-control',
                                    'id'=>'Salutation',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>护照/身分证号码 Passport / Photo ID Number</label></p>
                        <p><input type="text" id="IdNumber" name="meta[IdNumber]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>名字 First Name</label></p>
                        <p><input type="text" id="FirstName" name="meta[FirstName]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>国籍 Country of Citizenship</label></p>
                        <p>
                        <select id="Country" name="meta[Country]" class="form-control">
                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                            <?php } ?>
                        </select> 
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>姓氏 Last Name</label></p>
                        <p><input type="text" id="LastName" name="meta[LastName]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>税务居住国 Country of Tax Residency</label></p>
                        <p><select id="Country" name="acct[BusinessCountry]" class="form-control">
                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                <?php } ?>
                            </select>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>出生日期 Date of Birth</label></p>
                        <div class="col-md-3 col-sm-3 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Month]',
                                    'options'=>AppUtility::getMonth(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Month',
                                    'placeholder'=>'Month',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Day]',
                                    'options'=>AppUtility::getDay(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Day',
                                    'placeholder'=>'Day',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Year]',
                                    'options'=>AppUtility::getYear(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Year',
                                    'placeholder'=>'Year',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <p><label>婚姻狀況 Marital Status</label></p>
                        <p><?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'meta[CivilStatus]',
                                    'options'=>AppUtility::getCivilStatus(),
                                    'value'=>'SINGLE',
                                    'class'=>'form-control',
                                    'id'=>'CivilStatus',
                                    'custom'=>'required'
                                )
                            ); 
                        ?></p>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <p><label>性别 Gender</label></p>
                        <p><select id="Gender" name="meta[Gender]" class="form-control" required>
                                <option value="M">Male | 男</option>
                                <option value="F">Female | 女</option>
                        </select></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>职业及职位 <small>(如退休，请注明退休，或退休前的职位)</small> <br> Occupation & Job Title <small>(if retired, state retired and former occupation)</small></label></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><input type="text" id="Occupation" name="meta[Occupation]" class="form-control" placeholder="职业 | Occupation" value="" required="required"></p>
                        <p><input type="text" id="JobTitle" name="meta[JobTitle]" class="form-control" placeholder="职位 | Job Title" value="" required="required"></p>
                    </div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>语言 Prefered Language</label></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p>
                        <select id="Language" name="meta[Language]" class="form-control" required>
                                <option value="en">英文 | English</option>
                                <option value="tcn">繁体中文 Traditional Chinese</option>
                                <option value="scn">简体中文 Simplified Chinese</option>
                        </select>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>居住地址 Primary Residential Address</label></p>
                        <p><input type="text" id="Address" name="meta[Address]" class="form-control" value="" required="required"></p>
                        <p><input type="text" id="Address2" name="meta[Address2]" class="form-control" value=""></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>通讯地址 <small>（如与居住地址不同）</small> Correspondence Address</label></p>
                        <p><input type="text" id="CorrespondenceAddress" name="meta[Address3]" class="form-control" value=""></p>
                        <p><input type="text" id="CorrespondenceAddress2" name="meta[Address4]" class="form-control" value=""></p>
                        <p><span class="help-block">(if different from residential address)</span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>手机 Mobile</label></p>
                        <p><input type="text" id="Mobile" name="meta[Mobile]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>电邮 Email</label></p>
                        <p><input type="email" id="EmailChecker" name="acct[AccountEmail]" class="form-control" value="" data-validate-linked="email" required="required" rel="<?php echo View::url('checkemail'); ?>" checkmessage="Email already exists please enter a unique one or contact the administrator. 电子邮件已存在，请输入唯一的或联系管理员。"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></p>
                        <p>將以电子邮件作为与受托人(申购人)基本的联系方式 <br>The primary form of correspondence will be via email.</p>
                    </div>
                    
                </div>
                
            </div>
            
            <!-- Section 7 SUBSCRIBER BANK ACCOUNT INFORMATION -->
            <div class="section section-7">
                <div class="s-header">
                    <span><strong>07. 申购人银行帐户资料 SUBSCRIBER BANK ACCOUNT INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>收款银行地址 Bank Address</label></p>
                        <p><textarea rows="3" id="BankAddress" name="bank[Address]" class="form-control" value="" required></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>收款银行名称 Bank Name</label></p>
                        <p><input type="text" id="BankName" name="bank[Name]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户持有人姓名 Account Name</label></p>
                        <p><input type="text" id="BankAccountName" name="bank[AccountName]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户号码 Account Number</label></p>
                        <p><input type="text" id="BankAccountNumber" name="bank[AccountNumber]" class="form-control" value="" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行国际代码 / 城市代码 SWIFT Code</label></p>
                        <p><input type="text" id="SwiftCode" name="bank[SwiftCode]" class="form-control" value="" required="required"></p>
                    </div>
                </div> 
            </div>
            
            <!-- Section 8 ULTIMATE BENEFICIAL OWNER (UBO) DECLARATION< -->
            <div class="section section-8">
                <div class="s-header">
                    <span><strong>08. 最终受益方声明 (所指为信托受益人) ULTIMATE BENEFICIAL OWNER (UBO) DECLARATION</strong></span>
                </div>
                
                <div class="panel-body ubo-sect">
                    <p>I, <input type="text" placeholder="(全名，以护照为准 full name as in passport)" name="case[UBOName]" required="required" /> of <input type="text" name="case[UBOAddress]" placeholder="(详细住址 full residential address)" required="required" /></p>
                    <p>我，居住于以上填写住址，特此声明本人(我们)的资金及本人(我们)的资产来源于: <br>do hereby declare that my/our source(s) of funds which I/we have are my/our assets derived from: </p>
                    <section class="row">
                        <h5>请提供详细资讯 Please tick the source of funds</h5>
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOEmploymentIncome">
                            <p style="margin: -15px 0 0; padding-bottom: 0;">就业收入（任职公司,职务,年资,薪金金额）<br>Employment Income (employer, title, period, salary figures)</p></label>
                            
                            <div id="UBOEmploymentIncome" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOEmploymentIncome[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                        
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOCommission">
                            <p style="margin: -15px 0 0; padding-bottom: 0;">销售/佣金收入（公司的详细情况及相关资料）<br>Sales / Commission Income (details of company(ies) and value figures)</p></label>
                            <div id="UBOCommission" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOCommission[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                      
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOBusiness"> 
                            <p style="margin: -15px 0 0; padding-bottom: 0;">营业收入（营业相关资料与财务报表，确认每个相应年度的营业收入）<br>Business Income (business particulars and financial statements to determine general turnover and profits per respective years)</p></label>
                            <div id="UBOBusiness" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOBusiness[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                       
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOInheritance"> 
                            <p style="margin: -15px 0 0; padding-bottom: 0;">继承（继承血统及其价值）<br>Inheritance (inheritance lineage and value)</p></label>
                            <div id="UBOInheritance" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOInheritance[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                         
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOGift">                                             
                            <p style="margin: -15px 0 0; padding-bottom: 0;">赠与物（赠与者及赠与物之细节及其价值）<br>Gift (the details of giver and details of gift and its value)</p></label>
                            <div id="UBOGift" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOGift[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                        
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOSales"> 
                            <p style="margin: -15px 0 0; padding-bottom: 0;">出售物业收益或其他资产（资产价值的描述，以及如何在第一时间取得）<br>Proceeds from the sale of property and/or other assets (description of assets and value and how they were obtained in the first instance)</p></label>
                            <div id="UBOSales" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOSales[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                         
                        <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                           <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOOther">                                             
                           <p style="margin: -15px 0 0; padding-bottom: 0;">其他补充事项（请进行描述）<br>Any other (please describe)</p></label>
    
                            <div id="UBOOther" class="ubofiles">
                                Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOOther[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div>
                    </section>
                    
                    <p><label><input type="checkbox" id="UBODeclarationConfirm1" name="UBODeclarationConfirm" value="Y" required="required"> I further declare that :</label></p>
                    <ul style="padding-left: 20px;">
                        <li>I am not involved in money laundering and/or drug trafficking; and</li>
                        <li>any monies and/or securities that have been or will be deposited in
any bank account do not originate, either directly or indirectly, from
illegal and/or criminal activities.</li>
                    </ul>
                    <br>
                    <p><label>我进一步声明:</label></p>
                    <ul style="padding-left: 20px;">
                        <li>我没有参与洗黑钱或贩卖毒品;</li>
                        <li>已经存入或将要存入的公司银行帐户、及其任何款项或证券的
任何款项，并无直接地或间接地参与非法或犯罪活动。</li>
                    </ul>
                </div> 
            </div>
            
            <!-- Section 9 DECLARATION OF POLITICALLY EXPOSED PERSON (“PEP”) AND TAX STATUS -->
            <div class="section section-9">
                <div class="s-header">
                    <span><strong>09. 税务及政治人物声明 DECLARATION OF POLITICALLY EXPOSED PERSON (“PEP”) AND TAX STATUS</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p><strong>我特此声明：</strong></p>
                        <p>
                        <ul>
                            <li>本人／我们不是政治人物或政治人物的密切关连人士。</li>
                            <li>本人／我们并非美国人士1，并且不打算成为美国公民。</li>
                            <li>本人／我们的税务居地及税务识别号为：</li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <p><strong>I hereby declare that :</strong> </p>
                        <p>
                        <ul>    
                            <li>I am not a PEP2 or a Closely Connection Person to a PEP3. </li>
                            <li>I am not a U.S. Person and do not intent to be one. </li>
                            <li>My tax residency and tax identification number are:</li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>主要税务居地 Primary Tax Residency</label></p>
                        <p><textarea name="case[PrimaryTaxResidency]" class="form-control" rows="3" required></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>税务识别号 Tax ID No.</label></p>
                        <p><textarea class="form-control" name="case[TaxIdNumber]" rows="3" required></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <ul>
                            <li>以上所有资料属实、完整及正确无误，并且进一步承诺即时通知Odeon及信托公司此资料的任何变化及同意提交申购人所需要的额外文件。 <br> &nbsp;</li>
                            <li>The above information is true, complete and accurate; and further
undertake to notify Odeon and the Escrow Trustee promptly of any
change in the aforementioned information and agree to submit
additional documents to the Escrow Trustee upon its request.</li>
                        </ul><Br>
                        
                        <div class="g-header">
                            <strong><sup>1</sup> 就遵从美国《海外帐户纳税法案》而言，“美籍人士”是指：</strong>
                        </div>
                        <ul>
                            <li>个人：美国公民或合法永久居民 (绿卡持有者)；或出生在美国；或者有美国的居住地址或满足国税局 (www.irs.gov) 定义的实际居留身份。<br>
                                Individuals : U.S. citizen or lawful permanent resident (green card holder); or born in the U.S.; or have a U.S. residential address or meeting the substantial presence test for the calendar year as defined by the IRS (www.irs.gov).<br> &nbsp;
                            </li>
                            <li> 企業：美國的合夥企業或美國公司。 <br>Corporations: a U.S. partnership or U.S. corporation <br> &nbsp;</li>
                            <li>信託：任何信託，例： (a) 美國的法院可對信託的管理行使本監管；及 (b) 一個或多個美籍人士有權控制信託的所有重大決策。 <Br>Trusts : Any trust if (a) a court within U.S. is able to exercise primary supervision over the administration of the trust; and (b) one or more U.S. persons have the authority to control all substantial decisions of the trust. </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="g-header">
                            <strong><sup>2</sup> 政治人物的例子包括： <br> <sup>2</sup> Examples of Politically Exposed Persons include:</strong>
                        </div>
                        <p>
                        <ul>
                            <li>国家元首或政府首脑 Heads of State or Government </li>
                            <li>资深政治家 Senior Politicians </li>
                            <li>高级政府、司法或军事官员 Senior Government, Judicial or Military Officials </li>
                            <li>国有企业的高级管理人员  Senior Executives of State-Owned Corporations </li>
                            <li>重要政党官员 Important Political Party Officials </li>
                            <li>皇室 Royal Family</li>
                        </ul>
                        </p>
                        
                        <div class="g-header">
                            <strong><sup>3</sup> 密切关连人士的例子包括: <br><sup>3</sup> Examples of Closely Connected Persons include:</strong>
                        </div>
                        <p>
                        <ul>
                        <li>家庭成员(配偶、子女、父母、兄弟姐妹，包括姻亲) <Br>Family members (spouse, children, parents, brothers and sisters, including in-laws) <br> &nbsp;</li>
                        <li>助手和其他贴身顾问(社交或专业)<br> Aides and Other Close Advisors, socially or professionally <br> &nbsp;</li>
                        <li>业务伙伴／ 战略合作方 <br>Business associates <br> &nbsp;</li>
                        <li>高级政治人物拥有权益或影响力的公司 <Br>Companies in which a Senior Political Figure has an interest or exercises influence </li>
                        </ul>
                        </p>
                    </div>
        
                </div>
                
            </div>
            
            <!-- Section 11 TRUST ACCOUNT BANKING INFORMATION -->
            <div class="section section-10">
                <div class="s-header">
                    <span><strong>10. 信托帐户银行资料  TRUST ACCOUNT BANKING INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-12 col-xs-12">
                        <div class="g-header-1">信托存款，请汇款至 For Remittance into Trust</div><br>
                    </div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户名称 Account Name:</label> Odeon & Co Pte Ltd</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户名称 Account Name:</label> 深圳钰鼎一号创业投资企业(有限合伙）</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户名称 Account Name:</label></p>
                        <p><input type="text" id="TrustAccountName" name="trust[AccountName]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户号码 Account Number:</label></p>
                        <p>
                        373-307-132-8（SGD 新幣）<br>
                        373-901-199-8（USD 美元）
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帐户号码 Account Number:</label></p>

                        <p>
                        4000021209200754136(RMB)
                        </p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>收款人户口号码 Account Number:</label></p>
                        <p><input type="text" id="TrustAccountNumber" name="trust[AccountNumber]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行名称 Bank Name:</label></p>
                        <p>United Overseas Bank Limited</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行名称 Bank Name:</label></p>
                        <p>中国工商银行</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行名称 Bank Name:</label></p>
                        <p><input type="text" id="TrustName" name="trust[Name]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行国际代码 Swift Code:</label></p>
                        <p>UOVBSGSG</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行国际代码 Swift Code:</label></p>
                        <p>ICBKCNBJSZN</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行國際代碼 Swift Code:</label></p>
                        <p><input type="text" id="TrustSwiftCode" name="trust[SwiftCode]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行地址 Bank Address:</label></p>
                        <p>80 Raffles Place, UOB Plaza 1, Singapore 048624</p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行地址 Bank Address:</label></p>
                        <p>深圳市罗湖区深南东路5002号地王大厦附楼G层</p>
                    </div>
                    <!--<div class="col-sm-6 col-xs-12 form-group">
                        <p><label>银行地址 Bank Address: </label></p>
                        <p><input type="text" id="TrustAddress" name="trust[Address]" class="form-control" value="" required="required"></p>
                    </div>-->
                    <div class="clearfix"></div>
                    
                </div> 
            </div>
            
            <!-- Section 11 CONTRIBUTION METHODS -->
            <div class="section section-11">
                <div class="s-header">
                    <span><strong>11. 信托存款资讯 CONTRIBUTION METHODS</strong></span>
                </div>
                <!-- <pre>
                    <?php print_r($items); ?>
                </pre> --> 
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p class="hidden-xs"><img src="<?php echo view::url(); ?>/assets/images/qr.jpg"> <span style="display: inline-block; font-size: 20px; color: #78664a; position: absolute; top: 40%; margin: 0 0 0 40px;">信託存款帳戶資訊 <br><br><a href="http://tt.OdeonCo.com" style="color: #78664a; text-decoration: none;">http://tt.OdeonCo.com</a></span></p>
                        <div class="visible-xs text-center">
                            <p><img src="<?php echo view::url(); ?>/assets/images/qr.jpg" class="img-responsive" style="margin: 0 auto;"></p>
                            <p><span style="display: inline-block; font-size: 20px; color: #78664a;">信託存款帳戶資訊 <br><br><a href="http://tt.OdeonCo.com" style="color: #78664a; text-decoration: none;">http://tt.OdeonCo.com</a></span></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="form-group">
                            <p><label>Package Type | 購買商品系列(請選擇)</label></p>
                            <select name="acct[ProductID]" id="ProductID" class="form-control" required rel="<?php echo View::url('form/get/'); ?>">
                                <option selected>Select Option</option>
                                <?php
                                foreach ($prods as $prod) {
                                ?>
                                    <option value="<?php echo $prod->ProductID; ?>"><?php echo $prod->ProductName; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <p><label>Package Terms | 配套條款</label></p>
                            <select id="ProductItemID" class="form-control" required rel="<?php echo View::url('form/getPrItem/'); ?>">
                                <option selected="selected">Select</option>
                                
                            </select>
                            <input type="hidden" id="theProductItemID" name="acct[ProductItemID]" value="">
                            <input type="hidden" id="StepUp" value="">
                            <input type="hidden" id="Invested" value="">
                            <p id="couponAmount" class="help-block" style="margin-bottom: 0;"></p>
                        </div>

                        <div class="form-group">
                            <p><label>Final Amount | 投資金額</label></p>
                            <input type="text" name="acct[DepositedAmount]" class="form-control" id="DepositedAmount">
                        </div>

                    </div>     
                </div> 
            </div>
            
            <!-- Section 12 DOCUMENTATION REQUIREMENTS (INDIVIDUAL) -->
            <div class="section section-12 acounttype_individual">
                <div class="s-header">
                    <span><strong>12. 信托帐户申请的文件要求 (个人专用) DOCUMENTATION REQUIREMENTS (INDIVIDUAL)</strong></span>
                </div>
                
                <div class="panel-body g-body">                
                    <div class="g-header-1"><strong>个人申购人 INDIVIDUAL SUBSCRIBER</strong></div>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>身份证件 Photo ID<br> (如身份证/护照/驾驶证)<br> (National ID / Passport / Driver License preferred)<br><br> <input class="file" type="file" name="IAPhotoid[]" data-show-upload="true" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>居住证明（3个月内）<br> Proof of Residency (of not more than 3 months old)<br> (如银行对帐单/信用卡对帐单/保险对帐单/水电费帐单)<br> (Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill preferred)<br><br> <input id="ia-proofresidency" class="file" type="file" name="IAProofresidency[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>银行对帐单（3个月內）和/或同等文件<br> Bank Statement (of not more than 3 months old) and/or equivalent documents<br> (显示资产和/或基金多于信托存款金额)<br> (Displaying assets and/or funds more than the Trust Deposit Amount)<br><br> <input id="ia-bankstatement" class="file" type="file" name="IABankstatement[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>签名样本 Specimen Signature <br></p>
                                    <div class="signatureSection" upload-name="IASpecimensign" data-name="signatures[IASpecimensign]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder border-bot full">
                            <td>
                                <p>支付費用的证明<br>Proof of Payment of Fees<br><br>
                                <input id="ca-shareholders" class="file" type="file" name="IAProofOfPayment[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                    </tbody></table>
                    
                </div> 
            </div>
            
            <!-- Section 13 DOCUMENTATION REQUIREMENTS (CORPORATE) -->
            <div class="section section-13 acounttype_corporate">
                <div class="s-header">
                    <span><strong>13. 信托帐户申请的文件要求 (机构专用) DOCUMENTATION REQUIREMENTS (CORPORATE)</strong></span>
                </div>
                
                <div class="panel-body g-body">                
                    <div class="g-header-1"><strong>机构申购人资料（请勾选，并附上附件资料） CORPORATE SUBSCRIBER</strong></div>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%;">
                                <p>公司注册证 Certificate of Incorporation<br><br>
                                <input id="ca-namechange" class="file" type="file" name="CACertincorporation[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td>
                                <p>名称变更证明（如适用） Certificate of Name Change (if applicable)<br><br> 
                                <input id="ia-namechange" class="file" type="file" name="CANamechange[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>
                        
                        <tr class="tr-td-withborder">
                            <td style="width: 50%;">
                                <p>存续证明（如适用） Certificate of Good Standing (if applicable)<br><br>
                                <input id="ca-goodstand" class="file" type="file" name="CAGoodstand[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td>
                                <p>董事及股东名册 Registry of Director(s) & Officer(s)applicable)<br><br> 
                                <input id="ca-regdirector" class="file" type="file" name="CARegdirector[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>  
                    </tbody></table>

                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder full">
                            <td>
                                <p>如银行对帐单/信用卡对帐单/保险对帐单/水电费帐单<br>Proof of Business Address (Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill preferred)<br><br>
                                <input id="ca-proofbusadd" class="file" type="file" name="CAProofbusadd[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%;">
                                <p>公司章程及组织大纲<br>Memorandum and Articles of Association<br><br>
                                <input id="ca-memorandumaa" class="file" type="file" name="CAMemorandumaa[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td>
                                <p>近期财务报表 (如适用)<Br>Most Recent Financial Statements (if applicable)<br><br> 
                                <input id="ca-recentfinancialstatement" class="file" type="file" name="CARecentfinancialstatement[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%; border-right:0; border-bottom:0;">
                                <p><label>身份证件 Photo ID</label></p>
                                <p>法定代表人 Director(s)<br><br>
                                <input id="ca-directorsid" class="file" type="file" name="CADirectorsid[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td style="border-left:0; border-bottom:0;">
                                <p><label>&nbsp;</label></p>
                                <p>公司签署人 Company Signatory(ies)<br></p>
                                <div class="signatureSection" upload-name="CACompanysign" data-name="signatures[CACompanysign]">
                                    <div class="radio signOptions">
                                        <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                        <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                    </div>
                                    <div class="parentsign">
                                        <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                        <div class="sign-upload hidetag">
                                            <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">

                        <tbody>
                        <tr class="tr-td-withborder border-top full">
                            <td>
                                <p>股东持有公司股本10%或持有控制权的 (投票权) 股东<br>Shareholer(s) holding 10% or more of Company Share Capital and/or Controlling (Voting) Shareholder<br><br>
                                <input id="ca-shareholders" class="file" type="file" name="CAShareholders[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%; border-right:0; border-bottom:0;">
                                <p><label>居住证明 (3个月内) Proof of Residency (of not more than 3 months old)</label></p>
                                <p>法定代表人 Director(s)<br><br>
                                <input id="ca-directorsproof" class="file" type="file" name="CADirectorsproof[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                            <td style="border-left:0; border-bottom:0;">
                                <p>公司法定负责人 Company Signatory(ies)<br></p>
                                <div class="signatureSection" upload-name="CACompanysignproof" data-name="signatures[CACompanysignproof]">
                                    <div class="radio signOptions">
                                        <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                        <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                    </div>
                                    <div class="parentsign">
                                        <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                        <div class="sign-upload hidetag">
                                            <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    <table border="0" cellpadding="3" cellspacing="3">
    
                        <tbody>
                        <tr class="tr-td-withborder border-bot border-top full">
                            <td>
                                <p>股东 (持有公司股本10%或以上及有实质执行权的股东)<br>Shareholer(s) holding 10% or more of Company Share Capital and/or Controlling (Voting) Shareholder<br><br>
                                <input id="ca-shareholdersproof" class="file" type="file" name="CAShareholdersproof[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr> 
                        <tr class="tr-td-withborder border-bot full">
                            <td>
                                <p>支付費用的证明<br>Proof of Payment of Fees<br><br>
                                <input id="ca-shareholders" class="file" type="file" name="CAProofOfPayment[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                            </td>
                        </tr>
                    </tbody></table>
                    
                    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>  
                            <tr>
                                <td colspan="2">
                                    <p><strong>授权签署人名单及签名样本<br> Authorised Signatory Lists & Specimen Signatures</strong></p>
                                </td>
                            </tr>                            
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <div class="signatureSection" upload-name="CAAuthorizedone" data-name="signatures[CAAuthorizedone]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>                                    
                                    <input type="text" name="case[CAAuthorizedonename]" class="form-control" placeholder="姓名 Name :" /><br>
                                    <input type="text" name="case[CAAuthorizedonetitle]" class="form-control" placeholder="称谓 Title :" /></p>
                                </td>
                                <td>
                                    <div class="signatureSection" upload-name="CAAuthorizedtwo" data-name="signatures[CAAuthorizedtwo]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <input type="text" name="case[CAAuthorizedtwoname]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedtwotitle]" class="form-control" placeholder="称谓 Title :" />
                                    </p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <div class="signatureSection" upload-name="CAAuthorizedthree" data-name="signatures[CAAuthorizedthree]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <input type="text" name="case[CAAuthorizedthreename]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedthreetitle]" class="form-control" placeholder="称谓 Title :" /></p>
                                </td>
                                <td>
                                    <div class="signatureSection" upload-name="CAAuthorizedfour" data-name="signatures[CAAuthorizedfour]">
                                        <div class="radio signOptions">
                                            <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                            <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                        </div>
                                        <div class="parentsign">
                                            <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                            <div class="sign-upload hidetag">
                                                <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                    <input type="text" name="case[CAAuthorizedfourname]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedfourtitle]" class="form-control" placeholder="称谓 Title :" /></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
     
                </div> 
            </div>
            
            <!-- Section 14 -->
            <div class="section section-14">
                <div class="s-header">
                    <span><strong>14. 申购协议及重要资料</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>投资信托与多种投资产品相连结，使您的投资信托帐户具有多元化的投资种类。请详
细阅读开户程序的条款书和申购书，投资信托存有投资风险；除非书面声明，此投资没有保证回报。过往的投资回报并不一定反映未来的表现。</p>
                        <p>在每个条款清单上已有免责声明特别提示，您需要接受相对的投资风险，并要了解所有投资都是有风险的。</p>
                        <div class="g-header-2">申请加入信托声明</div>
                        <p>本人／我们，作为上述指定的个人，已拥有完整的法律权力，申请成为投资信托的申
购人，并证明：</p>
                        <ol type="a">
                            <li>本人／我们在没有丧失法律行为能力的情况下提出此申请;</li>
                            <li>本人／我们已遵守所有适用法律去提出本次申请;</li>
                            <li>本人／我们／我们在此开户程序中提供的所有资讯均是完整，真实且正确无误的。</li>
                            <li style="list-style:none; margin-left: -15px;"><p>本人／我们已阅读、了解并同意附加的申购人协议的条款;</p></li>
                            <li>作出草签，以表示本人／我们察觉与了解信托申购人会籍、放弃分散投资、免投
资风险和各项免责声明所带来的费用;</li>
                            <li>现申请作为此信托的申购人。</li>
                        </ol><br>
                        <p>本人／我们进一步声明,本人/我们是一个多元的、专业的、经认可的或是等同上述说
明的资深投资者、在我们司法管辖所在地任何适用的说明书及小册子中得以豁免显示
本人/我们的购买、供款、登记和通知要求。关于该投资信托计划的发行、销售或营
销等相关资讯、本人/我们已经获得或寻求金融和法律专业人士的适当建议来确认此
一说法、 任何因此而产生的纠纷或争议、本人/我们在此声明放弃任何诉讼或司法听
证会之权利。</p><Br>
                        <p>申购人执行日期如下:</p>
                        
                        <div class="g-header-2">基本释义</div>
                        <p>在这申购协议和整个开户程序中，下列词汇具有以下涵义:</p>
                        <ul>
                            <li>“开户程序”指全套的文件，包括申购协议，及申购人提交并入档的任何补充文
件或随附表格;</li>
                            <li>“费用”指信托申购人根据内附的标准需要交付的各项费用，或随信托计划不定
期的变更而可能产生的其他费用;</li>
                            <li>“ 托管公司”是 Odeon & Co Pte Ltd (简称“Odeon”)</li>
                            <li>“信托公司”是 Zico Allshores Trust (Singapore) Pte Ltd</li>
                            <li>“申购人”指签署信托之申购人，并获得信托公司和托管公司 Odeon 接受。在
信托公司按情况制定的合理时间内，申购人必须遵守上述的信托条件，方可获
得信托公司结算其投资回报;</li>
                            <li>“授权核证人”指一个在其所属司法辖区大于法定年龄的人，在得托管公司允许
其负责核证文件之前，须先由信托公司和托管公司对其进行反洗钱 (“AML”)
和公共记录核实或其他背景调查。</li>
                            <li>“信托契约”指在 2016 年已分别执行的 <信托契约> 和 <信托声明>和信托公司
有任何修订后存放于管理人及信托公司办事处的已执行副本。</li>
                        </ul><br>
                        <p>除非文义另有所指或是此申购协议内有更改或另有列明，载于此申购人协定内的文字应与信托契约内的文字具有相同含义:</p>
                        
                        <ol type="i">
                            <li>单数字词包含复数字词，反之亦然，</li>
                            <li>在此申购协议中标题可被忽略。</li>
                            <li>所述及的人应包括法人团体， 非法人社团和合伙公司。</li>
                            <li>凡述及的任何文件、协议、法律或法规，都应该理解为目前已生效、经过不定期修订、变更、补充、替代或以新代旧。</li>
                        </ol><Br>
                        
                        <p>这申购协议的任何条款细则和/或任何附属协议，在现在或将来某天，如果可能失去效力或变成无效，其申购协议和任何所有附属协定的其余部分应仍然生效。</p><br>
                        <p>申请程式以新加坡法律为解释依据，并受香港法律管辖 。</p><br>
                        
                        <div class="g-header-2">信托认购者向托管公司声明</div>
                        <p>本开户书签字人,已符合资格申请加入成为其信托申购人,现同意并做出如下承诺:</p>
                        <ol type="a">
                            <li>本人／我们声明，在开户程序填妥的内容里，本人／我们提供的资讯在各方面都准确无讹。</li>
                            <li>本人／我们声明在收到此开户程序时，本人／我们已得到相关建议，内容包括加入信托后本人／我们将得到的利益，决定这些利益的方法，以及这些利益的相关条款。</li>
                            <li>本人／我们兹向托管公司确认，已经知悉托管公司建议，并就其所推荐的投资信托计划，独立咨询与之相关的金融、法律和税务意见，而本人／我们亦就以上本
人／我们认为需要知道的其他关于该投资信托计划的可能含义和托管公司的托管情况作出询问。</li>
                            <li>本人／我们了解就所推荐的投资信托计划或该计划对本人／我们的影响，或该计划可能受惠于其他人的可能性，信托公司尚未向本人／我们提供与之相关的税务意见 。</li>
                        </ol>
                        
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="g-header-2">托管公司守则</div>
                        <ol type="a">
                            <li>有关投资信托基金的操作或管理，托管公司的任何决定均是最终决定，各方均同意由信托公司制定的政策、指引、指令或命令，均必须贯彻始终并严格执行。</li>
                            <li>如果任何一方希望改变托管公司制定的政策、指引、指令或命令，可以书面提出要求， 而托管公司可以全权决定是否准许。</li>
                        </ol>
                        <div class="g-header-2">放弃分散投资</div>
                        <p>本人／我们同意选择参与由信托公司建议并得到托管公司批准的相关投资或长期策略投资。因此，本人／我们放弃了由托管公司管理的分散投资需要。</p>
                        <p>本人／我们接受，当托管公司按本人／我们要求作出投资时:</p>
                        <ol type="a">
                            <li>若因信托公司和托管公司的任何建议、代表、举动、疏忽或行为而造成损失时，本人／我们不会向信托公司和托管公司索取赔偿。</li>
                        </ol>
                        <div class="g-header-2">投资</div>
                        <p>本人／我们明了在本人／我们提出要求信托公司指示财务顾问作出特定投资时，本人／我们知悉存在固有风险，市场价值有涨有跌，本人／我们不应为信托公司和托管公司执行了本人／我们的要求而向其索偿。托管公司并没有向本人／我们提供财务建议，亦未有就有关的任何投资作出担保。就本人／我们已选择的投资(包括但不限于专业投资)，本人／我们声明:</p>
                        <ol type="a">
                            <li>本人／我们有足够经验了解与该投资相关的特性和风险;</li>
                            <li>本人／我们在决定投资该项目前，已阅读并充分理解该销售档，尤其当中包括销售档内关于投资各项目的风险和费用的资讯;</li>
                            <li>本人／我们接受有关事项投资的所有风险，尤其本人／我们在专业投资所涉及的风险，有可能导致部分比例或全部投资的亏损;</li>
                            <li>就本投资结构内的各项投资专案于个人是否合适，本人／我们已听取过独立意见;</li>
                            <li>信托公司并没有进行促销或提供任何建议，亦没有就投资的绩效风​​险、投资的监管问题或安全性(包括任何明示或暗示的保证)表示任何意见。</li>
                            <li>本人／我们同意赔偿信托公司因投资选择问题引起或与其相关的索偿、成本支出、债务、费用、损害或损失(包括但不限于任何因此而受影响的损失/利润损失， 名誉损失， 全部利息罚款， 法律及其他专业收费) 。</li>
                        </ol>
                        <div class="g-header-2">标准规定收费</div>
                        <p>通过填妥本申请表，本人／我们同意支付一次性信托帐号开户费美元 $535 (个人申购 人) / 美元 $842 (机构申购人) 和本人所有受信托管理资产的每年 2％ 管理费。所有 费用提示前支付，不予退还。</p>
                        <p>信托公司和托管公司保留可追回已发放款项与实际支出（如直接从计画衍生的第三方 收费）的权利。尤其若基金结构复杂或按成员要求而独有，额外的第三方收费可能会 因管理该基金而产生。各项收费将根据个别具体情况而订，这些费用通常会事先通知 并商定后方确立收费。</p>
                        
                        <p>为达到“了解客户”，根据“信托公司条例”，本人／我们同意信托公司和托管公司可于全球范围内收到本人／我们资产基数的所有必要资讯。本人／我们明白，根据“ 个人资料(私隐)条例”，必须本人／我们授权方可披露本人／我们的个人资料，本人／我们也明白，在未经本人／我们进一步书面签署授权下，本人／我们的资料不会披露予信托公司的信托要求以外的任何人士、公司或机构。</p>
                        <div class="g-header-2">放弃冷静期</div>
                        <p>成员申请之日起有 7 个工作日的冷静期：在此期间申请将被搁置，若决定退出计划，
信托帐号开户费和首年的管理费，扣除银行汇款或转账手续费用后可获退还。由于直
到冷静期过后 Odeon 和信托公司才能开始进行相关作业，所以申购人可以在此签字
放弃冷静期。在这种情况下，信托基金的设置将在收到申请后随即开始。在冷静期后
或在申购人放弃冷静期的情况下，上述费用将不会退还。</p>
                        <table class="table" border="0">
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="cneedcoolingoff pull-left" id="WaiveCoolingOffC1" name="NeedCoolingOff" value="Y"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffC1"> 我选择 7 天的冷静期，如我 7 日内无额外通知，到时启动我的信托账号。</label></td>
                            </tr>
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="cneedcoolingoff pull-left" id="WaiveCoolingOffC2" name="NeedCoolingOff" value="N"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffC2"> 我选择放弃冷静期，立即启动我的信托账号。</label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
             
            <!-- Section 15 SUBSCRIPTION AGREEMENT & INFORMATION -->
            <div class="section section-15">
                <div class="s-header">
                    <span><strong>15. SUBSCRIPTION AGREEMENT & INFORMATION</strong></span>
                </div>
                
                <div class="panel-body">
                    <div class="col-sm-6 col-xs-12">
                        <p>Investment Trust is linked to various investment products which may include a wide array
of investments received in Escrow Trust for your account. Please see the Term Sheet and
Subscription included with this Account Opening Package. Each of the Investment Trust is
designed to follow a different investment strategy. Investment Trust carry investment risks.
Unless stated otherwise, the returns are not guaranteed. Do note that the past returns are not
necessarily indicative of the future performance. </p>
                        <p>Disclaimers specific to Investment Trust are written in each Term Sheet. You acknowledge
there is always risk in investments.</p>
                        <div class="g-header-2">Declaration to the Trust</div>
                        <p>I, being the above named individual and having full legal authority to make this Subscription for membership in the investment trust program offered by Odeon, certify that:</p>
                        <ol type="a">
                            <li>I have no legal disability of any description to make this subscription;</li>
                            <li>I have complied with all applicable laws in making this subscription;</li>
                            <li>All information given by me and contained in this Subscription Form is complete, true and correct.</li>
                            <li style="list-style:none; margin-left: -15px;">I have read and understood the Subscription Agreement; and</li>
                            <li>I have taken note of and initialled to signify my awareness and understanding of the fees payable by Subscriber, diversification waiver, investment risk waiver and disclaimers; and</li>
                            <li>hereby make subscription as a Subscriber of the investment trust program.</li>
                        </ol><br>
                        <p>I/we further declare that I am / We are a sophisticated, professional or accredited investor
                            as such terms are defined, or the equivalent thereof, in my home jurisdiction to qualify
                            and exempt my purchase, contribution, or participation in this investment program from
                            any applicable information memorandum, registration and notice requirements for the
                            distribution, sale, or marketing of securities that may apply to this investment trust program,
                            and I have obtained or sought the appropriate advice from financial and legal professionals to
                            confirm this statement, thus waiving my rights to claim otherwise in any dispute or proceeding
                            in law, court or hearing.</p>
                        <p>Executed by Subscriber on the date below:</p>
                        
                        <div class="g-header-2">General Interpretation</div>
                        <p>In this Subscription Agreement and the entire Account Opening Package the following terms shall have the following meanings:</p>
                        <ul>
                            <li>“Account Opening Package” means this entire document, including the Subscription Agreement, and any supplementary or accompanying forms provided and filed by the Subscriber;</li>
                            <li>“Fees” means the amounts payable by a Subscriber pursuant to attached Fee Schedule, or any other fees that may be charged, which are subject to change from time to time;</li>
                            <li>“Management Company” is Odeon & Co Pte Ltd (hereby also known as Odeon)</li>
                            <li>“Escrow Trustee” is Zico Allshores Trust (Singapore) Pte Ltd</li>
                            <li>“Subscriber” is a person who signs this Subscription Form and provides funds to Odeon
                                to become a Subscriber of the investment trust program. Under this program, Odeon
                                will transfer funds received from Subscribers. Any person who becomes a Subscriber
                                must fulfil the conditions of Subscription as set out above in order to be available for
                                settlements at Odeon’s discretion;</li>
                            <li>“Approved Certifier” is a natural person of greater than the age of majority in their home
                                jurisdiction, who has completed an Anti-Money Laundering (“AML”) and public records
                                check or other background reviews at the discretion of Odeon, prior to having been
                                approved by Odeon to certify documents for reliance upon the AML review by the Escrow
                                Trustee and Odeon;</li>
                            <li>“Trust Deed” means the Escrow Trust Deed [and Declaration of Trust respectively]
                                executed in the year of 2016 and any amendments by the Escrow Trustee, executed
                                copies of which are maintained at the offices of the Escrow Trustee.</li>
                        </ul><br>
                        <p>Unless the context otherwise requires and except as varied or otherwise specified in this
                            Subscription Agreement, words and expressions contained in this Subscription Agreement
                            shall bear the same meanings as in the Escrow Trust Deed:</p>
                        
                        <ol type="i">
                            <li>the singular includes the plural and vice versa;</li>
                            <li>headings shall be ignored in the construction of this Subscription Agreement;</li>
                            <li>references to persons shall include body corporates, unincorporated associations and partnerships; and</li>
                            <li>references to any documents or agreements, laws or regulations, are to be construed as
                                references to such documents or agreements, laws or regulations, as is in force for the
                                time being and as amended, varied, supplemented, substituted or novated from time to
                                time.</li>
                        </ol><Br>
                        
                        <p>In the event that any of the clauses, terms and conditions of this Subscription Agreement
                            and/or any of the subsidiary agreements either now or at a future date, should be held to be
                            without force and effect or be otherwise invalid, the rest of the Subscription Agreement and
                            any and all subsidiary agreements shall remain in effects.</p><br>
                        <p>This entire Account Opening Package is construed and shall be governed by and in accordance with the laws of Singapore.</p><br>
                        
                        <div class="g-header-2">Subscriber’s Declaration to Odeon</div>
                        <p>I, the undersigned person, hereby being eligible to apply for Subscription of the trust, agree and undertake as follows:</p>
                        <ol type="a">
                            <li>I declare that to the extent that I have completed the Account Opening Package, the information contained is accurate in every respect.</li>
                            <li>I declare that at the time I was given this Account Opening Package, I was also given advice about the benefits to which I 
                            would be entitled as a Subscriber, the method of determining that entitlement and the conditions relating to those benefits.</li>
                            <li>I confirm to Odeon, that I have been advised by Odeon to take independent financial,
                                legal and taxation advice on the investment trust program offered by Odeon, and that
                                I have made such enquiries and taken such financial, legal, taxation and other advice
                                as I consider necessary concerning all possible implications concerning the proposed
                                program.</li>
                            <li>I acknowledge that the Escrow Trustee has not given me any tax advice concerning the
                            proposed investment trust program on my circumstances or on the circumstances of any
                            other person likely to be affiliated with or benefiting from the plan.</li>
                        </ol>
                        
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="g-header-2">Code of Practice</div>
                        <ol type="a">
                            <li>Any decision by Odeon pertaining to the operation of or administration of the investment
                            trust program is final and the parties all hereby agree that policies, instructions,directions
                            or orders made by Odeon must be followed rigorously and at all times.</li>
                            <li>Should any party wish to vary the policies, instructions, directions or orders made by
                            Odeon, they may request such variance in writing and the approval of such variance shall
                            be at the sole discretion of Odeon.</li>
                        </ol>
                        <div class="g-header-2">Diversification Waiver</div>
                        <p>I have endorsed the Escrow Trustees’ election to participate in underlying investments or a
                            longer term investment strategy as proposed by Odeon and adopted by the Escrow Trustee. I
                            therefore waive the need for diversification of investments by Odeon.</p>
                        <p>I accept that when Odeon invests funds I have provided:</p>
                        <ol type="a">
                            <li>a. no claim will be made by me against Odeon or Escrow Trustee for any advice, representations, acts, omissions, or conduct of Odeon or Escrow Trustee.</li>
                        </ol>
                        <div class="g-header-2">Investments</div>
                        <p>I acknowledge that I am aware of the inherent risks that market values can go down as well as
                            up; and that I shall make no claim on Odeon or Escrow Trustee for carrying out my requests.
                            Odeon has not provided me financial advice and has made no warranty or representation in
                            relation to any investment. Where I choose or have chosen an investment, including but not
                            limited to specialist investment, I declare that:</p>
                        <ol type="a">
                            <li>l am sufficiently experienced to understand the features and risks associated with the investment; and;</li>
                            <li>I have read and fully understood the offering document, including in particular the
                            information on the risks and charges associated with investing in the investments
                            contained in the offering document before deciding to invest into it and;</li>
                            <li>I personally accept all risk associated with the investment, and in particular that my
                            investment in a specialist investment involves risk that could in loss of a significant
                            proportion or all of the sum involved; and;</li>
                            <li>I have taken independent advice on the suitability of the investments within this structure; and;</li>
                            <li>The Escrow Trustee has not promoted the investment or provided any advice, made any
                            recommendation or expressed any opinion whatsoever in respect of the performance
                            risk, regulatory issues or security (including any express or implied guarantees) of the
                            investment.</li>
                            <li>I agree to indemnify and keep the Escrow Trustee indemnified from and against all claims,
                            cost demands, liabilities, expenses, damages or losses (including without limitation any
                            consequential losses/loss of profit and loss of reputation, and all interest penalties and
                            legal and other professional costs) arising out of or in connection with the choice of
                            investment.</li>
                        </ol>
                        <div class="g-header-2">Standard Fee Schedule</div>
                        <p>By completing this Subscription, I agree to pay the one-time account set-up fee of USD535
                        (Individual Subscriber) / USD842 (Corporate Subscriber) (“Set-up Fee “) and the first annual
                        administration fee of 2% of all my Assets Under Management (AUM) (“Management Fee”). I
                        agree to pay subsequent annual Administration Fees. All fees are payable in advance and
                        non-refundable.</p>
                        <p>The Escrow Trustee and Odeon reserve the right to recover disbursements and out-of-pocket
                        expenses, such as third party charges, direct from the plan. Additional third party charges
                        may be incurred in administering the structure, in particular where the structure is complex or
                        unique to the Subscriber’s requirements. Charges will be discussed on a case-by-case basis
                        and such charges will normally be notified and agreed in advance.</p>
                        <div class="g-header-2">Authority for Disclosure of Personal Data</div>
                        <p>I hereby consent for Odeon and Escrow Trustee to receive all necessary information on my
                            asset base globally as it required for the purposes of “Know Your Client”. I understand that
                            my authority is required under the Personal Data (Privacy) Ordinance and I also understand
                            that this information will not be disclosed outside of Odeon and Escrow Trustee’s legal
                            requirements to any person, company or any institution without my further written and signed
                            authority.</p>
                        <div class="g-header-2">Cooling-off Period Waiver</div>
                        <p>There is a 14 working day cooling-off period from the date of this Subscription: during this
                            period, the Subscription will be on hold, and Set-up and first year Administration Fees can be
                            refunded, net of the relevant banking charges. As the Odeon and the escrow trustee cannot
                            commence set-up until after the Cooling-off Period, if the Subscription is time-sensitive, the
                            subscriber may, by opting here, waive the Cooling-off Period. In that event, set-up of the
                            investment trust program will commence upon receipt of the Subscription. After the Coolingoff
                            Period, or in the event that the Cooling-off Period is waived, fees shall be non-refundable.</p>
                        <table class="table" border="0">
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="eneedcoolingoff pull-left" id="WaiveCoolingOffE1" name="case[NeedCoolingOff]" value="Y"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffE1"> I need the cooling-off period. Only activate the account 14 working days thereafter.</label></td>
                            </tr>
                            <tr>
                                <td style="width:20px; border:none;" valign="middle"><input type="radio" class="eneedcoolingoff pull-left" id="WaiveCoolingOffE2" name="case[NeedCoolingOff]" value="N"></td>
                                <td style="border:none;"><label for="WaiveCoolingOffE2"> I opt to waive the cooling-off period. Activate my account immediatel</label></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
            </div>
            
            <!-- Section 16 CHECKED BY APPROVED CERTIFIER --> 
            <div class="section section-16">
                <div class="s-header">
                    <span><strong>16. 由适用签署人见证核实审查（合格审查人员专用） CHECKED BY APPROVED CERTIFIER</strong></span>
                </div>
                
                <div class="panel-body g-body" style="border-bottom: 1px solid #a3947d;">
    
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody>
                        <tr class="tr-td-withborder">
                            <td style="width: 50%; border-right:0;;">
                                <div class="row">
                                    <label class="col-sm-4">姓名 Name</label>
                                    <div class="col-sm-8">
                                        <input id="ApprovedBy" type="text" class="form-control" name="acct[ApprovedBy]">
                                    </div> 
                                </div>
                                <div class="clearfix"></div>
                            </td> 
                            <td>
                                <div class="row">
                                    <label class="col-sm-4">日期 Date</label>
                                    <div class="col-sm-8">
                                        <!--<input type="date" class="form-control" value="<?php echo date("d/m/Y");?>" readonly>-->
                                        <input type="text" class="form-control" name="acct[ApprovedDate]" value="<?php echo date("d-M-Y");?>" readonly>
                                    </div>  
                                </div>
                            </td>
                        </tr>
                        
                    </tbody></table>
                </div>
                
                <br>
                <div style="text-align: center;">
                    <p style="color:red;"><input style="margin-top:-3px;"type="checkbox" id="TermsAgree" name="TermsAgree" value="Y" required="required"> <label for="TermsAgree"><strong>我/我们阅读並理解上面的內容 | I/We Read and Understood the above.</strong></label> </p>
                    <button type="submit" class="btn">Submit | 提交</button>
                </div>
    
        </div>

        </div>
        </form>

    </div>
    <!-- /page content -->
<?php View::footer('forms'); ?>