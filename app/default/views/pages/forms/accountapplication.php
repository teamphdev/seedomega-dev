<?php 
View::$title = 'Odeon & Co. Application Form';
View::$bodyclass = 'guest';
View::header('forms'); 
?>
<style>
    .g-header td {text-indent: 15px;}
    .g-body ul li input[type=checkbox]{margin-top: 0px;}
    tr.tr-td-withborder td{border: 5px double #ddd;}
	.no-padding {
		padding: 0 !important;
	}
	.uboitem {
		display:block;
		margin-bottom:20px;
	}
	.acounttype_corporate { display:block; }
    </style>
<!-- page content -->
<div class="body-wrap">
    <div class="page-header">
        <div class="container">
            <h1>Odeon & Co. Application Form</h1>
        </div>
    </div>
    
    <form id="master_form" action="" method="post" class="form-horizontal" enctype="multipart/form-data">

        <input type="hidden" name="_token" value="<?php echo md5(microtime()); ?>">        
		<input type="hidden" name="action" value="addcasefile">
        <div class="table-responsive">
	        <?php echo View::getMessage(); ?> 
            <div>
                <div class="g-header">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 59%;">
                                <strong>01. General Guidance</strong>
                            </td>
                            <td>
                                <strong>概括指引</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p>Odeon Kirin Investment Trust is an investment deposit program offered by Odeon & Co. Pte Ltd ("Odeon") with Trust Agent Agent Zico Allshores Trust Pte Ltd ("Esrow Trustee") for the purpose of receiving and investing deposits to provide a return for Subscribers.</p>
                                <p>This Account Opening Package should only be completed by the applicant. This form should be read in conjunction with the Subscription Agreement and the Term Sheet. We seek your understanding and cooperation in furnishing the required documents and appreciate your time and effort in doing so.</p>
                                <p>It is important that you complete this Subscription form in full, and sign as required to enable us to consider your Subscription. Kindly attach necessary documents. Failure to provide all relevant information and supporting documentation will result in a delay to the Subscription being processed. Additional information may be included to support this subscription. Any additional details/notes/instructions or those provided at a non-designated area of the form may not be executed.</p>
                                <p><span style="color:red;">Note:</span> <strong>If the Chinese version and English version of the content is different, the English version shall prevail.</strong></p>
                            </td>
                            <td>
                                <p>您申购的投资信託存款計畫 《傲鼎麒麟投资信托》（Odeon Kirin Investment Trust），是由 Odeon & Co. Pte Ltd （简称"Odeon"）所提供與信託公司 Zico Allshores Trust 集團（简称"信托公司"）监督监管 ；以募集資金及投资存款，來提供申购者投資回報。</p>
                                <p>此申购者開戶程序應由申购者親自填寫。本公司的信託申购協議規範了投资信託的條款和條件，請與此申购書一起閱讀。感謝您的理解並希望您能配合本公司要求提供所需之文件。</p>
                                <p>請完整填妥此申购開戶書並按要求簽字，以方便本公司審核您的申請。如未能提供相關資料和證明文件將導致申购開戶有所延誤。附加資料應包括在內以作為本申請的佐證資料。在本申购開戶書非指定地方提供的任何額外資料、備註或指令將不會被執行。
注: 如本中文版本與英文版本內容有差異，將以英文版本為准。</p>
                                <p>注: 如本中文版本與英文版本內容有差異，將以英文版本為准。</p>
                            </td>
                            </tr>
                        </tbody>
                  	</table>
                </div>

                <div class="g-header">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 59%;">
                                <strong>02. Privacy Policy</strong>
                            </td>
                            <td>
                                <strong>私隐政策</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p>The Escrow Trustee and Odeon will not disclose information on the Subscriber and their affairs except as required under applicable laws, including the Anti-Money Laundering and Counter-Terrorist Financing (Financial Institutions) Ordinance ("AML Ordinance "). Odeon shall not disclose information on trust escrow deposits to any third party, except as required under applicable laws, including the AML Ordinance and the Inland Revenue Ordinance or otherwise as directed by a court order.</p>
                            </td>
                            <td>
                                <p>除非在《打擊洗錢及恐怖分子資金籌集(金融機構)條例》或《稅務條例》或法庭指令的要求下，否則Odeon不會披露成員任何資料; 在信託行政需要外，不論基於任何目的，Odeon一般不應透露關於信託存款的資料，也不會使用受益人的資料，所有有關信託申购者資料是受到保護的。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
                <div class="form-group alone">
                    <div class="col-md-12">
                        <label style="color: #ff0000;"><input type="checkbox" id="PrivacyPolicyConfirm" name="PrivacyPolicyConfirm" value="Y" required="required"> I/We Read and Understood the above.</label>
                    </div>
                </div>
                
                <div class="g-header">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <strong>03. Trust Account Type</strong>
                            </td>
                            <td>
                                <strong>信託帳戶類型</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom frm trustaccount">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <p><label><input type="radio" class="naccount" id="TrustAccountTypeIndividual" name="acct[AccountType]" value="Individual" checked="checked"> Individual 個人</label></p>
                            </td>
                            <td>
                                <p><label><input type="radio" class="naccount" id="TrustAccountTypeCorporate" name="acct[AccountType]" value="Corporate"> Corporate 機構</label></p>                                    
                            </td>
                        </tr>
                    </tbody></table>
                </div>
<!--    04  -->
                <div class="acounttype_corporate">
                    <div class="g-header">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tbody><tr>
                                <td style="width: 50%;">
                                    <strong>04. Corporate Client Information</strong>
                                </td>
                                <td>
                                    <strong>機構客戶資訊</strong>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="g-body margin-bottom">
                        <table border="0" cellpadding="3" cellspacing="3">
                            <tbody><tr>
                                <td style="width: 50%;">
                                    <p>
                                    	<label>Company Name 機構公司名稱</label>
                                    	<input type="text" id="CompanyName" name="acct[CompanyName]" class="form-control" value="">
                                    </p>
                                    <p>
                                    	<label>Registration No / Certificate of Incorporation No.<br> 機構公司註冊號碼</label>
                                    	<input type="text" id="RegistrationNo" name="acct[RegistrationNo]" class="form-control" value="">
                                  	</p>
                                </td>
                                <td>
                                    <p>
                                    	<label>Registered Address 註冊地址</label>
	                                    <textarea style="height:140px;" id="RegistrationAddress" name="acct[RegistrationAddress]" class="form-control"></textarea>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    <p>
                                    	<label>Country of Registration / Incorporation<br> 機構公司註冊國家</label>
	                                    <select id="CRINC" name="acct[RegistrationCountry]" class="form-control">
                                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                            <?php } ?>
                                        </select>
                                    </p>
                                    <p>
                                    	<label>Telephone 電話號碼</label>
                                    	<input type="text" id="CRINCTelephone" name="acct[RegistrationTelephone]" class="form-control" value="">
                                    </p>
                                </td>
                                <td>
                                    <p>
                                    	<label>Principal Business Address (If different from Registered Address)<br> 主要經營地址（如於註冊地址不同）</label>
	                                    <textarea style="height:120px;" id="PBAddress" name="acct[BusinessAddress]" class="form-control" value=""></textarea>
                                   	</p>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
               	</div>
<!--    05  -->                  
                <div class="g-header acounttype_corporate">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <strong>05. Director/Secretary Information</strong>
                            </td>
                            <td>
                                <strong>申請者的個人資料</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-header acounttype_individual">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <strong>05. Subscriber Personal Details</strong>
                            </td>
                            <td>
                                <strong>申請者的個人資料</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <label>Title | 稱呼 *</label>
                                <?php 
									View::form(
										'select',
										array(
											'name'=>'meta[Salutation]',
											'options'=>AppUtility::getSalutation(),
											'value'=>'Mr',
											'class'=>'form-control',
											'id'=>'Salutation'
										)
									); 
								?>
                            </td>
                            <td>
                                <label>Passport / Photo ID Number | 護照／附相片身份證號碼 *</label>
                                <input type="text" id="IdNumber" name="meta[IdNumber]" class="form-control" value="" required="required">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>First Name | 名字 *</label>
                                <input type="text" id="FirstName" name="meta[FirstName]" class="form-control" value="" required="required">
                            </td>
                            <td>
                                <label>Country of Citizenship | 國籍 *</label>
                                <select id="Country" name="meta[Country]" class="form-control">
									<?php foreach(AppUtility::getCountries() as $country) { ?>
                                    <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                    <?php } ?>
                                </select>                                 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Last Name | 姓氏 *</label>
                                <input type="text" id="LastName" name="meta[LastName]" class="form-control" value="" required="required">
                            </td>
                            <td>
                                <label>Country of Tax Residency | 稅務居住國 *</label>
                                <select id="Country" name="acct[BusinessCountry]" class="form-control">
									<?php foreach(AppUtility::getCountries() as $country) { ?>
                                    <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Date of Birth | 出生日期 *</label>
                                <div class="col-md-3 col-sm-3 col-xs-12 no-padding">
								<?php 
                                    View::form(
                                        'select',
                                        array(
                                            'name'=>'dob[Month]',
                                            'options'=>AppUtility::getMonth(),
                                            'value'=>'',
                                            'class'=>'form-control',
                                            'id'=>'Month',
                                            'placeholder'=>'Month',
                                            'custom'=>'required'
                                        )
                                    ); 
                                ?>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                <?php 
                                    View::form(
                                        'select',
                                        array(
                                            'name'=>'dob[Day]',
                                            'options'=>AppUtility::getDay(),
                                            'value'=>'',
                                            'class'=>'form-control',
                                            'id'=>'Day',
                                            'placeholder'=>'Day',
                                            'custom'=>'required'
                                        )
                                    ); 
                                ?>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                                <?php 
                                    View::form(
                                        'select',
                                        array(
                                            'name'=>'dob[Year]',
                                            'options'=>AppUtility::getYear(),
                                            'value'=>'',
                                            'class'=>'form-control',
                                            'id'=>'Year',
                                            'placeholder'=>'Year',
                                            'custom'=>'required'
                                        )
                                    ); 
                                ?>
                                </div>
                            </td>
                            <td>
                                <label>Marital Status | 婚姻狀況 *</label>
                                <?php 
									View::form(
										'select',
										array(
											'name'=>'dob[CivilStatus]',
											'options'=>AppUtility::getCivilStatus(),
											'value'=>'SINGLE',
											'class'=>'form-control',
											'id'=>'CivilStatus',
											'custom'=>'required'
										)
									); 
								?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Gender | 性别 *</label>
                                <select id="Gender" name="meta[Gender]" class="form-control" required="required">
                                        <option value="M">Male | 男</option>
                                        <option value="F">Female | 女</option>
                                </select>
                            </td>
                            <td>
                                <label>
                                    Occupation &amp; Job Title (if retired, state retired and former occupation) *<br>
                                    職業及職位 (如退休，請註明退休，並退休前的職位) *
                                </label>
                                <input type="text" id="Occupation" name="meta[Occupation]" class="form-control" placeholder="Occupation | 職業" value="" required="required">
                                <input type="text" id="JobTitle" name="meta[JobTitle]" class="form-control" placeholder="Job Title | 職位" value="" required="required">
                            </td>
                        </tr>
                    </tbody></table>
                </div>
<!--    06  -->               
                <div class="g-header acounttype_corporate">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <strong>06. Corporate Contact Details</strong>
                            </td>
                            <td>
                                <strong>申請者的個人資料</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-header acounttype_individual">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <strong>06. Subscriber Contact Details</strong>
                            </td>
                            <td>
                                <strong>申請者的個人資料</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <label>Preferred Language | 首選語言 *</label>
                                <select id="Language" name="meta[Language]" class="form-control" required="required">
                                        <option value="en">English | 英語</option>
                                        <option value="cn">Chinese | 中文</option>
                                </select>
                            </td>
                            <td>
                                <label>Home Telephone | 住宅電話</label>
                                <input type="text" id="Telephone" name="meta[Phone]" class="form-control" value="">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Mobile | 手機 *</label>
                                <input type="text" id="Mobile" name="meta[Mobile]" class="form-control" value="" required="required">
                            </td>
                            <td>
                                <label>Email | 電郵 *</label>
                                <input type="email" id="EmailChecker" name="user[Email]" class="form-control" value="" data-validate-linked="email" required="required" rel="<?php echo View::url('checkemail'); ?>" checkmessage="Email already exists please enter a unique one or contact the administrator. 电子邮件已存在，请输入唯一的或联系管理员。"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                
                                <span class="help-block">The primary form of correspondence will be via email. | 將以電子郵件作為與Odeon基本的聯繫方式</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Primary Residential Address | 居住地址 *</label>
                                <input type="text" id="Address" name="meta[Address]" class="form-control" value="" required="required">
                                <input type="text" id="Address2" name="meta[Address2]" class="form-control" value="">
                            </td>
                            <td>
                                <label>Correspondence Address | 通訊位址</label>
                                <input type="text" id="CorrespondenceAddress" name="meta[Address3]" class="form-control" value="">
                                <input type="text" id="CorrespondenceAddress2" name="meta[Address4]" class="form-control" value="">
                                <span class="help-block">If different from residential address | 通訊位址（如與居住地址不同）</span>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
<!--    07  -->
                <div class="acounttype_corporate">
                    <div class="g-header">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tbody><tr>
                                <td style="width: 59%;">
                                    <strong>07. Investment Trust</strong>
                                </td>
                                <td>
                                    <strong>投資信託</strong>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="g-body margin-bottom numbersix">
                        <table border="0" cellpadding="3" cellspacing="3">
                            <tbody><tr>
                                <td style="width: 60%;">
                                    <p>Investment Trust is linked to various investment products which may include a wide array of investments received in Escrow Trust for your account. Please see the Term Sheet and Subscription included with this Account Opening Package. Each of the Investment Trust is designed to follow a different investment strategy. Investment Trust carry investment risks. Unless stated otherwise, the returns are not guaranteed. Do note that the past returns are not necessarily indicative of the future performance. </p>
                                    <p>Disclaimers specific to Investment Trust are written in each Term Sheet. You acknowledge there is always risk in investments.</p>
                                    <ol>
                                        <li>There is always risk in investments; and</li>
                                        <li>You will indemnify the Trustee from any losses</li>
                                    </ol>
                                </td>
                                <td>
                                    <p>投資信託與多種投資產品相連結，使您的投資信託帳戶具有多元化的投資種類。請詳細閱讀開戶程序的條款書和申购書，投資信託存有投資風險；除非书面声明，此投资沒有保證回報。過往的投資回報並不一定反映未來的表現。</p>
                                    <p>在每個條款清單上已有免責聲明特別提，您需要接受相對的投資風險，并要瞭解所有投資都是有風險的。</p>
                                    <ol>
                                        <li>投資都是有風險的。</li>
                                        <li> 如有任何投資損失，您不能向受託人索取任何賠償。</li>
                                    </ol>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                </div> 
<!--    08  -->
                <div class="g-header">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 59%;">
                                <strong>08. Declaration to the Trust</strong>
                            </td>
                            <td>
                                <strong>申請加入信託聲</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p>I, being the above named individual and having full legal authority to make this Subscription for membership in the investment trust program offered by Odeon, certify that:</p>
                                <ol style="list-style-type: lower-alpha;">
                                    <li>I have no legal disability of any description to make this subscription;</li>
                                    <li>I have complied with all applicable laws in making this subscription;</li>
                                    <li>All information given by me and contained in this Subscription Form is complete, true and correct.</li>
                                </ol>
                                <p>I have read and understood the Subscription Agreement; and</p>
                                <ol style="list-style-type: lower-alpha;" start="4">
                                    <li>I have taken note of and initialed to signify my awareness and understanding of the fees payable by Subscriber, diversification waiver, investment risk waiver and disclaimers; and</li>
                                    <li>hereby make subscription as a Subscriber of the investment trust program.</li>
                                </ol>
                                <p>I/we further declare that I am / We are a sophisticated, professional or accredited investor as such terms are defined, or the equivalent thereof, in my home jurisdiction to qualify and exempt my purchase, contribution, or participation in this investment program from any applicable information memorandum, registration and notice requirements for the distribution, sale, or marketing of securities that may apply to this investment trust program, and I have obtained or sought the appropriate advice from financial and legal professionals to confirm this statement, thus waiving my rights to claim otherwise in any dispute or proceeding in law, court or hearing.</p>
                            </td>
                            <td>
                                <p>本人,作為上述指定的個人，已擁有完整的法律權力，申請成為投资信託的申购者，並證明:</p>
                                <ol style="list-style-type: lower-alpha;">
                                    <li>本人在沒有喪失法律行為能力的情況下提出此申請;</li>
                                    <li>本人已遵守所有適用法律去提出是次申請;</li>
                                    <li>本人在此開戶程序中提供的所有資訊均是完整，真實且正確無誤的。</li>
                                </ol>
                                <p>本人已閱讀、瞭解並同意附加的申购者協議的條款;</p>
                                <ol style="list-style-type: lower-alpha;" start="4">
                                    <li>作出草簽，以表示本人察覺與瞭解信託申购者會籍、放棄分散投資、免投資風險和各項免責聲明所帶來的費用;</li>
                                    <li>現申請作為此信託的申购者。</li>
                                </ol>
                                <p>我/我們進一步聲明,本人/我們是一個多元的、專業的、經認可的或是等同上述說明的资深投資者、在我們司法管轄所在地任何適用的說明書及小册子中得以豁免顯示本人/我們的購買、供款、登記和通知要求。關於該投資信托計畫的發行、銷售或營銷等相關資訊、本人/我們已經獲得或尋求金融和法律專業人士的適當建議來確認此一說法、 任何因此而產生的糾紛或爭議、本人/我們在此聲明放棄任何訴訟或司法聽證會之權利。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
                <div class="form-group alone">
                    <div class="col-md-12">
                        <label style="color: #ff0000;"><input type="checkbox" id="DeclarationToTheTrust" name="DeclarationToTheTrust" value="Y" required="required"> I/We Read and Understood the above.</label>
                    </div>
                </div>
<!--    09      -->
                <?php /*?><div class="g-header">
                    <strong>09. Subscriber's Nominated Beneficiaries | 提名受益人</strong>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 100%;">
                                <p>除非您有指定的信託受益人,您的遺產將在您逝世後屬信託的受益人所有。<br>
                                Upon your demise, your estate shall be the Beneficiary of this agreement unless you have nominated another person as a beneficiary or beneficiaries of this agreement.</p>
                                <p>
                                	<label>
                                    	<input type="checkbox" id="HasNominatedBeneficiaries" name="case[HasNominatedBeneficiaries]" value="Y" class="uboswitch" rel="#BeneficiariesForm"> 我想進一步提名受益人並附上一份提名受益人表格。
                                  	</label>
	                                I wish to nominate further beneficiaries and I include a Nominated Beneficiaries Form.
                              	</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div><?php */?>
<!--    10      -->
                <?php /*?><div id="BeneficiariesForm" class="ubofiles">
                    <div class="g-header">
                        <strong>10. 提 名 受 益 人 表 格 | Nominated Beneficiaries Form</strong>
                    </div>
                    <div class="g-body margin-bottom numberten">
                        <table cellspacing="3" cellpadding="3" border="0">
                            <tbody>
                                <tr>
                                    <td class="ubo-sect" style="width: 100%;">
                                        <p>Upon your demise all remaining assets held in trust will be held in favour for your nominated beneficiary or beneficiaries. If you have not nominated beneficiaries for the Trust consideration the remaining trust assets will be held in favour of your estate.</p>
                                        <ol>
                                            <li>If a designated beneficiary does not survive the other beneficiaries their share maybe distributed in proportion to the remaining beneficiaries share of benefits.</li>
                                            <li>If no beneficiaries are nominated or if none of the beneficiaries survive the Subscriber, the total amount shall normally be paid to the Subscriber's estate.</li>
                                            <li>Should you wish to change beneficiaries or include further beneficiaries please provide details to the Odeon and the Escrow Trustee.</li>
                                        </ol>
                                        <p>在您離世後，您所有遺留的信託資產將會由您提名的受益人繼承。如果您未有提名受益人，信託公司將會讓您的遺產繼承者繼承您遺留的信託資產。</p>
                                        <ol>
                                            <li>如果您所指定的受益人已經離世，其他受益人可能會按比例獲得遺留信託資產的利益。</li>
                                            <li>如果您沒有任何提名受益人或所有提名受益人都離世，所有資產應該由您的遺產繼承者來繼承。</li>
                                            <li>如果您想要更改受益人或添加更多受益人，請向信托公司提供詳細資料的書面意願書。</li>
                                        </ol>
                                        <p>Click browse to upload <br> 拖放文件或点击此处上传文件</p><br />
                                        <input id="file-0a" class="file" type="file" data-min-file-count="1" name="BeneficiariesForm[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div><?php */?>
<!--    11      -->
                <div class="g-header">
                    <strong>11. UBO Declaration | 最 終 受 益 方 聲 明 書</strong>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 100%;" class="ubo-sect">
                                <p>I, <input type="text" placeholder="(full name as in passport 全名，如護照)" name="case[UBOName]" /> of <input type="text" name="case[UBOAddress]" placeholder="(full residential address 詳細住址)" /></p>
                                <p>do hereby declare that my/our source(s) of funds which I/we have are my/our assets derived from:<br> 我，居住於以上填寫住址，特此聲明我(我們)的資金及我(我們)的資產來源於:</p>
                                <section>
                                    <h5>Please tick the source of funds 請提供資產來源</h5>
                                    <div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                        <input type="checkbox" class="uboswitch" value="Y" rel="#UBOEmploymentIncome">
                                        Employment Income (employee, title, period, salary figures)<br> 就業收入（用人單位，職務，期限，工資數額，履歷表/簡歷）
                                        <div id="UBOEmploymentIncome" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                            <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOEmploymentIncome[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
									</div> 
                                                                    
									<div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                        <input type="checkbox" class="uboswitch" value="Y" rel="#UBOCommission">
                                        Sales/Commision Income (details of company(ies) and value figures)<br> 銷售/佣金收入（公司的詳細情況及相關資料）
                                        <div id="UBOCommission" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                            <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOCommission[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
									</div> 
                                                                  
									<div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                    	<input type="checkbox" class="uboswitch" value="Y" rel="#UBOBusiness"> 
                                        Business Income (business particulars and financial statements to determine general turnover and profits per respective years)<br> 營業收入（營業相關資料與財務報表，確認每個相應年度的營業收入）
                                        <div id="UBOBusiness" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                            <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOBusiness[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div> 
                                                                   
									<div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                    	<input type="checkbox" class="uboswitch" value="Y" rel="#UBOInheritance"> 
                                        Inheritance (inheritance, lineage and value)<br> 繼承（繼承血統及其數值）
                                        <div id="UBOInheritance" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                            <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOInheritance[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div> 
                                                                     
									<div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                    	<input type="checkbox" class="uboswitch" value="Y" rel="#UBOGift">                                             
                                        Gift (the details of giver and details of gift and its value)<br> 贈與物（贈與者及贈與物之細節及其價值）
                                        <div id="UBOGift" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                            <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOGift[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div> 
                                                                    
									<div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                    	<input type="checkbox" class="uboswitch" value="Y" rel="#UBOSales"> 
                                        Proceeds from the sale of property and/or other assets (description of assets value and how they were obtained in the first instance)<br> 出售物業收益或其他資產<br> (資產價值的描述，以及如何在第一時間取得)
                                        <div id="UBOSales" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                        	<input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOSales[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
										</div>
                                    </div> 
                                                                     
									<div class="uboitem col-md-12 col-sm-12 col-xs-12">
                                    	<input type="checkbox" class="uboswitch" value="Y" rel="#UBOOther">                                             
                                        Any other (please describe) 其他補充事項 (請進行描述)
                                        <div id="UBOOther" class="ubofiles">
                                        	Click browse to upload <br> 拖放文件或点击此处上传文件<br />
                                            <input id="file-0a" class="file" type="file" data-min-file-count="1" name="UBOOther[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple>
                                        </div>
                                    </div>
                                </section>
                                <p><label><input type="checkbox" id="UBODeclarationConfirm" name="UBODeclarationConfirm" value="Y" required="required"> I further declare that : 我進一步聲明 :</label></p>
                                <ul>
                                    <li>I am not involved in money laundering and/or drug trafficking; and<br> 我沒有參與洗黑錢或販賣毒品;以及</li>
                                    <li>any monies and/or securities that have been or will be deposited in any company bank account in the name of the company do not originate, either directly or indirectly, from illegal and/or criminal activities.<br> 已經存入或將要存入的公司銀行帳戶、公司名稱、及其任何款項或證券，並無直接地或間接地參與非法或犯罪活動。</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
<!--    12      -->
                <div class="g-header">
                    <strong>12. PEP Declaration and Tax Status | PEP宣言和稅務狀態</strong>
                </div>
                <div class="g-body margin-bottom nutwelve">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 100%;">
                                <p>The above information is true, complete and accurate; and further undertake to notify <label style="color:red">Odeon and the Escrow Trustee</label> promptly of any change in the aforementioned information and agree to submit additional documents to the <label style="color:red">Escrow Trustee</label> upon its request.<br>
以上所有資料屬實、完整及正確無誤，<label style="color:red">並且進一步承諾即時通知Odeon及信托公司此資料的任何變化及同意提交申购者所需要的額外文件。</label></p>
                                <p>I hereby declare that : 我在此聲明：</p>
                                <ul>
                                    <li>I am not a PEP2 or a Closely Connection Person to a PEP3.<br> 我不是一個PEP2或密切的人連接到PEP3</li>
                                    <li>I am not a U.S. Person and do not intent to be one.<br> 我不是一個人美國並沒有意圖是一個。</li>
                                    <li>My tax residency and tax identification number are:<br> 我的稅居住和納稅識別號有：</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <p><strong>Primary Tax Residency 主要居住稅</strong></p>
                                <textarea style="height:120px;" name="case[PrimaryTaxResidency]" class="form-control"></textarea>
                            </td>
                            <td>
                                <p>Tax ID No. 稅務編號。</p>
                                <textarea style="height:120px;" name="case[TaxIdNumber]" class="form-control"></textarea>
                            </td>
                        </tr>
                    </tbody></table>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 100%;">
                                <ul>
                                    <li>The above information is true, complete and accurate; and further undertake to notify the Sponsor and the Trustee promptly of any change in the aforementioned information and agree to submit additional documents to the Trustee upon its request.<br>  上述信息是真實，完整和準確;並進一步承諾及時通知在上述資料有任何更改的保薦人及受託人，並同意根據其要求提交其他文件受託人。</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <ul>
                                    <li> <small>1</small>Individuals: U.S. citizen or lawful permanent resident (green card holder); or born in the U.S.; or have a U.S. residential address or meeting the substancial presence test for the calendar year as defined by IRS (www.irs.gov).</li>
                                    <li>Corporations: a U.S. partnership or U.S. corporation</li>
                                    <li>Trusts: Any trust if (a) a court within U.S. is able to exercise primary supervision over the administration of the trust; and (b) one or more U.S. persons have the authority to control all substantial decisions of the trust.</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><small>1</small>個人：美國公民或合法永久居民（綠卡持有者） ;或出生在中美;或有一個美國住址或滿足歷年具有牢固存在測試通過IRS （ www.irs.gov ）定義。</li>
                                    <li>企業：美國合作夥伴或美國公司</li>
                                    <li>信託：如果任何（ a）在美國法院能夠行使信託的管理主要監督信任;和（b）一個或多個美國的人要控制信託進行的所有實質性的決定權。</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <p><small>2</small> Examples of Politically Exposed Persons include: </p>
                                <ul>
                                    <li>Heads of State or Government</li>
                                    <li>Senior Politicians</li>
                                    <li>Senior Government, Judicial or Military Officials</li>
                                    <li>Senior Executives of State-Owned Corporations</li>
                                    <li>Important Political Party Officials</li>
                                    <li>Royal Family</li>
                                </ul>
                                <p><small>3</small> Examples of Closely Connected Persons include: </p>
                                <ul>
                                    <li>Family members (spouse, children, parents, brothers and sisters, including in-laws) </li>
                                    <li>Aides and Other Close Advisors, socially or professionally</li>
                                    <li>Business associates</li>
                                    <li>Companies in which a Senior Political Figure has an interest or exercises influence </li>
                                </ul>
                            </td>
                            <td>
                                <p><small>3</small> 密切關連人士的例子包括：</p>
                                <ul>
                                    <Li>家庭成員（配偶，子女，父母，兄弟姐妹，包括姻親）</li>
                                    <li>助手和其他關閉顧問，社會或專業</li>
                                    <li>商業夥伴</li>
                                    <li>公司所處高級政工圖擁有權益或練習影響</li>
                                </ul>
                                <p><small>2</small> 政治人物的例子包括：</p>
                                <ul>
                                    <li> 國家元首或政府首腦</li>
                                    <li> 資深政治家</li>
                                    <li> 高級政府，司法或軍事官員</li>
                                    <li> 國有企業的高級管理人員</li>
                                    <li> 重要的政黨官員</li>
                                    <li> 王室</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                </div>
<!--    13      -->
                <div class="acounttype_corporate">
                    <div class="g-header">
                        <strong>13. Power of Attorney | 授權書</strong>
                    </div>
                    <div class="g-body margin-bottom">
                        <table cellspacing="3" cellpadding="3" border="0">
                            <tbody>
                                <tr>
                                    <td class="ubo-sect" style="width: 100%;">
                                    <p>Click browse to upload <br> 拖放文件或点击此处上传文件</p><br />
                                    <input id="file-0a" class="file" type="file" data-min-file-count="1" name="PowerOfAttorney[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
              	</div>                
                <div class="form-group alone">
                    <div class="col-md-12">
                        <label style="color: #ff0000;"><input type="checkbox" id="PowerOfAttorneyConfirm" name="PowerOfAttorneyConfirm" value="Y" required="required"> I/We Read and Understood the above.</label>
                    </div>
                </div>            	
<!--    14a      -->
                <div class="g-header">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 59%;">
                                <strong>Managed Account Agreement</strong>
                            </td>
                            <td>
                                <strong>託管協議</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="g-body margin-bottom" style="margin-bottom:0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p><strong>14a. General Interpretation</strong></p>
                                <p>In this Subscription Agreement and the entire Account Opening Package the following terms shall have the following meanings:</p>
                                <p>a. "Account Opening Package" means this entire document, including the Subscription Agreement, and any supplementary or accompanying forms provided and filed by the Subscriber;</p>

                                <p>b. "Fees" means the amounts payable by a Subscriber pursuant to attached Fee Schedule, or any other fees that may be charged, which are subject to change from time to time;</p>

                                <p>c. "Management Company" is Odeon & Co Pte Ltd (hereby also known as Odeon)</p>

                                <p>d. "Escrow Trustee" is Zico Allshores Trust (Singapore) Pte Ltd</p>

                                <p>e. "Subscriber" is a person who signs this Subscription Form and provides funds to Odeon to become a Subscriber of the investment trust program. Under this program, Odeon will transfer funds received from Subscribers. Any person who becomes a Subscriber must fulfil the conditions of Subscription as set out above in order to be available for settlements at Odeon's discretion;</p>

                                <p>f. "Approved Certifier" is a natural person of greater than the age of majority in their home jurisdiction, who has completed an Anti-Money Laundering ("AML") and public records check or other background reviews at the discretion of Odeon, prior to having been approved by Odeon to certify documents for reliance upon the AML review by the Escrow Trustee and Odeon;</p>

                                <p>g. "Trust Deed" means the Escrow Trust Deed [and Declaration of Trust respectively] executed in the year of 2016 and any amendments by the Escrow Trustee, executed copies of which are maintained at the offices of the Escrow Trustee.</p>

                                <p>Unless the context otherwise requires and except as varied or otherwise specified in this Subscription Agreement, words and expressions contained in this Subscription Agreement shall bear the same meanings as in the Escrow Trust Deed:</p>
                                    <ul>
                                        <li>the singular includes the plural and vice versa;</li>
                                        <li>headings shall be ignored in the construction of this Subscription Agreement;</li>
                                        <li>references to persons shall include body corporates, unincorporated associations and partnerships; and</li>
                                        <li>references to any documents or agreements, laws or regulations, are to be construed as references to such documents or agreements, laws or regulations, as is in force for the time being and as amended, varied, supplemented, substituted or novated from time to time.</li>
                                    </ul>
                                    <p>In the event that any of the clauses, terms and conditions of this Subscription Agreement and/or any of the subsidiary agreements either now or at a future date, should be held to be without force and effect or be otherwise invalid, the rest of the Subscription Agreement and any and all subsidiary agreements shall remain in effects.</p>

                                    <p>This entire Account Opening Package is construed and shall be governed by and in accordance with the laws of Singapore.</p>
                            </td>
                            <td>
                                <p><strong>基本釋義</strong></p>
                                <p>在這申购協議和整個開戶程序中，下列詞彙具有以下涵義:</p>
                                <p>a. "開戶程序"指全套的文件，包括申购協議，及申购者提交併入檔的任何補充文件或隨附表格;</p>
                                <p>b. "費用"指信託申购者根據內附的標準需要交付的各項費用，或隨信託計畫不定期的變更而可能產生的其他費用;</p>
                                <p>c. "託管公司"是 Odeon & Co Pte Ltd (简称"Odeon")</p>
                                <p>d. "信託公司"是 Zico Allshores Trust (Singapore) Pte Ltd</p>
                                <p>e. "申购者"指簽署信託之申购者，並獲得信託公司和託管公司Odeon接受。在信託公司按情況制定的合理時間內， 申购者必須遵守上述的信託條件， 方可獲得信託公司結算其投資回報;</p>
                                <p>f. "授權核證人"指一個在其所屬司法轄區大於法定年齡的人，在得託管公司允許其負責核證文件之前，須先由信託公司和託管公司對其進行反洗錢 ("AML") 和公共記錄核實或其他背景調查。</p>
                                <p>g. "信託契約"指在2016年已分別執行的 <信託契約> 和 <信託聲明> 和信託公司有任何修訂後存放於管理人及信託公司辦事處的已執行副本。</p>

                                <p>除非文義另有所指或是此申购協議內有更改或另有列明，載于此申购者協定內的文字應與信託契約內的文字具有相同含義:</p>
                                    <!--ul>
                                        <li>i. 單數字詞包含複數字詞，反之亦然，和;</li>
                                        <li>在此申购協議中標題可被忽略。</li>
                                        <li>所述及的人應包括法人團體 ， 非法人社團和合夥公司。</li>
                                        <li>凡述及的任何文件、協議、法律或法規， 都應該理解為目前已生效、經過不定期修訂、變更、補充、替代或以新代舊。</li>
                                    </ul>
                                    <p>這申购協議的任何條款和細則和/或任何附屬協議， 在現在或將來某天， 如若可能失去效力或變成無效，其申购協議和任何所有附屬協定的其餘部分應仍然生效。</p>
                                    <p>申請程式以新加坡法律為解釋依據，並受香港法律管轄。</p-->
                                    <ul>
                                        <li>單數字詞包含複數字詞，反之亦然，和;</li>
                                        <li>在此申购協議中標題可被忽略。</li>
                                        <li>所述及的人應包括法人團體 ， 非法人社團和合夥公司。</li>
                                        <li>凡述及的任何文件、協議、法律或法規， 都應該理解為目前已生效、經過不定期修訂、變更、補充、替代或以新代舊。</li>
                                    </ul>
                                    <p>申請程式以新加坡法律為解釋依據，並受<label style="color:red;">香港</label>新加坡法律管轄。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>  
<!--    14b      -->
                <div class="g-body margin-bottom" style="margin-bottom:0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 59%;">
                                <p><strong>14b. Subscriber's Declaration to Odeon</strong></p>
                                <p>I， the undersigned person, hereby being eligible to apply for Subscription of the trust, agree and undertake as follows:</p>
                                <p>a. I declare that to the extent that I have completed the Account Opening Package, the information contained is accurate in every respect.</p>

                                <p>b. I declare that at the time I was given this Account Opening Package, I was also given advice about the benefits to which I would be entitled as a Subscriber, the method of determining that entitlement and the conditions relating to those benefits.</p>

                                <p>c. I confirm to Odeon, that I have been advised by Odeon to take independent financial, legal and taxation advice on the investment trust program offered by Odeon, and that I have made such enquiries and taken such financial, legal, taxation and other advice as I consider necessary concerning all possible implications concerning the proposed program.</p>

                                <p>d. I acknowledge that the Escrow Trustee has not given me any tax advice concerning the proposed investment trust program on my circumstances or on the circumstances of any other person likely to be affiliated with or benefiting from the plan.</p>
                            </td>
                            <td>
                                <p><strong>信託申购者向託管公司聲明</strong></p>
                                <p>本開戶書簽字人，已符合資格申請加入成為其信託申购者，現同意並做出如下承諾:</p>
                                <p>a. 本人聲明，在開戶程序填妥的內容裡，本人提供的資訊在各方面都準確無訛。</p>
                                <p>b. 本人聲明在收到此開戶程序時，本人已得到相關建議，內容包括加入信託後本人將得到的利益，決定這些利益的方法，以及這些利益的相關條款。</p>
                                <p>c. 本人茲向託管公司確認，已經知悉託管公司建議，並就其所推薦的投资信託計劃，獨立諮詢與之相關的金融、法律和稅務意見，而本人亦就以上本人認為需要知道的其他關於該投资信託計劃的可能含義和託管公司的託管情況作出詢問。</p>
                                <p>d. 本人瞭解就所推薦的投资信託計劃或該計劃對於本人的影響，或該計劃可能受惠於其他人的可能性，信託公司尚未向本人提供與之相關的稅務意見。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>    
<!--    14c      -->
                <div class="g-body margin-bottom" style="margin-bottom:0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p><strong>14c. Subscriber's Code of Conduct</strong></p>
                                <p>a. Any decision by Odeon pertaining to the operation of or administration of the investment trust program is final and the parties all hereby agree that policies, instructions, directions or orders made by Odeon must be followed rigorously and at all times.</p>

                                </p>b. Should any party wish to vary the policies, instructions, directions or orders made by Odeon, they may request such variance in writing and the approval of such variance shall be at the sole discretion of Odeon.</p>
                            </td>
                            <td>
                                <p><strong>託管公司守則</strong></p>
                                <p>a. 有關投资信託基金的操作或管理，託管公司的任何決定均是最終決定，各方均同意由信託公司制定的政策、指引、指令或命令，均必須貫徹始終並嚴格執行。</p>

                                </p>b. 如果任何一方希望改變託管公司制定的政策、指引、指令或命令，可以書面提出要求， 而託管公司可以全權決定是否准許。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>       
<!--    14d      -->
                <div class="g-body margin-bottom" style="margin-bottom:0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p><strong>14d. Diversification Waiver</strong></p>
                                <p>I have endorsed the Escrow Trustees' election to participate in underlying investments or a longer term investment strategy as proposed by Odeon and adopted by the Escrow Trustee. I therefore waive the need for diversification of investments by Odeon.</p>

                                <p>I accept that when Odeon invests funds I have provided:</p>

                                <p>a. no claim will be made by me against Odeon or Escrow Trustee for any advice, representations, acts, omissions, or conduct of Odeon or Escrow Trustee.</p>
                            </td>
                            <td>
                                <p><strong>放棄分散投資</strong></p>
                                <p>本人同意選擇參與由信託公司建議並得到託管公司批准的相關投資或長期策略投資。因此， 本人放棄了由託管公司管理的分散投資需要。</p>
                                <p>本人接受，當託管公司按本人要求作出投資時:</p>
                                <p>a. 若因信託公司和託管公司的任何建議、代表、舉動、疏忽或行為而造成損失時， 本人不會向信託公司和託管公司索取賠償。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>   
<!--    14f      -->
                <div class="g-body margin-bottom" style="margin-bottom:0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p><strong>14f. Investments</strong></p>
                                <p>I acknowledge that I am aware of the inherent risks that market values can go down as well as up; and that I shall make no claim on Odeon or Escrow Trustee for carrying out my requests. Odeon has not provided me financial advice and has made no warranty or representation in relation to any investment. Where I choose or have chosen an investment, including but not limited to specialist investment, I declare that:</p>
                                <ul>
                                    <li>a. l am sufficiently experienced to understand the features and risks associated with the investment; and;</li>
                                    <li>b. I have read and fully understood the offering document, including in particular the information on the risks and charges associated with investing in the investments contained in the offering document before deciding to invest into it and;</li>
                                    <li>c. I personally accept all risk associated with the investment, and in particular that my investment in a specialist investment involves risk that could in loss of a significant proportion or all of the sum involved; and;</li>
                                    <li>d. I have taken independent advice on the suitability of the investments within this structure; and;</li>
                                    <li>e. The Escrow Trustee has not promoted the investment or provided any advice, made any recommendation or expressed any opinion whatsoever in respect of the performance risk， regulatory issues or security (including any express or implied guarantees) of the investment.</li>li>
                                    <li>f. I agree to indemnify and keep the Escrow Trustee indemnified from and against all claims, cost demands, liabilities, expenses, damages or losses (including without limitation any consequential losses/loss of profit and loss of reputation, and all interest penalties and legal and other professional costs) arising out of or in connection with the choice of investment.</li>
                                </ul>
                            </td>
                            <td>
                                <p><strong>投資</strong></p>
                                <p>本人明瞭在本人提出要求信託公司指示財務顧問作出特定投資時，本人知悉存在固有風險，市場價值有漲有跌，本人不應為信託公司和託管公司執行了本人的要求而向其索償。託管公司並沒有向本人提供財務建議， 亦未有就有關的任何投資作出擔保。就本人已選擇的投資(包括但 不限於專業投資)，本人聲明:</p>
                                <ul>
                                    <li>a. 本人有足夠經驗瞭解與該投資相關的特性和風險;</li>
                                    <li>b. 本人在決定投資該項目前， 已閱讀並充分理解該銷售檔， 尤其當中包括銷售檔內關於投資各項目的風險和費用的資訊;</li>
                                    <li>c. 本人接受有關是項投資的所有風險，尤其本人在專業投資所涉及的風險，有可能導致部分比例或全部投資的虧損;</li>
                                    <li>d. 就本投資結構內的各項投資專案於個人是否合適，本人已聽取過獨立意見;</li>
                                    <li>e. 信託公司並沒有進行促銷或提供任何建議， 亦沒有就投資的績效風險、投資的監管問題或安全性(包括任何明示或暗示的保證)表示任何意見。</li>
                                    <li>f. 本人同意賠償信託公司因投資選擇問題引起或與其相關的索償、成本支出、債務、費用、損害或損失 (包括但不限於任何因此而受影響的損失/利潤損失， 名譽損失， 全部利息罰款， 法律及其他專業收費) 。</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                </div> 
<!--    14g      -->
                <div class="g-body margin-bottom" style="margin-bottom:0px;">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p><strong>14g. Standard Fee Schedule</strong></p>
                                <p>By completing this Subscription, I agree to pay the one-time account set-up fee of USD535 (Individual Subscriber) / USD842 (Corporate Subscriber) ("Set-up Fee ") and the first annual administration fee of 2% of all my Assets Under Management (AUM) ("Management Fee"). I agree to pay subsequent annual Administration Fees. All fees are payable in advance and non-refundable.</p>
                                <p>The Escrow Trustee and Odeon reserve the right to recover disbursements and out-of-pocket expenses, such as third party charges, direct from the plan. Additional third party charges may be incurred in administering the structure, in particular where the structure is complex or unique to the Subscriber's requirements. Charges will be discussed on a case-by-case basis and such charges will normally be notified and agreed in advance.</p>
                            </td>
                            <td>
                                <p><strong>標準規定收費</strong></p>
                                <p>通過填妥本申請表，本人同意支付一次性信託帳號開戶費美元$535 (个人申购者) / 美元$842 (机构申购者) 和本人所有受信託管理資產的每年2％管理費。所有費用提前支付，不予退還。</p>
                                <p>信託公司和託管公司保留可追回已發放款項與實際支出（如直接從計畫衍生的第三方收費）的權利。尤其若基金結構複雜或按成員要求而獨有，額外的第三方收費可能會因管理該基金而產生。各項收費將根據個別具體情況而訂，這些費用通常會事先通知並商定後方確立收費。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>   
<!--    14h      -->
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 60%;">
                                <p><strong>14h. Authority for Disclosure of Personal Data</strong></p>
                                <p>I hereby consent for Odeon and Escrow Trustee to receive all necessary information on my asset base globally as it required for the purposes of "Know Your Client". I understand that my authority is required under the Personal Data (Privacy) Ordinance and I also understand that this information will not be disclosed outside of Odeon and Escrow Trustee's legal requirements to any person, company or any institution without my further written and signed authority.</p>
                            </td>
                            <td>
                                <p><strong>標準規定收費</strong></p>
                                <p>為達到"瞭解客戶"，根據"信託公司條例"， 本人同意信託公司和託管公司可於全球範圍內收到本人資產基數的所有必要資訊。本人明白， 根據"個人資料 (私隱) 條例"， 必須本人授權方可披露本人的個人資料， 本人也明白， 在未經本人進一步書面簽署授權下， 本人的資料不會披露予信託公司的信託要求以外的任何人士、公司或機構。</p>
                            </td>
                        </tr>
                    </tbody></table>
                </div>       
                <div class="form-group alone">
                    <div class="col-md-12">
                        <label style="color: #ff0000;"><input type="checkbox" id="DisclosurePersonalDataConfirm" name="DisclosurePersonalDataConfirm" value="Y" required="required"> I have read and agree to the terms & conditions above | 我已阅读并同意上述条款和条件</label>
                    </div>
                </div>
<!--    14i      -->
                <div class="g-header">
                    <strong>14i. Cooling-off Period Waiver | 放棄冷靜期</strong>
                </div>
                <div class="g-body margin-bottom numberten">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 100%;">
                                <p>There is a 7 working day cooling-off period from the date of this Subscription: during this period, the Subscription will be on hold, and Set-up and first year Administration Fees can be refunded, net of the relevant banking charges. As the Odeon and the escrow trustee cannot commence set-up until after the Cooling-off Period, if the Subscription is time-sensitive, the subscriber may, by opting here, waive the Cooling-off Period. In that event, set-up of the investment trust program will commence upon receipt of the Subscription. After the Cooling-off Period, or in the event that the Cooling-off Period is waived, fees shall be non-refundable. <br>
                                    成員申請之日起有7个工作天的冷靜期：在此期間申請將被擱置，若決定退出計畫，信託帳號開戶費和首年的管理費，扣除银行汇款或转账手續費用後可獲退還。由於直到冷靜期過後Odeon和信托公司才能開始進行相關作業，所以申购者可以在此簽字放棄冷靜期。在這種情況下，信託基金的設置將在收到申請後隨即開始。在冷靜期後或在申购者放棄冷靜期的情況下，上述費用將不會退還。</p>
                                <ul style="list-style:none;">
                                    <li><input type="radio" id="NeedCoolingOff" name="case[NeedCoolingOff]" value="Y"> I need the cooling-off period. Only activate the account 7 working days thereafter.<br> <span style="margin-right:12px;">&nbsp;</span>我选择7天的冷静期，如我7日内无额外通知，到时启动我的信托账号。</li>
                                    <li><input type="radio" id="WaiveCoolingOff" name="case[NeedCoolingOff]" value="N"> I opt to waive the cooling-off period. Activate my account immediately.<br> <span style="margin-right:12px;">&nbsp;</span>我选择放弃冷静期，立即启动我的信托账号。</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody></table>
                </div>       
                <div class="form-group alone">
                    <div class="col-md-12">
                        <label style="color: #ff0000;"><input type="checkbox" id="CoolingOffPeriodConfirm" name="CoolingOffPeriodConfirm" value="Y" required="required"> I/We Read and Understood the above.</label>
                    </div>
                </div>
<!--    Bank      -->
                <div class="g-header">
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <strong>15.  Subscriber Banking Information</strong>
                            </td>
                            <td>
                                <strong>申請者銀行帳戶資料</strong>
                            </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="g-body margin-bottom">
                    <table border="0" cellpadding="3" cellspacing="3">
                        <tbody><tr>
                            <td style="width: 50%;">
                                <p><label>Bank Name | 受款銀行名稱 *</label>
                                <input type="text" id="BankName" name="bank[Name]" class="form-control" value="" required="required"></p>
                                
                                </p><label>Account Number | 收款人戶口號碼 *</label>
                                <input type="text" id="BankAccountNumber" name="bank[AccountNumber]" class="form-control" value="" required="required"></p>
                                
                                <p><label>Account Name | 收款人名稱 *</label>
                                <input type="text" id="BankAccountName" name="bank[AccountName]" class="form-control" value="" required="required"></p>
                    
                                <p><label>SWIFT Code | 銀行國際代碼 *</label>
                                <input type="text" id="SwiftCode" name="bank[SwiftCode]" class="form-control" value="" required="required"></p>
                            </td>
                            <td>
                                <label>Bank Address | 收款銀行地址 *</label>
                                <input type="text" id="BankAddress" name="bank[Address]" class="form-control" value="" required="required">
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody></table>
                </div>
<!--Documentation Requirement for Trust Account Application (Individual)-->
                <div class="acounttype_individual">
                    <div class="g-header">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tbody><tr>
                                <td style="width: 100%;">
                                    <strong>16.  信託帳戶申請的文件要求 (個人) | Documentation Requirement for Trust Account Application (Individual)</strong>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="g-body margin-bottom">
                        <table border="0" cellpadding="3" cellspacing="3">
                            <tbody><tr>
                                <td colspan="2">
                                    <p><strong>個人申请人 | INDIVIDUAL APPLICANT </strong></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>帶照片的身份證件 Photo ID<br> (如身份證/護照/駕駛證)<br> (National ID / Passport / Driver License preferred)<br><br> <input id="IAPhotoid" class="file" type="file" name="IAPhotoid[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>居住證明（不超過3個月）<br> Proof of Residency (of not more than 3 months old)<br> (如銀行對帳單/信用卡對帳單/保險對帳單/水電費帳單)<br> (Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill preferred)<br><br> <input id="ia-proofresidency" class="file" type="file" name="IAProofresidency[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>銀行對帳單（不超過3個月）和/或同等文件<br> Bank Statement (of not more than 3 months old) and/or equivalent documents<br> (顯示資產和/或基金多於信託存款金額)<br> (Displaying assets and/or funds more than the Trust Deposit Amount)<br><br> <input id="ia-bankstatement" class="file" type="file" name="IABankstatement[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>簽名樣本 Specimen Signature <br><br> <input id="IASpecimensign" class="file" type="file" name="IASpecimensign[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                </div>
<!--Documentation Requirement for Trust Account Application (Corporate)-->
                <div class="acounttype_corporate">
                    <div class="g-header">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tbody><tr>
                                <td style="width: 100%;">
                                    <strong>16.   信託帳戶申請的文件要求 (機構) | Documentation Requirement for Trust Account Application (Corporate)</strong>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="g-body margin-bottom">
                        <table border="0" cellpadding="3" cellspacing="3">
                            <tbody><tr>
                                <td colspan="2">
                                    <p><strong>機構申请人 CORPORATE APPLICANT </strong></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>公司註冊證<br> Certificate of Incorporation<br><br> <input id="ca-namechange" class="file" type="file" name="CACertincorporation[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>名稱變更證明（如適用）<br> Certificate of Name Change (if applicable)<br><br> <input id="ia-namechange" class="file" type="file" name="CANamechange[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>存續證明（如適用）<br> Certificate of Good Standing (if applicable)<br><br> <input id="ca-goodstand" class="file" type="file" name="CAGoodstand[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>董事及人員登記簿<br> Registry of Director(s) & Officer(s)<br><br> <input id="ca-regdirector" class="file" type="file" name="CARegdirector[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td colspan="2">
                                    <p>公司地址證明<br> Proof of Business Address<br> (如銀行對帳單/信用卡對帳單/保險對帳單/水電費帳單)<br> (Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill preferred)<br><br><input id="ca-proofbusadd" class="file" type="file" name="CAProofbusadd[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p>公司章程及組織大綱<br> Memorandum and Articles of Association<br><br> <input id="ca-memorandumaa" class="file" type="file" name="CAMemorandumaa[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                                <td>
                                    <p>近期財務報表（如適用）<br> Most Recent Financial Statements (if applicable) <br><br> <input id="ca-recentfinancialstatement" class="file" type="file" name="CARecentfinancialstatement[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td colspan="2">
                                    <p><strong>帶照片的身份證件 Photo ID</strong></p>
                                    <p>董事 Director(s)<br><input id="ca-directorsid" class="file" type="file" name="CADirectorsid[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                    <p>公司簽署人 Company Signatory(ies)<br><input id="ca-companysign" class="file" type="file" name="CACompanysign[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                    <p>股東持有公司股本10%或更多和/或持有控制權的（投票權）股東<br> Shareholder(s) holding 10% or more of Company Share Capital and/or Controlling (Voting) Shareholder<br><input id="ca-shareholders" class="file" type="file" name="CAShareholders[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td colspan="2">
                                    <p><strong>居住證明 (不超過3個月) Proof of Residency (of not more than 3 months old) </strong></p>
                                    <p>董事 Director(s)<br><input id="ca-directorsproof" class="file" type="file" name="CADirectorsproof" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple><br></p>
                                    <p>公司簽署人 Company Signatory(ies)<br><input id="ca-companysignproof" class="file" type="file" name="CACompanysignproof[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple><br></p>
                                    <p>股東持有公司股本10%或更多和/或持有控制權的（投票權）股東<br> Shareholder(s) holding 10% or more of Company Share Capital and/or Controlling (Voting) Shareholder<br><input id="ca-shareholdersproof" class="file" type="file" name="CAShareholdersproof[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple></p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <p><strong>授權簽署人名單及簽名樣本<br> Authorised Signatory Lists & Specimen Signatures</strong></p>
                                </td>
                            </tr>                            
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p><input id="ca-authorizedone" class="file" type="file" name="CAAuthorizedone[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple><br>
                                    <input type="text" name="case[CAAuthorizedonename]" class="form-control" placeholder="姓名 Name :" /><br>
                                    <input type="text" name="case[CAAuthorizedonetitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                                <td>
                                    <p><input id="ca-authorizedtwo" class="file" type="file" name="CAAuthorizedtwo[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple><br>
                                    <input type="text" name="case[CAAuthorizedtwoname]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedtwotitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                            </tr>
                            <tr class="tr-td-withborder">
                                <td style="width: 50%;">
                                    <p><input id="ca-authorizedthree" class="file" type="file" name="CAAuthorizedthree[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple><br>
                                    <input type="text" name="case[CAAuthorizedthreename]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedthreetitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                                <td>
                                    <p><input id="ca-authorizedfour" class="file" type="file" name="CAAuthorizedfour[]" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg","bmp"]' multiple><br>
                                    <input type="text" name="case[CAAuthorizedfourname]" class="form-control" placeholder="姓名 Name :"/><br>
                                    <input type="text" name="case[CAAuthorizedfourtitle]" class="form-control" placeholder="稱謂 Title :" /></p>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                </div>

                <i>ICP.MA.1. 2015 REV 1</i>
                <div style="text-align: center;"><button type="submit" class="btn btn-success">Submit | 提交</button></div>
            </div>
        </div>
    </form>
</div>
<!-- /page content -->
<?php View::footer('forms'); ?>