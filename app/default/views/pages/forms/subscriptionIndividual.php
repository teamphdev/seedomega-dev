<?php 
View::$title = 'Individual Skylight. Subscription Form';
View::$bodyclass = 'guest';
View::header('forms'); 

if(isset($_POST['acct'])){ $vacct = $_POST['acct']; }
if(isset($_POST['user'])){ $vuser = $_POST['user']; }
if(isset($_POST['meta'])){ $vmeta = $_POST['meta']; }
if(isset($_POST['dob'])){ $vdob = $_POST['dob']; }
if(isset($_POST['acctbe'])){ $vacctbe = $_POST['acctbe']; }
if(isset($_POST['bank'])){ $vbank = $_POST['bank']; }

?>

    <!-- page content -->
    <div class="body-wrap">
        <div class="page-header">
            <div class="container">
                <h1><?php echo View::$title; ?></h1>
            </div>
        </div>
        
        <form id="master_form" action="" method="post" class="form-horizontal subscription-form" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo md5(microtime()); ?>">        
        <input type="hidden" name="action" value="addcasefile">
        <input type="hidden" name="acct[FormType]" value="Traditional">
        <div class="section-holder masterform container">
            <?php echo View::getMessage(); ?> 
            
            <!-- Section 1 GENERAL GUIDANCE -->
            <!-- <div class="section section-0 text-center">
                <img src="<?php echo view::url(); ?>/assets/images/form/form-banner.jpg" class="img-responsive">
            </div> -->

            <!-- Section 1 GENERAL GUIDANCE -->
            <div class="section section-1">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        1. General Guidance
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        概括指引
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p>Issued by Global Asset Inc Limited (“Global Asset” or “Company”), Skylight is a special purpose vehicle which has engaged the following entity: Duo Asset Management (Hong Kong) Limited, Peninsular Capital (Labuan) Limited, Braun International Financial Leasing (China) Co Ltd and CC&Co Partners (Australia) Pty Ltd.</p>
                            <p>This Subscription Package should only be completed by the Subscriber. This form should be read in conjunction with the Subscription Agreement, the Product Highlight Sheet and the Information Memorandum. We seek your understanding and cooperation in furnishing the required documents and appreciate your time and effort in doing so.</p>
                            <p>It is important that you complete this Subscription Form in full, and sign as required to enable us to consider your Subscription. Kindly attach necessary documents. Failure to provide all relevant information and supporting documentation will result in a delay to the application being processed. Additional 
                            information may be required to support this Subscription. Any additional details / notes / instructions or those provided at a non-designated area of the form may not be executed.</p>
                            <p><span style="color:red; font-weight:bold;">Note:</span> If the Chinese version and English version of the content is different, the English version shall prevail.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c content-box">
                            <p>您申購的 Skylight SPV* (簡稱 “產品”) 計劃是由 Global Asset Inc Limited (簡 稱“環球聯富國際集團”或“公司”或“Global Asset”）所發行。Skylight（簡稱“產 品＂)是由雙瑩資產管理（香港）有限公司, 納閩 Peninsular Capital Limited 以 及博朗國際融資租賃(中國）有限公司及澳大利亞 CC&Co Partners Pty Ltd 攜手 管理。</p>
                            <p>此申購程序應由申購人親自填寫。本公司的申購協議規範了產品設計的條款及 條件，請與此申購書及主要條款書一起閱讀。感謝您的理解並希望您能配合本 公司要求提供所需之文件。</p>
                            <p>請完整填妥此申購書並按要求簽名，以方便本公司審核您的申請。如未能提供 相關資料和證明文件將導致申請開戶有所延誤。附加資料應包括在內以作為本 申請的佐證資料。在本申購書非指定地方提供的任何額外資料、備註或指令將 不會被執行。</p>
                            <p><span style="color:red; font-weight:bold;">注:</span> 如本中文版本與英文版本內容有差異，將以英文版本為准。</p>
                            <p>* SPV = Special Purpose Vehicle (特殊目的載體)</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            
            <!-- Section 2 Privacy Policy -->
            <div class="section section-2">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        2. Privacy Policy
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        私隱政策
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p>Global Asset will not disclose information on the Subscriber and their affairs except as required under applicable laws, including the Anti-Money Laundering and CounterTerrorist Financing (Financial Institutions) Ordinance (“AML Ordinance”). Global Asset will not disclose information to any third party, except as required under applicable laws, including the AML Ordinance and the Inland Revenue Ordinance or otherwise as directed by a court order.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c content-box">
                            <p>除非在《打擊洗錢及恐怖分子資金籌集(金融機構)條例》或《稅務條例》或法 庭指令的要求下，否則公司不會披露申購人任何資料;在任何行政需要外，不論 基於任何目的，公司一般不應透露申購人資料,也不會使用受益人的資料。所有 有關申購人資料是受到保護的。</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 3. Risk Warning -->
            <div class="section section-3">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        3. Risk Warning
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        風險聲明
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p>Please see the Product Highlight Sheet and the Subscription Package. Each of the Skylight Product is designed to follow a different strategy and calculation which you may selec. </p>
                            <p>The Skylight Products share the following characteristics: It may carry investment risks. Do note that the past returns are not necessarily indicative of the future performance.</p>
                            <p>Disclaimers specific to each Skylight Product are written in each Product Highlight Sheet. In selecting the Skylight Products, you must acknowledge and understand that there is always risk in any investments.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c content-box">
                            <p>請詳細閱讀申購程序的條款書和申購書，每種產品是都是以不同的策略及計算 方法而設計的。</p>
                            <p>產品會有以下特點:各個產品存有投資風險。過往的投資績效並不一定反映未來 的表現。</p>
                            <p>在每個條款書上已有免責聲明特別提及，在選擇每一項投資時,您需要瞭解所有 投資都是有風險的。</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 4. Subscriber Declaration -->
            <div class="section section-4">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        4. Subscriber Declaration
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        申請人加入聲明
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p>I, being the above named individual and having full legal authority to make this Subscription, certify that: </p>
                            <ol type="a">
                                <li>I have no legal disability of any description to make this Subscription</li>
                                <li>I have complied with all applicable laws in making this Subscription; </li>
                                <li>All information given by me and contained in this Subscription Form is complete, true and correct.</li>
                            </ol>
                            <p>I have read and understood the Subscription Agreement; and </p>
                            <ol type="a" start="4">
                                <li>I have taken note of and initialed to signify my awareness and understanding of the fees payable by Subscribers, diversification waiver, investment risk waiver and disclaimers; and </li>
                                <li>hereby make application as a Subscriber.</li>
                            </ol>
                            <br>
                            <p>I/we further declare that I am / We are a sophisticated, professional or accredited investor as such terms are defined, or the equivalent thereof, in my home jurisdiction to qualify and exempt my purchase, contribution, or participation in this investment   program from any applicable information memorandum, registration and notice requirements for the distribution, sale, or marketing of securities that may apply to this Skylight product, and I have obtained or sought the appropriate advice from financial and legal professionals to confirm this statement, thus waiving my rights to claim otherwise in any dispute or proceeding in law, court or hearing. I/We undertake to inform Global Asset promptly if I/we cease to be a sophisticated, professional or accredited investor at any time post this declaration.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p>本人，作為上述指定的個人，已擁有完整的法律權力，申請成為申購人，並證 明：</p>
                            <ol type="a">
                                <li>本人在沒有喪失法律行為能力的情況下提出此申請; </li>
                                <li>本人已遵守所有適用法律去提出此申請;</li>
                                <li>本人在此申購程序中提供的所有資訊均是完整 、真實且正確無誤的。</li>
                            </ol>
                            <p>本人已閱讀、瞭解並同意附加的申購人協議條款；</p>
                            <ol type="a" start="4">
                                <li>作出簡簽，以表示本人完全瞭解產品且明白申購此產品所需的費用、放棄 分散投資、免投資風險和各項免責聲明; </li>
                                <li>現申請作為此產品的申購人。。</li>
                            </ol>
                            <p>我/我們進一步聲明，本人/我們是一名專業且經認可或是等同上述說明的投資 者，在我們司法管轄所在地任何適用的說明書及小册子中得以豁免顯示本人/ 我們的購買、供款、登記和通知要求。關於該產品的發行、銷售或營銷等相關 資訊，本人/我們已經獲得或尋求金融和法律專業人士的適當建議來確認此一說 法，任何因此而產生的糾紛或爭議，本人/我們在此聲明放棄任何訴訟或司法聽 證會之權利。</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>                  

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 5. Account Type -->
            <div class="section section-5">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        5. Account Type
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        帳戶類型
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p class="radio">
                            <label>
                                <input type="radio" class="naccount" id="TrustAccountTypeIndividual" name="acct[AccountType]" value="Corporate" disabled=""> <strong>Corporate 機構</strong>
                            </label>
                            </p>
                            <p>(Please Fill Up Section 5 If Corporate is Selected) <br>
                            (如選擇機構，請填寫第5 節)</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p class="radio">
                            <label>
                                <input type="radio" class="naccount" id="TrustAccountTypeIndividual" name="acct[AccountType]" value="Individual" checked="checked"> <strong>Individual 個人</strong>
                            </label>
                            </p>
                            <p>(Please Skip Section 5 If Individual is Selected) <br>
                            (如選擇個人，請跳過第5 節)</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 6 Corporate Client Information -->
            <div class="section section-6">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        6. Subscriber’s Personal Details
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        申請人的個人資料
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Agent ID</label></p>
                        <p><input type="text" id="ReferrerUserChecker" class="form-control" name="user[ReferrerUserID]" value="<?php echo isset($vuser['ReferrerUserID']) ? $vuser['ReferrerUserID'] : ""; ?>" required="required" rel="<?php echo View::url('ajax/userinfo'); ?>" ><span id="referrerloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Agent Name</label></p>
                        <p><input type="text" id="referrerdata" class="form-control" value="" disabled=""></p>
                    </div>
                    
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Secondary Agent ID</label></p>
                        <p><input type="text" id="ReferrerUserChecker2" class="form-control" name="user[SecondReferrerUserID]" value="<?php echo isset($vuser['SecondReferrerUserID']) ? $vuser['SecondReferrerUserID'] : ""; ?>" rel="<?php echo View::url('ajax/userinfo'); ?>" ><span id="referrerloading2" class="glyphicon glyphicon-refresh glyphicon-refresh-animate" style="display: none;"></span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Secondary Agent Name</label></p>
                        <p><input type="text" id="referrerdata2" class="form-control" value="" disabled=""></p>
                    </div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>稱呼 Title</label></p>
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'meta[Salutation]',
                                    'options'=>AppUtility::getSalutation(),
                                    'value'=>'Mr',  
                                    'class'=>'form-control',
                                    'id'=>'Salutation',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Passport / Photo ID Number 護照 / 附相片身份證號碼</label></p>
                        <p><input type="text" id="IdNumber" name="meta[IdNumber]" class="form-control" value="<?php echo isset($vmeta['IdNumber']) ? $vmeta['IdNumber'] : ""; ?>" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>名字 Given Name</label></p>
                        <p><input type="text" id="FirstName" name="meta[FirstName]" class="form-control" value="<?php echo isset($vmeta['FirstName']) ? $vmeta['FirstName'] : ""; ?>" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Country of Citizenship 國籍</label></p>
                        <p>
                        <select id="Country" name="meta[Country]" class="form-control">
                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                            <?php } ?>
                        </select> 
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>姓氏 Surname</label></p>
                        <p><input type="text" id="LastName" name="meta[LastName]" class="form-control" value="<?php echo isset($vmeta['LastName']) ? $vmeta['LastName'] : ""; ?>" required="required"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Country of Tax Residency 稅務居住國</label></p>
                        <p><select id="Country" name="acct[BusinessCountry]" class="form-control">
                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                <option value="<?php echo $country; ?>" ><?php echo $country; ?></option>
                                <?php } ?>
                            </select>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Date of Birth 出生日期</label></p>
                        <div class="col-md-3 col-sm-3 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Month]',
                                    'options'=>AppUtility::getMonth(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Month',
                                    'placeholder'=>'Month',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Day]',
                                    'options'=>AppUtility::getDay(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Day',
                                    'placeholder'=>'Day',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 no-padding">
                        <?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'dob[Year]',
                                    'options'=>AppUtility::getYear(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'Year',
                                    'placeholder'=>'Year',
                                    'custom'=>'required'
                                )
                            ); 
                        ?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <p><label>Marital Status 婚姻狀況</label></p>
                        <p><?php 
                            View::form(
                                'select',
                                array(
                                    'name'=>'meta[CivilStatus]',
                                    'options'=>AppUtility::getCivilStatus(),
                                    'value'=>'',
                                    'class'=>'form-control',
                                    'id'=>'CivilStatus',
                                    'custom'=>'required'
                                )
                            ); 
                        ?></p>
                    </div>
                    <div class="col-sm-3 col-xs-12 form-group">
                        <p><label>Gender 性别</label></p>
                        <p><select id="Gender" name="meta[Gender]" class="form-control" required>
                                <option value="M">Male | 男</option>
                                <option value="F">Female | 女</option>
                        </select></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Occupation & Job Title <small>(if retired, state retired and former occupation)</small> <br>職業及職位 <small>(如退休，請註明退休，並退休前的職位)</small></label></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><input type="text" id="Occupation" name="meta[Occupation]" class="form-control" placeholder="職業 | Occupation" value="<?php echo isset($vmeta['Occupation']) ? $vmeta['Occupation'] : ""; ?>" required="required"></p>
                        <p><input type="text" id="JobTitle" name="meta[JobTitle]" class="form-control" placeholder="職位 | Job Title" value="<?php echo isset($vmeta['JobTitle']) ? $vmeta['JobTitle'] : ""; ?>" required="required"></p>
                    </div>
                     
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 7 Subscriber’s Contact Details -->
            <div class="section section-7">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        7. Subscriber’s Contact Details
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        個人資料
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Prefered Language 首選語言</label></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p>
                        <select id="Language" name="meta[Language]" class="form-control" required>
                                <option value="en">英語 | English</option>
                                <option value="tcn">繁體中文 Traditional Chinese</option>
                                <option value="scn">簡體中文 Simplified Chinese</option>
                        </select>
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Primary Residential Address 居住地址</label></p>
                        <p><input type="text" id="Address" name="meta[Address]" class="form-control" value="<?php echo isset($vmeta['Address']) ? $vmeta['Address'] : ""; ?>" required="required"></p>
                        <p><input type="text" id="Address2" name="meta[Address2]" class="form-control" value="<?php echo isset($vmeta['Address2']) ? $vmeta['Address2'] : ""; ?>"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Correspondence Address <small>(if different from residential address)</small> 通訊地址 <small>（如與居住地址不同）</small></label></p>
                        <p><input type="text" id="CorrespondenceAddress" name="meta[Address3]" class="form-control" value="<?php echo isset($vmeta['Address3']) ? $vmeta['Address3'] : ""; ?>"></p>
                        <p><input type="text" id="CorrespondenceAddress2" name="meta[Address4]" class="form-control" value="<?php echo isset($vmeta['Address4']) ? $vmeta['Address4'] : ""; ?>"></p>
                        <p><span class="help-block">(if different from residential address)</span></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Mobile 手機</label></p>
                        <p><input type="text" id="Mobile" name="meta[Mobile]" class="form-control" value="<?php echo isset($vmeta['Mobile']) ? $vmeta['Mobile'] : ""; ?>" required="required"></p>
                        <p><label>Home Telephone 住宅電話</label></p>
                        <p><input type="text" id="Phone" name="meta[Phone]" class="form-control" value="<?php echo isset($vmeta['Phone']) ? $vmeta['Phone'] : ""; ?>"></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>Email 電郵</label></p>

                        <p><input type="email" id="EmailCheckerOut" name="acct[AccountEmail]" class="form-control" value="<?php echo isset($vacct['AccountEmail']) ? $vacct['AccountEmail'] : ""; ?>" required="required">
                            
                        </p>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 9. Nominated Beneficiaries Form -->
            <div class="section section-9">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        8. Nominated Beneficiaries Form
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        提名受益人表格
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p>Upon your demise, all remaining assets will be held in favour for your nominated beneficiary or beneficiaries. If you have not nominated beneficiaries, the remaining assets will be held in favour of your estate. </p>
                            <p>If a designated beneficiary does not survive the other beneficiaries, their share maybe distributed in proportion to the remaining beneficiaries share of benefits. </p>
                            <p class="checkbox">
                                <label><strong><input type="checkbox" name="case[HasNominatedBeneficiaries]" value="yes" class="uboswitch" rel=".nombeneficiaris"> I wish to nominate further beneficiaries as per below: <br> 我想進一步提名以下受益人：</strong></label>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c content-box">
                            <p>在您離世後，您所有遺留的資產將會由您提名的受益人繼 承。如果您未有提名受益人，公司將會讓您的遺產繼承者繼 承您遺留的資產。</p>
                            <p>如果您所指定的受益人已經離世，其他受益人可能會按比例 獲得遺留基金存款資產的利益。</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 9. Nominated Beneficiaries -->
            <div class="section section-9 nombeneficiaris">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        Nominated Beneficiary
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        提名受益人
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Given Name 名</label></p>
                            <p><input type="text" name="acctbe[1][ABFirstName]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABFirstName']) ? $vacctbe[1]['ABFirstName'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>Contact Phone 聯絡電話</label></p>
                            <p><input type="text" name="acctbe[1][ABPhone]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABPhone']) ? $vacctbe[1]['ABPhone'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Surname 姓</label></p>
                            <p><input type="text" name="acctbe[1][ABLastName]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABLastName']) ? $vacctbe[1]['ABLastName'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>Email 電郵地址</label></p>
                            <p><input type="text" name="acctbe[1][ABEmail]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABEmail']) ? $vacctbe[1]['ABEmail'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Relationship 關係</label></p>
                            <p><input type="text" name="acctbe[1][ABRelationship]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABRelationship']) ? $vacctbe[1]['ABRelationship'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>Primary Correspondence Address 主要通訊地址</label></p>
                            <p><input type="text" name="acctbe[1][ABAddress1]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABAddress1']) ? $vacctbe[1]['ABAddress1'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Percentage (%) of Benefit 受益比例 %</label></p>
                            <p><input type="number" name="acctbe[1][ABPercentage]" class="form-control bot-border" value="<?php echo isset($vacctbe[1]['ABPercentage']) ? $vacctbe[1]['ABPercentage'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>&nbsp;</label></p>
                            <p></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 9. Nominated Beneficiaries -->
            <div class="section section-9 nombeneficiaris">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        Nominated Beneficiary
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        提名受益人
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Given Name 名</label></p>
                            <p><input type="text" name="acctbe[2][ABFirstName]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABFirstName']) ? $vacctbe[2]['ABFirstName'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>Contact Phone 聯絡電話</label></p>
                            <p><input type="text" name="acctbe[2][ABPhone]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABPhone']) ? $vacctbe[2]['ABPhone'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Surname 姓</label></p>
                            <p><input type="text" name="acctbe[2][ABLastName]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABLastName']) ? $vacctbe[2]['ABLastName'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>Email 電郵地址</label></p>
                            <p><input type="text" name="acctbe[2][ABEmail]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABEmail']) ? $vacctbe[2]['ABEmail'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Relationship 關係</label></p>
                            <p><input type="text" name="acctbe[2][ABRelationship]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABRelationship']) ? $vacctbe[2]['ABRelationship'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>Primary Correspondence Address 主要通訊地址</label></p>
                            <p><input type="text" name="acctbe[2][ABAddress1]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABAddress1']) ? $vacctbe[2]['ABAddress1'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Percentage (%) of Benefit 受益比例 %</label></p>
                            <p><input type="number" name="acctbe[2][ABPercentage]" class="form-control bot-border" value="<?php echo isset($vacctbe[2]['ABPercentage']) ? $vacctbe[2]['ABPercentage'] : ""; ?>"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><label>&nbsp;</label></p>
                            <p></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 10  Banking Information -->
            <div class="section section-10">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        9. Documentation Requirement for Trust Account Application (Individual)
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        基金存款帳戶申請的文件要求 (個人)
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-12 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>INDIVIDUAL SUBSCRIBER  個人申購人</strong></p>
                            
                            <div class="radio-upload checkbox">
                                <label><input type="checkbox" class="uboswitch" value="Y" rel="#indphotoid"> Photo ID (National ID / Passport / Driver License) 帶照片的身份證件 (如身份證/護照/駕駛證)</label>
                                
                                <div id="indphotoid" class="ubofiles">
                                    Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                    <input class="file" type="file" name="IAPhotoid[]" data-show-upload="true" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                                </div>
                            </div>

                            <div class="radio-upload checkbox">
                                <label><input type="checkbox" class="uboswitch" value="Y" rel="#indproofofresidency"> Proof of Residency (No more than 3 months) 居住證明（不超過3個月） <br>(Bank Statement / Credit Card Statement / Insurance Statement / Utility Bill) (如銀行對帳單/信用卡對帳單/保險對帳單/水電費帳單)</label>
                                
                                <div id="indproofofresidency" class="ubofiles">
                                    Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                    <input id="ia-proofresidency" class="file" type="file" name="IAProofresidency[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                                </div>
                            </div>

                            <div class="radio-upload checkbox">
                                <label><input type="checkbox" class="uboswitch" value="Y" rel="#indibankbankstatement"> Bank Statement (No more than 3 months) and/or equivalent documents 銀行對帳單(不超過3個月)和/或同等文件 <br> (Displaying assets and / or funds more than the Investment Amount) (顯示資產和/或基金多於基金存款金額)</label>
                                
                                <div id="indibankbankstatement" class="ubofiles">
                                    Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                    <input id="ia-bankstatement" class="file" type="file" name="IABankstatement[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                                </div>
                            </div>

                            <br>
                            <div id="specimensign" class="signatureSection" upload-name="IASpecimensign" data-name="signatures[IASpecimensign]">
                                <div class="radio signOptions">
                                    <label class="row">Specimen Signature 簽名樣本</label>
                                    <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                    <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                </div>
                                <div class="parentsign">
                                    <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                    <div class="sign-upload hidetag">
                                        <input id="IASpecimensigne" class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                                    </div>
                                </div>
                                <center><small>Please sign within the borders of the box 請在方格內簽署</small></center>
                            </div>
                            
                            
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 9. Subscriber Declaration -->
            <div class="section section-9 nombeneficiaris">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        Subscriber Declaration
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        申購人聲明
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Signed by the Subscriber on the date below: </label></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="right-c">
                            <p><strong>申購人簽名日期：</strong></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-12 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Subscriber’s Name  申購人姓名</label></p>
                            <p><input type="text" name="case[SubsDecName]" class="form-control box-border" value=""></p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 form-group">
                        <div class="left-c">
                            <div id="subssign" class="signatureSection" upload-name="SubsDecSignature" data-name="signatures[SubsDecSignature]">
                                <div class="radio signOptions">
                                    <label class="row">Subscriber’s Signature  申購人簽名</label>
                                    <label><input class="signradio" type="radio" value="0" checked="checked" /> Create Sign</label>
                                    <label><input class="signradio" type="radio" value="1"> Upload Sign</label>
                                </div>
                                <div class="parentsign">
                                    <div data-max-size="2048" data-pen-tickness="2" data-pen-color="black" class="sign-field file"></div>
                                    <div class="sign-upload hidetag">
                                        <input class="file sign-file-upload" type="file" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><input type="text" name="case[SubsDecDate]" class="form-control js-datepicker box-border" value=""></p>
                            <p><label>Date (YYYY/MM/DD)  日期（日/月/年）</label></p>
                            <br>
                            <p><small>Attention 注 :  The Board of Directors  董事會 <br>Dear Sir/Madam, 尊敬的先生/女士們</small></p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 10. Subscriber Declaration -->
            <div class="section section-10">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        10. Ultimate Beneficial Owner (UBO) Declaration
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        最終受益方聲明書 
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body ubo-sect">

                    <div class="row">
                        <div class="col-sm-6 col-xs-12 col-md-6">
                            <input placeholder="I, (full name as in passport 全名，如護照) " name="case[UBOName]" class="form-control" type="text">
                        </div>
                        <div class="col-sm-6 col-xs-12 col-md-6">
                            <input name="case[UBOAddress]" placeholder="Of (full residential address 詳細住址)" class="form-control" type="text">
                        </div>
                    </div>
                    <br>
                    <div class="clearfix"></div>
                    <p class="text-center">do hereby declare that my/our source(s) of funds which I/we have are my/our assets derived from: <br>我，居住於以上填寫住址，特此聲明我(我們)的資金及我(我們)的資產來源於:</p>

                    <section class="row">
                        <h5>Please tick the source of funds  請提供資產來源</h5>
                        <div class="uboitem checkbox">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOEmploymentIncome"> Employment Income (employer, title, period, salary figures, CV/Resume) <br> 就業收入（就業組織，職務，期限，工資數額，履歷表/簡歷）</label>
                            
                            <div id="UBOEmploymentIncome" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOEmploymentIncome[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                        
                        <div class="uboitem checkbox">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOCommission"> Sales / Commission Income (details of company(ies) and value figures)<br>銷售/佣金收入（公司的詳細情況及相關資料）</label>
                            <div id="UBOCommission" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOCommission[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                      
                        <div class="uboitem checkbox">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOBusiness"> Business Income (business particulars and financial statements to determine general turnover and profits per respective years)<br>營業收入（營業相關資料與財務報表，確認每個相應年度的營業收入）</label>
                            <div id="UBOBusiness" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOBusiness[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                       
                        <div class="uboitem checkbox">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOInheritance"> Inheritance (inheritance lineage and value) <br>繼承（繼承血統及其數值）</label>
                            <div id="UBOInheritance" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOInheritance[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                         
                        <div class="uboitem checkbox">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOGift"> Gift (the details of giver and details of gift and its value) <br> 贈與物（贈與者及贈與物之細節及其價值）</label>
                            <div id="UBOGift" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOGift[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                        
                        <div class="uboitem checkbox">
                            <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOSales"> Proceeds from the sale of property and/or other assets (description of assets and value and how they were obtained in the first instance) <br> 出售物業收益或其他資產 (資產價值的描述，以及如何在第一時間取得)</label>
                            <div id="UBOSales" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOSales[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div> 
                                                         
                        <div class="uboitem checkbox">
                           <label><input type="checkbox" class="uboswitch" value="Y" rel="#UBOOther"> Any other (please describe) <br> 其他補充事項 (請進行描述)</label>
    
                            <div id="UBOOther" class="ubofiles">
                                Click browse to upload <br> 拖放文件或點擊此處上傳文件<br />
                                <input id="file-0a" class="file" type="file" name="UBOOther[]" data-show-upload="false" data-allowed-file-extensions='["docx","pdf","jpeg","png","jpg","bmp"]' multiple>
                            </div>
                        </div>
                    </section>

                    <p><label><input type="checkbox" id="UBODeclarationConfirm1" name="UBODeclarationConfirm" value="Y"> I further declare that :  我進一步聲明 :</label></p>
                    <ul style="padding-left: 20px;">
                        <li>I am not involved in money laundering and/or drug trafficking; and <br>我沒有參與洗黑錢或販賣毒品;以及</li>
                        <li>any monies and/or securities that have been or will be deposited in any bank account in the name of the Company do not originate, either directly or indirectly, from illegal and/or criminal activities. <br> 已經存入或將要存入的銀行帳戶、公司名稱、及其任何款項或證券，並無直接地或間接地參與非法或犯罪活動。</li>
                    </ul>
                    <br>
                    <p><label>我進一步聲明 :</label></p>
                    <ul style="padding-left: 20px;">
                        <li>我沒有參與洗黑錢或販賣毒品;</li>
                        <li>已經存入或將要存入的公司銀行帳戶、及其任何款項或證券的任何款項，並無直接地或間接地參與非法或犯罪活動。</li>
                    </ul>
                    
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 11 Declaration of Politically Exposed Person (“PEP”) and Tax Status -->
            <div class="section section-10">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        11. Declaration of Politically Exposed Person (“PEP”) and Tax Status
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        稅務及政治人物聲明
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p>I hereby declare that : 我特此聲明 :</p>
                        <ul>
                            <li>I am not a PEP2 or a Closely Connected Person to a PEP3.  本人不是政治人物或政治人物的密切關連人士。</li>
                            <li>I am not a U.S. Citizen and do not intent to be one.  本人並非美國人士，並且不打算成爲美國人士。</li>
                            <li>My tax residency and tax identification number are:  本人的税務居地及稅務識別號為：</li>
                        </ul>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="boxed-border text-center">
                            <label>Primary Tax Residency  主要稅務居地</label>
                        </div>
                        <p><textarea name="case[PrimaryTaxResidency]" class="form-control" rows="3"></textarea></p>
                    </div> 
                                                     
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="boxed-border text-center">
                            <label>Tax ID No. 稅務識別號</label>
                        </div>
                        <p><textarea class="form-control" name="case[TaxIdNumber]" rows="3" ></textarea></p>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <ul>
                            <li>The above information is true, complete and accurate; and further undertake to notify the Global Asset  promptly of any change in the aforementioned information and agree to submit additional documents upon its request. <br>以上所有資料屬實、完整及正確無誤，並且進一步承諾即時通知保薦人及受託人此資料的任何變化及同意提交受託人所需 要的額外文件。</li>
                        </ul>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="boxed-border">
                            <ul>
                                <li><sup>1</sup>Individuals: U.S. citizen or lawful permanent resident (green card holder); or born in the U.S.; or have a U.S. residential address or meeting the substantial presence test for the calendar year as defined by the IRS (www.irs.gov).</li>
                                <li>Corporations: a U.S. partnership or U.S. corporation </li>
                                <li>Trusts: Any trust if (a) a court within U.S. is able to exercise primary supervision over the administration of the trust; and (b) one or more U.S. persons have the authority to control all substantial decisions of the trust.</li>
                            </ul>
                            <p>就遵從美國《海外賬戶納稅法案》而言，“美籍人士”是指： </p>
                            <ul>
                                <li><sup>1</sup>個人：美國公民或合法永久居民(綠卡持有者)；或出生在美國；或者有美國的居住地址或滿足國稅局(www.irs.gov)  定義的實 質居住測試。 </li>
                                <li>企業：美國的合夥企業或美國公司。 </li>
                                <li>基金存款：任何基金存款，倘若(a)美國的法院可對基金存款的管理行使基本監管；及(b)一個或多個美籍人士有權 控制基金存款的所有重大決策。</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="boxed-border">
                            <p><sup>2</sup> Examples of Politically Exposed Persons include : </p>
                            <ul>
                                <li>Heads of State or Government </li>
                                <li>Senior Politicians</li>
                                <li>Senior Government, Judicial or Military Officials </li>
                                <li>Senior Executives of State-Owned Corporations</li>
                                <li>Important Political Party Officials </li>
                                <li>Royal Family </li>
                            </ul>
                            <br>
                            <p><sup>2</sup> 政治人物的例子包括： </p>
                            <ul>
                                <li>國家元首或政府首腦 </li>
                                <li>資深政治家 </li>
                                <li>高級政府、司法或軍事官員 </li>
                                <li>國有企業的高級管理人員 </li>
                                <li>重要政黨官員 </li>
                                <li>皇室</li>
                            </ul>
                        </div>
                    </div> 
                                                     
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="boxed-border">
                            <p><sup>3</sup> Examples of Closely Connected Persons include : </p>
                            <ul>
                                <li>Family Subscribers (spouse, children, parents, <br>brothers and sisters, including in-laws) </li>
                                <li>Aides and Other Close Advisors, socially or <br>professionally </li>
                                <li>Business associates</li>
                                <li>Companies in which a Senior Political Figure has an <br>interest or exercises influence </li>
                            </ul>
                            <br>
                            <p><sup>3</sup> 密切關連人士的例子包括：  </p>
                            <ul>
                                <li>家庭申購人(配偶、子女、父母、兄弟姐妹，包括 <br>姻親) </li>
                                <li>助手和其他貼身顧問(社交或專業) </li>
                                <li>業務夥伴 </li>
                                <li>高級政治人物擁有權益或影響力的公司</li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    
                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 12  Subscription Agreement -->
            <div class="section section-12">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        12. Subscription Agreement
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        申購協議
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>General Interpretation</strong></p>
                            <p>In this Subscription Agreement and the entire Account Opening Package the following terms shall have the following meanings: </p>
                            <ul>
                                <li>“Account   Opening   Package” means   this   entire   document, including the Subscription Agreement, and any supplementary or accompanying forms provided and filed by the Subscriber; </li>
                                <li>“Fees” means the amounts payable by a Subscriber pursuant to attached Fee Schedule, or any other fees that may be charged, which are subject to change from time to time; </li>
                                <li>“Subscriber” is a person  who  signs  this  Subscription  Form  and provides funds to the Global Asset to become a Subscriber of the Skylight product offered by the Global Asset. Any person who becomes a Subscriber must fulfil the conditions of Subscription as set out above in order to be available for settlements at the Global Asset  discretion; </li>
                                <li>“Approved Certifier” is a natural person of greater than the age of majority in their home jurisdiction, who has completed an Anti- Money Laundering (“AML”) and public records check or other background reviews at the discretion of the Global Asset, prior to having been approved by the Global Asset to certify documents for reliance upon the AML review by  Global Asset;</li>
                            </ul>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c content-box">
                            <p><strong>基本釋義</strong></p>
                            <p>在這申購協議和整個申購程序中，下列詞彙具有以下涵義： </p>
                            <ul>
                                <li>“申購程序”指全套的文件，包括申購人協議，及申購人 提交併入檔的任何補充文件或隨附表格;  </li>
                                <li>“費用”指產品申購人根據內附的標準需要交付的各項費 用，或隨產品不定期的變更而可能產生的其他費用;</li>
                                <li>“申購人”指簽署產品的申請者，並獲得公司接受。在公 司按情況制定的合理時間內，申購人必須遵守上述的產 品條件，方可獲得公司結算其投資回報; </li>
                                <li>“授權核證人”指一個在其所屬司法轄區大於法定年齡的 人，在得公司允許其負責核證文件之前，須先對其進行 反洗錢 (“AML”) 和公共記錄核實或其他背景調調查。</li>
                            </ul>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Subscriber’s Declaration to Global Asset</strong> </p>
                            <p>In this Subscription Agreement  and the following terms shall have the following meanings</p>

                            <ol type="a">
                                <li>I declare that to the extent that I have completed the Subscription Package, the information contained is accurate in every respect; </li>
                                <li>I declare that at the time I was given this Subscription Package, I was also given advice about the benefits to which I would be entitled as a Subscriber, the method of determining that entitlement and the conditions relating to those benefits;</li>
                                <li>I confirm to the Global Asset, that I have been advised by the Global Asset to take independent financial, legal and taxation advice on the Skylight product offered by the Global Asset, and that I have made such enquiries and taken such financial, legal,  taxation and other advice as I consider necessary concerning all possible implications concerning the proposed product</li>
                                <li>I acknowledge that Global Asset and/or her affiliates has not given me any tax advice concerning the proposed Skylight product on my circumstances or on the circumstances of any other person likely to be affiliated with or benefiting from the plan.</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>申購人向公司聲明 </strong></p>
                            <p>本申購書簽字人，已符合資格申請加入成為其產品的申購 人，現同意並做出如下承諾：</p>
                            <ol type="a">
                                <li>本人聲明，在申購程序填妥的內容裡，本人提供的資訊 在各方面都準確無訛; </li>
                                <li>本人聲明在收到此申購程序時，本人已得到相關建議， 內容包括本人將得到的利益，決定這些利益的方法，以 及這些利益的相關條款. </li>
                                <li>本人茲向公司確認，已經知悉公司建議，並就其所推薦 的產品計劃，獨立諮詢與之相關的金融、法律和稅務意 見，而本人亦就以上本人認為需要知道的其他關於該產 品計劃的可能含義和公司的託管情況作出詢問。</li>
                                <li>本人瞭解就所推薦的產品計劃或該計劃對於本人的影 響，或該計劃可能受惠於其他人的可能性，公司尚未向 本人提供與之相關的稅務意見。</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Global Asset’s Code of Practice</strong></p>
                            <ol type="a">
                                <li>Any decision by Global Asset pertaining to the operation of or administration of the Skylight product is final and the parties all hereby agree that policies, instructions, directions or orders made by the Global Asset must be followed rigorously and at all times.</li>
                                <li>Should any party wish to vary the policies, instructions, directions or orders made by the Global Asset, they may request such variance in writing and the approval of such variance shall be at the sole discretion of Global Asset.</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>公司守則</strong></p>
                            <ol type="a">
                                <li>有關產品的操作或管理，公司的任何決定均是最終決 定，各方均同意由公司制定的政策、指引、指令或命 令，均必須貫徹始終並嚴格執行。</li>
                                <li>如果任何一方希望改變公司制定的政策、指引、指令 或 命令，可以書面提出要求，而公司可以全權決定是否准 許。</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Diversification Waiver</strong></p>
                            <p>I have endorsed Global Asset election to participate in underlying investments or a longer-term investment strategy as proposed by the Global Asset and her investment partners. I therefore waive the need for diversification of investments by the Global Asset. </p>
                            <p>I accept that when Global Asset invests funds, I have provided:</p>
                            <ol type="a">
                                <li>no claim will be made by me against the Global Asset for any advice, representations, acts, omissions, or conduct of the Global Asset.</li>
                                <li>I agree to indemnify and keep the Global Asset indemnified from and against all claims, costs demands, liabilities, expenses, damages or losses, without limitation, arising out of or in connection with the choice of investment.</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>放棄分散投資 </strong></p>
                            <p>本人同意選擇參與由公司建議及批准的相關投資或長期投資 策略。因此，本人放棄了由公司管理的分散投資需要。</p>
                            <p>本人接受，當公司選擇投資時： </p>
                            <ol type="a">
                                <li>若因產品和公司的任何建議、代表、舉動、疏忽 或行為 而造成損失時，本人不會向公司索取賠償。 </li>
                                <li>本人同意賠償因投資選擇引起或與其相關的索償、包括 成本支出、 債務、費用、損害或損失。</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Investments</strong></p>
                            <p>I acknowledge that I am aware of the inherent risks that market values can go down as well as up; and that I shall make no claim on the Global Asset for carrying out my requests.</p>
                            <p>Global Asset has not provided me financial advice  and  has  made no warranty or representation in relation to any investment. Where I choose or have chosen an investment, including but not limited to specialised investment, I declare that:</p>
                            <ol type="a">
                                <li>l am sufficiently experienced to understand the features and risks associated with the investment and; </li>
                                <li>I have read and fully understood the offering document, including in particular the information on the risks and charges associated with investing in the investments contained in the offering document before deciding to invest into it and; </li>
                                <li>I personally accept all risk associated with the investment, and in particular that my investment in a specialised investment involves risk that could result in loss of a significant proportion or all of the sum involved; and; </li>
                                <li>I have taken independent advice on the suitability of the investments within this structure and;</li>
                                <li>Global Asset has not promoted the investment or provided any advice, made any recommendation or expressed any opinion whatsoever in respect of the performance risk, regulatory issues or security (including any express or implied guarantees) of the investment. </li>
                                <li>I agree to indemnify and keep Global Asset indemnified from and against all claims, costs demands, liabilities, expenses, damages or losses (including without limitation any consequential losses/loss of profit and loss of reputation, and all interest penalties and legal and other professional costs) arising out of or in connection with the choice of investment</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>投資 </strong></p>
                            <p>本人明瞭在本人提出要求公司指示財務顧問作出特定投資 時，本人知悉存在固有風險，市場有漲有跌，本人不應為公 司執行了本人的要求而向其索償。</p>
                            <p>公司並沒有向本人提供財務建議，亦未有就有關的任何投資 作出擔保。就本人已選擇的投資(包括但不限於專業投資)， 本人聲明： </p>
                            <ol type="a">
                                <li>本人有足夠經驗瞭解與該投資相關的特性和風險; </li>
                                <li>本人在決定投資該項目前，已閱讀並充分理解該銷售檔 案，尤其當中包括銷售檔案內關於投資各項目的風險和 費用的資訊; </li>
                                <li>本人接受有關此項投資的所有風險，尤其本人在專業投 資 所涉及的風險，有可能導致部分比例或全部投資的虧 損; </li>
                                <li>就本產品結構內的各項投資專案於個人是否合適，本人 已 聽取過獨立意見;</li>
                                <li>公司並沒有進行促銷或提供任何建議，亦沒有就投資的 績效風險、投資的監管問題或安全性(包括任何明示或暗 示的保證)表示任何意見. </li>
                                <li>本人同意賠償公司因投資選擇問題引起或與其相關的索 償、成本支出、債務、費用、損害或損失（包括但不限 於任何因此而受影響的損失/利潤損失，名譽損失，全部 利息罰款，法律及其他專業收費)。</li>
                            </ol>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Standard Fee Schedule</strong></p>
                            <p>By completing this Subscription, I agree to pay the following fee: </p>
                            <table class="table  ">
                                <tr style="background: #d4d4d4;color: #1d1d1d;">
                                    <td>Product Name</td>
                                    <td>Account Opening Fee</td>
                                    <td>AUA %</td>
                                </tr>
                                <tr>
                                    <td>Skylight Platinum (USD)</td>
                                    <td>USD 300</td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>Skylight Platinum (RMB) </td>
                                    <td>RMB 2,000 </td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>Skylight Performance (USD) </td>
                                    <td>USD 1,000 </td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>Skylight Performance (RMB) </td>
                                    <td>RMB 10,000 </td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>Skylight Private (USD)</td>
                                    <td>USD 1,000 </td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>Skylight Private (RMB) </td>
                                    <td>RMB 10,000 </td>
                                    <td>1</td>
                                </tr>
                            </table>
                            <br>
                            <p>Global Asset reserve the right to recover disbursements and out-of-pocket expenses, such as third party charges, direct from the plan. Additional third party charges may be incurred in administering the structure, in particular where the structure is complex or unique to the Subscriber’s requirements. Charges will be discussed on a case-by-case basis and such charges will normally be notified and agreed in advance.</p>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>標準規定收費</strong></p>
                            <p>通過填妥本申購書， 本人同意支付以下費用:</p>
                            <table class="table  ">
                                <tr style="background: #d4d4d4;color: #1d1d1d;">
                                    <td>產品 </td>
                                    <td>開戶費 </td>
                                    <td>管理費 % </td>
                                </tr>
                                <tr>
                                    <td>星光 白金系列 (美元)</td>
                                    <td>美元 300 </td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>星光 白金系列 (人民幣) </td>
                                    <td>人民幣 2,000 </td>
                                    <td>2</td>
                                </tr>
                                <tr>
                                    <td>星光 高效系列 (美元) </td>
                                    <td>美元 1,000 </td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>星光 高效系列 (人民幣) </td>
                                    <td>人民幣 10,000 </td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>星光 尊爵系列 (美元) </td>
                                    <td>美元 1,000 </td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td>星光 尊爵系列 (人民幣) </td>
                                    <td>人民幣 10,000 </td>
                                    <td>1</td>
                                </tr>
                            </table>
                            <br>
                            <p>公司保留可追回已發放款項與實際支出（如直接從計畫衍生的 第三方收費）的權利。尤其若產品結構複雜或按申購人要求而 獨有，額外的第三方收費可能會因管理該產品而產生。各項收 費將根據個別具體情況而訂，這些費用通常會事先通知並協商 金額後確認收費。</p>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Authority for Disclosure of Personal Data</strong></p>
                            <p>I hereby consent for Global Asset to receive all necessary information on my asset base globally as it required for the purposes of “Know Your Client”.</p>
                            <p>I understand that my authority is required under the Personal Data (Privacy) Ordinance and I also understand that this information will not be disclosed outside of the legal requirements to any person, company or any institution without my further written and signed authority for the time being and as amended, varied, supplemented, substituted or novated from time to time.</p>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>授權披露個人資料</strong></p>
                            <p>為達到“瞭解客戶”，本人同意公司可於全球範圍內收到本人資 產基數的所有必要資訊。</p>
                            <p>本人明白“個人資料（私隱）條例”，必須本人授權方可披露本 人的個人資料，本人也明白，在未經本人進一步書面簽署授權 下，本人的資料不會披露予基金存款公司的基金存款要求以外 的任何人士、公司或機構。</p>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Cooling-off Period Waiver </strong></p>
                            <p>There is a 14 day cooling-off period from the date of this Subscription: during this period, the Subscription will be on hold, and Setup and first year Management Fees can be refunded, net of a 30% cancellation fee. As Global Asset cannot commence setup until after the Cooling-off Period, if the Subscription is time-sensitive, the Subscriber may, by initialing here, waive the Cooling-off Period. In that event, set-up of the Skylight product will commence upon receipt of the Subscription. After the Cooling-off Period, or in the event that the Cooling-off Period is waived, fees shall be non-refundable.</p>
                            <br>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>放棄冷靜期</strong></p>
                            <p>申購人申請之日起有14天的冷靜期：在此期間申請將被 擱置，若決定退出計畫，基金存款帳號開戶費和首年的 管理費，除30%的手續費用後可獲退還。由於直到冷靜期過 後，Global Asset 才能開始進行相關作業，所以申購人可以 在此簽字放棄冷靜期。在這種情況下，相关运作將在收到申 請後隨即開始。在冷靜期後或在申月月人放棄冷靜期的情況 下，上述費用將不會退還。</p>
                            <br>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 13  Subscriber’s Banking Information -->
            <div class="section section-13">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        13. Subscriber’s Banking Information
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        申請人銀行帳戶資料
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-12 col-xs-12 form-group">
                        <p><label>收款銀行地址 Bank Address</label></p>
                        <p><textarea rows="3" id="BankAddress" name="bank[Address]" class="form-control" value="" ></textarea></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>收款銀行名稱 Bank Name</label></p>
                        <p><input type="text" id="BankName" name="bank[Name]" class="form-control" value="" /></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶持有人姓名 Account Name</label></p>
                        <p><input type="text" id="BankAccountName" name="bank[AccountName]" class="form-control" value="" /></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>帳戶號碼 Account Number</label></p>
                        <p><input type="text" id="BankAccountNumber" name="bank[AccountNumber]" class="form-control" value="" /></p>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <p><label>銀行國際代碼/城市代碼 SWIFT Code</label></p>
                        <p><input type="text" id="SwiftCode" name="bank[SwiftCode]" class="form-control" value="" /></p>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 14  Banking Information -->
            <div class="section section-14">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        14. Banking Information
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        銀行資料
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Singapore Banking Coordinates 新加坡銀行賬號</strong></p>
                            <table class="bank-table table">
                                <tr>
                                    <td><div>Account Name <br>帳戶名稱</div></td>
                                    <td>Global Asset Inc Limited </td>
                                </tr>
                                <tr>
                                    <td><div>Account Number <br>帳戶號碼 </div></td>
                                    <td>0003-024576-01-9 (USD 美元) <br>003-931323-3 (SGD 新幣) </td>
                                </tr>
                                <tr>
                                    <td><div>Bank Name <br>銀行名稱 </div></td>
                                    <td>DBS Bank Singapore</td>
                                </tr>
                                <tr>
                                    <td>Swift Code <br>銀行國際代碼 </td>
                                    <td>DBSSSGSG</td>
                                </tr>
                                <tr>
                                    <td>Bank Address <br>銀行地址 </td>
                                    <td>12 Marina Boulevard, Level 3 <br>Marina Bay Financial Centre Tower 3 <br>Singapore 018982</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c">
                            <p><strong>Hong Kong Banking Coordinates 香港銀行賬號</strong></p>
                            <table class="bank-table table">
                                <tr>
                                    <td><div>Account Name <br>帳戶名稱</div></td>
                                    <td>Global Asia Pacific Corporate Limited </td>
                                </tr>
                                <tr>
                                    <td><div>Account Number <br>帳戶號碼 </div></td>
                                    <td>642060990838 (USD 美元 / HKD 港幣) </td>
                                </tr>
                                <tr>
                                    <td><div>Bank Name <br>銀行名稱 </div></td>
                                    <td>DHSBC Hong Kong </td>
                                </tr>
                                <tr>
                                    <td>Swift Code <br>銀行國際代碼 </td>
                                    <td>HSBCHKHHHKH</td>
                                </tr>
                                <tr>
                                    <td>Bank Address <br>銀行地址 </td>
                                    <td>1 Queen’s Road Central <br>Hong Kong</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 15 Contribution Methods -->
            <div class="section section-15">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        15. PRODUCT SELECTION
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        產品選擇
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body">
                    <!-- <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <p class="hidden-xs"><img src="<?php echo view::url(); ?>/assets/images/qr.jpg"> <span style="display: inline-block; font-size: 20px; color: #78664a; position: absolute; top: 40%; margin: 0 0 0 40px;">信託存款帳戶資訊 <br><br><a href="http://portal.gaskylight.com" style="color: #78664a; text-decoration: none;">http://portal.gaskylight.com</a></span></p>
                        <div class="visible-xs text-center">
                            <p><img src="<?php echo view::url(); ?>/assets/images/qr.jpg" class="img-responsive" style="margin: 0 auto;"></p>
                            <p><span style="display: inline-block; font-size: 20px; color: #78664a;">信託存款帳戶資訊 <br><br><a href="http://portal.gaskylight.com" style="color: #78664a; text-decoration: none;">http://portal.gaskylight.com</a></span></p>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <p><label>Package Type | 購買商品系列(請選擇)</label></p>
                            <select name="acct[ProductID]" id="ProductID" class="form-control" rel="<?php echo View::url('form/get/'); ?>">
                                <option selected>Select Option</option>
                                <?php
                                foreach ($prods as $prod) {
                                ?>
                                    <option value="<?php echo $prod->ProductID; ?>"><?php echo $prod->ProductName; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <p><label>Package Terms | 配套條款</label></p>
                            <select id="ProductItemID" class="form-control" rel="<?php echo View::url('form/getPrItem/'); ?>">
                                <option selected="selected">Select</option>
                                
                            </select>
                            <input type="hidden" id="theProductItemID" name="acct[ProductItemID]" value="">
                            <input type="hidden" id="StepUp" value="">
                            <input type="hidden" id="Invested" value="">
                            <p id="couponAmount" class="help-block" style="margin-bottom: 0;"></p>
                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <p><label>Final Amount | 投資金額</label></p>
                            <input type="text" name="acct[DepositedAmount]" class="form-control" id="DepositedAmount">
                        </div>

                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

            <!-- Section 18 Additional Information -->
            <div class="section section-18">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        16. Additional Information
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        申購協議
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row" style="font-size: 12px;">
                    <div class="col-sm-6 col-xs-12">
                        <div class="left-c content-box">
                            <p><strong>Privacy</strong></p>
                            <p>Global Asset respects your privacy and will not share your private details with any third party without obtaining prior approval. Third parties may include but not limited to: advisors, auditors, custodians, administrators and legal entities. </p>

                            <p>If investors wish to authorise parties such as private consultants, financial advisors, brokers or solicitors to act on their behalf, please inform Global Asset in formal writing beforehand so that Global Asset may be authorised to share your personal details and investment reports with external parties.</p>
                            <br>
                            <p><strong>Anti-Money Laundering</strong></p>
                            <p>In order to comply with current or future regulations aimed at the prevention of money laundering and to counter terrorism, Global Asset and its affiliates may require you to provide verification of your identity or that of the underlying beneficial owner(s), and/or the source of the fund being invested. </p>
                            <p>We may refuse to accept a Subscription of a prospective client who delays or fails to produce any information we request for verification purposes. </p>
                            <p>Global Asset has a comprehensive Anti-Money Laundering program in place and conduct world check on every prospective client.</p>

                            <p><strong>Complaints</strong></p>
                            <p>If a client submits to the Representative a complaint (whether it be written or verbal) alleging that he/ she has been adversely affected by the Representative’s conduct in its management or administration of the Trust, Global Asset:</p>
                            <ol type="a">
                                <li>must, if the complaint is in writing, acknowledge in writing receipt of the complaint as soon as practicable and in any event within 14 days from receipt;</li>
                                <li>must ensure that the complaint receives proper consideration resulting in a determination by a person or body designated by Global Asset as appropriate to handle complaints; </li>
                                <li>where the complaint relates to an error which is capable of being corrected without affecting the rights of third parties, act in good faith to deal with the complaint by endeavouring to correct the error; </li>
                                <li>may in its discretion give any of the following remedies to the complainant: <br>
                                    <ol type="number">
                                        <li>information and explanation regarding the circumstances giving rise to the complaint; </li>
                                        <li>an apology; or </li>
                                        <li>compensation for loss incurred by the client as a direct result of the breach (if any); and </li>
                                    </ol>
                                </li>
                                <li>must communicate to the complainant as soon as practicable and in any event not more than 45 days after receipt by Global Asset of the complaint: <br>
                                    <ol type="number">
                                        <li>the determination in relation to the complaint; </li>
                                        <li>the remedies (if any) available to the client</li>
                                    </ol>
                                </li>
                            </ol>
                            <br>

                            <p><strong>FATCA</strong></p>
                            <ul>
                                <li>Clients to provide certain information If requested by Global Asset, the Client agrees to provide certain required information in order to comply with any applicable law, including the United States Foreign Account Tax Compliance Act (FATCA). </li>
                            </ul>
                            <p><strong>Risks</strong></p>
                            <p>Global Asset does not guarantee the performance, return or liquidity of the SPV. Investors should carefully consider their tolerance for risk, intended investment horizon and other financial commitments before investing. The return of capital and the performance of the SPV are not guaranteed by any person or organisation, including Global Asset or the Prime Broker. Some key risks of investing in the SPV include, but are not limited to: </p>

                            <ul>
                                <li>Liquidity <p>There may be occasions where the investment may not be sold in the market. In this case, the lack of liquidity will affect the value of the SPV as it will be quoted at market prices. However, Global Asset will endeavour to mitigate this risk by maintaining sufficient cash margins to meet reasonable liquidity requirements.</p></li>
                                <li>Market or Sector Risk  <p>Market risk is characterised by macroeconomic fundamentals such as inflation rates, interest rates and political and environmental climates. Global Asset offers no guarantee that the SPV will accrue losses greater than or equal to leading market indices in the event of adverse shifts in the financial markets. There will also be inherent risk of investments in a particular industry that is facing unforeseen headwinds. </p></li>
                                <li>Counter-party Risk <p>Counter-party risk refers to the risk if a third party should fail to meet their financial obligations to the SPV. In such circumstances, the SPV may accrue substantial losses. Global Asset endeavours to mitigate this risk by carefully selecting investment opportunities that offer minimal risk and high return.</p></li>
                                <li>Exchange Rate Risk <p>A proportion of the SPV may be invested in opportunities that are significantly exposed to changes in the underlying exchange rate. Therefore, the return on the investment may sometimes be contingent on exchange rate risk. Foreign investors should also be wary of the exchange rate risk that accompanies their investment. </p></li>
                                <li>Diversification Risk <p>The SPV will seek to minimise risk and maximise returns where possible. However, the nature of the SPV demands that investment becomes concentrated within selective stocks. As a result, the SPV is riskier than its diversified counterparts. </p></li>
                                <li>Speculative Risk <p>Certain investments by the SPV may be regarded as speculative in nature and involve increased levels of investment risk. An inherent part of an investment strategy may be to identify securities which are undervalued by the marketplace. Success of such a strategy necessarily depends upon the market eventually recognising such value in the price of the security, which may not necessarily occur. Equity positions, including initial public offerings, may involve highly speculative securities.</p></li>
                            </ul>
                            
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="right-c content-box">
                            <ul>
                                <li><p>Stock Specific Risk <br>There is unsystematic risk contained in every investment. This is due to business specific factors. We aim to reduce this by diversification</p></li>
                                <li><p>Regulatory Risk <br>This includes potential changes in the law or the way the law is interpreted. This may include positive and negative taxation outcomes.</p></li>
                            </ul>
                            <br>
                            <p><strong>Subscription Process and Cut-off Times </strong></p>
                            <ul>
                                <li>Subscriptions can be made by completing the attached Subscription Agreement and forwarding it to Global Asset.</li>
                                <li>Upon complete submission of required documents and cleared fund receiving (by every Friday end of business day), the Commencement will be on the following Monday. </li>
                                <li>The original signed Subscription Agreement must be received by Global Asset and cleared funds must be electronically transferred into the Application Account. Any applications received after this cut-off will be processed at the next Dealing Monday. </li>
                                <li>A copy of the original executed and completed Subscription Agreement may be emailed to Accounts@GASkylight.com, but no Subscription Agreement will be processed until Global Asset has received a properly completed original and cleared subscription monies. </li>
                                <li>Early submission of applications is recommended to ensure the deadlines are met, as applications received after these cut off times may not be processed until the next Dealing Day</li>
                            </ul>

                            <p><strong>Key Dates </strong></p>
                            <ul>
                                <li>Weekly Subscription and Fund Clearing <br>Monday to Friday, by Friday end of business day</li>
                                <li>Weekly Commencement <br>The following Monday</li>
                                <li>Expected Dispatchment of Statement & Investment Report <br>1st Week of January & 1st Week of July </li>
                            </ul>
                            <br>

                            <p><strong>Expenses</strong></p>
                            <p>All expenses incurred in connection with the SPV are payable or reimbursable out of the Subscriptions. This includes the expenses connected with the following:</p>
                            <ol type="a">
                                <li>the Constitution, the formation of the SPV; </li>
                                <li>the preparation, review, distribution and promotion of any information memorandum or other promotion of the SPV; </li>
                                <li>the acquisition (including due diligence costs), disposal, insurance, custody (including custodian fees) and any other dealing with Assets, including commissions, whether charged as a fee or as a buy-sell differential in the price of an Asset; </li>
                                <li>any proposed acquisition, disposal or other dealing with an investment; </li>
                                <li>underwriting of any subscription, including underwriting fees, handling fees, costs and expenses, amounts payable under indemnity or reimbursement provisions in the underwriting agreement and any amounts becoming payable in respect of any breach (other than for negligence, fraud or breach of duty) by the SPV of its obligations, representations or warranties under any such underwriting agreement;</li>
                                <li>borrowing arrangements on behalf of the SPV or guarantees in connection with the SPV, including loan arranging costs, hedging costs, and costs relating to interest rate swaps; </li>
                                <li>the administration or management of the SPV or its Assets and Liabilities, including expenses in connection with the Register, valuation of any Asset or the Assets, and information technology systems and accounting; </li>
                                <li>tax and financial institution fees; </li>
                                <li>the engagement of agents (including real estate agents and managing agents), valuers, contractors and advisers (including legal advisers) whether or not the agents, valuers, contractors or advisers are associates of the SPV; </li>
                                <li>in connection with any real property in which the SPV has a direct or indirect interest, rates, development, insurance and redevelopment costs, insurance broking and quantity surveyor’s fees, subdivision and building costs, normal building operating expenses not paid by tenants, costs of leasing (including marketing) and leasing incentives; </li>
                                <li>accounting and compliance with taxation laws and procedures (whether internal expenses of the SPV or paid to third parties) and the preparation and audit of the taxation returns and accounts of the SPV; </li>
                                <li>all damages, expenses, payments, legal and other costs and disbursements incurred by the SPV in relation to or in connection with any claim, dispute or litigation (Claim) arising as a result of or in connection with any untrue representation or warranty contained in any document relating to any investment by the SPV (including any project document in connection with the investment) and any offering document or borrowing document in connection with the SPV; </li>
                                <li>the cost of handling complaints from unit holders and resolving disputes with them </li>
                                <li>all communications with and methods of redemption offered to unit holders including without limitation providing reports and statements, providing telephone enquiry services, providing online services and internet banking and similar services to the extent that the cost of providing such services is not recovered by the SPV through charging other fees provided for in this constitution; </li>
                                <li>the preparation, implementation, amendment and audit of the compliance plan; </li>
                                <li>the cost of the SPV employing a compliance officer to carry out compliance duties under the compliance plan, in so far as the allocation of their time is attributable to matters connected with the SPV; </li>
                                <li>the preparation of reports including compliance reports; </li>
                                <li>recording, responding to and dealing with any complaints from subscribers in connection with the SPV; and </li>
                                <li>complying with any law, and any request or requirement. </li>
                            </ol>
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
                <div class="clearfix"></div>
            </div>
            
            <!-- Section 19. CHECKED BY APPROVED CERTIFIER -->
            <div class="section section-19">
                <div class="s-header">
                    <div class="left col-sm-6 col-xs-6">
                        17. CHECKED BY APPROVED CERTIFIER  
                    </div>
                    <div class="right col-sm-6 col-xs-6">
                        由適用簽署人見證核實審查
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="panel-body row ">
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Name 姓名</label></p>
                            <p><input type="text" name="acct[ApprovedBy]" class="form-control box-border"></p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12 form-group">
                        <div class="left-c content-box">
                            <p><label>Date (YYYY/MM/DD)  日期（日/月/年）</label></p>
                            <p><input type="text" name="acct[ApprovedDate]" class="form-control js-datepicker box-border"></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <br>
            <div style="text-align: center;">
                <p style="color:red;"><input style="margin-top:-3px;"type="checkbox" id="TermsAgree" name="TermsAgree" value="Y" /> <label for="TermsAgree"><strong>我/我們閱讀並理解上面的內容 | I/We Read and Understood the above.</strong></label> </p>
                <button type="submit" class="btn">Submit | 提交</button>
            </div>

        </div>
        </form>

    </div>
    <!-- /page content -->
<?php View::footer('forms'); ?>