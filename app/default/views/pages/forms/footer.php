            <!-- footer content -->
            <footer>
                <div class="container">
                    <p><span class="text-muted">Copyright © <?php echo date('Y'); ?> <a href="https://portal.gaskylight.com">Skylight & Co. Pte Ltd.</a> All rights reserved</span> <span class="text-muted pull-right"><a href="#">Terms of Use</a></span></p>
                </div>
                <div class="clearfix"></div>
            </footer>
            <!-- /footer content -->
        </div>
        <?php View::footers(); ?>
    </body>
</html>