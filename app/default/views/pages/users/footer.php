        <!-- Slide Up Modal -->
	    <div class="modal fade" id="emailcheck-modal-slideup" tabindex="-1" role="dialog" aria-hidden="true">
	        <div class="modal-dialog modal-dialog-slideup">
	            <div class="modal-content">
	                <div class="block block-themed block-transparent remove-margin-b">
	                    <div class="block-header bg-primary-dark">
	                        <ul class="block-options">
	                            <li>
	                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
	                            </li>
	                        </ul>
	                        <h3 class="block-title">Notice</h3>
	                    </div>
	                    <div class="block-content"></div>
	                </div>
	                <div class="modal-footer">
	                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
	                    <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- END Slide Up Modal -->
        <?php View::footers(); ?>
    </body>
</html>