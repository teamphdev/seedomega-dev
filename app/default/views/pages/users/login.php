<?php 
View::$title = 'User Login';
View::$bodyclass = 'loginpage';
View::header(); 
?>

<!-- ************************ Page Content ************************ -->
 <section class="gray">
     <article class="container project-single">
         
         
         <div class="block block-themed">
             <!--div class="block-header bg-primary">
                 <h3 class="block-title"><?php //echo Lang::get('LOGIN_SUBTITLE'); ?>&nbsp;</h3>
             </div-->
             <div class="centered max600">
                <div class="block-content">
                    <form method="post">
                     <input type="hidden" name="action" value="login" />
                     <?php echo View::getMessage(); ?>
                        <div class="form-group text-center">
                            <p>&nbsp;</p>
                            <h3 class="push-10">Welcome to SeedOmega!</h3>
                            <p>Please login below using your email and password.</p>
                            <p>&nbsp;</p>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                 <input type="text" class="form-control" placeholder="<?php echo Lang::get('LOGIN_USERLABEL'); ?>" name="usr" required />
                             </div>
                        </div>

                        <div class="form-group">                            
                           <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                              <input type="password" class="form-control" placeholder="<?php echo Lang::get('LOGIN_PASSLABEL'); ?>" name="pwd" required />
                           </div>
                        </div>
                        
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-4 green default submit font-18"><?php echo Lang::get('LOGIN_BUTTON'); ?></button>
                        </div>

                        <div class="clearfix"></div>
                        <div class="divider push-10-t"><span>OR</span></div>
                        
                        <a href="<?php echo App::fbUrl(); ?>" class="btn btn-block btn-fb push-10 social" type="button"><i class="fa fa-facebook pull-left"></i> Signin with Facebook</a>
                        <a href="<?php echo App::gAuthURL();?>" class="btn btn-block btn-gplus push-10 social"><i class="fa fa-google-plus pull-left"></i> Signin with Google+</a>

                        <div class="form-group">
                          <div class="push-20-t">
                              <a class="link" href="<?php echo View::url('users/requestresetpassword') ?>"><?php echo Lang::get('LOGIN_RESET'); ?></a>
                                <br><br>
                                  <a class="link" href="<?php echo View::url('join/100000') ?>">New to SeedOmega? <span class="text-gray">Signup</span></a>
                           </div>
                        </div>

                        <div class="clearfix"></div>


                    </form>
                </div>
             </div>
         </div>

     </article>
 </section>

<?php View::footer(); ?>