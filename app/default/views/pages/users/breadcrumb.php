<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><?php echo View::$title; ?></li>
          </ul>
        </div>
      </div>
    </article>
</section>
