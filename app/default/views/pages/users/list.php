<?php 
    View::$title = 'Manage Users';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo View::url(); ?>">Home</a></li>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
                    <?php if( isset(View::$segments[1]) ) { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-6 align-right">
            </div>
        </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage();  ?> 
                <table class="table table-divide js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" width="100%" addbutton='<a href="<?php echo View::url('users/add'); ?>" class="btn btn-rounded btn-success push-10-r" data-toggle="tooltip" title="Add User"><i class="si si-user-follow"></i> Add User</a><a href="<?php echo View::url('users/trashbin'); ?>" class="btn btn-rounded btn-warning" data-toggle="tooltip" title="Trash Bin"><i class="si si-trash"></i> Trash Bin</a>'>
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-ceter">Photo</th>
                            <th class="sort-this"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_EML'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_GNDR'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_PHNE'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_LVL'); ?></th>
                            <th class="no-sorting text-center" style="width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if( count( $users ) ){
                            foreach( $users as $user ){ $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td class="text-center">
                                <div class="profile-image">
                                    <?php echo $user->Data['AvatarLink']; ?>
                                </div>
                            </td>
                            <td><?php echo $user->Code; ?>-<?php echo $user->UserID; ?></td>
                            <td><?php echo $user->Email; ?></td>
                            <td><?php echo $user->LastName; ?><?php echo ', '.$user->FirstName; ?></td>
                            <td><?php echo $user->Gender; ?></td>
                            <td><?php echo $user->Phone; ?></td>
                            <td><?php echo $user->Name; ?></td>
                            <td class="text-center">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'users/edit/'.$user->UserID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'users/trash/'.$user->UserID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to put user <?php echo $user->FirstName; ?> <?php echo $user->LastName; ?> to trash bin?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } 
                            } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="8">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>