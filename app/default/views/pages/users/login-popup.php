<form method="post" action="<?php echo View::url('users/login'); ?>">
    <input type="hidden" name="action" value="login" />
    <div class="form-group">
        <h3>Sign In</h3>                     

        <input type="text" class="form-control" placeholder="<?php echo Lang::get('LOGIN_USERLABEL'); ?>" name="usr" required="" />
        <input type="password" class="form-control" placeholder="<?php echo Lang::get('LOGIN_PASSLABEL'); ?>" name="pwd" required="" />
        <div class="form-group push-10-t text-center">
            <button type="submit" class="btn btn-4 green default submit">Sign In</button>
        </div>
        <div class="divider"><span>OR</span></div>
        <button class="btn btn-block btn-fb push-10 social" type="button" onclick="window.location='<?php echo App::fbUrl(); ?>'"><i class="fa fa-facebook pull-left"></i> Login with Facebook</button>
        <button class="btn btn-block btn-gplus push-10 social" type="button" onclick="window.location='<?php echo App::gAuthURL(); ?>'"><i class="fa fa-google-plus pull-left"></i> Login with Google+</button>
    </div>

    <hr><br>
    <a class="link" href="<?php echo View::url('users/requestresetpassword') ?>" onclick="window.location = '<?php echo View::url('users/requestresetpassword') ?>'"><?php echo Lang::get('LOGIN_RESET'); ?></a>
    <br>
    <a class="link" href="<?php echo View::url('join/100000') ?>" onclick="window.location = '<?php echo View::url('join/100000') ?>'">New to SeedOmega? <span class="text-gray">signup</span></a>
</form>