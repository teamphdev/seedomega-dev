<?php
View::$title = 'Dashboard';
View::$bodyclass = '';
View::inlineCss( '.header-bottom', 'background:url('.View::asset('images/singapore-banner.jpg').');' );
View::header();
?>
    <section class="header-bottom">
        <article>
            <div class="container animated fadeInDown">
                <h1>Welcome Seeder!</h1>
                <p>SeedOmega makes investing affordable, easy and fun. Until recently only the very wealthy and venture capitalists could get involved in capital raising. Now through SeedOmega, anyone can invest in startup businesses that have exciting growth potential.</p>
            </div>
        </article>
    </section>

    <!-- Full Width Project Grid start -->
    <section>
        <article class="container projects-page" id="">

            <?php echo View::getMessage(); ?>

            <!-- Projects Grid start -->
            <div class="row">
                <div class="col-lg-2 animated slideInLeft">
                    <div class="sw-title">
                        Categories
                    </div>
                    <div class="sw-content">
                        <ul class="sw-list">
                            <li class="active"><a href="javascript:void()" class="categories-filt" data-filter="*">All Categories</a></li>
                            <?php foreach (Apputility::getClientCategories() as $clientcat) { ?>
                            <li><a href="javascript:void()" class="categories-filt" data-filter=".ccatid-<?php echo $clientcat->ClientCatID; ?>"><?php echo ucwords($clientcat->CCatName); ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>                    
                </div>
                <div class="col-lg-10 col-xs-12 content-cnt animated slideInUp">
                    <!-- Filter & Sorting Form start -->
                    <form action="" class="dataTables_wrapper form-inline text-right push-20" method="get">
                        <div class="form-group">
                            <label for="">Sort By</label>
                            <select id="projsortby" class="form-control userdash-select arrow-down">
                                <option value="*">-</option>
                                <option value="sort|topfunded">Top Funded</option>
                                <option value="sort|lowfunded">Low Funded</option>
                                <option value="sort|dateupdated">Recently Added</option>
                            </select>
                        </div>
                    </form>
                    <div class="clear"></div>
                    <!-- Filter & Sorting Form end -->

                    <div class="project-grid row">

                        <?php 
                        $cntr = 0;
                        foreach( $projects as $proj ): $cntr++ ?>

                            <div class="col-lg-4 col-md-6 col-sm-3 col-xs-12 project-cnt ccatid-<?php echo $proj->ClientCatID; ?> <?php echo $proj->Data['ProjectClass']; ?> sortcatid-<?php echo $proj->ClientCatID; ?>">
                                <div class="project-item ribbon ribbon-left">                                

                                    <?php if ($proj->DaysToOpen > 0) : ?>
                                    <div class="ribbon-box pink">UPCOMING</div>
                                    <?php endif; ?>

                                    <?php if ($proj->Data['Percentage'] >= 100) : ?>
                                    <div class="ribbon-box blue">SUCCESSFUL</div>
                                    <?php endif; ?>

                                    <div class="percent-funded" style="display: none;"><?php echo $proj->Data['Percentage']; ?></div>
                                    <div class="date-updated" style="display: none;"><?php echo date( 'Y-m-d', strtotime( $proj->UpdatedAt ) ); ?></div>

                                    <div class="ft-img">
                                        <?php echo $proj->Data['CompanyPhotoLink']; ?>
                                        <figcaption><a href="/project/view/<?php echo $proj->ClientProfileID;?>">View Details</a></figcaption>
                                    </div>
                                    <div class="project-content">
                                        <h5><a href="/project/view/<?php echo $proj->ClientProfileID;?>"><?php echo $proj->CompanyName; ?></a></h5>
                                        <p class="project-text"><?php echo isset($proj->OfferOverview) ? AppUtility::excerptText( $proj->OfferOverview, 150, ' <a href="/project/view/'.$proj->ClientProfileID.'" class="item_button1">...read more</a>' ) : 'No Description'; ?></p>

                                        <div class="project-progress">
                                            <div class="project-data row">                             
                                                <div class="col-lg-6">
                                                    <div class="pie_progress" role="progressbar" data-goal="<?php echo $proj->Data['Percentage']; ?>" data-barcolor="#7390d6" data-barsize="5" data-size="96">
                                                      <?php echo $proj->Data['CompanyLogoLink']; ?>
                                                    </div>                                
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="progress_number"><?php echo $proj->Data['Percentage']; ?>%</div>
                                                    <div class="progress_label">Completed</div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="project-summary">
                                            <div class="">
                                                <div class="col-lg-6 col-xs-6 no-padding daysback">
                                                    <span><?php echo isset( $proj->DaysLeft ) ? number_format( $proj->DaysLeft ) : "0"; ?></span> DAYS LEFT
                                                </div>
                                                <div class="col-lg-6 col-xs-6 no-padding text-right daysback">
                                                    <span><?php echo isset( $proj->TotalInvestor ) ? number_format( $proj->TotalInvestor ) : "0"; ?></span> BACKERS
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="text-center project-funded">
                                                <span><?php echo isset( $proj->TotalRaisedAmt ) ? "$".number_format( $proj->TotalRaisedAmt ) : "0"; ?></span><br>
                                                Funded
                                            </div>
                                        </div>

                                        <div class="clear"></div>
                                        <div class="project-btn">
                                            <?php 
                                            if( $enabled || Option::get('global_promo') == 1 ){ echo $proj->Data['BookHTML']; ?>
                                            <?php } else { ?>
                                                <a href="#" class="btn btn-success item_button2 nonverified" data-toggle="modal" data-target="#myModal" style="cursor: not-allowed;"><?php echo $proj->Data['BookButton']; ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php 
                        endforeach; ?>                                                
                    </div>
                </div>
            </div>
            <!-- Projects Grid end -->

            <div class="clear"></div>

            <?php if( $incomplete ){ ?>
                <div id="dialog" class="w3-modal">
                    <div class="w3-modal-content" style="width: 600px;">
                        <div id="dialog-form" class="text-center">
                            <button type="button" class="close" aria-label="Close" onclick="document.getElementById('dialog').style.display='none'">
                                <i class="si si-close text-red"></i>
                            </button>
                            <hr>
                            <div class="modal-body heading"><h3>Please complete your profile!</h3></div>
                            <a href="/users/profile" class="btn btn-3 blue">
                                <i class="fa fa-pencil text-white"></i>  Complete Profile
                            </a>&nbsp;
                            <a href="/project/view/<?php echo $proj->ClientProfileID;?>" class="btn btn-3 green">
                                <i class="si si-eye text-white"></i>  View
                            </a>
                            <div class="text-reminder text-white">You are 1 step away to get started</div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </article>
    </section>
    <!-- Full Width Project Grid end -->

    <!-- Recently Viewed Project Grid start -->
    <section class="parallax events singal">
        <article class="container recentview-box">
            <h3>Recently Viewed Projects</h3>
            <div class="row">                
                <?php foreach( $recentview as $project ): ?>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 project-cnt">

                        <div class="project-item ribbon ribbon-left">                                

                            <div class="ribbon-box pink"><?php echo isset( $project->TypeOfOffer ) ? $project->TypeOfOffer : "";?></div>

                            <div class="percent-funded" style="display: none;"><?php echo $project->Data['Percentage']; ?></div>
                            <div class="date-updated" style="display: none;"><?php echo date( 'Y-m-d', strtotime( $proj->UpdatedAt ) ); ?></div>

                            <div class="ft-img">
                                <?php echo $project->Data['CompanyPhotoLink']; ?>
                                <figcaption><a href="/project/view/<?php echo $proj->ClientProfileID;?>">View Details</a></figcaption>
                            </div>
                            <div class="project-content">
                                <h5><a class="recent" href="/project/view/<?php echo $proj->ClientProfileID;?>"><?php echo $project->CompanyName; ?></a></h5>
                                <p class="project-text-recent"><?php echo isset($project->OfferOverview) ? AppUtility::excerptText( $project->OfferOverview, 90, ' <a href="/project/view/'.$project->ClientProfileID.'" class="item_button1">...read more</a>' ) : 'No Description'; ?></p>
                                
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            </div>
        </article>
    </section>
    <!-- Recently Viewed Project Grid end -->

<?php View::footer(); ?>
<script type="text/javascript">
    $( document ).ready( function(){

        $('.nonverified').click(function(){
            var refTop, diaHeight;
            $('#dialog').css('display', 'block');
        });

        // init Isotope
        var $grid = $('.project-grid').isotope({
            itemSelector: '.project-cnt',
            layoutMode: 'fitRows',
            getSortData: {
              topfunded: '.percent-funded parseInt',
              lowfunded: '.percent-funded parseInt',
              dateupdated : '.date-updated parseInt'
            }
        });

        // bind filter button click
        $('#categories').change(function() {
            var filterValue = $( this ).val();

            // use filterFn if matches value

           // $grid.isotope({ filter: filterValue });
        });

        $('.categories-filt').click(function(){
            var datacat = $(this).attr('data-filter');
            $('.categories-filt').parent('li').removeClass('active');
            $(this).parent('li').addClass('active');
            $grid.isotope({ filter: datacat });
        })

        $('#projsortby').on('change', function() {

            var $optvalue = $(this).val();
            var $vals = $optvalue.split('|');
            var filterByValue = $vals[1];

            if($vals[0] == 'sort'){
                if($vals[1] == 'lowfunded'){ 
                    $sorting = true; 
                }else{ 
                    $sorting = false; 
                }
              $grid.isotope({ sortBy: filterByValue, sortAscending: $sorting });
            }else{
              $grid.isotope({ filter: filterByValue });
            }
        });

    });
</script>

<?php /*
<script>
    // THIS IS A TUTORIAL SCRIPT
    // diaHeight = $('#dialog').height();
    // refTop = $('#popular').offset().top;
    // $('#dialog').css('padding-top', (refTop - 85) + 'px');
    // $('#myModal').css('padding-top', (refTop / 2) + 'px');


    var tours = {
        autoStart: true,
        data: [
            {
                element: '.project-grid',
                tooltip: 'Projects',
                position: 'T',
                text: '<h5 class="push-20">This is all the company available to your dashboard</h5>',                
                controlsPosition: 'TR'
            },{
                element: '.recentview-box',
                tooltip: 'Recently Viewed Projects',
                position: 'T',
                text: '<h1>Data</h1><p>It is a attribute that contains every the texts and configurations that the plugin use</p>',
                controlsPosition: 'TL'
            },{
                element: '.sorting-area',
                tooltip: 'Sorting Form',
                position: 'L',
                text: '<h5 class="push-20">This is all categories available for sorting.</h5>',                
                controlsPosition: 'TR'
            },{
                element: '.dropdown-toggle',
                tooltip: 'Account Menu',
                text: '<h5 class="push-20">Look out for our account menu</h5>',
                class: 'btn btn-primary'
            }
        ],
        controlsPosition: 'TR',
        welcomeMessage : '<h5 class="push-20">Click Start to begin the tutorial.</h5>',
        buttons: {
            next: {
                text: 'Next &rarr;',
                class: 'btn btn-default'
            },
            prev: {
                text: '&larr; Previous',
                class: 'btn btn-default'
            },
            start: {
                text: 'Start',
                class: 'btn btn-primary'
            },
            end: {
                text: 'End',
                class: 'btn btn-primary'
            }
        },
        controlsCss: {
            background: 'rgba(55, 59, 65, 0.9)',
            color: '#fff',
            width: '400px',
            'border-radius': 0
        },
        
        
    };

    $(document).ready(function() {
        $('.tour').on('click', function() {
            $.aSimpleTour(tours);
        });
        $.aSimpleTour(tours);
    });
</script>
*/ ?>