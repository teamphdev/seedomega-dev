<?php
    View::$title = 'Recently Viewed Projects';
    View::$bodyclass = '';
    View::header();
?>

    <?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        
        <article class="container projects-page" id="popular">

            <?php 
            $incomplete = false;
            if( user::info('UserLevel') == 'User' ) { 
                switch (Apputility::investorinfo('InvestorStatus')) {
                    case 'Verification':
                        echo '<div class="alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your profile is awaiting for verification! <a href="/users/profile"> Check Here.</a></div>';
                        break;
                    case 'Incomplete':
                        echo '<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>You are 1 step away to get started please complete your profile! <a href="/users/profile"> <b>Click Here.</b></a></div>';
                        $incomplete = true;
                        break;
                }
            } ?>
            <?php echo View::getMessage(); ?>

            <div class="row">

                <!-- ************************ Left Side Area ************************ -->
                <div class="col-lg-12 col-xs-12">
                    <div class="block-header push-10" style="padding: 10px 15px;">
                        <span class="badge badge-danger"><?php echo $pendingcount; ?></span> Pending(s) - Upload TT Report
                    </div>
                </div>
                <div class="col-lg-12 content-cnt">
                    <article class="container projects-page row">
                        <div class="project-grid">

                        <?php foreach( $projects as $project ): ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 project-cnt ccatid-<?php echo $project->ClientCatID; ?> <?php echo $project->Data['ProjectClass']; ?> sortcatid-<?php echo $project->ClientCatID; ?>">
                                <div class="percent-funded" style="display: none;"><?php echo $percentage; ?></div>
                                <div class="date-updated" style="display: none;"><?php echo date('Y-m-d', strtotime( $project->UpdatedAt ) ); ?></div>
                                <div class="popular-item">
                                    <figure class="project-image">
                                        <figcaption >
                                            <a href="/clients/view/<?php echo $project->ClientProfileID;?>" class="item_button1">View</a>
                                            <?php 
                                            if( Apputility::investorinfo('InvestorStatus') == 'Approved' || User::info('UserLevel') == "Administrator" ){ ?>
                                                <?php echo $project->Data['BookHTML']; ?>
                                            <?php } else { ?>
                                                <a href="#" class="item_button2 nonverified" data-toggle="modal" data-target="#myModal" style="cursor: not-allowed;">Book</a>
                                            <?php } ?>
                                        </figcaption>
                                        <?php echo $project->Data['CompanyPhotoLink']; ?>
                                    </figure>
                                    <div class="popular-content">
                                        <div class="project-desc project-desc2">
                                            <h5><a href="#"><?php echo $project->CompanyName;?>  <span class="line-green"></span></a></h5>
                                            <p><?php echo AppUtility::excerptText( $project->OfferOverview, 200, ' <a href="/clients/view/'.$project->ClientProfileID.'" class="item_button1">...read more</a>' ); ?></p>
                                        </div>

                                        <div class="popular-data"> <?php echo $project->Data['AvatarLink']; ?>
                                            <div class="pie_progress" role="progressbar" data-goal="<?php echo $project->Data['Percentage']; ?>" data-barcolor="<?php echo $project->Data['BarColor']; ?>" data-barsize="8">
                                              <div class="pie_progress__number"><?php echo $project->Data['Percentage']; ?>%</div>
                                              <div class="pie_progress__label">Completed</div>
                                            </div>
                                        </div>

                                        <div class="popular-details">
                                            <ul>
                                                <li><strong><?php echo $project->DaysLeft;?></strong> Days Left</li>
                                                <li><strong><?php echo isset($project->TotalInvestor) ? number_format($project->TotalInvestor) : "0";?></strong> Backers</li>
                                                <li class="last"><strong><?php echo isset($project->TotalRaisedAmt) ? "$".number_format($project->TotalRaisedAmt) : "0";?></strong> Funded</li>
                                            </ul>
                                        </div>                                        
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="clear"></div>                        

                        <!-- <div class="popular-btn"> <a href="#">See More</a> </div> -->
                        </div>
                    </article>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

            <?php if( $incomplete ){ ?>
            <div id="dialog" class="w3-modal">
                <div class="w3-modal-content" style="width: 600px;">
                    <div id="dialog-form" class="text-center">
                        <button type="button" class="close" aria-label="Close" onclick="document.getElementById('dialog').style.display='none'">
                          <i class="si si-close text-red"></i>
                        </button>
                        <hr>
                        <div class="modal-body heading"><h3>Please complete your profile!</h3></div>
                        <a href="/users/profile" class="btn btn-3 blue"><i class="fa fa-pencil text-white"></i>  Complete Profile</a>&nbsp;
                        <a href="/clients/view/<?php echo $proj->ClientProfileID;?>" class="btn btn-3 green"><i class="si si-eye text-white"></i>  View</a>
                        <div class="text-reminder text-white">You are 1 step away to get started</div>
                    </div>
                </div>
            </div>
            <?php } ?>

        </article>
    </section>

<?php View::footer(); ?>
<script type="text/javascript">
    $( document ).ready( function(){

        $('.nonverified').click(function(){
            var refTop, diaHeight;
            $('#dialog').css('display', 'block');
            diaHeight = $('#dialog').height();
            refTop = $('#popular').offset().top;
            $('#dialog').css('padding-top', (refTop - 85) + 'px');
            $('#myModal').css('padding-top', (refTop / 2) + 'px');
        });

        // init Isotope
        var $grid = $('.project-grid').isotope({
            itemSelector: '.project-cnt',
            layoutMode: 'fitRows',
            getSortData: {
              topfunded: '.percent-funded parseInt',
              lowfunded: '.percent-funded parseInt',
              dateupdated : '.date-updated parseInt'
            }
        });

        // bind filter button click
        $('#categories').on('change', function() {
            var filterValue = $( this ).val();

            // use filterFn if matches value
            $grid.isotope({ filter: filterValue });
        });

        $('#projsortby').on('change', function() {

            var $optvalue = $(this).val();
            var $vals = $optvalue.split('|');
            var filterByValue = $vals[1];

            if($vals[0] == 'sort'){
                if($vals[1] == 'lowfunded'){ 
                    $sorting = true; 
                }else{ 
                    $sorting = false; 
                }
              $grid.isotope({ filter: '*', sortBy: filterByValue, sortAscending: $sorting });
            }else{
              $grid.isotope({ filter: filterByValue });
            }
        });

    });
</script>