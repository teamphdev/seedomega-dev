<?php
View::$title = 'Member Information';
View::$bodyclass = User::info('Sidebar');
View::header();
?>
    <!-- ************************ Page Content ************************ -->
    <section class="header-bottom">
        <article>
            <div class="container">
                <div class="text-center overflow-hidden">
                    <div class="push-50-t push animated fadeInDown">
                        <?php $avatar = View::common()->getUploadedFiles( $userinfo->Avatar ); ?>
                        <?php echo View::photo( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png', "Avatar", "img-avatar img-avatar128 img-avatar-thumb" ); ?>
                    </div>
                    <div class="push-20-t animated fadeInUp">            
                        <h4 class="h3 push-10-t text-white"><?php echo $userinfo->FirstName; ?> <?php echo $userinfo->LastName; ?></h4>
                    </div>
                </div>

            </div>
        </article>
    </section>
    <?php View::template('users/breadcrumb'); ?>

    <section class="gray">
        <article class="container project-single">
            <div class="start-project centered max800">
                <!-- Main Content -->
                <div class="block items-push">
                    <div class="block-header">
                        <?php echo View::$title; ?>
                    </div>
                    <div class="block-content">
                        <?php echo View::getMessage(); ?>
                        <div class="member-info items-push">
                            <div class="">
                                <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="action" value="updatepersonal" />
                                    <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                                    <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                                    <input type="hidden" name="avatarid" value="<?php echo $userinfo->Avatar; ?>" />

                                    <div class="form-group profile-img">
                                        <label><?php echo Lang::get('USR_PRF_PPCTURE'); ?> </label>
                                        <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                        <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_PRF_FN'); ?> <span class="required">*</span></label>
                                            <input type="text" value="<?php echo $userinfo->FirstName; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control">
                                        </div>
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_PRF_LN'); ?> <span class="required">*</span></label>
                                            <input type="text" value="<?php echo $userinfo->LastName; ?>" id="lname" name="meta[LastName]" required="required" class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_PRF_EML'); ?> <span class="required">*</span></label>
                                            <input type="email" id="EmailChecker2" name="user[Email]" class="form-control" value="<?php echo isset($userinfo->Email) ? $userinfo->Email : ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($userinfo->UserID) ? $userinfo->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>">
                                            <span id="emailloading" class="fa fa-refresh fa-spin"></span>
                                        </div>
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_PRF_PHNE'); ?> <span class="required">*</span></label>
                                            <input type="phone" value="<?php echo $userinfo->Phone; ?>" id="phone" name="meta[Phone]" required="required" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('USR_PRF_GNDR'); ?> <span class="required">*</span></label>
                                        <div>
                                            <label class="css-input css-radio css-radio-primary push-10-r">
                                                <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $userinfo->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                            </label>
                                            <label class="css-input css-radio css-radio-primary">
                                                <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $userinfo->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-left">
                                            <label>Occupation</label>
                                            <input type="text" value="<?php echo $userinfo->Occupation; ?>" name="meta[Occupation]" class="form-control">
                                        </div>
                                        <div class="form-right">
                                            <label>Job Title</label>
                                            <input type="text" value="<?php echo $userinfo->JobTitle; ?>" name="meta[JobTitle]" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('USR_PRF_RMRKS'); ?></label>
                                        <textarea class="form-control dowysiwyg" name="meta[Bio]"><?php echo $userinfo->Bio; ?></textarea>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="text-center">
                                            <!-- <a href="<?php echo View::url('users/'); ?>" class="btn btn-warning"><?php echo Lang::get('USR_PRF_CANBTN'); ?></a> -->
                                            <button class="btn btn-rounded btn-default green" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END Main Content -->
            </div>
        </article>
    </section>

<?php View::footer(); ?>