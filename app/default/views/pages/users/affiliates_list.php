<?php 
    View::$title = 'Referrals';
    View::$bodyclass = '';
    View::header(); 
?>
<?php $userinfo = User::info(); ?>

<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <?php $affiliateUrl = View::url( 'join/'.User::info( 'UserID' ) ); ?>
        <div class="row animated fadeInDown">

            <div class="col-lg-6 push-20">
                <div class="block-header">
                    <div class="block-title">Earn More</div>
                </div>
                <div class="block-content">
                    <div class="form-group">
                        <label>Share your affiliate link:</label> 
                        <input type="text" class="form-control" value="<?php echo $affiliateUrl; ?>" readonly />
                    </div>
                    <div class="form-group"><label>Share with social media:</label>                     
                        <ul class="affiliate-share">
                            <li title="Facebook">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $affiliateUrl; ?>" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li title="Twitter">
                                <a href="https://twitter.com/home?status=<?php echo $affiliateUrl; ?>" target="_blank">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li title="LinkedIn">
                                <a href="https://www.linkedin.com/shareArticle?mini=true&title=Join%20Now!&url=<?php echo $affiliateUrl; ?>" target="_blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li title="Google+">
                                <a href="https://plus.google.com/share?url=<?php echo $affiliateUrl; ?>" target="_blank">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php 
            $promo = true; // TODO: link this to the promo switch
            if(User::role() == 'Client' && $promo == true) {
            $affiliatePromoUrl = View::url( 'join/'.User::info( 'UserID' ).'/'.AppUtility::getClientProfileInfo(User::info( 'UserID' ))->ClientProfileID ); ?>
                <div class="col-lg-6 push-20">
                    <div class="block-header">
                        <div class="block-title">SeedOmega Promo Link</div>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <label>Share your affiliate link:</label> 
                            <input type="text" class="form-control" value="<?php echo $affiliatePromoUrl; ?>" readonly />
                        </div>
                        <div class="form-group"><label>Share with social media:</label>                     
                            <ul class="affiliate-share">
                                <li title="Facebook">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $affiliatePromoUrl; ?>" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li title="Twitter">
                                    <a href="https://twitter.com/home?status=<?php echo $affiliatePromoUrl; ?>" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li title="LinkedIn">
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&title=Join%20Now!&url=<?php echo $affiliatePromoUrl; ?>" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li title="Google+">
                                    <a href="https://plus.google.com/share?url=<?php echo $affiliatePromoUrl; ?>" target="_blank">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            <?php } ?>


        </div>
        
        <?php /* echo View::getMessage(); ?>
        <div class="block">
            <div class="block-content">
                <h3><?php echo $mytitle; ?> Invites</h3>
                <table>
                    <tr>
                        <td><small class="text-muted"><?php echo Lang::get('USR_MNG_COMMI'); ?> Rate:</small></td>
                        <td style="padding-left:10px;"><small class="text-muted"><b><?php echo isset( $myCommRate ) ? number_format( $myCommRate->CommissionRate ) : '0'; ?>%</b></small></td>
                    </tr>
                    <tr>
                        <td><small class="text-muted">Outstanding Balance:</small></td>
                        <td style="padding-left:10px;"><small class="text-muted"><b>$<?php echo $totalcommission; ?></b></small></td>
                    </tr>
                </table>
                <hr><br>
                <ul class="nav nav-tabs" data-toggle="tabs">
                    <li class="text-center active">
                        <a href="#referral-converted"><b>Converted</b>&nbsp;<span class="badge badge-success"><?php echo isset( $totalconverted ) ? $totalconverted : '0'; ?></span><br><small class="text-muted">already invested</small></a>
                    </li>
                    <li class="text-center">
                        <a href="#referral-invited"><b>Invited</b>&nbsp;<span class="badge badge-warning"><?php echo isset( $totalinvited ) ? $totalinvited : '0' ;?></span><br><small class="text-muted">no investment yet</small></a>
                    </li>
                    <li class="text-center">
                        <a href="#referral-withdrawal"><b>Withdrawal</b>&nbsp;<span class="badge badge-info"><?php echo $totalwithdrawable ;?></span><br><small class="text-muted">request form</small></a>
                    </li>
                    <li class="text-center">
                        <a href="#referral-earning"><b>Earning</b><br><small class="text-muted">current year</small></a>
                    </li>
                </ul>
                <div class="block-content tab-content bg-white">
                    <!-- Converted -->
                    <div class="tab-pane fade fade-up in active" id="referral-converted">
                        <table class="table  js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter in active" cellspacing="0" style="width: 100%;">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center" style="min-width:20%;"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                                    <th class="text-center">Company</th>
                                    <th class="text-center">Booking Date</th>
                                    <th width="15%" class="text-center">Invested</th>
                                    <th width="14%" class="text-center">SO Commission</th>
                                    <th width="16%" class="text-center"><?php echo Lang::get('USR_MNG_COMMI_AMT'); ?></th>
                                    <th width="12%" class="text-center">Registration Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $cntr = 0;
                                    if( count( $converted ) ){
                                        foreach( $converted as $ref ){
                                            if( isset( $ref->InvestmentAmount ) ){ $cntr++; ?>
                                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                                    <td>
                                                        <div class="profile-image"><?php echo $ref->Data['AvatarLink']; ?></div>
                                                        <?php echo $ref->Data['Name']; ?>
                                                    </td>
                                                    <td><?php echo isset( $ref->InvestedCompany ) ? $ref->InvestedCompany : '' ; ?></td>
                                                    <td class="text-center"><?php echo isset( $ref->BookingDate ) && $ref->BookingDate != "0000-00-00" ? date( 'j M Y', strtotime( $ref->BookingDate ) ) : "-"; ?></td>
                                                    <td class="text-right"><?php echo '$ '.$ref->InvestmentAmount; ?></td>
                                                    <td class="text-right"><?php echo '$ '.$ref->SOCommission; ?></td>
                                                    <td class="text-right"><?php echo '$ '.$ref->Commission; ?></td>
                                                    <td class="text-center"><?php echo isset( $ref->JoinDate ) && $ref->JoinDate != "0000-00-00" ? date( 'j M Y', strtotime( $ref->JoinDate ) ) : "-"; ?></td>
                                                </tr>
                                        <?php } }
                                    } else { ?>
                                    <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                        <td colspan="7">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END Converted -->

                    <!-- Invited -->
                    <div class="tab-pane fade fade-up" id="referral-invited">
                        <table class="table  js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter in active" cellspacing="0" style="width: 100%;">
                            <thead>
                                <tr class="headings">
                                    <th class="text-center"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                                    <th width="20%" class="hidden"></th>
                                    <th width="15%" class="text-center">Profile <?php echo Lang::get('USR_MNG_STATUS'); ?></th>
                                    <th width="15%" class="text-center">Registration Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $cntr = 0;
                                    if( count( $invited ) ){
                                        foreach( $invited as $ref ){
                                            if( !isset( $ref->TotalAmountAttached ) ){ $cntr++;
                                                switch( $ref->Status ){
                                                    case 'Approved':
                                                        $status = '<span class="text-success"><b>Approved</b></span>';
                                                        break;
                                                    case 'Verification':
                                                        $status = '<span class="text-warning"><b>Verification</b></span>';
                                                        break;
                                                    case 'Incomplete':
                                                    default:
                                                        $status = '<span class="text-danger"><b>Incomplete</b></span>';
                                                        break;
                                                } ?>
                                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                                    <td>
                                                        <div class="profile-image"><?php echo $ref->Data['AvatarLink']; ?></div>
                                                        <?php echo $ref->Data['Name']; ?>
                                                    </td>
                                                    <td class="hidden"></td>
                                                    <td class="text-center"><?php echo $status ? $status : ''; ?></td>
                                                    <td class="text-center"><?php echo isset( $ref->JoinDate ) && $ref->JoinDate != "0000-00-00" ? date( 'j M Y', strtotime( $ref->JoinDate ) ) : "-"; ?></td>                                                    
                                                </tr>
                                        <?php } }
                                    } else {?>
                                    <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                        <td colspan="4">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END Invited -->

                    <!-- Request Withdrawal -->
                    <div class="tab-pane fade fade-up" id="referral-withdrawal">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                            <input type="hidden" name="action" value="withdraw" />
                            <span class="text-muted text-info">Note: check the desired commission that you want to withdraw.</span>
                            <table id="withdraw" class="table  js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter in active" cellspacing="0" style="width: 100%;">
                                <thead>
                                    <tr class="headings">
                                        <td class="text-center">
                                            <label class="pointer" for="checkall">All</label>
                                            <div class="checkall"><input class="checkbox pointer" type="checkbox" name="" id="checkall"></div>
                                        </td>
                                        <th class="text-center" style="min-width:20%;"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                                        <th class="text-center">Company</th>
                                        <th class="text-center">Booking Date</th>
                                        <th width="15%" class="text-center">Invested</th>
                                        <th width="14%" class="text-center">SO Commission</th>
                                        <th width="16%" class="text-center no-sorting">
                                            <?php echo Lang::get('USR_MNG_COMMI_AMT'); ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $cntr = 0;
                                        if( count( $converted ) ){
                                            foreach( $converted as $ref ){
                                                if( isset( $ref->InvestmentAmount ) ){ $cntr++; ?>
                                                    <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                                        <td class="text-center">
                                                            <input class="checkbox pointer commcheck" type="checkbox" id="id<?php echo $cntr; ?>" name="iIDs[]" value="<?php echo $ref->InvestmentBookingID; ?>" data-amount="<?php echo $ref->Commission; ?>">
                                                        </td>
                                                        <td>
                                                            <div class="profile-image"><?php echo $ref->Data['AvatarLink']; ?></div>
                                                            <?php echo $ref->Data['Name']; ?>
                                                        </td>
                                                        <td><?php echo isset( $ref->InvestedCompany ) ? $ref->InvestedCompany : '' ; ?></td>
                                                        <td class="text-center"><?php echo isset( $ref->BookingDate ) && $ref->BookingDate != "0000-00-00" ? date( 'j M Y', strtotime( $ref->BookingDate ) ) : "-"; ?></td>
                                                        <td class="text-right"><?php echo '$ '.$ref->InvestmentAmount; ?></td>
                                                        <td class="text-right"><?php echo '$ '.$ref->SOCommission; ?></td>
                                                        <td class="text-right">
                                                            <label class="pointer" for="id<?php echo $cntr; ?>"><?php echo '$ '.$ref->Commission; ?></label>
                                                        </td>
                                                    </tr>
                                            <?php } }
                                        } else { ?>
                                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                            <td colspan="7">No Data</td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <!-- END Request Withdrawal -->

                    <!-- Earnings -->
                    <div class="tab-pane fade fade-up" id="referral-earning">
                        <div class="col-sm-6 col-sm-offset-3">
                            <table id="earning" class=" table-header-bg table-hover table-vcenter in active">
                                <thead>
                                    <tr class="headings">
                                        <th class="text-center">Month</th>
                                        <th class="text-center">Commission</th>
                                        <th class="text-center">Currency</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr = 0; foreach( $monthlyEarning as $key => $value ){ $ctr++;?>
                                    <tr  class="<?php echo $ctr % 2 == 0 ? 'even' : 'odd'; ?> pointer">
                                        <td><?php echo $key; ?></td>
                                        <td class="text-right"><?php echo $value; ?></td>
                                        <td class="text-left">$</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Earning -->

                </div>
            </div>
        </div>
        <?php */ ?>
    </div>
</section>
<!-- page content -->

<?php View::footer(); ?>

<script language="javascript">
$(document).ready(function() {

    var table = $( '#withdraw1' ).DataTable({
            "order": [],
            "columnDefs": [{
                "targets": 'no-sorting',
                "orderable": false,
                "visible": false
            }]
        });
    
    var tags = '<div class="withdrawal-form">Total:<b><input type="textbox" class="form-control text-right disabled" id="totalAmount" value="$ 0.00"></b>';
    tags += '&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-info btn-default inline disabled" id="process-withdrawal">Withdraw</button></div>';
    $( '#withdraw_filter' ).append( tags );

    $( "#checkall" ).click( function(){
        $( 'input:checkbox' ).not( this ).prop( 'checked', this.checked );
        computeTotalAmount();
    });
    $( ".commcheck" ).change( function(){
        computeTotalAmount();
    });

});

function computeTotalAmount(){
    var amount = 0;
    $( '#totalAmount' ).val( '$ 0.00' );
    $( ".commcheck" ).each( function(){
        if( $( this ).prop( 'checked' ) ){
            var val = $( this ).attr( 'data-amount' );
            amount += parseFloat( val.replace( ',', '' ) );
        }
    });
    amount = addCommas( amount.toFixed( 2 ) );
    $( '#totalAmount' ).val( '$ ' + amount );

    if( amount == 0 ){
        $( '#process-withdrawal' ).addClass( "disabled" );
        $( '#process-withdrawal' ).attr( "disabled", "disabled" );
    } else {
        $( '#process-withdrawal' ).removeClass( "disabled" );
        $( '#process-withdrawal' ).removeAttr( "disabled" );
    }
}

function addCommas( nStr )
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

</script>