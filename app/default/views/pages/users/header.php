<?php $userinfo = User::info(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $favicon = View::common()->getUploadedFiles(Option::get('favicon')); ?>
        <link rel="icon" href="<?php echo isset($favicon[0]->FileSlug) ? View::url('assets/files').$favicon[0]->FileSlug : View::url('assets/images/')."plus.png"; ?>" type="image/png" sizes="16x16">
        <title><?php echo View::$title . ' | '. Option::get('site_title',Config::get('SITE_TITLE')); ?></title>

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css">
        <?php View::headers(); ?>
        <?php View::template('jslanguage'); ?> 
    </head>

    <body class="<?php echo View::$bodyclass; ?>">
        
        <script>
            function checkLoginState() {                
              FB.getLoginStatus(function(response) {
                
                if(response.status=='connected') {
                    //alert("You are connected bro!\nID"+ response.authResponse.userID);
                    FB.api('/me', {fields: 'last_name,email'}, function(response) {                    
                      console.log(response);
                        alert("You are connected bro! "+ response.email);
                    }, {scope: 'email,user_likes'});
                    
                } else {
                    alert('-_-');
                }
              });
            }
        </script>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=332788940563970&autoLogAppEvents=1';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <div class="wrapper"> 
            <!-- Header -->
            <header>
                <div class="container">
                    <?php View::template('logo'); ?>
                    <?php View::template('nav'); ?>                            
                </div>
            </header>