<?php 
View::$title = 'Sign Up';
View::$bodyclass = 'loginpage';
View::header(); 
?>

<?php /*
    <section class="breadcrumb">
        <article class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul>
                        <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                        <li><a href="<?php echo View::url(); ?>">Home</a></li>
                        <li class="fa fa-angle-right"></li>
                        <li><?php echo View::$title; ?></li>
                    </ul>
                </div>
            </div>
        </article>
    </section>
*/ ?>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container project-single">
            <div class="start-project">
                <!-- Main Content -->
                <div class="block items-push">
                    <div class="block-content tab-content">
                        <?php echo View::getMessage(); ?>

                        <!-- Account Validation -->
                        <div class="row items-push">
                            <div class="col-sm-8 col-sm-offset-2">
                                <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                    <input type="hidden" name="action" value="signupuser" />
                                    <input type="hidden" name="ReferrerUserID" value="<?php echo $ReferrerUserID; ?>" />
                                    <input type="hidden" name="uref[Exemption]" value="<?php echo $isExempted; ?>" />
                                    <div class="form-group">
                                        <label>Account Type <span class="required">*</span></label>
                                        <div>
                                            <label class="css-input css-radio css-radio-primary push-10-r">
                                                <input type="radio" class="flat" name="acct[AccountType]" id="Individual" value="Individual" checked required /><span></span> Individual
                                            </label>
                                            <label class="css-input css-radio css-radio-primary">
                                                <input type="radio" class="flat" name="acct[AccountType]" id="Corporate" value="Corporate" required /><span></span> Corporate
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-left">
                                            <input type="text" value="" class="form-control" placeholder="Last Name" name="meta[LastName]" required="required">
                                        </div>
                                        <div class="form-right">
                                            <input type="text" value="" class="form-control" placeholder="First Name" name="meta[FirstName]" required="required">
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-left">
                                            <input type="email" id="EmailChecker" name="user[Email]" class="form-control" value="" required="required" rel="<?php echo View::url('ajax/checkemail/'); ?>" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>" placeholder="<?php echo Lang::get('USR_PRF_EML'); ?>">
                                            <span id="emailloading" class="fa fa-refresh fa-spin"></span>
                                        </div>
                                        <div class="form-right">
                                            <select class="form-control arrow-down" name="meta[Country]">
                                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="form-group">                                        
                                        <div class="form-left">
                                            <input type="password" value="" class="form-control" placeholder="Password" name="user[Password]" required="required">
                                        </div>
                                        <div class="form-right">
                                        </div><div class="clear"></div>
                                    </div>

                                    <div class="form-group">
                                        <label>Complete my information (Optional)</label>
                                        <div>
                                            <label class="css-input css-radio css-radio-primary push-10-r">
                                                <input type="radio" class="flat CompleteInfo" id="CompleteLater" value="Later" name="CompleteInfo" checked required /><span></span> Later
                                            </label>
                                            <label class="css-input css-radio css-radio-primary">
                                                <input type="radio" class="flat CompleteInfo" id="CompleteNow" value="Now" name="CompleteInfo" required /><span></span> Now
                                            </label>
                                        </div>
                                    </div>

                                    <div class="CompleteInformation" style="display: none;">
                                        <div class="form-group">                                        
                                            <div class="form-left">
                                                <input type="phone" value="" id="phone" name="meta[Phone]" placeholder="Phone" class="form-control">
                                            </div>
                                            <div class="form-right">
                                            </div><div class="clear"></div>
                                        </div>

                                        <div class="form-group">                                        
                                            <div class="form-left">
                                            <input type="text" value="" id="address" name="meta[Address]" placeholder="<?php echo Lang::get('USR_PRF_ADDS'); ?>" class="form-control">
                                            </div>
                                            <div class="form-right">
                                                <input type="text" value="" id="city" name="meta[City]" placeholder="<?php echo Lang::get('USR_PRF_CTY'); ?>" class="form-control">
                                            </div><div class="clear"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-left">
                                                <input type="text" value="" id="state" name="meta[State]" placeholder="<?php echo Lang::get('USR_PRF_STATES'); ?>" class="form-control">
                                            </div>
                                            <div class="form-right">
                                                <input type="text" value="" id="postal" name="meta[PostalCode]" placeholder="<?php echo Lang::get('USR_PRF_PCODE'); ?>" class="form-control">
                                            </div><div class="clear"></div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_UPLGOVID'); ?> </label>
                                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="IdPhoto" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg"]'>
                                            <span class="text-muted">Allowed file types: pdf, jpeg, jpg, png</span>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_UPLADDRPRF'); ?> </label>
                                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="AddressPhoto" data-show-upload="false" data-allowed-file-extensions='["pdf","jpeg","png","jpg"]'>
                                            <span class="text-muted">Allowed file types: pdf, jpeg, jpg, png</span>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="text-center">
                                            <button class="btn btn-4 blue green font-18" type="submit"><i class="fa fa-check push-5-r"></i> Join Now!</button>
                                        </div>
                                    </div>

                                    <div class="divider"><span>OR</span></div>

                                    <a href="<?php echo App::fbUrl('users/fbsignup'); ?>" class="btn btn-block btn-fb push-10 social" type="button"><i class="fa fa-facebook pull-left"></i> Signup with Facebook</a>
                                    <a href="<?php echo App::gAuthURL();?>" class="btn btn-block btn-gplus push-10 social"><i class="fa fa-google-plus pull-left"></i> Signup with Google+</a>


                                </form>

                            </div>
                        </div>
                        <!-- END Account Validation -->

                    </div>
                    <!-- <div class="block-content block-content-full bg-gray-lighter text-center">
                        <button class="btn btn-4 blue green" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                        <button class="btn btn-4 blue yellow" type="reset"><i class="fa fa-refresh push-5-r"></i> Reset</button>
                    </div> -->
                </div>
                <!-- END Main Content -->
            </div>
        </article>
    </section>
<?php View::footer(); ?>