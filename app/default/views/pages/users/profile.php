<?php
    View::$title = $mytitle;
    View::$bodyclass = User::info('Sidebar').' user-profile-page';
    View::header();
?>
    <section class="header-bottom no-padding">
        <article>
            <div class="container animated fadeInDown">

                <div class="start-project profile-tabs profile-tabs-4">
                
                    <div class="title">
                        <ul id="profileTabs">
                            <li data-link="my-profile" class="<?php echo $thispage == 'documents' ? 'current' : ''; ?>" id="tab-my-profile">
                                <a href="<?php View::url( 'users/profile/documents', true ); ?><?php echo $thisid ? '/'.$thisid : ''; ?><?php echo $thisrequest ? '/'.$thisrequest : ''; ?>"><i class="fa fa-file"></i><span>My Documents</span></a>
                            </li>
                            <li data-link="my-details" class="<?php echo $thispage == 'details' ? 'current' : ''; ?>" id="tab-my-details">
                                <a href="<?php View::url( 'users/profile/details', true ); ?><?php echo $thisid ? '/'.$thisid : ''; ?><?php echo $thisrequest ? '/'.$thisrequest : ''; ?>" ><i class="fa fa-user"></i><span>My Details</span></a>
                            </li>
                            <li data-link="more-about-me" class="<?php echo $thispage == 'more-about-me' ? 'current' : ''; ?>" id="tab-more-about-me">
                                <a href="<?php View::url( 'users/profile/more-about-me', true ); ?><?php echo $thisid ? '/'.$thisid : ''; ?><?php echo $thisrequest ? '/'.$thisrequest : ''; ?>" ><i class="fa fa-vcard-o"></i><span>More About Me</span></a>
                            </li>
                            <li data-link="my-other" class="<?php echo $thispage == 'other' ? 'current' : ''; ?>" id="tab-my-other">
                                <a href="<?php View::url( 'users/profile/other', true ); ?><?php echo $thisid ? '/'.$thisid : ''; ?><?php echo $thisrequest ? '/'.$thisrequest : ''; ?>"><i class="fa fa-photo"></i><span>Other</span></a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </article>
    </section>

    <section class="gray min-height-20 no-padding" style="margin-top:-20px;">
        <div class="container animated fadeIn">

            <div class="profile-progress">
                <div class="pp-percentage"><?php echo isset( $percentcomplete ) ? $percentcomplete : 0; ?>%</div>
                <div class="pp-label">Completed</div>
                <div class="pp-progress">
                    <div class="pp-line" style="width: <?php echo isset( $percentcomplete ) ? $percentcomplete : 0; ?>%;"></div>
                </div>
            </div>

        </div>
    </section>

    <!-- ************************ Page Content ************************ -->
    <section class="gray no-padding">
        <div class="text-right">
            <div class="container ">
                <ul class="push-20 push-20-t" style="list-style: none;">
                    <?php echo $requestHTML; ?>
                </ul>
            </div>
        </div>
        <article class="container project-single animated fadeIn">
            <div class="start-project" id="profile_wizard">                

                <!-- Main Content -->
                <div class="start-content no-padding push-40">                    

                    <?php echo View::getMessage(); ?>
                    <div id="my-profile" class="form-wizard no-padding <?php echo $thispage == 'documents' ? 'active' : ''; ?>">
                        <form class="form-horizontal form-ui no-padding form-label-left input_mask" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="updateprofile" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="acctid" value="<?php echo $profiles[0]->AccountID; ?>" />
                            <input type="hidden" name="status" value="<?php echo $profiles[0]->Status; ?>" />
                            <input type="hidden" name="section" value="documents" />
                            <input type="hidden" name="IdPhotoID" value="<?php echo $profiles[0]->IdPhoto; ?>" />
                            <input type="hidden" name="AddressPhotoID" value="<?php echo $profiles[0]->AddressPhoto; ?>" />

                            <div class="block-header">
                                <div class="block-title">Upload Documents</div>
                                <div class="block-title-sub">(Government Issued IDs and Proof of Address)</div>
                            </div>

                            <div class="block-content push-20"><?php echo $info; ?></div>

                            <div class="block-content">

                                <div class="upload-documents">
                                    <div id="file-wrapper" class="file-wrapper">
                                        <?php echo isset( $userinfo ) ? AppUtility::getInvestorFileList( $profiles[0], true ) : ''; ?>
                                        <?php /*
                                        $ctr = 0;
                                        if( isset( $profiles ) && count( $profiles ) ){
                                            foreach( $profiles as $profile ){
                                                $fgID = isset( $profile->FileGroupID ) ? $profile->FileGroupID : '0';
                                                $upload = $profile->FileID == NULL || strlen( $profile->FileID ) == 0 ? true : false;
                                                echo AppUtility::getCustomFileList( $fgID.'-SeederProfileDocs'.$ctr, $profile, $upload );
                                                $ctr++;
                                            }
                                            if( $profiles[0]->FileID != NULL && $profiles[0]->Disabled == '' ){
                                                echo AppUtility::getCustomFileList( '0-SeederProfileDocs'.$ctr++, false, true );
                                            }
                                        } else {
                                            echo AppUtility::getCustomFileList( '0-SeederProfileDocs'.$ctr++, false, true );
                                        } ?>
                                        <?php echo '<a href="javascript:void(0);" class="btn btn-4 blue'.$profiles[0]->Disabled.'" id="addDocForm">Create Upload Form</a>'; ?>
                                        <?php echo '<input type="hidden" id="counter" value="'.$ctr.'" />'; */ ?>
                                    </div>
                                </div>

                                <?php echo $changeStatusHTML; ?>

                                <hr><br>

                                <div class="ln_solid"></div>
                                <div class="form-group<?php echo $approvehide; ?>">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-rounded btn-primary " onclick="return validateForm( 'btnSubmitDocumentSave' );">Save</button>
                                        <button type="button" class="btn btn-rounded btn-warning " onclick="return validateForm( 'btnSubmitDocumentSaveNext' );">Save & Next <i class="si si-action-redo push-5-l"></i></button>
                                        <button type="submit" class="btn btn-rounded btn-default  hidden" name="btnsubmit" id="btnSubmitDocumentSave">Save</button>
                                        <button type="submit" class="btn btn-rounded btn-warning  hidden" value="savenextdetails" name="btnsubmit" id="btnSubmitDocumentSaveNext">Save & Next <i class="si si-action-redo push-5-l"></i></button>
                                    </div>
                                </div>

                            </div>

                        </form>
                    </div>

                    <div id="my-details" class="form-wizard <?php echo $thispage == 'details' ? 'active' : ''; ?>">
                        <form class="form-horizontal no-padding form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="updateprofile" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="acctid" value="<?php echo $profiles[0]->AccountID; ?>" />
                            <input type="hidden" name="status" value="<?php echo $profiles[0]->Status; ?>" />

                            <div class="block-header">
                                <div class="block-title">Member Details</div>
                            </div>

                            <div class="block-content push-20"><?php echo $info; ?></div>

                            <div class="block-content">

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('USR_PRF_FN'); ?> <span class="required">*</span></label>
                                        <input type="text" value="<?php echo $userinfo->FirstName; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control">
                                    </div>
                                    <div class="form-right">
                                        <label><?php echo Lang::get('USR_PRF_LN'); ?> <span class="required">*</span></label>
                                        <input type="text" value="<?php echo $userinfo->LastName; ?>" id="lname" name="meta[LastName]" required="required" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('USR_PRF_EML'); ?> <span class="required">*</span></label>
                                        <input type="email" id="EmailChecker2" name="user[Email]" class="form-control" value="<?php echo isset($userinfo->Email) ? $userinfo->Email : ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($userinfo->UserID) ? $userinfo->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>">
                                        <span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                    </div>
                                    <div class="form-right">
                                        <label><?php echo Lang::get('USR_PRF_PHNE'); ?> <span class="required">*</span></label>
                                        <input type="phone" value="<?php echo $userinfo->Phone; ?>" id="phone" name="meta[Phone]" required="required" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('USR_PRF_BDAY'); ?> </label>
                                        <input type="text" id="DateOfBirth" name="meta[DateOfBirth]" class="form-control jsdate" value="<?php echo isset($userinfo->DateOfBirth) ? $userinfo->DateOfBirth: ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($userinfo->UserID) ? $userinfo->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="">
                                    </div>
                                    <div class="form-right">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label><?php echo Lang::get('USR_PRF_ADDS'); ?></label>
                                    <input type="text" value="<?php echo $userinfo->Address; ?>" id="address" name="meta[Address]" class="form-control" required="required">
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('USR_PRF_CTY'); ?></label>
                                        <input type="text" value="<?php echo $userinfo->City; ?>" id="city" name="meta[City]" class="form-control" required="required">
                                    </div>
                                    <div class="form-right">
                                        <label><?php echo Lang::get('USR_PRF_STATES'); ?></label>
                                        <input type="text" value="<?php echo $userinfo->State; ?>" id="state" name="meta[State]" class="form-control" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('USR_PRF_CNTRY'); ?></label>
                                        <select name="meta[Country]" class="form-control form-control" required="required">
                                            <?php foreach(AppUtility::getCountries() as $country) { ?>
                                                <option value="<?php echo $country; ?>" <?php echo $userinfo->Country == $country ? 'selected' : ''; ?>><?php echo $country; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-right">
                                        <label for="postal" class="control-label"><?php echo Lang::get('USR_PRF_PCODE'); ?></label>
                                        <input type="text" value="<?php echo isset($userinfo->PostalCode) ? $userinfo->PostalCode : ''; ?>" id="postal" name="meta[PostalCode]" class="form-control" required="required">
                                    </div>
                                </div>

                                <?php echo $changeStatusHTML; ?>

                                <br><hr><br>

                                <?php if( $profiles[0]->AccountType == 'Corporate' ){ ?>

                                <?php } ?>

                                <div class="ln_solid"></div>
                                <div class="form-group<?php echo $approvehide; ?>">
                                    <div class="text-center">
                                        <a class="btn btn-rounded btn-info " href="/users/profile/documents"><i class="si si-action-undo push-5-r"></i> Back</a>
                                        <button class="btn btn-rounded btn-primary " type="submit" name="btnsubmit">Save</button>
                                        <button class="btn btn-rounded btn-warning " type="submit" value="savenextmoreabout" name="btnsubmit">Save & Next <i class="si si-action-redo push-5-l"></i></button>
                                    </div>

                                    <!-- <button type="button" data-toggle="modal" data-target="#modal-updaterequest"  class="btn btn-default ">Update Request</button> -->
                                </div>
                            </div>

                        </form>
                    </div>

                    <!--more about me-->
                    <div id="more-about-me" class="form-wizard <?php echo $thispage == 'more-about-me' ? 'active' : ''; ?>">
                        <form class="form-horizontal form-ui no-padding form-label-left input_mask" method="post">
                            <input type="hidden" name="action" value="updateprofile" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="acctid" value="<?php echo $profiles[0]->AccountID; ?>" />
                            <input type="hidden" name="status" value="<?php echo $profiles[0]->Status; ?>" />

                            <div class="block-header">
                                <div class="block-title">More About Me</div>
                            </div>

                            <div class="block-content push-20"><?php echo $info; ?></div>

                            <div class="block-content">
                                <div class="accordion" id="accordion-faq">
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                <strong>Additional Information</strong>
                                                <span class="updown-cion"></span>
                                            </a>
                                        </div>
                                        <div id="collapseOne" class="accordion-body collapse in" aria-expanded="true" style="">
                                            <div class="accordion-inner">
                                                <label>Security Question</label>
                                                <textarea class="form-control dowysiwyg" name="acct[SecurityAnswer]"><?php echo $profiles[0]->SecurityAnswer; ?></textarea>
                                            </div>

                                            <div class="accordion-inner">
                                                <p>Within the last 10 years, have you worked in an investment-related field for 3 or more
                                                    consecutive years?</p>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[WorkedInInvestmentField]" id="WorkedInInvestmentFieldNo" value="0" <?php echo $profiles[0]->WorkedInInvestmentField == '0' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[WorkedInInvestmentField]" id="WorkedInInvestmentFieldY" value="1" <?php echo $profiles[0]->WorkedInInvestmentField== '1' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="accordion-inner">
                                                <p><?php echo Lang::get('USR_PRF_INVQS_EMPSTAT'); ?></p>
                                                <?php View::form('selecta',array('name'=>'acct[EmploymentStatus]','options'=>['Employed'=>'Employed','Homemaker'=>'Homemaker','Retired'=>'Retired','Self-Employed'=>'Self-Employed','Student'=>'Student','Unemployed'=>'Unemployed'],'class'=>'form-control','value'=>$profiles[0]->EmploymentStatus)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                <strong>Education</strong>
                                                <span class="updown-cion"></span>
                                            </a>
                                        </div>
                                        <div id="collapseTwo" class="accordion-body collapse in" aria-expanded="false" style="height: auto;">
                                            <div class="accordion-inner">
                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_EDULEVEL'); ?></label>
                                                    <?php View::form('selecta',array('name'=>'acct[HighestLevelEdu]','options'=>['Graduate'=>'Graduate','Masters'=>'Masters','College'=>'College','Undergraduate'=>'Undergraduate','Postgraduate'=>'Postgraduate'],'class'=>'form-control','value'=>$profiles[0]->HighestLevelEdu)); ?>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_FINCERT'); ?></label>
                                                    <textarea class="form-control dowysiwyg" name="acct[FinanceCertification]"><?php echo $profiles[0]->FinanceCertification; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false">
                                                <strong>Investment Objective</strong>
                                                <span class="updown-cion"></span>
                                            </a>
                                        </div>
                                        <div id="collapseThree" class="accordion-body collapse in" aria-expanded="false" style="height: auto;">
                                            <div class="accordion-inner">
                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveIncome]" id="InvestObjectiveIncomeHigh" value="High" <?php echo $profiles[0]->InvestObjectiveIncome == 'High' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveIncome]" id="InvestObjectiveIncomeMedium" value="Medium" <?php echo $profiles[0]->InvestObjectiveIncome== 'Medium' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveIncome]" id="InvestObjectiveIncomeLow" value="Low" <?php echo $profiles[0]->InvestObjectiveIncome== 'Low' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_INVOBJCT_FORMSPEC_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecHigh" value="High" <?php echo $profiles[0]->InvestObjectiveFormSpec == 'High' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecMedium" value="Medium" <?php echo $profiles[0]->InvestObjectiveFormSpec== 'Medium' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecLow" value="Low" <?php echo $profiles[0]->InvestObjectiveFormSpec == 'Low' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_INVOBJCT_INVRISK_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskHigh" value="High" <?php echo $profiles[0]->InvestObjectiveInvestRisk == 'High' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskMedium" value="Medium" <?php echo $profiles[0]->InvestObjectiveInvestRisk== 'Medium' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskLow" value="Low" <?php echo $profiles[0]->InvestObjectiveInvestRisk == 'Low' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false">
                                                <strong>Income and Net Worth</strong>
                                                <span class="updown-cion"></span>
                                            </a>
                                        </div>
                                        <div id="collapseFour" class="accordion-body collapse in" aria-expanded="false" style="height: auto;">
                                            <div class="accordion-inner">
                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_ANNUAL_LBL'); ?></label>
                                                    <div>
                                                        <?php View::form('selecta',array('name'=>'acct[AnnualIncome]','options'=>['0-5000'=>'0-5000 USD','5001-10000'=>'5,001-10,000 USD','10001 - 50000'=>'10,001 - 50,000 USD','50001 - 100000'=>'50,000 - 100,000 USD','100000 Above'=>'100,000 USD Above'],'class'=>'form-control','value'=>$profiles[0]->AnnualIncome)); ?>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_NET_LBL'); ?></label>
                                                    <div>
                                                        <?php View::form('selecta',array('name'=>'acct[NetWorth]','options'=>['0-5000'=>'0-5000 USD','5001-10000'=>'5,001-10,000 USD','10001 - 50000'=>'10,001 - 50,000 USD','50001 - 100000'=>'50,000 - 100,000 USD','100000 Above'=>'100,000 USD Above'],'class'=>'form-control','value'=>$profiles[0]->NetWorth)); ?>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_DESC_LBL'); ?></label>
                                                    <div>
                                                        <?php View::form('selecta',array('name'=>'acct[SourceOfFunds]','options'=>['Employment of Wages'=>'Employment of Wages','Gift'=>'Gift','Inheritance/Trust'=>'Inheritance/Trust','Investments'=>'Investments','Legal Settlement'=>'Legal Settlement','Lottery/Gaming'=>'Lottery/Gaming','Retirement Funds'=>'Retirement Funds','Savings'=>'Savings','Spouse/Parent Support'=>'Spouse/Parent Support','Unemployment/Disability'=>'Unemployment/Disability','Other'=>'Other'],'class'=>'form-control','value'=>$profiles[0]->SourceOfFunds)); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false">
                                                <strong>Affiliations &amp; Disclosures</strong>
                                                <span class="updown-cion"></span>
                                            </a>
                                        </div>
                                        <div id="collapseFive" class="accordion-body collapse in" aria-expanded="false" style="height: auto;">
                                            <div class="accordion-inner">
                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_AFFDISC_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[AssociatedWithBroker]" id="AssociatedWithBrokerN" value="0" <?php echo $profiles[0]->AssociatedWithBroker == '0' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[AssociatedWithBroker]" id="AssociatedWithBrokerY" value="1" <?php echo $profiles[0]->AssociatedWithBroker == '1' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_AFFDISC_PUBTRADED_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[PublicTradedCompany]" id="PublicTradedCompanyN" value="0" <?php echo $profiles[0]->PublicTradedCompany == '0' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[PublicTradedCompany]" id="PublicTradedCompanyY" value="1" <?php echo $profiles[0]->PublicTradedCompany == '1' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_AFFDISC_INSIDERS_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[AffInsiders]" id="AffInsidersN" value="0" <?php echo $profiles[0]->AffInsiders == '0' ? 'checked' : ''; ?> /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[AffInsiders]" id="AffInsidersY" value="1" <?php echo $profiles[0]->AffInsiders == '1' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_AFFDISC_POLITICS_LBL'); ?></label>
                                                    <div>
                                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                                            <input type="radio" class="flat" name="acct[PoliticalExposedPerson]" id="PoliticalExposedPersonN" value="0" <?php echo $profiles[0]->PoliticalExposedPerson == '0' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                        </label>
                                                        <label class="css-input css-radio css-radio-primary">
                                                            <input type="radio" class="flat" name="acct[PoliticalExposedPerson]" id="PoliticalExposedPersonY" value="1" <?php echo $profiles[0]->PoliticalExposedPerson == '1' ? 'checked' : ''; ?>  /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false">
                                                <strong>Stock Trading Experiences</strong>
                                                <span class="updown-cion"></span>
                                            </a>
                                        </div>
                                        <div id="collapseSix" class="accordion-body collapse in" aria-expanded="false" style="height: auto;">
                                            <div class="accordion-inner">
                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_STOCK_YRS_LBL'); ?></label>
                                                    <div>
                                                        <?php View::form('selecta',array('name'=>'acct[YearsOfTrading]','options'=>range(0, 30),'class'=>'form-control','value'=>$profiles[0]->YearsOfTrading)); ?>
                                                    </div>
                                                </div>

                                                <div class="accordion-inner">
                                                    <label><?php echo Lang::get('USR_PRF_STOCK_TRADESPAST3_LBL'); ?></label>
                                                    <div>
                                                        <?php View::form('selecta',array('name'=>'acct[TradesMadePast3Yrs]','options'=>range(0, 30),'class'=>'form-control','value'=>$profiles[0]->TradesMadePast3Yrs)); ?>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group<?php echo $approvehide; ?>">
                                    <div class="text-center">
                                        <a class="btn btn-rounded btn-info" href="/users/profile/details"><i class="si si-action-undo push-5-r"></i> Back</a>
                                        <button class="btn btn-rounded btn-primary " type="submit" name="btnsubmit">Save</button>
                                        <button class="btn btn-rounded btn-warning " type="submit" value="savenextother" name="btnsubmit">Save & Next <i class="si si-action-redo push-5-l"></i></button>
                                        
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <!--end more about me-->

                    <div id="my-other" class="form-wizard <?php echo $thispage == 'other' ? 'active' : ''; ?>">
                        <form class="form-horizontal form-ui no-padding form-label-left input_mask" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="updatepersonal" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="avatarid" value="<?php echo $userinfo->Avatar; ?>" />
                            <input type="hidden" name="status" value="<?php echo $profiles[0]->Status; ?>" />

                            <div class="block-content push-20"><?php echo $info; ?></div>

                                <div class="block-content">

                                <div class="form-group">
                                    <div class="form-left">
                                        <div class="avatar-container personal">
                                        <?php $avatar = View::common()->getUploadedFiles( $userinfo->Avatar ); ?>
                                        <?php echo View::photo( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png', "Avatar" ); ?>
                                        </div>
                                    </div>
                                    <div class="form-right">
                                        <label><?php echo Lang::get('USR_PRF_PPCTURE'); ?> </label>
                                        <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                        <span class="text-muted">Allowed file types: jpeg, jpg, png</span>                                   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><?php echo Lang::get('USR_PRF_GNDR'); ?> <span class="required">*</span></label>
                                    <div>
                                        <label class="css-input css-radio css-radio-primary push-10-r">
                                            <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo isset( $userinfo->Gender ) && $userinfo->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                        </label>
                                        <label class="css-input css-radio css-radio-primary">
                                            <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo isset( $userinfo->Gender ) && $userinfo->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-left">
                                        <label>Occupation</label>
                                        <input type="text" value="<?php echo $userinfo->Occupation; ?>" name="meta[Occupation]" class="form-control">
                                    </div>
                                    <div class="form-right">
                                        <label>Job Title</label>
                                        <input type="text" value="<?php echo $userinfo->JobTitle; ?>" name="meta[JobTitle]" class="form-control">
                                    </div>
                                </div>

                                <?php echo $changeStatusHTML; ?>

                                <br><hr><br>
                                <div class="ln_solid"></div>
                                <div class="form-group<?php echo $approvehide; ?>">
                                    <div class="text-center">
                                        <button class="btn btn-rounded btn-primary " type="submit" name="btnsubmit">Save</button>
                                        <a class="btn btn-rounded btn-info " href="/users/profile/more-about-me"><i class="si si-action-undo push-5-r"></i> Back</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- END Main Content -->

                <!-- modal profile request -->
                <div class="modal fade" id="modal-updaterequest" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                        <div class="modal-content">
                            <form method="post" enctype="multipart/form-data">
                                <input type="hidden" name="action" value="saveupdaterequest" />
                                <input type="hidden" name="UserID" value="<?php echo isset( $request->UserID ) ? $request->UserID : '0'; ?>" />
                                <input type="hidden" name="RequestID" value="<?php echo isset( $request->RequestID ) ? $request->RequestID : '0'; ?>" />
                                <input type="hidden" name="ProofID" value="<?php echo isset( $request->ProofID ) ? $request->ProofID : '0'; ?>" />
                                <input type="hidden" name="RequestAvatar" value="<?php echo isset( $request->RequestAvatar ) ? $request->RequestAvatar : '0'; ?>" />
                                <input type="hidden" name="AddressPhoto" value="<?php echo isset( $request->AddressPhoto ) ? $request->AddressPhoto : '0'; ?>" />
                                <div class="block block-themed block-transparent remove-margin-b">
                                    <div class="block-header bg-primary-dark">
                                        <ul class="block-options">
                                            <li>
                                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                            </li>
                                        </ul>
                                        <h3 class="block-title">Update Request</h3>
                                    </div>
                                    <div class="block-content">
                                        <div class="row push-15">
                                            <div class="col-xs-4 col-md-6">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" placeholder="" name="request[FirstName]" value="<?php echo $request->FirstName; ?>"">
                                            </div>
                                            <div class="col-xs-4 col-md-6">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" placeholder="" name="request[LastName]" value="<?php echo $request->LastName; ?>"">
                                            </div>
                                        </div>
                                        <div class="row push-15">
                                            <div class="col-xs-4 col-md-6">
                                                <label><?php echo Lang::get('USR_PRF_PPCTURE'); ?> </label>
                                                <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                                <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                            </div>
                                            <div class="col-xs-4 col-md-6">
                                                <?php echo View::photo( isset( $request->AvatarData[0] ) ? 'files'.$request->AvatarData[0]->FileSlug : 'images/user.png', "Avatar" ); ?>
                                            </div>
                                        </div>
                                        <div class="row push-15">
                                            <div class="col-xs-4 col-md-6">
                                                <label>Occupation</label>
                                                <input type="text" class="form-control" placeholder="" name="request[Occupation]" value="<?php echo $request->Occupation; ?>"">
                                            </div>
                                            <div class="col-xs-4 col-md-6">
                                                <label>Job Title</label>
                                                <input type="text" class="form-control" placeholder="" name="request[JobTitle]" value="<?php echo $request->JobTitle; ?>"">
                                            </div>
                                        </div>
                                        <div class="row push-15">
                                            <div class="col-md-12 col-lg-12">
                                                <label>Address</label>
                                                <input type="text" class="form-control" placeholder="" name="request[Address]" value="<?php echo $request->Address; ?>">
                                            </div>
                                        </div>
                                        <div class="row push-15">
                                            <div class="col-xs-4 col-md-6">
                                                <label>City</label>
                                                <input type="text" class="form-control" placeholder="" name="request[City]" value="<?php echo $request->City; ?>">
                                            </div>
                                            <div class="col-xs-4 col-md-6">
                                                <label>State</label>
                                                <input type="text" class="form-control" placeholder="" name="request[State]" value="<?php echo $request->State; ?>">
                                            </div>
                                        </div>
                                        <div class="row push-15">
                                            <div class="col-xs-4 col-md-6">
                                                <label>Country</label>
                                                <select name="request[Country]" class="form-control form-control" required="required">
                                                    <?php foreach(AppUtility::getCountries() as $country) { ?>
                                                        <option value="<?php echo $country; ?>" <?php echo $request->Country == $country ? 'selected' : ''; ?>><?php echo $country; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-xs-4 col-md-6">
                                                <label>Postal Code</label>
                                                <input type="text" class="form-control" placeholder="" name="request[PostalCode]" value="<?php echo $request->PostalCode; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <br/>
                                            <?php 
                                                $temp = isset( $request->AddressPhoto ) && $request->AddressPhoto != '' ? $request : $profiles[0];
                                                echo isset( $request ) ? AppUtility::getProoOfAddressFile( $temp, true ) : '';
                                                // $fgID = isset( $proofs[0]->FileGroupID ) ? $proofs[0]->FileGroupID : '0';
                                                // echo AppUtility::getCustomFileList( $fgID.'-SeederProfileDocs'.$ctr++, isset( $proofs[0] ) ? $proofs[0] : false, false );

                                                // $temp = new stdClass;
                                                // $temp->DocumentName = 'Proof of Address';
                                                // echo AppUtility::getCustomFileList( '0-SeederProfileDocs'.$ctr++, $temp, true );
                                            ?>
                                            <label class="text-info change-address">*On address change, new proof of address must be uploaded!</label>
                                        </div>

                                        <!-- <div class="row">
                                            <div class="form-group">
                                                <div class="text-center">
                                                </div>
                                            </div>
                                        </div> -->

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-rounded btn-primary text-uppercase" type="submit"><i class="fa fa-check"></i> Submit Request</button>
                                    <button class="btn btn-rounded btn-info text-uppercase" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END modal profile request -->
            </div>
        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        $(".remove-form").removeClass( 'hidden' );
        $( '.remove-form' ).click( function(){
            var cfid = $( this ).attr( 'data-id' );
            removeForm( cfid );
        });
    });

    $( '#addDocForm' ).click( function( e ){
        e.preventDefault();
        var count = $( '#counter' ).val();
        var fileArray = '0-SeederProfileDocs'+count;
        $.ajax({
            type: "GET",
            url: '/clients/createFileForm/'+fileArray,
            dataType: 'json',
            success: function( data ){
                $( '#addDocForm' ).before( data );
                $( '.remove-form' ).removeClass( 'hidden' );
                $( '.remove-form' ).click( function(){
                    var cfid = $( this ).attr( 'data-id' );
                    removeForm( cfid );
                });

                var $input = $('input.file[type=file]');
                if( $input.length ){
                    $input.fileinput({ maxFileSize: 8000 });
                }
            }
        });

        $( '#counter' ).val( ++count );
    });

    function removeForm( cfid ){
        var count = $( '#counter' ).val();
        $( '#counter' ).val( --count );
        $( '#'+cfid ).remove();
    }

    function validateForm( btn ){
        var proceed = true;
        $( ".docs-input" ).each( function( index ){
            var name = $( this ).val().trim(); //get document name value
            var thisname = $( this ).attr( 'name' ); //get the name of this input
            var stripped = thisname.replace( 'DN-', '' ); //process this name for file input reference
            var hide = $("input[id="+thisname+"]").val(); //get hidden input value
            try{
                var file = $("input[name="+stripped+"]").val(); //get file input value
                var split = hide.split( '|' ); //do split to avoid multiple appending of document name, always preserved index 0 as FileGroupID

                $("input[id="+thisname+"]").val( split[0] + '|' + name ); //append document name to hidden input value

                if( file.length > 0 && name.length == 0 ){
                    proceed = false;
                    alert( 'Please provide a document name or delete the upload form!' );
                    return false;
                }
            } catch( err ){}
        });
        if( proceed ){ document.getElementById( btn ).click() };

        return true;
    }
</script>