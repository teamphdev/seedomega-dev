<?php 
View::$title = 'Cash-In Approved';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="row">
                    <div class="col-xs-12 col-lg-6">
                        <div class="block" style="min-height: 147px;">
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title font-w400 animated fadeIn">CASH IN</h3>
                            </div>
                            <div class="block-content">
                                <ul class="nav nav-pills nav-stacked push">
                                    <li class="">
                                        <a href="/wallet/cashins/pending">
                                            <i class="si si-clock push-10-r"></i> Pending
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="/wallet/cashins/approved">
                                            <i class="si si-check push-10-r"></i> Approved
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="block" style="min-height: 147px;">
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title font-w400 animated fadeIn">CASH OUT</h3>
                            </div>
                            <div class="block-content">
                                <ul class="nav nav-pills nav-stacked push">
                                    <li class="">
                                        <a href="/wallet/cashouts/pending">
                                            <i class="si si-clock push-10-r"></i> Pending
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="/wallet/cashouts/approved">
                                            <i class="si si-check push-10-r"></i> Approved
                                        </a>
                                    </li>
                                </ul>

                            </div>
                            <div class="clear"></div>
                            <div class="push-20"></div>
                        </div>
                    </div>
                </div>                
            </div>

            <?php /* ?>
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="block">
                    <div class="block-header bg-gray-lighter">
                        <h3 class="block-title">Search</h3>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12 col-lg-7">
                                <form method="post" action="" class="form-inline">
                                    <div class="form-group text-left">
                                        <label style="display: inline-block;">Type</label><br>
                                        <select name="navproductitem" class="form-control">
                                            <option value="">-</option>
                                            <option value="Credit">Credit</option>
                                            <option value="Debit">Debit</option>
                                        </select>
                                    </div>
                                    <div class="form-group text-left">
                                        <label style="display: inline-block;"><?php echo Lang::get('RPTS_FILBYDATE'); ?></label><br>
                                        <input type="text" name="navdates" class="form-control daterange-filter" placeholder="FROM - TO" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">&nbsp;</label><br>
                                        <input type="submit" class="btn btn-info" style="margin: 0;" value="FILTER">
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12 col-lg-5 form-inline">
                                <div class="form-group text-left push-5-t">
                                    <label class="font-w400 text-muted animated fadeIn"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label><br>
                                    <a class="h2 font-w300 text-primary" href="#">$ <?php echo number_format($mybalance,2) ?></a>
                                </div>
                            </div>
                        </div>

                        <div class="push-20"></div>
                    </div>
                </div>
            </div>
            <?php */ ?>
            

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <!-- Tickets List -->
                <div class="block">
                    <div class="block-header bg-gray-lighter">                        
                        <div class="block-title text-normal">
                            <span class="font-w400"><?php echo View::$title; ?></span>
                        </div>
                    </div>
                    <div class="" style="min-height: 300px;">
                        <table id="cashin-statement" class="table table-hover table-vcenter">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="text-center" style="width: 100px;">ID</th>
                                    <th class="text-center" style="width: 150px;">Name</th>
                                    <th class="visible-lg" width="220">Description</th>
                                    <th class="hidden-xs text-center">Date</th>
                                    <th class="text-center">TT Receipt</th>
                                    <th class="text-right">Amount</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $cntr = 0;
                                    if(count($transdata)) {
                                        foreach($transdata as $data) { $cntr++; ?>

                                            <tr>
                                                <td><?php echo ($data->TransactionDate != '0000-00-00 00:00:00') ? date('Y-m-d H:i:s', strtotime($data->TransactionDate)) : '-'; ?></td>
                                                <td class="text-center">
                                                    <strong>ID.<?php echo $data->WalletID; ?></strong>
                                                </td>
                                                <td class="text-muted">
                                                    <?php echo ($data->FullName) ? $data->FullName : '-'; ?>
                                                </td>
                                                <td class="visible-lg">
                                                    <?php echo ($data->Description) ? $data->Description : '-'; ?><br>
                                                    <?php echo ($data->TransactionStatus == 'Approved') ? '<small class="text-info">Approved</small>' : '<small class="text-warning">Pending</small>'; ?>
                                                </td>
                                                <td class="hidden-xs text-center"><?php echo ($data->TransactionDate != '0000-00-00 00:00:00') ? date('M d Y', strtotime($data->TransactionDate)) : '-'; ?></td>
                                                
                                                <td class="text-center">
                                                    <?php if($data->TTreceipt){ ?>
                                                        <a href="<?php echo View::url('assets/files'.$data->TTReceiptFileSlug); ?>" class="html5lightbox text-muted"><i class="si si-picture fa-2x"></i></a>
                                                    <?php }else{ ?>
                                                        <span>-</span>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-right text-success">
                                                    <strong> $ <?php echo number_format($data->TransactionAmount,2) ?></strong>
                                                </td>
                                                <td class="text-center text-muted">
                                                    <a href="/wallet/cashins/pending/<?php echo $data->WalletID;?>" class="btn btn-sm btn-rounded btn-default" type="button"><i class="si si-eye"></i> View</a>
                                                </td>
                                            </tr>

                                        <?php } 
                                    }else{ ?>
                                        <tr>
                                            <td>No Records</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    <?php } ?>
                                
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- END Tickets List -->
            </div>
        </div>

    </div>
</section>

<!-- Details Modal -->
<div class="modal" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog push-100-t">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <!-- <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">View Details</h3>
                </div> -->
                <div class="block-content form-ui">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-down field-icon fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="field1">$0.00</span> <br><span class="h4 font-w300 field8">-</span></div>
                    </div>                  

                    <table class="table remove-margin-b bordered-table font-s13 bordered-table">
                        <tr>
                            <td>Status</td>
                            <td class="text-right field9">-</td>
                        </tr>
                        <tr>
                            <td>Transaction ID</td>
                            <td class="text-right field11">-</td>
                        </tr>
                        <tr>
                            <td>Method</td>
                            <td class="text-right methodtype">-</td>
                        </tr>
                        <tr>
                            <td>Transaction Date</td>
                            <td class="text-right field7">-</td>
                        </tr>
                    </table>

                    <div class="bank-details push-20-t" style="display: none;">
                        <label>Bank Details</label>
                        <table class="table remove-margin-b bordered-table font-s13 bordered-table">
                            <tr>
                                <td>Bank Name</td>
                                <td id="b-field1" class="text-right field2">-</td>
                            </tr>
                            <tr>
                                <td>SwiftCode</td>
                                <td id="b-field2" class="text-right field5">-</td>
                            </tr>
                            <tr>
                                <td>Account Name</td>
                                <td id="b-field3" class="text-right field4">-</td>
                            </tr>
                            <tr>
                                <td>Account Number</td>
                                <td id="b-field4" class="text-right field3">-</td>
                            </tr>
                        </table>
                    </div>

                    <div class="push-20-t">
                        <label>Remarks</label>
                        <table class="table remove-margin-b font-s13 bordered-table">
                            <tr>
                                <td class="field12">-</td>
                            </tr>
                        </table>
                    </div>

                    <div class="push-20"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#cashin-statement").DataTable({
            "pageLength": 25,
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ]
            // searching: false, paging: false
        });
    });
    
</script>