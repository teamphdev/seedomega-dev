<?php 
View::$title = 'Cash In Details';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]."/".View::$segments[1].'/'.View::$segments[2]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]."/".View::$segments[1].'/'.View::$segments[2].'/'.View::$segments[3]); ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block block-themed centered max800">
            <div class="block-header bg-primary">
                <h3 class="block-title"><?php echo View::$title; ?> : <?php echo ($walletData->FullName) ? $walletData->FullName : '-'; ?></h3>
            </div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-lg-6">
                        <div class="font-w400 text-muted">Transaction ID</div>
                        <div class="h5 font-w300 text-primary">ID.<?php echo $walletData->WalletID ; ?></div>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="font-w400 text-muted animated fadeIn">DATE</div>
                        <div class="h5 font-w300 text-primary"><?php echo ($walletData->TransactionDate != '0000-00-00 00:00:00') ? date('M j Y g:i a', strtotime($walletData->TransactionDate)) : '' ; ?></div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="clear"></div>
            <div class="block-content form-ui">

                <form id="form-data" class="form-horizontal form-ui form-label-left input_mask" action="" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="updatedata">
                    <input type="hidden" name="walletid" value="<?php echo $walletData->WalletID ; ?>">
                    <input type="hidden" name="usrid" value="<?php echo $walletData->UserID ; ?>">
                    <input type="hidden" name="description" value="<?php echo ($walletData->Description) ? $walletData->Description : '-'; ?>">
                    <input type="hidden" name="ttfileid" value="<?php echo ($walletData->TTreceipt) ? $walletData->TTreceipt : ''; ?>">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-down fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="text-success">$ <?php echo ($walletData->TransactionAmount) ? number_format($walletData->TransactionAmount,2) : ''; ?></span> <br><span class="h4 font-w300"><?php echo ($walletData->Description) ? $walletData->Description : '-'; ?></span></div>
                    </div>                  

                    <table class="table remove-margin-b bordered-table text-muted font-s13" style="border: 1px solid #f0f0f0;">
                        <tr>
                            <td>Status</td>
                            <td class="text-right text-info font-w700"><?php echo ($walletData->TransactionStatus) ? $walletData->TransactionStatus : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Bank Name</td>
                            <td class="text-right"><?php echo ($coreBank->Name) ? $coreBank->Name : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account name</td>
                            <td class="text-right"><?php echo ($coreBank->AccountName) ? $coreBank->AccountName : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account number</td>
                            <td class="text-right"><?php echo ($coreBank->AccountNumber) ? $coreBank->AccountNumber : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account Address</td>
                            <td class="text-right"><?php echo ($coreBank->Address) ? $coreBank->Address : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>CCV</td>
                            <td class="text-right"><?php echo ($coreBank->CCV) ? $coreBank->CCV : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Method</td>
                            <td class="text-right"><?php echo ($walletData->MethodName) ? $walletData->MethodName : '-'; ?></td>
                        </tr>
                    </table>                    

                    <div class="col-lg-12 push-20-t">
                        <?php echo AppUtility::getWalletTTReceipt($walletData); ?>
                    </div>
                    <div class="clear"></div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Remarks</label>
                            <textarea class="form-control" name="wallet[Remarks]"><?php echo ($walletData->Remarks) ? $walletData->Remarks : '-'; ?></textarea>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="form-group row">
                        <div class="col-xs-12 text-center">
                            <label for="">Mark As</label><br>
                            <button id="btn-reject" class="btn btn-rounded btn-danger push-10-t" type="submit" name="submit-btn" value="Reject" onclick="return confirm('Are you sure you want to Reject?');" <?php echo ($walletData->TransactionStatus == 'Rejected') ? 'disabled' : ''; ?>><i class="si si-close push-5-r"></i> Reject</button>
                            <button id="btn-update" class="btn btn-rounded btn-warning push-10-r push-10-l push-10-t" type="submit" name="submit-btn" value="Pending">Pending</button>
                            <button id="btn-update" class="btn btn-rounded btn-primary push-10-r push-10-l push-10-t" type="submit" name="submit-btn" value="Verified">Verified</button>
                            <?php if( User::can( 'Approve Cash In' ) ){ ?>
                                <button id="btn-approved" class="btn btn-rounded btn-info push-10-t" type="submit" name="submit-btn" value="Approved" onclick="return confirm('Are you sure you want to Approved?');" <?php echo ($walletData->TransactionStatus == 'Approved') ? 'disabled' : ''; ?>><i class="si si-check push-5-r"></i> Approved</button>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="clear"></div>

                </form>

            </div>

        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        // $( "#acknowledge-btn" ).click(function(){
        //     $( "#form-data" ).submit();
        // })    
        
    });
</script>