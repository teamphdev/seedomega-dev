<?php 
View::$title = 'Account Statements';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="row">
                    <div class="col-xs-12 col-lg-6">
                        <div class="block" style="min-height: 147px;">
                            <div class="block-content wallet-box">
                                <div class="animated fadeIn"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</div>
                                <a class="h2 text-primary animated" href="#">$ <?php echo number_format($mybalance,2) ?></a>

                                <div class="push-20-t"></div>
                                <a class="btn btn-success btn-rounded" href="/wallet/deposit"><i class="si si-arrow-down push-5-r"></i> Cash-In</a>
                                <a class="btn btn-danger btn-rounded" href="/wallet/withdraw"><i class="si si-arrow-up push-5-r"></i> Cash-Out</a>
                                <?php /* 
                                <a class="btn btn-sm btn-danger btn-rounded" href="/wallet/transfer"><i class="fa fa-send-o"></i> Send</a>
                                */ ?>
                                <div class="push-20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="block" style="min-height: 147px;">
                            <div class="">
                                <ul class="nav nav-pills nav-stacked push">
                                    <li class="">
                                        <a href="/wallet/transactions">
                                            <i class="si si-list push-10-r"></i> Transaction History
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/wallet/transactions/pending">
                                            <?php echo ($pendings) ? '<span class="badge pull-right">'.count($pendings).'</span>' : ''; ?><i class="si si-clock  push-10-r"></i> Pending Transactions
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <div class="push-20"></div>
                        </div>
                    </div>
                </div>                
            </div>

            <?php /* ?>
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="block">
                    <div class="block-header bg-gray-lighter">
                        <h3 class="block-title">Search</h3>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12 col-lg-12 text-left">
                                <form method="post" action="" class="form-inline push-20-t">
                                    <div class="form-group text-left">
                                        <label style="display: inline-block;">Type</label><br>
                                        <select name="transtype" class="form-control">
                                            <option value="">-</option>
                                            <option value="Credit">Credit</option>
                                            <option value="Debit">Debit</option>
                                        </select>
                                    </div>
                                    <div class="form-group text-left">
                                        <label style="display: inline-block;"><?php echo Lang::get('RPTS_FILBYDATE'); ?></label><br>
                                        <input type="text" name="filterdate" class="form-control wallet-datefilter" placeholder="FROM - TO" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">&nbsp;</label><br>
                                        <input type="submit" class="btn btn-info" style="margin: 0;" value="FILTER">
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="push-20"></div>
                    </div>
                </div>
            </div>
            <?php */ ?>            

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <!-- Tickets List -->
                <div class="block">
                    <div class="block-header bg-gray-lighter">
                        
                        <div class="block-title text-normal">
                            <span class="font-w400">E-Statement</span>
                        </div>
                    </div>
                    <div class="table-responsive" style="min-height: 300px;">
                        <table id="wallet-statement" class="table table-hover table-vcenter" addbutton='<form method="post" action="" class="form-inline">
                                    <div class="form-group text-left">
                                        <select name="transtype" class="form-control">
                                            <option value="">Type</option>
                                            <option value="Credit">Credit</option>
                                            <option value="Debit">Debit</option>
                                        </select>
                                    </div>
                                    <div class="form-group text-left">
                                        <input type="text" name="filterdate" class="form-control wallet-datefilter" placeholder="<?php echo Lang::get('RPTS_FILBYDATE'); ?>" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary btn-rounded" style="margin: 0;" value="FILTER">
                                    </div>
                                </form>'>
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="text-center" style="width: 100px;">ID</th>
                                    <th class="" width="420">Description</th>
                                    <th class=" text-center">Date</th>
                                    <th class="text-right">Credit</th>
                                    <th class="text-right">Debit</th>
                                    <th class=" text-right">Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $cntr = 0;
                                    if(count($statements)) {
                                        $runningbal = 0;
                                        $prevval = 0;
                                        foreach(array_reverse($statements) as $data) { $cntr++;
                                            // Calculate Running Balance
                                            switch ($data->TransactionType) {
                                                case 'Credit':
                                                $runningbal = $prevval+str_replace('-', '', $data->TransactionAmount);
                                                break;
                                            case 'Debit':
                                                $runningbal = $prevval-str_replace('-', '', $data->TransactionAmount);
                                                break;
                                            }
                                            $prevval = $runningbal;

                                            // Data Method Switch
                                            switch (true) {
                                                case ($data->Method == 1):
                                                    $datamethod = 'cashin';
                                                    break;
                                                case ($data->Method == 4):
                                                    $datamethod = 'cashoutbanktransfer';
                                                    break;
                                                case ($data->Method == 2):
                                                    $datamethod = 'cashoutpickup';
                                                    break;
                                                case ($data->TransactionType == 'Debit' AND $data->Method == 3):
                                                    $datamethod = 'transfersend';
                                                    break;
                                                case ($data->TransactionType == 'Credit' AND $data->Method == 3):
                                                    $datamethod = 'transferreceived';
                                                    break;
                                            }

                                            // For Attributes Data
                                            $attrAmt            = number_format($data->TransactionAmount,2);
                                            $attrBankName       = ($data->Name) ? $data->Name : '-';
                                            $attrAccntNumber    = ($data->AccountNumber) ? $data->AccountNumber : '-';
                                            $attrAccntName      = ($data->AccountName) ? $data->AccountName : '-';
                                            $attrSwiftcode      = ($data->SwiftCode) ? $data->SwiftCode : '-';
                                            $attrTranstype      = ($data->TransactionType) ? $data->TransactionType : '-';
                                            $attrTransDate      = ($data->TransactionDate != '0000-00-00 00:00:00') ? date('M j Y g:i a', strtotime($data->TransactionDate)) : '-';
                                            $attrDescription    = ($data->Description) ? $data->Description : '-';
                                            $attrTransStatus    = ($data->TransactionStatus) ? $data->TransactionStatus : '';
                                            $attrTransMethod    = ($data->Method) ? $data->Method : '-';
                                            $attrTransID        = ($data->WalletID) ? $data->WalletID : '-';
                                            $attrRemarks        = ($data->Remarks) ? $data->Remarks : '-'; ?>

                                            <tr data-toggle="modal" data-target="#view-modal" datamethod="<?php echo $datamethod; ?>" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                <td><?php echo ($data->ApproveDate != '0000-00-00 00:00:00') ? date('Y-m-d H:i:s', strtotime($data->ApproveDate)) : date('Y-m-d H:i:s', strtotime($data->TransactionDate)); ?></td>
                                                <td class="text-center">
                                                    <strong>ID.<?php echo $data->WalletID; ?></strong>
                                                </td>
                                                <td class=" text-muted">
                                                    <?php echo $data->Description ? $data->Description : '-'; ?>
                                                </td>                                                
                                                <td class="text-center" data-order="<?php echo $data->ApproveDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $data->ApproveDate ) ) : '000000'; ?>">
                                                    <?php echo $data->ApproveDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $data->ApproveDate ) ) : '-'; ?>
                                                </td>
                                                <td class="text-right text-success" data-order="<?php echo isset( $data->TransactionAmount ) ? $data->TransactionAmount : '0'; ?>">
                                                    <?php echo $data->TransactionType == 'Credit' ? '<strong>$'.number_format( $data->TransactionAmount, 2 ).'</strong>' : ''; ?>
                                                </td>
                                                <td class="text-right text-danger" data-order="<?php echo isset( $data->TransactionAmount ) ? $data->TransactionAmount : '0'; ?>">
                                                    <?php echo $data->TransactionType == 'Debit' ? '<strong>($'.number_format( $data->TransactionAmount, 2 ).')</strong>' : ''; ?>
                                                </td>
                                                <td class="text-right  text-muted">
                                                    <strong> $ <?php echo number_format($runningbal,2) ?></strong>
                                                </td>
                                            </tr>

                                        <?php } 
                                    }else{ ?>
                                        <tr>
                                            <td colspan="7">No Records</td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                            <td class="hidden"></td>
                                        </tr>
                                    <?php } ?>
                                
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- END Tickets List -->
            </div>
        </div>

    </div>
</section>

<!-- Details Modal -->
<div class="modal" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog push-100-t">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <!-- <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">View Details</h3>
                </div> -->
                <div class="block-content form-ui">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-down field-icon fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="field1">$0.00</span> <br><span class="h4 font-w300 field8">-</span></div>
                    </div>                  

                    <table class="table remove-margin-b bordered-table font-s13 bordered-table">
                        <tr>
                            <td>Status</td>
                            <td class="text-right field9">-</td>
                        </tr>
                        <tr>
                            <td>Transaction ID</td>
                            <td class="text-right field11">-</td>
                        </tr>
                        <tr>
                            <td>Method</td>
                            <td class="text-right methodtype">-</td>
                        </tr>
                        <tr>
                            <td>Transaction Date</td>
                            <td class="text-right field7">-</td>
                        </tr>
                    </table>

                    <div class="bank-details push-20-t" style="display: none;">
                        <label>Bank Details</label>
                        <table class="table remove-margin-b bordered-table font-s13 bordered-table">
                            <tr>
                                <td>Bank Name</td>
                                <td id="b-field1" class="text-right field2">-</td>
                            </tr>
                            <tr>
                                <td>SwiftCode</td>
                                <td id="b-field2" class="text-right field5">-</td>
                            </tr>
                            <tr>
                                <td>Account Name</td>
                                <td id="b-field3" class="text-right field4">-</td>
                            </tr>
                            <tr>
                                <td>Account Number</td>
                                <td id="b-field4" class="text-right field3">-</td>
                            </tr>
                        </table>
                    </div>

                    <div class="push-20-t">
                        <label>Remarks</label>
                        <table class="table remove-margin-b font-s13 bordered-table">
                            <tr>
                                <td class="field12">-</td>
                            </tr>
                        </table>
                    </div>

                    <div class="push-20"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#wallet-statement").DataTable({
            "pageLength": 25,
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "drawCallback": function( settings ) {
                jQuery(this).each(function(){
                    var addBtn = jQuery(this).attr('addbutton');
                    if(addBtn){
                        jQuery(this).parents().eq(2).find('.table-custom-btn').not('.addedbtn').append(addBtn).addClass('addedbtn');
                    }
                    jQuery(".wallet-datefilter").daterangepicker({
                        showDropdowns: true,
                        linkedCalendars: false,
                        autoUpdateInput: true,
                        locale: {
                          format: 'YYYY-MM-DD',
                          "cancelLabel": "Clear"
                        },
                        ranges: {                          
                           'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().endOf('month')],             
                           'Last 2 Months': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
                           'Last 1 Months': [moment().subtract(1, 'month').startOf('month'), moment().endOf('month')],
                           'Last Week': [moment().startOf('week').subtract(7, 'days'), moment().startOf('week').subtract(1, 'days')],
                           'This Week': [moment().startOf('week'), moment().endOf('week')],
                           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                           'This Month': [moment().startOf('month'), moment().endOf('month')]
                        }
                    });
                });
            },
            dom:
                "<'row filters-table'<'col-sm-6 table-custom-btn'><'col-sm-6 form-inline text-right'lf>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            // searching: false, paging: false
        });
    });

    function getDetails( fieldVal ){

        var datamethod = $( fieldVal ).attr( 'datamethod' );

        var field1 = $( fieldVal ).attr( 'amount' );
        var field2 = $( fieldVal ).attr( 'bankname' );
        var field3 = $( fieldVal ).attr( 'accountnumber' );
        var field4 = $( fieldVal ).attr( 'accountname' );
        var field5 = $( fieldVal ).attr( 'swiftcode' );
        var field6 = $( fieldVal ).attr( 'transtype' );
        var field7 = $( fieldVal ).attr( 'transdate' );
        var field8 = $( fieldVal ).attr( 'description' );
        var field9 = $( fieldVal ).attr( 'transtatus' );
        var field10 = $( fieldVal ).attr( 'method' );
        var field11 = $( fieldVal ).attr( 'transid' );
        var field12 = $( fieldVal ).attr( 'remarks' );

        $( '.field1' ).text( field1 );
        $( '.field2' ).text( field2 );
        $( '.field3' ).text( field3 );
        $( '.field4' ).text( field4 );
        $( '.field5' ).text( field5 );
        $( '.field6' ).text( field6 );
        $( '.field7' ).text( field7 );
        $( '.field8' ).text( field8 );
        $( '.field9' ).text( field9 );
        $( '.field10' ).text( field10 );
        $( '.field11' ).text( '#ID.'+field11 );
        $( '.field12' ).text( field12 );        

        $('.field-icon').removeClass('si-arrow-down si-arrow-up si-share-alt');
        $('.bank-details').hide();

        switch (field6) { 
            case 'Credit': 
                $('.field-icon').addClass('si-arrow-down');
                $('.field1').addClass('text-success');
                break;
            case 'Debit': 
                $('.field-icon').addClass('si-arrow-up');
                $('.field1').addClass('text-danger');
                break;
        }

        switch (datamethod) { 
            // CASH IN : Bank Deposit
            case 'cashin': 
                $('.methodtype').text('Cash In - Bank Deposit');                
                break;

            // CASH OUT : BANK TRANSFER
            case 'cashoutbanktransfer': 
                $('.methodtype').text('Cash Out - Bank Transfer');
                $('.bank-details').show();
                break;

            // CASH OUT : PICKUP
            case 'cashoutpickup': 
                $('.methodtype').text('Cash Out - Cash Pickup');
                break;

            // Wallet Transfer : Sent
            case 'transfersend': 
                $('.methodtype').text('Wallet Transfer');                
                break;

            // Wallet Transfer : Received
            case 'transferreceived': 
                $('.methodtype').text('Wallet Transfer');
                break;
        }
    }
</script>