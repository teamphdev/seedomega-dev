<?php 
View::$title = 'Wallet Transfer';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block block-themed centered max600">
            <div class="block-header bg-primary">
                <h3 class="block-title"><?php echo View::$title; ?> : Transaction Details</h3>
            </div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="transfer">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-share-alt fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="h4 font-w300">Wallet Transfer</div>
                    </div> 

                    <hr class="push-20">

                    <div class="form-group push-20-t">
                        <div id="wrapchecker" class="col-xs-12 col-lg-12">
                            <label class="font-w400 text-muted animated fadeIn"><i class="si si-user push-5-r"></i>Send To</label>
                            <input id="ToUserID" value="" name="wallet[ToUserID]" type="hidden">
                            <input id="UserSearch" type="text" value="" class="form-control" data-value="" placeholder="User ID / Email Address" rel="<?php echo View::url('ajax/getuserInfo'); ?>">
                            <span id="searchloading" class="loading-icon"><i class="si si-refresh"></i></span>
                            <div id="userdataSearch" class="help-block animated fadeInDown">-</div>
                        </div>
                    </div>

                    <div class="form-group push-20-t">
                        <div class="col-xs-12 col-lg-6 push-10">
                            <label class="font-w400 text-muted animated fadeIn">Running Balance</label>
                            <input type="text" value="$ <?php echo number_format($mybalance,2) ?>" class="form-control" readonly="">
                        </div>
                        <div class="col-xs-12 col-lg-6 push-10">
                            <label class="font-w400 text-muted animated fadeIn">Amount</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input id="amt-input" type="text" name="wallet[TransactionAmount]" value="" required="required" class="form-control money-mask" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <label class="css-input css-checkbox css-checkbox-info">
                                <input type="checkbox" value="" required="required"> <span></span> I/We Read and verified the above
                            </label><br>
                            <button id="submit-btn" class="btn btn-4 blue green" type="submit" disabled=""><i class="fa fa-check push-5-r"></i> Transfer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {    
        
        $('#amt-input').on('change keyup keypress',function(){
            $balance = <?php echo $mybalance; ?>;
            $val = $(this).val().replace(/,/g, '')
            if(parseInt($val) > parseInt($balance)){
                alert('You exceed the running balance '+commaSeparateNumber($balance));
                $(this).val(commaSeparateNumber($balance));
            }
        })
    }); 
</script>