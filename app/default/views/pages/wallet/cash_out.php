<?php 
View::$title = 'Cash Out';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block block-themed centered max600">
            <div class="block-header bg-primary">
                <h3 class="block-title"><?php echo View::$title; ?> : Transaction Details</h3>
            </div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="cashout">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-up fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="h4 font-w300">Cash Out via</span> <span class="h4 font-w300">Bank Deposit</span></div>
                    </div> 

                    <hr class="push-20">

                    <div class="form-group">
                        <div class="col-xs-12 col-lg-12">
                            <label>How would you like to Cash-Out?</label>
                            <div>
                                <label class="css-input css-checkbox css-checkbox-info push-10-r">
                                    <input name="wallet[Method]" type="radio" class="co-method" value="4" required="required" checked=""><span></span> Bank Transfer
                                </label>

                                <label class="css-input css-checkbox css-checkbox-info push-10-r">
                                    <input name="wallet[Method]" type="radio" class="co-method" value="2" required="required"><span></span> Cash Pickup
                                </label>
                            </div>
                        </div>                      
                    </div>

                    <div class="bank-section">

                        <?php if (count( $bankaccounts )) { ?>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">Bank Account</span>
                                        <select id="co-bankaccounts" class="form-control" name="wallet[BankAccountID]" required="">
                                            <option value="">Please Select</option>
                                            <?php foreach ($bankaccounts as $bank) { ?>
                                                <option value="<?php echo $bank->BankAccountID; ?>|<?php echo $bank->Name; ?>|<?php echo $bank->SwiftCode; ?>|<?php echo $bank->AccountName; ?>|<?php echo $bank->AccountNumber; ?>">
                                                    <?php echo ($bank->Name) ? $bank->Name : ''; ?> 
                                                    ( <?php echo ($bank->AccountNumber) ? $bank->AccountNumber : ''; ?> )
                                                </option>
                                            <?php } ?>
                                        </select>                                    
                                    </div>
                                    <input id="bankaccntid" name="wallet[BankAccountID]" type="hidden" value="">
                                </div>
                            </div>

                            <div id="banksec-details" class="form-group">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="font-w300 text-muted animated fadeIn">Bank Name</div>
                                    <div class="h5 font-w400 text-primary push-5" id="bank-field1">-</div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="font-w300 text-muted animated fadeIn">SwiftCode</div>
                                    <div class="h5 font-w400 text-primary push-5" id="bank-field2">-</div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="font-w300 text-muted animated fadeIn">Account Name</div>
                                    <div class="h5 font-w400 text-primary push-5" id="bank-field3">-</div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="font-w300 text-muted animated fadeIn">Account Number</div>
                                    <div class="h5 font-w400 text-primary push-5" id="bank-field4">-</div>
                                </div>
                            </div>
                        <?php }else{ ?>

                            <div class="bank-form">
                                <div class="form-group">
                                    <div class="col-xs-12 col-lg-6">
                                        <label class="font-w300 text-muted animated fadeIn">Bank Name</label>
                                        <input type="text" id="ct_BankName" class="form-control banknew" name="bank[Name]" placeholder="Bank Name" value="">
                                    </div>
                                    <div class="col-xs-12 col-lg-6">
                                        <label class="font-w300 text-muted animated fadeIn">SwiftCode</label>
                                        <input type="text" id="ct_SwiftCode" class="form-control banknew" name="bank[SwiftCode]" placeholder="SWIFT Code" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-lg-6">
                                        <label class="font-w300 text-muted animated fadeIn">Account Name</label>
                                        <input type="text" id="ct_AccountName" class="form-control banknew" name="bank[AccountName]" placeholder="Account Name" value="">
                                    </div>
                                    <div class="col-xs-12 col-lg-6">
                                        <label class="font-w300 text-muted animated fadeIn">Account Number</label>
                                        <input type="text" id="ct_AccountNumber" class="form-control banknew" name="bank[AccountNumber]" placeholder="Account Number" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-lg-12">
                                        <label class="font-w300 text-muted animated fadeIn">Bank Address</label>
                                        <input type="text" id="ct_BankAddress" class="form-control banknew" name="bank[Address]" placeholder="Bank Address" value="">
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                    </div>

                    <hr>

                    <div class="form-group push-20-t">
                        <div class="col-xs-12 col-lg-6 push-10">
                            <label class="font-w400 text-muted animated fadeIn"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label>
                            <input type="text" value="$ <?php echo number_format($mybalance,2) ?>" class="form-control" readonly="">
                        </div>
                        <div class="col-xs-12 col-lg-6 push-10">
                            <label class="font-w400 text-muted animated fadeIn">Amount</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input id="amt-input" name="wallet[TransactionAmount]" type="text" value="" required="required" class="form-control money-mask" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <label class="css-input css-checkbox css-checkbox-info">
                                <input type="checkbox" value="" required="required"> <span></span> I/We Read and verified the above
                            </label><br>
                            <button class="btn btn-rounded btn-primary" type="submit"><i class="fa fa-check push-5-r"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {    
        $('.co-method').change(function(){
            if($(this).val() == 4){
                $(".bank-section").slideDown();
                $("#co-bankaccounts").attr('required','required');
                $("#co-bankaccounts").attr('name','wallet[BankAccountID]');
            }else{
                $(".bank-section").slideUp();
                $('#bankaccntid').val('');
                $("#co-bankaccounts").removeAttr('required');
                $("#co-bankaccounts").removeAttr('name');
            }
        });

        $('#co-bankaccounts').change(function(){
            if($(this).val() != ''){
                var $step = $(this).val();
                var $vals = $step.split('|');

                $('#bankaccntid').val($vals[0]);
                $('#bank-field1').text($vals[1]);
                $('#bank-field2').text($vals[2]);
                $('#bank-field3').text($vals[3]);
                $('#bank-field4').text($vals[4]);
            }else{
                $('#bankaccntid').val('');
                $('#bank-field1,#bank-field2,#bank-field3,#bank-field4').text('-');
            }
        });

        $('#amt-input').on('change keyup keypress',function(){
            $balance = <?php echo $mybalance; ?>;
            $val = $(this).val().replace(/,/g, '')
            if(parseInt($val) > parseInt($balance)){
                alert('You exceed the running balance '+commaSeparateNumber($balance));
                $(this).val(commaSeparateNumber($balance));
            }
        })
    }); 
</script>