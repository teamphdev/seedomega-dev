<?php 
View::$title = 'Transactions';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="row">
                    <div class="col-xs-12 col-lg-6">
                        <div class="block" style="min-height: 147px;">
                            <div class="block-content wallet-box">
                                <div class="animated fadeIn"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</div>
                                <a class="h2 text-primary animated" href="#">$ <?php echo number_format($mybalance,2) ?></a>

                                <div class="push-20-t"></div>
                                <a class="btn btn-success btn-rounded" href="/wallet/deposit"><i class="si si-arrow-down push-5-r"></i> Cash-In</a>
                                <a class="btn btn-danger btn-rounded" href="/wallet/withdraw"><i class="si si-arrow-up push-5-r"></i> Cash-Out</a>
                                <?php /* 
                                <a class="btn btn-sm btn-danger btn-rounded" href="/wallet/transfer"><i class="fa fa-send-o"></i> Send</a>
                                */ ?>
                                <div class="push-20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="block" style="min-height: 147px;">
                            <div class="">
                                <ul class="nav nav-pills nav-stacked push">
                                    <li class="">
                                        <a href="/wallet/estatement">
                                            <i class="si si-layers push-10-r"></i> E-Statement
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/wallet/transactions/pending">
                                            <?php echo $pendings ? '<span class="badge pull-right">'.count( $pendings ).'</span>' : ''; ?><i class="si si-clock  push-10-r"></i> Pending Transactions
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <div class="push-20"></div>
                        </div>
                    </div>
                </div>                
            </div>

            <?php /* ?>
            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="block">
                    <div class="block-header bg-gray-lighter">
                        <h3 class="block-title">Search</h3>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12 col-lg-7">
                                <form method="post" action="" class="form-inline">
                                    <div class="form-group text-left">
                                        <label style="display: inline-block;">Type</label><br>
                                        <select name="navproductitem" class="form-control">
                                            <option value="">-</option>
                                            <option value="Credit">Credit</option>
                                            <option value="Debit">Debit</option>
                                        </select>
                                    </div>
                                    <div class="form-group text-left">
                                        <label style="display: inline-block;"><?php echo Lang::get('RPTS_FILBYDATE'); ?></label><br>
                                        <input type="text" name="navdates" class="form-control daterange-filter" placeholder="FROM - TO" value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">&nbsp;</label><br>
                                        <input type="submit" class="btn btn-info" style="margin: 0;" value="FILTER">
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12 col-lg-5 form-inline">
                                <div class="form-group text-left push-5-t">
                                    <label class="font-w400 text-muted animated fadeIn"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label><br>
                                    <a class="h2 font-w300 text-primary" href="#">$ <?php echo number_format($mybalance,2) ?></a>
                                </div>
                            </div>
                        </div>

                        <div class="push-20"></div>
                    </div>
                </div>
            </div>
            <?php */ ?>            

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <!-- Tickets List -->
                <div class="block">
                    <div class="block-header bg-gray-lighter">
                        
                        <div class="block-title text-normal">
                            <span class="font-w400">History</span>
                        </div>
                    </div>
                    <div class="" style="min-height: 300px;">
                        
                        <table id="wallet-transaction" class="table table-hover table-vcenter table-divide">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th class="text-center" style="width: 100px;">ID</th>
                                    <th class="visible-lg">Description</th>
                                    <th class="hidden-xs text-center">Date</th>
                                    <th class="text-right">Method</th>
                                    <th class="text-right">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( count( $historylog ) ){
                                    foreach( $historylog as $trans ){ 
                                        $attrAmt            = number_format( $trans->TransactionAmount, 2 );
                                        $attrBankName       = $trans->Name ? $trans->Name : '-';
                                        $attrAccntNumber    = $trans->AccountNumber ? $trans->AccountNumber : '-';
                                        $attrAccntName      = $trans->AccountName ? $trans->AccountName : '-';
                                        $attrSwiftcode      = $trans->SwiftCode ? $trans->SwiftCode : '-';
                                        $attrTranstype      = $trans->TransactionType ? $trans->TransactionType : '-';
                                        $attrTransDate      = $trans->TransactionDate != '0000-00-00 00:00:00' ? date('M j Y g:i a', strtotime( $trans->TransactionDate ) ) : '-';
                                        $attrDescription    = $trans->Description ? $trans->Description : '-';
                                        $attrTransStatus    = $trans->TransactionStatus ? $trans->TransactionStatus : '';
                                        $attrTransMethod    = $trans->Method ? $trans->Method : '-';
                                        $attrTransID        = $trans->WalletID ? $trans->WalletID : '-';
                                        $attrRemarks        = $trans->Remarks ? $trans->Remarks : '-';

                                        switch( true ){

                                            // CASH IN : VERIFIED
                                            case ( $trans->TransactionStatus == 'Verified' AND $trans->Method == 1 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashinverified" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->ApproveDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->ApproveDate ) ) : date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ); ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?><br>
                                                        <small class="text-info">Awaiting for fund acknowledgement</small>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-success" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>                                                
                                            <?php 
                                            break;

                                            // CASH IN : PENDING
                                            case ( $trans->TransactionStatus == 'Pending' AND $trans->Method == 1 ): ?>
                                                <tr>
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?><br>
                                                        <small class="text-info">Upload your TT receipt.</small>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-success">
                                                        <a href="/wallet/deposit/<?php echo $trans->WalletID; ?>" class="btn btn-rounded btn-default">Upload</a>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // CASH IN : APPROVED
                                            case ( $trans->TransactionStatus == 'Approved' AND $trans->Method == 1 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashinapproved" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo ($trans->TransactionDate != '0000-00-00 00:00:00') ? date('M d Y', strtotime($trans->TransactionDate)) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-success" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WITHDRAWAL : BANK TRANSFER PENDING
                                            case ( $trans->TransactionStatus == 'Pending' AND $trans->Method == 4 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashoutverified" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?> <br>
                                                        <small class="text-warning">PENDING <?php echo isset( $trans->Remarks ) ? ': '.$trans->Remarks : ''; ?></small>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date('M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-danger" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WITHDRAWAL : BANK TRANSFER VERIFIED
                                            case ( $trans->TransactionStatus == 'Verified' AND $trans->Method == 4 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashoutverified" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?> <br>
                                                        <small class="text-info">Awaiting for approval</small>
                                                    </td>
                                                    <td class="h6 hidden-xs text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-danger" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WITHDRAWAL : BANK TRANSFER APPROVED
                                            case ( $trans->TransactionStatus == 'Approved' AND $trans->Method == 4 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashoutapproved" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-danger" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WITHDRAWAL : CASH PICKUP VERIFIED
                                            case ( $trans->TransactionStatus == 'Verified' AND $trans->Method == 2 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashoutpickup" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?> <br>
                                                        <small class="text-info">Awaiting for approval.</small>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-danger" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;  

                                            // WITHDRAWAL : CASH PICKUP APPROVED
                                            case ( $trans->TransactionStatus == 'Approved' AND $trans->Method == 2 ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="cashoutpickupapproved" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-danger" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WALLET TRANSFER : SEND
                                            case ( $trans->TransactionStatus == 'Approved' AND $trans->Method == 3 AND $trans->TransactionType == "Debit" ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="transfersend" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-danger" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WALLET TRANSFER : RECEIVED
                                            case ( $trans->TransactionStatus == 'Approved' AND $trans->Method == 3 AND $trans->TransactionType == "Credit" ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="transferreceived" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-success" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                            // WALLET TRANSFER : RECEIVED
                                            case ( $trans->TransactionStatus == 'Rejected' ): ?>
                                                <tr class="pointer" data-toggle="modal" data-target="#view-modal" datamethod="transrejected" amount="$ <?php echo $attrAmt; ?>" bankname="<?php echo $attrBankName; ?>" accountnumber="<?php echo $attrAccntNumber; ?>" accountname="<?php echo $attrAccntName; ?>" swiftcode="<?php echo $attrSwiftcode; ?>" transtype="<?php echo $attrTranstype; ?>" transdate="<?php echo $attrTransDate; ?>" description="<?php echo $attrDescription; ?>" transtatus="<?php echo $attrTransStatus; ?>" method="<?php echo $attrTransMethod; ?>" transid="<?php echo $attrTransID; ?>" remarks="<?php echo $attrRemarks; ?>" onclick="getDetails(this)">
                                                    <td><?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'Y-m-d H:i:s', strtotime( $trans->TransactionDate ) ) : ''; ?></td>
                                                    <td class="text-center">
                                                        <strong>ID.<?php echo $trans->WalletID; ?></strong>
                                                    </td>
                                                    <td class="visible-lg text-muted">
                                                        <?php echo $trans->Description ? $trans->Description : '-'; ?><br>
                                                        <small class="text-danger">REJECTED <?php echo $trans->Remarks ? ': '.$trans->Remarks : ''; ?></small>
                                                    </td>
                                                    <td class="h6 text-muted text-center" data-order="<?php echo $trans->TransactionDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $trans->TransactionDate ) ) : '000000'; ?>">
                                                        <?php echo $trans->TransactionDate != '0000-00-00 00:00:00' ? date( 'M d Y', strtotime( $trans->TransactionDate ) ) : '-'; ?>
                                                    </td>
                                                    <td class="h5 text-center text-muted h6"><?php echo $trans->MethodName; ?></td>
                                                    <td class="text-right text-muted" data-order="<?php echo isset( $trans->TransactionAmount ) ? $trans->TransactionAmount : '0'; ?>">
                                                        <strong>$<?php echo number_format( $trans->TransactionAmount, 2 ); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php 
                                            break;

                                        } ?>

                                    <?php } 
                                }else{ ?>
                                    <tr>
                                        <td colspan="5">No Records</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- END Tickets List -->
            </div>
        </div>

    </div>
</section>

<!-- Details Modal -->
<div class="modal" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog push-100-t">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <!-- <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">View Details</h3>
                </div> -->
                <div class="block-content form-ui">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-down field-icon fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="field1">$0.00</span> <br><span class="h4 font-w300 field8">-</span></div>
                    </div>                  

                    <table class="table remove-margin-b bordered-table font-s13 bordered-table">
                        <tr>
                            <td>Status</td>
                            <td class="text-right field9">-</td>
                        </tr>
                        <tr>
                            <td>Transaction ID</td>
                            <td class="text-right field11">-</td>
                        </tr>
                        <tr>
                            <td>Method</td>
                            <td class="text-right methodtype">-</td>
                        </tr>
                        <tr>
                            <td>Transaction Date</td>
                            <td class="text-right field7">-</td>
                        </tr>
                    </table>

                    <div class="bank-details push-20-t" style="display: none;">
                        <label>Bank Details</label>
                        <table class="table remove-margin-b bordered-table font-s13 bordered-table">
                            <tr>
                                <td>Bank Name</td>
                                <td id="b-field1" class="text-right field2">-</td>
                            </tr>
                            <tr>
                                <td>SwiftCode</td>
                                <td id="b-field2" class="text-right field5">-</td>
                            </tr>
                            <tr>
                                <td>Account Name</td>
                                <td id="b-field3" class="text-right field4">-</td>
                            </tr>
                            <tr>
                                <td>Account Number</td>
                                <td id="b-field4" class="text-right field3">-</td>
                            </tr>
                        </table>
                    </div>

                    <div class="push-20-t">
                        <label>Remarks</label>
                        <table class="table remove-margin-b font-s13 bordered-table">
                            <tr>
                                <td class="field12">-</td>
                            </tr>
                        </table>
                    </div>

                    <div class="push-20"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#wallet-transaction").DataTable({
            "pageLength": 25,
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });
    });

    function getDetails( fieldVal ){

        var datamethod = $( fieldVal ).attr( 'datamethod' );

        var field1 = $( fieldVal ).attr( 'amount' );
        var field2 = $( fieldVal ).attr( 'bankname' );
        var field3 = $( fieldVal ).attr( 'accountnumber' );
        var field4 = $( fieldVal ).attr( 'accountname' );
        var field5 = $( fieldVal ).attr( 'swiftcode' );
        var field6 = $( fieldVal ).attr( 'transtype' );
        var field7 = $( fieldVal ).attr( 'transdate' );
        var field8 = $( fieldVal ).attr( 'description' );
        var field9 = $( fieldVal ).attr( 'transtatus' );
        var field10 = $( fieldVal ).attr( 'method' );
        var field11 = $( fieldVal ).attr( 'transid' );
        var field12 = $( fieldVal ).attr( 'remarks' );

        $( '.field1' ).text( field1 );
        $( '.field2' ).text( field2 );
        $( '.field3' ).text( field3 );
        $( '.field4' ).text( field4 );
        $( '.field5' ).text( field5 );
        $( '.field6' ).text( field6 );
        $( '.field7' ).text( field7 );
        $( '.field8' ).text( field8 );
        $( '.field9' ).text( field9 );
        $( '.field10' ).text( field10 );
        $( '.field11' ).text( '#ID.'+field11 );
        $( '.field12' ).text( field12 );

        $('.field-icon').removeClass('si-arrow-down si-arrow-up si-share-alt');
        $('.bank-details').hide();

        switch (datamethod) { 
            // CASH IN : VERIFIED
            case 'cashinverified': 
                $('.field-icon').addClass('si-arrow-down');
                $('.field1').addClass('text-success');
                $('.methodtype').text('Cash In - Bank Deposit');                
                break;

            // CASH IN : APPROVED
            case 'cashinapproved': 
                $('.field-icon').addClass('si-arrow-down');
                $('.field1').addClass('text-success');
                $('.methodtype').text('Cash In - Bank Transfer');
                break;

            // WITHDRAWAL : BANK TRANSFER VERIFIED
            case 'cashoutverified': 
                $('.field-icon').addClass('si-arrow-up');
                $('.field1').addClass('text-danger');
                $('.methodtype').text('Cash Out - Bank Transfer');
                $('.bank-details').show();
                break;

            // WITHDRAWAL : BANK TRANSFER APPROVED
            case 'cashoutapproved': 
                $('.field-icon').addClass('si-arrow-up');
                $('.field1').addClass('text-danger');
                $('.methodtype').text('Cash Out - Bank Transfer');
                $('.bank-details').show();
                break;

            // WITHDRAWAL : CASH PICKUP VERIFIED
            case 'cashoutpickup': 
                $('.field-icon').addClass('si-arrow-up');
                $('.field1').addClass('text-danger');
                $('.methodtype').text('Cash Out - Cash Pickup');
                break;

            // WITHDRAWAL : CASH PICKUP APPROVED
            case 'cashoutpickupapproved': 
                $('.field-icon').addClass('si-arrow-up');
                $('.field1').addClass('text-danger');
                $('.methodtype').text('Cash Out - Cash Pickup');
                break;

            // WALLET TRANSFER : SEND
            case 'transfersend': 
                $('.field-icon').addClass('si-arrow-up');
                $('.field1').addClass('text-danger');
                $('.methodtype').text('Wallet Transfer');
                break;

            // WALLET TRANSFER : RECEIVED
            case 'transferreceived': 
                $('.field-icon').addClass('si-arrow-down');
                $('.field1').addClass('text-success');
                $('.methodtype').text('Wallet Transfer');
                break;
            default : 
                $('.field-icon').removeClass('si-arrow-down si-arrow-up');
                $('.field1').removeClass('text-success text-danger')
                $('.methodtype').text('-');
                break;
        }
    }
</script>