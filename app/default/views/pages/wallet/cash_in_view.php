<?php 
View::$title = 'Cash In';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block block-themed centered max600">
            <div class="block-header bg-primary">
                <h3 class="block-title"><?php echo View::$title; ?> : Transaction Details</h3>
            </div>
            <div class="block-content">
                <?php echo View::getMessage(); ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-lg-6">
                        <div class="font-w400 text-muted">Transaction ID</div>
                        <div class="h5 font-w300 text-primary">ID.<?php echo $walletData->WalletID ; ?></div>
                    </div>
                    <div class="col-xs-12 col-lg-6">
                        <div class="font-w400 text-muted animated fadeIn">DATE</div>
                        <div class="h5 font-w300 text-primary"><?php echo ($walletData->TransactionDate != '0000-00-00 00:00:00') ? date('M j Y g:i a', strtotime($walletData->TransactionDate)) : '' ; ?></div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="clear"></div>
            <div class="block-content form-ui">

                <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="uploadtt">
                    <input type="hidden" name="walletid" value="<?php echo $walletData->WalletID ; ?>">
                    <input type="hidden" name="usrid" value="<?php echo $walletData->UserID ; ?>">
                    <input type="hidden" name="description" value="<?php echo ($walletData->Description) ? $walletData->Description : '-'; ?>">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-down fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="text-success">$ <?php echo ($walletData->TransactionAmount) ? number_format($walletData->TransactionAmount,2) : ''; ?></span> <br><span class="h4 font-w300"><?php echo ($walletData->Description) ? $walletData->Description : '-'; ?></span></div>
                    </div>                  

                    <table class="table remove-margin-b bordered-table text-muted font-s13" style="border: 1px solid #f0f0f0;">
                        <tr>
                            <td>Status</td>
                            <td class="text-right text-info"><?php echo ($walletData->TransactionStatus) ? $walletData->TransactionStatus : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Bank Name</td>
                            <td class="text-right"><?php echo ($coreBank->Name) ? $coreBank->Name : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account name</td>
                            <td class="text-right"><?php echo ($coreBank->AccountName) ? $coreBank->AccountName : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account number</td>
                            <td class="text-right"><?php echo ($coreBank->AccountNumber) ? $coreBank->AccountNumber : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account Address</td>
                            <td class="text-right"><?php echo ($coreBank->Address) ? $coreBank->Address : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>CCV</td>
                            <td class="text-right"><?php echo ($coreBank->CCV) ? $coreBank->CCV : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Method</td>
                            <td class="text-right"><?php echo ($walletData->MethodName) ? $walletData->MethodName : '-'; ?></td>
                        </tr>
                    </table>

                    <div class="col-lg-12 push-20-t">
                        <?php echo AppUtility::getWalletTTReceipt($walletData); ?>
                    </div>

                    <div class="clear"></div>

                    <div class="form-group">                            
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon">TT Receipt</span>
                                <input class="file form-control" name="TTreceipt" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                            </div>
                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Remarks</label>
                            <textarea class="form-control" name="wallet[Remarks]" disabled="disabled"><?php echo ($walletData->Remarks) ? $walletData->Remarks : '-'; ?></textarea>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="form-group row">
                        <div class="col-xs-12 text-center">
                            <label class="css-input css-checkbox css-checkbox-info">
                                <input name="ttupload" type="checkbox" value="1" required="required"> <span></span> Mark As Paid
                            </label><br>
                            <button class="btn btn-rounded btn-primary push-10-t" type="submit"><i class="fa fa-check push-5-r"></i> Submit</button>
                        </div>
                    </div>
                    <div class="clear"></div>

                </form>

            </div>

        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>