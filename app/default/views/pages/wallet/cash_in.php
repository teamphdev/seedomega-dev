<?php 
View::$title = 'Cash In';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block block-themed centered max600">
            <div class="block-header bg-primary">
                <h3 class="block-title"><?php echo View::$title; ?> : Transaction Details</h3>
            </div>            

            <div class="block-content form-ui">

                <?php echo View::getMessage(); ?>

                <form id="cashinform" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="cashin">

                    <div class="text-center push-20">
                        <div class="push-10"><i class="si si-arrow-down fa-3x"></i></div>
                        <div class="h2 font-w300 text-muted"><span class="h4 font-w300">Cash In via Bank Deposit</span></div>
                    </div>

                    <table class="table remove-margin-b bordered-table text-muted font-s13" style="border: 1px solid #f0f0f0;">
                        <tr>
                            <td>Date</td>
                            <td class="text-right"><?php echo date('M d, Y') ?></td>
                        </tr>
                        <tr>
                            <td>Bank Name</td>
                            <td class="text-right"><?php echo ($coreBank->Name) ? $coreBank->Name : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account name</td>
                            <td class="text-right"><?php echo ($coreBank->AccountName) ? $coreBank->AccountName : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account number</td>
                            <td class="text-right"><?php echo ($coreBank->AccountNumber) ? $coreBank->AccountNumber : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>Account Address</td>
                            <td class="text-right"><?php echo ($coreBank->Address) ? $coreBank->Address : '-'; ?></td>
                        </tr>
                        <tr>
                            <td>CCV</td>
                            <td class="text-right"><?php echo ($coreBank->CCV) ? $coreBank->CCV : '-'; ?></td>
                        </tr>
                        <!-- <tr>
                            <td>Amount Due</td>
                            <td class="text-right h4 font-w400">$ <span id="amt-due">0.00</span></td>
                        </tr> -->
                    </table>

                    <div class="form-group push-20-t">                            
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon">Amount</span>
                                <input id="amt-input" type="text" name="wallet[TransactionAmount]" value="" required="required" class="form-control money-mask" placeholder="0.00">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-lg-12">
                            <div>
                                <label class="css-input css-checkbox css-checkbox-info push-10-r">
                                    <input name="ttupload" type="checkbox" class="ci-ttupload" value="1"><span></span> Mark As Paid
                                </label>
                            </div>
                        </div>                      
                    </div>

                    <div class="form-group push-20-t tt-section" style="display: none;">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon">TT Receipt</span>
                                <input id="TTreceipt" class="file form-control" type="file" data-min-file-count="0" name="TTreceipt" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                            </div>
                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-xs-12 text-center">
                            <button class="btn btn-4 blue green push-10-t" type="submit"><i class="fa fa-check push-5-r"></i> Cash In</button>
                            <!-- <button class="btn btn-4 blue green push-10-t" type="button" data-toggle="modal" data-target="#confirm-modal"><i class="fa fa-check push-5-r"></i> Cash In</button> -->
                        </div>
                    </div>
                    <div class="clear"></div>

                </form>

            </div>

        </div>
    </div>
</section>
<!-- /page content -->

<!-- Confirm Modal -->
<div class="modal" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog push-50-t">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Please Confirm</h3>
                </div>
                <div class="block-content form-ui">
                    <p>You are about to cash in money to your wallet, do you want to proceed?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                <a id="confirmed-btn" href="" class="btn btn-rounded btn-primary"><i class="fa fa-check"></i> Proceed</a>
            </div>
        </div>
    </div>
</div>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".ci-ttupload").click(function(){
            if(this.checked == true) {
                $('.tt-section').slideDown();
                $('#TTreceipt').attr('name','TTreceipt');
            }else{
                $('.tt-section').slideUp();
                $('#TTreceipt').removeAttr('name');
            }
        });

        $('#confirmed-btn').click(function(){
            $('#cashinform').submit();
        })

        // $('#amt-input').on('change keyup keypress',function(){
        //     $val = $(this).val();
        //     if($val != 0){
        //         $('#amt-due').text($val);
        //     }else{
        //         $('#amt-due').text('0.00');
        //     }
        // });
    });
</script>