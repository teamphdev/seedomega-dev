<?php
View::$title = 'Dashboard';
View::$bodyclass = User::info('Sidebar').' ';
View::header();
?>

    <!-- <section class="header-bottom">
        <article>
            <div class="container">
                <h1>Customer Oriented</h1>
                <p>A customer service representative interacts with a company’s customers to provide them with information to address inquiries regarding products and services. In addition, they deal with and help resolve any customer complaints.</p>
            </div>
        </article>
    </section> -->

    <!-- content start -->
    <section class="gray">
        <article class="container admin-page" id="admindashboard">
            <div class="row">
                <?php echo View::getMessage();  ?>

                <div class="col-md-12">
                    <div class="block">
                        <!-- Booking Summary -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="booking-summary">

                                    <div class="block-header">
                                        <div class="block-title">Hub Care Pendings</div>
                                    </div>
                                    <div class="block-content text-center">
                                        <div class="row items-push text-center">
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'manage/clients/pending' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $csPendings->Clients ) ? number_format( $csPendings->Clients ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-bank fa-1x"></i></div>
                                                    <label>Clients</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'manage/investors/verification' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $csPendings->Seeders ) ? number_format( $csPendings->Seeders ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-user fa-1x"></i></div>
                                                    <label for="">Seeders</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'cs/bookings' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $csPendings->Bookings ) ? number_format( $csPendings->Bookings ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-book fa-1x"></i></div>
                                                    <label for="">Bookings</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'cs/documents' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $csPendings->Documents ) ? number_format( $csPendings->Documents ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-files-o fa-1x"></i></div>
                                                    <label for="">Documents</label>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="booking-summary push-20-t">
                                    <div class="block-header">
                                        <div class="block-title">Accounting Pendings</div>
                                    </div>
                                    <div class="block-content text-center">
                                        <div class="row items-push text-center">
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/create/1' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $acctPendings->Invoices ) ? number_format( $acctPendings->Invoices ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-file-text-o fa-1x"></i></div>
                                                    <label>For Invoicing</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/documents' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $acctPendings->Documents ) ? number_format( $acctPendings->Documents ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-files-o fa-1x"></i></div>
                                                    <label for="">TT Receipts</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/banks/1' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $acctPendings->Clients ) ? number_format( $acctPendings->Clients ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-bank fa-1x"></i></div>
                                                    <label>Bank Accounts</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'invoices/withdrawal' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $acctPendings->Earnings ) ? number_format( $acctPendings->Earnings ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="si si-wallet fa-1x"></i></div>
                                                    <label>Withdrawals</label>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END Booking Summary -->

                    </div>
                </div> 

            </div>
        </article>
    </section>
    <!-- content end -->

<?php View::footer(); ?>

<script type="text/javascript">

</script>