<?php 
View::$title = 'Options';
View::$bodyclass = User::info('Sidebar');
View::header(); 
$env = Config::get('ENVIRONMENT');
?>
<?php $userinfo = User::info(); ?>

<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<!-- page content -->
<section class="gray">
  <article class="container project-single">
    <div class="start-project">
        <?php 
        $dropdown = array(
            'new_user_role' => $levels,
            'time_zone' => $timezones = timezone_identifiers_list(),
            'site_language' => AppUtility::getLanguagesAsOption(),
            'primary_bank_account' => AppUtility::getCoreBankAccountsAsOption(),
            'global_promo' => array('0' => 'No', '1' => 'Yes'),
        );
        ?>
        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
            <input type="hidden" name="action" value="updateoptions" />
            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
            <div class="block" role="tabpanel" data-example-id="togglable-tabs">
                <ul class="nav nav-tabs nav-justified push-20" data-toggle="tabs">                                    
                    <?php 
                    $cntr = 0; 
                    foreach($options as $kk => $vv) { $cntr++;?>
                    <li role="presentation" class="<?php echo $cntr <= 1 ? 'active' : ''; ?>"><a href="#<?php echo $kk; ?>" role="tab" id="profile-tab5" data-toggle="tab" aria-expanded="<?php echo $cntr <= 1 ? 'true' : 'false'; ?>"><?php echo $kk; ?></a></li>
                    <?php } ?>
                </ul>
                <div class="block-content tab-content">
                    <?php 
                    echo View::getMessage();
                    $cntr = 0;
                    foreach($options as $key => $vals) { $cntr++; ?>
                    <div role="tabpanel" class="tab-pane fade <?php echo $cntr <= 1 ? 'active' : ''; ?> in" id="<?php echo $key; ?>" aria-labelledby="profile-tab1">
                        <?php foreach($vals as $v) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="SiteTitle"><?php echo $v->OptionLabel; ?> <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-7 col-xs-12">
                                <?php 
                                    if($v->FormType == 'upload') {
                                        $f = View::common()->getUploadedFiles($v->OptionValue);
                                        echo '<div class="col-md-12 col-sm-12"><div class="col-md-4 col-sm-4">'.View::photo( (($f) ? 'files'.$f[0]->FileSlug : ''),($v->OptionValue) ? $v->OptionKey : '',"img-responsive",false,false).'</div></div><p>&nbsp;</p>';
                                    }

                                    switch ($v->OptionKey) {
                                        case 'primary_bank_account':
                                            View::form(
                                                $v->FormType.'a',
                                                array(
                                                    'name'=> ($v->FormType == 'upload') ? $v->OptionKey : 'settings['.$v->OptionKey.']',
                                                    'value'=>$v->OptionValue,
                                                    'id' => $v->OptionKey,
                                                    'options'=>isset($dropdown[$v->OptionKey]) ? $dropdown[$v->OptionKey] : array(),
                                                    'class'=>'form-control col-md-7 col-xs-12'
                                                )
                                            );
                                            break;
                                        case 'global_promo':
                                            View::form(
                                                $v->FormType.'a',
                                                array(
                                                    'name'=> ($v->FormType == 'upload') ? $v->OptionKey : 'settings['.$v->OptionKey.']',
                                                    'value'=>$v->OptionValue,
                                                    'id' => $v->OptionKey,
                                                    'options'=>isset($dropdown[$v->OptionKey]) ? $dropdown[$v->OptionKey] : array(),
                                                    'class'=>'form-control col-md-7 col-xs-12'
                                                )
                                            );
                                            break;
                                        
                                        default:
                                            View::form(
                                                $v->FormType,
                                                array(
                                                    'name'=> ($v->FormType == 'upload') ? $v->OptionKey : 'settings['.$v->OptionKey.']',
                                                    'value'=>$v->OptionValue,
                                                    'id' => $v->OptionKey,
                                                    'options'=>isset($dropdown[$v->OptionKey]) ? $dropdown[$v->OptionKey] : array(),
                                                    'class'=>'form-control col-md-7 col-xs-12'
                                                )
                                            );
                                            break;
                                    }
                                    
                                    
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        
                    </div>
                    <?php } ?>
                    
                </div>
            </div>

            <div class="block-content block-content-full bg-gray-lighter text-center">
                <button class="btn btn-rounded btn-primary" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
            </div>

        </form>
    </div>
    </article>
</section>

<?php View::footer(); ?>