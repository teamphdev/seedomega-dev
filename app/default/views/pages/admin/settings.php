<?php 
View::$title = 'Configurations';
View::$bodyclass = User::info('Sidebar');
View::header(); 
$env = Config::get('ENVIRONMENT');
?>
<?php $userinfo = User::info(); ?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?> - <span>MPF</span></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content form-horizontal form-label-left">

                <?php echo View::getMessage(); ?>
                <?php $settings = Config::get(); ?>
                <?php foreach($settings as $key => $val) { ?>
                <div class="form-group">
                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="SiteTitle"><?php echo $key; ?></label>
                    <div class="col-md-10 col-sm-6 col-xs-12">
                        <input type="text" id="SiteTitle" value="<?php echo $val; ?>" class="form-control col-md-7 col-xs-12" readonly="">
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>


<?php View::footer(); ?>