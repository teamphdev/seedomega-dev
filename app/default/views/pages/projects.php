<?php 
  View::$title = 'IPO Listing';
  View::$bodyclass = 'loginpage';
  View::header();
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1>IPO Listing</h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
  <article class="container">
    <div class="row">
      <div class="col-lg-6">
        <ul>
          <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
          <li><a href="<?php echo View::url(); ?>">Home</a></li>
          <li class="fa fa-angle-right"></li>
          <li>Projects</li>
        </ul>
      </div>
      <div class="col-lg-6 align-right sub-menu">
        
      </div>
    </div>
  </article>
</section>
*/ ?>

<!-- ************************ Page Content ************************ -->
<section>
    <article class="container projects-page" id="">
        <!-- Projects Grid start -->
        <div class="row">
            <div class="col-lg-2">
                <div class="sw-title">
                    Categories
                </div>
                <div class="sw-content">
                    <ul class="sw-list">
                        <li class="active"><a href="javascript:void()" class="categories-filt" data-filter="*">All Categories</a></li>
                        <?php foreach (Apputility::getClientCategories() as $clientcat) { ?>
                        <li><a href="javascript:void()" class="categories-filt" data-filter=".ccatid-<?php echo $clientcat->ClientCatID; ?>"><?php echo ucwords($clientcat->CCatName); ?></a></li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="sidebar-item">
                  <div class="stories-slider">

                    <?php 
                      $ctr = 0;
                      if( isset( $testimonials ) ){
                        if( count( $testimonials ) ){
                          foreach( $testimonials as $testi ){ $ctr++;
                            $avatar = View::common()->getUploadedFiles( $testi->Avatar ); ?>

                            <div class="slide">
                              <div class="sw-title">Success Stories</div>
                              <div class="w-content success-stories">
                                <div class="text-center">
                                  <?php echo View::photo( ( ( $avatar ) ? 'files'.$avatar[0]->FileSlug : 'images/user.png' ), false, 'img-avatar', false, 'height: 103px; width: 105px;' ); ?>
                                </div>
                                <h5><b><?php echo isset( $testi->Name ) ? $testi->Name : '' ;?></b></h5>
                                <p><i class="fa fa-quote-left"></i><?php echo isset( $testi->Message ) ? $testi->Message : ''; ?> <i class="fa fa-quote-right"></i></p>
                              </div>
                            </div>
                    <?php } } }?>

                  </div>
                </div>
            </div>
            <div class="col-lg-10 col-xs-12 content-cnt">
                <!-- Filter & Sorting Form start -->
                <form action="" class="dataTables_wrapper form-inline text-right push-20" method="get">
                    <div class="form-group">
                        <label for="">Sort By</label>
                        <select id="projsortby" class="form-control userdash-select arrow-down">
                            <option value="*">-</option>
                            <option value="sort|topfunded">Top Funded</option>
                            <option value="sort|lowfunded">Low Funded</option>
                            <option value="sort|dateupdated">Recently Added</option>
                        </select>
                    </div>
                </form>
                <div class="clear"></div>
                <!-- Filter & Sorting Form end -->

                <div class="project-grid row">

                    <?php 
                    $cntr = 0;
                    foreach( $projects as $proj ): $cntr++;
                      $percentage = 0;
                      if( $proj->TargetGoal > 0 ){
                          $percentage = ( $proj->TotalRaisedAmt ) ? (int)( ( $proj->TotalRaisedAmt / $proj->TargetGoal ) * 100 + .5 ) : 0;
                          if( $percentage > 100 ) $percentage = 100;
                          $thedate = date('Y-m-d');
                      } ?>

                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 project-cnt ccatid-<?php echo $proj->ClientCatID; ?> <?php echo $proj->OfferOpening > $thedate ? 'upcoming' : 'currentoffer'; ?> sortcatid-<?php echo $proj->ClientCatID; ?>">
                            <div class="project-item ribbon ribbon-left">

                                <?php if ($percentage >= 100) : ?>
                                <div class="ribbon-box blue">SUCCESSFUL</div>
                                <?php endif; ?>

                                <div class="percent-funded" style="display: none;"><?php echo $percentage; ?></div>
                                <div class="date-updated" style="display: none;"><?php echo date( 'Y-m-d', strtotime( $proj->UpdatedAt ) ); ?></div>

                                <div class="ft-img">
                                    <?php 
                                    $comphoto = View::common()->getUploadedFiles( $proj->CompanyPhoto ); 
                                    $img = isset( $comphoto[0] ) ? 'files'.$comphoto[0]->FileSlug : '/images/placeholder-proj.jpg'; 
                                    if( View::photo( $img,false,false,false,false,false ) ) {
                                        View::photo( $img, $proj->CompanyName, "articleFeaturedImage" );
                                    } else { 
                                        View::photo( 'images/placeholder-proj.jpg' );                                             
                                    }?> 

                                    <figcaption><a href="/project/view/<?php echo $proj->ClientProfileID; ?>">View Details</a></figcaption>
                                </div>
                                <div class="project-content">
                                    <h5><a href="/project/view/<?php echo $proj->ClientProfileID;?>"><?php echo $proj->CompanyName; ?></a></h5>
                                    <p class="project-text"><?php echo isset($proj->OfferOverview) ? AppUtility::excerptText( $proj->OfferOverview, 150, ' <a href="/clients/view/'.$proj->ClientProfileID.'" class="item_button1">...read more</a>' ) : 'No Description'; ?></p>

                                    <?php $avatar = View::common()->getUploadedFiles( $proj->Avatar ); ?>
                                    <div class="project-progress">
                                        <div class="project-data row">                             
                                            <div class="col-lg-6">
                                                <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#7390d6" data-barsize="5" data-size="96">
                                                  <?php echo View::photo( ( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'), "funder" ); ?>
                                                </div>                                
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="progress_number"><?php echo $percentage; ?>%</div>
                                                <div class="progress_label">Completed</div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="project-summary">
                                        <div class="">
                                            <div class="col-lg-6 col-xs-6 no-padding daysback">
                                                <span><?php echo isset( $proj->DaysLeft ) ? number_format( $proj->DaysLeft ) : "0"; ?></span> DAYS LEFT
                                            </div>
                                            <div class="col-lg-6 col-xs-6 no-padding text-right daysback">
                                                <span><?php echo isset( $proj->TotalInvestor ) ? number_format( $proj->TotalInvestor ) : "0"; ?></span> BACKERS
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="text-center project-funded">
                                            <span><?php echo isset( $proj->TotalRaisedAmt ) ? "$".number_format( $proj->TotalRaisedAmt ) : "0"; ?></span><br>
                                            Funded
                                        </div>
                                    </div>

                                    <div class="clear"></div>
                                    <div class="project-btn">
                                        <!-- <?php 
                                        if( $enabled || Option::get('global_promo') == 1 ){ echo $proj->Data['BookHTML']; ?>
                                        <?php } else { ?>
                                            <a href="#" class="btn btn-success item_button2 nonverified" data-toggle="modal" data-target="#myModal" style="cursor: not-allowed;"><?php echo $proj->Data['BookButton']; ?></a>
                                        <?php } ?> -->
                                        <?php 
                                        if( Auth::isLoggedIn() ){ ?>
                                        <?php if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || User::info( 'UserLevel' ) == "Administrator" ){ ?>
                                              <?php if( $proj->TotalBookingAmounts >= $proj->TargetGoal && $proj->TypeOfOffer == 'Limited' ){ ?>
                                                  <a href="/bookings/book/<?php echo $proj->ClientProfileID; ?>" class="btn btn-success">Register</a>
                                              <?php }else{ ?>
                                                  <a href="/bookings/book/<?php echo $proj->ClientProfileID; ?>" class="btn btn-success">Book</a>
                                              <?php } ?>
                                          <?php } else { ?>
                                              <a href="javascript:void(0)" class="item_button2" style="cursor: not-allowed;">Book</a>
                                          <?php }
                                        }else{ ?>
                                          <a href="/project/view/<?php echo $proj->ClientProfileID; ?>" class="btn btn-success item_button1">View Details</a>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php 
                    endforeach; ?>                                                
                </div>
            </div>
        </div>
        <!-- Projects Grid end -->

        <div class="clear"></div>

    </article>
</section>

<?php View::footer(); ?>

<style>
  .content-cnt { overflow: hidden; }
</style>
<script>
 $(document).ready(function(){

  // init Isotope
  var $grid = $('.project-grid').isotope({
    itemSelector: '.project-cnt',
    layoutMode: 'fitRows',
    getSortData: {
      topfunded: '.percent-funded parseInt',
      lowfunded: '.percent-funded parseInt',
      dateupdated : '.date-updated parseInt'
    }
  });

  // bind filter button click
  $('#categories').on('change', function() {
    var filterValue = $( this ).val();
    $('.quicksearch').val('');

    // use filterFn if matches value
    $grid.isotope({ filter: filterValue });
  });

  $('.categories-filt').click(function(){
      var datacat = $(this).attr('data-filter');
      $('.categories-filt').parent('li').removeClass('active');
      $(this).parent('li').addClass('active');
      $grid.isotope({ filter: datacat });
  })

  $('#projsortby').on('change', function() {
    $('.quicksearch').val('');

    var $optvalue = $(this).val();
    var $vals = $optvalue.split('|');
    var filterByValue = $vals[1];

    if($vals[0] == 'sort'){
      if($vals[1] == 'lowfunded'){ 
          $sorting = true; 
      }else{ 
          $sorting = false; 
      }
      $grid.isotope({ sortBy: filterByValue, sortAscending: $sorting });
    }else{
      $grid.isotope({ filter: filterByValue });
    }
    
  });

  $('.popular-cat').each(function(){
    $(this).on('click', function(){ 
      var filterValue = $( this ).attr('data-sort-by');
      $grid.isotope({ filter: filterValue });
    })
  });

 })
</script>