<?php 
View::$title = isset($clientdata->CompanyName) ? $clientdata->CompanyName : "Client Profiles";
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink();?>">Home</a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1];?>">Client Profiles</a></li>
            <?php } ?>
            <?php if( isset(View::$segments[2]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><?php echo View::$title;?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      <?php echo View::getMessage(); ?>
      
      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-8">
        <div class="embed-responsive embed-responsive-16by9">
          <?php echo $clientdata->Video; ?>
        </div>

        <div class="block" style="margin-top: 20px;">

          <ul class="nav nav-tabs" data-toggle="tabs">
              <li class="active">
                  <a href="#client-info">Info</a>
              </li>
              <li class="" id="client-comments-tab">
                  <a href="#client-comments">Comments</a>
              </li>
          </ul>
          
          <div class="block-content tab-content bg-white">

            <div class="tab-pane fade fade-up in active" id="client-info">
              <div class="pcontent tab-content tab-pane">
                <p class="text-muted"><i><?php echo isset($clientdata->VideoDescription) ? $clientdata->VideoDescription : "No Description"; ?></i></p>

                <h5>Offer Overview</h5>
                <p><?php echo isset($clientdata->OfferOverview) ? $clientdata->OfferOverview : ""; ?></p>
                
                <h5>Business Model</h5>
                <p><?php echo isset($clientdata->BusinessModel) ? $clientdata->BusinessModel : ""; ?></p>
                
                <h5>Key Invest Highlights</h5>
                <p><?php echo isset($clientdata->KeyInvestHighlights) ? $clientdata->KeyInvestHighlights : ""; ?></p>

                <h5>Strategy Vision</h5>
                <p><?php echo isset($clientdata->StrategyVision) ? $clientdata->StrategyVision : ""; ?></p>

                <h5>Market Demand</h5>
                <p><?php echo isset($clientdata->MarketDemand) ? $clientdata->MarketDemand : ""; ?></p>

                <h5>Board Management</h5>
                <p><?php echo isset($clientdata->BoardManagement) ? $clientdata->BoardManagement : ""; ?></p>

                <h5>Usage Of Funds</h5>
                <p><?php echo isset($clientdata->UsageOfFunds) ? $clientdata->UsageOfFunds : ""; ?></p>

                <h5>Financial Summary</h5>
                <p><?php echo isset($clientdata->FinancialSummary) ? $clientdata->FinancialSummary : ""; ?></p>

                <h5>Press Coverage</h5>
                <p><?php echo isset($clientdata->PressCoverage) ? $clientdata->PressCoverage : ""; ?></p>

                <h5>Disclosure</h5>
                <p><?php echo isset($clientdata->Disclosure) ? $clientdata->Disclosure : ""; ?></p>
              </div>

              <?php if( isset( $teams ) && count( $teams ) ){ ?>

              <!-- <div class="content"> -->
                <hr><br>
                <h2 class="content-heading text-center push-0-t">Company Team</h2>
                <div class="row">

                  <?php foreach( $teams as $team ){ ?>
                  <div class="col-sm-6 col-lg-4">
                    <a class="block block-rounded block-link-hover3" href="javascript:void(0)">
                      <div class="block-content block-content-full clearfix">
                        <div class="pull-right">
                          <?php echo View::photo( $team->FileSlug ? 'files'.$team->FileSlug : 'images/user.png', false, 'img-avatar', false, false ); ?>
                        </div>
                        <div class="pull-left push-10-t">
                          <div class="font-w600 push-5"><?php echo isset( $team->Name ) ? $team->Name : ''; ?></div>
                          <div class="text-muted"><?php echo isset( $team->Position ) ? $team->Position : ''; ?></div>
                        </div>
                      </div>
                    </a>
                  </div>
                  <?php } ?>

                </div>
              <!-- </div> -->

              <?php } ?>
            </div>

            <div class="tab-pane fade fade-up" id="client-comments">
                <!--<h4>Comments</h4>-->
                <div id="client-comments-content" class="comments-tab">
                  <div class="actionBox">
                    <ul class="commentList">
                    <?php 
                      $ctr = 0;
                      if( isset( $comments ) ){
                        if( count( $comments ) ){
                          foreach( $comments as $comment ){ $ctr++; ?>
                            <li class="">
                              <div class="commenterImage">
                                <?php echo $comment->AvatarLink; ?>
                              </div>
                              <div class="commentText">
                                <p class=""><b><?php echo $comment->FirstName.' '.$comment->LastName; ?></b>:&nbsp; <?php echo $comment->CommentContent; ?></p> <span class="date sub-text">on <?php echo date( 'M jS, Y', strtotime( $comment->CommentDate ) ) ?></span>
                              </div>
                            </li>
                    <?php } } } ?>
                    </ul>
                  </div>
                </div>
                <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="sendcommentview" />
                    <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
                    <input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">
                    <div class="form-group">
                        <div class="">
                            <input type="text" value="" class="form-control" placeholder="Write a comment..." name="CommentContent" required="required">
                            <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>
                        </div>
                        <div class="clear"></div>
                    </div>
                </form>
            </div>

          </div><!--end block-content-->

        </div><!--end block-->

      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4">
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="project-progress">
              <?php $percentage = (int)(@($clientdata->TotalRaisedAmt / $clientdata->TargetGoal) * 100+.5); ?>
              <div class="popular-data data-single"> 
                <!-- <img src="<?php echo View::url() ?>/assets/images/funder_1.jpg" alt="Funder" /> -->
                
                <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>              

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($clientdata->TotalRaisedAmt) ? "$".number_format($clientdata->TotalRaisedAmt) : "0";?><br>
                        <small class="text-muted">Raised</small>
                    </div>                                        
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($clientdata->TotalInvestor) ? number_format($clientdata->TotalInvestor) : "0";?><br>
                        <small class="text-muted">Investors</small>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset($clientdata->TypeOfOffer) ? $clientdata->TypeOfOffer : "-"; ?><small>Type of Offer</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php 
                      $startdate = date_create($clientdata->OfferOpening);
                      $closedate = date_create($clientdata->OfferClosing);
                      $interval = date_diff($startdate, $closedate); 
                      ?>
                    <?php echo str_replace('+', '', $interval->format('%R%a'));?><small>Days Left</small>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo ($clientdata->OfferOpening != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferOpening)) : "-"; ?><small>Offer Open</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo ($clientdata->OfferClosing != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferClosing)) : "-"; ?><small>Offer Closing</small>
                  </div>
                </div>
                <?php /*
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->TargetGoal) ? number_format($clientdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    <?php echo isset($clientdata->SizeOfOffer) ? $clientdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
                  </div>
                </div>
                */ ?>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?><small>Price</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->TargetGoal) ? number_format($clientdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->MinimumBid) ? number_format($clientdata->MinimumBid) : "-"; ?><small>Minimum Investment</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->MaximumBid) ? number_format($clientdata->MaximumBid) : "-"; ?><small>Maximum Investment</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 offer-lead offer-data">
                    <?php echo isset($clientdata->LeadManager) ? ucwords($clientdata->LeadManager) : "-"; ?><small>Lead Manager</small>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>
          
          <!-- We Need Volunteers -->
          <div class="sidebar-item">
            <div class="w-title">Status</div>
            <div class="text-center" style="padding: 5px 15px 15px;">
                <?php if( $clientdata->Status == 'Pending' ){ ?>
                  Pending projects will not be shown to the investor.
                <?php } ?>

                <form class="form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                  <input type="hidden" name="action" value="<?php echo $clientdata->Status == 'Pending' ? 'approveclient' : 'updateclient'; ?>">
                  <input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">
                  <div class="input-group" style="padding: 15px 0 8px;">
                    <span class="input-group-addon"><?php echo Lang::get('CLN_CS_RATE'); ?></span>
                    <input type="text" value="<?php echo isset( $clientdata->CommRate ) && $clientdata->CommRate > 0 ? number_format( $clientdata->CommRate ) : '25'; ?>" class="form-control" pattern="^[0-9]\d*(\.\d+)?$" name="CommissionRate" placeholder="25" step="1"> 
                  </div>

                  <div class="input-group" style="padding: 15px 0 8px;">
                    <span class="input-group-addon"><?php echo Lang::get('CLN_SS_RATE'); ?></span>
                    <input type="text" value="<?php echo isset( $clientdata->SubsRate ) ? number_format( $clientdata->SubsRate, 2 ) : '0.5'; ?>" class="form-control" pattern="^[0-9]\d*(\.\d+)?$" name="SubscriptionRate" placeholder="0.00" step=".01">
                  </div>
                  <small class="text-muted">must be positive(+) value</small>

                  <input type="submit" class="btn btn-3 green" style="margin:10px 0 5px;" value="<?php echo $clientdata->Status == 'Pending' ? 'Approve' : 'Update'; ?> this Client">
                </form>

            </div>
          </div>
          <!-- END We Need Volunteers -->

        </div>
      </div> 
             
    </div>
  </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
  $(document).ready(function() {

    $( '#client-comments-tab' ).click( function( e ) {
      e.preventDefault();
      setTimeout(function(){ autoScroll(); }, 800);
    });

    function autoScroll(){
      $( '#client-comments-content' ).animate( { scrollTop: $( '.commentList' )[0].scrollHeight }, 2000);
    }

  });
</script>