<?php
    View::$title = 'Company Profile';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>

<?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

    <!-- ************************ Page Content ************************ -->
    <script>
        <?php if( $tab != '' ){ ?>
        var selectedTab = '<?php echo $tab?>';
        <?php } ?>
    </script>

    <section class="gray">
        <article class="container project-single">
            <div class="start-project" id="profile_wizard">
                <!-- Main Content -->
                <div class="title">
                    <ul id="profileTabs">
                        <li data-link="company-info" class="" id="tab-company-info"><a href="<?php echo View::url('clients/profile/company-info'); ?>"><i class="fa fa-pagelines"></i><span>1: Company Info</span></a></li>
                        <li data-link="offer" class="" id="tab-offer"><a href="<?php echo View::url('clients/profile/offer'); ?>" ><i class="fa fa-link"></i><span>2: Offer</span></a></li>
                        <li data-link="documents" class="" id="tab-documents"><a href="<?php echo View::url('clients/profile/documents'); ?>"><i class="fa fa-tags"></i><span>3: Documents</span></a></li>
                        <li data-link="bank-accounts" class="" id="tab-bank-accounts"><a href="<?php echo View::url('clients/profile/bank-accounts'); ?>"><i class="fa fa-bank"></i><span>4: Bank Accounts</span></a></li>
                        <li data-link="summary" class="" id="tab-summary"><a href="<?php echo View::url('clients/profile/summary'); ?>"><i class="fa fa-file-image-o"></i><span>5: Summary</span></a></li>
                    </ul>
                </div>
                <hr>
                <?php echo View::getMessage(); ?>
                <div class="start-content">
                    <!-- Company Tab -->
                    <div id="company-info" class="form-wizard">
                        <input name="image" type="file" id="upload" class="hidden" onchange="">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formCompanyInfo">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo (isset($client->ClientProfileID)) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo $userID; ?>" />
                            <input type="hidden" name="bankid" value="<?php echo isset( $client->BankAccountID ) ? $client->BankAccountID : '0'; ?>" />
                            <input type="hidden" name="section" value="companyinfo" />
                            <input type="hidden" name="next" value="offer" id="companytab" />

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?></label>
                                    <input type="text" value="<?php echo (isset($client->CompanyName))?$client->CompanyName:''; ?>" id="CompanyName" name="client[CompanyName]" class="form-control<?php echo $client->Disabled; ?>">                                    
                                </div>
                                <div class="form-right">
                                    <label>Company Image</label>
                                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="CompanyPhoto" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <?php if (isset($client->Video) && $client->Video!='') {
                                        echo $client->Video;
                                    } ?>
                                </div>
                                <div class="form-right">
                                    <div class="avatar-container">
                                        <?php $avatar = View::common()->getUploadedFiles($client->CompanyPhoto); ?>
                                        <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar"); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_VIDEO'); ?></label>
                                <textarea class="form-control" name="client[Video]" rows="2" style="height:75px !important"><?php echo (isset($client->Video))?$client->Video:''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_VIDEODESC'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[VideoDescription]" id="VideoDescription"><?php echo @$client->VideoDescription; ?></textarea>
                            </div>
                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="text-center">
                                    <div class="next-btn">
                                        <button type="button" class="btn btn-5 green" data-link="company-info" onClick="document.getElementById('companytab').value=''; document.getElementById('btnFirstNext').click();">Save</button>
                                        <button type="button" class="btn btn-4 blue" data-link="offer" onClick="" id="btnFirstNext">Save and Next</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Company Tab -->

                    <!-- Offer Tab -->
                    <div id="offer" class="form-wizard ">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formOffer">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo isset( $client->ClientProfileID ) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo $userID; ?>" />
                            <input type="hidden" name="bankid" value="<?php echo isset( $client->BankAccountID ) ? $client->BankAccountID : '0'; ?>" />
                            <input type="hidden" name="section" value="offer" />
                            <input type="hidden" name="next" value="documents" id="offertab" />

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_TYPEOFFER'); ?></label>
                                <select id="TypeOfOffer" name="client[TypeOfOffer]" class="form-control<?php echo $client->Disabled; ?>" required="required">
                                    <option value="">Select</option>
                                    <option value="Open" <?php echo $client->TypeOfOffer == 'Open' ? 'selected' : ""; ?>>Open</option>
                                    <option value="Limited" <?php echo $client->TypeOfOffer == 'Limited' ? 'selected' : ""; ?>>Limited</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_PRF_OFFEROPENING'); ?></label>
                                    <input type="text" value="<?php echo isset( $client->OfferOpening ) ? $client->OfferOpening : ''; ?>" id="OfferOpening" name="client[OfferOpening]" class="form-control jsdate<?php echo $client->Disabled; ?>" required="required">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('CLN_PRF_OFFERCLOSING'); ?></label>
                                    <input type="text" value="<?php echo isset( $client->OfferClosing ) ? $client->OfferClosing : ''; ?>" id="OfferClosing" name="client[OfferClosing]" class="form-control jsdate<?php echo $client->Disabled; ?>" required="required">
                                </div>
                            </div>

                            <?php /*
                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                <input type="text" value="<?php echo isset( $client->Price ) ? $client->Price : ''; ?>" id="Price" name="client[Price]" class="form-control<?php echo $client->Disabled; ?>">
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_PRF_SZEOFFER'); ?></label>
                                    <input type="text" value="<?php echo isset( $client->SizeOfOffer ) ? $client->SizeOfOffer : ''; ?>" id="SizeOfOffer" name="client[SizeOfOffer]" class="form-control<?php echo $client->Disabled; ?>">
                                </div>
                                <div class="form-right">
                                    <label>Target Goal</label>
                                    <input type="number" value="<?php echo isset( $client->TargetGoal ) ? $client->TargetGoal : ''; ?>" id="TargetGoal" name="client[TargetGoal]" class="form-control<?php echo $client->Disabled; ?>">
                                </div>
                            </div>
                            */ ?>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                    <input type="text" value="<?php echo isset( $client->Price ) ? $client->Price : ''; ?>" id="Price" name="client[Price]" class="form-control<?php echo $client->Disabled; ?>">
                                </div>
                                <div class="form-right">
                                    <label>Target Goal</label>
                                    <input type="number" value="<?php echo isset( $client->TargetGoal ) ? $client->TargetGoal : ''; ?>" id="TargetGoal" name="client[TargetGoal]" class="form-control<?php echo $client->Disabled; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_PRF_MINBID'); ?></label>
                                    <input type="number" value="<?php echo isset( $client->MinimumBid ) ? $client->MinimumBid : ''; ?>" id="MinimumBid" name="client[MinimumBid]" class="form-control<?php echo $client->Disabled; ?>">
                                </div>
                                <div class="form-right">
                                    <label>Maximum Bid</label>
                                    <input type="number" value="<?php echo isset( $client->MaximumBid ) ? $client->MaximumBid : ''; ?>" id="MaximumBid" name="client[MaximumBid]" class="form-control<?php echo $client->Disabled; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_LDMNGR'); ?></label>
                                <input type="text" value="<?php echo isset( $client->LeadManager ) ? $client->LeadManager : ''; ?>" id="LeadManager" name="client[LeadManager]" class="form-control<?php echo $client->Disabled; ?>">
                            </div>

                            <div class="ln_solid"></div>

                            <div class="next-btn">
                                <button type="button" class="btn btn-5 red" data-link="company-info" onClick="moveform(this,'company-info')">Back</button>
                                <button type="button" class="btn btn-5 green" data-link="company-info" onClick="document.getElementById('offertab').value=''; document.getElementById('btnSecondNext').click();">Save</button>
                                <button type="button" class="btn btn-4 blue" data-link="documents" onClick="" id="btnSecondNext">Save and Next</button>
                                <div class="clear"></div>
                            </div>
                        </form>
                    </div>
                    <!-- END Offer Tab -->

                    <!-- Files Tab -->
                    <div id="documents" class="form-wizard ">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formDocuments">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo (isset($client->ClientProfileID)) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo $userID; ?>" />
                            <input type="hidden" name="section" value="documents" />
                            <input type="hidden" name="next" value="bank-accounts" id="filetab" />

                            <div class="upload-documents"><?php echo AppUtility::getClientFileList($profile, true); ?></div>

                            <div class="ln_solid"></div>
                            <div class="next-btn">
                                <br/><br/>
                                <button type="button" class="btn btn-5 red" data-link="offer" onClick="moveform(this,'offer')">Back</button>
                                <button type="button" class="btn btn-5 green" data-link="company-info" onClick="document.getElementById('filetab').value=''; document.getElementById('btnThirdNext').click();">Save</button>
                                <button type="button" class="btn btn-4 blue" data-link="bank-accounts" onClick="" id="btnThirdNext">Save and Next</button>
                                <div class="clear"></div>
                            </div>
                        </form>
                    </div>
                    <!-- END Files Tab -->

                    <!-- Bank Tab -->
                    <div id="bank-accounts" class="form-wizard ">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formBankAccounts">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo (isset($client->ClientProfileID)) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo $userID ?>" />
                            <input type="hidden" name="bankid" value="<?php echo isset( $client->BankAccountID ) ? $client->BankAccountID : '0'; ?>" />
                            <input type="hidden" name="section" value="bankaccounts" />
                            <input type="hidden" name="next" value="summary" id="banktab" />

                            <div class="form-group">
                                <div class="form-left">
                                    <label>Bank Name</label>
                                    <input type="text" value="<?php echo isset( $client->Name ) ? $client->Name : ''; ?>" id="Name" name="bank[Name]" class="form-control" required="required">
                                </div>
                                <div class="form-right">
                                    <label>Swift Code</label>
                                    <input type="text" value="<?php echo isset( $client->SwiftCode ) ? $client->SwiftCode : ''; ?>" id="SwiftCode" name="bank[SwiftCode]" class="form-control" required="required">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Bank Address</label>
                                <input type="text" value="<?php echo isset( $client->Address ) ? $client->Address : ''; ?>" id="Address" name="bank[Address]" class="form-control">
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label>Account Name</label>
                                    <input type="text" value="<?php echo isset( $client->AccountName ) ? $client->AccountName : ''; ?>" id="AccountName" name="bank[AccountName]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label>Account Number</label>
                                    <input type="text" value="<?php echo isset( $client->AccountNumber ) ? $client->AccountNumber : ''; ?>" id="AccountNumber" name="bank[AccountNumber]" class="form-control">
                                </div>
                            </div>

                            <div class="ln_solid"></div>

                            <div class="next-btn">
                                <button type="button" class="btn btn-5 red" data-link="documents" onClick="moveform(this,'documents')">Back</button>
                                <button type="button" class="btn btn-5 green" data-link="company-info" onClick="document.getElementById('banktab').value=''; document.getElementById('btnFourthNext').click();">Save</button>
                                <button type="button" class="btn btn-4 blue" data-link="summary" onClick="" id="btnFourthNext">Save and Next</button>
                                <div class="clear"></div>
                            </div>
                        </form>
                    </div>
                    <!-- END Banks Tab -->

                    <!-- Summary Tab -->
                    <div id="summary" class="form-wizard">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formSummary">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo (isset($client->ClientProfileID)) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo $userID; ?>" />
                            <input type="hidden" name="bankid" value="<?php echo isset( $client->BankAccountID ) ? $client->BankAccountID : '0'; ?>" />
                            <input type="hidden" name="section" value="summary"  />
                            <input type="hidden" name="next" value="" id="summarytab" />

                            <div class="form-group">
                                <label>Executive Summary</label>
                                <textarea class="form-control tinyMCE" name="client[ExecutiveSummary]" id="clientExecutiveSummary"><?php echo (isset($client->ExecutiveSummary)) ? $client->ExecutiveSummary : ''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_OFFVIEW'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[OfferOverview]" id="clientOfferOverview"><?php echo (isset($client->OfferOverview)) ? $client->OfferOverview : ''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_BUSSMODEL'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[BusinessModel]" id="clientBusinessModel"><?php echo (isset($client->BusinessModel))?$client->BusinessModel:''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_KEYINVST'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[KeyInvestHighlights]" id="clientKeyInvestHighlights"><?php echo (isset($client->KeyInvestHighlights))?$client->KeyInvestHighlights:''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_STRATVISION'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[StrategyVision]" id="clientStrategyVision"><?php echo (isset($client->StrategyVision))?$client->StrategyVision:''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_MARKTDEMAND'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[MarketDemand]" id="clientMarketDemand"><?php echo (isset($client->MarketDemand))?$client->MarketDemand:''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_BRDMNGT'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[BoardManagement]" id="clientBoardManagement"><?php echo (isset($client->BoardManagement))?$client->BoardManagement:''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_USEFUNDS'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[UsageOfFunds]" id="clientUsageOfFunds"><?php echo (isset($client->UsageOfFunds))?$client->UsageOfFunds : ''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_FINSUMMRY'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[FinancialSummary]" id="clientFinancialSummary"><?php echo (isset($client->FinancialSummary))? $client->FinancialSummary : ''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_PRESS'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[PressCoverage]" id="clientPressCoverage"><?php echo (isset($client->PressCoverage))?$client->PressCoverage :''; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('CLN_PRF_DISCLOSURE'); ?></label>
                                <textarea class="form-control tinyMCE" name="client[Disclosure]" id="clientDisclosure"><?php echo (isset($client->Disclosure))?$client->Disclosure : ''; ?></textarea>
                            </div>

                            <div class="ln_solid"></div>

                            <div class="next-btn">
                                <button type="button" class="btn btn-5 red" data-link="bank-accounts" onClick="moveform(this,'bank-accounts')">Back</button>
                                <button type="button" class="btn btn-5 green" data-link="company-info" onClick="document.getElementById('summarytab').value='summary'; document.getElementById('btnSubmitData').click();">Save</button>
                                <button type="submit" class="btn btn-4 blue" id="btnSubmitData">Save and Exit</button>
                            </div>
                    </div>
                    <!-- END Summary Tab -->
                </div>

                <!-- END Main Content -->
                <div class="modal fade" id="modalWizardSuccess" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="block block-themed block-transparent remove-margin-b">
                                <div class="block-header bg-success">
                                    <ul class="block-options">
                                        <li>
                                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Saving Profile...</h3>
                                </div>
                                <div class="block-content">
                                    <p id="alertmessage">.</p>
                                </div>
                            </div>
                            <div class="modal-footer text-center">
                                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>

<?php View::footer(); ?>