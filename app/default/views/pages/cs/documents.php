<?php
View::$title = 'Pending Documents';
View::$bodyclass = 'cs-documents';
View::header();
?>
    <?php //View::template('users/banner'); ?>
    <section class="header-bottom">
        <article>
            <div class="container">
                <h1><?php echo View::$title; ?></h1>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->

    <section class="gray">
        <article class="container projects-page">
            <?php echo View::getMessage(); ?>            
            <div class="block">
                <div class="block tab-content">
                    
                    <table class="table table-divide js-dataTable-full-pagination dt-responsive table-hover table-vcenter table-header-bg" style="width: 100%;">
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Level</th>                                
                                <th class="text-center no-wrap">Document Name</th>
                                <th class="text-center">Section</th>
                                <th class="text-center no-wrap">File Name</th>
                                <th class="text-center no-wrap" style="min-width:14%;">Date Added</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cntr = 0;
                            if( isset( $documents ) && count( $documents ) ){
                                foreach( $documents as $doc ): $cntr++;                                    
                                    $refdata = User::info( false, $doc->ReferenceID );
                                    $accntlvl = 'Project';
                                    $name = $refdata == NULL ? $doc->Name : (isset($refdata->LastName) ? $refdata->LastName : '').' '.(isset($refdata->FirstName) ? $refdata->FirstName : '');
                                    if( isset( $refdata->Level ) ){
                                        switch( $refdata->Level ){
                                             case '4':
                                                $accntlvl = 'Project';
                                                break;
                                             
                                             default:
                                                $accntlvl = $refdata->UserLevel;
                                                break;
                                        }
                                    } ?>
                                    <tr>
                                        <td>
                                            <?php echo $cntr; ?>
                                        </td>
                                        <td class="no-wrap">
                                            <?php echo $name; ?>
                                        </td>
                                        <td>
                                            <?php echo isset( $accntlvl ) ? $accntlvl : ''; ?>
                                        </td>
                                        <td>
                                            <?php echo isset( $doc->DocumentName ) ? $doc->DocumentName : ""; ?>
                                        </td>
                                        <td>
                                            <?php echo isset( $doc->GroupName ) ? ucwords( $doc->GroupName ) : ""; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo isset( $doc->FileSlug ) ? View::asset( 'files' ).$doc->FileSlug : ""; ?>" class="html5lightbox"><?php echo isset( $doc->FileName ) ? $doc->FileName : ""; ?></a>
                                        </td>
                                        <td class="text-center">
                                            <?php echo isset( $doc->DateAdded ) && $doc->DateAdded != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $doc->DateAdded ) ) : "-"; ?>
                                        </td>
                                        <td class="text-center">
                                            <div class="">
                                                <?php $link = '';
                                                switch( $doc->GroupName ){
                                                    case 'Accounts':
                                                    case 'Seeders':
                                                        $link = 'users/profile/documents/'.$doc->ReferenceID;
                                                        break;
                                                    case 'Clients':
                                                        $link = 'clients/profile/documents/'.$doc->ReferenceID;
                                                        break;
                                                    case 'Bookings':
                                                        $link = 'bookings/info/'.$doc->TableID;
                                                        break;
                                                    
                                                    default: break;
                                                } ?>
                                                <a href="<?php echo View::url( $link ); ?>" target="_blank" class="btn btn-sm btn-default btn-rounded" title=""><i class="fa fa-eye"></i> View</a>
                                                <?php ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="8">No Data</td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>

<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });

    });
</script> -->