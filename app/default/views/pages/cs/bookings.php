<?php
View::$title = 'Manage Bookings';
View::$bodyclass = 'cs-bookings';
View::header();
?>
    <?php //View::template('users/banner'); ?>
    <section class="header-bottom">
        <article>
            <div class="container">
                <h1><?php echo View::$title; ?></h1>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->

    <section class="gray">
        <article class="container projects-page">
            <?php echo View::getMessage(); ?>
            <!-- Stats -->
            <!-- <div class="content bg-white border-b push-20">
                <div class="row items-push text-uppercase">
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Total Investor</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-users"></i> Only Total Approved</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);">$0</a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">All Companies</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);">0</a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-check"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);">0</a>
                    </div>                   
                    
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Pendings</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-clock"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);">0</a>
                    </div>
                </div>
            </div> -->
            <!-- END Stats -->
            
            <div class="block">
                <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                    <!-- <li class="">
                        <a href="#booking-incomplete">Incomplete <span class="badge badge-danger"><?php echo isset($bookings['Pending']['Incomplete']) ? count($bookings['Pending']['Incomplete']) : 0; ?></span></a>
                    </li> -->
                    <li class="active">
                        <a href="#booking-pending">Pending <span class="badge badge-warning"><?php echo isset( $bookings['Pending']['Pending'] ) ? count( $bookings['Pending']['Pending'] ) : 0; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#booking-verified">Verified <span class="badge badge-info"><?php echo isset( $bookings['Verified'] ) ? count( $bookings['Verified'] ) : 0; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#booking-approved">Approved <span class="badge badge-success"><?php echo isset( $bookings['Approved'] ) ? count( $bookings['Approved'] ) : 0; ?></span></a>
                    </li>
                </ul>
                <div class="block tab-content">
                    
                    <!-- Pending -->
                    <div class="tab-pane fade fade-up active in" id="booking-pending">
                        <p class="text-muted push-20-t push-0">Subject for approval of TT Receipt Uploaded.</p>
                        <table class="table table-divide js-dataTable-full-pagination dt-responsive table-hover table-vcenter" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Investment</th>
                                    <th class="text-center no-wrap">Submission Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $bookings['Pending']['Pending'] ) && count( $bookings['Pending']['Pending'] ) ){
                                    foreach( $bookings['Pending']['Pending'] as $book ):

                                        switch( $book->BookingStatus ){
                                            case 'Approved':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Pending':
                                                $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Verified':
                                                $statusfield = '<span class="label label-info"><i class="si si-user-following"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                        } ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600">
                                                    <?php echo isset( $book->Name ) ? ucwords( $book->Name ) : ""; ?>
                                                </h3>
                                                <div class="push-10-t visible-xs">
                                                    <!-- <?php echo $statusfield; ?> -->
                                                    <span class="text-info font-w600"><?php echo isset( $book->TotalAmountAttached ) ? '$'.number_format( $book->TotalAmountAttached ) : ""; ?></span>
                                                </div>
                                            </td>
                                            <td class="h5 text-info font-w600 hidden-xs">
                                                <?php echo isset( $book->CompanyName ) ? $book->CompanyName : ""; ?>
                                            </td>
                                            <td class="h5 text-right text-info font-w600 hidden-xs">
                                                <?php echo isset( $book->TotalAmountAttached ) ? '$'.number_format( $book->TotalAmountAttached ) : ""; ?>
                                            </td>
                                            <td class="h5 text-center text-info font-w600 hidden-xs">
                                                <span class="hidden"><?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $book->CreatedAt ) ) : "-"; ?></span>
                                                <?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $book->CreatedAt ) ) : "-"; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo $statusfield; ?>
                                            </td>
                                            <td class="h5 text-primary text-center">
                                                <a href="<?php echo View::url( 'bookings/info/'.$book->InvestmentBookingID ); ?>" class="btn btn-sm btn-rounded btn-default" title="">Verify Booking</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- Verified -->
                    <div class="tab-pane fade fade-up" id="booking-verified">
                        <p class="text-muted push-20-t push-0">All booking with approved TT Receipt and waiting for client acknowledgement.</p>
                        <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Investment</th>
                                    <th class="text-center no-wrap">Submission Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $bookings['Verified'] ) && count( $bookings['Verified'] ) ){
                                    foreach($bookings['Verified'] as $book):

                                        switch ($book->BookingStatus) {
                                            case 'Approved':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Pending':
                                                $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Verified':
                                                $statusfield = '<span class="label label-info"><i class="si si-user-following"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600">
                                                    <?php echo isset( $book->Name ) ? ucwords( $book->Name ) : ""; ?>
                                                </h3>
                                                <div class="push-10-t visible-xs">
                                                    <!-- <?php echo $statusfield; ?> -->
                                                    <span class="text-info font-w600"><?php echo isset( $book->TotalAmountAttached ) ? '$'.number_format( $book->TotalAmountAttached ) : ""; ?></span>
                                                </div>
                                            </td>
                                            <td class="h5 text-info font-w600 hidden-xs">
                                                <?php echo isset( $book->CompanyName ) ? $book->CompanyName : ""; ?>
                                            </td>
                                            <td class="h5 text-right text-info font-w600 hidden-xs">
                                                <?php echo isset( $book->TotalAmountAttached ) ? '$'.number_format( $book->TotalAmountAttached ) : ""; ?>
                                            </td>
                                            <td class="h5 text-center text-info font-w600 hidden-xs">
                                                <span class="hidden"><?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $book->CreatedAt ) ) : "-"; ?></span>
                                                <?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $book->CreatedAt ) ) : "-"; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo $statusfield; ?>
                                            </td>
                                            <td class="h5 text-primary text-center">
                                                
                                                <a href="<?php echo View::url( 'bookings/info/'.$book->InvestmentBookingID ); ?>" class="btn btn-sm btn-rounded btn-default" title="View Booking Info">Booking Info</a>
                                                
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- Approved -->
                    <div class="tab-pane fade fade-up" id="booking-approved">
                        <p class="text-muted push-20-t push-0">All approved booking.</p>
                        <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Investment</th>
                                    <th class="text-center no-wrap">Submission Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $bookings['Approved'] ) && count( $bookings['Approved'] ) ){
                                    foreach($bookings['Approved'] as $book):

                                        switch ($book->BookingStatus) {
                                            case 'Approved':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Pending':
                                                $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Verified':
                                                $statusfield = '<span class="label label-info"><i class="si si-user-following"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600">
                                                    <?php echo isset( $book->Name ) ? ucwords( $book->Name ) : ""; ?>
                                                </h3>
                                                <div class="push-10-t visible-xs">
                                                    <!-- <?php echo $statusfield; ?> -->
                                                    <span class="text-info font-w600"><?php echo isset( $book->TotalAmountAttached ) ? '$'.number_format( $book->TotalAmountAttached ) : ""; ?></span>
                                                </div>
                                            </td>
                                            <td class="h5 text-info font-w600 hidden-xs">
                                                <?php echo isset( $book->CompanyName ) ? $book->CompanyName : ""; ?>
                                            </td>
                                            <td class="h5 text-right text-info font-w600 hidden-xs">
                                                <?php echo isset( $book->TotalAmountAttached ) ? '$'.number_format( $book->TotalAmountAttached ) : ""; ?>
                                            </td>
                                            <td class="h5 text-center text-info font-w600 hidden-xs">
                                                <span class="hidden"><?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $book->CreatedAt ) ) : "-"; ?></span>
                                                <?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $book->CreatedAt ) ) : "-"; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo $statusfield; ?>
                                            </td>
                                            <td class="h5 text-primary text-center">
                                                <a href="<?php echo View::url( 'bookings/info/'.$book->InvestmentBookingID ); ?>" class="btn btn-sm btn-rounded btn-default" title="View Booking Info">Booking Info</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- Incomplete -->
                    <div class="tab-pane fade fade-up" id="booking-incomplete">
                        <p class="text-muted push-20-t">All bookings without TT Receipt.</p>
                        <table class="table table-divide table-hover table-header-bg table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Project</th>
                                    <th class="text-center">Investment</th>
                                    <th class="text-center no-wrap">Submission Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $bookings['Pending']['Incomplete'] ) && count( $bookings['Pending']['Incomplete'] ) ){
                                    foreach($bookings['Pending']['Incomplete'] as $book):

                                        switch ($book->BookingStatus) {
                                            case 'Approved':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                            case 'Pending':
                                                $statusfield = '<span class="label label-danger"><i class="si si-close"></i> Incomplete</span>';
                                                break;
                                            case 'Verified':
                                                $statusfield = '<span class="label label-info"><i class="si si-user-following"></i> '.$book->BookingStatus.'</span>';
                                                break;
                                        } ?>
                                            <tr>
                                                <td>
                                                    <h3 class="h5 font-w600">
                                                        <?php echo isset($book->Name) ? ucwords($book->Name) : ""; ?>
                                                    </h3>
                                                    <div class="push-10-t visible-xs">
                                                        <!-- <?php echo $statusfield; ?> -->
                                                        <span class="text-info font-w600"><?php echo isset($book->TotalAmountAttached) ? '$'.number_format($book->TotalAmountAttached) : ""; ?></span>
                                                    </div>
                                                </td>
                                                <td class="h5 text-info font-w600 hidden-xs">
                                                    <?php echo isset( $book->CompanyName ) ? $book->CompanyName : ""; ?>
                                                </td>
                                                <td class="h5 text-right text-info font-w600 hidden-xs">
                                                    <?php echo isset($book->TotalAmountAttached) ? '$'.number_format($book->TotalAmountAttached) : ""; ?>
                                                </td>
                                                <td class="h5 text-center text-info font-w600 hidden-xs">
                                                    <?php echo isset( $book->CreatedAt ) && $book->CreatedAt != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $book->CreatedAt ) ) : "-"; ?>
                                                </td>
                                                <td class="h5 text-center hidden-xs">
                                                    <?php echo $statusfield; ?>
                                                </td>
                                                <td class="h5 text-primary text-center">
                                                    <div class="">
                                                        <a href="<?php echo View::url( 'bookings/info/'.$book->InvestmentBookingID ); ?>" class="btn btn-sm btn-rounded btn-default" data-toggle="tooltip" title="View Booking Info">Booking Info</a>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        // $('#projlist').easyPaginate({
        //     paginateElement : 'article',
        //     elementsPerPage: 12,
        //     effect: 'fade',
        //     slideOffset : 100,
        //     nextButtonText : "Next",
        //     prevButtonText : "Prev",
        //     lastButtonText : "Last",
        //     firstButtonText: "First"
        // });
    });
</script>