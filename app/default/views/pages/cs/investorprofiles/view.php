<?php 
View::$title = isset($investordata->FirstName) ? $investordata->FirstName.' '.$investordata->LastName : "Investor Profiles";
View::$bodyclass = '';
View::header();
?>
<section class="header-bottom">
    <article>
        <div class="container">
            <div class="text-center overflow-hidden">
                <div class="push-50-t push animated fadeInDown">
                    <?php echo View::photo( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png', "Avatar", "img-avatar img-avatar128 img-avatar-thumb" ); ?>
                </div>
                <div class="push-20-t animated fadeInUp">            
                    <h5 class="h3 push-10-t text-white"><?php echo View::$title; ?></h5>
                </div>
            </div>

        </div>
    </article>
</section>

<section class="breadcrumb">
  <article class="container">
    <div class="row">
      <div class="col-lg-6">
        <ul>
          <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
          <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
          <li class="fa fa-angle-right"></li>
          <li><a href="<?php echo View::url();?>"><?php echo View::$segments[0]; ?></a></li>
          <?php if( isset(View::$segments[1]) ) { ?>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>">Investor Profiles</a></li>
            <li class="fa fa-angle-right"></li>
            <li><?php echo View::$title; ?></li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </article>
</section>

<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      
      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-8">
        <div class="block">
          <div class="block-header">
            Investor Profiles
          </div>
          <div class="block-content tab-content bg-white min-height-400">
            <div class="tab-pane fade fade-up in active no-padding form-ui" id="investor-info">
              <?php echo View::getMessage();?>

              <?php if($investorFiles){ ?>
                <div class="form-group">
                  <h3 class="text-center">Documents</h3>
                </div>
                <?php /* echo AppUtility::getInvestorFileList( $investordata ); */ ?>
                <?php echo $investorFiles; ?>
              <?php } ?>

              <div class="form-group">

                <div class="form-left">
                  <div class="push-20">
                    <h5><?php echo isset( $investordata->Address ) ? $investordata->Address : ''; ?></h5>
                    <div class="text-muted">
                      <small><?php echo Lang::get('USR_EDIT_ADDS'); ?></small>
                    </div>
                  </div>
                </div>

                <div class="form-right">
                  <div class="push-20">
                    <h5><?php echo isset( $investordata->City ) ? $investordata->City : ''; ?></h5>
                    <div class="text-muted">
                      <small><?php echo Lang::get('USR_EDIT_CTY'); ?></small>
                    </div>
                  </div>
                </div>

              </div>

              <div class="form-group">

                <div class="form-left">
                  <div class="push-20">
                    <h5><?php echo isset( $investordata->State ) ? $investordata->State : ''; ?></h5>
                    <div class="text-muted">
                      <small><?php echo Lang::get('USR_EDIT_STATES'); ?></small>
                    </div>
                  </div>
                </div>

                <div class="form-right">
                  <div class="push-20">
                    <h5><?php echo isset( $investordata->Country ) ? $investordata->Country : ''; ?></h5>
                    <div class="text-muted">
                      <small><?php echo Lang::get('USR_EDIT_CNTRY'); ?></small>
                    </div>
                  </div>
                </div>

              </div>

              <div class="form-group">

                <div class="form-left">
                  <div class="push-20">
                    <h5><?php echo isset( $investordata->PostalCode ) ? $investordata->PostalCode : ''; ?></h5>
                    <div class="text-muted">
                      <small><?php echo Lang::get('USR_EDIT_PCODE'); ?></small>
                    </div>
                  </div>
                </div>

                <div class="form-right">
                  <div class="push-20">
                    <h5><?php echo isset( $investordata->Country ) ? $investordata->Country : ''; ?></h5>
                    <div class="text-muted">
                      <small><?php echo Lang::get('USR_EDIT_CNTRY'); ?></small>
                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div><!--end block-content-->

        </div><!--end block-->
      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4">
        
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="">
              <div class="w-title">
                Personal Info
              </div>

              <div class="block-content min-height-400" style="">

                <div class="push-20">
                  <h5><?php echo isset( $investordata->LastName ) ? $investordata->LastName : ''; ?></h5>
                  <div class="text-muted">
                    <small><?php echo Lang::get('USR_EDIT_LN'); ?></small>
                  </div>
                </div>

                <div class="push-20">
                  <h5><?php echo isset( $investordata->FirstName ) ? $investordata->FirstName : ''; ?></h5>
                  <div class="text-muted">
                    <small><?php echo Lang::get('USR_EDIT_FN'); ?></small>
                  </div>
                </div>

                <div class="push-20">
                  <h5><?php echo isset( $investordata->Email ) ? $investordata->Email : ''; ?></h5>
                  <div class="text-muted">
                    <small><?php echo Lang::get('USR_EDIT_EML'); ?></small>
                  </div>
                </div>

                <div class="push-20">
                  <h5><?php echo isset( $investordata->Phone ) ? $investordata->Phone : ''; ?></h5>
                  <div class="text-muted">
                    <small><?php echo Lang::get('USR_EDIT_PHNE'); ?></small>
                  </div>
                </div>

                <div class="push-20">
                  <?php 
                  switch ($investordata->Gender) {
                     case 'M':
                       $genderEcho = 'Male';
                       break;
                     case 'F':
                       $genderEcho = 'Female';
                       break;                     
                     default:
                       $genderEcho = '-';
                       break;
                   } ?>
                  <h5><?php echo $genderEcho; ?></h5>
                  <div class="text-muted">
                    <small><?php echo Lang::get('USR_EDIT_GNDR'); ?></small>
                  </div>
                </div>

                <div class="push-20">
                  <label for="">Status</label><br>
                  <?php
                    switch( $iData['Status'] ){
                      case 'Verification':
                        echo '<a href="'.View::url( 'cs/investorprofiles/approve/'.$iData['UserID'] ).'" class="btn btn-3 green'.$iData['Disabled'].'" style="margin:10px 0 5px 0;">Approve this Investor</a>';
                        break;

                      case 'Approved':
                        echo '<span class="label label-success"><i class="fa fa-check"></i> '.$iData['Status'].'</span>';
                        break;
                        
                      case 'Incomplete':
                        echo '<span class="label label-danger"><i class="fa fa-times"></i> '.$iData['Status'].'</span>';
                        break;
                      
                      default: break;
                    } ?>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>

        </div>
      </div>

    </div>
  </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
      $( '.remove-ib' ).removeClass( 'inline-block' );
    });
</script>