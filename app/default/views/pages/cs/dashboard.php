<?php
View::$title = 'Dashboard';
View::$bodyclass = User::info('Sidebar').' client-dashboard-page';
View::header();
?>

    <section class="header-bottom">
        <article>
            <div class="container">
                <h1>Customer Oriented</h1>
                <p>A customer service representative interacts with a company’s customers to provide them with information to address inquiries regarding products and services. In addition, they deal with and help resolve any customer complaints.</p>
            </div>
        </article>
    </section>

    <!-- content start -->
    <section class="gray">
        <article class="container projects-page" id="clientdashboard">
            <div class="row">
                <?php echo View::getMessage();  ?>

                <div class="col-md-12">
                    <div class="block">
                        <!-- Booking Summary -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="booking-summary">

                                    <div class="block-header">
                                        <div class="block-title">Tasks To Accomplish</div>
                                    </div>
                                    <div class="block-content text-center">
                                        <div class="row items-push text-center">
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'cs/clientprofiles' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Clients ) ? number_format( $allPendings->Clients ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-bank fa-1x"></i></div>
                                                    <label>Pending Clients</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'cs/investorprofiles' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Seeders ) ? number_format( $allPendings->Seeders ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-user fa-1x"></i></div>
                                                    <label for="">Pending Seeders</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'cs/bookings' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Bookings ) ? number_format( $allPendings->Bookings ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-book fa-1x"></i></div>
                                                    <label for="">Pending Bookings</label>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-lg-3">
                                                <a href="<?php echo View::url( 'cs/documents' ); ?>">
                                                    <div class="box-circle">
                                                        <?php echo isset( $allPendings->Documents ) ? number_format( $allPendings->Documents ) : '0'; ?>
                                                    </div>
                                                    <div class="push-10 icon"><i class="fa fa-files-o fa-1x"></i></div>
                                                    <label for="">Pending Documents</label>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END Booking Summary -->

                    </div>
                </div> 

            </div>
        </article>
    </section>
    <!-- content end -->

<?php View::footer(); ?>

<script type="text/javascript">

</script>