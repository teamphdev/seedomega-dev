<?php 
View::$title = isset($clientdata->CompanyName) ? $clientdata->CompanyName : "Client Profiles";
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink();?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url();?>"><?php echo View::$segments[0];?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1];?>">Client Profiles</a></li>
            <?php } ?>
            <?php if( isset(View::$segments[2]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><?php echo View::$title;?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      
      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-8">
        <div class="embed-responsive embed-responsive-16by9">
          <?php echo $clientdata->Video; ?>
        </div>
        <div class="pcontent tab-content tab-pane">
          <p class="text-muted"><i><?php echo isset($clientdata->VideoDescription) ? $clientdata->VideoDescription : "No Description"; ?></i></p>
          <h5>Offer Overview</h5>
          <p><?php echo isset($clientdata->OfferOverview) ? $clientdata->OfferOverview : ""; ?></p>
          
          <h5>Business Model</h5>
          <p><?php echo isset($clientdata->BusinessModel) ? $clientdata->BusinessModel : ""; ?></p>
          
          <h5>Key Invest Highlights</h5>
          <p><?php echo isset($clientdata->KeyInvestHighlights) ? $clientdata->KeyInvestHighlights : ""; ?></p>

          <h5>Strategy Vision</h5>
          <p><?php echo isset($clientdata->StrategyVision) ? $clientdata->StrategyVision : ""; ?></p>

          <h5>Market Demand</h5>
          <p><?php echo isset($clientdata->MarketDemand) ? $clientdata->MarketDemand : ""; ?></p>

          <h5>Board Management</h5>
          <p><?php echo isset($clientdata->BoardManagement) ? $clientdata->BoardManagement : ""; ?></p>

          <h5>Usage Of Funds</h5>
          <p><?php echo isset($clientdata->UsageOfFunds) ? $clientdata->UsageOfFunds : ""; ?></p>

          <h5>Financial Summary</h5>
          <p><?php echo isset($clientdata->FinancialSummary) ? $clientdata->FinancialSummary : ""; ?></p>

          <h5>Press Coverage</h5>
          <p><?php echo isset($clientdata->PressCoverage) ? $clientdata->PressCoverage : ""; ?></p>

          <h5>Disclosure</h5>
          <p><?php echo isset($clientdata->Disclosure) ? $clientdata->Disclosure : ""; ?></p>
        </div>
      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4">
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="project-progress">
              <?php $percentage = (int)(@($clientdata->TotalRaisedAmt / $clientdata->TargetGoal) * 100+.5); ?>
              <div class="popular-data data-single"> 
                <!-- <img src="<?php echo View::url() ?>/assets/images/funder_1.jpg" alt="Funder" /> -->
                
                <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>              

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($clientdata->TotalRaisedAmt) ? "$".number_format($clientdata->TotalRaisedAmt) : "0";?><br>
                        <small class="text-muted">Raised</small>
                    </div>                                        
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($clientdata->TotalInvestor) ? number_format($clientdata->TotalInvestor) : "0";?><br>
                        <small class="text-muted">Investors</small>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset($clientdata->TypeOfOffer) ? $clientdata->TypeOfOffer : "-"; ?><small>Type of Offer</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php 
                      $startdate = date_create($clientdata->OfferOpening);
                      $closedate = date_create($clientdata->OfferClosing);
                      $interval = date_diff($startdate, $closedate); 
                      ?>
                    <?php echo str_replace('+', '', $interval->format('%R%a'));?><small>Days Left</small>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo ($clientdata->OfferOpening != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferOpening)) : "-"; ?><small>Offer Open</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo ($clientdata->OfferClosing != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferClosing)) : "-"; ?><small>Offer Closing</small>
                  </div>
                </div>
                <?php /*
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->TargetGoal) ? number_format($clientdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    <?php echo isset($clientdata->SizeOfOffer) ? $clientdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
                  </div>
                </div>
                */ ?>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?><small>Price</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->TargetGoal) ? number_format($clientdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->MinimumBid) ? number_format($clientdata->MinimumBid) : "-"; ?><small>Minimum Investment</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($clientdata->MaximumBid) ? number_format($clientdata->MaximumBid) : "-"; ?><small>Maximum Investment</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 offer-lead offer-data">
                    <?php echo isset($clientdata->LeadManager) ? ucwords($clientdata->LeadManager) : "-"; ?><small>Lead Manager</small>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>
          
          <!-- We Need Volunteers -->
          
          <div class="sidebar-item">
            <div class="w-title">Status</div>
            <div class="text-center" style="padding:15px;">
                <?php if ($clientdata->Status =='Pending') : ?>
                Pending projects will not be shown to the investor.
                <a href="<?php echo View::url('cs/clientprofiles/approve/'.$clientdata->ClientProfileID); ?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;">Approve this Client</a>
                <?php else: ?>
                    <span class="label label-success"><i class="fa fa-check"></i> <?php echo $clientdata->Status;?></span>
                <?php endif; ?>
            </div>
          </div>
          
          
        </div>
      </div> 
             
    </div>
  </article>
</section>

<?php View::footer(); ?>