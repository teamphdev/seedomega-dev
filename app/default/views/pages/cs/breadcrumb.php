<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><?php echo View::$title; ?></li>
          </ul>
        </div>
        <div class="col-lg-6 align-right sub-menu">
          <a class="btn btn-info btn-sm text-uppercase" href="/manage/clients/add"><i class="si si-user-follow"></i> Create New</a>
        </div>
      </div>
    </article>
</section>