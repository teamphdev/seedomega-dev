<?php
    View::$title = 'Client Profiles';
    View::$bodyclass = 'cs-clients';
    View::header();
?>
<?php 
//View::template('users/banner'); ?>
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>
<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page">
        <?php echo View::getMessage(); ?>
        <!-- Stats -->
        <div class="content bg-white border-b push-20">
            <div class="row items-push text-uppercase">

                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Total Clients</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $allClientsCount; ?></a>
                </div>

               <!-- <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">All Companies</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"></a>
                </div>-->

                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                    <div class="text-muted animated fadeIn"><small><i class="si si-check"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $approvedClientsCount; ?></a>
                </div>

                <div class="col-xs-6 col-sm-3">
                    <div class="font-w700 text-gray-darker animated fadeIn">Pendings</div>
                    <div class="text-muted animated fadeIn"><small><i class="si si-clock"></i> Total</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $pendingClientsCount; ?></a>
                </div>

            </div>
        </div>
        <!-- END Stats -->

        <div class="block">
            <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                <li class="active">
                    <a href="#clients-all">All Client Profiles <span class="badge"><?php echo $allClientsCount; ?></span></a>
                </li>
                <li class="">
                    <a href="#clients-pending">Pending <span class="badge badge-warning"><?php echo $pendingClientsCount; ?></span></a>
                </li>
                <li class="">
                    <a href="#clients-request">Change Request <span class="badge badge-warning"><?php echo $pendingClientRequestsCount; ?></span></a>
                </li>
            </ul>
            <div class="block tab-content">
                <!-- Projects -->
                <div class="tab-pane fade fade-up in active" id="clients-all">
                    <table class="table table-divide table-hover table-vcenter clientprofiles" style="width: 100%;" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="/manage/clients/add"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Create New</a>'>
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th class="text-center"><i class="fa fa-suitcase text-gray"></i> Projects</th>
                            <th class="text-center no-wrap"><i class="fa fa-line-chart text-gray"></i> Target Goal</th>
                            <th class="text-center"><i class="fa fa-dollar text-gray"></i> Price</th>
                            <th class="text-center"><i class="fa fa-ticket text-gray"></i> Status</th>
                            <th class="text-center"><i class="fa fa-money text-gray"></i> Funded</th>
                            <th class="text-center" style="max-width:110px;"><i class="fa fa-gavel text-gray"></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( count( $allClients ) ){
                            foreach( $allClients as $cp ):

                                $startdate = date_create();
                                $closedate = date_create();
                                $interval = 1;

                                switch( $cp->Status ){
                                    case 'Completed':
                                        $statusfield = '<span class="label label-success"><i class="si si-check"></i> '.$cp->Status.'</span>';
                                        break;
                                    case 'Approved':
                                        $statusfield = '<span class="label label-success"><i class="si si-check"></i> '.$cp->Status.'</span>';
                                        break;
                                    case 'Pending':
                                    default:
                                        $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$cp->Status.'</span>';
                                        break;
                                    case 'Verified':
                                        $statusfield = '<span class="label label-info"><i class="si si-clock"></i> '.$cp->Status.'</span>';
                                        break;
                                } ?>
                                <tr>
                                    <td><?php echo $cp->DateAdded != '0000-00-00' ? date( 'Y-m-d H:i:s', strtotime( $cp->DateAdded ) ) : '-'; ?>                                        
                                    </td>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/project/view/<?php echo $cp->ClientProfileID;?>"><?php echo isset( $cp->CompanyName ) ? $cp->CompanyName: "-"; ?> </a>
                                        </h3><!-- 
                                        <div class="push-10 visible-xs">
                                            <?php echo $statusfield; ?>
                                        </div>

                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info">
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?> <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                </div>
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span>$<?php echo isset( $cp->SizeOfOffer ) ? $cp->SizeOfOffer : "-"; ?> <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                </div>
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span><?php echo isset( $cp->TargetGoal ) ? "$".number_format( $cp->TargetGoal ) : "-"; ?> <br><small class="text-muted">TARGET GOAL</small></span>
                                                </div>
                                                <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                    <span><?php echo isset( $cp->Price ) ? "$".number_format( $cp->Price, 2 ) : "-"; ?> <br><small class="text-muted">PRICE</small></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div> -->
                                    </td>
                                    <td class="text-right">
                                        <span><?php echo isset( $cp->TargetGoal ) ? "$".number_format( $cp->TargetGoal ) : "-"; ?></span>

                                    </td>
                                    <td class="text-right">
                                        <span><?php echo isset( $cp->Price ) ? "$".number_format( $cp->Price, 2 ) : "-"; ?></span>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td>
                                    <td class="h4 text-right text-primary" data-order="<?php echo isset( $cp->TotalRaisedAmt ) ? number_format( $cp->TotalRaisedAmt, 2 ) : ""; ?>">
                                        <?php echo isset( $cp->TotalRaisedAmt ) ? "$".number_format( $cp->TotalRaisedAmt, 2 ) : "-"; ?> <br>
                                        <div class="push-5-t visible-xs">
                                            <div class="btn-group">
                                                <a href="/cs/clientprofiles/view/<?php echo $cp->ClientProfileID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Details"><i class="fa fa-eye"></i></a>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <?php /*
                                        <div class="btn-group">
                                            <a href="/users/personal/<?php echo $cp->UserID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Edit Personal"><i class="si si-user"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="/clients/profile/company-info/<?php echo $cp->ClientProfileID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Edit Profile"><i class="si si-book-open"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="/cs/clientprofiles/view/<?php echo $cp->ClientProfileID;?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Details"><i class="fa fa-eye"></i></a>
                                        </div>
                                        */ ?>
                                        <div class="">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">

                                                    <li><a href="<?php echo View::url( 'users/personal/'.$cp->UserID ); ?>" data-toggle="tooltip" title="Edit Personal"><i class="si si-user pull-right"></i> Edit Personal</a></li>

                                                    <li><a href="<?php echo View::url( 'clients/profile/company-info/'.$cp->ClientProfileID ); ?>" data-toggle="tooltip" title="Edit Profile"><i class="fa fa-edit pull-right"></i> Edit Profile</a></li>

                                                    <li><a href="<?php echo View::url( 'project/view/'.$cp->ClientProfileID ); ?>" data-toggle="tooltip" title="<?php echo isset( $cp->Status ) && $cp->Status == 'Pending' ? 'Approve Profile' : 'View Details'; ?>"><i class="fa fa-eye pull-right"></i> <?php echo isset( $cp->Status ) && $cp->Status == 'Pending' ? 'Approve Profile' : 'View Details'; ?></a></li>

                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="7">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END Projects -->

                <!-- Users -->
                <div class="tab-pane fade fade-up" id="clients-pending">
                    <table class="table table-divide table-hover table-vcenter dt-responsive js-dataTable-full-pagination" style="width: 100%;" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="/manage/clients/add"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Create New</a>'>
                        <thead>
                        <tr>
                            <th class="text-center hidden-xs hidden-sm" style="width:30%;"><i class="fa fa-suitcase text-gray"></i> Projects</th>
                            <th class="text-center hidden-xs hidden-sm"><i class="si si-drawer text-gray"></i> Type Of Offer</th>
                            <th class="text-center hidden-xs hidden-sm"><i class="si si-note text-gray"></i> Notes</th>
                            <!-- <th class="text-center" style="width: 15%;"><i class="fa fa-money text-gray"></i> Investment</th> -->
                            <th class="text-center hidden-xs hidden-sm" style="max-width:110px;"><i class="fa fa-gavel text-gray"></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( count( $pendingClients ) ){
                            foreach( $pendingClients as $cp ):

                                switch( $cp->Status ){
                                    case 'Pending':
                                    default:
                                        $statusfield = '<small class="text-danger">Please approve this Profile!</small>';
                                        break;
                                    case 'Verified':
                                        $statusfield = '<small class="text-info">Subject for client Approval!</small>';
                                        break;
                                }
                                if( $cp->Status != 'Approved' ){ ?>
                                    <tr>
                                        <td class="">
                                            <span class="font-w600"><?php echo isset( $cp->CompanyName ) ? $cp->CompanyName : "-"; ?></span><br>
                                            <table class="table table-borderless visible-xs visible-sm  font-s13">
                                                <tbody>
                                                <tr>
                                                    <td class="font-w600" style="width: 30%;">Type</td>
                                                    <td><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="font-w600">Notes</td>
                                                    <td><?php echo $statusfield; ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td class="hidden-xs hidden-sm"><small><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?></small></td>
                                        <td class="hidden-xs hidden-sm"><?php echo $statusfield; ?></td>
                                        <!-- <td class="h4 text-right text-primary">
                                            <?php echo isset( $cp->TotalAmountAttached ) ? "$".number_format( $cp->TotalAmountAttached, 2 ) : "-"; ?>
                                            <div class="push-5-t visible-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/<?php echo $cp->ClientProfileID;?>" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </td> -->
                                        <td class="text-center hidden-xs hidden-sm">
                                            <div class="">
                                                <div class="dropdown more-opt">
                                                    <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                    <ul class="dropdown-menu dropdown-menu-right">

                                                        <li><a href="<?php echo View::url( 'users/personal/'.$cp->UserID ); ?>" data-toggle="tooltip" title="Edit Personal"><i class="si si-user pull-right"></i> Edit Personal</a></li>

                                                        <li><a href="<?php echo View::url( 'clients/profile/company-info/'.$cp->ClientProfileID ); ?>" data-toggle="tooltip" title="Edit Profile"><i class="fa fa-edit pull-right"></i> Edit Profile</a></li>

                                                        <li><a href="<?php echo View::url( 'project/view/'.$cp->ClientProfileID ); ?>" data-toggle="tooltip" title="<?php echo isset( $cp->Status ) && $cp->Status == 'Pending' ? 'Approve Profile' : 'View Details'; ?>"><i class="fa fa-eye pull-right"></i> <?php echo isset( $cp->Status ) && $cp->Status == 'Pending' ? 'Approve Profile' : 'View Details'; ?></a></li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="4">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Users -->

                <!-- Request -->
                <div class="tab-pane fade fade-up" id="clients-request">
                    <table class="table table-divide table-hover table-vcenter dt-responsive js-dataTable-full-pagination" style="width: 100%;" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="/manage/clients/add"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Create New</a>'>
                        <thead>
                        <tr>
                            <th class="text-center hidden-xs" style="width:30%;"><i class="fa fa-suitcase text-gray"></i> Projects</th>
                            <th class="text-center hidden-xs no-wrap"><i class="si si-drawer text-gray"></i> Type Of Offer</th>
                            <th class="text-center hidden-xs"><i class="si si-note text-gray"></i> Notes</th>
                            <th class="text-center hidden-xs no-wrap" style="width:15%;"><i class="fa fa-calendar text-gray"></i> Request Date</th>
                            <th class="text-center hidden-xs" style="max-width:110px;"><i class="fa fa-gavel text-gray"></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( count( $pendingClientRequests ) ){
                            foreach( $pendingClientRequests as $cp ):

                                $statusfield = '<small class="text-danger">Please approve this Profile change request!</small>'; ?>
                                <tr>
                                    <td class="">
                                        <span class="font-w600"><?php echo isset( $cp->CompanyName ) ? $cp->CompanyName : "-"; ?></span><br>
                                        <table class="table table-borderless visible-xs visible-sm  font-s13">
                                            <tbody>
                                            <tr>
                                                <td class="font-w600" style="width: 30%;">Type</td>
                                                <td><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="font-w600">Notes</td>
                                                <td><?php echo $statusfield; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="hidden-xs hidden-sm"><small><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?></small></td>
                                    <td class="hidden-xs no-wrap"><?php echo $statusfield; ?></td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo isset( $cp->RequestDate ) && $cp->RequestDate != "0000-00-00" ? date( 'j M Y', strtotime( $cp->RequestDate ) ) : "-"; ?>
                                    </td>
                                    <td class="text-center hidden-xs hidden-sm">
                                        <div class="">
                                            <a href="<?php echo View::url( 'clients/changeprofile/company-info/'.$cp->ClientProfileID ); ?>" class="btn btn-sm btn-default btn-rounded" type="button">Approve Profile</i></a>
                                        </div>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Request -->
            </div>
        </div>

<?php /*
        <div id="projlist" class="row">
                <?php
        if(count($bookedFunds)){
            foreach($bookedFunds as $proj):

                $startdate = date_create($cp->OfferOpening);
                $closedate = date_create($cp->OfferClosing);
                $interval = date_diff($startdate, $closedate);
                ?>
                        <article class="col-lg-3 col-md-3 col-sm-4 col-xs-12 project-cnt">
                            <div class="popular-item">
                                <?php
                switch ($cp->Status) {
                    case 'Approved':
                        $ribboncolor = "ribbon-success";
                        $ribbonico = '<i class="si si-check"></i>';
                        break;
                    case 'Pending':
                        $ribboncolor = "ribbon-warning";
                        $ribbonico = '<i class="si si-clock"></i>';
                        break;
                    case 'Verified':
                        $ribboncolor = "ribbon-info";
                        $ribbonico = '<i class="si si-user-following"></i>';
                        break;
                }
                ?>
                                <figure class="project-image ribbon ribbon-modern <?php echo $ribboncolor; ?>">
                                    <div class="ribbon-box font-w600">
                                       <?php echo $ribbonico; ?> <?php echo $cp->Status; ?>
                                    </div>
                                    <figcaption >
                                        <a href="/bookings/info/<?php echo $cp->InvestmentBookingID;?>" class="item_button1">Edit</a>
                                        <a href="/clients/view/<?php echo $cp->ClientProfileID;?>" class="item_button2">View</a>
                                    </figcaption>
                                    <?php echo View::image('popular_1.jpg','Popular'); ?>
                                </figure>
                                <div class="popular-content">
                                    <div class="project-desc">
                                        <h5><a href="#"><?php echo $cp->CompanyName;?>  <span class="line-green"></span></a></h5>
                                        <p>Investor Name : <b><?php echo isset($cp->FirstName) ? $cp->FirstName : ""; ?> <?php echo isset($cp->LastName) ? $cp->LastName : ""; ?></strong></b></p>
                                        <p>Size of Offer : <b><?php echo isset($cp->SizeOfOffer) ? $cp->SizeOfOffer : ""; ?></b> </p>
                                        <p>Minimum Bid : <b>$<?php echo isset($cp->MinimumBid) ? number_format($cp->MinimumBid) : ""; ?></b> </p>
                                        <p>Price : <b>$<?php echo isset($cp->Price) ? number_format($cp->Price,2) : ""; ?></b> </p>
                                    </div>
                                    <div class="popular-data">
                                        <div class="c100 p75 small green">
                                            <span>25%</span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="popular-details">
                                        <ul>
                                            <li><strong>$<?php echo isset($cp->TotalAmountAttached) ? number_format($cp->TotalAmountAttached,2) : ""; ?></strong> <small>Invested Amount</small></li>
                                            <li class="last"><strong><?php echo isset($cp->SharesToApply) ? number_format($cp->SharesToApply) : ""; ?></strong> <small>Total Shares</small></li>
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </article>
                    <?php endforeach; ?>
                    <?php }else{ ?>
                        <article class="col-lg-3 col-md-3 col-sm-4 col-xs-12 project-cnt">
                            <div class="popular-item">
                                <figure class="project-image ribbon ribbon-modern ribbon-warning">
                                    <div class="ribbon-box font-w600">
                                        <i class="fa fa-clock-o"></i> Pending
                                    </div>
                                    <figcaption>
                                        <a href="#" class="item_button1">Edit</a>
                                        <a href="#" class="item_button2">View</a>
                                    </figcaption>
                                    <?php echo View::image('popular_1.jpg','Popular'); ?>
                                </figure>
                                <div class="popular-content">
                                    <div class="project-desc">
                                        <h5><a href="#">Company Title here  <span class="line-green"></span></a></h5>
                                        <p>No Data</p>
                                    </div>
                                    <div class="popular-data">
                                        <div class="c100 p0 small green">
                                            <span>0%</span>
                                            <div class="slice">
                                                <div class="bar"></div>
                                                <div class="fill"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="popular-details">
                                        <ul>
                                            <li><strong>$0</strong> <small>Invested Amount</small></li>
                                            <li class="last"><strong>10</strong> <small>Your Total Shares</small></li>
                                        </ul>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </article>
                    <?php } ?>
                <div class="clear"></div>

            </div>
*/ ?>

    </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".clientprofiles").dataTable({
            "lengthMenu": [ [25, 50, 75, 100, -1], [25, 50, 75, 100, "All"] ],
            "pageLength": 25,
            "order": [[ 0, "desc" ]],
            "columnDefs": [
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                }
            ],
            "drawCallback": function( settings ) {
                jQuery(this).each(function(){
                    var addBtn = jQuery(this).attr('addbutton');
                    if(addBtn){
                        jQuery(this).parents().eq(2).find('.table-custom-btn').not('.addedbtn').append(addBtn).addClass('addedbtn');
                    }
                });
            }
        });
    });
</script>
