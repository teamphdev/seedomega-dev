<?php 
View::$title = isset($investordata->FirstName) ? $investordata->FirstName.' '.$investordata->LastName : "Investor Profiles";
View::$bodyclass = '';
View::header();
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title;?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>">Investor Profiles</a></li>
              <li class="fa fa-angle-right"></li>
              <li><?php echo View::$title; ?></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      
      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-8">
          <div class="block">

            <div class="block-content tab-content bg-white">
              <div class="tab-pane fade fade-up in active" id="investor-info">
                <?php echo View::getMessage();?>
              </div>

              <div class="tab-pane fade fade-up" id="client-blogs">
              </div>
            </div><!--end block-content-->

          </div><!--end block-->
      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4">
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="project-progress">

              <div class="offer-details" style="padding: 15px;">
                <div class="row">
                    <div class="col-md-4 col-sm-7 col-xs-12 col-md-offset-1">
                        <div class="avatar-container personal">
                            <?php $avatar = View::common()->getUploadedFiles($investordata->Avatar); ?>
                            <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lname"><?php echo Lang::get('USR_EDIT_LN'); ?></label>
                  </div>
                  <div class="col-xs-6 offer-close offer-data" id="lname"><?php echo $investordata->LastName;?></div>
                </div>
                <div class="row">
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname"><?php echo Lang::get('USR_EDIT_FN'); ?></label>
                  </div>
                  <div class="col-xs-6 offer-close offer-data" id="fname"><?php echo $investordata->FirstName;?></div>
                </div>

                <div class="row">
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"><?php echo Lang::get('USR_EDIT_EML'); ?></label>
                  </div>
                  <div class="col-xs-6 offer-close offer-data" id="email"><?php echo $investordata->Email;?></div>
                </div>
                <div class="row">
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone"><?php echo Lang::get('USR_EDIT_PHNE'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="phone"><?php echo $investordata->Phone;?></div>
                </div>
                <div class="row">
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gender"><?php echo Lang::get('USR_EDIT_GNDR'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="gender">
                    <label class="css-input css-radio css-radio-info push-10-r">
                        <input readonly type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $investordata->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                    </label>
                    <label class="css-input css-radio css-radio-info">
                        <input readonly type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $investordata->Gender == 'F' ? 'checked' : ''; ?> required /><span></span><?php echo Lang::get('USR_PRF_GNDRF'); ?>
                    </label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address"><?php echo Lang::get('USR_EDIT_ADDS'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="address"><?php echo $investordata->Address;?></div>
                </div>
                <div class="row">                    
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city"><?php echo Lang::get('USR_EDIT_CTY'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="city"><?php echo $investordata->City;?></div>
                </div>
                <div class="row">                    
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state"><?php echo Lang::get('USR_EDIT_STATES'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="state"><?php echo $investordata->State;?></div>
                </div>
                <div class="row">                    
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="country"><?php echo Lang::get('USR_EDIT_CNTRY'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="country"><?php echo $investordata->Country;?></div>
                </div>
                <div class="row">                    
                  <div class="col-xs-3 offer-open offer-data">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pcode"><?php echo Lang::get('USR_EDIT_PCODE'); ?></label>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data" id="pcode"><?php echo $investordata->PostalCode;?></div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>
          
          <!-- We Need Volunteers -->
          <?php if(Apputility::investorinfo('InvestorStatus',$investordata->UserID) == 'Verification'){ ?>
            <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
              <input type="hidden" name="action" value="approveinvestor" />
              <input type="hidden" name="Status" value="Approved" />
              <input type="hidden" name="UserID" value="<?php echo $investordata->UserID;?>" />
              <input type="hidden" name="Investor" value="<?php echo $investordata->FirstName.' '.$investordata->LastName;?>" />
              <div class="sidebar-item">
                <div class="w-title">Click Approve to accept Investor!</div>
                <div class="text-center" style="padding:15px;">
                  <a href="<?php echo View::url('cs/investorprofiles/view'); ?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;">Approve</a>
                </div>
              </div>
            </form>
          <?php } ?>
        </div>
      </div>

    </div>
  </article>
</section>

<?php View::footer(); ?>