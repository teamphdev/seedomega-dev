<?php
View::$title = 'Investor Profiles';
View::$bodyclass = 'dev';
View::header();
?>
    <?php //View::template('users/banner') ?>
    <section class="header-bottom">
        <article>
            <div class="container">
                <h1><?php echo View::$title; ?></h1>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container projects-page">
            <?php echo View::getMessage(); ?>
            <!-- Stats -->
            <div class="content bg-white border-b push-20">
                <div class="row items-push text-uppercase">
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Total Investor</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-users"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $allCount; ?></a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-following"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $approvedCount; ?></a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Verification</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-follow"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $verificationCount; ?></a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <div class="font-w700 text-gray-darker animated fadeIn">Incomplete</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-user-unfollow"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo $incompleteCount; ?></a>
                    </div>
                </div>
            </div>
            <!-- END Stats -->
            
            <div class="block">
                <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                    <li class="active">
                        <a href="#Investor-incomplete">Incomplete <span class="badge badge-danger"><?php echo $incompleteCount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#Investor-verification">Verification <span class="badge badge-info"><?php echo $verificationCount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#Investor-approved">Approved <span class="badge badge-success"><?php echo $approvedCount; ?></span></a>
                    </li>
                    <li class="">
                        <a href="#Investor-request">Change Request <span class="badge badge-warning"><?php echo $requestCount; ?></span></a>
                    </li>
                </ul>
                <div class="block tab-content">

                    <!-- incomplete -->
                    <div class="tab-pane fade fade-up in active" id="Investor-incomplete">
                        <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Join Date</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $profiles['Incomplete'] ) ){
                                    foreach( $profiles['Incomplete'] as $profile ): ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600 push-10">
                                                    <?php echo isset( $profile->FirstName ) ? ucwords( $profile->FirstName ) : ""; ?> <?php echo isset( $profile->LastName ) ? ucwords( $profile->LastName ) : ""; ?>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <?php echo $profile->StatusHTML; ?>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="hidden"><?php echo isset( $profile->JoinDate ) && $profile->JoinDate != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $profile->JoinDate ) ) : "-"; ?></span>
                                                <?php echo isset( $profile->JoinDate ) && $profile->JoinDate != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $profile->JoinDate ) ) : ""; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo isset( $profile->AccountType ) ? ucwords( $profile->AccountType ) : ""; ?>
                                            </td>

                                            <td class="h5 text-primary text-center">
                                                <?php /*
                                                <div class="btn-group">
                                                    <a href="/users/profile/documents/<?php echo $profile->UserID; ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="Edit Profile"><i class="si si-book-open"></i></a>
                                                </div>
                                                <div class="btn-group">
                                                    <a href="/cs/investorprofiles/view/<?php echo $profile->UserID; ?>" class="btn btn-xs btn-default" data-toggle="tooltip" title="View Profile"><i class="fa fa-eye"></i></a>
                                                </div>
                                                */ ?>

                                                <div class="">
                                                    <div class="dropdown centered more-opt max120">
                                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                        <ul class="dropdown-menu dropdown-menu-right">

                                                            <li><a href="<?php echo View::url( 'users/profile/documents/'.$profile->UserID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-edit pull-right"></i> Edit</a></li>

                                                            <li><a href="<?php echo View::url( 'cs/investorprofiles/view/'.$profile->UserID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-eye pull-right"></i> View</a></li>

                                                        </ul>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="4">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END incomplete -->
                    
                    <!-- verification -->
                    <div class="tab-pane fade fade-up" id="Investor-verification">
                        <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width:100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Join Date</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $profiles['Verification'] ) ){
                                    foreach( $profiles['Verification'] as $profile ): ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600">
                                                    <?php echo isset( $profile->FirstName ) ? ucwords( $profile->FirstName ) : ""; ?> <?php echo isset( $profile->LastName ) ? ucwords( $profile->LastName ) : ""; ?>
                                                </h3>
                                                <div class="push-10-t visible-xs">
                                                    <?php echo $profile->StatusHTML; ?>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="hidden"><?php echo isset( $profile->JoinDate ) && $profile->JoinDate != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $profile->JoinDate ) ) : "-"; ?></span>
                                                <?php echo ( $profile->JoinDate != '0000-00-00 00:00:00' ) ? date( 'j M Y', strtotime( $profile->JoinDate ) ) : ""; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo isset( $profile->AccountType ) ? ucwords( $profile->AccountType ) : ""; ?>
                                            </td>

                                            <td class="h5 text-primary text-center">

                                                <div class="">
                                                    <div class="dropdown centered more-opt max120">
                                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                        <ul class="dropdown-menu dropdown-menu-right">

                                                            <li><a href="<?php echo View::url( 'users/profile/documents/'.$profile->UserID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-edit pull-right"></i> Edit</a></li>

                                                            <li><a href="<?php echo View::url( 'cs/investorprofiles/view/'.$profile->UserID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-eye pull-right"></i> View</a></li>

                                                        </ul>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="4">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END verification -->

                    <!-- approved -->
                    <div class="tab-pane fade fade-up " id="Investor-approved">
                        <table class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Join Date</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $profiles['Approved'] ) ){
                                    foreach( $profiles['Approved'] as $profile ): ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600">
                                                    <?php echo isset( $profile->FirstName ) ? ucwords( $profile->FirstName ) : ""; ?> <?php echo isset( $profile->LastName ) ? ucwords( $profile->LastName ) : ""; ?>
                                                </h3>
                                                <div class="push-10-t visible-xs">
                                                    <?php echo $profile->StatusHTML; ?>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="hidden"><?php echo isset( $profile->JoinDate ) && $profile->JoinDate != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $profile->JoinDate ) ) : "-"; ?></span>
                                                <?php echo ( $profile->JoinDate != '0000-00-00 00:00:00' ) ? date( 'j M Y', strtotime( $profile->JoinDate ) ) : ""; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo isset( $profile->AccountType ) ? ucwords( $profile->AccountType ) : ""; ?>
                                            </td>

                                            <td class="h5 text-primary text-center">
                                                <div class="">
                                                    <div class="dropdown centered more-opt max120">
                                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                        <ul class="dropdown-menu dropdown-menu-right">

                                                            <li><a href="<?php echo View::url( 'users/profile/documents/'.$profile->UserID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-edit pull-right"></i> Edit</a></li>

                                                            <li><a href="<?php echo View::url( 'cs/investorprofiles/view/'.$profile->UserID ); ?>" data-toggle="tooltip" title=""><i class="fa fa-eye pull-right"></i> View</a></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="4">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END approved -->

                    <!-- request -->
                    <div class="tab-pane fade fade-up " id="Investor-request">
                        <table class="table table-divide table-hover table-vcenter table-bordered  js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-user text-gray"></i> Name</th>
                                    <th class="text-center">Request Date</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( isset( $profileChangeRequests ) ){
                                    foreach( $profileChangeRequests as $request ): ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600">
                                                    <?php echo isset( $request->FirstName ) ? ucwords( $request->FirstName ) : ""; ?> <?php echo isset( $request->LastName ) ? ucwords( $request->LastName ) : ""; ?>
                                                </h3>
                                                <div class="push-10-t visible-xs">
                                                    <?php echo $request->StatusHTML; ?>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="hidden"><?php echo isset( $request->RequestDate ) && $request->RequestDate != '0000-00-00 00:00:00' ? date( 'Y-m-d h:i:s', strtotime( $request->RequestDate ) ) : "-"; ?></span>
                                                <?php echo isset( $request->RequestDate ) && $request->RequestDate != '0000-00-00 00:00:00' ? date( 'j M Y', strtotime( $request->RequestDate ) ) : "-"; ?>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo isset( $request->AccountType ) ? ucwords( $request->AccountType ) : ""; ?>
                                            </td>
                                            <td class="h5 text-primary text-center">
                                                <div class="">
                                                    <a href="<?php echo View::url( 'users/profile/documents/'.$request->UserID.'/1' ); ?>" class="btn btn-sm btn-default btn-rounded" type="button">Approve Profile</i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="4">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END request -->

                </div>
            </div>
        </article>
    </section>

<?php View::footer(); ?>

<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script> -->