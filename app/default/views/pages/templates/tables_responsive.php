<?php 
View::$title = 'Tables';
View::$bodyclass = 'loginpage';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1>Tables</h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li>Tables</li>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Full Table -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Full Table</h3>
            </div>
            <div class="block-content">
                <p class="push-30">The first way to make a table responsive, is to wrap it with <code>&lt;div class=&quot;table-responsive&quot;&gt;&lt;/div&gt;</code>. This way the table will be horizontally scrollable and all data will be accessible on smaller screens (&lt; 768px).</p>
                <div class="table-responsive">
                    <table class="table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 120px;"><i class="si si-user"></i></th>
                                <th>Name</th>
                                <th style="width: 30%;">Email</th>
                                <th style="width: 15%;">Access</th>
                                <th class="text-center" style="width: 100px;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_1.jpg" alt="">
                                </td>
                                <td class="font-w600">Sara Holland</td>
                                <td>client1@example.com</td>
                                <td>
                                    <span class="label label-warning">Trial</span>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_2.jpg" alt="">
                                </td>
                                <td class="font-w600">Ann Parker</td>
                                <td>client2@example.com</td>
                                <td>
                                    <span class="label label-warning">Trial</span>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_3.jpg" alt="">
                                </td>
                                <td class="font-w600">Amber Walker</td>
                                <td>client3@example.com</td>
                                <td>
                                    <span class="label label-success">VIP</span>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_4.jpg" alt="">
                                </td>
                                <td class="font-w600">Evelyn Willis</td>
                                <td>client4@example.com</td>
                                <td>
                                    <span class="label label-danger">Disabled</span>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_1.jpg" alt="">
                                </td>
                                <td class="font-w600">Ann Parker</td>
                                <td>client5@example.com</td>
                                <td>
                                    <span class="label label-warning">Trial</span>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Full Table -->

        <!-- Partial Table -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Partial Table</h3>
            </div>
            <div class="block-content">
                <p>The second way is to use specific CSS classes for hiding columns in various screen resolutions. This way you can hide the less important columns and keep the most valuable on mobiles. At the following example the <strong>Access</strong> column isn't visible on small and extra small screens and <strong>Email</strong> column isn't visible on extra small screens.</p>
                <table class="table table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 120px;"><i class="si si-user"></i></th>
                            <th>Name</th>
                            <th class="hidden-xs" style="width: 30%;">Email</th>
                            <th class="hidden-xs hidden-sm" style="width: 15%;">Access</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">
                                <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_2.jpg" alt="">
                            </td>
                            <td class="font-w600">Donald Barnes</td>
                            <td class="hidden-xs">client1@example.com</td>
                            <td class="hidden-xs hidden-sm">
                                <span class="label label-success">VIP</span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_3.jpg" alt="">
                            </td>
                            <td class="font-w600">George Stone</td>
                            <td class="hidden-xs">client2@example.com</td>
                            <td class="hidden-xs hidden-sm">
                                <span class="label label-success">VIP</span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_4.jpg" alt="">
                            </td>
                            <td class="font-w600">Keith Simpson</td>
                            <td class="hidden-xs">client3@example.com</td>
                            <td class="hidden-xs hidden-sm">
                                <span class="label label-warning">Trial</span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_1.jpg" alt="">
                            </td>
                            <td class="font-w600">Vincent Sims</td>
                            <td class="hidden-xs">client4@example.com</td>
                            <td class="hidden-xs hidden-sm">
                                <span class="label label-success">VIP</span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <img class="img-avatar img-avatar48" src="<?php echo View::url('assets/images/'); ?>funder_2.jpg" alt="">
                            </td>
                            <td class="font-w600">Bruce Edwards</td>
                            <td class="hidden-xs">client5@example.com</td>
                            <td class="hidden-xs hidden-sm">
                                <span class="label label-info">Business</span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit Client"><i class="fa fa-pencil"></i></button>
                                    <button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove Client"><i class="fa fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Partial Table -->

        <!-- Helper Classes -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Helper Classes</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-striped table-borderless text-center">
                        <thead>
                            <tr>
                                <th style="width: 10%;"></th>
                                <th class="text-center">
                                    Extra small devices<br>
                                    <small class="text-capitalize font-w400 text-muted">Phones (&lt;768px)</small>
                                </th>
                                <th class="text-center">
                                    Small devices<br>
                                    <small class="text-capitalize font-w400 text-muted">Tablets (&ge;768px)</small>
                                </th>
                                <th class="text-center">
                                    Medium devices<br>
                                    <small class="text-capitalize font-w400 text-muted">Desktops (&ge;992px)</small>
                                </th>
                                <th class="text-center">
                                    Large devices<br>
                                    <small class="text-capitalize font-w400 text-muted">Desktops (&ge;1200px)</small>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><code>.visible-xs</code></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                            </tr>
                            <tr>
                                <td><code>.visible-sm</code></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                            </tr>
                            <tr>
                                <td><code>.visible-md</code></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                            </tr>
                            <tr>
                                <td><code>.visible-lg</code></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <td><code>.hidden-xs</code></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                            </tr>
                            <tr>
                                <td><code>.hidden-sm</code></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                            </tr>
                            <tr>
                                <td><code>.hidden-md</code></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                            </tr>
                            <tr>
                                <td><code>.hidden-lg</code></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-check text-success"></i></td>
                                <td><i class="si si-close text-danger"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Helper Classes -->
    </div>
    <!-- END Page Content -->
</section>

<?php View::footer(); ?>