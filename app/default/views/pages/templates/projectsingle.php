<?php 
View::$title = 'IPO Single';
View::$bodyclass = 'loginpage';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1>IPO Single</h1></div>
    </article>
</section>

<section class="breadcrumb">
  <article class="container">
    <div class="row">
      <div class="col-lg-6">
        <ul>
          <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
          <li><a href="index.html">Home</a></li>
          <li class="fa fa-angle-right"></li>
          <li><a href="projects.html">Projects</a></li>
          <li class="fa fa-angle-right"></li>
          <li>Park At Birdcagewalk</li>
        </ul>
      </div>
      <div class="col-lg-6 align-right sub-menu social">
        <ul>
          <li><i class="fa fa-map-marker"></i> &nbsp; Birdcage Walk, London, UK &nbsp;&nbsp;</li>
          <li>&nbsp;&nbsp;<a href="#"><span class="icon-community"></span> &nbsp; Community</a></li>
        </ul>
      </div>
    </div>
  </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      
      <!-- ************************ Left Side Area ************************ -->
      
      <div class="col-lg-8">
        
        <!-- ************************ Project Gallery ************************ -->
      
        <div class="pslider">
          <div class="slide"><img src="assets/images/projects/singe-2.jpg" alt="img" /></div>
          <div class="slide"><img src="assets/images/projects/singe-1.jpg" alt="img" /></div>
          <div class="slide"><img src="assets/images/projects/singe-3.jpg" alt="img" /></div>
          <div class="slide"><img src="assets/images/projects/singe-4.jpg" alt="img" /></div>
          <div class="slide"><img src="assets/images/projects/singe-5.jpg" alt="img" /></div>
          <div class="slide"><img src="assets/images/projects/singe-6.jpg" alt="img" /></div>
        </div>
        
        <div class="pcontent">
          <!-- ************************ Tabs Start ************************ -->
          <div class="tabpanel">
            <ul class="nav nav-tabs pstabs">
              <li class="active"><a href="#about-project">About the project</a></li>
              <li><a href="#costs">Costs</a></li>
              <li><a href="#faqs">FAQ's</a></li>
              <li><a href="#news">News <i>4</i></a></li>
              <li><a href="#comments">Comments <i>57</i></a></li>
            </ul>
            
            <div class="tab-content">
              
              <!-- ************************ Details About Project ************************ -->
              
              <div class="tab-pane active" id="about-project">
                <h5>We are building a park at Birdcagewalk</h5>
                <p>Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. <a href="#">Pellentesque habitant morbi</a> tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero. Morbi aliquam ornare lobortis. Nam nec fermentum erat.</p>
                
                <ul>
                  <li>Aenean pharetra diam in nisl mattis</li>
                  <li>Ut a lacinia metus</li>
                  <li>Nunc ullamcorper varius metus</li>
                  <li>Donec ut ligula vitae risus elementum tempor</li>
                  <li>Etiam porta massa sit amet purus viverra</li>
                </ul>
                
                <p>Vestibulum a justo libero. Praesent vehicula id lectus semper hendrerit. Vestibulum tempus porta mi, sit amet blandit velit mattis nec. Nunc accumsan elementum gravida. Integer eu gravida massa. Maecenas ligula leo, feugiat a finibus ut, placerat a turpis. Ut viverra neque non suscipit imperdiet. Proin ut erat eleifend metus tristique rutrum ut at enim. Aliquam eget blandit dolor, ut interdum metus. Maecenas eros orci, congue quis urna eget, condimentum semper risus. Etiam feugiat rutrum viverra.</p>
                
                <figure>
                  <img src="assets/images/projects/singe-7.jpg" alt="The Pulpit Rock">
                </figure>
                
                <p>Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero. Morbi aliquam ornare lobortis. Nam nec fermentum erat.</p>
                
                <h5>What does Phase 1 include?</h5>
                <ul>
                  <li>Aenean pharetra diam in nisl mattis</li>
                  <li>Ut a lacinia metus</li>
                  <li>Nunc ullamcorper varius metus</li>
                  <li>Donec ut ligula vitae risus elementum tempor</li>
                  <li>Etiam porta massa sit amet purus viverra</li>
                </ul>
                
                <p><strong>Aenean pharetra</strong> diam in nisl mattis, nec laoreet libero mollis. Ut a lacinia metus, non luctus diam. Nullam at nibh facilisis, ullamcorper ipsum ut, finibus magna. Etiam porta massa sit amet purus viverra, et lacinia lectus tempor.Aenean elit purus, imperdiet nec faucibus ut, finibus ac metus. Donec ut ligula vitae risus elementum tempor. Nunc ullamcorper varius metus, id ullamcorper dolor commodo ac.</p>
                
                <figure>
                  <img src="assets/images/graph.png" alt="The Pulpit Rock">
                </figure>
                
                <p>Vestibulum a justo libero. Praesent vehicula id lectus semper hendrerit. Vestibulum tempus porta mi, sit amet blandit velit mattis nec. Nunc accumsan elementum gravida. Integer eu gravida massa. Maecenas ligula leo, feugiat a finibus ut, placerat a turpis. Ut viverra neque non suscipit imperdiet. Proin ut erat eleifend metus tristique rutrum ut at enim. Aliquam eget blandit dolor, ut interdum metus. <a href="#">Maecenas eros orci</a> congue quis urna eget, condimentum semper risus. Etiam feugiat rutrum viverra.</p>
                
                <div class="videoWrapper">
                  <iframe src="https://player.vimeo.com/video/136277304?title=0&amp;byline=0&amp;portrait=0"></iframe>
                </div>
                
                <p>Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero. Morbi aliquam ornare lobortis. Nam nec fermentum erat.</p>
                
                <h5>What else you can do for Birdcagewalk</h5>
                <p>Aenean pharetra diam in nisl mattis, nec laoreet libero mollis. Ut a lacinia metus, non luctus diam. Nullam at nibh facilisis, ullamcorper ipsum ut, finibus magna. Etiam porta massa sit amet purus viverra, et lacinia lectus tempor.Aenean elit purus, imperdiet nec faucibus ut, finibus ac metus. Donec ut ligula vitae risus elementum tempor. Nunc ullamcorper varius metus, id ullamcorper dolor commodo ac.</p>

                <p>Vestibulum a justo libero. Praesent vehicula id lectus semper hendrerit. Vestibulum tempus porta mi, sit amet blandit velit mattis nec. Nunc accumsan elementum gravida. Integer eu gravida massa. Maecenas ligula leo, feugiat a finibus ut, placerat a turpis. Ut viverra neque non suscipit imperdiet. Proin ut erat eleifend metus tristique rutrum ut at enim. Aliquam eget blandit dolor, ut interdum metus. Maecenas eros orci, congue quis urna eget, condimentum semper risus. Etiam feugiat rutrum viverra.</p>
                
              </div>
              
              <!-- ************************ Costs Chart ************************ -->
              
              <div class="tab-pane costs-detail" id="costs">
                <h5><i class="fa fa-tags"></i>Costs</h5>
                <ul>
                  <li data-cost="420000" data-color="79FED1">Cost to build and plant boundary beds <span>$4,200,00</span></li>
                  <li data-cost="220000" data-color="5AF2BF">Cost to build and plant Natural Play garden <span>$2,200,00</span></li>
                  <li data-cost="220000" data-color="47DEAB">Cost to plant community orchard with 10 fruiting trees <span>$2,200,00</span></li>
                  <li data-cost="70000" data-color="34CC99">Other <span>$700,00</span></li>
                </ul>
                <div class="cost-total">Total <span>$ 9,300,00 <span>USD</span></span></div>
                <div id="total-barChart"><!--Total cost bar chart--></div>
              </div>
              
              <!-- ************************ Project FAQs ************************ -->
              
              <div class="tab-pane" id="faqs">
                <h5><i class="fa fa-question q-circle"></i>FAQ</h5>
                <div class="accordion" id="accordion-faq">
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne">
                        Pellentesque pharetra sapien non quam placerat vestibulum ?
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapseOne" class="accordion-body collapse in">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <ul>
                          <li>Aenean pharetra diam in nisl mattis</li>
                          <li>Ut a lacinia metus</li>
                          <li>Nunc ullamcorper varius metus</li>
                          <li>Donec ut ligula vitae risus elementum tempor</li>
                          <li>Etiam porta massa sit amet purus viverra</li>
                        </ul>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapseTwo">
                        Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat ?
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapseThree">
                        Pellentesque pharetra sapien non quam placerat vestibulum ?
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapseThree" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                </div>
              
              </div>
              
              <!-- ************************ Project News ************************ -->
              
              <div class="tab-pane" id="news">
                <h5><i class="fa fa-newspaper-o"></i>News</h5>
                
                <div class="accordion" id="accordion-news">
                
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapse1">
                        Pellentesque pharetra sapien non quam placerat vestibulum.
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapse1" class="accordion-body collapse in">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam.</p>
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <ul>
                          <li>Aenean pharetra diam in nisl mattis</li>
                          <li>Ut a lacinia metus</li>
                          <li>Nunc ullamcorper varius metus</li>
                          <li>Donec ut ligula vitae risus elementum tempor</li>
                          <li>Etiam porta massa sit amet purus viverra</li>
                        </ul>
                        <p><strong>Pellentesque pharetra</strong> sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <figure>
                          <img src="assets/images/projects/news-img.jpg" alt="News" title="">
                        </figure>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapse2">
                        Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapse2" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapse3">
                        Pellentesque pharetra sapien non quam placerat vestibulum.
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapse3" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                  
                  <div class="accordion-group">
                    <div class="accordion-heading">
                      <a class="accordion-toggle" data-toggle="collapse" href="#collapse4">
                        Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.
                        <span class="updown-cion"></span>
                      </a>
                    </div>
                    <div id="collapse4" class="accordion-body collapse">
                      <div class="accordion-inner">
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                        <p>Etiam sagittis ante eget nisi tempus, elementum sollicitudin nulla lacinia Aenean sed odio ipsum. Nam tincidunt bibendum quam, sed ultricies nisl tristique eget. Maecenas facilisis sollicitudin magna at fringilla. Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat condimentum. Sed iaculis magna mi, sed semper ex semper eu. Vestibulum auctor mi sed.</p>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              
              
              <!-- ************************ Project Comments ************************ -->
              
              <div class="tab-pane" id="comments">
                <h5><i class="fa fa-comments"></i>Comments</h5>
                
                <div class="detailBox">
                  <div class="actionBox">
                    <ul class="commentList">
                      <li>
                        <div class="commenterImage"><img src="assets/images/funder_1.jpg" alt="Funder" /></div>
                        <div class="commentText">
                          <p class="">Hello this is a test comment.</p> <span class="date sub-text">on July 24th, 2015</span>
                        </div>
                      </li>
                      <li>
                        <div class="commenterImage"><img src="assets/images/funder_2.jpg" alt="Funder" /></div>
                        <div class="commentText">
                          <p class="">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p> <span class="date sub-text">on July 24th, 2015</span>
                        </div>
                      </li>
                      <li>
                        <div class="commenterImage"><img src="assets/images/funder_3.jpg" alt="Funder" /></div>
                        <div class="commentText">
                          <p class="">Hello this is a test comment.</p> <span class="date sub-text">on July 24th, 2015</span>
                        </div>
                      </li>
                      <li>
                        <div class="commenterImage"><img src="assets/images/funder_4.jpg" alt="Funder" /></div>
                        <div class="commentText">
                          <p class="">Hello this is a test comment and this comment is particularly very long and it goes on and on and on. Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p> <span class="date sub-text">on July 24th, 2015</span>
                        </div>
                      </li>
                    </ul>
                    <p>&nbsp;</p>
                    <h5>Leave A Reply</h5>
                    <form class="form-inline" action="project-single.html" method="post">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="Your Name" />
                      </div>
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="Your Email" />
                      </div>
                      <div class="form-group">
                        <textarea class="form-control" placeholder="Your Comments"></textarea>
                      </div>
                      <div class="form-group">
                        <button type="button" class="btn btn-5 blue">Add Comment</button>
                      </div>
                    </form>
                  </div>
                </div>
                  
              </div>
              
                              
            </div>
          </div>
          <!-- Tabs End -->
        </div>
      </div>
      
      <!-- ************************ Right Side Area ************************ -->
      
      <div class="col-lg-4">
        <div class="sidebar">
          
          <!-- Project Progress -->
          
          <div class="sidebar-item">
            <div class="project-progress">
              <div class="popular-data data-single"> <img src="assets/images/funder_1.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="75" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
              <div class="popular-details">
                <ul>
                  <li class="last"><strong>$7,000,00</strong> Funded</li>
                  <li><strong>175</strong> Backers</li>
                  <li><strong>12</strong> Days Left</li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
          
          <!-- We Need Volunteers -->
          
          <div class="sidebar-item">
            <div class="w-title">We Need Volunteers!</div>
            <div style="padding:15px;">
              <p>Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero.</p>
              <div class="list-info">
                <p>Bibiana Serpa <span>Responsive! Park Project</span></p>
              </div>
              <div class="side-box">
                <form action="project-single.html">
                  <div class="form-group">
                    <label for="amount"><strong>Your Contribution</strong></label>
                    <input type="text" class="form-control" id="amount" placeholder="Enter an amount">
                  </div>
                  <div style="text-align:center;"><button type="button" class="btn btn-3 green" style="margin:10px 0px 5px 0px;">Contribute Now</button></div>
                </form>
              </div>
            </div>
          </div>
          
          <!-- Select a Perk -->
          
          <div class="sidebar-item">
            <div class="w-title">Select a Perk</div>
            <div class="perk-wrapper">
              <ul>
                <li>
                  <a href="#">
                    <span class="perk-price"><strong>$50</strong> USD</span>
                    <span class="perk-type">Featured</span>
                    <span class="clear"></span>
                    <span class="perk-title">Limited Birdcagewalk T-shirts</span>
                    <span class="perk-txt">Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero.</span>
                    <span class="perk-claimed">35 claimed</span>
                    <span class="perk-delivery"><strong>Estimated delivery:</strong> November 2015</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="perk-price"><strong>$10</strong> USD</span>
                    <span class="clear"></span>
                    <span class="perk-title">Walking Tours 8/15</span>
                    <span class="perk-txt">Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero.</span>
                    <span class="perk-claimed">2 out of 90 claimed</span>
                    <span class="perk-delivery"><strong>Estimated delivery:</strong> November 2015</span>
                  </a>
                </li>
                <li class="perk-disabled">
                  <a href="#">
                    <span class="perk-price"><strong>$50</strong> USD</span>
                    <span class="perk-type">Sold Out</span>
                    <span class="clear"></span>
                    <span class="perk-title">Multicombiloop Lock</span>
                    <span class="perk-txt">Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero.</span>
                    <span class="perk-claimed">3 out of 3 claimed</span>
                    <span class="perk-delivery"><strong>Estimated delivery:</strong> November 2015</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="perk-price"><strong>$125</strong> USD</span>
                    <span class="clear"></span>
                    <span class="perk-title">T-shirt Package</span>
                    <span class="perk-txt">Etiam id eleifend nisl. Fusce ultricies ultricies lorem, sit amet fringilla magna scelerisque non. Sed auctor facilisis est, a porttitor risus tempus sit amet. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc vel imperdiet nisl, sit amet semper libero.</span>
                    <span class="perk-claimed">35 claimed</span>
                    <span class="perk-delivery"><strong>Estimated delivery:</strong> November 2015</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          
          <!-- Find This Project -->
          
          <div class="sidebar-item">
            <div class="w-title">Find This Project On</div>
            <div class="find-project">
              <ul>
                <li><a target="_blank" href="https://www.facebook.com/" class="fb transition-all" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a target="_blank" href="https://twitter.com/" class="tw transition-all" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a target="_blank" href="https://www.youtube.com/" class="yt transition-all" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                <li><a target="_blank" href="#" class="wb transition-all" title="Website"><i class="fa fa-link"></i></a></li>
              </ul>
            </div>
          </div>
          
        </div>
      </div> 
             
    </div>
  </article>
</section>

<?php View::footer(); ?>