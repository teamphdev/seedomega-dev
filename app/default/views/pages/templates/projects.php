<?php 
View::$title = 'IPO Listing';
View::$bodyclass = 'loginpage';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1>IPO Listing</h1></div>
    </article>
</section>

<section class="breadcrumb">
  <article class="container">
    <div class="row">
      <div class="col-lg-6">
        <ul>
          <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
          <li><a href="index.html">Home</a></li>
          <li class="fa fa-angle-right"></li>
          <li>Projects</li>
        </ul>
      </div>
      <div class="col-lg-6 align-right sub-menu">
        <ul>
          <li><a href="projects.html">Trending</a></li>
          <li><a href="projects.html">Final Countdown</a></li>
          <li><a href="projects.html">New This Week</a></li>
          <li><a href="projects.html">Most Funded</a></li>
        </ul>
      </div>
    </div>
  </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="gray">
  <article class="container projects-page" id="popular">
    <div class="row">
      
      <!-- ************************ Left Side Area ************************ -->
      
      <div class="col-lg-9 content-cnt">
        <div class="col-lg-4 project-cnt">
          <div class="popular-item">
            <figure class="project-image">
              <figcaption><a href="project-single.html">View Details</a></figcaption>
              <img src="assets/images/popular_1.jpg" alt="img" />
            </figure>
            <div class="popular-content">
              <div class="project-desc">
                <h5><a href="#">xHelp us to save the nature  <span class="line-green"></span></a></h5>
                <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
              </div>
              <div class="popular-data"> <img src="assets/images/funder_1.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="75" data-barcolor="#34cc99" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
              <div class="popular-details">
                <ul>
                  <li><strong>12</strong> Days Left</li>
                  <li><strong>175</strong> Backers</li>
                  <li class="last"><strong>R$7.000,00</strong> Funded</li>
                </ul>
              </div>
              <div class="cler"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 project-cnt">
          <div class="popular-item">
            <figure class="project-image">
              <figcaption><a href="project-single.html">View Details</a></figcaption>
              <img src="assets/images/popular_2.jpg" alt="img" />
            </figure>
            <div class="popular-content">
              <div class="project-desc">
                <h5><a href="#">Charity hospital  <span class="line-yellow"></span></a></h5>
                <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
              </div>
              <div class="popular-data"> <img src="assets/images/funder_2.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="85" data-barcolor="#f2d031" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
             <div class="popular-details">
                <ul>
                 <li><strong>12</strong> Days Left</li>
                  <li><strong>175</strong> Backers</li>
                  <li class="last"><strong>R$7.000,00</strong> Funded</li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 project-cnt">
          <div class="popular-item">
            <figure class="project-image">
              <figcaption><a href="project-single.html">View Details</a></figcaption>
              <img src="assets/images/popular_4.jpg" alt="img" />
            </figure>
            <div class="popular-content">
              <div class="project-desc">
                <h5><a href="#">Flood affected people  <span class="line-blue"></span></a></h5>
                <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
              </div>
              <div class="popular-data"> <img src="assets/images/funder_4.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="65" data-barcolor="#3298c9" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
              <div class="popular-details">
                <ul>
                  <li><strong>12</strong> Days Left</li>
                  <li><strong>175</strong> Backers</li>
                  <li class="last"><strong>R$7.000,00</strong> Funded</li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        
        <div class="col-lg-4 project-cnt">
          <div class="popular-item">
            <figure class="project-image">
              <figcaption><a href="project-single.html">View Details</a></figcaption>
              <img src="assets/images/popular_4.jpg" alt="img" />
            </figure>
            <div class="popular-content">
              <div class="project-desc">
                <h5><a href="#">Flood affected people  <span class="line-red"></span></a></h5>
                <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
              </div>
              <div class="popular-data"> <img src="assets/images/funder_1.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="55" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
              <div class="popular-details">
                <ul>
                  <li><strong>12</strong> Days Left</li>
                  <li><strong>175</strong> Backers</li>
                  <li class="last"><strong>R$7.000,00</strong> Funded</li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 project-cnt">
          <div class="popular-item">
            <figure class="project-image">
              <figcaption><a href="project-single.html">View Details</a></figcaption>
              <img src="assets/images/popular_3.jpg" alt="img" />
            </figure>
            <div class="popular-content">
              <div class="project-desc">
                <h5><a href="#">We Can Build Our Church  <span class="line-green"></span></a></h5>
                <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
              </div>
              <div class="popular-data"> <img src="assets/images/funder_2.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="20" data-barcolor="#34cc99" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
             <div class="popular-details">
                <ul>
                 <li><strong>12</strong> Days Left</li>
                  <li><strong>175</strong> Backers</li>
                  <li class="last"><strong>R$7.000,00</strong> Funded</li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 project-cnt">
          <div class="popular-item">
            <figure class="project-image">
              <figcaption><a href="project-single.html">View Details</a></figcaption>
              <img src="assets/images/popular_1.jpg" alt="img" />
            </figure>
            <div class="popular-content">
              <div class="project-desc">
                <h5><a href="#">Help us to save the nature <span class="line-yellow"></span></a></h5>
                <p>Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
              </div>
              <div class="popular-data"> <img src="assets/images/funder_4.jpg" alt="Funder" />
                <div class="pie_progress" role="progressbar" data-goal="70" data-barcolor="#f2d031" data-barsize="7.1">
                  <div class="pie_progress__number">0%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
              <div class="popular-details">
                <ul>
                  <li><strong>12</strong> Days Left</li>
                  <li><strong>175</strong> Backers</li>
                  <li class="last"><strong>R$7.000,00</strong> Funded</li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        
        <div class="popular-btn"> <a href="#">See More</a> </div>        
      </div>
      
      <!-- ************************ Right Side Area ************************ -->
      
      <div class="col-lg-3 sidebar-container">
        <div class="sidebar">
        
          <div class="sidebar-item">
            <div class="w-title">Search Projects</div>
            <div class="w-content">
              <form action="projects.html">
                <fieldset>
                  <div class="form-group">
                    <label for="categories"><strong>Categories</strong></label>
                    <select id="categories" class="form-control arrow-down">
                      <option value="">All Categories</option>
                      <option value="Animals">Animals</option>
                      <option value="Community">Community</option>
                      <option value="Education">Education</option>
                      <option value="Environment">Environment</option>
                      <option value="Food">Food</option>
                      <option value="Health">Health</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="city"><strong>City</strong></label>
                    <input type="text" class="form-control" id="city" placeholder="City name">
                  </div>
                  <div class="form-group">
                    <label for="country"><strong>Country</strong></label>
                    <select id="country" class="form-control arrow-down">
                      <option value="">All Countries</option>
                      <option value="Australia">Australia</option>
                      <option value="Canada">Canada</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="United States">United States</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><strong>Percent Funded</strong></label>
                  </div>
                  <div class="radio-inputs">

                    <div class="form-group radio-cnt">
                      <input type="radio" value="all" name="percent-funded" id="all" checked="checked" >
                      <label for="all">All</label>
                    </div>
                    <div class="form-group radio-cnt">
                      <input type="radio" value="50-75" name="percent-funded" id="5075" >
                      <label for="5075">50% - 75%</label>
                    </div>
                    <div class="form-group radio-cnt">
                      <input type="radio" value="75-100" name="percent-funded" id="75100" >
                      <label for="75100">75% - 100%</label>
                    </div>
                  </div>
                  <div class="clear"></div>
                  <button type="button" class="btn btn-4 green submit-filters">Submit</button>
                </fieldset>
              </form>
            </div>
          </div>
        
          <div class="sidebar-item">
            <div class="w-title">Popular Categories</div>
            <div class="w-content popular-categories">
              <ul>
                <li><a href="#"><span><i class="icon-animals"></i></span>Animals</a></li>
                <li><a href="#"><span><i class="icon-art"></i></span>Art</a></li>
                <li><a href="#"><span><i class="icon-community"></i></span>Community</a></li>
                <li><a href="#"><span><i class="icon-dance"></i></span>Dance</a></li>
                <li><a href="#"><span><i class="icon-design"></i></span>Design</a></li>
                <li><a href="#"><span><i class="icon-education"></i></span>Education</a></li>
                <li><a href="#"><span><i class="icon-environment"></i></span>Environment</a></li>
                <li><a href="#"><span><i class="icon-fashion"></i></span>Fashion</a></li>
                <li><a href="#"><span><i class="icon-film"></i></span>Film</a></li>
                <li><a href="#"><span><i class="icon-food"></i></span>Food</a></li>
                <li><a href="#"><span><i class="icon-gaming"></i></span>Gaming</a></li>
                <li><a href="#"><span><i class="icon-health"></i></span>Health</a></li>
                <li><a href="#"><span><i class="icon-music"></i></span>Music</a></li>
                <li><a href="#"><span><i class="icon-politics"></i></span>Politics</a></li>
              </ul>
              <div class="clear"></div>
            </div>
          </div>
          
          <div class="sidebar-item">
            <div class="stories-slider">
              <div class="slide">
                <div class="w-title">Success Stories</div>
                <div class="w-content success-stories">
                  <div class="ssimg"><img src="assets/images/stories/1.jpg" alt="Success Stories" /></div>
                  <h5>John Doe</h5>
                  <p><i class="fa fa-quote-left"></i> Pellentesque pharetra sapien non quam placerat vestibulum. Aliquam dignissim ex nec feugiat.</p>
                </div>
              </div>
              <div class="slide">
                <div class="w-title">Success Stories</div>
                <div class="w-content success-stories">
                  <div class="ssimg"><img src="assets/images/stories/2.jpg" alt="Success Stories" /></div>
                  <h5>John Doe</h5>
                  <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
                </div>
              </div>
              <div class="slide">
                <div class="w-title">Success Stories</div>
                <div class="w-content success-stories">
                  <div class="ssimg"><img src="assets/images/stories/3.jpg" alt="Success Stories" /></div>
                  <h5>John Doe</h5>
                  <p><i class="fa fa-quote-left"></i> Iincididunt ut labore et dolore magna aliqua. Sed ut perspiciatis unde omnis iste natus.</p>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      
    </div>
    
  </article>
</section>

<?php View::footer(); ?>