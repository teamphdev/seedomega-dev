<?php 
View::$title = 'User Login';
View::$bodyclass = 'loginpage';
View::header(); 
?>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li>Join</li>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray" style="background: url('<?php echo View::url('assets/');?>images/photo28.jpg') no-repeat 0 center; background-size: cover;">
    <article class="container contactpage">
      <div class="row">
        
          <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">

            <div class="start-project">
                
                <div class="block">

                  <!-- Nav tabs -->
                  <ul class="nav nav-justified nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#signup" aria-controls="signup" role="tab" data-toggle="tab"><i class="fa fa-user"></i> Sign Up</a></li>
                    <li role="presentation"><a href="#login" aria-controls="login" role="tab" data-toggle="tab"><i class="fa fa fa-sign-in"></i> Login</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content start-content">
                    <div role="tabpanel" class="tab-pane active" id="signup">
                        <br>
                        <h3>Sign Up</h3>
                        
                        <div class="alert hide" id="form-message">
                          <!-- Form Alert Goes Here -->
                        </div>
                        
                        <form class="form-ui" method="post" id="contact-form">
                        
                        <div id="basic-data" class="form-wizard active">
                                    
                          <div class="form-group">
                            <div class="form-left">
                              <input type="text" value="" class="form-control" placeholder="Last Name" name="lastname"">
                            </div>
                            <div class="form-right">
                              <input type="text" value="" class="form-control" placeholder="First Name" name="firstname">
                            </div>
                            <div class="clear"></div>
                          </div>
                          
                          <div class="form-group">
                            <input type="email" value="" class="form-control" placeholder="Email Address" name="email">
                          </div>
                          
                          <div class="form-group">
                            <select class="form-control arrow-down">
                                <?php foreach(AppUtility::getCountries() as $country) { ?>
                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                <?php } ?>
                            </select>
                          </div>
                          
                          <div class="form-group text-center">
                            <button type="submit" class="btn btn-4 blue default">Submit</button>
                          </div>
                          
                        </div>
                        
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="login">
                        <br>
                        <h3>Login</h3>
                        
                        <div class="alert hide" id="form-message">
                          <!-- Form Alert Goes Here -->
                        </div>
                        
                        <form class="form-ui" method="post" id="contact-form">
                        
                        <div id="basic-data" class="form-wizard active">
                                    
                          <div class="form-group">
                            <input type="text" value="" class="form-control" placeholder="Username" name="username"">
                          </div>
                          
                          <div class="form-group">
                            <input type="password" value="" class="form-control" placeholder="Password" name="password">
                          </div>

                          <!-- <a href="#" class="btn btn-facebook"> <i class="fa fa-facebook"></i> &nbsp; Log in with Facebook </a> -->
                          
                          <div class="form-group">
                            <a type="submit" class="btn btn-4 blue default">Login</a>
                          </div>
                          
                        </div>
                        
                        </form>
                    </div>
                  </div>

                </div>

            </div>
            
          </div>
               
      </div>
    </article>
</section>

<?php View::footer(); ?>