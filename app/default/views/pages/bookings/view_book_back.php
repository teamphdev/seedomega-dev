<?php 
View::$title = 'View Booking';
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">

    <div class="client-company text-center">
      <h3>SHARE APPLICATION FORM </h3>
      <p>GLOBAL ASSET RESOURCES LTD (ACN 618 792 877) – APPLICATION FORM </p>
      <hr><br>
      <div class="status-wizzard">
        <div class="well">
          <div class="title">
            <?php 
            switch ($bookdata->BookingStatus) {
              case 'Pending':
                $link1 = "done";
                $link2 = "";
                $link3 = "";
                break;
              case 'Verified':
                $link1 = "done";
                $link2 = "done";
                $link3 = "";
                break;
              case 'Approved':
                $link1 = "done";
                $link2 = "done";
                $link3 = "done";
                break;
            }
            ?>
            <ul>
              <li class="<?php echo $link1; ?>"><a href="javascript:void();"><i class="si si-clock"></i><span>Pending</span></a></li>
              <li class="<?php echo $link2; ?>"><a href="javascript:void();"><i class="si si-user-following"></i><span>Verified</span></a></li>
              <li class="<?php echo $link3; ?>"><a href="javascript:void();"><i class="si si-check"></i><span>Approved</span></a></li> 
            </ul>
          </div>
        </div>
      </div>
    </div><br>

    <form class="form-ui input_mask" enctype="multipart/form-data" method="post" style="padding: 0;">
    <?php 
    $hide = '';
    if($hide != "hide"){ ?>
    <input type="hidden" name="action" value="updatebooking">
    <?php } ?>
    <input type="hidden" name="bookid" value="<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : ""; ?>">
    <input type="hidden" name="usrid" value="<?php echo isset($bookdata->UserID) ? $bookdata->UserID : ""; ?>">

    <div class="row about-project">
      
      <!-- ************************ Left Side Area ************************ -->
      <div class="col-lg-8">
        
        <div class="pcontent">
          <?php echo View::getMessage(); ?>
             <div id="basic-data" class="form-wizard active">
              <?php 
              if($bookdata->BookingStatus == "Approved" || $bookdata->BookingStatus == "Verified"){
                $readonly = 'readonly';
                $disabled = 'disabled';
                $hide = 'hide';
                if(User::info('UserLevel') == "Administrator"){
                  $readonly = '';
                  $disabled = '';
                  $hide = '';
                }
              }else{
                $readonly = 'readonly';
                $disabled = 'disabled';
                $hide = '';
              } ?>
                <b>INVESTMENT INFORMATION </b>
                
                <?php if($bookdata->TTReceiptPhoto == 0){ ?>
                  <div class="alert alert-danger push-10-t fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Please Uploaded The TT Receipt Photo!</div>
                <?php } ?>

                <?php echo AppUtility::getBookedTTReceipt($bookdata); ?>
                <?php if($hide != "hide" || User::can('Manage Uploaded Documents')){ ?>
                <div class="form-group">
                  <label for="">TT Receipt Photo <small class="text-muted">(Transaction Receipt)</small></label>
                  <input type="file" value="" class="file form-control" name="TTReceiptPhoto" placeholder="">
                </div>
                <?php } ?>
                <div class="form-group">
                   <div class="form-left">
                    <label for="">Total Shares at <span>$<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?></span> per Share</label>
                      <input type="hidden" id="shareprice" value="<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?>">
                      <input type="hidden" id="minimumbid" value="<?php echo isset($bookdata->MinimumBid) ? $bookdata->MinimumBid : "0"; ?>">
                      <div class="input-group">
                        <input type="text" id="SharesToApply" value="<?php echo isset($bookdata->SharesToApply) ? $bookdata->SharesToApply : "0"; ?>" class="form-control" name="book[SharesToApply]" placeholder="0" required="required" <?php echo $readonly; ?>>
                        <span class="input-group-addon">Shares</span>
                      </div>
                   </div>
                   <div class="form-right">
                    <label for="">Total application monies of</label>
                      <div class="input-group">
                        <span class="input-group-addon">USD</span>
                        <input type="text" id="TotalAmountAttached" value="<?php echo isset($bookdata->TotalAmountAttached) ? $bookdata->TotalAmountAttached : "0"; ?>" class="form-control" name="book[TotalAmountAttached]" placeholder="0.00" readonly="readonly">
                      </div>
                   </div>
                   <div class="clear"></div>
                </div>                  

                <hr><br>
                <b>PERSONAL DETAILS</b>
                <div class="form-group">
                  <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                  <input type="text" value="<?php echo isset($bookdata->Name) ? $bookdata->Name : ""; ?>" class="form-control" name="book[Name]" placeholder="" <?php echo $readonly; ?>>
                </div>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">Joint Applicant 2 or account name</label>
                      <input type="text" value="<?php echo isset($bookdata->AccoutName2) ? $bookdata->AccoutName2 : ""; ?>" class="form-control" name="book[AccoutName2]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">   
                    <label for="">Joint Applicant 3 or account name</label>                     
                      <input type="text" value="<?php echo isset($bookdata->AccuntName3) ? $bookdata->AccuntName3 : ""; ?>" class="form-control" name="book[AccuntName3]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <hr><br>
                <b>POSTAL ADDRESS </b>

                <div class="form-group">
                   <div class="form-left">
                    <label for="">Address</label>
                    <input type="text" value="<?php echo isset($bookdata->Address) ? $bookdata->Address : ""; ?>" class="form-control" name="book[Address]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Suburb</label>                    
                    <input type="text" value="<?php echo isset($bookdata->Suburb) ? $bookdata->Suburb : ""; ?>" class="form-control" name="book[Suburb]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">State</label>
                      <input type="text" value="<?php echo isset($bookdata->State) ? $bookdata->State : ""; ?>" class="form-control" name="book[State]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Postcode</label>                   
                    <input type="text" value="<?php echo isset($bookdata->PostCode) ? $bookdata->PostCode : ""; ?>" class="form-control" name="book[PostCode]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <hr><br>
                <b>CONTACT DETAILS </b>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">Telephone Number</label>
                      <input type="text" value="<?php echo isset($bookdata->Phone) ? $bookdata->Phone : ""; ?>" class="form-control" name="book[Phone]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Email Address</label>                   
                    <input type="email" value="<?php echo isset($bookdata->Email) ? $bookdata->Email : ""; ?>" class="form-control" name="book[Email]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <hr><br>
                <b>Tax File Number(s) or Exemption(s)* </b>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">Holder 1</label>
                      <input type="text" value="<?php echo isset($bookdata->TaxHolder1) ? $bookdata->TaxHolder1 : ""; ?>" class="form-control" name="book[TaxHolder1]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Holder 2</label>                   
                    <input type="text" value="<?php echo isset($bookdata->TaxHolder2) ? $bookdata->TaxHolder2 : ""; ?>" class="form-control" name="book[TaxHolder2]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                   <p class="text-left"><small class="text-muted">*Collection of Australian Tax File Numbers is authorised by taxation laws. Quotation of your TFN is not compulsory and will not affect your Application. Non-residents – please indicate County of residence.</small></p>
                </div>

                <hr><br>
                <b>Applications – cheque details </b>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">Drawer</label>
                      <input type="text" value="<?php echo isset($bookdata->ChequeDrawer) ? $bookdata->ChequeDrawer : ""; ?>" class="form-control" name="book[ChequeDrawer]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Bank</label>                   
                    <input type="text" value="<?php echo isset($bookdata->ChequeBank) ? $bookdata->ChequeBank : ""; ?>" class="form-control" name="book[ChequeBank]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">Branch</label>
                      <input type="text" value="<?php echo isset($bookdata->ChequeBranch) ? $bookdata->ChequeBranch : ""; ?>" class="form-control" name="book[ChequeBranch]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Amount</label>                   
                    <input type="text" value="<?php echo isset($bookdata->ChequeAmount) ? $bookdata->ChequeAmount : ""; ?>" class="form-control" name="book[ChequeAmount]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <?php if($hide != "hide"){ ?>
                <div class="form-group text-center">
                  <label class="css-input css-checkbox css-checkbox-info">
                      <input type="checkbox" value="" required="required"> <span></span> I/We Read and Understood the above
                  </label>
                  <br>
                   <button type="submit" class="btn btn-4 blue default">Save Changes</button>
                </div>
                <?php } ?>
             </div>
          
        </div>
      </div>
      
      <!-- ************************ Right Side Area ************************ -->
      <div class="col-lg-4">
        <div class="sidebar">

          <?php if(User::info('UserLevel') == "Administrator" || User::can('Verify Booking')){ ?>
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="w-title">Change Status</div>
            <div class="pcontent">
              <select name="book[BookingStatus]" class="form-control">
                <option value="Pending" <?php echo ($bookdata->BookingStatus == 'Pending') ? "selected" : ""; ?>>Pending</option>
                <option value="Verified" <?php echo ($bookdata->BookingStatus == 'Verified') ? "selected" : ""; ?>>Verified</option>
                <?php if(User::info('UserLevel') == "Administrator"){ ?>
                <option value="Approved" <?php echo ($bookdata->BookingStatus == 'Approved') ? "selected" : ""; ?>>Approved</option>
                <?php } ?>
              </select>
            </div>
          </div>
          <?php } ?>

          <?php if($bookdata->TTReceiptPhoto == 0 || User::info('UserLevel') == "Administrator"){ ?>
          <div class="sidebar-item">
            <div class="w-title">Demand Note</div>
            <div class="pcontent text-center">
              <a class="btn btn-danger" href="<?php echo View::url('bookings/downloadbookingdemandnote/'.$bookdata->InvestmentBookingID) ?>" style="margin-right: 5px;"><i class="fa fa-cloud-download"></i> Download</a>
            </div>
          </div>
          <?php } ?>

          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="w-title"><?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : "No title"; ?></div>

            <div class="project-progress client-company pcontent">

              <?php 
              $percentage = 0;
              if($bookdata->TargetGoal > 0){
                $percentage = (int)(($bookdata->TotalRaisedAmt / $bookdata->TargetGoal) * 100+.5); 
              } ?>
              <div class="popular-data data-single"> 
                <!-- <img src="<?php echo View::url() ?>/assets/images/funder_1.jpg" alt="Funder" /> -->
                
                <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>
              

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->TotalRaisedAmt) ? "$".number_format($bookdata->TotalRaisedAmt) : "0";?><br>
                        <small class="text-muted">Raised</small>
                    </div>                                        
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->TotalInvestor) ? number_format($bookdata->TotalInvestor) : "0";?><br>
                        <small class="text-muted">Investors</small>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset($bookdata->TypeOfOffer) ? $bookdata->TypeOfOffer : "-"; ?><small>Type of Offer</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php 
                      $startdate = date_create($bookdata->OfferOpening);
                      $closedate = date_create($bookdata->OfferClosing);
                      $interval = date_diff($startdate, $closedate); 
                      ?>
                    <?php echo str_replace('+', '', $interval->format('%R%a'));?><small>Days Left</small>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo ($bookdata->OfferOpening != "0000-00-00") ? date('j M Y', strtotime($bookdata->OfferOpening)) : "-"; ?><small>Offer Open</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo ($bookdata->OfferClosing != "0000-00-00") ? date('j M Y', strtotime($bookdata->OfferClosing)) : "-"; ?><small>Offer Closing</small>
                  </div>
                </div>
                <?php /*
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->TargetGoal) ? number_format($bookdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    <?php echo isset($bookdata->SizeOfOffer) ? $bookdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
                  </div>
                </div>
                */ ?>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?><small>Price</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->TargetGoal) ? number_format($bookdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->MinimumBid) ? number_format($bookdata->MinimumBid) : "-"; ?><small>Minimum Investment</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->MaximumBid) ? number_format($bookdata->MaximumBid) : "-"; ?><small>Maximum Investment</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 offer-lead offer-data">
                    <?php echo isset($bookdata->LeadManager) ? ucwords($bookdata->LeadManager) : "-"; ?><small>Lead Manager</small>
                  </div>

                  <div class="col-sm-12 col-xs-12 col-md-12">
                    <div class="embed-responsive embed-responsive-16by9">
                      <?php echo isset($bookdata->Video) ? $bookdata->Video : ""; ?>
                    </div>
                    <p class="small video-caption"><?php echo isset($bookdata->VideoDescription) ? $bookdata->VideoDescription : "No Description"; ?></p>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>

      </div> 
             
    </div>
    </form>
  </article>
</section>

<?php View::footer(); ?>