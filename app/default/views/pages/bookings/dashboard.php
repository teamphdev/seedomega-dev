<?php
View::$title = 'Bookings';
View::$bodyclass = '';
View::header();
?>
    <section class="header-bottom">
        <article>
            <div class="container animated fadeInDown">
                <h1>My Seeding Account</h1>
                <p>SeedOmega makes investing affordable, easy and fun. Until recently only the very wealthy and venture capitalists could get involved in capital raising. Now through SeedOmega, anyone can invest in startup businesses that have exciting growth potential.</p>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->
    
    <section class="gray">
        <article class="container projects-page animated zoomIn">
            <?php echo View::getMessage(); ?>
            <!-- Stats -->
            <div class="content bg-white border-b push-20">
                <div class="row items-push text-uppercase">
                    <div class="col-sm-3 col-xs-12">
                        <div class="font-w700 text-gray-darker animated fadeIn">Total Investment</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Approved</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);">$<?php echo isset($totals->TotalAmountInvestment) ? number_format($totals->TotalAmountInvestment) : 0; ?></a>
                    </div>                                      
                    <div class="col-sm-2 col-xs-12">
                        <div class="font-w700 text-gray-darker animated fadeIn">Pendings</div>
                        <div class="text-muted animated fadeIn"><small><i class="si si-clock"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo View::url( 'bookings/pending' ); ?>"><?php echo isset($totals->TotalPendings) ? number_format($totals->TotalPendings) : 0; ?></a>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <div class="font-w700 text-gray-darker animated fadeIn">Verified</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo View::url( 'bookings/verified' ); ?>"><?php echo isset($totals->TotalVerified) ? number_format($totals->TotalVerified) : 0; ?></a>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <div class="font-w700 text-gray-darker animated fadeIn">Registered</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo View::url( 'bookings/registered' ); ?>"><?php echo isset($totals->TotalRegistered) ? number_format($totals->TotalRegistered) : 0; ?></a>
                    </div>
                    <div class="col-sm-2 col-xs-12">
                        <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                        <div class="text-muted animated fadeIn"><small><i class="fa fa-check"></i> Total</small></div>
                        <a class="h2 font-w300 text-primary animated flipInX" href="<?php echo View::url( 'bookings/approved' ); ?>"><?php echo isset($totals->TotalApproved) ? number_format($totals->TotalApproved) : 0; ?></a>
                    </div>  
                </div>
            </div>
            <!-- END Stats -->

            <div class="project-grid row animated zoomIn">

                <?php
                $cntr = 0;
                foreach( $projects as $proj ): $cntr++; ?>
                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12 project-cnt ccatid-<?php echo $proj->ClientCatID; ?> <?php echo $proj->Data['ProjectClass']; ?> sortcatid-<?php echo $proj->ClientCatID; ?>">

                        <div class="percent-funded" style="display: none;"><?php echo $proj->Data['Percentage']; ?></div>
                        <div class="date-updated" style="display: none;"><?php echo date( 'Y-m-d', strtotime( $proj->UpdatedAt ) ); ?></div>

                        <div class="popular-item booking-list">
                            <div class="ft-img">
                                <?php $companyImage = View::common()->getUploadedFiles( $proj->CompanyPhoto ); ?>
                                <?php echo View::photo( ( isset( $companyImage[0] ) ? 'files'.$companyImage[0]->FileSlug : '/images/user.png'), "" ); ?>
                                <figcaption><a class="showDetails" href="<?php echo View::url( "bookings/getbookingsbyproject/$proj->ClientProfileID", true ); ?>">View Bookings</a></figcaption>
                            </div>

                            <?php /*
                            <figure class="project-image">
                                <figcaption >
                                    <!-- <a href="/bookings/info/<?php echo $proj->InvestmentBookingID;?>" class="btn button-success item_button1">View Booking Info</a> -->
                                    <a class="block block-rounded block-link-hover3 showDetails" href="<?php echo View::url( "bookings/getbookingsbyproject/$proj->ClientProfileID", true ); ?>">View Bookings</a>
                                </figcaption>
                                <?php $companyImage = View::common()->getUploadedFiles( $proj->CompanyPhoto ); ?>
                                <?php echo View::photo( ( isset( $companyImage[0] ) ? 'files'.$companyImage[0]->FileSlug : '/images/user.png'), "" ); ?>
                            </figure>
                            */ ?>

                            <div class="popular-content">
                                <div class="project-desc project-desc" style="height: 30px;">
                                    <h5><a href="/project/view/<?php echo $proj->ClientProfileID;?>"><?php echo $proj->CompanyName; ?>  <span class="line-green"></span></a></h5>
                                </div>

                                <div class="popular-button">
                                    <a href="<?php echo View::url('project/view/'.$proj->ClientProfileID); ?>" class="btn btn-primary btn-rounded nonverified">View Project</a>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </div>
                    <?php
                    //echo ($cntr % 3) ? '' : '<div class="clear"></div>';
                endforeach; ?>
            </div>

        </article>
    </section>

    <div class="modal" id="modal-global" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Booking Info</h3>
                    </div>
                    <div class="block-content">
                        <!-- <div>Loading...</div> -->
                        <!-- modal spinner -->
                        <div class="modal fade spinner-modal-pop" data-backdrop="static" data-keyboard="false" tabindex="-1">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <span class="fa fa-spinner fa-spin fa-5x"></span>
                            </div>
                          </div>
                        </div>
                        <!-- END modal spinner -->
                    </div>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                </div>
            </div>
        </div>
    </div>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script>