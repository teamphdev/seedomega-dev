<?php 
View::$title = 'Register';
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown">
        <h1>REGISTER INSTEREST SHARE APPLICATION FORM</h1>
    </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Dashboard</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
   <article class="container bookpage">
      <?php if ($exemption == 1) : ?>
      <div class="corner-box"><div class="cribbon pink"><span>PROMO</span></div></div>
      <?php endif; ?>
      <?php echo View::getMessage(); ?>

      <div class="client-company">
        <div class="row">
          <!--p>GLOBAL ASSET RESOURCES LTD (ACN 618 792 877) – APPLICATION FORM </p-->
          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row push-20">
              <div class="col-md-2 com-logo">
                <?php echo $clientdata->Data['CompanyLogoLink']; ?>
              </div>
              <div class="col-md-10">
                <h4 class="intro">
                  <?php echo isset($clientdata->CompanyName) ? $clientdata->CompanyName : "No title"; ?>
                </h4>
                <?php 
                $descOfferOverview = isset( $clientdata->OfferOverview ) ? $clientdata->OfferOverview : "No Description";
                $descOfferOverview = preg_replace( "/<img[^>]+\>/i", "", $descOfferOverview );
                echo AppUtility::excerptText( $descOfferOverview, 858, '  <a href="'.View::url( 'project/view/'.$clientdata->ClientProfileID).'" class="">...Read More</a>' ); ?>
              </div>
            </div>
            <div class="clear"></div>
            <div class="border border-green">
              <h5>Calculator</h5>
              <div id="calculator"><span><?php echo isset($clientdata->MinimumBid) ? floor($clientdata->MinimumBid / $clientdata->Price) : "0"; ?><br><small>Shares</small></span> <span>@</span> <span><?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?><br><small>Per Share</small></span> <span>=</span> <span><?php echo isset($clientdata->MinimumBid) ? $clientdata->MinimumBid : "0"; ?><br><small>Total Value</small></span></div>
             </div>
          </div>
        </div>
        
        <div class="row" style="display: none;">
          <div class="col-sm-12 col-xs-12 col-md-6">
            <div class="embed-responsive embed-responsive-16by9">
              <?php echo isset($clientdata->Video) ? $clientdata->Video : ""; ?>
            </div>
            <p class="small video-caption">
              <?php 
              $description = isset( $clientdata->VideoDescription ) ? $clientdata->VideoDescription : "No Description";
              $description = preg_replace( "/<img[^>]+\>/i", "", $description );
              echo $description; ?>  
            </p>
          </div>
          <div class="col-sm-6 col-xs-12 offer-details">
            <div class="row">
              <div class="col-sm-12 offer-type offer-data">
                <?php echo isset($clientdata->TypeOfOffer) ? ucwords($clientdata->TypeOfOffer) : "-"; ?><small>Type of Offer</small>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 offer-open offer-data">
                <?php echo ($clientdata->OfferOpening != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferOpening)) : "-"; ?><small>Offer Open</small>
              </div>
              <div class="col-xs-6 offer-close offer-data">
                <?php echo ($clientdata->OfferClosing != "0000-00-00") ? date('j M Y', strtotime($clientdata->OfferClosing)) : "-"; ?><small>Offer Closing</small>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-sm-6 offer-size offer-data">
                $<?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?><small>Price</small>
              </div>
              <div class="col-xs-6 col-sm-6 offer-size offer-data">
                $<?php echo isset($clientdata->MinimumBid) ? number_format($clientdata->MinimumBid) : "0"; ?><small>Minimum Bid</small>
              </div>
            </div>
            <?php /*
            <div class="row">
              <div class="col-sm-12 offer-price offer-data">
                $<?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?><small>Price</small>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-6 col-sm-6 offer-size offer-data">
                <?php echo isset($clientdata->SizeOfOffer) ? $clientdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
              </div>
              <div class="col-xs-6 col-sm-6 offer-size offer-data">
                $<?php echo isset($clientdata->MinimumBid) ? number_format($clientdata->MinimumBid) : "0"; ?><small>Minimum Bid</small>
              </div>
            </div>
            */ ?>
            <div class="row">
              <div class="col-sm-12 offer-lead offer-data">
                <?php echo isset($clientdata->LeadManager) ? ucwords($clientdata->LeadManager) : "-"; ?><small>Lead Manager</small>
              </div>
            </div>
          </div>
        </div>
      </div>

      <br><hr><br>

      <div class="row">
         <div class="col-lg-12">
            <form class="form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
            <input type="hidden" name="action" value="doregister">
            <input type="hidden" name="book[ClientProfileID]" value="<?php echo isset($clientdata->ClientProfileID) ? $clientdata->ClientProfileID : ""; ?>">
            <input type="hidden" name="book[BookingType]" value="Interest">
            <input type="hidden" name="book[ExemptedCommission]" value="<?php echo $exemption; ?>">

               <div id="basic-data" class="form-wizard active">
                  <div class="form-group">
                     <div class="form-left">
                      <label for="">I/We apply for Ordinary Shares at <span>$<?php echo isset($clientdata->Price) ? number_format($clientdata->Price, 2) : "0.00"; ?></span> per Share</label>
                        <input type="hidden" id="shareprice" value="<?php echo isset($clientdata->Price) ? $clientdata->Price : "0.00"; ?>">
                        <input type="hidden" name="minimumbid" id="minimumbid" value="<?php echo isset($clientdata->MinimumBid) ? $clientdata->MinimumBid : "0"; ?>">
                        <input type="hidden" name="maximumbid" id="maximumbid" value="<?php echo isset($clientdata->MaximumBid) ? $clientdata->MaximumBid : "0"; ?>">
                         <input type="hidden" name="minimumShare" id="minimumShare" value="<?php echo isset($clientdata->MinimumBid) ? floor($clientdata->MinimumBid / $clientdata->Price) : "0"; ?>">
                         <input type="hidden" name="maxShare" id="maxShare" value="<?php echo isset($clientdata->MaximumBid) ? floor($clientdata->MaximumBid / $clientdata->Price) : "0"; ?>">
                        <div class="input-group">
                          <input type="text" id="SharesToApply" value="<?php echo isset($clientdata->MinimumBid) ? floor($clientdata->MinimumBid / $clientdata->Price) : "0"; ?>" class="form-control" name="book[SharesToApply]" placeholder="0" required="required">
                          <span class="input-group-addon">Shares</span>
                        </div>
                     </div>
                     <div class="form-right">
                      <label for="">Total application monies of <span style="font-weight: normal">(Minimum Amount: USD <?php echo isset($clientdata->MinimumBid) ? number_format($clientdata->MinimumBid) : "0"; ?>)</span></label>
                        <div class="input-group">
                          <span class="input-group-addon">USD</span>
                          <input type="number" id="TotalAmountAttached" value="<?php echo isset($clientdata->MinimumBid) ? $clientdata->MinimumBid : "0"; ?>" class="form-control" name="book[TotalAmountAttached]" placeholder="0.00">
                        </div>
                     </div>
                     <div class="clear"></div>
                  </div>
                  
                  <div class="block-header-2">Personal Details</div>
                  <div class="form-group">
                    <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                    <input type="text" value="<?php echo $fullname; ?>" class="form-control" name="book[Name]" placeholder="">
                  </div>

                  <div class="block-header-2">Postal Address</div>

                  <div class="form-group">
                     <div class="form-left">
                      <label for="">Address</label>
                      <input type="text" value="<?php echo User::info('Address'); ?>" class="form-control" name="book[Address]" placeholder="">
                     </div>
                     <div class="form-right">
                      <label for="">Suburb</label>                    
                      <input type="text" value="<?php echo User::info('City'); ?>" class="form-control" name="book[Suburb]" placeholder="">
                     </div>
                     <div class="clear"></div>
                  </div>

                  <div class="form-group">
                     <div class="form-left">
                        <label for="">State</label>
                        <input type="text" value="<?php echo User::info('State'); ?>" class="form-control" name="book[State]" placeholder="">
                     </div>
                     <div class="form-right">
                      <label for="">Postcode</label>                   
                      <input type="text" value="<?php echo User::info('PostalCode'); ?>" class="form-control" name="book[PostCode]" placeholder="">
                     </div>
                     <div class="clear"></div>
                  </div>

                  <div class="block-header-2">Contact Details</div>

                  <div class="form-group">
                     <div class="form-left">
                        <label for="">Telephone Number</label>
                        <input type="text" value="<?php echo User::info('Phone'); ?>" class="form-control" name="book[Phone]" placeholder="">
                     </div>
                     <div class="form-right">
                      <label for="">Email Address</label>                   
                      <input type="email" value="<?php echo User::info('Email'); ?>" class="form-control" name="book[Email]" placeholder="" required>
                     </div>
                     <div class="clear"></div>
                  </div>

                  <!-- <hr><br>
                  <b>Tax File Number(s) or Exemption(s)* </b>

                  <div class="form-group">
                     <div class="form-left">
                        <label for="">Holder 1</label>
                        <input type="text" value="" class="form-control" name="book[TaxHolder1]" placeholder="">
                     </div>
                     <div class="form-right">
                      <label for="">Holder 2</label>                   
                      <input type="text" value="" class="form-control" name="book[TaxHolder2]" placeholder="">
                     </div>
                     <div class="clear"></div>
                     <p class="text-left"><small class="text-muted">*Collection of Australian Tax File Numbers is authorised by taxation laws. Quotation of your TFN is not compulsory and will not affect your Application. Non-residents – please indicate County of residence.</small></p>
                  </div> -->



                  <div class="">
                    <p class="text-muted text-left" style="text-align: left;"><b>IMPORTANT NOTICE AND ACKNOWLEDGEMENTS</b> - I/We, by lodging this form, hereby acknowledge that we qualify as sophisticated investors for the purposes of s.708 of the Corporations Act 2001 (Cth), or are otherwise person(s) to whom offers of Shares may be made without a Prospectus under the Corporations Act. I/We understand that there are inherent risks associated with an investment in Global Asset Resources Ltd (‘Company’) and therefore apply for the Shares in the knowledge and acceptance of such risks. I/We acknowledge that I/We have had an opportunity to obtain a copy of the Constitution of the Company and obtain independent professional advice in relation to this investment. I/We acknowledge that the issue of Shares is at the discretion of the Board of the Company. I/We hereby authorise the Directors of the Company to register me/us as the holder(s) of the Shares issued to me/us. I/We acknowledge that the Shares or part thereof may be classified as restricted securities by the ASX and subject to escrow conditions upon the Company undertaking a listing on the ASX. I/We undertake and agree to sign a standard form ASX Restriction Agreement to acknowledge the restrictions imposed on such Shares as determined by the ASX. </p>
                     <div class="clear"></div>
                  </div>

                  <div class="form-group text-center">
                    <label class="css-input css-checkbox css-checkbox-info">
                        <input type="checkbox" value="" required="required"> <span></span> I/We Read and Understood the above
                    </label>
                    <br>
                     <button type="submit" class="btn btn-rounded btn-primary">Register Interest</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
  $( document ).ready( function(){
  });
</script>