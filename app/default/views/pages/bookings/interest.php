<?php
  View::$bodyclass = '';
  View::header(); 
?>
<section class="header-bottom">
    <article>
        <div class="container">
            <h1>BOOKING INTEREST SUBMITTED</h1>
            <p>Your booking interest transaction has been submitted. Our CRS / Hub Care is escalating the transaction to <?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : ""; ?> <?php echo isset($bookdata->LeadManager) ? '(c/o '.$bookdata->LeadManager.')' : ""; ?> to ensure that the booking insterest has been notified.</p>
        </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">

    <form class="form-ui input_mask" enctype="multipart/form-data" method="post" style="padding: 0;">
    <?php if($bookdata->BookingStatus != 'Approved' || User::info('UserLevel') == 'Administrator'){ ?>
    <input type="hidden" name="action" value="updatebooking">
    <?php } ?>
    <input type="hidden" name="bookid" value="<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : ""; ?>">
    <input type="hidden" name="usrid" value="<?php echo isset($bookdata->UserID) ? $bookdata->UserID : ""; ?>">

    <div class="row about-project">
      <!-- ************************ Left Side Area ************************ -->
      
      <div class="col-md-8">
          <div class="block">
              <div class="row">
                  <div class="col-lg-12">

                      <div class="booking-summary">
                          <div class="block-header">
                              <div class="block-title">INTEREST INFORMATION</div>
                          </div>
                      </div>

                  </div>
              </div>

              <!-- Booking Summary  -->
              <div class="row">
                  <div class="col-lg-12">

                      <div class="block-content">

                          <?php echo View::getMessage(); ?>
                          <div class="booking-id">
                            <span>Booking ID: BK-533D0M364-<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : '-' ?>
                            </span>
                          </div>

                          <div class="form-group">
                             <div class="form-left">
                              <label for="">Total Shares at <span>$<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?></span> per Share</label>
                                <input type="hidden" id="shareprice" value="<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?>">
                                <input type="hidden" id="minimumbid" value="<?php echo isset($bookdata->MinimumBid) ? $bookdata->MinimumBid : "0"; ?>">
                                <div class="input-group">
                                  <input type="text" value="<?php echo isset($bookdata->SharesToApply) ? number_format($bookdata->SharesToApply) : "0"; ?>" class="form-control" placeholder="0" required="required" readonly="readonly">
                                  <span class="input-group-addon">Shares</span>
                                </div>
                             </div>
                             <div class="form-right">
                              <label for="">Total application monies of</label>
                                <div class="input-group">
                                  <span class="input-group-addon">USD</span>
                                  <input type="text" value="<?php echo isset($bookdata->TotalAmountAttached) ? number_format($bookdata->TotalAmountAttached,2) : "0"; ?>" class="form-control" placeholder="0.00" readonly="readonly">
                                </div>
                             </div>
                             <div class="clear"></div>
                          </div>

                          <div class="form-group">
                             <div class="form-left">
                              <label for="">Company Name</label>
                              <input type="text" value="<?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : ""; ?>" class="form-control" placeholder="" readonly="readonly">
                             </div>
                             <div class="form-right">
                              <label for="">Lead Manager</label>
                              <input type="text" value="<?php echo isset($bookdata->LeadManager) ? $bookdata->LeadManager : ""; ?>" class="form-control" placeholder="" readonly="readonly">
                            </div>
                             <div class="clear"></div>
                          </div>

                      </div>

                  </div>
              </div>
              <!-- END Booking Summary  -->

              <!-- Personal Details -->
              <div class="row">
                  <div class="col-lg-12">

                      <div class="block-content">

                          <div class="block-header-2 pd">Personal Details</div>
                          <div class="form-group">
                            <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                            <input type="text" value="<?php echo isset($bookdata->Name) ? $bookdata->Name : ""; ?>" class="form-control" name="book[Name]" placeholder="" readonly="readonly">
                          </div>

                      </div>

                  </div>
              </div>
              <!-- END Personal Details -->

              <!-- Postal Address -->
              <div class="row">
                  <div class="col-lg-12">

                      <div class="block-content">
                          <div class="block-header-2">Postal Address</div>
                          <div class="form-group">
                           <div class="form-left">
                            <label for="">Address</label>
                            <input type="text" value="<?php echo isset($bookdata->Address) ? $bookdata->Address : ""; ?>" class="form-control" name="book[Address]" placeholder="" readonly="readonly">
                           </div>
                           <div class="form-right">
                            <label for="">Suburb</label>                    
                            <input type="text" value="<?php echo isset($bookdata->Suburb) ? $bookdata->Suburb : ""; ?>" class="form-control" name="book[Suburb]" placeholder="" readonly="readonly">
                           </div>
                           <div class="clear"></div>
                        </div>

                        <div class="form-group">
                           <div class="form-left">
                              <label for="">State</label>
                              <input type="text" value="<?php echo isset($bookdata->State) ? $bookdata->State : ""; ?>" class="form-control" name="book[State]" placeholder="" readonly="readonly">
                           </div>
                           <div class="form-right">
                            <label for="">Postcode</label>                   
                            <input type="text" value="<?php echo isset($bookdata->PostCode) ? $bookdata->PostCode : ""; ?>" class="form-control" name="book[PostCode]" placeholder="" readonly="readonly">
                           </div>
                           <div class="clear"></div>
                        </div>

                      </div>

                  </div>
              </div>
              <!-- END Postal Address -->

              <!-- Contact Details -->
              <div class="row">
                  <div class="col-lg-12">

                      <div class="block-content">

                          <div class="block-header-2">Contact Details</div>
                          <div class="form-group">
                             <div class="form-left">
                                <label for="">Telephone Number</label>
                                <input type="text" value="<?php echo isset($bookdata->Phone) ? $bookdata->Phone : ""; ?>" class="form-control" name="book[Phone]" placeholder="" readonly="readonly">
                             </div>
                             <div class="form-right">
                              <label for="">Email Address</label>                   
                              <input type="email" value="<?php echo isset($bookdata->Email) ? $bookdata->Email : ""; ?>" class="form-control" name="book[Email]" placeholder="" readonly="readonly">
                             </div>
                             <div class="clear"></div>
                          </div>

                      </div>

                  </div>
              </div>
              <!-- END Contact Details -->
          </div>
      </div> 
      
      <!-- ************************ Right Side Area ************************ -->
      <div class="col-lg-4">
        <div class="sidebar">

          <?php if(User::info('UserLevel') == "Administrator" || User::can('Verify Booking')){ ?>
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="w-title">Change Status</div>
            <div class="pcontent">
              <select name="book[BookingStatus]" class="form-control">
                <option value="Pending" <?php echo ($bookdata->BookingStatus == 'Pending') ? "selected" : ""; ?>>Pending</option>
                <option value="Verified" <?php echo ($bookdata->BookingStatus == 'Verified') ? "selected" : ""; ?>>Verified</option>
                <?php if(User::info('UserLevel') == "Administrator"){ ?>
                <option value="Approved" <?php echo ($bookdata->BookingStatus == 'Approved') ? "selected" : ""; ?>>Approved</option>
                <?php } ?>
              </select>

              <div class="text-center push-10-t">
                <button type="submit" class="btn btn-rounded btn-primary">Save Changes</button>
              </div>

            </div>
          </div>
          <?php } ?>

            <?php if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' ): ?>
            <div class="sidebar-item">
                <div class="w-title">What Can I Do Next?</div>
                <div class="w-content">
                    <div class="popular-posts-widget viewbookhelp userhelp">
                      <div class="side-nav-wcidn">
                          <span class="num">1</span>
                          <div class="num-content">
                              <h4 class="wcidn-title"><a>View Company</a></h4>
                              <p class="wcidn-text">Learn more about the company, including the company profile, company fact sheet, corporate responsibility, investment information, and more.</p>
                              <a href="<?php echo View::url('project/view/'.$bookdata->ClientProfileID); ?>" class="btn btn-xs btn-default btn-rounded ">View Info</a>
                          </div>
                      </div>
                      <div class="side-nav-wcidn">
                          <span class="num">2</span>
                          <div class="num-content">
                              <h4 class="wcidn-title"><a>Contact Support Team</a></h4>
                              <p class="wcidn-text">For more information and inquiries regarding your transaction please feel free to contact our support.</p>
                              <a class="btn btn-xs btn-default btn-rounded " href="<?php echo View::url('support/dashboard/'); ?>">Contact Support</a>
                          </div>
                      </div>
                    
                    </div>
                </div>
            </div>
            <?php endif; ?>

          <!-- Project Progress -->

      </div> 
             
    </div>
    </form>
  </article>
</section>

<?php View::footer(); ?>

<script>
setTimeout(function () { window.location.reload(); }, 5*60*1000);
// just show current time stamp to see time of last refresh.
document.write(new Date());
</script>