<?php
  View::$bodyclass = '';
  View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <?php echo $breadcrumb; ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">

    <div class="client-company text-center">
      <h3>CONGRATULATIONS</h3>
      <p>&nbsp;<br>Thank you for sharing with us, we assure you to give a high valued information and updates with regards to your investments.<br>We value your trust and we will succeed together with you.<br>For more updates kindly check the right hand sidebar for the things you can do next.</p>
      <?php /*
      <hr><br>
      <div class="status-wizzard">
        <div class="well">
          <div class="title">
            <?php 
            switch ($bookdata->BookingStatus) {
              case 'Pending':
                $link1 = "done";
                $link2 = "";
                $link3 = "";
                break;
              case 'Verified':
                $link1 = "done";
                $link2 = "done";
                $link3 = "";
                break;
              case 'Approved':
                $link1 = "done";
                $link2 = "done";
                $link3 = "done";
                break;
            }
            ?>            
            <ul>
              <li class="<?php echo $link1; ?>"><a href="javascript:void();"><i class="si si-clock"></i><span>Pending</span></a></li>
              <li class="<?php echo $link2; ?>"><a href="javascript:void();"><i class="si si-user-following"></i><span>Verified</span></a></li>
              <li class="<?php echo $link3; ?>"><a href="javascript:void();"><i class="si si-check"></i><span>Approved</span></a></li> 
            </ul>
          </div>
        </div>
      </div>
      */ ?>
    </div><br>

    <form class="form-ui input_mask" enctype="multipart/form-data" method="post" style="padding: 0;">
    <?php if($bookdata->BookingStatus != 'Approved' || User::info('UserLevel') == 'Administrator'){ ?>
    <input type="hidden" name="action" value="updatebooking">
    <?php } ?>
    <input type="hidden" name="bookid" value="<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : ""; ?>">
    <input type="hidden" name="usrid" value="<?php echo isset($bookdata->UserID) ? $bookdata->UserID : ""; ?>">

    <div class="row about-project">
      <!-- ************************ Left Side Area ************************ -->
      <div class="col-lg-8">
        
        <div class="pcontent">
          <?php echo View::getMessage(); ?>
             <div id="basic-data" class="form-wizard active">

              <?php if($bookdata->BookingType != 'Interest'){ ?>

              <?php
              $notice = '';             

              if($bookdata->DaysLeft == 0) {
                  $notice = 'Booking will expire today';
              } else {
                  $notice = 'Booking will expire in ';
                  $notice .= ($bookdata->DaysLeft > 1) ? $bookdata->DaysLeft .' days' : $bookdata->DaysLeft .' day'; 
              }
              $notice .= ' if remain unattended.';
              ?>

                <div class="push-10"><b>INVESTMENT INFORMATION </b> <a class="btn btn-xs btn-default" href="<?php echo View::url('bookings/goprint/'.$bookdata->InvestmentBookingID); ?>" target="_blank"><i class="si si-printer"></i> Print Form</a> <b class="pull-right text-success">Booking ID: <code>BK-533D0M364-<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : '-' ?></code></b></div>
                <?php if($bookdata->ReceiptOption == 'TTPhoto'){ ?>
                  <?php if($bookdata->TTReceiptPhoto == 0){ ?>
                    <div class="alert alert-danger push-10-t fade in">
                      Please Upload The TT Receipt Photo! <?php echo $notice; ?><br><br><a class="push-10-l text-danger" href="<?php echo View::url('bookings/downloadbookingdemandnote/'.$bookdata->InvestmentBookingID) ?>" style="margin: 0 !important;"><i class="fa fa-cloud-download"></i> <b>Download Demand Note!</b></a></div>
                  <?php } ?>                  
                <?php } else { ?>
                  <?php if($bookdata->ChequeDrawer == '' || $bookdata->ChequeBank == '' || $bookdata->ChequeBranch == '' || $bookdata->ChequeAmount == 0){ ?>
                    <div class="alert alert-danger push-10-t fade in">
                      Please complete your cheque details! <?php echo $notice; ?>
                    </div>
                  <?php } ?>
                <?php } ?>

                <div class="form-group">
                  <?php 
                  $disable = ($bookdata->BookingStatus == 'Approved') ? 'disabled="disabled"' : '';
                   ?>
                  <label class="css-input css-radio css-radio-info push-10-r">
                    <input type="radio" class="receiptopt" name="book[ReceiptOption]" value="TTPhoto" <?php echo ($bookdata->ReceiptOption == 'TTPhoto') ? 'checked=""' : ''; ?> <?php echo $disable; ?>><span></span> TT Receipt Photo
                  </label>
                  <label class="css-input css-radio css-radio-info">
                    <input type="radio" class="receiptopt" name="book[ReceiptOption]" value="Cheque" <?php echo ($bookdata->ReceiptOption == 'Cheque') ? 'checked=""' : ''; ?> <?php echo $disable; ?>><span></span> Cheque Details 
                  </label>
                </div>

                <div class="ttreceipt-opt" style="display: none;">
                  <?php echo AppUtility::getBookedTTReceipt($bookdata); ?>

                  <?php 
                  $ttphoto = View::common()->getUploadedFiles($bookdata->TTReceiptPhoto);

                  if($bookdata->TTReceiptPhoto == 0 || $ttphoto[0]->Active == 0){ ?>
                    <label class="push-10-t">TT Receipt Photo <small class="text-muted">(Transaction Receipt)</small></label>
                    <div class="form-group">                  
                      <input type="file" value="" class="form-control file" name="TTReceiptPhoto" placeholder="" data-min-file-count="0" name="Avatar" data-show-upload="true" data-allowed-file-extensions='["jpeg","png","jpg","pdf"]' >
                      <small class="text-danger">If you are using check to send funds, please upload the scanned check and supply the fields below</small>
                    </div>
                  <?php } ?>
                </div>

                <div class="cheque-opt" style="display: none;">
                  <div class="text-divider font-w600 push-10-t text-left"><span>Cheque details </span></div>

                  <?php if($bookdata->BookingStatus == 'Approved'){ ?>
                    <table class="table  ">
                      <tbody>
                        <tr>
                          <td class="font-w600" style="width: 30%;">Drawer</td>
                          <td><?php echo isset($bookdata->ChequeDrawer) ? $bookdata->ChequeDrawer : "-"; ?></td>
                        </tr>
                        <tr>
                          <td class="font-w600">Bank</td>
                          <td><?php echo isset($bookdata->ChequeBank) ? $bookdata->ChequeBank : "-"; ?></td>
                        </tr>
                        <tr>
                          <td class="font-w600">Branch</td>
                          <td><?php echo isset($bookdata->ChequeBranch) ? $bookdata->ChequeBranch : "-"; ?></td>
                        </tr>
                        <tr>
                          <td class="font-w600">Amount</td>
                          <td><?php echo isset($bookdata->ChequeAmount) ? number_format($bookdata->ChequeAmount,2) : "-"; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  <?php }else{ ?>

                    <div class="form-group">
                       <div class="form-left">
                          <label for="">Drawer</label>
                          <input type="text" value="<?php echo isset($bookdata->ChequeDrawer) ? $bookdata->ChequeDrawer : ""; ?>" class="form-control" name="book[ChequeDrawer]" placeholder="">
                       </div>
                       <div class="form-right">
                        <label for="">Bank</label>                   
                        <input type="text" value="<?php echo isset($bookdata->ChequeBank) ? $bookdata->ChequeBank : ""; ?>" class="form-control" name="book[ChequeBank]" placeholder="">
                       </div>
                       <div class="clear"></div>
                    </div>

                    <div class="form-group">
                       <div class="form-left">
                          <label for="">Branch</label>
                          <input type="text" value="<?php echo isset($bookdata->ChequeBranch) ? $bookdata->ChequeBranch : ""; ?>" class="form-control" name="book[ChequeBranch]" placeholder="">
                       </div>
                       <div class="form-right">
                        <label for="">Amount</label>
                        <input type="text" value="<?php echo isset($bookdata->ChequeAmount) ? $bookdata->ChequeAmount : ""; ?>" class="form-control" name="book[ChequeAmount]" placeholder="">
                       </div>
                       <div class="clear"></div>
                    </div>

                    <div class="form-group text-center">
                       <button type="submit" class="btn btn-4 blue default">Update</button>
                    </div>

                  <?php } ?>

                </div>

              <?php } // End of Not Interest condition ?>

                <div class="form-group">
                   <div class="form-left">
                    <label for="">Total Shares at <span>$<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?></span> per Share</label>
                      <input type="hidden" id="shareprice" value="<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?>">
                      <input type="hidden" id="minimumbid" value="<?php echo isset($bookdata->MinimumBid) ? $bookdata->MinimumBid : "0"; ?>">
                      <div class="input-group">
                        <input type="text" value="<?php echo isset($bookdata->SharesToApply) ? number_format($bookdata->SharesToApply) : "0"; ?>" class="form-control" placeholder="0" required="required" readonly="readonly">
                        <span class="input-group-addon">Shares</span>
                      </div>
                   </div>
                   <div class="form-right">
                    <label for="">Total application monies of</label>
                      <div class="input-group">
                        <span class="input-group-addon">USD</span>
                        <input type="text" value="<?php echo isset($bookdata->TotalAmountAttached) ? number_format($bookdata->TotalAmountAttached,2) : "0"; ?>" class="form-control" placeholder="0.00" readonly="readonly">
                      </div>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="text-divider font-w600 push-40-t text-left"><span>PERSONAL DETAILS</span></div>
                <div class="form-group">
                  <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                  <input type="text" value="<?php echo isset($bookdata->Name) ? $bookdata->Name : ""; ?>" class="form-control" name="book[Name]" placeholder="" readonly="readonly">
                </div>
                
                <div class="text-divider font-w600 push-40-t text-left"><span>POSTAL ADDRESS </span></div>
                  <div class="form-group">
                     <div class="form-left">
                      <label for="">Address</label>
                      <input type="text" value="<?php echo isset($bookdata->Address) ? $bookdata->Address : ""; ?>" class="form-control" name="book[Address]" placeholder="" readonly="readonly">
                     </div>
                     <div class="form-right">
                      <label for="">Suburb</label>                    
                      <input type="text" value="<?php echo isset($bookdata->Suburb) ? $bookdata->Suburb : ""; ?>" class="form-control" name="book[Suburb]" placeholder="" readonly="readonly">
                     </div>
                     <div class="clear"></div>
                  </div>

                  <div class="form-group">
                     <div class="form-left">
                        <label for="">State</label>
                        <input type="text" value="<?php echo isset($bookdata->State) ? $bookdata->State : ""; ?>" class="form-control" name="book[State]" placeholder="" readonly="readonly">
                     </div>
                     <div class="form-right">
                      <label for="">Postcode</label>                   
                      <input type="text" value="<?php echo isset($bookdata->PostCode) ? $bookdata->PostCode : ""; ?>" class="form-control" name="book[PostCode]" placeholder="" readonly="readonly">
                     </div>
                     <div class="clear"></div>
                  </div>

                  <div class="text-divider font-w600 push-40-t text-left"><span>CONTACT DETAILS </span></div>       

                  <div class="form-group">
                     <div class="form-left">
                        <label for="">Telephone Number</label>
                        <input type="text" value="<?php echo isset($bookdata->Phone) ? $bookdata->Phone : ""; ?>" class="form-control" name="book[Phone]" placeholder="" readonly="readonly">
                     </div>
                     <div class="form-right">
                      <label for="">Email Address</label>                   
                      <input type="email" value="<?php echo isset($bookdata->Email) ? $bookdata->Email : ""; ?>" class="form-control" name="book[Email]" placeholder="" readonly="readonly">
                     </div>
                     <div class="clear"></div>
                  </div>

                    
                  <?php /*
                  <div class="text-divider font-w600 push-40-t text-left"><span>Tax File Number(s) or Exemption(s)* </span></div>
                  <div class="form-group">
                     <div class="form-left">
                        <label for="">Holder 1</label>
                        <input type="text" value="<?php echo isset($bookdata->TaxHolder1) ? $bookdata->TaxHolder1 : ""; ?>" class="form-control" name="book[TaxHolder1]" placeholder="" readonly="readonly">
                     </div>
                     <div class="form-right">
                      <label for="">Holder 2</label>                   
                      <input type="text" value="<?php echo isset($bookdata->TaxHolder2) ? $bookdata->TaxHolder2 : ""; ?>" class="form-control" name="book[TaxHolder2]" placeholder="" readonly="readonly">
                     </div>
                     <div class="clear"></div>
                     <p class="text-left"><small class="text-muted">*Collection of Australian Tax File Numbers is authorised by taxation laws. Quotation of your TFN is not compulsory and will not affect your Application. Non-residents – please indicate County of residence.</small></p>
                  </div>  
                  */ ?>              

             </div>
          
        </div>
      </div>
      
      <!-- ************************ Right Side Area ************************ -->
      <div class="col-lg-4">
        <div class="sidebar">

          <?php if(User::info('UserLevel') == "Administrator" || User::can('Verify Booking')){ ?>
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="w-title">Change Status</div>
            <div class="pcontent">
              <select name="book[BookingStatus]" class="form-control">
                <option value="Pending" <?php echo ($bookdata->BookingStatus == 'Pending') ? "selected" : ""; ?>>Pending</option>
                <option value="Verified" <?php echo ($bookdata->BookingStatus == 'Verified') ? "selected" : ""; ?>>Verified</option>
                <?php if(User::info('UserLevel') == "Administrator"){ ?>
                <option value="Approved" <?php echo ($bookdata->BookingStatus == 'Approved') ? "selected" : ""; ?>>Approved</option>
                <?php } ?>
              </select>

              <div class="text-center push-10-t">
                <button type="submit" class="btn btn-success default btn-block">Save Changes</button>
              </div>

            </div>
          </div>
          <?php } ?>

            <?php if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' ): ?>
            <div class="sidebar-item">
                <div class="w-title">What Can I Do Next?</div>
                <div class="w-content">
                    <div class="popular-posts-widget viewbookhelp">
                        <div class="side-nav-wcidn">
                            <span class="num">1</span>
                            <div class="num-content">
                                <h4 class="wcidn-title">Join the Community Forums</h4>
                                <p class="wcidn-text">Share something, connect with others! The community forums are where the SeedOmega community actively answers questions just like yours. It’s a dynamic and secure place to connect with other investors and clients.</p>
                                <a class="btn btn-xs btn-default" href="<?php echo View::url('forums/dashboard/'.$bookdata->ClientProfileID); ?>" target="_blank">Start Now</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">2</span>
                            <div class="num-content">
                                <h4 class="wcidn-title">Leave a Comment</h4>
                                <p class="wcidn-text">Have a question, need an answer, or just want to join in on discussions? Click the chat button below. Visit our FAQ section to find the answers for the most common questions asked by our clients and investors.</p>
                                <a class="btn btn-xs btn-default" href="javascript:void(0);" target="_blank">Chat Now</a>&nbsp;<a class="btn btn-xs btn-default">FAQ</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">3</span>
                            <div class="num-content">
                                <h4 class="wcidn-title">Company News</h4>
                                <p class="wcidn-text">Stay up to date with the most recent company news, company results, industry news, corporate financials and much more!</p>
                                <a class="btn btn-xs btn-default ">Blogs</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">4</span>
                            <div class="num-content">
                                <h4 class="wcidn-title"><a>View Company</a></h4>
                                <p class="wcidn-text">Learn more about the company, including the company profile, company fact sheet, corporate responsibility, investment information, and more.</p>
                                <a href="<?php echo View::url('clients/view/'.$bookdata->ClientProfileID); ?>" class="btn btn-xs btn-default ">View Info</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?php endif; ?>

          <!-- Project Progress -->
          <?php /* ?>
          <div class="sidebar-item">
            <div class="w-title"><?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : "No title"; ?></div>

            <div class="project-progress client-company pcontent">

              <?php 
              $percentage = 0;
              if($bookdata->TargetGoal > 0){
                $percentage = (int)(($bookdata->TotalRaisedAmt / $bookdata->TargetGoal) * 100+.5); ?>
                <div class="popular-data data-single"> 
                  <!-- <img src="<?php echo View::url() ?>/assets/images/funder_1.jpg" alt="Funder" /> -->
                  
                  <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#ef6342" data-barsize="7.1">
                    <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                    <div class="pie_progress__label">Completed</div>
                  </div>
                </div>
              <?php } ?>
              

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->TotalRaisedAmt) ? "$".number_format($bookdata->TotalRaisedAmt) : "0";?><br>
                        <small class="text-muted">Raised</small>
                    </div>                                        
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->TotalInvestor) ? number_format($bookdata->TotalInvestor) : "0";?><br>
                        <small class="text-muted">Investors</small>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset($bookdata->TypeOfOffer) ? $bookdata->TypeOfOffer : "-"; ?><small>Type of Offer</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php 
                      $startdate = date_create($bookdata->OfferOpening);
                      $closedate = date_create($bookdata->OfferClosing);
                      $interval = date_diff($startdate, $closedate); 
                      ?>
                    <?php echo str_replace('+', '', $interval->format('%R%a'));?><small>Days Left</small>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo ($bookdata->OfferOpening != "0000-00-00") ? date('j M Y', strtotime($bookdata->OfferOpening)) : "-"; ?><small>Offer Open</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo ($bookdata->OfferClosing != "0000-00-00") ? date('j M Y', strtotime($bookdata->OfferClosing)) : "-"; ?><small>Offer Closing</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->TargetGoal) ? number_format($bookdata->TargetGoal) : "-"; ?><small>Target Goal</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    <?php echo isset($bookdata->SizeOfOffer) ? $bookdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?><small>Price</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset($bookdata->MinimumBid) ? number_format($bookdata->MinimumBid) : "-"; ?><small>Minimum Investment</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 offer-lead offer-data">
                    <?php echo isset($bookdata->LeadManager) ? ucwords($bookdata->LeadManager) : "-"; ?><small>Lead Manager</small>
                  </div>

                  <div class="col-sm-12 col-xs-12 col-md-12">
                    <div class="embed-responsive embed-responsive-16by9">
                      <?php echo isset($bookdata->Video) ? $bookdata->Video : ""; ?>
                    </div>
                    <p class="small video-caption"><?php echo isset($bookdata->VideoDescription) ? $bookdata->VideoDescription : "No Description"; ?></p>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>
          <?php */ ?>

      </div> 
             
    </div>
    </form>
  </article>
</section>

<?php View::footer(); ?>

<script>
setTimeout(function () { window.location.reload(); }, 5*60*1000);
// just show current time stamp to see time of last refresh.
document.write(new Date());
</script>