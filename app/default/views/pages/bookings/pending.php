<?php
ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );
View::$title = 'Pending Bookings';
View::$bodyclass = '';
View::header();
?>
    <section class="header-bottom">
        <article>
          <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
        </article>
    </section>
    <?php /* View::template('users/breadcrumb'); */ ?>

    <!-- ************************ Page Content ************************ -->
    
    <section class="gray">
        <article class="container projects-page style-2 animated fadeInUp">
            <?php echo View::getMessage(); ?>
            <div class="block">
                
                <div class="tab-content">
                    <!-- Pending -->
                    <div class="tab-pane fade fade-up in active" id="bookings-pending">
                        <table class="table table-divide table-hover table-vcenter table-header-bg js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center no-wrap"><i class="fa fa-suitcase text-gray"></i> Company Name</th>
                                    <th class="text-center"><i class="si si-note text-gray"></i> Notes</th>
                                    <th class="text-center"><i class="fa fa-money text-gray"></i> Investment</th>
                                    <th class="text-center no-wrap"><i class="si si-calendar text-gray"></i> Booking Date</th>
                                    <th class="text-center no-wrap"><i class="si si-close text-gray"></i> Expires In</th>
                                    <th class="text-center" style="width: 100px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( count( $bookings ) ){
                                    foreach( $bookings as $proj ):
                                        if( $proj->BookingStatus == 'Pending' && $proj->BookingType == 'Booking' ){ ?>
                                        <tr>
                                            <td class="">
                                                <h3 class="h5 font-w600 push-10">
                                                    <!--a class="" href="/bookings/info/<?php //echo $proj->InvestmentBookingID;?>"><?php //echo isset($proj->CompanyName) ? $proj->CompanyName : "-"; ?></a -->
                                                    <a class="" href="/project/view/<?php echo $proj->ClientProfileID; ?>"><?php echo isset( $proj->CompanyName ) ? $proj->CompanyName : "-"; ?></a><br>
                                                    <small class="text-muted"><?php echo isset($proj->TypeOfOffer) ? $proj->TypeOfOffer : "-"; ?></small>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <?php echo $statusfield; ?>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php if($proj->ReceiptOption == 'TTPhoto'){ ?>
                                                  <?php echo ($proj->TTReceiptPhoto != 0) ? '<small class="text-info">TT Photo : Awaiting Verification!</small>' : '<small class="text-danger">Please Upload The TT Receipt Photo!</small>'; ?>                 
                                                <?php } else { ?>
                                                  <?php echo ($proj->ChequeDrawer == '' || $proj->ChequeBank == '' || $proj->ChequeBranch == '' || $proj->ChequeAmount == 0) ? '<small class="text-danger">Please complete your cheque details!</small>' : '<small class="text-danger">Checks Details : Awaiting Verification!</small>'; ?>
                                                <?php } ?>
                                            </td>
                                            <td class="text-right text-primary" data-order="<?php echo isset( $proj->TotalAmountAttached ) ? $proj->TotalAmountAttached : "0"; ?>">
                                                <?php echo isset( $proj->TotalAmountAttached ) ? "$".number_format( $proj->TotalAmountAttached, 2 ) : "-"; ?>
                                            </td>
                                            <td class="text-center" data-order="<?php echo $proj->CreatedAt != "0000-00-00" ? date( 'Ymd', strtotime( $proj->CreatedAt ) ) : "00000000"; ?>">
                                                <?php echo $proj->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $proj->CreatedAt ) ) : "-"; ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                if( $proj->TTReceiptPhoto == 0 ){
                                                    if( $proj->DaysLeft == 0 ){
                                                        echo 'Today';
                                                    } else {
                                                        echo ($proj->DaysLeft > 1) ? $proj->DaysLeft .' days' : $proj->DaysLeft .' day'; 
                                                    }
                                                } else echo 'NA';
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="/bookings/info/<?php echo $proj->InvestmentBookingID;?>" class="btn btn-rounded btn-default btn-sm" type="button"><?php echo ($proj->ReceiptOption == 'TTPhoto') ? 'Upload File' : 'Complete Info'; ?>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                                                         
                            </tbody>
                        </table>
                    </div>
                    <!-- END Pending -->
                </div>
            </div>
            

        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script>