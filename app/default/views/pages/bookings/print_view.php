<?php
View::$title = 'Booking';
View::$bodyclass = '';
View::header( false,'header_print' );
?>

<div class="content content-boxed">

    <div class="col-lg-8">
        <?php if ($bookdata->ExemptedCommission == 1) : ?>
           <?php echo View::image('/img-promo-print.png', 'Waived','pull-right','');?>
        <?php endif; ?>

        <br/><br/> <br/><br/>

        <div class="pcontent">

            <div id="basic-data" class="form-wizard active">

                <?php if($bookdata->BookingType != 'Interest'){ ?>

                    <?php
                    $notice = '';

                    if($bookdata->DaysLeft == 0) {
                        $notice = 'Booking will expire today';
                    } else {
                        $notice = 'Booking will expire in ';
                        $notice .= ($bookdata->DaysLeft > 1) ? $bookdata->DaysLeft .' days' : $bookdata->DaysLeft .' day';
                    }
                    $notice .= ' if remain unattended.';
                    ?>

                    <div class="row items-push-2x">
                        <div class="col-xs-6 col-sm-4 col-lg-3">
                            <?php echo View::photo('images/omegalogo.png');?>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-sm-offset-4 col-lg-3 col-lg-offset-5 text-right">
                            <?php $companyLogo = View::common()->getUploadedFiles( $bookdata->CompanyLogo ); ?>
                            <?php echo View::photo( ( isset( $companyLogo[0] ) ? 'files'.$companyLogo[0]->FileSlug : '/images/user.png'), $bookdata->CompanyName, "img-avatar" ); ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="push-10"><b>INVESTMENT INFORMATION </b> <b class="pull-right text-success">Booking ID: <code>BK-533D0M364-<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : '-' ?></code></b></div>



                <?php } // End of Not Interest condition ?>

                <div class="form-group">
                    <div class="form-left">
                        <label for="">Acknowledgment Date:</label>
                        <?php echo isset($bookdata->AcknowledgeDate) ? App::date($bookdata->AcknowledgeDate,'d/m/Y') : "0"; ?>
                    </div>

                    <div class="clear"></div>
                </div>

                <div class="form-group">
                    <div class="form-left">
                        <label for="">Total Shares at <span>$<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?></span> per Share: </label>
                        <?php echo isset($bookdata->SharesToApply) ? number_format($bookdata->SharesToApply) : "0"; ?>
                    </div>
                    <div class="form-right">
                        <label for="">Total application monies of: </label>
                        USD <?php echo isset($bookdata->TotalAmountAttached) ? number_format($bookdata->TotalAmountAttached,2) : "0"; ?>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="form-group">
                    <div class="form-left">
                        <label for="">Company Name: </label>
                        <?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : ""; ?>
                    </div>
                    <div class="form-right">
                        <label for="">Lead Manager </label>
                        <?php echo isset($bookdata->LeadManager) ? $bookdata->LeadManager : ""; ?>

                    </div>
                    <div class="clear"></div>
                </div>

                <div class="text-divider font-w600 push-40-t text-left"><span>PERSONAL DETAILS</span></div>
                <div class="form-group">
                    <label for="">Title, Given Name(s) (no initials) & Surname or Company Name: </label> <br/>
                    <?php echo isset($bookdata->Name) ? $bookdata->Name : ""; ?>
                </div>

                <div class="text-divider font-w600 push-40-t text-left"><span>POSTAL ADDRESS </span></div>
                <div class="form-group">
                    <div class="form-left">
                        <label for="">Address: </label>
                        <?php echo isset($bookdata->Address) ? $bookdata->Address : ""; ?>
                    </div>
                    <div class="form-right">
                        <label for="">Suburb: </label>
                        <?php echo isset($bookdata->Suburb) ? $bookdata->Suburb : ""; ?>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="form-group">
                    <div class="form-left">
                        <label for="">State: </label>
                        <?php echo isset($bookdata->State) ? $bookdata->State : ""; ?>
                    </div>
                    <div class="form-right">
                        <label for="">Postcode: </label>
                        <?php echo isset($bookdata->PostCode) ? $bookdata->PostCode : ""; ?>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="text-divider font-w600 push-40-t text-left"><span>CONTACT DETAILS </span></div>

                <div class="form-group">
                    <div class="form-left">
                        <label for="">Telephone Number: </label>
                        <?php echo isset($bookdata->Phone) ? $bookdata->Phone : ""; ?>
                    </div>
                    <div class="form-right">
                        <label for="">Email Address: </label>
                        <?php echo isset($bookdata->Email) ? $bookdata->Email : ""; ?>
                    </div>
                    <div class="clear"></div>
                </div>


                <?php /*
                  <div class="text-divider font-w600 push-40-t text-left"><span>Tax File Number(s) or Exemption(s)* </span></div>
                  <div class="form-group">
                     <div class="form-left">
                        <label for="">Holder 1</label>
                        <input type="text" value="<?php echo isset($bookdata->TaxHolder1) ? $bookdata->TaxHolder1 : ""; ?>" class="form-control" name="book[TaxHolder1]" placeholder="" readonly="readonly">
                     </div>
                     <div class="form-right">
                      <label for="">Holder 2</label>
                      <input type="text" value="<?php echo isset($bookdata->TaxHolder2) ? $bookdata->TaxHolder2 : ""; ?>" class="form-control" name="book[TaxHolder2]" placeholder="" readonly="readonly">
                     </div>
                     <div class="clear"></div>
                     <p class="text-left"><small class="text-muted">*Collection of Australian Tax File Numbers is authorised by taxation laws. Quotation of your TFN is not compulsory and will not affect your Application. Non-residents – please indicate County of residence.</small></p>
                  </div>
                  */ ?>

            </div>

        </div>
    </div>
</div>
