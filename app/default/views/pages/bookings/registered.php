<?php
View::$title = 'Registered Interest';
View::$bodyclass = '';
View::header();
?>
    <section class="header-bottom">
        <article>
          <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
        </article>
    </section>
    <?php /* View::template('users/breadcrumb'); */ ?>

    <!-- ************************ Page Content ************************ -->
    
    <section class="gray">
        <article class="container projects-page style-2 animated fadeInUp">
            <?php echo View::getMessage(); ?>
            <div class="block">
                
                <div class="tab-content">

                    <!-- Registered -->
                    <div class="tab-pane fade fade-up active in" id="bookings-registered">
                        <table class="table table-divide table-hover table-vcenter table-header-bg js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center no-wrap"><i class="fa fa-suitcase text-gray"></i> Company Name</th>
                                    <th class="text-center no-wrap"><i class="si si-drawer text-gray"></i> Type Of Offer</th>
                                    <th class="text-center"><i class="si si-note text-gray"></i> Notes</th>
                                    <th class="text-center"><i class="fa fa-money text-gray"></i> Investment</th>
                                    <th class="text-center"><i class="si si-calendar text-gray"></i> Date</th>
                                    <th class="text-center" style="width: 100px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count($bookings)){
                                    foreach($bookings as $proj):
                                        if($proj->BookingStatus == 'Pending' && $proj->BookingType == 'Interest'){ ?>
                                        <tr>
                                            <td class="">
                                                <a class="" href="/project/view/<?php echo $proj->ClientProfileID; ?>"><span class=""><?php echo isset($proj->CompanyName) ? $proj->CompanyName : "-"; ?></span></a><br>
                                            </td>
                                            <td class="text-center"><small><?php echo isset($proj->TypeOfOffer) ? $proj->TypeOfOffer : "-"; ?></small></td>
                                            <td class="text-center">
                                                <small class="text-info">Awaiting for <?php echo isset($proj->CompanyName) ? $proj->CompanyName : "-"; ?> to accept interest.</small>
                                            </td>
                                            <td class="text-right text-primary" data-order="<?php echo isset( $proj->TotalAmountAttached ) ? $proj->TotalAmountAttached : "0"; ?>">
                                                <?php echo isset( $proj->TotalAmountAttached ) ? "$".number_format( $proj->TotalAmountAttached, 2 ) : "-"; ?>
                                            </td>
                                            <td class="text-center no-wrap" data-order="<?php echo $proj->CreatedAt != "0000-00-00" ? date( 'Ymd', strtotime( $proj->CreatedAt ) ) : "00000000"; ?>">
                                                <?php echo $proj->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $proj->CreatedAt ) ) : "-"; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="/bookings/interest/<?php echo $proj->InvestmentBookingID;?>" class="btn btn-sm btn-rounded btn-default" type="button">View Info</a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="6">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>
                    </div>
                    <!-- END Registered -->

                </div>
            </div>
        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script>