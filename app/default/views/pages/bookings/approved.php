<?php
View::$title = 'Approved Bookings';
View::$bodyclass = '';
View::header();
?>

    <section class="header-bottom">
        <article>
          <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
        </article>
    </section>
    <?php /* View::template('users/breadcrumb'); */ ?>

    <!-- ************************ Page Content ************************ -->
    
    <section class="gray">
        <article class="container projects-page style-2 animated fadeInUp">
            <?php echo View::getMessage(); ?>
            <div class="block">                
                <div class="tab-content">
                    <!-- Projects -->
                    <div class="tab-pane fade fade-up in active" id="bookings-all">
                        <table class="table table-divide table-hover table-vcenter table-header-bg js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center no-wrap"><i class="fa fa-suitcase text-gray"></i> Company Name</th>
                                    <th class="text-center"><i class="fa fa-ticket text-gray"></i> Status</th>
                                    <th class="text-center"><i class="fa fa-money text-gray"></i> Investment</th>
                                    <th class="text-center no-wrap"><i class="fa fa-calendar text-gray"></i> Approved Date</th>
                                    <th class="text-center" style="width: 100px;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( count( $bookingss ) ){
                                    foreach( $bookingss as $proj ):
                                        $startdate = date_create( $proj->OfferOpening );
                                        $closedate = date_create( $proj->OfferClosing );
                                        $interval = date_diff( $startdate, $closedate );
                                        switch( $proj->BookingStatus ){
                                            case 'Approved':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$proj->BookingStatus.'</span>';
                                                break;
                                            case 'Pending':
                                                $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$proj->BookingStatus.'</span>';
                                                break;
                                            case 'Verified':
                                                $statusfield = '<span class="label label-info"><i class="fa fa-check"></i> '.$proj->BookingStatus.'</span>';
                                                break;
                                        }

                                        if( $proj->BookingStatus == 'Approved' ){ ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600 push-10">
                                                    <!--a class="" href="/bookings/info/<?php //echo $proj->InvestmentBookingID;?>"><?php //echo isset($proj->CompanyName) ? $proj->CompanyName : "-"; ?></a -->
                                                    <a class="" href="/project/view/<?php echo $proj->ClientProfileID;?>"><?php echo isset( $proj->CompanyName ) ? $proj->CompanyName : "-"; ?></a><br>
                                                    <small class="text-muted"><?php echo isset( $proj->TypeOfOffer ) ? $proj->TypeOfOffer : "-"; ?></small>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <?php echo $statusfield; ?>                                                     
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $statusfield; ?>
                                            </td>
                                            <td class="text-right text-primary" data-order="<?php echo isset( $proj->TotalAmountAttached ) ? $proj->TotalAmountAttached : "0"; ?>">
                                                <?php echo isset( $proj->TotalAmountAttached ) ? "$".number_format( $proj->TotalAmountAttached, 2 ) : "-"; ?>
                                            </td>
                                            <td class="text-center" data-order="<?php echo $proj->AcknowledgeDate != "0000-00-00 00:00:00" ? date( 'Ymd', strtotime( $proj->AcknowledgeDate ) ) : date( 'Ymd', strtotime( $proj->CreatedAt ) ); ?>">
                                                <?php echo isset( $proj->AcknowledgeDate ) && $proj->AcknowledgeDate != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $proj->AcknowledgeDate ) ) : date( 'j M Y', strtotime( $proj->CreatedAt ) ); ?>
                                            </td>
                                            <td class="text-primary text-center">
                                                <div class="">
                                                    <div class="dropdown more-opt">
                                                        <a href="#" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                        <ul class="dropdown-menu dropdown-menu-right">         
                                                           <li><a href="/bookings/info/<?php echo $proj->InvestmentBookingID;?>"><i class="si si-notebook pull-right"></i> Booking Info</a></li>
                                                           <li><a href="/project/view/<?php echo $proj->ClientProfileID;?>"><i class="fa fa-building-o pull-right"></i> View Company</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="5">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END Projects -->
                </div>
            </div>
        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script>