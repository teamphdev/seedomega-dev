<?php
  View::$bodyclass = '';
  View::header(); 
?>

<section class="header-bottom">
    <article>
        <div class="container">
            <?php if($bookdata->BookingStatus == 'Approved') { ?> 
              <h1>CONGRATULATIONS!</h1>    
              <p>Thank you for sharing with us, we assure you to give a high valued information and updates with regards to your investments. We value your trust and we will succeed together with you. For more updates kindly check the right hand sidebar for the things you can do next.</p>
            <?php } elseif($bookdata->BookingStatus == 'Verified') { ?> 
              <h1>BOOKING VERIFIED</h1> 
              <p>Great your booking transaction has been verified and awaiting acknowledgement. Our CSR / Hub Care is escalating the transaction to <?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : ""; ?> <?php echo isset($bookdata->LeadManager) ? '(c/o '.$bookdata->LeadManager.')' : ""; ?> to assure that process is as fast as possible. </p>
            <?php } else { ?>
              <h1>BOOKING SUBMITTED</h1>
              <p>You are one step away from completing your booking transaction. Upload the screen shot or photo of your TT receipt given by the bank where you processed your fund transfer.</p>
            <?php } ?>
        </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">

    <form class="form-ui input_mask" enctype="multipart/form-data" method="post" style="padding: 0;">
    <?php if($bookdata->BookingStatus != 'Approved' || User::info('UserLevel') == 'Administrator'){ ?>
    <input type="hidden" name="action" value="updatebooking">
    <?php } ?>
    <input type="hidden" name="bookid" value="<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : ""; ?>">
    <input type="hidden" name="usrid" value="<?php echo isset($bookdata->UserID) ? $bookdata->UserID : ""; ?>">

    <div class="row about-project">
      <!-- ************************ Left Side Area ************************ -->
    <div class="col-md-8">
        <div class="block">
            <?php echo View::getMessage(); ?>
            <div id="basic-data" class="form-wizard active">

                <?php if($bookdata->BookingType != 'Interest'){ ?>

                <?php
                $notice = '';             

                if($bookdata->DaysLeft == 0) {
                    $notice = 'Booking will expire today';
                } else {
                    $notice = 'Booking will expire in ';
                    $notice .= ($bookdata->DaysLeft > 1) ? $bookdata->DaysLeft .' days' : $bookdata->DaysLeft .' day'; 
                }
                $notice .= ' if remain unattended.';
                ?>
                <div class="row">
                    <div class="col-lg-12">

                        <div class="booking-summary">
                            <div class="block-header">
                                <div class="block-title">INTEREST INFORMATION</div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Booking Summary  -->
                <div class="row">
                    <div class="col-lg-12">

                        <div class="block-content">
                            <div class="booking-id">
                                <span>
                                    Booking ID: BK-533D0M364-<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : '-' ?>
                                </span>
                                <?php if($bookdata->BookingStatus == 'Approved') { ?>
                                <a class="btn btn-xs btn-default pull-right" href="<?php echo View::url('bookings/goprint/'.$bookdata->InvestmentBookingID); ?>" target="_blank"><i class="si si-printer"></i> Print Form</a>
                                <?php } ?>
                            </div>

                            <?php if($bookdata->ReceiptOption == 'TTPhoto'){ ?>
                              <?php if($bookdata->TTReceiptPhoto == 0){ ?>
                                <div class="alert alert-danger push-10-t fade in">
                                  Please Upload The TT Receipt Photo! <?php echo $notice; ?><br><br><a class="push-10-l text-danger" href="<?php echo View::url('bookings/downloadbookingdemandnote/'.$bookdata->InvestmentBookingID) ?>" style="margin: 0 !important;" download><i class="fa fa-cloud-download"></i> <b>Download Demand Note!</b></a></div>
                              <?php } ?>                  
                            <?php } else { ?>
                              <?php if($bookdata->ChequeDrawer == '' || $bookdata->ChequeBank == '' || $bookdata->ChequeBranch == '' || $bookdata->ChequeAmount == 0){ ?>
                                <div class="alert alert-danger push-10-t fade in">
                                  Please complete your cheque details! <?php echo $notice; ?>
                                </div>
                              <?php } ?>
                            <?php } ?>

                            <div class="form-group">
                                <?php 
                                $disable = ($bookdata->BookingStatus == 'Approved') ? 'disabled="disabled"' : '';
                                 ?>
                                <label class="css-input css-radio css-radio-info push-10-r">
                                  <input type="radio" class="receiptopt" name="book[ReceiptOption]" value="TTPhoto" <?php echo ($bookdata->ReceiptOption == 'TTPhoto') ? 'checked=""' : ''; ?> <?php echo $disable; ?>><span></span> TT Receipt Photo
                                </label>
                                <label class="css-input css-radio css-radio-info">
                                  <input type="radio" class="receiptopt" name="book[ReceiptOption]" value="Cheque" <?php echo ($bookdata->ReceiptOption == 'Cheque') ? 'checked=""' : ''; ?> <?php echo $disable; ?>><span></span> Cheque Details 
                                </label>
                            </div>

                            <div class="ttreceipt-opt" style="display: none;">
                                <?php echo AppUtility::getBookedTTReceipt($bookdata); ?>

                                <?php 
                                $ttphoto = View::common()->getUploadedFiles($bookdata->TTReceiptPhoto);

                                if($bookdata->TTReceiptPhoto == 0 || $ttphoto[0]->Active == 0){ ?>
                                  <label class="push-10-t">TT Receipt Photo <small class="text-muted">(Transaction Receipt)</small></label>
                                  <div class="form-group">                  
                                    <input type="file" value="" class="form-control file" name="TTReceiptPhoto" placeholder="" data-min-file-count="0" name="Avatar" data-show-upload="true" data-allowed-file-extensions='["jpeg","png","jpg","pdf"]' >
                                    <small class="text-danger">If you are using check to send funds, please upload the scanned check and supply the fields below</small>
                                  </div>
                                <?php } ?>
                            </div>

                            <div class="cheque-opt" style="display: none;">
                            <div class="text-divider font-w600 push-10-t text-left"><span>Cheque details </span></div>

                              <?php if($bookdata->BookingStatus == 'Approved'){ ?>
                                <table class="table">
                                  <tbody>
                                    <tr>
                                      <td class="font-w600" style="width: 30%;">Drawer</td>
                                      <td><?php echo isset($bookdata->ChequeDrawer) ? $bookdata->ChequeDrawer : "-"; ?></td>
                                    </tr>
                                    <tr>
                                      <td class="font-w600">Bank</td>
                                      <td><?php echo isset($bookdata->ChequeBank) ? $bookdata->ChequeBank : "-"; ?></td>
                                    </tr>
                                    <tr>
                                      <td class="font-w600">Branch</td>
                                      <td><?php echo isset($bookdata->ChequeBranch) ? $bookdata->ChequeBranch : "-"; ?></td>
                                    </tr>
                                    <tr>
                                      <td class="font-w600">Amount</td>
                                      <td><?php echo isset($bookdata->ChequeAmount) ? number_format($bookdata->ChequeAmount,2) : number_format(0,2); ?></td>
                                    </tr>
                                  </tbody>
                                </table>
                              <?php }else{ ?>

                                <div class="form-group">
                                   <div class="form-left">
                                      <label for="">Drawer</label>
                                      <input type="text" value="<?php echo isset($bookdata->ChequeDrawer) ? $bookdata->ChequeDrawer : ""; ?>" class="form-control" name="book[ChequeDrawer]" placeholder="">
                                   </div>
                                   <div class="form-right">
                                    <label for="">Bank</label>                   
                                    <input type="text" value="<?php echo isset($bookdata->ChequeBank) ? $bookdata->ChequeBank : ""; ?>" class="form-control" name="book[ChequeBank]" placeholder="">
                                   </div>
                                   <div class="clear"></div>
                                </div>

                                <div class="form-group">
                                   <div class="form-left">
                                      <label for="">Branch</label>
                                      <input type="text" value="<?php echo isset($bookdata->ChequeBranch) ? $bookdata->ChequeBranch : ""; ?>" class="form-control" name="book[ChequeBranch]" placeholder="">
                                   </div>
                                   <div class="form-right">
                                    <label for="">Amount</label>
                                    <input type="text" value="<?php echo isset($bookdata->ChequeAmount) ? number_format($bookdata->ChequeAmount,2) : number_format(0,2); ?>" class="form-control" name="book[ChequeAmount]" placeholder="">
                                   </div>
                                   <div class="clear"></div>
                                </div>

                                <div class="form-group text-center">
                                   <button type="submit" class="btn btn-rounded btn-primary text-uppercase">Update</button>
                                </div>

                              <?php } ?>

                            </div>

                            <?php } // End of Not Interest condition ?>

                            <div class="form-group">
                               <div class="form-left">
                                <label for="">Total Shares at <span>$<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?></span> per Share</label>
                                  <input type="hidden" id="shareprice" value="<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?>">
                                  <input type="hidden" id="minimumbid" value="<?php echo isset($bookdata->MinimumBid) ? $bookdata->MinimumBid : "0"; ?>">
                                  <div class="input-group">
                                    <input type="text" value="<?php echo isset($bookdata->SharesToApply) ? number_format($bookdata->SharesToApply) : "0"; ?>" class="form-control" placeholder="0" required="required" readonly="readonly">
                                    <span class="input-group-addon">Shares</span>
                                  </div>
                               </div>
                               <div class="form-right">
                                <label for="">Total application monies of</label>
                                  <div class="input-group">
                                    <span class="input-group-addon">USD</span>
                                    <input type="text" value="<?php echo isset($bookdata->TotalAmountAttached) ? number_format($bookdata->TotalAmountAttached,2) : "0"; ?>" class="form-control" placeholder="0.00" readonly="readonly">
                                  </div>
                               </div>
                               <div class="clear"></div>
                            </div>
                            
                        </div>

                    </div>
                </div>
                <!-- END Booking Summary  -->

                <!-- Personal Details -->
                <div class="row">
                    <div class="col-lg-12">

                        <div class="block-content">

                            <div class="block-header-2 pd">Personal Details</div>
                            <div class="form-group">
                              <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                              <input type="text" value="<?php echo isset($bookdata->Name) ? $bookdata->Name : ""; ?>" class="form-control" name="book[Name]" placeholder="" readonly="readonly">
                            </div>

                        </div>

                    </div>
                </div>
                <!-- END Personal Details -->

                <!-- Postal Address -->
                <div class="row">
                    <div class="col-lg-12">

                        <div class="block-content">
                            <div class="block-header-2">Postal Address</div>
                            <div class="form-group">
                                <div class="form-left">
                                    <label for="">Address</label>
                                    <input type="text" value="<?php echo isset($bookdata->Address) ? $bookdata->Address : ""; ?>" class="form-control" name="book[Address]" placeholder="" readonly="readonly">
                                </div>
                                <div class="form-right">
                                    <label for="">Suburb</label>                    
                                    <input type="text" value="<?php echo isset($bookdata->Suburb) ? $bookdata->Suburb : ""; ?>" class="form-control" name="book[Suburb]" placeholder="" readonly="readonly">
                                    </div>
                                <div class="clear"></div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label for="">State</label>
                                    <input type="text" value="<?php echo isset($bookdata->State) ? $bookdata->State : ""; ?>" class="form-control" name="book[State]" placeholder="" readonly="readonly">
                                </div>
                                <div class="form-right">
                                    <label for="">Postcode</label>                   
                                    <input type="text" value="<?php echo isset($bookdata->PostCode) ? $bookdata->PostCode : ""; ?>" class="form-control" name="book[PostCode]" placeholder="" readonly="readonly">
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- END Postal Address -->

                <!-- Contact Details -->
                <div class="row">
                    <div class="col-lg-12">

                        <div class="block-content">

                            <div class="block-header-2">Contact Details</div>
                            <div class="form-group">
                                <div class="form-left">
                                    <label for="">Telephone Number</label>
                                    <input type="text" value="<?php echo isset($bookdata->Phone) ? $bookdata->Phone : ""; ?>" class="form-control" name="book[Phone]" placeholder="" readonly="readonly">
                                </div>
                                <div class="form-right">
                                    <label for="">Email Address</label>                   
                                    <input type="email" value="<?php echo isset($bookdata->Email) ? $bookdata->Email : ""; ?>" class="form-control" name="book[Email]" placeholder="" readonly="readonly">
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- END Contact Details -->
            </div>
        </div>
    </div> 
      
      <!-- ************************ Right Side Area ************************ -->
      <div class="col-lg-4">
        <div class="sidebar">

          <?php if(User::info('UserLevel') == "Administrator" || User::can('Verify Booking')){ ?>
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="w-title">Change Status</div>
            <div class="pcontent">
              <select name="book[BookingStatus]" class="form-control">
                <option value="Pending" <?php echo ($bookdata->BookingStatus == 'Pending') ? "selected" : ""; ?>>Pending</option>
                <option value="Verified" <?php echo ($bookdata->BookingStatus == 'Verified') ? "selected" : ""; ?>>Verified</option>
                <?php if(User::info('UserLevel') == "Administrator"){ ?>
                <option value="Approved" <?php echo ($bookdata->BookingStatus == 'Approved') ? "selected" : ""; ?>>Approved</option>
                <?php } ?>
              </select>

              <div class="text-center push-10-t">
                <button type="submit" class="btn btn-rounded btn-primary btn-block">Save Changes</button>
              </div>

            </div>
          </div>
          <?php } ?>

            <?php if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' ): ?>
            <div class="sidebar-item">
                <div class="w-title">What Can I Do Next?</div>
                <div class="w-content">
                    <div class="popular-posts-widget viewbookhelp userhelp">
                      <?php if($bookdata->BookingStatus == 'Approved') { ?>
                        <div class="side-nav-wcidn">
                            <span class="num">1</span>
                            <div class="num-content">
                                <h4 class="wcidn-title sopink">Join the Community Forums</h4>
                                <p class="wcidn-text">Share something, connect with others! The community forums are where the SeedOmega community actively answers questions just like yours. It’s a dynamic and secure place to connect with other investors and clients.</p>
                                <a class="btn btn-xs btn-default btn-rounded " href="<?php echo View::url('forums/dashboard/'.$bookdata->ClientProfileID); ?>" target="_blank">Start Now</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">2</span>
                            <div class="num-content">
                                <h4 class="wcidn-title">Leave a Comment</h4>
                                <p class="wcidn-text">Have a question, need an answer, or just want to join in on discussions? Click the comment button below. Visit our FAQ section to find the answers for the most common questions asked by our clients and investors.</p>
                                <a class="btn btn-xs btn-default btn-rounded " href="<?php echo View::url('project/view/'.$bookdata->ClientProfileID.'/client-comments'); ?>" target="_blank">Comment Now</a>&nbsp;<a href="<?php echo View::url('project/view/'.$bookdata->ClientProfileID.'/client-faq'); ?>" class="btn btn-xs btn-default btn-rounded ">FAQ</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">3</span>
                            <div class="num-content">
                                <h4 class="wcidn-title">Company News</h4>
                                <p class="wcidn-text">Stay up to date with the most recent company news, company results, industry news, corporate financials and much more!</p>
                                <a href="<?php echo View::url('project/view/'.$bookdata->ClientProfileID.'/client-blogs'); ?>" class="btn btn-xs btn-default btn-rounded ">Blogs</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">4</span>
                            <div class="num-content">
                                <h4 class="wcidn-title"><a>View Company</a></h4>
                                <p class="wcidn-text">Learn more about the company, including the company profile, company fact sheet, corporate responsibility, investment information, and more.</p>
                                <a href="<?php echo View::url('project/view/'.$bookdata->ClientProfileID); ?>" class="btn btn-xs btn-default btn-rounded ">View Info</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">5</span>
                            <div class="num-content">
                                <h4 class="wcidn-title"><a>Print Booking Form</a></h4>
                                <p class="wcidn-text">Do you need a hard copy of your booking form? We got you covered just click the button below to print your form. </p>
                                <a class="btn btn-xs btn-default btn-rounded " target="_blank" href="<?php echo View::url('bookings/goprint/'.$bookdata->InvestmentBookingID); ?>"><i class="si si-printer"></i> Print Form</a>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">6</span>
                            <div class="num-content">
                                <h4 class="wcidn-title"><a>Contact Support Team</a></h4>
                                <p class="wcidn-text">For more information and inquiries regarding your transaction please feel free to contact our support.</p>
                                <a class="btn btn-xs btn-default btn-rounded " href="<?php echo View::url('support/dashboard/'); ?>">Contact Support</a>
                            </div>
                        </div>
                      <?php } elseif($bookdata->BookingStatus == 'Verified') { ?>
                        <div class="side-nav-wcidn">
                            <span class="num">1</span>
                            <div class="num-content">
                                <h4 class="wcidn-title sopink">Booking has been Verified</h4>
                                <p class="wcidn-text">Sit tight and relax, rest assured our CSR / Hub Care is escalating the transaction to <?php echo isset($bookdata->CompanyName) ? $bookdata->CompanyName : ""; ?> <?php echo isset($bookdata->LeadManager) ? '(c/o '.$bookdata->LeadManager.')' : ""; ?> to assure that process is as fast as possible.</p>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">2</span>
                            <div class="num-content">
                                <h4 class="wcidn-title"><a>Contact Support Team</a></h4>
                                <p class="wcidn-text">For more information and inquiries regarding your transaction please feel free to contact our support.</p>
                                <a class="btn btn-xs btn-default btn-rounded " href="<?php echo View::url('support/dashboard/'); ?>">Contact Support</a>
                            </div>
                        </div>
                      <?php } else { ?>
                        <div class="side-nav-wcidn">
                            <span class="num">1</span>
                            <div class="num-content">
                                <h4 class="wcidn-title">Complete your booking</h4>
                                <p class="wcidn-text">Upload the screen shot or photo of your TT receipt given by the bank where you processed your fund transfer</p>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">2</span>
                            <div class="num-content">
                                <h4 class="wcidn-title sopink">I already uploaded my TT receipt</h4>
                                <p class="wcidn-text">No worries our Customer Service Representative or Hub Care is already verifying your receipt and you will receive an email to confirm that your booking is completed.</p>
                            </div>
                        </div>
                        <div class="side-nav-wcidn">
                            <span class="num">3</span>
                            <div class="num-content">
                                <h4 class="wcidn-title"><a>Contact Support Team</a></h4>
                                <p class="wcidn-text">For more information and inquiries regarding your transaction please feel free to contact our support.</p>
                                <a class="btn btn-xs btn-default btn-rounded " href="<?php echo View::url('support/dashboard/'); ?>">Contact Support</a>
                            </div>
                        </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

      </div> 
             
    </div>
    </form>
  </article>
</section>

<?php View::footer(); ?>

<script>
// setTimeout(function () { window.location.reload(); }, 5*60*1000);
// just show current time stamp to see time of last refresh.
// document.write(new Date());
</script>