<?php
ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );
View::$title = 'Verified Bookings';
View::$bodyclass = '';
View::header();
?>
    <section class="header-bottom">
        <article>
          <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
        </article>
    </section>
    <?php /* View::template('users/breadcrumb'); */ ?>

    <!-- ************************ Page Content ************************ -->
    
    <section class="gray">
        <article class="container projects-page style-2 animated fadeInUp">
            <?php echo View::getMessage(); ?>
                        
            <div class="block">
                <div class="tab-content">

                     <!-- Verified -->
                    <div class="tab-pane fade fade-up in active" id="bookings-verified">
                        <table class="table table-divide table-hover table-vcenter table-header-bg js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center no-wrap"><i class="fa fa-suitcase text-gray"></i> Company Name</th>
                                    <th class="text-center no-wrap"><i class="si si-drawer text-gray"></i> Type Of Offer</th>
                                    <th class="text-center"><i class="si si-note text-gray"></i> Notes</th>
                                    <th class="text-center"><i class="fa fa-money text-gray"></i> Investment</th>
                                    <th class="text-center no-wrap"><i class="si si-calendar text-gray"></i> Booking Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if( count( $bookings ) ){
                                    foreach( $bookings as $proj ):
                                        if( $proj->BookingStatus == 'Verified' ){ ?>
                                        <tr>
                                            <td class="">
                                                <a class="" href="/bookings/info/<?php echo $proj->InvestmentBookingID;?>"><span class="font-w600"><?php echo isset( $proj->CompanyName ) ? $proj->CompanyName : "-"; ?></span></a><br>
                                            </td>
                                            <td class="text-center"><small><?php echo isset( $proj->TypeOfOffer ) ? $proj->TypeOfOffer : "-"; ?></small></td>
                                            <td class="text-center"><small class="text-info">Awaiting fund acknowledgement.</small></td>
                                            <td class="text-right text-primary">
                                                <?php echo isset( $proj->TotalAmountAttached ) ? "$".number_format( $proj->TotalAmountAttached, 2 ) : "-"; ?>
                                            </td>
                                            <td class="text-center" data-order="<?php echo $proj->CreatedAt != "0000-00-00" ? $proj->CreatedAt : "0000-00-00"; ?>">
                                                <?php echo $proj->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $proj->CreatedAt ) ) : "-"; ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="5">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                                                         
                            </tbody>
                        </table>
                    </div>
                    <!-- END Verified -->

                </div>
            </div>
            
            

        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script>