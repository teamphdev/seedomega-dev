<?php 
View::$title = 'Thank You!';
View::$bodyclass = '';
View::header(); 
?>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Dashboard</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
  <article class="container project-single">
<!-- BK-533D0M364 -->
    <div class="client-company text-center">
      <h3><?php echo isset($bookdata->CompanyName) ? 'Seeding with '.$bookdata->CompanyName.' submitted!': "No title"; ?></h3>
      <p>&nbsp;</p>
      <h4>You are one step away from completing your booking transaction.<br>Please check the information below then download the "Demand Note" for your referrence in depositing the funds.</h4>
      <p>&nbsp;</p>
      <hr><br>

    </div>

    <div class="row about-project text-center">        
      <div class="" style="max-width: 480px; margin:0 auto;">
        <div class="sidebar">

          <!-- Project Progress -->
          <div class="sidebar-item">

            <div class="project-progress form-ui input_mask client-company pcontent">

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->InvestmentBookingID) ? 'BK-533D0M364-'.$bookdata->InvestmentBookingID : ""; ?><br>
                        <small class="text-muted">Book ID</small>
                    </div>
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->CreatedAt) ? date('M d Y',strtotime($bookdata->CreatedAt)) : "0"; ?><br>
                        <small class="text-muted">Date Submitted</small>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->SharesToApply) ? number_format($bookdata->SharesToApply) : "0"; ?><br>
                        <small class="text-muted">Shares</small>
                    </div>
                    <div class="col-xs-6 push-10 offer-data">
                        USD <?php echo isset($bookdata->TotalAmountAttached) ? number_format($bookdata->TotalAmountAttached,2) : "0"; ?><br>
                        <small class="text-muted">Total Attachment</small>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-xs-12 col-sm-12 offer-size offer-data">
                        $<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?><br>
                        <small class="text-muted">Price</small>
                    </div>
                </div>
                <?php /*
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        $<?php echo isset($bookdata->Price) ? number_format($bookdata->Price, 2) : "0.00"; ?><br>
                        <small class="text-muted">Price</small>
                    </div>
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset($bookdata->SizeOfOffer) ? $bookdata->SizeOfOffer : "-"; ?><br>
                        <small class="text-muted">Size Of Offer</small>
                    </div>
                </div>
                */ ?>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset($bookdata->Name) ? $bookdata->Name : ""; ?><small>Name</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo isset($bookdata->UserID) ? $bookdata->UserID : ""; ?><small>User ID</small>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-xs-12 col-sm-12 offer-size offer-data">
                    <a class="btn btn-block btn-success" href="<?php echo View::url('bookings/downloadbookingdemandnote/'.$bookdata->InvestmentBookingID) ?>" style="margin-right: 5px;" download><i class="fa fa-cloud-download"></i> Download Demand Note</a>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>

      </div> 

      <div class="text-center push-30-t">
        <a href="<?php echo View::url('users/dashboard'); ?>" class="btn btn-secondary"><i class="fa fa-undo"></i> Back To Dashboard</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo View::url('bookings/info/'.$bookdata->InvestmentBookingID); ?>" class="btn btn-primary"><i class="fa fa-cloud-upload"></i> Upload TT Receipt</span></a>
      </div>

    </div>
  </div>

  </article>
</section>

<?php View::footer(); ?>