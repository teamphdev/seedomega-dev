<?php 
View::$title = 'Media Manager';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo Lang::get('MEDIA_TITLE'); ?></h2>
                        <?php if(User::can('Add Media')) { ?>
                        <a class="btn btn-warning btn-default pull-right" href="<?php echo View::url('mediamanager/add'); ?>">Add Media</a>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php echo View::getMessage();  ?> 
                        <form method="post" action="">
                        <input type="hidden" name="action" value="savefileorder" />
                        <table id="mediamanagertable" class="table default-table dt-responsive bulk_action nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr class="headings">
                                    <th width="30" class="text-center">Order</th> 
                                    <th width="30" class="text-center"><?php echo Lang::get('MEDIA_IMG'); ?></th> 
                                    <th class="text-center">Upload Name</th>
                                    <th class="text-center"><?php echo Lang::get('MEDIA_NAME'); ?></th>
                                    <th class="text-center">Chinese <?php echo Lang::get('MEDIA_NAME'); ?></th>
                                    <th class="text-center">Category</th>
                                    <th class="text-center">Sub Category</th>
                                    <th class="text-center"><?php echo Lang::get('MEDIA_DATEADD'); ?></th>
                                    <th class="no-sorting text-center"><?php echo Lang::get('MEDIA_ACTN'); ?></th>
                                </tr>
                            </thead>
                            <tfoot class="filter">
                                <tr class="headings">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php 
                                $cntr = 0;
                                if(count($images)) {
                                foreach($images as $image) { $cntr++;
                                    $ext = pathinfo($image->FileSlug, PATHINFO_EXTENSION);
                                    $dlink = View::asset("files/downloads".$image->FileSlug);
                                    switch ($ext) {
                                        case 'pdf':
                                            $fileSlugUrl = View::asset("images/pdf.png");
                                            break;
                                        default:                                            
                                            $fileSlugUrl = getimagesize($dlink) !== false ? $dlink : View::asset("images/blank-icon.jpg");
                                            break;
                                    }
                                ?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <td><input class="form-control orderinput" type="number" name="order[<?php echo $image->FileID; ?>]" value="<?php echo $image->Order; ?>"></td>
                                    <td>
                                        <a href="<?php echo $dlink; ?>" title="<?php echo $image->FileDescription; ?>" data-group="<?php echo isset($image->FileCategoryName) ? $image->FileCategoryName : "nogroup"; ?>" data-thumbnail="<?php echo $fileSlugUrl; ?>" class="html5lightbox">
                                        <img src="<?php echo $fileSlugUrl; ?>" title="<?php echo $image->FileName; ?>" alt="<?php echo $image->FileName; ?>" class="img-responsive center-block" style="width:30px; height: 30px;" /></a>
                                    </td>
                                    <td><a href="<?php echo View::url()."/assets/files/downloads".$image->FileSlug; ?>" data-group="<?php echo isset($image->FileCategoryName) ? $image->FileCategoryName.'2' : "nogroup"; ?>" title="<?php echo $image->FileDescription; ?>" data-thumbnail="<?php echo $fileSlugUrl; ?>" class="html5lightbox"><?php echo $image->FileName; ?></a></td>
                                    <td class="text-center"><?php echo $image->FileDescription; ?></td>
                                    <td class="text-center"><?php echo $image->FileNameC; ?></td>
                                    <td class="text-center"><?php echo isset($image->FileCategoryName) ? $image->FileCategoryName : "-"; ?></td>
                                    <td class="text-center"><?php echo isset($image->FileSubCategoryName) ? $image->FileSubCategoryName : "-"; ?></td>
                                    <td class="text-center"><?php echo App::date($image->DateAdded, 'Y-m-d'); ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo View::url('mediamanager/edit/'.$image->FileID); ?>" title="Delete" class="blue">Edit</a>
                                        
                                        <a href="<?php echo View::url('mediamanager/delete/'.$image->FileID); ?>" title="Delete" onclick="return confirm('Are you sure you want to delete <?php echo $image->FileName; ?>?');" class="red"><?php echo Lang::get('MEDIA_ACTN_DEL'); ?></a>
                                    </td>
                                </tr>
                                <?php } 
                                } else {?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <td colspan="9">No Data</td>
                                </tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>
                        <input type="submit" name="SaveOrder" class="btn btn-success" value="Save Order" />
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /page content -->
<?php View::footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        
        // Casefile Table
        $("#mediamanagertable").dataTable({
            "lengthMenu": [ [25, 50, 75, 100, -1], [25, 50, 75, 100, "All"] ],
            "pageLength": 25
        }).yadcf([
            {column_number : 0, filter_type: 'none'},
            {column_number : 1, filter_type: 'none'},
            {column_number : 2, filter_type: 'none'},
            {column_number : 3, filter_type: 'none'},
            {column_number : 4, filter_type: 'none'},
            {column_number : 5, filter_type: 'select', filter_default_label: "All"},
            {column_number : 6, filter_type: 'select', filter_default_label: "All"},
            {column_number : 7, filter_type: 'none'},
            {column_number : 8, filter_type: 'none'}
        ], 'footer');

    });
</script>
