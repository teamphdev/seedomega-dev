<?php 
View::$title = 'Sub Categories';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <a class="btn btn-warning btn-default pull-right" href="<?php echo View::url('mediamanager/addsubcategory'); ?>">Add Sub Category</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php echo View::getMessage();  ?> 
                        <form method="post" action="">
                        <input type="hidden" name="action" value="savesubcategoryorder" />
                        <table id="datatable-responsive" class="table default-table dt-responsive bulk_action nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr class="headings">
                                    <th width="30" class="text-center">Order</th> 
                                    <th>Sub Category Name</th>
                                    <th>Sub Category Description</th>
                                    <th class="text-center">Parent Category</th>
                                    <th width="200" data-priority="1" class="no-sorting text-center"><?php echo Lang::get('MEDIA_ACTN'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $cntr = 0;
                                if(count($subcats)) {
                                foreach($subcats as $catsub) { $cntr++;                                    
                                ?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer catID-<?php echo $cat->FileCategoryID; ?>">
                                    <td class="text-center">
                                    <input class="form-control orderinput" type="number" name="order[<?php echo $catsub->FileSubCategoryID; ?>]" value="<?php echo $catsub->Order; ?>"></td>
                                    <td><?php echo ($catsub->FileSubCategoryName) ? $catsub->FileSubCategoryName : "---"; ?></td>
                                    <td><?php echo ($catsub->FileSubCategoryDescription) ? $catsub->FileSubCategoryDescription : "---"; ?></td>
                                    <td class="text-center"><?php echo ($catsub->ParentCatName) ? $catsub->ParentCatName : "---"; ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo View::url('mediamanager/editsubcategory/'.$catsub->FileSubCategoryID); ?>" title="Edit" class="green">Edit</a> | 
                                        <?php if(User::can('Delete Media')) { ?>
                                        <a href="<?php echo View::url('mediamanager/deletesubcategory/'.$catsub->FileSubCategoryID); ?>" title="Delete" onclick="return confirm('Are you sure you want to delete <?php echo ($catsub->FileSubCategoryName) ? $catsub->FileSubCategoryName : "---"; ?>?');" class="red">Delete</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } 
                                } else {?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <td colspan="5">No Data</td>
                                </tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>
                        <input type="submit" name="SaveOrder" class="btn btn-success" value="Save Order" />
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /page content -->
<?php View::footer(); ?>