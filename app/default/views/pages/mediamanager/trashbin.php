<?php 
View::$title = 'Trashed Files';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <?php //View::template('toolboxnav'); ?>
                        <a class="btn btn-warning btn-default pull-right" href="<?php echo View::url('products/add'); ?>">Add Product</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php echo View::getMessage();  ?> 
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive bulk_action nowrap" cellspacing="0" width="100%">
                            <tr class="headings">
                                <th class="sorting_disabled no-sorting text-center"><input type="checkbox" id="check-all" class="flat"></th>
                                <th width="80" class="text-center">Image</th> 
                                <th class="sort-this">Name</th>
                                <th>Description</th> 
                                <th>Slug</th> 
                                <th>Date Added</th>
                                <th class="no-sorting text-center">Action</th>
                            </tr>
                            <tbody>
                                <?php 
                                $cntr = 0;
                                if(count($images)) {
                                foreach($images as $image) { $cntr++;
                                ?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <td class="text-center"><input type="checkbox" name="table_records" value="<?php echo $image->FileID; ?>" class="flat"></td>
                                    <td>
                                        <img src="<?php echo view::url(); ?>/assets/files<?php echo $image->FileSlug; ?>" title="<?php echo $image->FileName; ?>" alt="<?php echo $image->FileName; ?>" class="img-responsive center-block" style="width:80px; height: 80px;" />
                                    </td>
                                    <td><?php echo $image->FileName; ?></td>
                                    <td><?php echo $image->FileDescription; ?></td>
                                    <td><a href="<?php echo view::url(); ?>/assets/files/<?php echo $image->FileSlug; ?>" onclick="PopUpWindow(this.href,'MyPopupWindow','1000','800','no','center');return false"><?php echo view::url(); ?>/assets/files<?php echo $image->FileSlug; ?></a></td>
                                    <td><?php echo date('d-M-Y', strtotime($image->DateAdded)); ?></td>
                                    <td width="120px" class="center">
                                        <?php if($userinfo->Level == 1) { ?>
                                        <a href="<?php echo View::url('mediamanager/restore/'.$image->FileID); ?>" title="Restore" class="mo-icon green"><span class="fa fa-undo"></span></a>
                                        <a href="<?php echo View::url('mediamanager/delete/'.$image->FileID); ?>" title="Delete" onclick="return confirm('Are you sure you want to delete this product permanently? If you continue you will be able to undo this action!');" class="mo-icon red"><span class="fa fa-remove"></span></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } 
                                } else {?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <!--td class="a-center"><input type="checkbox" name="table_records" value="<?php //echo $user->ProductID; ?>" class="flat"></td-->
                                    <td colspan="9">Trash bin is empty</td>
                                </tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php View::footer(); ?>