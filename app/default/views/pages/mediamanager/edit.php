<?php 
View::$title = 'Edit Media';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<div class="right_col" role="main">

    <div class=""> 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php echo View::getMessage(); ?>   
                            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="fileid" value="<?php echo $files->FileID; ?>" />
                                <input type="hidden" name="fileitemid" value="<?php echo $files->FileItemID; ?>" />
                                <input type="hidden" name="action" value="updatemedia" />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align:left;">
                                        <?php 
                                        $ext = pathinfo($files->FileSlug, PATHINFO_EXTENSION);
                                        switch ($ext) {
                                            case 'pdf':
                                                $fileSlugUrl = view::url()."/assets/images/pdf.png";
                                                break;
                                            default:
                                                $fileSlugUrl = view::url()."/assets/files/downloads".$files->FileSlug;
                                                break;
                                        }
                                        ?>
                                        <a href="<?php echo view::url()."/assets/files/downloads".$files->FileSlug; ?>" title="<?php echo $files->FileDescription; ?>" data-group="<?php echo isset($files->FileCategoryName) ? $files->FileCategoryName : "nogroup"; ?>" data-thumbnail="<?php echo $fileSlugUrl; ?>" class="html5lightbox">
                                            <img src="<?php echo getimagesize($fileSlugUrl) !== false ? $fileSlugUrl : view::url()."/assets/images/blank-icon.jpg"; ?>" title="<?php echo $files->FileName; ?>" alt="<?php echo $files->FileName; ?>" class="img-responsive center-block" style="width:300px; margin: 0;" /></a>
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        File Name
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">                                        
                                        <input type="text" value="<?php echo $files->FileDescription; ?>" id="fname" name="fi[FileDescription]" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        Chinese File Name
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">                                        
                                        <input type="text" value="<?php echo $files->FileNameC; ?>" id="fname" name="fi[FileNameC]" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">File Category</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="CategoriesLists" class="form-control" name="FileCategoryID" required="required" rel="<?php echo View::url('mediamanager/getsubcats/'); ?>">
                                            <?php if(count($fileCategories)){ ?>
                                                <?php foreach ($fileCategories as $fileCategory) { 
                                                    $sel = $files->FileCategoryID == $fileCategory->FileCategoryID ? 'selected="selected"' : '';
                                                ?>
                                                    <option value="<?php echo isset($fileCategory->FileCategoryID) ? $fileCategory->FileCategoryID : "" ?>" <?php echo $sel; ?>><?php echo isset($fileCategory->FileCategoryName) ? $fileCategory->FileCategoryName : "" ?></option>
                                                <?php } ?>
                                            <?php }else{ ?>
                                                <option value="">No Category</option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">File Sub Category</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="SubCatLists" class="form-control" name="FileSubCategoryID">
                                            <option value="0" selected="selected">None</option>
                                        </select>
                                        <input id="SubCatID" type="hidden" value="<?php echo $files->FileSubCategoryID; ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Agencies</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <?php if(count($agencies)){  
                                            
                                            $agencyids = (View::common()->stringToArray($files->FileAccess)) ? View::common()->stringToArray($files->FileAccess) : array(); ?>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <input id="GAAall" type="checkbox" value="All"> 
                                                <label for="GAA<?php echo $agency->UserID; ?>">All</label>
                                            </div>
                                            <?php foreach ($agencies as $agency) { 
                                                $checked = in_array($agency->UserID,$agencyids) ? 'checked="checked"' : '';
                                                ?>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <input id="GAA<?php echo $agency->UserID; ?>" type="checkbox" class="GAmedia" name="FileAccess[]" value="<?php echo $agency->UserID; ?>" <?php echo $checked; ?>> 
                                                    
                                                    <label for="GAA<?php echo $agency->UserID; ?>"><?php echo $agency->FirstName.' '.$agency->LastName; ?> <?php echo ($agency->CompanyName) ? '<small>('.$agency->CompanyName.')</small>' : ''; ?></label>
                                                    
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>

                                
                                <div class="ln_solid"></div>
                                <div class="item form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <a href="<?php echo View::url('mediamanager'); ?>" class="btn btn-warning">Back</a>
                                        <button id="send" type="submit" class="btn btn-success">Update Media Info</button>
                                    </div>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<!-- /page content -->
<?php View::footer(); ?>