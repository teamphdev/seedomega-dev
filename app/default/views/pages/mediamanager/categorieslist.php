<?php 
View::$title = 'Folder Categories';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <a class="btn btn-warning btn-default pull-right" href="<?php echo View::url('mediamanager/addcategory'); ?>">Add Category</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <?php echo View::getMessage();  ?> 
                        <form method="post" action="">
                        <input type="hidden" name="action" value="savecategoryorder" />
                        <table id="datatable-responsive" class="table default-table dt-responsive bulk_action nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr class="headings">
                                    <th width="30" class="text-center">Order</th> 
                                    <th class="text-center" data-priority="1">Category Name</th>
                                    <th class="text-center">Description</th>
                                    <th width="200" data-priority="1" class="no-sorting text-center"><?php echo Lang::get('MEDIA_ACTN'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $cntr = 0;
                                if(count($categories)) {
                                foreach($categories as $cat) { $cntr++;                                    
                                ?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer catID-<?php echo $cat->FileCategoryID; ?>">
                                    <td class="text-center"><input class="form-control orderinput" type="number" name="order[<?php echo $cat->FileCategoryID; ?>]" value="<?php echo $cat->Order; ?>"></td>
                                    <td class="text-center"><?php echo ($cat->FileCategoryName) ? $cat->FileCategoryName : "No Title"; ?></td>
                                    <td class="text-center"><?php echo isset($cat->FileCategoryDescription) ? $cat->FileCategoryDescription : ""; ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo View::url('mediamanager/editcategory/'.$cat->FileCategoryID); ?>" title="Edit" class="blue">Edit / Sub categories</a> |
                                        
                                        <a href="<?php echo View::url('mediamanager/deletecategory/'.$cat->FileCategoryID); ?>" title="Delete" onclick="return confirm('Are you sure you want to delete <?php echo $cat->FileCategoryName; ?>? This will also delete the sub categories, you will not be able to undo the action if you continue.');" class="red"><?php echo Lang::get('MEDIA_ACTN_DEL'); ?></a>
                                    </td>
                                </tr>
                                <?php } 
                                } else {?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <td colspan="9">No Data</td>
                                </tr>
                                <?php } ?>
                                
                            </tbody>
                        </table>
                        <input type="submit" name="SaveOrder" class="btn btn-success" value="Save Order" />
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /page content -->
<?php View::footer(); ?>