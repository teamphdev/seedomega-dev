<?php 
View::$title = 'Add Category';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">

    <div class=""> 

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php echo View::getMessage(); ?>   
                            <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="action" value="addcategory" />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Order</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" value="0" id="fname" name="Order" required="required" class="form-control col-md-7 col-xs-12">
                                        <small>Ascending from 0 onwards</small>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        Category Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="" id="fname" name="FileCategoryName" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('PRD_EDIT_DESC'); ?></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control dowysiwyg" name="FileCategoryDescription"></textarea>
                                    </div>
                                </div>



                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <a href="javascript:void(0);" onclick="window.history.back();" class="btn btn-warning"><?php echo Lang::get('PRD_EDIT_BCKBTN'); ?></a>
                                        <button id="send" type="submit" class="btn btn-success">Add Category</button>
                                    </div>
                                </div>
                                
                            </form>
                               
                        </div>
                    </div>
                </div>
                               
            </div>
        </div>

    </div>
    
</div>
<!-- /page content -->
<?php View::footer(); ?>