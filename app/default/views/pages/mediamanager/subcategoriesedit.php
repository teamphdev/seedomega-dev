<?php 
View::$title = 'Edit Sub Category';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">

    <div class=""> 

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php echo View::getMessage(); ?>   
                            <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="action" value="editsubcategory" />
                                <input type="hidden" name="subcatid" value="<?php echo $subcats->FileSubCategoryID; ?>" />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        Parent Category <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select name="FileCategoryID" class="form-control">
                                            <?php
                                            $cntr = 0;
                                            if(count($categories)) {
                                            foreach($categories as $cat) { $cntr++;

                                            ?>
                                                <option value="<?php echo $cat->FileCategoryID; ?>" <?php echo ($cat->FileCategoryID == $subcats->FileCategoryID) ? "selected" : ""; ?>><?php echo ($cat->FileCategoryName) ? $cat->FileCategoryName : "No Title"; ?></option>
                                            <?php } 
                                            } else {?>
                                                <option value="">--No Data--</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Order</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" value="<?php echo $subcats->Order; ?>" id="fname" name="Order" required="required" class="form-control col-md-7 col-xs-12">
                                        <small>Ascending from 0 onwards</small>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                        Sub Category Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="<?php echo ($subcats->FileSubCategoryName) ? $subcats->FileSubCategoryName : "--"; ?>"  name="FileSubCategoryName" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('PRD_EDIT_DESC'); ?></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control dowysiwyg" name="FileSubCategoryDescription"><?php echo ($subcats->FileSubCategoryDescription) ? $subcats->FileSubCategoryDescription : ""; ?></textarea>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <a href="javascript:void(0);" onclick="<?php echo ($CurrentCatID) ? 'window.location=\''.View::url('mediamanager/editcategory/'.$CurrentCatID).'\'' : 'window.history.back();'; ?>" class="btn btn-warning"><?php echo Lang::get('PRD_EDIT_BCKBTN'); ?></a>
                                        <button id="send" type="submit" class="btn btn-success">Update Sub Category</button>
                                    </div>
                                </div>
                                
                            </form>
                               
                        </div>
                    </div>
                </div>
                               
            </div>
        </div>

    </div>
    
</div>
<!-- /page content -->
<?php View::footer(); ?>