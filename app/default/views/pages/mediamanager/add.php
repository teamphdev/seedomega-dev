<?php 
View::$title = 'Add Media';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<div class="right_col" role="main">

    <div class=""> 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php echo View::getMessage(); ?>   
                            <form class="form-horizontal form-label-left" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="f[UserID]" value="<?php echo User::info('UserID'); ?>" />
                                <input type="hidden" name="action" value="addmedia" />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        File Name
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="" id="fname" name="FileName" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        Chinese File Name
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="" id="fname" name="FileNameC" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">File Category</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="CategoriesLists" class="form-control" name="FileCategoryID" required="required" rel="<?php echo View::url('mediamanager/getsubcats/'); ?>">
                                            <?php if(count($fileCategories)){ ?>
                                                <?php foreach ($fileCategories as $fileCategory) { ?>
                                                    <option value="<?php echo isset($fileCategory->FileCategoryID) ? $fileCategory->FileCategoryID : "" ?>"><?php echo isset($fileCategory->FileCategoryName) ? $fileCategory->FileCategoryName : "" ?></option>
                                                <?php } ?>
                                            <?php }else{ ?>
                                                <option value="">No Category</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">File Sub Category</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select id="SubCatLists" class="form-control" name="FileSubCategoryID">
                                            <option value="0" selected="selected">None</option>
                                        </select>
                                        <input id="SubCatID" type="hidden" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Agencies</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <?php if(count($agencies)){ ?>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <input id="GAAall" type="checkbox" value="All"> 
                                                <label for="GAA<?php echo $agency->UserID; ?>">All</label>
                                            </div>
                                            <?php foreach ($agencies as $agency) { ?>
                                                <div class="col-md-4 col-sm-4 col-xs-12">
                                                    <input id="GAA<?php echo $agency->UserID; ?>" type="checkbox" class="GAmedia" name="FileAccess[]" value="<?php echo $agency->UserID; ?>"> 
                                                    <label for="GAA<?php echo $agency->UserID; ?>"><?php echo $agency->FirstName.' '.$agency->LastName; ?> <?php echo ($agency->CompanyName) ? '<small>('.$agency->CompanyName.')</small>' : ''; ?></label>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">File Upload</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input class="file" type="file" name="MediaUpload" data-show-upload="false" data-allowed-file-extensions='["docx","doc","pdf","jpeg","png","jpg","bmp"]'>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="item form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <a href="<?php echo View::url('mediamanager'); ?>" class="btn btn-warning">Back</a>
                                        <button id="send" type="submit" class="btn btn-success">Add Media</button>
                                    </div>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<!-- /page content -->
<?php View::footer(); ?>