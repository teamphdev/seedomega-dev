<?php 
View::$title = 'Edit Category';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<div class="right_col" role="main">

    <div class=""> 

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2><?php echo View::$title; ?></h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php echo View::getMessage(); ?>   
                            <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="action" value="updatecategory" />
                                <input type="hidden" name="catid" value="<?php echo $cat->FileCategoryID; ?>" />
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Order</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" value="<?php echo $cat->Order; ?>" id="fname" name="Order" required="required" class="form-control col-md-7 col-xs-12">
                                        <small>Ascending from 0 onwards</small>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                        Category Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="<?php echo isset($cat->FileCategoryName) ? $cat->FileCategoryName : ''; ?>" id="fname" name="FileCategoryName" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('PRD_EDIT_DESC'); ?></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea class="form-control dowysiwyg" name="FileCategoryDescription"><?php echo isset($cat->FileCategoryDescription) ? $cat->FileCategoryDescription : ''; ?></textarea>
                                    </div>
                                </div>
                                
                                

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <a href="javascript:void(0);" onclick="window.history.back();" class="btn btn-warning"><?php echo Lang::get('PRD_EDIT_BCKBTN'); ?></a>
                                        <button id="send" type="submit" class="btn btn-success">Update Category</button>
                                    </div>
                                </div>
                                
                            </form>
                               
                        </div>
                    </div>
                </div>
                               
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sub Categories</h2>
                        <a class="btn btn-warning btn-default pull-right" href="<?php echo View::url('mediamanager/addsubcategory/'.$cat->FileCategoryID); ?>">Add Sub Category</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form method="post" action="">
                        <input type="hidden" name="action" value="savesubcategoryorder" />
                        <table id="datatable-responsive" class="table table-striped default-table table-bordered dt-responsive bulk_action nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr class="headings">
                                    <th width="30" class="text-center">Order</th>
                                    <th>Sub Category Name</th>
                                    <th>Sub Category Description</th>
                                    <th class="text-center">Parent Category</th>
                                    <th width="200" class="no-sorting text-center"><?php echo Lang::get('PRD_EDIT_ACTN'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cntr = 0;
                                if(count($catsubs)) {
                                foreach($catsubs as $catsub) { $cntr++;
                                ?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer catsubID-<?php echo $catsub->FileSubCategoryID; ?>">
                                    <td class="text-center">
                                    <input class="form-control orderinput" type="number" name="order[<?php echo $catsub->FileSubCategoryID; ?>]" value="<?php echo $catsub->Order; ?>"></td>
                                    <td><?php echo ($catsub->FileSubCategoryName) ? $catsub->FileSubCategoryName : "---"; ?></td>
                                    <td><?php echo ($catsub->FileSubCategoryDescription) ? $catsub->FileSubCategoryDescription : "---"; ?></td>
                                    <td class="text-center"><?php echo ($catsub->ParentCatName) ? $catsub->ParentCatName : "---"; ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo View::url('mediamanager/editsubcategory/'.$catsub->FileSubCategoryID.'/'.$cat->FileCategoryID); ?>" title="Edit" class="green">Edit</a>
                                        <?php if(User::can('Delete Media')) { ?>
                                        &nbsp;|&nbsp;<a href="<?php echo View::url('mediamanager/deletesubcategory/'.$catsub->FileSubCategoryID.'/'.$cat->FileCategoryID); ?>" title="Delete" onclick="return confirm('Are you sure you want to delete <?php echo ($catsub->FileSubCategoryName) ? $catsub->FileSubCategoryName : "---"; ?>?');" class="red">Delete</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } 
                                } else {?>
                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                    <!--td class="a-center"><input type="checkbox" name="table_records" value="<?php //echo $user->ProductID; ?>" class="flat"></td-->
                                    <td class="text-center" colspan="5">No Data</td>
                                </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                        <input type="submit" name="SaveOrder" class="btn btn-success" value="Save Order" />
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</div>
<!-- /page content -->
<?php View::footer(); ?>