<?php $userinfo = User::info(); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $favicon = View::common()->getUploadedFiles(Option::get('favicon')); ?>
        <link rel="icon" href="<?php echo isset($favicon[0]->FileSlug) ? View::url('assets/files').$favicon[0]->FileSlug : View::url('assets/images/')."plus.png"; ?>" type="image/png" sizes="16x16">
        <title><?php echo View::$title . ' | '. Option::get('site_title',Config::get('SITE_TITLE')); ?></title>

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css">
        <?php View::headers(); ?>
        <?php View::template('jslanguage'); ?> 
    </head>

    <body class="<?php echo View::$bodyclass; ?>"  onload="window.print();">
        <div class="wrapper">

  