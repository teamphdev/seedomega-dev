
            <footer>
                <div class="container footer-links">
                  <div class="row">
                    <div class="col-lg-4 footer-about">

                      <?php View::image('omegalogo.png','SeedOmega',true)?>
                      <p>We are a group of investors who have decades of combined experienced consulting and investing into companies going for IPO. And we want to share the same kind of experiences with like-minded individuals who wants to have a share of pie as well.</p>
                      
                    </div>
                    <div class="col-lg-2 col-sm-6 col-xs-12 links">
                      <h5>Menu</h5>
                      <ul>
                        <li><a href="<?php echo View::url();?>">Home</a></li>                        
                        <li><a href="<?php echo View::url('faq');?>">FAQ</a></li>
                        <li><a href="<?php echo View::url('blogs');?>">Blogs</a></li>
                        <li><a href="<?php echo View::url('contactus');?>">Contact Us</a></li>
                        
                      </ul>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-xs-12 links">
                      <h5>Important Links</h5>
                      <ul>
                        <li><a href="<?php echo View::url('projects');?>">Currently Funding</a></li>
                        <li><a href="<?php echo View::url('ipoplacement');?>">IPO & Placements</a></li>
                        <li><a href="<?php echo View::url('seeders');?>">Seeders</a></li>
                      </ul>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-xs-12 links">
                      <h5>About SeedOmega</h5>
                      <ul>
                        <li><a href="<?php echo View::url('aboutus');?>">About Us</a></li>
                        <li><a href="<?php echo View::url('ourteam');?>">Our Team</a></li>
                        <li><a href="<?php echo View::url('privacypolicy');?>">Privacy Policy</a></li>
                      </ul>
                    </div>
                    <div class="col-lg-2 col-sm-6 col-xs-12 links">
                      <h5>Contact Info</h5>
                      <ul class="contact-info">
                        <li><i class="fa fa-map-marker"></i> <span>&nbsp;&nbsp;18 Marina Boulevard<br />#35-08<br />Singapore 018980</span></li>
                        <li><i class="fa fa-phone"></i> <span>+65 6651 4886</span></li>
                        <li><i class="fa fa-envelope-o"></i> <span>info@seedomega.com</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="copyrights">
                  <div class="container">
                    <div class="row">
                      <div class="col-lg-7 copyright-text">
                        <?php echo Lang::get('FOOT_COPYRIGHT'); ?> &copy <?php echo date('Y'); ?> <a href="<?php echo Config::get('SITE_URI'); ?>"><?php echo Config::get('SITE_TITLE'); ?></a> All rights reserved</span>
                      </div>
                      <div class="col-lg-5 social-media">
                        <ul>
                          <li title="Facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                          <li title="Twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                          <li title="Youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                          <li title="LinkedIn"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                          <li title="Google+"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                      </div>
                      <!--div class="col-lg-6 newsletter">
                        Subscribe for our newsletter
                        <form action="" method="post" onsubmit="return false;">
                          <input type="text" name="email" id="email-id" value="Enter your e-mail" />
                          <button type="submit"><i class="fa fa-check"></i></button>
                        </form>
                      </div-->
                    </div>
                  </div>
                </div>
            </footer>
        </div>

        <!-- Slide Up Modal -->
        <div class="modal fade" id="emailcheck-modal-slideup" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-slideup">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Notice</h3>
                        </div>
                        <div class="block-content"></div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Slide Up Modal -->
        
        <?php View::footers(); ?>
    </body>
</html>