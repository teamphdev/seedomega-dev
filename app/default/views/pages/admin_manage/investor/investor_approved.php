<?php
View::$title = 'Seeders';
View::$bodyclass = 'dev';
View::header();
?>
    <?php View::template('admin_manage/top'); ?>

    <?php
    $IncompleteCount = isset($InvestorsCounts['Incomplete']) && (count($InvestorsCounts['Incomplete']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Incomplete']).'<span>' : '';
    $ApprovedCount = (count($InvestorsCounts['Approved']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Approved']).'<span>' : '';
    $VerificationCount = (count($InvestorsCounts['Verification']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Verification']).'<span>' : '';
    ?>
    <div class="breadcrumb-nav">
        <ul class="nav nav-tabs container">
            <li class=""><a href="<?php echo View::url('manage/investors/'); ?>">Dashboard</a></li>  
            <li class=""><a href="<?php echo View::url('manage/investors/verification'); ?>">Verification <?php echo $VerificationCount; ?></a></li>            
            <li class="active"><a href="<?php echo View::url('manage/investors/approved'); ?>">Approved <?php echo $ApprovedCount; ?></a></li>
            <li class=""><a href="<?php echo View::url('manage/investors/incomplete'); ?>">Incomplete <?php echo $IncompleteCount; ?></a></li>            
            <li class=""><a href="<?php echo View::url('users/signup/'); ?>" class="">Create New Investor</a></li>
        </ul>
    </div>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container">
            
            <div class="">
                <?php /* <div class="row push-20" style="margin-top:-20px;">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul class="list-inline">
                            <li class="push-20-r"><span class="fa fa-address-card-o fa-1x text-success push-5-r" data-toggle="tooltip" title=""></span>ID Photo</li>
                            <li><span class="si si-pointer fa-1x text-success push-5-r" data-toggle="tooltip" title=""></span>Address Photo</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <a href="<?php echo View::url('manage/investors/trashbin'); ?>" class="btn btn-sm btn-warning pull-right"><i class="fa fa-trash"></i> Trash Bin</a>
                    </div>
                </div> */ ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="block block-opt-refresh-icon4">                            
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title">Approved</h3>
                                <p class="text-muted push-0 push-10-t">All approved investor profiles.</p>
                            </div>
                            <div class="">
                                <table class="fold-table table table-divide js-dataTable-full-pagination table-boderless dt-responsive table-hover table-vcenter dataTable no-footer dtr-inline" width="100%" addbutton='<a class="btn btn-rounded btn-warning text-uppercase" href="<?php echo View::url('manage/investors/trashbin/'); ?>"><i class="fa fa-trash push-5-r"></i> Trash Bin</a>'>
                                    <thead class="headings">
                                        <th class="text-center" width="70">Photo</th>
                                        <th class="text-center">#ID</th>
                                        <th>Name</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if(isset($InvestorsCounts['Approved'])){
                                            foreach ($InvestorsCounts['Approved'] as $investor) :
                                                $IdPhotoFile = View::common()->getUploadedFiles( $investor->IdPhoto );
                                                $IdPhotoSlug = isset( $IdPhotoFile[0]->FileSlug ) ? $IdPhotoFile[0]->FileSlug : false;
                                                $IdPhotoSlug = $IdPhotoSlug ? View::asset( 'files'.$IdPhotoSlug ) : '';

                                                $AddressPhotoFile = View::common()->getUploadedFiles( $investor->AddressPhoto );
                                                $AddressPhotoSlug = isset( $AddressPhotoFile[0]->FileSlug ) ? $AddressPhotoFile[0]->FileSlug : false;
                                                $AddressPhotoSlug = $AddressPhotoSlug ? View::asset( 'files'.$AddressPhotoSlug ) : '';

                                                switch( $investor->Status ){
                                                    case 'Incomplete':
                                                        $statustd = '<span class="label label-danger">'.$investor->Status.'</span>';
                                                        break;
                                                    case 'Approved':
                                                        $statustd = '<span class="label label-success">'.$investor->Status.'</span>';
                                                        break;
                                                    case 'Verification':
                                                        $statustd = '<span class="label label-info">'.$investor->Status.'</span>';
                                                        break;
                                                    
                                                    default:
                                                        $statustd = '<span class="label label-danger">No Account</span>';
                                                        break;
                                                }
                                            ?>
                                            <tr class="">
                                                <?php $avatar = View::common()->getUploadedFiles( $investor->Avatar ); ?>
                                                <td class="text-center hidden-xs" style="width:70px;"><?php echo View::photo( ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' ),false,'img-avatar',false,'height:30px;width:30px;'); ?>
                                                </td>
                                                <td class="text-center hidden-xs" style="width: 120px;">
                                                    <a href="<?php echo View::url( 'users/profile/documents/'.$investor->UserID ); ?>"><strong>#<?php echo $investor->UserID;?></strong></a>
                                                </td>
                                                <td class="">
                                                    <a href="javascript:void(0);">
                                                        <?php echo isset($investor->FirstName) ? $investor->FirstName : '-'; ?> <?php echo isset($investor->LastName) ? $investor->LastName : '-'; ?><br>
                                                        <small class="visible-xs">#<?php echo $investor->UserID;?></small>
                                                    </a>
                                                    <div class="visible-xs">
                                                        <div class="fold-content">
                                                            <table class="table dt-responsive" style="overflow: scroll; background: transparent;">
                                                                <tr>
                                                                    <td style="min-width: 70px; background: transparent;"><small class="text-muted">Status : </small></td>
                                                                    <td><?php echo $statustd; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><small class="text-muted">Email : </small></td>
                                                                    <td><?php echo isset($investor->Email) ? $investor->Email : ''; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><small class="text-muted">Docs : </small></td>
                                                                    <td>
                                                                        <?php if($investor->IdPhoto > 0 && $investor->IdPhoto != '') { ?>
                                                                            <span class="fa fa-address-card-o fa-1x text-success push-10-r" data-toggle="tooltip" title="ID Photo"></span>
                                                                        <?php } 
                                                                        if($investor->AddressPhoto > 0 && $investor->AddressPhoto != '') { ?>
                                                                            <span class="si si-pointer fa-1x text-success" data-toggle="tooltip" title="Address Photo"></span>
                                                                        <?php } ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><small class="text-muted">Action : </small></td>
                                                                    <td>
                                                                        <a href="<?php echo View::url('users/profile/documents/'.$investor->UserID); ?>" title="Edit" class="btn btn-sm btn-default" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                                                        <a href="<?php echo View::url('manage/trash/'.$investor->UserID.'/investors/approved'); ?>" title="Delete" class="btn btn-sm btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to put this to trash bin?');"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                </td>                                            
                                                <td class="hidden-xs hidden-sm text-center">
                                                    <?php echo isset( $investor->AccountType ) ? $investor->AccountType : ''; ?>
                                                </td>
                                                <td class="hidden-xs text-center">
                                                    <?php if( $investor->IdPhoto > 0 && $investor->IdPhoto != '' ){ ?>
                                                        <span class="fa fa-address-card-o fa-2x text-success push-10-r" data-toggle="tooltip" title="ID Photo"></span>
                                                    <?php } 
                                                    if( $investor->AddressPhoto > 0 && $investor->AddressPhoto != '' ){ ?>
                                                        <span class="si si-pointer fa-2x text-success" data-toggle="tooltip" title="Address Photo"></span>
                                                    <?php } ?>
                                                </td>
                                                <td class="text-center hidden-xs" style="width: 150px;">
                                                    <div class="">
                                                        <div class="dropdown more-opt">
                                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="<?php echo View::url( 'users/profile/documents/'.$investor->UserID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                                <li><a href="<?php echo View::url( 'manage/trash/'.$investor->UserID.'/investors/approved' ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to put seeder <?php echo $investor->FirstName; ?> <?php echo $investor->LastName; ?> to trash bin?' );"><i class="fa fa-trash pull-right"></i> Trash</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php }else{ ?>
                                            <tr>
                                                <td colspan="6"><strong>No Data</strong></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $(".fold-table tr").on("click", function(){
            $(".fold-table tr").not(this).removeClass('open');
            $(this).toggleClass("open");
            // $(this).toggleClass("open").find(".fold-content").toggleClass("open");
        });

    });
</script>