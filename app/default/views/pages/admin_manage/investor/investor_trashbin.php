<?php
View::$title = 'Seeders Trash';
View::$bodyclass = 'dev';
View::header();
?>
    <?php View::template('admin_manage/top'); ?>

    <?php
    $IncompleteCount = isset($InvestorsCounts['Incomplete']) && (count($InvestorsCounts['Incomplete']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Incomplete']).'<span>' : '';
    $ApprovedCount = (count($InvestorsCounts['Approved']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Approved']).'<span>' : '';
    $VerificationCount = (count($InvestorsCounts['Verification']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Verification']).'<span>' : '';
    ?>
    <div class="breadcrumb-nav">
        <ul class="nav nav-tabs container">
            <li class=""><a href="<?php echo View::url('manage/investors/'); ?>">Dashboard</a></li>  
            <li class=""><a href="<?php echo View::url('manage/investors/verification'); ?>">Verification <?php echo $VerificationCount; ?></a></li>            
            <li class=""><a href="<?php echo View::url('manage/investors/approved'); ?>">Approved <?php echo $ApprovedCount; ?></a></li>
            <li class=""><a href="<?php echo View::url('manage/investors/incomplete'); ?>">Incomplete <?php echo $IncompleteCount; ?></a></li>            
            <li class=""><a href="<?php echo View::url('users/signup/'); ?>" class="">Create New Investor</a></li>
        </ul>
    </div>

    <!-- ************************ Page Content ************************ -->

    <section class="gray">
        <article class="container">
            
            <form method="post" action="">
            <input type="hidden" name="doemptytrashbin">

            <div class="">
                <?php /* <div class="row push-20" style="margin-top:-20px;">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <ul class="list-inline">
                            <li class="push-20-r"><span class="fa fa-address-card-o fa-1x text-success push-5-r" data-toggle="tooltip" title=""></span>ID Photo</li>
                            <li><span class="si si-pointer fa-1x text-success push-5-r" data-toggle="tooltip" title=""></span>Address Photo</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                        <button type="submit" class="btn btn-sm btn-danger pull-right" onclick="return confirm('Are you sure you want to delete trashed users permanently? If you continue you will be able to undo this action!');">Empty Trash Bin</button>
                    </div>
                </div> */ ?>
                
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="block block-opt-refresh-icon4">                            
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title">Trash Bin</h3>
                                <p class="text-muted push-0 push-10-t">All investor profiles.</p>                                
                            </div>
                            <div class="">
                                <table class="fold-table table table-divide dataTable-cleanfilter table-boderless dt-responsive table-hover table-vcenter dataTable no-footer dtr-inline" width="100%">
                                    <thead class="headings">
                                        <th class="text-center" width="70">Photo</th>
                                        <th class="text-center">#ID</th>
                                        <th>Name</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if(count($allusers)){
                                            foreach ($allusers as $investor) :
                                                $IdPhotoFile = View::common()->getUploadedFiles($investor->IdPhoto);
                                                $IdPhotoSlug = isset( $IdPhotoFile[0]->FileSlug ) ? $IdPhotoFile[0]->FileSlug : false;
                                                $IdPhotoSlug = $IdPhotoSlug ? View::url( 'assets/files'.$IdPhotoSlug ) : '';

                                                $AddressPhotoFile = View::common()->getUploadedFiles($investor->AddressPhoto);
                                                $AddressPhotoSlug = isset( $AddressPhotoFile[0]->FileSlug ) ? $AddressPhotoFile[0]->FileSlug : false;
                                                $AddressPhotoSlug = $AddressPhotoSlug ? View::url( 'assets/files'.$AddressPhotoSlug ) : '';

                                                switch ($investor->Status) {
                                                    case 'Incomplete':
                                                        $statustd = '<span class="label label-danger">'.$investor->Status.'</span>';
                                                        break;
                                                    case 'Approved':
                                                        $statustd = '<span class="label label-success">'.$investor->Status.'</span>';
                                                        break;
                                                    case 'Verification':
                                                        $statustd = '<span class="label label-info">'.$investor->Status.'</span>';
                                                        break;
                                                    
                                                    default:
                                                        $statustd = '<span class="label label-danger">No Account</span>';
                                                        break;
                                                }
                                            ?>
                                            <tr class="">
                                                <?php $avatar = View::common()->getUploadedFiles($investor->Avatar); ?>
                                                <td class="text-center hidden-xs" style="width:70px;"><?php echo View::photo( ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' ),false,'img-avatar',false,'height:30px;width:30px;'); ?>
                                                </td>
                                                <td class="text-center hidden-xs" style="width: 120px;">
                                                    <a href="<?php echo View::url('users/profile/'.$investor->UserID); ?>"><strong>#<?php echo $investor->UserID;?></strong></a>
                                                </td>
                                                <td class="">
                                                    <a href="javascript:void(0);">
                                                        <?php echo isset($investor->FirstName) ? $investor->FirstName : '-'; ?> <?php echo isset($investor->LastName) ? $investor->LastName : '-'; ?><br>
                                                        <small class="visible-xs">#<?php echo $investor->UserID;?></small>
                                                        <input name="uids[]" value="<?php echo $investor->UserID;?>" type="hidden">
                                                    </a>
                                                    <div class="visible-xs">
                                                        <div class="fold-content">
                                                            <table class="table dt-responsive" style="overflow: scroll; background: transparent;">
                                                                <tr>
                                                                    <td style="min-width: 70px; background: transparent;"><small class="text-muted">Status : </small></td>
                                                                    <td><?php echo $statustd; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><small class="text-muted">Email : </small></td>
                                                                    <td><?php echo isset($investor->Email) ? $investor->Email : ''; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><small class="text-muted">Docs : </small></td>
                                                                    <td>
                                                                        <?php if($investor->IdPhoto > 0 && $investor->IdPhoto != '') { ?>
                                                                            <span class="fa fa-address-card-o fa-1x text-success push-10-r" data-toggle="tooltip" title="ID Photo"></span>
                                                                        <?php } 
                                                                        if($investor->AddressPhoto > 0 && $investor->AddressPhoto != '') { ?>
                                                                            <span class="si si-pointer fa-1x text-success" data-toggle="tooltip" title="Address Photo"></span>
                                                                        <?php } ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><small class="text-muted">Action : </small></td>
                                                                    <td>
                                                                        <a href="<?php echo View::url('users/profile/documents/'.$investor->UserID); ?>" title="Edit" class="btn btn-sm btn-default" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                                                        <a href="<?php echo View::url('manage/investortrashbin/'.$investor->UserID); ?>" title="Delete" class="btn btn-sm btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to put this to trash bin?');"><i class="fa fa-trash"></i></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                </td>
                                                <td class="hidden-xs hidden-sm text-center">
                                                    <?php echo isset($investor->AccountType) ? $investor->AccountType : ''; ?>
                                                </td>
                                                <td class="hidden-xs text-center">
                                                    <?php if($investor->IdPhoto > 0 && $investor->IdPhoto != '') { ?>
                                                        <span class="fa fa-address-card-o fa-2x text-success push-10-r" data-toggle="tooltip" title="ID Photo"></span>
                                                    <?php } 
                                                    if($investor->AddressPhoto > 0 && $investor->AddressPhoto != '') { ?>
                                                        <span class="si si-pointer fa-2x text-success" data-toggle="tooltip" title="Address Photo"></span>
                                                    <?php } ?>
                                                    <?php if($investor->AddressPhoto == 0 && $investor->IdPhoto == 0){ echo $statustd; } ?>
                                                </td>
                                                <td class="text-center hidden-xs" style="width: 150px;">
                                                    <div class="">
                                                        <div class="dropdown more-opt">
                                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="<?php echo View::url( 'manage/restore/'.$investor->UserID.'/investors/trashbin' ); ?>" title="" data-toggle="tooltip"><i class="fa fa-undo pull-right"></i> Restore</a></li>
                                                                <li><a href="<?php echo View::url( 'manage/delete/'.$investor->UserID.'/investors/trashbin' ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to permanently delete seeder <?php echo $investor->FirstName; ?> <?php echo $investor->LastName; ?> to trash bin?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php }else{ ?>
                                            <tr>
                                                <td>No Data</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>

                                <!-- <table class="table table-hover table-vcenter  js-table-checkable js-dataTable-full-pagination dataTable no-footer" style="width: 100%;" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label=" Investor: activate to sort column descending" style="width: 564px;"><i class="fa fa-suitcase text-gray"></i> Investor</th>
                                            <th class="text-center hidden-xs sorting" style="width: 124px;" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Status: activate to sort column ascending"><i class="fa fa-ticket text-gray"></i> Status</th>
                                            <th class="text-center hidden-xs hidden-sm sorting" style="width: 124px;" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label=" Investment: activate to sort column ascending"><i class="fa fa-money text-gray"></i> Investment</th>
                                            <th class="text-center sorting" style="width: 125px; min-width: 110px;" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-0">
                                                    <a class="link-effect" href="/bookings/info/21020">3D Printer </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>Crowdfunding <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$200000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$200,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$500,000.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21020" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21020" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="even">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21021">EyeTech </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>Equity Crowdfunding <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$7,000,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$0 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$5.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21021" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21021" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21012">GreenLife </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>Crowdfunding <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$7,000,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$10,000,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$5.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21012" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21012" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="even">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21022">Grid Works Architecture </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span> <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$7,000,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$0 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$5.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21022" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21022" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21014">MonkMakes Protoboard </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>Crowdfunding <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$3,000,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$4,000,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$2.50 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21014" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21014" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="even">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21023">Pet Grandma </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-warning"><i class="si si-clock"></i> </span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>IPO <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$340,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$600,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$150.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-warning"><i class="si si-clock"></i> </span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21023" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21023" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21018">RoVa4D Color Blender 3D Printer </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-warning"><i class="si si-clock"></i> Pending</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>Crowdfunding <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$100000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$100,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$500,000.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-warning"><i class="si si-clock"></i> Pending</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21018" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21018" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="even">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21013">Team ZWATT Bike Power Meter </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-warning"><i class="si si-clock"></i> Pending</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>IPO <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$9,000,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$9,000,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$2.00 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-warning"><i class="si si-clock"></i> Pending</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21013" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21013" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/bookings/info/21015">World’s First Jet Engine </a>
                                                </h3>
                                                <div class="push-10 visible-xs">
                                                    <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                        
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>Crowdfunding <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$50,000 <br><small class="text-muted">SIZE OF OFFER</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$100,000 <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span>$2.50 <br><small class="text-muted">PRICE</small></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <span class="label label-success"><i class="fa fa-check"></i> Approved</span>                                    
                                            </td>
                                            <td class="h4 text-center text-primary">
                                                - <br>
                                                <div class="push-5-t visible-xs">
                                                    <div class="btn-group">
                                                        <a href="/cs/clientprofiles/view/21015" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="h5 text-primary text-center hidden-xs">
                                                <div class="btn-group">
                                                    <a href="/cs/clientprofiles/view/21015" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="View Details"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table> -->


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            </form>


        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $(".fold-table tr").on("click", function(){
            $(".fold-table tr").not(this).removeClass('open');
            $(this).toggleClass("open");
            // $(this).toggleClass("open").find(".fold-content").toggleClass("open");
        });

    });
</script>