<?php
View::$title = 'Seeders';
View::$bodyclass = 'dev';
View::header();
?>
    <?php View::template('admin_manage/top'); ?>

    <?php
    $IncompleteCount = isset($InvestorsCounts['Incomplete']) && (count($InvestorsCounts['Incomplete']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Incomplete']).'<span>' : '';
    $ApprovedCount = (count($InvestorsCounts['Approved']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Approved']).'<span>' : '';
    $VerificationCount = (count($InvestorsCounts['Verification']) > 0) ? '<span class="badge">'.count($InvestorsCounts['Verification']).'<span>' : '';
    ?>
    <div class="breadcrumb-nav">
        <ul class="nav nav-tabs container">
            <li class="active"><a href="<?php echo View::url('manage/investors/'); ?>">Dashboard</a></li>  
            <li class=""><a href="<?php echo View::url('manage/investors/verification'); ?>">Verification <?php echo $VerificationCount; ?></a></li>            
            <li class=""><a href="<?php echo View::url('manage/investors/approved'); ?>">Approved <?php echo $ApprovedCount; ?></a></li>
            <li class=""><a href="<?php echo View::url('manage/investors/incomplete'); ?>">Incomplete <?php echo $IncompleteCount; ?></a></li>            
            <li class=""><a href="<?php echo View::url('users/signup'); ?>" class="">Create New Investor</a></li>
        </ul>
    </div>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container">
            <?php 
            $totalIncomplete = isset($InvestorsCounts['Incomplete']) ? count($InvestorsCounts['Incomplete']) : '0';
            $totalApproved = isset($InvestorsCounts['Approved']) ? count($InvestorsCounts['Approved']) : '0';
            $totalVerification = isset($InvestorsCounts['Verification']) ? count($InvestorsCounts['Verification']) : '0';
            $totalInvtrs = $totalIncomplete+$totalApproved+$totalVerification;
            ?>
            <div class="">
                <div class="row">
                    <div class="col-xs-6 col-lg-3">
                        <a class="block block-link-hover1" href="<?php echo View::url('manage/investors/invtslist'); ?>">
                            <div class="block-content block-content-full clearfix">
                                <div class="pull-right push-15-t push-15">
                                    <i class="fa fa-users fa-2x text-primary"></i>
                                </div>
                                <div class="h2 text-primary">
                                    <?php echo $totalInvtrs; ?>
                                    </div>
                                <div class="text-uppercase font-w600 font-s12 text-muted">All Seeders</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-lg-3">
                        <a class="block block-link-hover1" href="<?php echo View::url('manage/investors/approved'); ?>">
                            <div class="block-content block-content-full clearfix">
                                <div class="pull-right push-15-t push-15">
                                    <i class="si si-user-following fa-2x text-success"></i>
                                </div>
                                <div class="h2 text-success"><?php echo $totalApproved; ?></div>
                                <div class="text-uppercase font-w600 font-s12 text-muted">Approved</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-lg-3">
                        <a class="block block-link-hover1" href="<?php echo View::url('manage/investors/verification'); ?>">
                            <div class="block-content block-content-full clearfix">
                                <div class="pull-right push-15-t push-15">
                                    <i class="si si-user-follow fa-2x text-info"></i>
                                </div>
                                <div class="h2 text-info"><?php echo $totalVerification; ?></div>
                                <div class="text-uppercase font-w600 font-s12 text-muted">Verification</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-6 col-lg-3">
                        <a class="block block-link-hover1" href="<?php echo View::url('manage/investors/incomplete'); ?>">
                            <div class="block-content block-content-full clearfix">
                                <div class="pull-right push-15-t push-15">
                                    <i class="si si-user-unfollow fa-2x text-danger"></i>
                                </div>
                                <div class="h2 text-danger"><?php echo $totalIncomplete; ?></div>
                                <div class="text-uppercase font-w600 font-s12 text-muted">Incomplete</div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="block block-opt-refresh-icon4">
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title">Recently Added Seeders</h3>
                            </div>
                            <div class="">
                                <table class="table table-divide table-borderless table-vcenter">
                                    <tbody>
                                        <?php 
                                        if(count($allinvestors)){
                                            foreach ($allinvestors as $investor) :
                                                switch ($investor->Status) {
                                                    case 'Incomplete':
                                                        $statustd = '<span class="label label-danger">'.$investor->Status.'</span>';
                                                        break;
                                                    case 'Approved':
                                                        $statustd = '<span class="label label-success">'.$investor->Status.'</span>';
                                                        break;
                                                    case 'Verification':
                                                        $statustd = '<span class="label label-info">'.$investor->Status.'</span>';
                                                        break;
                                                    
                                                    default:
                                                        $statustd = '<span class="label label-danger">No Account</span>';
                                                        break;
                                                }
                                            ?>
                                            <tr>
                                                <?php $avatar = View::common()->getUploadedFiles( $investor->Avatar ); ?>
                                                <td class="text-center hidden-xs" style="width:70px;"><?php echo View::photo( ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' ),false,'img-avatar',false,'height:30px;width:30px;'); ?>
                                                </td>
                                                <td class="text-center" style="width: 120px;"><a href="<?php echo View::url('users/profile/documents/'.$investor->UserID); ?>"><strong>USR-<?php echo $investor->UserID;?></strong></a></td>
                                                <td class="hidden-xs"><a href="javascript:void(0);"><?php echo isset($investor->FirstName) ? $investor->FirstName : '-'; ?> <?php echo isset($investor->LastName) ? $investor->LastName : '-'; ?></a></td>
                                                <td class="hidden-xs text-left"><?php echo isset($investor->AccountType) ? $investor->AccountType : ''; ?></td>
                                                <td class="text-left" style="width: 150px;"><?php echo $statustd; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php }else{ ?>
                                            <tr>
                                                <td colspan="5"><strong>No Data</strong></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>

<!-- <script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });

    });
</script> -->