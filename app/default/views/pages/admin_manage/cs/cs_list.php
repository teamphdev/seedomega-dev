<?php
View::$title = 'Hub Care';
View::$bodyclass = ' dev';
View::header();
View::template('admin_manage/top');
?>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container">
            
            <div class="">
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="block block-opt-refresh-icon4">                            
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title"><?php echo View::$title; ?></h3>
                                <p class="text-muted push-0 push-10-t">All customer service who can manage clients & investor profiles.</p>
                            </div>
                            <div class="">
                                <?php echo View::getMessage(); ?>
                                <table class="fold-table table table-divide js-dataTable-full-pagination table-boderless dt-responsive table-hover table-vcenter dataTable no-footer dtr-inline" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('manage/cs/add'); ?>"><i class="si si-user-follow push-5-r"></i> Create New</a>'>
                                    <thead class="headings">
                                        <th class="text-center" width="70">Photo</th>
                                        <th class="text-center">#ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th class="text-center">Action</th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if(isset($users['Customer Service'])){
                                            foreach ($users['Customer Service'] as $data) :
                                            ?>
                                            <tr class="">
                                                <?php $avatar = View::common()->getUploadedFiles($data->Avatar); ?>
                                                <td class="text-center hidden-xs" style="width:70px;"><?php echo View::photo( ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' ),false,'img-avatar',false,'height:30px;width:30px;'); ?></td>
                                                <td class="text-center hidden-xs" style="width: 120px;">
                                                    <a href="<?php echo View::url('manage/cs/edit/'.$data->UserID); ?>"><strong>#<?php echo $data->UserID;?></strong></a>
                                                </td>
                                                <td class="">
                                                    <a href="javascript:void(0);">
                                                        <?php echo isset($data->FirstName) ? $data->FirstName : '-'; ?> <?php echo isset($data->LastName) ? $data->LastName : '-'; ?><br>
                                                        <small class="visible-xs">#<?php echo $data->UserID;?></small>
                                                    </a>
                                                </td>                                            
                                                <td class="hidden-xs hidden-sm text-left">
                                                    <?php echo isset($data->Email) ? $data->Email : ''; ?>
                                                </td>
                                                <td class="text-center hidden-xs" style="width: 150px;">
                                                    <div class="">
                                                        <div class="dropdown more-opt">
                                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="<?php echo View::url( 'manage/cs/edit/'.$data->UserID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                                <li><a href="<?php echo View::url( 'manage/delete/'.$data->UserID.'/cs' ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete user <?php echo $data->FirstName; ?> <?php echo $data->LastName; ?>?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php }else{ ?>
                                            <tr>
                                                <td colspan="5"><strong>No Data</strong></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $(".fold-table tr").on("click", function(){
            $(".fold-table tr").not(this).removeClass('open');
            $(this).toggleClass("open");
            // $(this).toggleClass("open").find(".fold-content").toggleClass("open");
        });

    });
</script>