<?php 
    View::$title = 'Edit Slider';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>
<?php $userinfo = User::info(); ?>

<?php View::page('admin_manage/head'); ?>

<section class="gray">
    <article class="container start-project">
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
        <input type="hidden" name="action" value="updateslider">
        <input type="hidden" name="UserID" value="<?php echo isset( $userinfo->UserID ) ? $userinfo->UserID : ''; ?>">
        <input type="hidden" name="SlideID" value="<?php echo isset( $slider->SlideID ) ? $slider->SlideID : '' ; ?>">
        <input type="hidden" name="SliderPhotoOld" value="<?php echo isset( $slider->ImageID ) ? $slider->ImageID : '0' ; ?>">

        <div class="block-content">
            <div class="row">
                <?php echo View::getMessage();  ?>
                <div class="col-sm-9">
                    <div class="push-30-r">

                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="Title" value="<?php echo isset( $slider->Title ) ? $slider->Title : '' ; ?>" required="">
                        </div>

                        <div class="form-group">
                            <label>Sub Title</label>
                            <input type="text" class="form-control" name="SubTitle" value="<?php echo isset( $slider->SubTitle ) ? $slider->SubTitle : '' ; ?>" required="">
                        </div>

                        <div class="form-group">
                            <label>Slider Image</label><br>
                            <div class="gray text-center pad-20 pad-20-t pad-20-r pad-20-l min-height-200">
                                <?php $image = View::common()->getUploadedFiles( $slider->ImageID );
                                echo View::photo( ( isset($image[0] ) ? 'files'.$image[0]->FileSlug : '' ), "Image", false, false, "width: 86%;" ); ?>
                            </div>
                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="SliderPhoto" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                        </div>

                        <div class="form-group">
                            <label for="Description">Description</label>
                            <textarea class="form-control tinyMCE" name="Description" cols="30" rows="10"><?php echo isset( $slider->Description ) ? $slider->Description : '' ; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="form-group">
                        <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?></label>
                        <input type="text" class="form-control" value="<?php echo isset( $slider->CompanyName ) ? $slider->CompanyName : '' ; ?>" readonly="">
                    </div>

                    <div class="form-group">
                        <label>Display?</label>
                        <?php
                            $statusOptions = array( 0 => 'Hidden', 1 => 'Show' );
                            $selected = isset( $slider->Status ) ? $slider->Status : 0 ;
                            View::form( 'selecta', array('name' => 'Status', 'value' => $selected, 'options'=>$statusOptions, 'class' => 'form-control' ) );
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Date Published</label>
                        <input type="text" class="form-control" value="<?php echo isset( $slider->Date ) ? $slider->Date : '' ; ?>" readonly="">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-rounded btn-primary" style="min-width: 120px;">Save Changes</button>
                    </div>
                    
                </div>
            </div>
            
        </div>            

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>