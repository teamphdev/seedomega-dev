<?php 
    View::$title = 'Add Slider';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>
<?php $userinfo = User::info(); ?>

<?php View::page('admin_manage/head'); ?>

<section class="gray">
    <article class="container start-project">
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
        <input type="hidden" name="action" value="addslider">
        <input type="hidden" name="UserID" value="<?php echo isset( $userinfo->UserID ) ? $userinfo->UserID : ''; ?>">
        <input type="hidden" name="ClientProfileID" value="" id="ClientProfileID">

        <div class="block-content">
            <div class="row">
                <?php echo View::getMessage();  ?>
                <div class="col-sm-9">
                    <div class="push-30-r">

                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" id="Title" name="Title" value="" required="">
                        </div>

                        <div class="form-group">
                            <label>Sub Title</label>                        
                            <input type="text" class="form-control" id="SubTitle" name="SubTitle" value="" required="">
                        </div>

                        <div class="form-group">
                            <label>Slider Image</label>
                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="SliderPhoto" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                        </div>

                        <div class="form-group">
                            <label for="Description">Description</label>
                            <textarea class="form-control tinyMCE" name="Description" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="form-group">
                        <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?></label>
                        <?php View::form('selecta', array(
                            'id' => 'selCompanies',
                            'value' => '0',
                            'custom' => 'required',
                            'options' => $optionsname,
                            'class' => 'form-control col-md-7 col-xs-12'
                            ));
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Display?</label>
                        <?php
                            $statusOptions = array( 0 => 'Hidden', 1 => 'Show' );
                            View::form( 'selecta', array('name' => 'Status', 'options'=>$statusOptions, 'class' => 'form-control' ) );
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Date Published</label>
                        <input type="text" class="form-control" value="<?php echo date('d M Y'); ?> @ <?php echo date('h:i:a'); ?>" readonly="">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-rounded btn-primary" style="min-width: 120px;">Publish</button>
                    </div>
                    
                </div>
            </div>
            
        </div>            

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>