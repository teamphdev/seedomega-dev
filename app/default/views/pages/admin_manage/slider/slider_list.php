<?php 
    View::$title = 'Slider';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
    $param = array();
    $param['view'] = '';
    $userinfo = User::info();
?>
<?php View::page( 'admin_manage/head', $param ); ?>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?>
                <table id="slider-table" class="table table-divide js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-primary" href="<?php echo View::url( 'manage/slider/add' ); ?>">Add Slider</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th>ID</th>
                            <th>Company</th>
                            <th>Title</th>
                            <th class="no-wrap">Sub Title</th>
                            <th>Description</th>
                            <th class="text-center">Display</th>
                            <th class="text-center">Date</th>
                            <th class="no-sorting text-center" style="max-width:150px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        foreach( $sliders as $slide ){ $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="a-center"><input type="checkbox" name="table_records" value="<?php echo $slide->UserLevelID; ?>" class="flat"></td> -->
                            
                            <td class="text-center" style="vertical-align: top;"><?php echo $slide->SlideID; ?></td>
                            <td style="vertical-align: top;"><?php echo $slide->CompanyName; ?></td>
                            <td style="vertical-align: top;"><?php echo $slide->Title; ?></td>
                            <td style="vertical-align: top;"><?php echo $slide->SubTitle; ?></td>
                            <td style="vertical-align: top;" data-toggle="modal" data-target="#modal-form" onclick="viewDetails( '<?php echo $slide->SlideID; ?>' );">
                                <div class="hiddenMCE font-12">
                                    <?php
                                        $image = View::common()->getUploadedFiles( $slide->ImageID );
                                        $excerpt = AppUtility::excerptAsNeeded( $slide->Description, 200, '... <span class="text-info">(click to view more)</span>' );
                                        echo View::photo( ( isset( $image[0] ) ? 'files'.$image[0]->FileSlug : '' ), "Image" );
                                        echo $excerpt;
                                    ?>
                                </div>
                            </td>
                            <td class="text-center text-top"><?php echo $slide->Status; ?></td>
                            <td class="text-center text-top"><?php echo $slide->Date; ?></td>
                            <td class="text-center text-top">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'manage/slider/edit/'.$slide->SlideID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'manage/managedelete/slider/'.$slide->SlideID.'/'.$slide->ImageID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this slider?' );"><i class="fa fa-trash pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                    
                </table>

                <!-- Modal -->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="modal-title"></span></h3>
                          </div>
                          <div class="block-content">
                            <div id="modal-content" class="hiddenMCE"></div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-sm btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END Modal -->
            </div>
        </div>
    </div>
</section>
<input type="hidden" id="jteams" value="<?php echo $jdata; ?>">
<!-- /page content -->

<?php View::footer(); ?>

<script type="text/javascript">
    var datas = [];
    $( document ).ready( function(){
        $datas = $( '#jteams' ).val();
        datas = jQuery.parseJSON( $datas );

        // initialize tinyMCE on contents having .hiddenMCE class
        hiddenMCEconfig.inline = true;
        hiddenMCEconfig.readonly = 1;
        tinymce.init( hiddenMCEconfig ); // hiddenmce.js
    });

    function viewDetails( id ){
        $( '#modal-title' ).text( datas[id]['Title'] );
        $( '#modal-content' ).text( datas[id]['Desc'] );
        tinymce.get( 'modal-content' ).setContent( datas[id]['Desc'] );
    }
</script>