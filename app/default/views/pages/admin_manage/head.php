<?php
    $links = '';
    $bcrumb = '<li><span class="fa fa-home"></span>&nbsp; You are here:</li>';
    $bcrumb .= '<li><a href="'.User::dashboardLink().'">Home</a></li>';
    $segments = View::$segments;

    if( isset( $segments ) && count( $segments ) ){
        $ctr = 0;
        $count = count( $segments );
        foreach ( $segments as $segment ){
            $ctr++;
            if( $ctr == $count ){
                $bcrumb .= '<li class="fa fa-angle-right"></li>';
                $bcrumb .= '<li>'.View::$title.'</li>';
            } else {
                if( $segment != 'add' && $segment != 'edit' && $segment != 'delete' ){
                    $bcrumb .= '<li class="fa fa-angle-right"></li>';
                    if( $count == $ctr ){
                        $bcrumb .= '<li>'.View::$title.'</li>';
                    } else {
                        $links .= '/'.$segment;
                        $bcrumb .= '<li><a href="'.$links.'">'.$segment.'</a></li>';
                    }
                }
            }
        }
    }
?>

<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul><?php echo isset( $bcrumb ) ? $bcrumb : ''; ?></ul>
            </div>
            <?php if( isset( $view ) && $view == 'list' ){ ?>
            <div class="col-lg-6 align-right sub-menu block-title">
                <a class="btn btn-sm btn-primary btn-rounded" href="<?php echo $segments[ $count - 1 ] . '/add'; ?>"><?php echo 'ADD '.View::$title; ?></a>
            </div>
            <?php } ?>
        </div>
    </article>
</section>
*/ ?>