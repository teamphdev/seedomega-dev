<?php 
    View::$title = 'Edit Testimonial';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>
<?php $userinfo = User::info(); ?>

<?php View::page('admin_manage/head'); ?>

<section class="gray">
    <article class="container start-project">
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
        <input type="hidden" name="action" value="updatetestimonial">
        <input type="hidden" value="" id="<?php echo isset( $testimonial->UserID ) ? $testimonial->UserID : '' ; ?>">
        <input type="hidden" name="TestimonialID" value="<?php echo isset( $testimonial->TestimonialID ) ? $testimonial->TestimonialID : '' ; ?>">

        <div class="block-content">
            <div class="row">
                <?php echo View::getMessage();  ?>
                <div class="col-sm-9">
                    <div class="push-30-r">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" value="<?php echo isset( $testimonial->Name ) ? $testimonial->Name : '' ; ?>" readonly="">
                        </div>

                        <div class="form-group">
                            <label for="Message">Message</label>
                            <textarea class="form-control tinyMCE" name="Message" cols="30" rows="10"><?php echo isset( $testimonial->Message ) ? $testimonial->Message : '' ; ?></textarea>
                        </div>
                    </div>             
                </div>
                <div class="col-sm-3">

                    <div class="form-group">
                        <label>Company</label>
                        <input type="text" class="form-control" value="<?php echo isset( $testimonial->Company ) ? $testimonial->Company : '' ; ?>" readonly="">
                    </div>
                    <div class="form-group">
                        <label>Position</label>
                        <input type="text" class="form-control" value="<?php echo isset( $testimonial->Position ) ? $testimonial->Position : '' ; ?>" readonly="">
                    </div>

                    <div class="form-group">
                        <label>Date Published</label>
                        <input type="text" class="form-control" value="<?php echo isset( $testimonial->Date ) ? $testimonial->Date : '' ; ?>" readonly="">
                    </div>

                    <div class="form-group">
                        <label>Display?</label>
                        <?php
                            $statusOptions = array( 0 => 'Hidden', 1 => 'Show' );
                            $selected = isset( $testimonial->Status ) ? $testimonial->Status : 0 ;
                            View::form( 'selecta', array( 'name' => 'Status', 'value' => $selected, 'options' => $statusOptions, 'class' => 'form-control' ) );
                        ?>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-rounded btn-primary blog-btn" style="min-width: 120px;">Save Changes</button>
                    </div>
                    
                </div>
            </div>
            
        </div>            

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>