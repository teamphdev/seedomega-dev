<?php 
    View::$title = 'Add Testimonial';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>
<?php $userinfo = User::info(); ?>

<?php View::page('admin_manage/head'); ?>

<section class="gray">
    <article class="container start-project">
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
        <input type="hidden" name="action" value="addtestimonial">
        <input type="hidden" name="UserID" value="" id="UserID">
        <input type="hidden" name="Name" value="" id="Name">

        <div class="block-content">
            <div class="row">
                <?php echo View::getMessage();  ?>
                <div class="col-sm-9">
                    <div class="push-30-r">
                        <div class="form-group">
                            <label>Name</label>
                            <?php View::form('selecta', array(
                                'id' => 'selNames',
                                'value' => '0',
                                'custom' => 'required',
                                'options' => $optionsname,
                                'class' => 'form-control col-md-7 col-xs-12'
                                ));
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="Message">Message</label>
                            <textarea class="form-control tinyMCE" name="Message" cols="30" rows="10"></textarea>
                        </div>
                    </div>             
                </div>
                <div class="col-sm-3">

                    <div class="form-group">
                        <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?></label>                        
                        <input type="text" class="form-control" id="Company" name="Company" value="" readonly="">
                    </div>
                    <div class="form-group">
                        <label>Position</label>                        
                        <input type="text" class="form-control" id="Position" name="Position" value="" readonly="">
                    </div>

                    <div class="form-group">
                        <label>Display?</label>
                        <?php
                            $statusOptions = array( 0 => 'Hidden', 1 => 'Show' );
                            View::form( 'selecta', array('name' => 'Status', 'options'=>$statusOptions, 'class' => 'form-control' ) );
                        ?>
                    </div>

                    <div class="form-group">
                        <label>Date Published</label>
                        <input type="text" class="form-control" value="<?php echo date('d M Y'); ?> @ <?php echo date('h:i:a'); ?>" readonly="">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-rounded btn-primary blog-btn" style="min-width: 120px;">Publish</button>
                    </div>
                    
                </div>
            </div>
            
        </div>            

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>