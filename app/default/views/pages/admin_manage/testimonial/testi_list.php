<?php 
    View::$title = 'Testimonial';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
    $param = array();
    $param['view'] = '';
    $userinfo = User::info();
?>
<?php View::page('admin_manage/head', $param); ?>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block">
                <?php echo View::getMessage(); ?>
                <table id="testimonial-table" class="table table-divide js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" width="100%" addbutton='<a class="btn btn-rounded btn-primary" href="<?php echo View::url( 'manage/testimonial/add' ); ?>">Add Testimonial</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th>ID</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Position</th>
                            <th>Message</th>
                            <th class="text-center">Display</th>
                            <th class="text-center">Date</th>
                            <th class="no-sorting text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        foreach( $testimonials as $testi ){ $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="a-center"><input type="checkbox" name="table_records" value="<?php echo $testi->UserLevelID; ?>" class="flat"></td> -->
                            
                            <td class="text-center"><?php echo $testi->TestimonialID; ?></td>
                            <td><?php echo $testi->Name; ?></td>
                            <td><?php echo $testi->Company; ?></td>
                            <td><?php echo $testi->Position; ?></td>
                            <td style="vertical-align: top;" data-toggle="modal" data-target="#modal-form" onclick="viewDetails( '<?php echo $testi->TestimonialID; ?>' );">
                                <div class="hiddenMCE font-12">
                                    <?php
                                        $excerpt = AppUtility::excerptAsNeeded( $testi->Message, 200, '... <span class="text-info">(click to view more)</span>' );
                                        echo $excerpt;
                                    ?>
                                </div>
                            </td>
                            <td class="text-center"><?php echo $testi->Status; ?></td>
                            <td class="text-center"><?php echo $testi->Date; ?></td>
                            <td class="text-center">
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'manage/testimonial/edit/'.$testi->TestimonialID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'manage/managedelete/testimonial/'.$testi->TestimonialID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this testimonial?' );"><i class="fa fa-trash pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                    
                </table>

                <!-- Modal -->
                <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="modal-title"></span></h3>
                          </div>
                          <div class="block-content">
                            <div id="modal-content" class="hiddenMCE"></div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-sm btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END Modal -->
            </div>
        </div>
    </div>
</section>
<input type="hidden" id="jteams" value="<?php echo $jdata; ?>">
<!-- /page content -->

<?php View::footer(); ?>

<script type="text/javascript">
    var datas = [];
    $( document ).ready( function(){
        $datas = $( '#jteams' ).val();
        datas = jQuery.parseJSON( $datas );

        // initialize tinyMCE on contents having .hiddenMCE class
        hiddenMCEconfig.inline = true;
        hiddenMCEconfig.readonly = 1;
        tinymce.init( hiddenMCEconfig ); // hiddenmce.js
    });

    function viewDetails( id ){
        $( '#modal-title' ).text( datas[id]['FullName'] );
        $( '#modal-content' ).text( datas[id]['Message'] );
        tinymce.get( 'modal-content' ).setContent( datas[id]['Message'] );
    }
</script>