<?php 
    View::$title = 'Slider';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
    $param = array();
    $param['view'] = 'list';
    $userinfo = User::info();
?>
<?php View::page('admin_manage/head', $param); ?>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?>
                <table id="slider-table" class="table table-divide js-table-checkable js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" cellspacing="0" width="100%">
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th>ID</th>
                            <th>Company</th>
                            <th>Title</th>
                            <th>Sub Title</th>
                            <th>Description</th>
                            <th class="text-center">Display</th>
                            <th class="text-center">Date</th>
                            <th class="no-sorting text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        foreach( $sliders as $slide ){ $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <!-- <td class="a-center"><input type="checkbox" name="table_records" value="<?php echo $slide->UserLevelID; ?>" class="flat"></td> -->
                            
                            <td class="text-center" style="vertical-align: top;"><?php echo $slide->SlideID; ?></td>
                            <td style="vertical-align: top;"><?php echo $slide->CompanyName; ?></td>
                            <td style="vertical-align: top;"><?php echo $slide->Title; ?></td>
                            <td style="vertical-align: top;"><?php echo $slide->SubTitle; ?></td>
                            <td style="vertical-align: top;"><?php
                                $image = View::common()->getUploadedFiles( $slide->ImageID );
                                echo View::photo( ( isset($image[0] ) ? 'files'.$image[0]->FileSlug : '' ), "Image" );
                                echo $slide->Description; ?>
                            </td>
                            <td class="text-center" style="vertical-align: top;"><?php echo $slide->Status; ?></td>
                            <td class="text-center" style="vertical-align: top;"><?php echo $slide->Date; ?></td>
                            <td class="text-center" style="vertical-align: top;">
                                <a href="<?php echo View::url('manage/slider/edit/'.$slide->SlideID); ?>" title="Edit" class="btn btn-xs btn-default" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo View::url('manage/managedelete/slider/'.$slide->SlideID.'/'.$slide->ImageID); ?>" title="Delete" class="btn btn-xs btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete data?');"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->

<?php View::footer(); ?>