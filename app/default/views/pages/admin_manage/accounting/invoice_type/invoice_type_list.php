<?php
View::$title = 'Manage Invoice Type';
View::$bodyclass = 'dev';
View::header();
?>
    <?php View::template( 'admin_manage/top' ); ?>

    <!-- ************************ Page Content ************************ -->

    <section class="gray">
        <article class="container">
            
            <div class="">
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="block block-opt-refresh-icon4">                            
                            <div class="block-header bg-gray-lighter">
                                <h3 class="block-title">Invoice Type</h3>
                                <p class="text-muted push-0 push-10-t">All invoice types.</p>
                            </div>
                            <div class="">
                                <?php echo View::getMessage(); ?>
                                <table class="fold-table table table-divide js-dataTable-full-pagination table-boderless dt-responsive table-hover table-vcenter dataTable no-footer dtr-inline" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('manage/invoice_type/add'); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Create New</a>'>
                                    <thead class="headings">
                                        <th>Type ID</th>
                                        <th>Description</th>
                                        <th>Date Added</th>
                                        <th width="200" class="no-sorting text-center">Action</th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if( isset( $itypes ) && count( $itypes ) ){
                                            foreach( $itypes as $type ) : ?>
                                            <tr class="">
                                                <td><?php echo isset( $type->InvoiceTypeID ) ? $type->InvoiceTypeID : '0'; ?></td>
                                                <td><?php echo isset( $type->Description ) ? $type->Description : ''; ?></td>
                                                <td>
                                                    <?php echo isset( $type->DateAdded ) && $type->DateAdded != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $type->DateAdded ) ) : '-'; ?>
                                                </td>
                                                <td class="text-center hidden-xs" style="width: 150px;">
                                                    <?php if( User::can( 'Administer All' ) || User::can( 'Manage Invoices' ) ){ ?>

                                                        <div class="">
                                                            <div class="dropdown more-opt">
                                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                                <ul class="dropdown-menu dropdown-menu-right">
                                                                    <li><a href="<?php echo View::url( 'manage/invoice_type/edit/'.$type->InvoiceTypeID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                                    <li><a href="<?php echo View::url( 'manage/invoice_type/delete/'.$type->InvoiceTypeID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this type?' );"><i class="fa fa-trash pull-right"></i> Delete</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    <?php } else { ?>

                                                        <div class="">
                                                            <a href="<?php echo View::url( 'manage/invoice_type/edit/'.$type->InvoiceTypeID ); ?>" class="btn btn-sm btn-default" title="" data-toggle="tooltip">Edit</a>
                                                        </div>

                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php }else{ ?>
                                            <tr>
                                                <td colspan="4">No Data</td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                                <td class="hidden"></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $(".fold-table tr").on("click", function(){
            $(".fold-table tr").not(this).removeClass('open');
            $(this).toggleClass("open");
            // $(this).toggleClass("open").find(".fold-content").toggleClass("open");
        });

    });
</script>