<?php 
    View::$title = 'Add Client';
    View::$bodyclass = User::info('Sidebar').' add-client';
    View::header();
    $url = User::can( 'Administer All' ) ? 'manage/clients' : 'cs/clientprofiles';
?>

<section class="header-bottom no-padding">
    <article>
        <div class="container animated fadeInDown">
            <div class="start-project profile-tabs profile-tabs-4" id="profile_wizard">                
                <div class="title">
                    <ul id="profileTabs" data-toggle="tabs">
                        <li class="active">
                            <a href="#personal-info" id="personalinfo"><i class="fa fa-user"></i><span>1: Personal Info</span></a>
                        </li>
                        <li class="">
                            <a href="#company-info" id="companyinfo"><i class="fa fa-pagelines"></i><span>2: Company Info</span></a>
                        </li>
                        <li class="">
                            <a href="#offer" id="offerinfo"><i class="fa fa-link"></i><span>3: Company Offer</span></a>
                        </li>
                        <li class="">
                            <a href="#summary" id="summaryinfo"><i class="fa fa-file-image-o"></i><span>4: Summary</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
</section>

<script>
function clickMe(ID) {
    document.getElementById(ID).click();
}
</script> 
<form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
    <input type="hidden" name="action" value="addproject" />
    <input type="hidden" value="4" name="user[Level]" />

    <section class="gray">
        <article class="container project-single">
            <!-- Main Content -->
            <div class="block">
                <div class="block tab-content">
                    <?php echo View::getMessage(); ?>

                    <div class="tab-pane fade fade-up in active" id="personal-info">
                        <div class="client-profiles-section">
                            <div class="block-header">Personal Info</div>
                            <div class="block-content">

                                <?php if( User::can( 'Administer All' ) || User::can( 'Edit Clients' ) ): ?>
                                    <div class="form-group hidden">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label><?php echo Lang::get('USR_ADD_ULVL'); ?></label>
                                            <div class="">
                                                <?php 
                                                    View::form(
                                                        'selecta',
                                                        array(
                                                            'options' => $levels,
                                                            'value' => '4',
                                                            'class' => 'form-control',
                                                            'id' => 'userlevelopt',
                                                            'custom ' => 'disabled'
                                                        )
                                                    );
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="form-group">
                                    
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_ADD_PPCTURE'); ?></label>
                                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                            <span>* Allowed file types: jpeg, jpg, png</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_ADD_LN'); ?> <span class="required text-danger">*</span></label>
                                            <input type="text" value="Your surname" id="lname" name="meta[LastName]" required="required" class="form-control uppercase">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_ADD_FN'); ?> <span class="required text-danger">*</span></label>
                                            <input type="text" value="Your given name" id="fname" name="meta[FirstName]" required="required" class="form-control uppercase">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_ADD_NN'); ?></label>
                                            <input type="text" value="" id="nname" name="meta[NickName]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_ADD_EML'); ?> <span class="required text-danger">*</span></label>
                                            <input type="email" id="EmailChecker" name="user[Email]" class="form-control" value="" required="required" rel="<?php echo View::url('ajax/checkemail'); ?>" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>"><span id="emailloading" class="fa fa-refresh fa-spin"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_ADD_GNDR'); ?></label><br>
                                            <label class="css-input css-radio css-radio-info push-10-r">
                                                <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" checked required /><span></span> <?php echo Lang::get('USR_ADD_GNDRM'); ?>
                                            </label>
                                            <label class="css-input css-radio css-radio-info">
                                                <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" required /><span></span> <?php echo Lang::get('USR_ADD_GNDRF'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_ADD_PHNE'); ?> <span class="required text-danger">*</span></label>
                                            <input type="phone" value="" id="phone" name="meta[Phone]" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label><?php echo Lang::get('USR_ADD_ADDS'); ?> <span class="required text-danger">*</span></label>
                                        <input type="text" value="" id="address" name="meta[Address]" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_ADD_CTY'); ?></label>
                                            <input type="text" value="" id="city" name="meta[City]" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_ADD_STATES'); ?></label>
                                            <input type="text" value="" id="state" name="meta[State]" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('USR_ADD_CNTRY'); ?></label>
                                            <select id="Country" name="meta[Country]" class="form-control">
                                                <?php foreach( AppUtility::getCountries() as $country ){ ?>
                                                <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('USR_ADD_PCODE'); ?></label>
                                            <input type="text" value="" id="postal" name="meta[PostalCode]" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label><?php echo Lang::get('USR_ADD_RMRKS'); ?></label>
                                        <textarea class="form-control dowysiwyg" name="meta[Bio]"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content text-center pad-0-t">                       

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="#" onclick="clickMe('companyinfo');" class="btn btn-rounded btn-info back">Next</a>
                                        <button type="submit" class="btn btn-rounded btn-primary">Save Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade fade-up" id="company-info">
                        <div class="client-profiles-section">
                            <div class="block-header">Company Info</div>
                            <div class="block-content">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?> <span class="text-danger">*</span></label>
                                        <input type="text" value="My Company" id="CompanyName" name="client[CompanyName]" class="form-control" required="required">
                                    </div>
                                    <div class="parent-image">
                                        <label>Company Logo</label>
                                        <input id="file-0a" class="file form-control company-logo" type="file" data-min-file-count="0" name="CompanyLogo" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]' required="required">
                                        <span class="text-muted">Allowed file types: jpeg, jpg, png<br>Recommended image size: <b>width (<i>350px</i>) x height (<i>350px</i>)</b></span>
                                    </div>

                                    <div class="form-group">
                                        <br>
                                        <div class="parent-image">
                                            <label>Company Image</label>
                                            <input id="file-0b" class="file form-control company-photo" type="file" data-min-file-count="0" name="CompanyPhoto" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]' required="required">
                                            <span class="text-muted">Allowed file types: jpeg, jpg, png<br>Recommended image size: <b>width (<i>1900px</i>) x height (<i>450px</i>)</b></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_VIDEO'); ?><br>Required Dimension: Width = 560 pixels, Height = 315 pixels</label>
                                        <textarea class="form-control" name="client[Video]" rows="2" style="height:75px">My Video embed code</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_VIDEODESC'); ?></label>
                                        <textarea class="form-control profileMCE" name="client[VideoDescription]" id="VideoDescription">Video Description</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content text-center pad-0-t">                       

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="#" onclick="clickMe('personalinfo');" class="btn btn-rounded btn-warning back">Back</a>
                                        <a href="#" onclick="clickMe('offerinfo');" class="btn btn-rounded btn-info back">Next</a>
                                        <button type="submit" class="btn btn-rounded btn-primary">Save Project</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="tab-pane fade fade-up" id="offer">
                        <div class="client-profiles-section">
                            <div class="block-header">Company Offer</div>
                            <div class="block-content">
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label><?php echo Lang::get('CLN_PRF_TYPEOFFER'); ?> <span class="text-danger">*</span></label>
                                        <select id="TypeOfOffer" name="client[TypeOfOffer]" class="form-control" required="required">
                                            <option value="">Select</option>
                                            <option value="Open" selected>Open</option>
                                            <option value="Limited">Limited</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('CLN_PRF_OFFEROPENING'); ?> <span class="text-danger">*</span></label>
                                            <input type="text" value="<?php echo date('Y-m-d'); ?>" id="OfferOpening" name="client[OfferOpening]" class="form-control jsdate" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label><?php echo Lang::get('CLN_PRF_OFFERCLOSING'); ?> <span class="text-danger">*</span></label>
                                            <input type="text" value="<?php echo date('Y-m-d', strtotime('+5 years')); ?>" id="OfferClosing" name="client[OfferClosing]" class="form-control jsdate" required="required">
                                        </div>
                                    </div>
                                </div>

                                <?php /*
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="number" value="1.00" id="Price" name="client[Price]" class="price-input form-control" min="1.00" placeholder="1.00" required step="0.01">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('CLN_PRF_SZEOFFER'); ?> <span class="text-danger">*</span></label>
                                            <div class="">
                                                <input type="text" value="" id="SizeOfOffer" name="client[SizeOfOffer]" class="form-control transparent" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label>Target Goal</label><br>
                                            <div class="prefix">
                                                <span class="currency text-muted">$</span>
                                                <input type="number" value="1000.00" id="TargetGoal" name="client[TargetGoal]" class="form-control transparent" placeholder="1000.00" required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                */ ?>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                            <div class="prefix">
                                                <span class="currency text-muted">$</span>
                                                <input type="number" value="1.00" id="Price" name="client[Price]" class="price-input form-control" min="1" placeholder="1.00" required step="0.01">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label>Target Goal</label><br>
                                            <div class="prefix">
                                                <span class="currency text-muted">$</span>
                                                <input type="number" value="10000.00" id="TargetGoal" name="client[TargetGoal]" class="form-control transparent" placeholder="0.00" required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('CLN_PRF_MINBID'); ?> <span class="text-danger">*</span></label>
                                            <div class="prefix">
                                                <span class="currency text-muted">$</span>
                                                <input type="number" value="1.00" id="MinimumBid" name="client[MinimumBid]" class="bid-input form-control" placeholder="0.00" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label>Maximum Bid <span class="text-danger">*</span></label>
                                            <div class="prefix">
                                                <span class="currency text-muted">$</span>
                                                <input type="number" value="100.00" id="MaximumBid" name="client[MaximumBid]" class="bid-input form-control" placeholder="0.00" required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-left">
                                            <label><?php echo Lang::get('CLN_PRF_LDMNGR'); ?> <span class="text-danger">*</span></label>
                                            <input type="text" value="Manager" id="LeadManager" name="client[LeadManager]" class="form-control" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-right">
                                            <label>Website (optional)</label>
                                            <input type="text" value="" id="Website" name="client[Website]" class="form-control">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="block-content text-center pad-0-t">                       

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="#" onclick="clickMe('companyinfo');" class="btn btn-rounded btn-warning back">Back</a>
                                        <a href="#" onclick="clickMe('summaryinfo');" class="btn btn-rounded btn-info">Next</a>
                                        <button type="submit" class="btn btn-rounded btn-primary">Save Project</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="tab-pane fade fade-up" id="summary">
                        <div class="client-profiles-section">
                            <div class="block-header">Summary</div>
                            <div class="block-content">
                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_EXECSUMMARY'); ?></label>
                                        <textarea class="form-control" name="client[ExecutiveSummary]">Executive Summary</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_OFFVIEW'); ?></label>
                                        <textarea class="form-control" name="client[OfferOverview]">Offer Overview</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_BUSSMODEL'); ?></label>
                                        <textarea class="form-control" name="client[BusinessModel]">Business Model</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_KEYINVST'); ?></label>
                                        <textarea class="form-control" name="client[KeyInvestHighlights]">Key InvestHighlights</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_STRATVISION'); ?></label>
                                        <textarea class="form-control" name="client[StrategyVision]">Strategy Vision</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_MARKTDEMAND'); ?></label>
                                        <textarea class="form-control" name="client[MarketDemand]">Market Demand</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_BRDMNGT'); ?></label>
                                        <textarea class="form-control" name="client[BoardManagement]">Board Management</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_USEFUNDS'); ?></label>
                                        <textarea class="form-control" name="client[UsageOfFunds]">Usage Of Funds</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_FINSUMMRY'); ?></label>
                                        <textarea class="form-control" name="client[FinancialSummary]">Financial Summary</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_PRESS'); ?></label>
                                        <textarea class="form-control" name="client[PressCoverage]">Press Coverage</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_DISCLOSURE'); ?></label>
                                        <textarea class="form-control" name="client[Disclosure]">Disclosure</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content text-center pad-0-t">
                                <p>* <?php echo Lang::get('USR_ADD_PWINF'); ?></p>
                                <div class="ln_solid"></div>                        

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <a href="#" onclick="clickMe('offerinfo');" class="btn btn-rounded btn-info back">Back</a>
                                        <button type="submit" class="btn btn-rounded btn-primary">Save Project</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END Main Content -->
        </div>
    </section>

</form>

<!-- /page content -->
<?php View::footer(); ?>