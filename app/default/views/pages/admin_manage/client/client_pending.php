<?php
View::$title = 'Pending Client Profiles';
View::$bodyclass = 'manage-clients';
View::header();
?>
    <?php View::template('admin_manage/top'); ?>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="block block-opt-refresh-icon4">                            
                        <div class="block-header bg-gray-lighter">
                            <h3 class="block-title">All Pending Clients</h3>
                            <p class="text-muted push-0 push-10-t">All client profiles or the companies.</p>
                        </div>
                        <div class="">
                            <?php echo View::getMessage(); ?>
                            <table class="table table-divide table-hover table-vcenter dt-responsive js-dataTable-full-pagination" style="width:100%;" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('manage/clients/add'); ?>"><i class="si si-user-follow push-5-r"></i> Create New</a>'>
                                <thead>
                                <tr>
                                    <th class="no-wrap"><i class="fa fa-suitcase text-gray"></i> Client Profiles</th>
                                    <th class="text-center" style="width: 15%;">Status</th>
                                    <th class="text-center" style="width: 15%;">Percentage</th>
                                    <th class="text-center" style="width: 15%; min-width: 110px;">Funded</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if( count( $allClients ) ){
                                    foreach( $allClients as $cp ):
                                        $percentage = 0;
                                        $thedate = date('Y-m-d');
                                        if( $cp->TargetGoal > 0 ){
                                            $percentage = $cp->TotalRaisedAmt ? (int)( ( $cp->TotalRaisedAmt / $cp->TargetGoal ) * 100+.5 ) : 0;                                                
                                        }
                                        $percentage = $percentage > 100 ? 100 : $percentage;
                                        $startdate = date_create();
                                        $closedate = date_create();
                                        $interval = 1;

                                        switch( $cp->Status ){
                                            case 'Completed':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$cp->Status.'</span>';
                                                break;
                                            case 'Approved':
                                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> '.$cp->Status.'</span>';
                                                break;
                                            case 'Pending':
                                            default:
                                                $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$cp->Status.'</span>';
                                                break;
                                            case 'Verified':
                                                $statusfield = '<span class="label label-info"><i class="fa fa-check"></i> '.$cp->Status.'</span>';
                                                break;
                                        } ?>
                                        <tr>
                                            <td>
                                                <h3 class="h5 font-w600 push-10">
                                                    <a class="link-effect" href="/project/view/<?php echo $cp->ClientProfileID;?>"><?php echo isset( $cp->CompanyName ) ? $cp->CompanyName: "-"; ?> </a>
                                                </h3>                                                    
                                                <div class="push-10 visible-xs">
                                                    <?php echo $statusfield; ?>
                                                </div>
                                                <div class="font-s13 hidden-xs">
                                                    <div class="project-info">
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span><?php echo isset( $cp->TypeOfOffer ) ? $cp->TypeOfOffer : "-"; ?> <br><small class="text-muted">TYPE OF OFFER</small></span>
                                                        </div>                                                            
                                                        <div class="col-xs-3 col-sm-6 col-md-3 text-left">
                                                            <span><?php echo isset( $cp->TargetGoal ) ? "$".number_format($cp->TargetGoal) : "-"; ?> <br><small class="text-muted">TARGET GOAL</small></span>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-12 col-md-6 text-left">
                                                            <span style="border:none;"><?php echo ( $cp->OfferOpening != '0000-00-00' ) ? date( 'Y-m-d', strtotime( $cp->OfferOpening ) ) : ""; ?> - <?php echo ( $cp->OfferClosing != '0000-00-00' ) ? date( 'Y-m-d', strtotime( $cp->OfferClosing ) ) : ""; ?> <br><?php echo $thedate > $cp->OfferClosing ? '<span class="text-danger text-uppercase">Closed</span>' : '<span class="text-success text-uppercase">Live</span>'; ?></span>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                                <div class="buttons">
                                                    <a href="/project/view/<?php echo $cp->ClientProfileID; ?>" class="text-muted push-20-r push-20-l" data-toggle="tooltip" title="View Details"><i class="fa fa-eye"></i></a>
                                                    <a href="/clients/profile/company-info/<?php echo $cp->ClientProfileID; ?>" class="text-muted push-20-r" data-toggle="tooltip" title="Edit Details"><i class="fa fa-edit"></i></a>
                                                    <a href="/manage/clients/delete/<?php echo $cp->ClientProfileID; ?>" class="text-danger" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete client <?php echo $cp->CompanyName; ?>? You will NOT be able to undo this action!' );" title="Delete Client"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                            <td class="h5 text-center hidden-xs">
                                                <?php echo $statusfield; ?>
                                            </td>
                                            <td class="h5 text-center text-muted">
                                                <?php echo $percentage; ?>%
                                            </td>
                                            <td class="h4 text-primary text-right hidden-xs">
                                                $<?php echo $cp->TotalRaisedAmt ? number_format( $cp->TotalRaisedAmt ) : '0'; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php }else{ ?>
                                    <tr>
                                        <td colspan="4">No Data</td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                        <td class="hidden"></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {

        $(".fold-table tr").on("click", function(){
            $(".fold-table tr").not(this).removeClass('open');
            $(this).toggleClass("open");
            // $(this).toggleClass("open").find(".fold-content").toggleClass("open");
        });

    });
</script>