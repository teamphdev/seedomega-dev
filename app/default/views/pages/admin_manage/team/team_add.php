<?php 
    View::$title = 'Add Core Team Member';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>

<section class="header-bottom">
    <article>
      <div class="container animated fadeInDown"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url( View::$segments[0].'/'.View::$segments[1] ); ?>">Core <?php echo View::$segments[1]; ?></a></li>
                <li class="fa fa-angle-right"></li>
                <li>Add</li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <article class="container project-single">
        <div class="start-project">
            <!-- Main Content -->        
            <div class="block items-push">
                <div class="block-header"><?php echo View::$title; ?></div>
                <div class="block-content tab-content">
                    <?php echo View::getMessage(); ?>
                    <form class="form-horizontal form-ui no-padding input_mask" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="add" />

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">                                
                                    <label class=""><?php echo Lang::get('TEAM_EDIT_PPICTURE'); ?></label>
                                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Photo" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <span>* Allowed file types: jpeg, jpg, png</span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>
                                        <?php echo Lang::get('TEAM_MNG_NAME'); ?> <span class="required">*</span>
                                    </label>
                                    <input type="text" value="" id="lname" name="team[Name]" required="required" class="form-control uppercase">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>
                                        <?php echo Lang::get('TEAM_MNG_POSN'); ?> <span class="required">*</span>
                                    </label>
                                    <input type="text" value="" id="fname" name="team[Position]" required="required" class="form-control uppercase">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label><?php echo Lang::get('TEAM_MNG_BIO'); ?></label>
                            <textarea class="form-control tinyMCE" name="team[Bio]"></textarea>
                        </div>
                        <div class="ln_solid"></div>                        

                        <div class="form-group text-center">
                            <a href="<?php echo View::url( 'manage/team/team_list' ); ?>" class="btn btn-rounded btn-danger"><i class="si si-action-undo"></i> <?php echo Lang::get('USR_ADD_CANBTN'); ?></a>
                            <button type="submit" class="btn btn-rounded btn-primary"><?php echo Lang::get('TEAM_ADD_ADDBTN'); ?></button>
                        </div>

                    </form>
                </div>   
            </div>
            <!-- END Main Content -->
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>