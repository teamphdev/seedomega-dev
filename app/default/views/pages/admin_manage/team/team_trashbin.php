<?php 
View::$title = 'Trashed Team Members';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url( View::$segments[0].'/'.View::$segments[1] ); ?>">Core <?php echo View::$segments[1]; ?></a></li>
                <li class="fa fa-angle-right"></li>
                <li>Trashed</li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<form method="post" action="">
<input type="hidden" name="doemptytrashbin">

<section class="gray">
    <!-- Page Content -->
        
    <div class="container">
        <div class="block-header">
            <h3 class="block-title">All <?php echo View::$title; ?><small></small>
                
            </h3>
        </div>

        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?> 
                <table class="table table-divide js-dataTable-full-pagination dt-responsive table-hover table-vcenter" width="100%" addbutton='<button type="submit" class="btn btn-danger btn-rounded" onclick="return confirm(&#39;Are you sure you want to delete trashed team members permanently? If you continue you will be able to undo this action!&#39;);"><i class="fa fa-trash"></i> <?php echo Lang::get("USR_TRSH_EMPBTN"); ?></button>'>
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-center">Photo</th>
                            <th class="text-center"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th class="text-center"><?php echo Lang::get('COMM_INFT_TR3TH2'); ?></th>
                            <th><?php echo Lang::get('USR_PRF_RMRKS'); ?></th>
                            <th class="no-sorting text-center" style="width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if( isset( $teams ) && count( $teams ) ){
                        foreach( $teams as $team ){ $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td class="text-center">
                                <div class="profile-image"><?php echo $team->Data['PhotoLink']; ?></div>
                                <input type="hidden" name="uids[]" value="<?php echo $team->CoreTeamID; ?>" />
                            </td>
                            <td><?php echo $team->Name; ?></td>
                            <td><?php echo $team->Position; ?></td>
                            <td data-toggle="modal" data-target="#modal-team-bio" onclick="viewBio( '<?php echo $team->CoreTeamID; ?>' );">
                                <div class="hiddenMCE font-12">
                                    <?php $excerpt = AppUtility::excerptAsNeeded( $team->Bio, 200, '... <span class="text-info">(click to view more)</span>' ); ?>
                                    <?php echo $excerpt; ?>
                                </div>
                            </td>
                            <td style="text-align:center;">
                                <?php if( User::can( 'Administer All' ) || User::can( 'Delete Team Members' ) ){ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'manage/team/restore/'.$team->CoreTeamID ); ?>" title="Restore Member" data-toggle="tooltip"><i class="fa fa-undo pull-right"></i> Restore</a></li>
                                            <li><a href="<?php echo View::url( 'manage/team/delete/'.$team->CoreTeamID ); ?>" title="Permanently Delete Member" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete team member <?php echo $team->Name; ?>? If you continue you will be able to undo this action!' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'manage/team/restore/'.$team->CoreTeamID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Restore Member" data-toggle="tooltip"><i class="fa fa-undo"></i> Restore</a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr>
                            <td colspan="5">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>

                <!-- Team Bio Modal -->
                <div class="modal fade" id="modal-team-bio" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="modal-title"></span></h3>
                          </div>
                          <div class="block-content">
                            <div id="modal-content" class="hiddenMCE"></div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-sm btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END Team Bio Modal -->
            </div>
        </div>
    </div>
</section>
<input type="hidden" id="jteams" value="<?php echo $jdata; ?>">

</form>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    var teams = [];
    $( document ).ready( function(){
        $teams = $( '#jteams' ).val();
        teams = jQuery.parseJSON( $teams );

        // initialize tinyMCE on contents having .hiddenMCE class
        hiddenMCEconfig.inline = true;
        hiddenMCEconfig.readonly = 1;
        tinymce.init( hiddenMCEconfig ); // hiddenmce.js
    });

    function viewBio( tid ){
        $( '#modal-title' ).text( teams[tid]['FullName'] );
        $( '#modal-content' ).text( teams[tid]['Bio'] );
        tinymce.get( 'modal-content' ).setContent( teams[tid]['Bio'] );
    }
</script>