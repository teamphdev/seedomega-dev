<?php 
    View::$title = 'Edit Core Team Member';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>

<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <div class="text-center overflow-hidden">
                <div class="push-30-t push animated fadeInDown">
                    <div class="img-wrap img-avatar img-avatar128 img-avatar-thumb"><?php echo $teams[0]->Data['PhotoLink']; ?></div>
                </div>
                <div class="push-20-t animated fadeInUp">            
                    <h4 class="h3 push-10-t text-white"><?php echo isset( $teams[0]->Name ) ? $teams[0]->Name : ''; ?></h4>
                </div>
            </div>

        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url( View::$segments[0].'/'.View::$segments[1] ); ?>">Core <?php echo View::$segments[1]; ?></a></li>
                <li class="fa fa-angle-right"></li>
                <li><?php echo isset( $teams[0]->Name ) ? $teams[0]->Name : 'Unknown'; ?></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <article class="container project-single">
        <div class="start-project">
            <!-- Main Content -->        
            <div class="block items-push">
                <div class="block-header"><?php echo View::$title; ?></div>
                <div class="block-content tab-content">
                    <?php echo View::getMessage(); ?>
                    <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="edit" />
                        <input type="hidden" name="photoid" value="<?php echo isset( $teams[0]->Photo ) ? $teams[0]->Photo : '0'; ?>" />

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">                                
                                    <label class=""><?php echo Lang::get('TEAM_EDIT_PPICTURE'); ?></label>
                                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Photo" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <span>* Allowed file types: jpeg, jpg, png</span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>
                                        <?php echo Lang::get('TEAM_MNG_NAME'); ?> <span class="required">*</span>
                                    </label>
                                    <input type="text" value="<?php echo isset( $teams[0]->Name ) ? $teams[0]->Name : ''; ?>" id="lname" name="team[Name]" required="required" class="form-control uppercase">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>
                                        <?php echo Lang::get('TEAM_MNG_POSN'); ?> <span class="required">*</span>
                                    </label>
                                    <input type="text" value="<?php echo isset( $teams[0]->Position ) ? $teams[0]->Position : ''; ?>" id="fname" name="team[Position]" required="required" class="form-control uppercase">
                                </div>
                            </div>
                        </div>                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('TEAM_MNG_BIO'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <textarea class="form-control tinyMCE" name="team[Bio]"><?php echo isset( $teams[0]->Bio ) ? $teams[0]->Bio : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="ln_solid"></div>                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo View::url( 'manage/team/team_list' ); ?>" class="btn btn-rounded btn-danger"><i class="si si-action-undo"></i> <?php echo Lang::get('USR_ADD_CANBTN'); ?></a>
                                <button type="submit" class="btn btn-rounded btn-primary"><?php echo Lang::get('TEAM_EDIT_EDITBTN'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- END Main Content -->
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>