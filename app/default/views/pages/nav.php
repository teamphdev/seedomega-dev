<?php $userinfo = User::info(); ?>

<div class="nav-header-right">
	<nav class="main-nav">
		<?php AppMenu::show(); ?>
	</nav>

	<div class="header-right">
		<div class="header-right-btns">
			<ul>
				<?php 
				if( isset( $userinfo->UserID ) ){
					$myprofile = $userinfo->Code == 'ASST' ? 'Company Profile' : 'My Profile';
					$userID = isset( $userinfo->Code ) && $userinfo->Code == 'ASST' ? $userinfo->ReferrerUserID : $userinfo->UserID; ?>
					<li>
						<div class="btn-group">
							<button class="btn btn-default btn-image dropdown-toggle font-14" data-toggle="dropdown" type="button" title="<?php echo (AppUtility::getCompanyName($userID)) ? AppUtility::getCompanyName($userID) : ''; ?>">
								<?php $avatar = View::common()->getUploadedFiles( $userinfo->Avatar ); ?>
								<?php echo View::photo( ( isset( $avatar[0] ) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'), "Avatar" ); ?>
								<?php echo $userinfo->FirstName; ?> | <?php echo $userinfo->Code; ?>-<?php echo $userinfo->UserID; ?>
								<i class="fa fa-bars"></i>
							</button>
						
							<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="<?php echo User::dashboardLink(); ?>"><i class="si si-home pull-right"></i> Dashboard</a></li>
								<?php if( AppMenu::profileLink() ){
									$completeness = AppUtility::profileCompleteness( $userID ); ?>
									<li><a href="<?php View::url( AppMenu::profileLink(), true ) ?>"><i class="si si-user pull-right"></i><?php echo $myprofile; ?> <span class="badge badge-<?php echo $completeness < 100 ? 'danger' : 'success'; ?>"><?php echo $completeness; ?>%</span></a></li>
								<?php 
								$linkthis = 'users/personal';
								if( User::is( 'User' ) ){ $linkthis = 'users/profile/details'; } ?>
									<li><a href="<?php View::url( $linkthis, true ) ?>"><i class="si si-user pull-right"></i> Member Details</a></li>
									<?php } else { ?>
										<li><a href="<?php View::url( 'users/personal', true ) ?>"><i class="si si-user pull-right"></i> Profile</a></li>
									<?php } ?>
									<li><a href="<?php View::url( 'users/security', true ) ?>"><i class="si si-lock pull-right"></i> Security</a></li>
									<?php if( User::is( 'User' ) || User::is( 'Client' ) || User::is( 'Assistants' ) ){ ?>
											<li class="divider"></li>
									<?php if( User::can( 'View Wallet' ) ){ ?>
											<li><a href="/wallet"><i class="si si-wallet pull-right"></i> My Wallet </a></li>
									<?php } ?>
									<li><a href="/users/referrals"><i class="si si-badge pull-right"></i> Referral Earnings </a></li>
									<li class="divider"></li>
									<!--li class="dropdown-header">Other</li-->                  
									<li><a href="/support/dashboard"><i class="si si-support pull-right"></i> Support </a></li>
									<li><a href="<?php View::url( 'termsandcondition', true ) ?>"><i class="si si-info pull-right"></i> Terms & Conditions</a>
								<?php } ?>
								</li>
								<li><a href="<?php View::url( 'users/logout', true ) ?>"><i class="si si-login pull-right"></i> Log out</a></li>
							</ul>
						</div>
					</li>
				<?php } else { ?>
			
					<li class="login-panel"><a href="#login">Sign In</a><span>Or</span>
						<div class="dropdown-login">
							<div class="arrow"></div>
							<div class="reverse"></div>
							<div class="box-reverse left"></div>
							<div class="box-reverse right"></div>
							<?php View::template('users/login-popup'); ?>
						</div>
					</li>
					<li><a href="<?php echo View::url('users/signup') ?>" class="btn btn-4 orange">Join</a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="search-box border-right">
			<form action="/search" enctype="multipart/form-data" method="post" onsubmit="if(document.getElementById('thesearchkey').value == ''){ return false; } else { return true; }">
				<input type="hidden" name="action" value="sendsearch" />
				<input id="thesearchkey" type="text" value="" name="keyword" placeholder="Search projects...">
				<span class="icon fa fa-search transition-color" title="Search Projects"></span>
			</form>
		</div>
	</div>      
</div>