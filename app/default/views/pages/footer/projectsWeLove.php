<?php 
View::$title = 'Projects We Love';
View::$bodyclass = '';
View::header(); 
?>
<?php $userinfo = User::info();  //print_r(unserialize(base64_decode($_SESSION[SESSIONCODE])));?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title;?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url();?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="#"><?php echo View::$segments[0];?></a></li>
              <?php if(isset(View::$segments[1])) { ?>
                <li class="fa fa-angle-right"></li>
                <li><?php echo View::$title;?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="white">
   <article class="container footer-links">
      <div class="row">
         <div class="col-lg-12">
            <h3 style="text-align: left;">Projects We Love</h3>
            <hr><br>
            <p style="text-align: left;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
         </div>
      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>