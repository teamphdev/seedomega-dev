<?php 
    View::$title = 'Manage Users';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo View::url(); ?>">Home</a></li>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
                    <?php if( isset(View::$segments[1]) ) { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-6 align-right">
                <ul>
                    <li><a href="<?php echo View::url('seeders/add'); ?>" class="btn btn-sm btn-rounded btn-success" data-toggle="tooltip" title="Add User"><i class="si si-user-follow"></i> Add User</a></li>
                    <li><a href="<?php echo View::url('seeders/trashbin'); ?>" class="btn btn-sm btn-rounded btn-warning" data-toggle="tooltip" title="Trash Bin"><i class="si si-trash"></i> Trash Bin</a></li>
                </ul>
            </div>
        </div>
    </article>
</section>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage();  ?> 
                <table class="table js-table-checkable js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" cellspacing="0" width="100%">
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-align"><?php echo Lang::get('USR_MNG_IMG'); ?></th>
                            <th class="sort-this"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_EML'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_GNDR'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_PHNE'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_LVL'); ?></th>
                            <th class="no-sorting text-center" style="text-align:center; width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if(count($users)) {
                            foreach($users as $user) { $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <?php $avatar = View::common()->getUploadedFiles($user->Avatar); ?>
                            <td class="text-center"><?php echo View::photo( ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' ),false,false,false,'width:30px;'); ?></td>
                            <td><?php echo $user->Code; ?>-<?php echo $user->UserID; ?></td>
                            <td><?php echo $user->Email; ?></td>
                            <td><?php echo $user->LastName; ?> <?php echo $user->FirstName; ?></td>
                            <td><?php echo $user->Gender; ?></td>
                            <td><?php echo $user->Phone; ?></td>
                            <td><?php echo $user->Name; ?></td>
                            <td class="text-center">
                                <a href="<?php echo View::url('seeders/edit/'.$user->UserID); ?>" title="Edit" class="btn btn-xs btn-default" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo View::url('seeders/trash/'.$user->UserID); ?>" title="Delete" class="btn btn-xs btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to put user <?php echo $user->FirstName; ?> <?php echo $user->LastName; ?> to trash bin?');"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php } 
                            } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="99">No Data</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>