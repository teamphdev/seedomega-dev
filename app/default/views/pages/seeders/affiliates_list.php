<?php 
    View::$title = 'Referrals';
    View::$bodyclass = '';
    View::header(); 
?>
<?php $userinfo = User::info(); ?>

<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
                    <?php if( isset( View::$segments[1] ) ){ ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-6 align-right sub-menu">
               
            </div>
        </div>
    </article>
</section>
<?php $affiliateUrl = View::url( 'join/'.User::info( 'UserID' ) ); ?>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <div class="block">
            <div class="block-content">
                <h3 class="header-underline">Earn More</h3>
                <div class="form-group">
                    <label>Share your affiliate link:</label> 
                    <input type="text" class="form-control" value="<?php echo $affiliateUrl; ?>" readonly />
                </div>
                <div class="form-group"><label>Share with social media:</label>                     
                    <ul class="affiliate-share">
                        <li title="Facebook">
                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $affiliateUrl; ?>" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li title="Twitter">
                            <a href="https://twitter.com/home?status=<?php echo $affiliateUrl; ?>" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li title="LinkedIn">
                            <a href="https://www.linkedin.com/shareArticle?mini=true&title=Join%20Now!&url=<?php echo $affiliateUrl; ?>" target="_blank">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <li title="Google+">
                            <a href="https://plus.google.com/share?url=<?php echo $affiliateUrl; ?>" target="_blank">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <?php echo View::getMessage(); ?>
        <div class="block">
            <div class="block-content">
                <h3>My Invites</h3><small class="text-muted">Total <?php echo Lang::get('USR_MNG_COMMI'); ?>: $ <b><?php echo number_format( $totalcommission, 2 ); ?></b></small>
                <hr><br>
                <ul class="nav nav-tabs" data-toggle="tabs">
                    <li class="text-center active">
                        <a href="#referral-converted"><b>Converted</b>&nbsp;<span class="badge badge-success"><?php echo isset( $totalconverted ) ? $totalconverted : '0'; ?></span><br><small class="text-muted">already invested</small></a>
                    </li>
                    <li class="text-center">
                        <a href="#referral-invited"><b>Invited</b>&nbsp;<span class="badge badge-warning"><?php echo isset( $totalinvited ) ? $totalinvited : '0' ;?></span><br><small class="text-muted">no investment yet</small></a>
                    </li>
                </ul>
                <div class="block-content tab-content bg-white">
                    <!-- Converted -->
                    <div class="tab-pane fade fade-up in active" id="referral-converted">
                        <table class="table  js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter in active" cellspacing="0" style="width: 100%;">
                            <thead>
                                <tr class="headings">
                                    <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                                    <th style="min-width: 25%;"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                                    <th>Company</th>
                                    <th width="20%" class="text-center"><?php echo Lang::get('USR_MNG_COMMI'); ?> Amount</th>
                                    <th width="15%" class="text-center"><?php echo Lang::get('USR_MNG_STATUS'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $cntr = 0;
                                    if( count( $converted ) ) {
                                        foreach( $converted as $ref ){
                                            if( isset( $ref->TotalAmountAttached ) ){ $cntr++;
                                                switch( $ref->Status ){
                                                    case 'Approved':
                                                        $status = '<span class="text-success"><b>Approved</b></span>';
                                                        break;
                                                    case 'Verification':
                                                        $status = '<span class="text-warning"><b>Verification</b></span>';
                                                        break;
                                                    case 'Incomplete':
                                                    default:
                                                        $status = '<span class="text-danger"><b>Incomplete</b></span>';
                                                        break;
                                                } ?>
                                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                                    <td><div class="profile-image">
                                                            <img src="<?php echo View::url( 'assets/'.( $ref->AvatarData ? 'files/'.$ref->AvatarData[0]->FileSlug : 'images/user.png' ) ); ?>" />
                                                        </div>
                                                        <?php
                                                            echo $ref->FirstName ? ucwords( $ref->FirstName ) : '';
                                                            echo $ref->LastName ? ' '.ucwords( $ref->LastName ) : '';
                                                        ?>
                                                    </td>
                                                    <td><?php echo $ref->CompanyName ; ?></td>
                                                    <td class="text-center"><?php echo '$ ' . number_format( $ref->Commission, 2 ) ; ?></td>
                                                    <td class="text-center"><?php echo $status ? $status : ''; ?></td>
                                                </tr>
                                        <?php } }
                                    } else {?>
                                    <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                        <td colspan="99">No Data</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END Converted -->

                    <!-- Invited -->
                    <div class="tab-pane fade fade-up" id="referral-invited">
                        <table class="table  js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter in active" cellspacing="0" style="width: 100%;">
                            <thead>
                                <tr class="headings">
                                    <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                                    <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                                    <th width="20%" class="hidden"></th>
                                    <th width="15%" class="text-center"><?php echo Lang::get('USR_MNG_STATUS'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $cntr = 0;
                                    if( count( $invited ) ){
                                        foreach( $invited as $ref ){
                                            if( !isset( $ref->TotalAmountAttached ) ){ $cntr++;
                                                switch( $ref->Status ){
                                                    case 'Approved':
                                                        $status = '<span class="text-success"><b>Approved</b></span>';
                                                        break;
                                                    case 'Verification':
                                                        $status = '<span class="text-warning"><b>Verification</b></span>';
                                                        break;
                                                    case 'Incomplete':
                                                    default:
                                                        $status = '<span class="text-danger"><b>Incomplete</b></span>';
                                                        break;
                                                } ?>
                                                <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                                    <td>
                                                        <div class="profile-image">
                                                            <img src="<?php echo View::url( 'assets/'.( $ref->AvatarData ? 'files/'.$ref->AvatarData[0]->FileSlug : 'images/user.png' ) ); ?>" />
                                                        </div>
                                                        <?php
                                                            echo $ref->FirstName ? ucwords( $ref->FirstName ) : '';
                                                            echo $ref->LastName ? ' '.ucwords( $ref->LastName ) : '';
                                                        ?>
                                                    </td>
                                                    <td class="text-center"><?php echo $status ? $status : ''; ?></td>
                                                </tr>
                                        <?php } }
                                    } else {?>
                                    <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                                        <td colspan="99">No Data</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END Invited -->

                </div>
            </div>
        </div>

    </div>
</section>
<!-- page content -->

<?php View::footer(); ?>