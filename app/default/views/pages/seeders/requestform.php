<?php 
View::$title = 'Reset Password';
View::$bodyclass = 'loginpage';
View::header(); 
?>
<section class="breadcrumb">
   <article class="container">
      <div class="row">
         <div class="col-lg-6">
            <ul>
               <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
               <li><a href="<?php echo View::url(); ?>">Home</a></li>
               <li class="fa fa-angle-right"></li>
               <li><?php echo View::$title; ?></li>
            </ul>
         </div>
      </div>
   </article>
</section>

<section class="gray bg-image">
   <article class="container contactpage">
      <div class="row row-header">
         <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
            <div class="start-project">
               <div class="block">

                  <!-- Tab panes -->
                  <div class="tab-content start-content">
                     <p class="font-20">Please enter a new password.</p>
                     <div class="alert hide" id="form-message">
                        <!-- Form Alert Goes Here -->
                     </div>
                     <form method="post">
                        <input type="hidden" name="action" value="requestreset" />
                        <input type="hidden" name="resetkey" value="<?php echo $segment; ?>">
                        <div id="reset-form" class="form-wizard active">
                           <?php echo View::getMessage(); ?>
                           <div class="form-group">
                              <input type="password" class="form-control" placeholder="New Password" name="pass1" required />
                           </div>
                           <div class="form-group">
                              <input type="password" class="form-control" placeholder="Re-Enter New Password" name="pass2" required />
                           </div>
                           <div class="form-group text-center">
                              <button type="submit" class="btn btn-4 green default submit font-18" id="reset-submit">Reset</button>
                           </div>

                           <div class="clearfix"></div>
                           <div class="divider"><span></span></div>
                           <div class="form-group text-left">
                              <!--a class="link" href="<?php echo View::url('users/login') ?>">Login<span class="text-gray"> I know my password </span></a-->
                           </div>
                        </div>
                     </form>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </article>
</section>
<?php View::footer(); ?>