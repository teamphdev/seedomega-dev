<?php 
View::$title = 'Add User';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>

<?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

<section class="gray">
    <article class="container project-single">
        <div class="start-project">
            <!-- Main Content -->        
            <div class="block items-push">
                <div class="block-content tab-content">
                    <?php echo View::getMessage(); ?>
                    <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="adduser" />

                        <?php if(User::can('Administer All')): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">
                               <?php echo Lang::get('USR_ADD_ULVL'); ?>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?php 
                                    View::form(
                                        'selecta',
                                        array(
                                            'name'=>'user[Level]',
                                            'options'=>$levels,
                                            'value'=>isset($pdata->Level) ? $pdata->Level : '',
                                            'class'=>'form-control col-md-7 col-xs-12',
                                            'id'=>'userlevelopt'
                                            //'inarray'=>$inarray
                                        )
                                    );  
                                ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_PLANG'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?php View::form('selecta',array('name'=>'meta[Language]','options'=>AppUtility::getLanguages(),'class'=>'form-control','value'=>'')); ?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                                
                            <label class="control-label col-md-3 col-sm-3 col-xs-12 bottom-align-text" for="fname"><?php echo Lang::get('USR_EDIT_PPCTURE'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                <span>* Allowed file types: jpeg, jpg, png</span>
                            </div>
                        </div>                        
                       
                        <!-- <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('CLN_EDIT_RC'); ?> / <?php echo Lang::get('CLN_EDIT_RN'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <input type="text" name="user[ReferrerUserID]" value="<?php echo User::info('UserID'); ?>" id="ReferrerUserID">
                                        <input type="text" id="ReferrerUserChecker" class="form-control" value="<?php echo User::info('UserID'); ?>" rel="<?php echo View::url('ajax/userinfo'); ?>"><span id="referrerloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <input type="text" id="referrerdata" class="form-control" value="" disabled="">
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lname">
                                <?php echo Lang::get('USR_ADD_LN'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->LastName) ? $pdata->LastName : ''; ?>" id="lname" name="meta[LastName]" required="required" class="form-control uppercase col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">
                                <?php echo Lang::get('USR_ADD_FN'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->FirstName) ? $pdata->FirstName : ''; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control uppercase col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_NN'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->NickName) ? $pdata->NickName : ''; ?>" id="nname" name="meta[NickName]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                                <?php echo Lang::get('USR_ADD_EML'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="email" id="EmailChecker" name="user[Email]" class="form-control col-md-7 col-xs-12" value="" required="required" rel="<?php echo View::url('ajax/checkemail'); ?>" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">
                                <?php echo Lang::get('USR_ADD_PHNE'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="phone" value="<?php echo isset($pdata->Phone) ? $pdata->Phone : ''; ?>" id="phone" name="meta[Phone]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                <?php echo Lang::get('USR_ADD_GNDR'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?php $gender = isset($pdata->Gender) ? $pdata->Gender : 'M'; ?>                                
                                <label class="css-input css-radio css-radio-info push-10-r">
                                    <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_ADD_GNDRM'); ?>
                                </label>
                                <label class="css-input css-radio css-radio-info">
                                    <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_ADD_GNDRF'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_ADDS'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->Address) ? $pdata->Address : ''; ?>" id="address" name="meta[Address]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_CTY'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->City) ? $pdata->City : ''; ?>" id="city" name="meta[City]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_STATES'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->State) ? $pdata->State : ''; ?>" id="state" name="meta[State]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_CNTRY'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <select id="Country" name="meta[Country]" class="form-control">
                                    <?php foreach(AppUtility::getCountries() as $country) { ?>
                                    <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="postal" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_PCODE'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($pdata->PostalCode) ? $pdata->PostalCode : ''; ?>" id="postal" name="meta[PostalCode]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_ADD_RMRKS'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <textarea class="form-control dowysiwyg" name="meta[Bio]"><?php echo isset($pdata->Bio) ? $pdata->Bio : ''; ?></textarea>
                            </div>
                        </div>                        

                        <div class="client-profiles-section">
                            <div class="row">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="text-divider">
                                        <span><b>Company Info</b></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?></label>
                                        <input type="text" value="" id="CompanyName" name="client[CompanyName]" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_VIDEO'); ?></label>
                                        <textarea class="form-control" name="client[Video]" rows="2" style="height:75px !important"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_VIDEODESC'); ?></label>
                                        <textarea class="form-control" name="client[VideoDescription]" id="VideoDescription"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-profiles-section">
                            <div class="row">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="text-divider">
                                        <span><b>Company Offer</b></span>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_TYPEOFFER'); ?></label>
                                        <select id="TypeOfOffer" name="client[TypeOfOffer]" class="form-control">
                                            <option value="">Select</option>
                                            <option value="Limited">Limited</option>
                                            <option value="Open">Open</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('CLN_PRF_OFFEROPENING'); ?></label>
                                                <input type="text" value="" id="OfferOpening" name="client[OfferOpening]" class="form-control jsdate">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('CLN_PRF_OFFERCLOSING'); ?></label>
                                                <input type="text" value="" id="OfferClosing" name="client[OfferClosing]" class="form-control jsdate">
                                            </div>
                                        </div>
                                    </div>                                    

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                        <input type="text" value="" id="Price" name="client[Price]" class="form-control" placeholder="0.00">
                                    </div>

                                    <?php /*
                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_SZEOFFER'); ?></label>
                                        <input type="text" value="" id="SizeOfOffer" name="client[SizeOfOffer]" class="form-control">
                                    </div>
                                    */ ?>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Minimum Invest</label>
                                                <input type="text" value="" id="MinimumBid" name="client[MinimumBid]" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Maximum Invest</label>
                                                <input type="text" value="" id="MinimumBid" name="client[MaximumBid]" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_LDMNGR'); ?></label>
                                        <input type="text" value="" id="LeadManager" name="client[LeadManager]" class="form-control">
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="client-profiles-section">
                            <div class="row">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-7 col-sm-7 col-xs-12">
                                    <div class="text-divider">
                                        <span><b>Company Summary</b></span>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_EXECSUMMARY'); ?></label>
                                        <textarea class="form-control" name="client[ExecutiveSummary]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_OFFVIEW'); ?></label>
                                        <textarea class="form-control" name="client[OfferOverview]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_BUSSMODEL'); ?></label>
                                        <textarea class="form-control" name="client[BusinessModel]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_KEYINVST'); ?></label>
                                        <textarea class="form-control" name="client[KeyInvestHighlights]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_STRATVISION'); ?></label>
                                        <textarea class="form-control" name="client[StrategyVision]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_MARKTDEMAND'); ?></label>
                                        <textarea class="form-control" name="client[MarketDemand]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_BRDMNGT'); ?></label>
                                        <textarea class="form-control" name="client[BoardManagement]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_USEFUNDS'); ?></label>
                                        <textarea class="form-control" name="client[UsageOfFunds]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_FINSUMMRY'); ?></label>
                                        <textarea class="form-control" name="client[FinancialSummary]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_PRESS'); ?></label>
                                        <textarea class="form-control" name="client[PressCoverage]"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label><?php echo Lang::get('CLN_PRF_DISCLOSURE'); ?></label>
                                        <textarea class="form-control" name="client[Disclosure]"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <p>* <?php echo Lang::get('USR_ADD_PWINF'); ?></p>
                            </div>
                        </div>
                        <div class="ln_solid"></div>                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo View::url('users/list'); ?>" class="btn btn-4 blue yellow"><?php echo Lang::get('USR_ADD_CANBTN'); ?></a>
                                <button type="submit" class="btn btn-4 blue green"><?php echo Lang::get('USR_ADD_ADDBTN'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- END Main Content -->
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>