<?php
View::$title = 'Update Password';
View::$bodyclass = User::info('Sidebar');
View::header();
?>
    <?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container project-single">
            
            
            <div class="block block-themed centered max800">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Change your password</h3>
                </div>
                <div class="block-content">
                    <?php echo View::getMessage(); ?>
                    <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="action" value="savepassword" />
                    <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="oldpassword"><?php echo Lang::get('USR_PRF_OPWD'); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                                    <input type="password" value="" id="oldpassword" name="OldPassword" placeholder="<?php echo Lang::get('USR_PRF_OPWD'); ?>" required="required" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label for="newpassword"><?php echo Lang::get('USR_PRF_NPWD'); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                    <input type="password" value="" id="newpassword" name="NewPassword" required="required" class="form-control col-md-7 col-xs-12" data-validate-length-range="5,6,7,8,9,10,50">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <label for="newpasswordconfirm"><?php echo Lang::get('USR_PRF_CNPWD'); ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                    <input type="password" value="" id="newpasswordconfirm" name="NewPasswordConfirm" required="required" class="form-control" data-validate-length-range="5,6,7,8,9,10,50">
                                </div>

                            </div>
                        </div>
                        <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#generatepass" type="button">Generate Password</button>
                        
                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <button class="btn btn-4 blue green font-16" type="submit"><i class="fa fa-check push-5-r"></i> <?php echo Lang::get('USR_PRF_RESBTN'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </article>
    </section>

<?php View::footer(); ?>
<div class="modal" id="generatepass" ole="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content push-50-t">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Generate Password</h3>
                </div>
                <div class="block-content form-horizontal">
                   <div class="input-group push-20">
                        <input type="text" id="passwordss" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn" onclick="$('#passwordss').val(password.generate());">Generate</button>
                        </span>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>