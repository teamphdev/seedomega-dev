<?php
View::$title = 'User Profile';
View::$bodyclass = User::info('Sidebar');
View::header();
?>
<?php $userinfo = User::info(); ?>
    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container project-single">
            <div class="block">
                <div class="bg-image" style="background-image: url('<?php echo View::url('assets/images/'); ?>slide-2.jpg');">
                    <div class="block-content bg-primary-dark-op text-center overflow-hidden">
                        <div class="push-30-t push animated fadeInDown">
                            <?php $avatar = View::common()->getUploadedFiles($userinfo->Avatar); ?>
                            <?php View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar","img-avatar img-avatar96 img-avatar-thumb"); ?>
                        </div>
                        <div class="push-30 animated fadeInUp">
                            <h2 class="h4 font-w600 text-white push-5"><?php echo $userinfo->FirstName; ?> <?php echo $userinfo->LastName; ?></h2>
                            <h3 class="h5 text-white-op"><?php echo !empty($userinfo->Occupation) ? $userinfo->Occupation : "Occupation"; ?></h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="start-project">
                <!-- Main Content -->
                <div class="block items-push">

                    <ul class="nav nav-tabs nav-justified push-20" data-toggle="tabs">
                        <li role="presentation"><a href="#tab-profile-personal" aria-controls="tab-profile-personal" role="tab" data-toggle="tab"></i>Tier 1: Personal</a></li>
                        <li role="presentation" class="active"><a href="#tab-profile-acctvalidation" aria-controls="tab-profile-acctvalidation" role="tab" data-toggle="tab">Tier 2: Profile Validation</a></li>
                        <li role="presentation"><a href="#tab-corporate" aria-controls="tab-corporate" role="tab" data-toggle="tab">Tier 3: Corporate Profile</a></li>
                    </ul>
                    <div class="block-content tab-content">
                        <?php echo View::getMessage(); ?>
                        <!-- Personal Tab -->
                        <div class="tab-pane fade" id="tab-profile-personal">
                            <div class="row items-push">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="action" value="updateuser" />
                                        <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                                        <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                                        <input type="hidden" name="avatarid" value="<?php echo $userinfo->Avatar; ?>" />

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_PPCTURE'); ?> </label>
                                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-left">
                                                <label><?php echo Lang::get('USR_PRF_FN'); ?> <span class="required">*</span></label>
                                                <input type="text" value="<?php echo $userinfo->FirstName; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control">
                                            </div>
                                            <div class="form-right">
                                                <label><?php echo Lang::get('USR_PRF_LN'); ?> <span class="required">*</span></label>
                                                <input type="text" value="<?php echo $userinfo->LastName; ?>" id="lname" name="meta[LastName]" required="required" class="form-control">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="form-left">
                                                <label><?php echo Lang::get('USR_PRF_EML'); ?> <span class="required">*</span></label>
                                                <input type="email" id="EmailChecker2" name="user[Email]" class="form-control" value="<?php echo isset($userinfo->Email) ? $userinfo->Email : ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($userinfo->UserID) ? $userinfo->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>">
                                                <span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                            </div>
                                            <div class="form-right">
                                                <label><?php echo Lang::get('USR_PRF_PHNE'); ?> <span class="required">*</span></label>
                                                <input type="phone" value="<?php echo $userinfo->Phone; ?>" id="phone" name="meta[Phone]" required="required" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_GNDR'); ?> <span class="required">*</span></label>
                                            <div>
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $userinfo->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                                </label>
                                                <label class="css-input css-radio css-radio-primary">
                                                    <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $userinfo->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-left">
                                                <label>Occupation</label>
                                                <input type="text" value="<?php echo $userinfo->Occupation; ?>" name="meta[Occupation]" class="form-control">
                                            </div>
                                            <div class="form-right">
                                                <label>JobTitle</label>
                                                <input type="text" value="<?php echo $userinfo->JobTitle; ?>" name="meta[JobTitle]" class="form-control">
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_RMRKS'); ?></label>
                                            <textarea class="form-control dowysiwyg" name="meta[Bio]"><?php echo $userinfo->Bio; ?></textarea>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="text-center">
                                                <!-- <a href="<?php echo View::url('users/'); ?>" class="btn btn-warning"><?php echo Lang::get('USR_PRF_CANBTN'); ?></a> -->
                                                <button class="btn btn-4 blue green" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END Personal Tab -->

                        <!-- Account Validation -->
                        <div class="tab-pane fade in active" id="tab-profile-acctvalidation">
                            <div class="row items-push">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="action" value="updateuser" />
                                        <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                                        <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                                        <input type="hidden" name="avatarid" value="<?php echo $userinfo->Avatar; ?>" />

                                        <?php if ($userinfo->Phone=='' || $userinfo->Address=='' || $userinfo->City =='' || $userinfo->State=='' || $userinfo->Country=='' || $userinfo->PostalCode=='' ){ ?>
                                            <div class="alert alert-warning fade in">Please complete all the information below!</div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_PHNE'); ?></label>
                                            <input type="text" value="<?php echo $userinfo->Phone; ?>" id="address" name="meta[Phone]" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_ADDS'); ?></label>
                                            <input type="text" value="<?php echo $userinfo->Address; ?>" id="address" name="meta[Address]" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <div class="form-left">
                                                <label><?php echo Lang::get('USR_PRF_CTY'); ?></label>
                                                <input type="text" value="<?php echo $userinfo->City; ?>" id="city" name="meta[City]" class="form-control">
                                            </div>
                                            <div class="form-right">
                                                <label><?php echo Lang::get('USR_PRF_STATES'); ?></label>
                                                <input type="text" value="<?php echo $userinfo->State; ?>" id="state" name="meta[State]" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="form-left">
                                                <label><?php echo Lang::get('USR_PRF_CNTRY'); ?></label>
                                                <select name="meta[Country]" class="form-control form-control">
                                                    <?php foreach(AppUtility::getCountries() as $country) { ?>
                                                        <option value="<?php echo $country; ?>" <?php echo $userinfo->Country == $country ? 'selected' : ''; ?>><?php echo $country; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-right">
                                                <label for="postal" class="control-label"><?php echo Lang::get('USR_PRF_PCODE'); ?></label>
                                                <input type="text" value="<?php echo isset($userinfo->PostalCode) ? $userinfo->PostalCode : ''; ?>" id="postal" name="meta[PostalCode]" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_UPLGOVID'); ?> </label>
                                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('USR_PRF_UPLADDRPRF'); ?> </label>
                                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span>
                                        </div>

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="text-center">
                                                <button class="btn btn-4 blue green" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- END Account Validation -->

                        <!-- Corporate Tab -->
                        <div class="tab-pane fade" id="tab-corporate">
                            <div class="row items-push">
                                <div class="col-sm-8 col-sm-offset-2 ">
                                    <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="action" value="updateuser" />
                                        <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                                        <input type="hidden" name="metacorpid" value="<?php echo $userMetaCorp->UserMetaCorpID; ?>" />
                                        <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                                        <div class="accordion" id="accordion-faq">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                                        <strong>Additional Information</strong>
                                                        <span class="updown-cion"></span>
                                                    </a>
                                                </div>
                                                <div id="collapseOne" class="accordion-body collapse in" aria-expanded="true" style="">
                                                    <div class="accordion-inner">
                                                        <label>Security Question</label>
                                                        <textarea class="form-control dowysiwyg" name="metaCorp[SecurityAnswer]"><?php echo $userMetaCorp->SecurityAnswer; ?></textarea>
                                                    </div>

                                                    <div class="accordion-inner">
                                                        <p>Within the last 10 years, have you worked in an investment-related field for 3 or more
                                                            consecutive years?</p>


                                                        <div>
                                                            <label class="css-input css-radio css-radio-primary push-10-r">
                                                                <input type="radio" class="flat" name="metaCorp[WorkedInInvestmentField]" id="WorkedInInvestmentFieldNo" value="0" <?php echo $userMetaCorp->WorkedInInvestmentField == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                            </label>
                                                            <label class="css-input css-radio css-radio-primary">
                                                                <input type="radio" class="flat" name="metaCorp[WorkedInInvestmentField]" id="WorkedInInvestmentFieldY" value="1" <?php echo $userMetaCorp->WorkedInInvestmentField== '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                            </label>
                                                        </div>

                                                    </div>

                                                    <div class="accordion-inner">
                                                        <p><?php echo Lang::get('USR_PRF_INVQS_EMPSTAT'); ?></p>
                                                        <?php View::form('selecta',array('name'=>'metaCorp[EmploymentStatus]','options'=>['Employed'=>'Employed','Homemaker'=>'Homemaker','Retired'=>'Retired','Self-Employed'=>'Self-Employed','Student'=>'Student','Unemployed'=>'Unemployed'],'class'=>'form-control','value'=>$userMetaCorp->EmploymentStatus)); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                                        <strong>Education</strong>
                                                        <span class="updown-cion"></span>
                                                    </a>
                                                </div>
                                                <div id="collapseTwo" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="accordion-inner">
                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_EDULEVEL'); ?></label>
                                                            <?php View::form('selecta',array('name'=>'metaCorp[HighestLevelEdu]','options'=>['Graduate'=>'Graduate','Masters'=>'Masters','College'=>'College','Undergraduate'=>'Undergraduate','Postgraduate'=>'Postgraduate'],'class'=>'form-control','value'=>$userMetaCorp->HighestLevelEdu)); ?>
                                                        </div>

                                                        <div class="accordion-inner">
                                                            <label><?php echo Lang::get('USR_PRF_FINCERT'); ?></label>
                                                            <textarea class="form-control dowysiwyg" name="metaCorp[FinanceCertification]"><?php echo $userMetaCorp->FinanceCertification; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false">
                                                        <strong>Investment Objective</strong>
                                                        <span class="updown-cion"></span>
                                                    </a>
                                                </div>
                                                <div id="collapseThree" class="accordion-body collapse" aria-expanded="false">
                                                    <div class="accordion-inner">
                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveIncome]" id="InvestObjectiveIncomeHigh" value="High" <?php echo $userMetaCorp->InvestObjectiveIncome == 'High' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveIncome]" id="InvestObjectiveIncomeMedium" value="Medium" <?php echo $userMetaCorp->InvestObjectiveIncome== 'Medium' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveIncome]" id="InvestObjectiveIncomeLow" value="Low" <?php echo $userMetaCorp->InvestObjectiveIncome== 'Low' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_INVOBJCT_FORMSPEC_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecHigh" value="High" <?php echo $userMetaCorp->InvestObjectiveFormSpec == 'High' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecMedium" value="Medium" <?php echo $userMetaCorp->InvestObjectiveFormSpec== 'Medium' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecLow" value="Low" <?php echo $userMetaCorp->InvestObjectiveFormSpec == 'Low' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_INVOBJCT_INVRISK_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskHigh" value="High" <?php echo $userMetaCorp->InvestObjectiveInvestRisk == 'High' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskMedium" value="Medium" <?php echo $userMetaCorp->InvestObjectiveInvestRisk== 'Medium' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskLow" value="Low" <?php echo $userMetaCorp->InvestObjectiveInvestRisk == 'Low' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false">
                                                        <strong>Income and Net Worth</strong>
                                                        <span class="updown-cion"></span>
                                                    </a>
                                                </div>
                                                <div id="collapseFour" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="accordion-inner">
                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_ANNUAL_LBL'); ?></label>
                                                            <div>
                                                                <?php View::form('selecta',array('name'=>'metaCorp[AnnualIncome]','options'=>['0-5000'=>'0-5000 USD','5001-10000'=>'5,001-10,000 USD','10001 - 50000'=>'10,001 - 50,000 USD','50001 - 100000'=>'50,000 - 100,000 USD'],'class'=>'form-control','value'=>$userMetaCorp->AnnualIncome)); ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_NET_LBL'); ?></label>
                                                            <div>
                                                                <?php View::form('selecta',array('name'=>'metaCorp[NetWorth]','options'=>['0-5000'=>'0-5000 USD','5001-10000'=>'5,001-10,000 USD','10001 - 50000'=>'10,001 - 50,000 USD','50001 - 100000'=>'50,000 - 100,000 USD'],'class'=>'form-control','value'=>$userMetaCorp->NetWorth)); ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_DESC_LBL'); ?></label>
                                                            <div>
                                                                <?php View::form('selecta',array('name'=>'metaCorp[SourceOfFunds]','options'=>['Employment of Wages'=>'Employment of Wages','Gift'=>'Gift','Inheritance/Trust'=>'Inheritance/Trust','Investments'=>'Investments','Legal Settlement'=>'Legal Settlement','Lottery/Gaming'=>'Lottery/Gaming','Retirement Funds'=>'Retirement Funds','Savings'=>'Savings','Spouse/Parent Support'=>'Spouse/Parent Support','Unemployment/Disability'=>'Unemployment/Disability','Other'=>'Other'],'class'=>'form-control','value'=>$userMetaCorp->SourceOfFunds)); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false">
                                                        <strong>Affiliations &amp; Disclosures</strong>
                                                        <span class="updown-cion"></span>
                                                    </a>
                                                </div>
                                                <div id="collapseFive" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="accordion-inner">
                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_AFFDISC_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[AssociatedWithBroker]" id="AssociatedWithBrokerN" value="0" <?php echo $userMetaCorp->AssociatedWithBroker == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[AssociatedWithBroker]" id="AssociatedWithBrokerY" value="1" <?php echo $userMetaCorp->AssociatedWithBroker == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_AFFDISC_PUBTRADED_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[PublicTradedCompany]" id="PublicTradedCompanyN" value="0" <?php echo $userMetaCorp->PublicTradedCompany == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[PublicTradedCompany]" id="PublicTradedCompanyY" value="1" <?php echo $userMetaCorp->PublicTradedCompany == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_AFFDISC_INSIDERS_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[AffInsiders]" id="AffInsidersN" value="0" <?php echo $userMetaCorp->AffInsiders == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[AffInsiders]" id="AffInsidersY" value="1" <?php echo $userMetaCorp->AffInsiders == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_AFFDISC_POLITICS_LBL'); ?></label>
                                                            <div>
                                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                                    <input type="radio" class="flat" name="metaCorp[PoliticalExposedPerson]" id="PoliticalExposedPersonN" value="0" <?php echo $userMetaCorp->PoliticalExposedPerson == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                                </label>
                                                                <label class="css-input css-radio css-radio-primary">
                                                                    <input type="radio" class="flat" name="metaCorp[PoliticalExposedPerson]" id="PoliticalExposedPersonY" value="1" <?php echo $userMetaCorp->PoliticalExposedPerson == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false">
                                                        <strong>Stock Trading Experiences</strong>
                                                        <span class="updown-cion"></span>
                                                    </a>
                                                </div>
                                                <div id="collapseSix" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                                    <div class="accordion-inner">
                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_STOCK_YRS_LBL'); ?></label>
                                                            <div>
                                                                <?php View::form('selecta',array('name'=>'metaCorp[YearsOfTrading]','options'=>range(0, 30),'class'=>'form-control','value'=>$userMetaCorp->YearsOfTrading)); ?>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label><?php echo Lang::get('USR_PRF_STOCK_TRADESPAST3_LBL'); ?></label>
                                                            <div>
                                                                <?php View::form('selecta',array('name'=>'metaCorp[TradesMadePast3Yrs]','options'=>range(0, 30),'class'=>'form-control','value'=>$userMetaCorp->TradesMadePast3Yrs)); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="text-center">
                                                <button class="btn btn-4 blue green" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END Privacy Tab -->

                    </div>
                    <!-- <div class="block-content block-content-full bg-gray-lighter text-center">
                        <button class="btn btn-4 blue green" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                        <button class="btn btn-4 blue yellow" type="reset"><i class="fa fa-refresh push-5-r"></i> Reset</button>
                    </div> -->
                </div>
                <!-- END Main Content -->

            </div>

        </article>
    </section>

<?php View::footer(); ?>