<?php 
View::$title = 'Request Password Reset';
View::$bodyclass = 'loginpage';
View::header(); 
?>
<section class="breadcrumb">
   <article class="container">
      <div class="row">
         <div class="col-lg-6">
            <ul>
               <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
               <li><a href="<?php echo View::url(); ?>">Home</a></li>
               <li class="fa fa-angle-right"></li>
               <li><?php echo View::$title; ?></li>
            </ul>
         </div>
      </div>
   </article>
</section>

<section class="gray bg-image">
   <article class="container contactpage">
      <div class="row row-header">
         <div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
            <div class="start-project">
               <div class="block">

                  <!-- Tab panes -->
                  <div class="tab-content start-content">
                     <p class="font-20"><?php echo Lang::get('LOGIN_RESET_SUBTITLE'); ?></p>
                     <div class="alert hide" id="form-message">
                        <!-- Form Alert Goes Here -->
                     </div>
                     <form method="post">
                        <input type="hidden" name="action" value="request" />
                        <div id="basic-data" class="form-wizard active">
                           <?php echo View::getMessage(); ?>
                           <div class="form-group">
                              <input type="text" class="form-control" placeholder="<?php echo Lang::get('LOGIN_USERLABEL'); ?>" name="usr" required />
                           </div>
                           <div class="form-group text-center">
                              <button type="submit" class="btn btn-4 green default submit font-18"><?php echo Lang::get('LOGIN_RESET_BUTTON'); ?></button>
                           </div>
                           
                           
                           <br>

                           <div class="clearfix"></div>
                           <div class="divider"><span></span></div>

                           <div class="form-group text-left">
                              <a class="link" href="<?php echo View::url('users/login') ?>">Login<span class="text-gray"> I know my password </span></a>
                           </div>
                        </div>
                     </form>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </article>
</section>
<?php View::footer(); ?>