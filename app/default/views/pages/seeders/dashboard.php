<?php
View::$title = 'Dashboard';
View::$bodyclass = '';
View::header();
?>
    <?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

    <!-- ************************ Page Content ************************ -->

    <section class="gray">
        
        <article class="container projects-page" id="">

            <?php 
            $incomplete = false;
            if( user::info( 'UserLevel' ) == 'User' ){ 
                switch( Apputility::investorinfo( 'InvestorStatus' ) ){
                    case 'Verification':
                        echo '<div class="alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your profile is awaiting for verification! <a href="/users/profile"> Check Here.</a></div>';
                        break;
                    case 'Incomplete':
                        echo '<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Welcome to SeedOmega! You are one step away to get started. Please complete your profile <a href="/users/profile"> <b>HERE</b></a>.</div>';
                        $incomplete = true;
                        break;
                }
            } ?>
            <?php echo View::getMessage(); ?>

            <div class="row">

                <!-- ************************ Left Side Area ************************ -->
               
                <div class="col-md-9 col-sm-12 col-xs-12 content-cnt">
                    <div class="project-grid">

                        <?php foreach($projects as $proj):
                            $percentage = 0;
                            if($proj->TargetGoal > 0){
                                $percentage = ($proj->TotalRaisedAmt) ? (int)(($proj->TotalRaisedAmt / $proj->TargetGoal) * 100+.5) : 0;
                                $thedate = date('Y-m-d');
                            }
                            ?>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 project-cnt ccatid-<?php echo $proj->ClientCatID; ?> <?php echo ($proj->OfferOpening > $thedate) ? 'upcoming' : 'currentoffer'; ?> sortcatid-<?php echo $proj->ClientCatID; ?>">                                
                                <div class="percent-funded" style="display: none;"><?php echo $percentage; ?></div>
                                <div class="date-updated" style="display: none;"><?php echo date('Y-m-d', strtotime($proj->UpdatedAt )); ?></div>

                                <div class="popular-item">
                                    <figure class="project-image">
                                        <figcaption >
                                            <a href="/clients/view/<?php echo $proj->ClientProfileID;?>" class="item_button1">View</a>
                                            <?php 
                                            if(Apputility::investorinfo('InvestorStatus') == 'Approved' || User::info('UserLevel') == "Administrator"){ ?>
                                                <?php if($proj->TotalBookingAmounts >= $proj->TargetGoal && $proj->TypeOfOffer == 'Limited'){ ?>
                                                    <a href="/bookings/book/<?php echo $proj->ClientProfileID;?>" class="item_button2">Register</a>
                                                <?php }else{ ?>
                                                    <a href="/bookings/book/<?php echo $proj->ClientProfileID;?>" class="item_button2">Book</a>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <a href="#" class="item_button2 nonverified" data-toggle="modal" data-target="#myModal" style="cursor: not-allowed;">Book</a>
                                            <?php } ?>
                                        </figcaption>
                                        <?php $comphoto = View::common()->getUploadedFiles($proj->CompanyPhoto); ?>                                        
                                        <?php echo View::photo((isset($comphoto[0]) ? 'files'.$comphoto[0]->FileSlug : '/images/placeholder-proj.jpg'),"img"); ?>
                                    </figure>
                                    <div class="popular-content">
                                        <div class="project-desc project-desc2">
                                            <h5><a href="#"><?php echo $proj->CompanyName;?>  <span class="line-green"></span></a></h5>
                                            <p><?php echo AppUtility::excerptText($proj->OfferOverview,200,' <a href="/clients/view/'.$proj->ClientProfileID.'" class="item_button1">...read more</a>'); ?></p>
                                        </div>

                                        <?php 
                                        $barcolor = '#18aab2';
                                        if($percentage <= 35) { $barcolor = '#f2d031'; }
                                        if($percentage <= 75 && $percentage > 35) { $barcolor = '#3298c9'; }
                                        ?>
                                        <?php $avatar = View::common()->getUploadedFiles($proj->Avatar); ?>
                                        <div class="popular-data"> <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"funder"); ?>
                                            <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="<?php echo $barcolor; ?>" data-barsize="8">
                                              <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                                              <div class="pie_progress__label">Completed</div>
                                            </div>
                                        </div>

                                        <div class="popular-details">
                                            <ul>
                                                <li><strong><?php echo $proj->DaysLeft;?></strong> Days Left</li>
                                                <li><strong><?php echo isset($proj->TotalInvestor) ? number_format($proj->TotalInvestor) : "0";?></strong> Backers</li>
                                                <li class="last"><strong><?php echo isset($proj->TotalRaisedAmt) ? "$".number_format($proj->TotalRaisedAmt) : "0";?></strong> Funded</li>
                                            </ul>
                                        </div>                                        
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="clear"></div>                        

                        <!-- <div class="popular-btn"> <a href="#">See More</a> </div> -->
                    </div>
                </div>

                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="sidebar">

                        <div class="sidebar-item">
                            <div class="w-title">Search Projects</div>
                            <div class="w-content">
                                <form action="" method="get">
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="categories"><strong>Categories</strong></label>
                                            <select id="categories" name="categories" class="form-control arrow-down">
                                                <option value="*" selected="">All Categories</option>
                                                <?php 
                                                    foreach (Apputility::getClientCategories() as $clientcat) { ?>
                                                <option value=".ccatid-<?php echo $clientcat->ClientCatID; ?>"><?php echo ucwords($clientcat->CCatName); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="sr-only" for="country"><strong>Sort By</strong></label>
                                            <select id="projsortby" class="form-control arrow-down">
                                                <option value="*">Sort By</option>
                                                <option value="filter|.upcoming">Upcoming</option>
                                                <option value="sort|topfunded">Top Funded</option>
                                                <option value="sort|lowfunded">Low Funded</option>
                                                <option value="sort|dateupdated">Recently Added</option>
                                            </select>
                                        </div>
                                        
                                        <div class="clear"></div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                        <div class="sidebar-item">
                            <div class="w-title">Recently Viewed</div>
                            <div class="w-content">

                                <?php foreach($recentview as $project):
                                    $percentage = 0;
                                    if($project->TargetGoal > 0){
                                        $percentage = ($project->TotalRaisedAmt) ? (int)(($project->TotalRaisedAmt / $project->TargetGoal) * 100+.5) : 0;
                                        $thedate = date('Y-m-d');
                                    }
                                    ?>
                                    <div class="img-container ribbon ribbon-left ribbon-warning">
                                        <div class="ribbon-box font-w600"><?php echo isset($project->TypeOfOffer) ? $project->TypeOfOffer : "-";?></div>
                                        <?php $comphoto = View::common()->getUploadedFiles($project->CompanyPhoto); ?>                                        
                                        <?php echo View::photo((isset($comphoto[0]) ? 'files'.$comphoto[0]->FileSlug : '/images/placeholder-proj.jpg'),"img","img-responsive","","max-height:140px;"); ?>
                                        <h3 class="h5 font-w600 push-10 push-10-t">
                                            <a class="link-effect" href="/clients/view/<?php echo $project->ClientProfileID;?>"><?php echo $project->CompanyName;?></a>
                                        </h3>
                                        <div class="font-s13 text-muted hidden-xs">
                                            <p class="text-muted"><?php echo AppUtility::excerptText($project->OfferOverview,70,' ...'); ?>.</p>
                                        </div>
                                        <div class="row items-push text-center">
                                            <div class="col-xs-6">
                                                <div class="push-5"><i class="si si-pie-chart fa-2x"></i></div>
                                                <div class="h5 font-w300 text-muted"><?php echo $percentage; ?> %</div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="push-5"><i class="si si-bar-chart fa-2x"></i></div>
                                                <div class="h5 font-w300 text-muted">$ <?php echo isset($project->TotalRaisedAmt) ? number_format($project->TotalRaisedAmt) : "0";?></div>
                                            </div>
                                        </div>
                                        <div class="img-options">
                                            <div class="img-options-content">
                                                <a class="btn btn-sm btn-default" href="/clients/view/<?php echo $project->ClientProfileID;?>"><i class="si si-eye"></i> View Details</a>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <hr class="push-10-t push-10">
                                <?php endforeach; ?>

                                <div class="clear"></div>
                                <div class="text-center">
                                    <a href="/users/recentlyviewed" class="btn btn-sm btn-rounded btn-success push-10-t push-10"><i class="si si-eye text-gray"></i> View More</a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div><!-- End Row -->


            <div class="clear"></div>

            <?php if($incomplete){ ?>
            <div id="dialog" class="w3-modal">
                <div class="w3-modal-content" style="width: 600px;">
                    <div id="dialog-form" class="text-center">
                        <div class="alert alert-danger fade in">You are 1 step away to get started please complete your profile!</div>
                        <a href="/users/profile" class="btn btn-3 blue" style="margin:10px 0px 5px 0px;"><i class="fa fa-pencil text-gray"></i>  Complete Profile</a>&nbsp;
                        <a href="/clients/view/<?php echo $proj->ClientProfileID;?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;"><i class="si si-eye text-gray"></i>  View</a>&nbsp;
                        <a href="#" class="btn btn-3 red" style="margin:10px 0px 5px 0px;" onclick="document.getElementById('dialog').style.display='none'"><i class="si si-close text-gray"></i>  Cancel</a><br><br>
                    </div>
                </div>
            </div>            
            <?php } ?>

        </article>
    </section>

<?php View::footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        // $('#projlist').easyPaginate({
        //     paginateElement : 'article',
        //     elementsPerPage: 15,
        //     effect: 'fade',
        //     slideOffset : 100,
        //     nextButtonText : "Next",
        //     prevButtonText : "Prev",
        //     lastButtonText : "Last",
        //     firstButtonText: "First"
        // });

        $('.nonverified').click(function(){
            var refTop, diaHeight;
            $('#dialog').css('display', 'block');
            diaHeight = $('#dialog').height();
            refTop = $('#popular').offset().top;
            $('#dialog').css('padding-top', (refTop - 85) + 'px');
            $('#myModal').css('padding-top', (refTop / 2) + 'px');
        });

        // init Isotope
        var $grid = $('.project-grid').isotope({
            itemSelector: '.project-cnt',
            layoutMode: 'fitRows',
            getSortData: {
              topfunded: '.percent-funded parseInt',
              lowfunded: '.percent-funded parseInt',
              dateupdated : '.date-updated parseInt'
            }
        });

        // bind filter button click
        $('#categories').on('change', function() {
            var filterValue = $( this ).val();

            // use filterFn if matches value
            $grid.isotope({ filter: filterValue });
        });

        $('#projsortby').on('change', function() {

            var $optvalue = $(this).val();
            var $vals = $optvalue.split('|');
            var filterByValue = $vals[1];

            if($vals[0] == 'sort'){
                if($vals[1] == 'lowfunded'){ 
                    $sorting = true; 
                }else{ 
                    $sorting = false; 
                }
              $grid.isotope({ sortBy: filterByValue, sortAscending: $sorting });
            }else{
              $grid.isotope({ filter: filterByValue });
            }
        });

    });
</script>