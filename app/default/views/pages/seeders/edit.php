<?php 
View::$title = 'Edit User';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>

<?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

<section class="gray">
    <article class="container project-single">        
        <div class="start-project">
            <!-- Main Content -->        
            <div class="block items-push">
                <div class="block-content tab-content">
                    <?php echo View::getMessage(); ?>
                    <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="updateuser" />
                        <input type="hidden" name="userid" value="<?php echo $user->UserID; ?>" />
                        <input type="hidden" name="metaid" value="<?php echo $user->UserMetaID; ?>" />
                        <input type="hidden" name="avatarid" value="<?php echo $user->Avatar; ?>" />

                        <div class="form-group">
                            
                            <label class="control-label col-md-3 col-sm-3 col-xs-12 bottom-align-text" for="fname"></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="avatar-container personal">
                                    <?php $avatar = View::common()->getUploadedFiles($user->Avatar); ?>                                        
                                    <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar"); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            
                            <label class="control-label col-md-3 col-sm-3 col-xs-12 bottom-align-text" for="fname"><?php echo Lang::get('USR_EDIT_PPCTURE'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                <span>* Allowed file types: jpeg, jpg, png</span>
                            </div>
                        </div>
                        <?php if(User::can('Administer All')): ?>
                        <!-- <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('CLN_EDIT_RC'); ?> / <?php echo Lang::get('CLN_EDIT_RN'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <input type="text" name="user[ReferrerUserID]" value="<?php echo $user->ReferrerUserID; ?>" id="ReferrerUserID">
                                        <input type="text" id="ReferrerUserChecker" class="form-control" value="<?php echo $user->ReferrerUserID; ?>" data-value="<?php echo $user->UserID; ?>" rel="<?php echo View::url('ajax/userinfo'); ?>"><span id="referrerloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <input type="text" id="referrerdata" class="form-control" value="" disabled="">
                                    </div>
                                </div>
                            </div>                                            
                            </div> -->
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">
                            <?php echo Lang::get('USR_EDIT_ULVL'); ?>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?php 
                                    View::form(
                                        'selecta',
                                        array(
                                            'name'=>'user[Level]',
                                            'options'=>$levels,
                                            'value'=>$user->Level,
                                            'class'=>'form-control col-md-7 col-xs-12'
                                            //'inarray'=>$inarray
                                        )
                                    );  
                                    ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_PLANG'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <?php View::form('selecta',array('name'=>'meta[Language]','options'=>AppUtility::getLanguages(),'class'=>'form-control','value'=>$user->Language)); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lname">
                            <?php echo Lang::get('USR_EDIT_LN'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo $user->LastName; ?>" id="lname" name="meta[LastName]" required="required" class="form-control uppercase col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">
                            <?php echo Lang::get('USR_EDIT_FN'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo $user->FirstName; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control uppercase col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nname" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_NN'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo $user->NickName; ?>" id="nname" name="meta[NickName]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                            <?php echo Lang::get('USR_EDIT_EML'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="email" id="EmailChecker2" name="user[Email]" class="form-control col-md-7 col-xs-12" value="<?php echo isset($user->Email) ? $user->Email : ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($user->UserID) ? $user->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">
                            <?php echo Lang::get('USR_EDIT_PHNE'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="phone" value="<?php echo $user->Phone; ?>" id="phone" name="meta[Phone]" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">
                            <?php echo Lang::get('USR_EDIT_GNDR'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <p>
                                    <label class="css-input css-radio css-radio-info push-10-r">
                                        <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $user->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                    </label>
                                    <label class="css-input css-radio css-radio-info">
                                        <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $user->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                    </label>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_ADDS'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo $user->Address; ?>" id="address" name="meta[Address]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_CTY'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo $user->City; ?>" id="city" name="meta[City]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_STATES'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo $user->State; ?>" id="state" name="meta[State]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_CNTRY'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <select id="Country" name="meta[Country]" class="form-control">
                                    <?php foreach(AppUtility::getCountries() as $country) { ?>
                                    <option value="<?php echo $country; ?>" <?php echo $user->Country == $country ? 'selected' : ''; ?>><?php echo $country; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="postal" class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_PCODE'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <input type="text" value="<?php echo isset($user->PostalCode) ? $user->PostalCode : ''; ?>" id="postal" name="meta[PostalCode]" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo Lang::get('USR_EDIT_RMRKS'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <textarea class="form-control dowysiwyg" name="meta[Bio]"><?php echo $user->Bio; ?></textarea>
                            </div>
                        </div>
                        <?php if(User::can('Administer All')): ?>
                        <div class="ln_solid"></div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Load Default Permissions</label>
                            <div class="col-md-7 col-sm-9 col-xs-9 caplist">
                                <select id="defaultPermissions" class="form-control" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                    <option value="">Select</option>
                                    <?php foreach ($roles as $role) { ?>
                                    <option value="<?php echo View::url('users/edit/'.$user->UserID.'/'. $role->UserLevelID); ?>"><?php echo isset($role->Name) ? $role->Name : ""; ?></option>
                                    <?php } ?>
                                </select>
                                <small class="red">*This will be loaded and check all the default settings as per selected role but will not be saved</small><br>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3"><?php echo Lang::get('USR_EDIT_CAP'); ?></label>
                            <div class="col-md-7 col-sm-7 col-xs-9 caplist">
                                <label class="css-input css-checkbox css-checkbox-sm css-checkbox-default"><input type="checkbox" id="check-toggle" value=""> <span></span>Check All / Uncheck All</label>
                            </div>
                        </div>
                        <?php 
                            if($loadedcapa) {
                                $userCapa = $loadedcapa;  
                            } else { 
                                $userCapa = isset($user->Capability) ? View::common()->stringToArray($user->Capability) : array(); 
                            } 
                            foreach($capabilities as $ckey => $cval){ 
                                ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3"><?php echo $ckey; ?></label>
                            <div class="col-md-7 col-sm-9 col-xs-9">
                                <?php if(is_array($cval)) { ?>
                                    <div class="row">
                                        <?php foreach($cval as $citem){ ?>
                                            
                                            <div class="col-md-4 col-sm-6 col-xs-12 caplist">
                                                <label class="css-input css-radio css-radio-sm css-radio-default"><input type="checkbox" id="capability-<?php echo $citem->UserCapabilityID; ?>" name="capabilities[<?php echo $citem->UserCapabilityID; ?>]" class="capabilities" value="<?php echo $citem->Name; ?>" <?php echo isset($userCapa[$citem->UserCapabilityID]) ? 'checked' : ''; ?> <?php echo $citem->Name == 'Administer All' ? 'readonly="readonly"' : ''; ?>><span></span> <?php echo $citem->Name; ?></label>
                                            </div>
                                            
                                        <?php }; ?>
                                    </div>
                                <?php }; ?>
                            </div>
                        </div>
                        <?php }
                            ?>
                        <?php endif; ?>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">
                            <?php echo Lang::get('USR_EDIT_STATUS'); ?> <span class="required">*</span>
                            </label>
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <p><?php $active = $user->Active; ?>
                                    <label class="css-input css-radio css-radio-info push-10-r">
                                        <input type="radio" name="user[Active]" value="1" <?php echo $active == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_EDIT_STATUSA'); ?>
                                    </label>
                                    <label class="css-input css-radio css-radio-info">
                                        <input type="radio" name="user[Active]" value="0" <?php echo $active == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_EDIT_STATUSI'); ?>
                                    </label>
                                </p>
                            </div>
                        </div>
                        <br><hr><br>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo View::url('users/list'); ?>" class="btn btn-4 blue yellow">Back</a>
                                <button type="submit" class="btn btn-4 blue green"><?php echo Lang::get('USR_EDIT_SVBTN'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Main Content -->
        </div>        
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>