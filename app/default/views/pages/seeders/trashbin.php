<?php 
View::$title = 'Trashed Users';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<form method="post" action="">
<input type="hidden" name="doemptytrashbin">

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <div class="block-header">
            <h3 class="block-title">All Capability <small></small>
                <button type="submit" class="btn btn-success pull-right" onclick="return confirm('Are you sure you want to delete trashed users permanently? If you continue you will be able to undo this action!');"><?php echo Lang::get('USR_TRSH_EMPBTN'); ?></button></h3>
        </div>

        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage(); ?> 
                <table class="table js-table-checkable js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" cellspacing="0" width="100%">
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-align"><?php echo Lang::get('USR_MNG_IMG'); ?></th>
                            <th class="sort-this"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_EML'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_GNDR'); ?></th>                                    
                            <th><?php echo Lang::get('USR_MNG_PHNE'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_LVL'); ?></th>
                            <th class="no-sorting text-center" style="text-align:center; width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if(count($users)) {
                        foreach($users as $user) { $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <?php $avatar = View::common()->getUploadedFiles($user->Avatar); ?>
                            <td><img src="<?php echo View::url('assets/'. ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' )); ?>" style="width:40px;" /></td>
                            <td><?php echo $user->Code; ?>-<?php echo $user->UserID; ?><input type="hidden" name="uids[]" value="<?php echo $user->UserID; ?>" /></td>
                            <td><?php echo $user->Email; ?></td>
                            <td><?php echo $user->LastName; ?> <?php echo $user->FirstName; ?></td>
                            <td><?php echo $user->Gender; ?></td>
                            <td><?php echo $user->Phone; ?></td>
                            <td><?php echo $user->Name; ?></td>
                            <td style="text-align:center;">
                                <?php if($userinfo->Level == 1) { ?>
                                <a href="<?php echo View::url('users/restore/'.$user->UserID); ?>" title="Edit" class="btn btn-xs btn-default" data-toggle="tooltip"><i class="fa fa-undo"></i></a>
                                <a href="<?php echo View::url('users/delete/'.$user->UserID); ?>" title="Delete" class="btn btn-xs btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete user <?php echo $user->FirstName; ?> <?php echo $user->LastName; ?>? If you continue you will be able to undo this action!');"><i class="fa fa-times"></i></a>
                                <?php } ?>

                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr>
                            <!--td class="a-center"><input type="checkbox" name="table_records" value="<?php //echo $user->UserID; ?>" class="flat"></td-->
                            <td>No Data</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

</form>

<!-- /page content -->
<?php View::footer(); ?>