<?php
$status = '';
$mytitle = "My Profile";
if(isset($profile)){
    if($profile != NULL){
        $status = ($profile->Status) ? '<br><small>('.$profile->AccountType.' - '.$profile->Status.')</small>' : '';
        if($profile->UserID != $currentuser->UserID){
            $mytitle = $fname.' '.$lname;
        }
    }
}
View::$title = $mytitle;
View::$bodyclass = User::info('Sidebar');
View::header();
?>
    <?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

    <!-- ************************ Page Content ************************ -->
    <section class="gray">
        <article class="container project-single">
            <div class="start-project" id="profile_wizard">
                
                <div class="title">
                    <h4 class="text-center" style="color: #18aab2;"><?php echo $percentcomplete; ?>% Completed</h4>
                    <ul id="profileTabs">
                        <li data-link="my-profile" class="<?php echo $thispage == 'documents' ? 'current' : ''; ?>" id="tab-my-profile">
                        <a href="<?php View::url('users/profile/documents', true); ?><?php echo ($thisid) ? '/'.$thisid : ''; ?>"><i class="fa fa-file"></i><span>My Documents</span></a></li>
                        <li data-link="my-details" class="<?php echo $thispage == 'details' ? 'current' : ''; ?>" id="tab-my-details">
                        <a href="<?php View::url('users/profile/details', true); ?><?php echo ($thisid) ? '/'.$thisid : ''; ?>" ><i class="fa fa-user"></i><span>My Details</span></a></li>
                        <li data-link="my-other" class="<?php echo $thispage == 'other' ? 'current' : ''; ?>" id="tab-my-other">
                        <a href="<?php View::url('users/profile/other', true); ?><?php echo ($thisid) ? '/'.$thisid : ''; ?>"><i class="fa fa-photo"></i><span>Other</span></a></li>
                    </ul>
                </div>
                <hr>                

                <!-- Main Content -->
                <div class="start-content">
                    <?php echo View::getMessage(); ?>
                    <div id="my-profile" class="form-wizard <?php echo $thispage == 'documents' ? 'active' : ''; ?>">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="updateprofile" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="acctid" value="<?php echo $profile->AccountID; ?>" />
                            <input type="hidden" name="IdPhotoID" value="<?php echo $profile->IdPhoto; ?>" />
                            <input type="hidden" name="AddressPhotoID" value="<?php echo $profile->AddressPhoto; ?>" />

                            <div class="form-group"><h3 class="text-center">Upload Documents</h3></div>

                            <div class="upload-documents"><?php echo AppUtility::getInvestorFileList($profile, true); ?></div>

                            <?php if(User::info('UserLevel') == "Administrator" || User::can('Manage Profiles')){ ?>
                            <div class="form-group">
                                <label>Change Status</label>
                                <select name="acct[Status]" class="form-control">
                                    <option value="Incomplete" <?php echo ($profile->Status == "Incomplete") ? "selected" : ""; ?>>Incomplete</option>
                                    <option value="Verification" <?php echo ($profile->Status == "Verification") ? "selected" : ""; ?>>Verification</option>                                            
                                    <option value="Approved" <?php echo ($profile->Status == "Approved") ? "selected" : ""; ?>>Approved</option>
                                </select>
                                <input type="hidden" value="<?php echo $profile->Status; ?>" name="oldstatus">
                            </div>
                            <?php } ?>

                            <br><hr><br>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-4 blue green font-18" type="submit"><i class="fa fa-check push-5-r"></i> Update</button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div id="my-details" class="form-wizard <?php echo $thispage == 'details' ? 'active' : ''; ?>">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="updateprofile" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="acctid" value="<?php echo $profile->AccountID; ?>" />
                            <div class="form-group"><h3 class="text-center">Member Details</h3></div>
                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_PRF_FN'); ?> <span class="required">*</span></label>
                                    <input type="text" value="<?php echo $userinfo->FirstName; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_PRF_LN'); ?> <span class="required">*</span></label>
                                    <input type="text" value="<?php echo $userinfo->LastName; ?>" id="lname" name="meta[LastName]" required="required" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_PRF_EML'); ?> <span class="required">*</span></label>
                                    <input type="email" id="EmailChecker2" name="user[Email]" class="form-control" value="<?php echo isset($userinfo->Email) ? $userinfo->Email : ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($userinfo->UserID) ? $userinfo->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>">
                                    <span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_PRF_PHNE'); ?> <span class="required">*</span></label>
                                    <input type="phone" value="<?php echo $userinfo->Phone; ?>" id="phone" name="meta[Phone]" required="required" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label><?php echo Lang::get('USR_PRF_ADDS'); ?></label>
                                <input type="text" value="<?php echo $userinfo->Address; ?>" id="address" name="meta[Address]" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_PRF_CTY'); ?></label>
                                    <input type="text" value="<?php echo $userinfo->City; ?>" id="city" name="meta[City]" class="form-control" required="required">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_PRF_STATES'); ?></label>
                                    <input type="text" value="<?php echo $userinfo->State; ?>" id="state" name="meta[State]" class="form-control" required="required">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_PRF_CNTRY'); ?></label>
                                    <select name="meta[Country]" class="form-control form-control" required="required">
                                        <?php foreach(AppUtility::getCountries() as $country) { ?>
                                            <option value="<?php echo $country; ?>" <?php echo $userinfo->Country == $country ? 'selected' : ''; ?>><?php echo $country; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-right">
                                    <label for="postal" class="control-label"><?php echo Lang::get('USR_PRF_PCODE'); ?></label>
                                    <input type="text" value="<?php echo isset($userinfo->PostalCode) ? $userinfo->PostalCode : ''; ?>" id="postal" name="meta[PostalCode]" class="form-control" required="required">
                                </div>
                            </div>
                            
                            <?php if(User::info('UserLevel') == "Administrator" || User::can('Manage Profiles')){ ?>
                            <div class="form-group">
                                <label>Change Status</label>
                                <select name="acct[Status]" class="form-control">
                                    <option value="Incomplete" <?php echo ($profile->Status == "Incomplete") ? "selected" : ""; ?>>Incomplete</option>
                                    <option value="Verification" <?php echo ($profile->Status == "Verification") ? "selected" : ""; ?>>Verification</option>                                            
                                    <option value="Approved" <?php echo ($profile->Status == "Approved") ? "selected" : ""; ?>>Approved</option>
                                </select>
                                <input type="hidden" value="<?php echo  $profile->Status; ?>" name="oldstatus">
                            </div>
                            <?php } ?>

                            <br><hr><br>

                            <?php if($profile->AccountType == 'Corporate') { ?>
                            <div class="accordion" id="accordion-faq">
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                                            <strong>Additional Information</strong>
                                            <span class="updown-cion"></span>
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="accordion-body collapse in" aria-expanded="true" style="">
                                        <div class="accordion-inner">
                                            <label>Security Question</label>
                                            <textarea class="form-control dowysiwyg" name="acct[SecurityAnswer]"><?php echo $profile->SecurityAnswer; ?></textarea>
                                        </div>

                                        <div class="accordion-inner">
                                            <p>Within the last 10 years, have you worked in an investment-related field for 3 or more
                                                consecutive years?</p>
                                            <div>
                                                <label class="css-input css-radio css-radio-primary push-10-r">
                                                    <input type="radio" class="flat" name="acct[WorkedInInvestmentField]" id="WorkedInInvestmentFieldNo" value="0" <?php echo $profile->WorkedInInvestmentField == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                </label>
                                                <label class="css-input css-radio css-radio-primary">
                                                    <input type="radio" class="flat" name="acct[WorkedInInvestmentField]" id="WorkedInInvestmentFieldY" value="1" <?php echo $profile->WorkedInInvestmentField== '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="accordion-inner">
                                            <p><?php echo Lang::get('USR_PRF_INVQS_EMPSTAT'); ?></p>
                                            <?php View::form('selecta',array('name'=>'acct[EmploymentStatus]','options'=>['Employed'=>'Employed','Homemaker'=>'Homemaker','Retired'=>'Retired','Self-Employed'=>'Self-Employed','Student'=>'Student','Unemployed'=>'Unemployed'],'class'=>'form-control','value'=>$profile->EmploymentStatus)); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                                            <strong>Education</strong>
                                            <span class="updown-cion"></span>
                                        </a>
                                    </div>
                                    <div id="collapseTwo" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="accordion-inner">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_EDULEVEL'); ?></label>
                                                <?php View::form('selecta',array('name'=>'acct[HighestLevelEdu]','options'=>['Graduate'=>'Graduate','Masters'=>'Masters','College'=>'College','Undergraduate'=>'Undergraduate','Postgraduate'=>'Postgraduate'],'class'=>'form-control','value'=>$profile->HighestLevelEdu)); ?>
                                            </div>

                                            <div class="accordion-inner">
                                                <label><?php echo Lang::get('USR_PRF_FINCERT'); ?></label>
                                                <textarea class="form-control dowysiwyg" name="acct[FinanceCertification]"><?php echo $profile->FinanceCertification; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false">
                                            <strong>Investment Objective</strong>
                                            <span class="updown-cion"></span>
                                        </a>
                                    </div>
                                    <div id="collapseThree" class="accordion-body collapse" aria-expanded="false">
                                        <div class="accordion-inner">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveIncome]" id="InvestObjectiveIncomeHigh" value="High" <?php echo $profile->InvestObjectiveIncome == 'High' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveIncome]" id="InvestObjectiveIncomeMedium" value="Medium" <?php echo $profile->InvestObjectiveIncome== 'Medium' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveIncome]" id="InvestObjectiveIncomeLow" value="Low" <?php echo $profile->InvestObjectiveIncome== 'Low' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_INVOBJCT_FORMSPEC_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecHigh" value="High" <?php echo $profile->InvestObjectiveFormSpec == 'High' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecMedium" value="Medium" <?php echo $profile->InvestObjectiveFormSpec== 'Medium' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveFormSpec]" id="InvestObjectiveFormSpecLow" value="Low" <?php echo $profile->InvestObjectiveFormSpec == 'Low' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_INVOBJCT_INVRISK_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskHigh" value="High" <?php echo $profile->InvestObjectiveInvestRisk == 'High' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_HIGH'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskMedium" value="Medium" <?php echo $profile->InvestObjectiveInvestRisk== 'Medium' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_MEDIUM'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[InvestObjectiveInvestRisk]" id="InvestObjectiveInvestRiskLow" value="Low" <?php echo $profile->InvestObjectiveInvestRisk == 'Low' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVOBJCT_INCOME_LOW'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false">
                                            <strong>Income and Net Worth</strong>
                                            <span class="updown-cion"></span>
                                        </a>
                                    </div>
                                    <div id="collapseFour" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="accordion-inner">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_ANNUAL_LBL'); ?></label>
                                                <div>
                                                    <?php View::form('selecta',array('name'=>'acct[AnnualIncome]','options'=>['0-5000'=>'0-5000 USD','5001-10000'=>'5,001-10,000 USD','10001 - 50000'=>'10,001 - 50,000 USD','50001 - 100000'=>'50,000 - 100,000 USD'],'class'=>'form-control','value'=>$profile->AnnualIncome)); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_NET_LBL'); ?></label>
                                                <div>
                                                    <?php View::form('selecta',array('name'=>'acct[NetWorth]','options'=>['0-5000'=>'0-5000 USD','5001-10000'=>'5,001-10,000 USD','10001 - 50000'=>'10,001 - 50,000 USD','50001 - 100000'=>'50,000 - 100,000 USD'],'class'=>'form-control','value'=>$profile->NetWorth)); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_INVOBJCT_INCOMENETWORTH_DESC_LBL'); ?></label>
                                                <div>
                                                    <?php View::form('selecta',array('name'=>'acct[SourceOfFunds]','options'=>['Employment of Wages'=>'Employment of Wages','Gift'=>'Gift','Inheritance/Trust'=>'Inheritance/Trust','Investments'=>'Investments','Legal Settlement'=>'Legal Settlement','Lottery/Gaming'=>'Lottery/Gaming','Retirement Funds'=>'Retirement Funds','Savings'=>'Savings','Spouse/Parent Support'=>'Spouse/Parent Support','Unemployment/Disability'=>'Unemployment/Disability','Other'=>'Other'],'class'=>'form-control','value'=>$profile->SourceOfFunds)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false">
                                            <strong>Affiliations &amp; Disclosures</strong>
                                            <span class="updown-cion"></span>
                                        </a>
                                    </div>
                                    <div id="collapseFive" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="accordion-inner">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_AFFDISC_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[AssociatedWithBroker]" id="AssociatedWithBrokerN" value="0" <?php echo $profile->AssociatedWithBroker == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[AssociatedWithBroker]" id="AssociatedWithBrokerY" value="1" <?php echo $profile->AssociatedWithBroker == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_AFFDISC_PUBTRADED_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[PublicTradedCompany]" id="PublicTradedCompanyN" value="0" <?php echo $profile->PublicTradedCompany == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[PublicTradedCompany]" id="PublicTradedCompanyY" value="1" <?php echo $profile->PublicTradedCompany == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_AFFDISC_INSIDERS_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[AffInsiders]" id="AffInsidersN" value="0" <?php echo $profile->AffInsiders == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[AffInsiders]" id="AffInsidersY" value="1" <?php echo $profile->AffInsiders == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_AFFDISC_POLITICS_LBL'); ?></label>
                                                <div>
                                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                                        <input type="radio" class="flat" name="acct[PoliticalExposedPerson]" id="PoliticalExposedPersonN" value="0" <?php echo $profile->PoliticalExposedPerson == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELNO'); ?>
                                                    </label>
                                                    <label class="css-input css-radio css-radio-primary">
                                                        <input type="radio" class="flat" name="acct[PoliticalExposedPerson]" id="PoliticalExposedPersonY" value="1" <?php echo $profile->PoliticalExposedPerson == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_INVQS_INVRELYES'); ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="accordion-group">
                                    <div class="accordion-heading">
                                        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false">
                                            <strong>Stock Trading Experiences</strong>
                                            <span class="updown-cion"></span>
                                        </a>
                                    </div>
                                    <div id="collapseSix" class="accordion-body collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="accordion-inner">
                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_STOCK_YRS_LBL'); ?></label>
                                                <div>
                                                    <?php View::form('selecta',array('name'=>'acct[YearsOfTrading]','options'=>range(0, 30),'class'=>'form-control','value'=>$profile->YearsOfTrading)); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><?php echo Lang::get('USR_PRF_STOCK_TRADESPAST3_LBL'); ?></label>
                                                <div>
                                                    <?php View::form('selecta',array('name'=>'acct[TradesMadePast3Yrs]','options'=>range(0, 30),'class'=>'form-control','value'=>$profile->TradesMadePast3Yrs)); ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-4 blue green font-18" type="submit"><i class="fa fa-check push-5-r"></i> Update</button>
                                </div>
                            </div>

                        </form>
                    </div>

                    <div id="my-other" class="form-wizard <?php echo $thispage == 'other' ? 'active' : ''; ?>">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="action" value="updatepersonal" />
                            <input type="hidden" name="userid" value="<?php echo $userinfo->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $userinfo->UserMetaID; ?>" />
                            <input type="hidden" name="avatarid" value="<?php echo $userinfo->Avatar; ?>" />

                            <div class="form-group">
                                <div class="form-left">
                                    <div class="avatar-container personal">
                                    <?php $avatar = View::common()->getUploadedFiles($userinfo->Avatar); ?>
                                    <?php echo  View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar"); ?>
                                    </div>
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_PRF_PPCTURE'); ?> </label>
                                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <span class="text-muted">Allowed file types: jpeg, jpg, png</span>                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label><?php echo Lang::get('USR_PRF_GNDR'); ?> <span class="required">*</span></label>
                                <div>
                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                        <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $userinfo->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                    </label>
                                    <label class="css-input css-radio css-radio-primary">
                                        <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $userinfo->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-left">
                                    <label>Occupation</label>
                                    <input type="text" value="<?php echo $userinfo->Occupation; ?>" name="meta[Occupation]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label>JobTitle</label>
                                    <input type="text" value="<?php echo $userinfo->JobTitle; ?>" name="meta[JobTitle]" class="form-control">
                                </div>
                            </div>
                            <br><hr><br>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-4 blue green font-18" type="submit"><i class="fa fa-check push-5-r"></i> Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END Main Content -->
            </div>
        </article>
    </section>

<?php View::footer(); ?>