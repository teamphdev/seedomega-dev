<?php
View::$title = 'Recently Viewed Projects';
View::$bodyclass = '';
View::header();
?>

    <?php View::template('users/banner'); View::template('users/breadcrumb'); ?>

    <!-- ************************ Page Content ************************ -->

    <section class="gray">
        
        <article class="container projects-page" id="popular">

            <?php 
            $incomplete = false;
            if(user::info('UserLevel') == 'User') { 
                switch (Apputility::investorinfo('InvestorStatus')) {
                    case 'Verification':
                        echo '<div class="alert alert-info fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your profile is awaiting for verification! <a href="/users/profile"> Check Here.</a></div>';
                        break;
                    case 'Incomplete':
                        echo '<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Welcome to SeedOmega! You are one step away to get started. Please complete your profile <a href="/users/profile"> <b>HERE</b></a>.</div>';
                        $incomplete = true;
                        break;
                }
            } ?>
            <?php echo View::getMessage(); ?>

            <div class="row">

                <!-- ************************ Left Side Area ************************ -->
                <div class="col-lg-12 col-xs-12">
                    <?php
                        $ctr = 0;
                        if( isset( $projects ) ){ if( count( $projects ) ){
                            foreach ($projects as $project) {
                                if( $project->Status == 'Pending' ){
                                    $ctr++;
                                }
                            }
                        }
                    } ?>
                    <div class="block-header push-10" style="padding: 10px 15px;">
                        <span class="badge badge-danger"><?php echo $ctr; ?></span> Pending(s) - Upload TT Report
                    </div>
                </div>
                <div class="col-lg-12 content-cnt">
                    <article class="container projects-page row">
                        <div class="project-grid">

                        <?php foreach($projects as $project):
                            $percentage = 0;
                            if($project->TargetGoal > 0){
                                $percentage = ($project->TotalRaisedAmt) ? (int)(($project->TotalRaisedAmt / $project->TargetGoal) * 100+.5) : 0;
                                $thedate = date('Y-m-d');
                            }
                            ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 project-cnt ccatid-<?php echo  $project->ClientCatID; ?> <?php echo  ($project->OfferOpening > $thedate) ? 'upcoming' : 'currentoffer'; ?> sortcatid-<?php echo $project->ClientCatID; ?>">                                
                                <div class="percent-funded" style="display: none;"><?php echo  $percentage; ?></div>
                                <div class="date-updated" style="display: none;"><?php echo  date('Y-m-d', strtotime($project->UpdatedAt )); ?></div>

                                <div class="popular-item">
                                    <figure class="project-image">
                                        <figcaption >
                                            <a href="/clients/view/<?php echo $project->ClientProfileID;?>" class="item_button1">View</a>
                                            <?php 
                                            if(Apputility::investorinfo('InvestorStatus') == 'Approved' || User::info('UserLevel') == "Administrator"){ ?>
                                                <?php if($project->TotalBookingAmounts >= $project->TargetGoal && $project->TypeOfOffer == 'Limited'){ ?>
                                                    <a href="/bookings/book/<?php echo $project->ClientProfileID;?>" class="item_button2">Register</a>
                                                <?php }else{ ?>
                                                    <a href="/bookings/book/<?php echo $project->ClientProfileID;?>" class="item_button2">Book</a>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <a href="#" class="item_button2 nonverified" data-toggle="modal" data-target="#myModal" style="cursor: not-allowed;">Book</a>
                                            <?php } ?>
                                        </figcaption>
                                        <?php $comphoto = View::common()->getUploadedFiles($project->CompanyPhoto); ?>                                        
                                        <?php echo View::photo((isset($comphoto[0]) ? 'files'.$comphoto[0]->FileSlug : '/images/placeholder-proj.jpg'),"img"); ?>
                                    </figure>
                                    <div class="popular-content">
                                        <div class="project-desc project-desc2">
                                            <h5><a href="#"><?php echo $project->CompanyName;?>  <span class="line-green"></span></a></h5>
                                            <p><?php echo AppUtility::excerptText($project->OfferOverview,200,' <a href="/clients/view/'.$project->ClientProfileID.'" class="item_button1">...read more</a>'); ?></p>
                                        </div>

                                        <!--div class="project-info row">
                                          <div class="col-xs-6 col-sm-6 text-left">
                                            <span><?php echo mb_strimwidth(isset($project->TypeOfOffer) ? $project->TypeOfOffer: "-", 0, 12, ""); ?> <br><small class="text-muted">TYPE OF OFFER</small></span>
                                          </div>
                                          <div class="col-xs-6 col-sm-6 text-right">
                                            <span><?php echo isset($project->SizeOfOffer) ? "$".$project->SizeOfOffer : "-";?> <br><small class="text-muted">SIZE OF OFFER</small></span>
                                          </div>
                                          <div class="clear"></div>
                                          <div class="col-xs-6 col-sm-6 text-left">
                                            <span><?php echo isset($project->TargetGoal) ? "$".number_format($project->TargetGoal) : "-";?> <br><small class="text-muted">TARGET GOAL</small></span>
                                          </div>                                    
                                          <div class="col-xs-6 col-sm-6 text-right">
                                            <span><?php echo isset($project->Price) ? "$".number_format($project->Price,2) : "-";?> <br><small class="text-muted">PRICE</small></span>
                                          </div>                                      
                                        </div>
                                        <div class="clear"></div>

                                        <div class="project-progressbar progress active">
                                            <div class="progress-bar progress-bar-success progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><small><?php echo $percentage; ?>%</small></div>
                                        </div-->
                                        <?php 
                                        $barcolor = '#18aab2';
                                        if($percentage <= 35) { $barcolor = '#f2d031'; }
                                        if($percentage <= 75 && $percentage > 35) { $barcolor = '#3298c9'; }
                                        ?>
                                        <?php $avatar = View::common()->getUploadedFiles($project->Avatar); ?>
                                        <div class="popular-data"> <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"funder"); ?>
                                            <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="<?php echo $barcolor; ?>" data-barsize="8">
                                              <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                                              <div class="pie_progress__label">Completed</div>
                                            </div>
                                        </div>

                                        <div class="popular-details">
                                            <ul>
                                                <li><strong><?php echo $project->DaysLeft;?></strong> Days Left</li>
                                                <li><strong><?php echo isset($project->TotalInvestor) ? number_format($project->TotalInvestor) : "0";?></strong> Backers</li>
                                                <li class="last"><strong><?php echo isset($project->TotalRaisedAmt) ? "$".number_format($project->TotalRaisedAmt) : "0";?></strong> Funded</li>
                                            </ul>
                                        </div>                                        
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="clear"></div>                        

                        <!-- <div class="popular-btn"> <a href="#">See More</a> </div> -->
                        </div>
                    </article>
                    </div>
                </div>

                <?php if($incomplete){ ?>
                <div id="dialog" class="w3-modal">
                    <div class="w3-modal-content" style="width: 600px;">
                        <div id="dialog-form" class="text-center">
                            <div class="alert alert-danger fade in">You are 1 step away to get started please complete your profile!</div>
                            <a href="/users/profile" class="btn btn-3 blue" style="margin:10px 0px 5px 0px;"><i class="fa fa-pencil text-gray"></i>  Complete Profile</a>&nbsp;
                            <a href="/clients/view/<?php echo $project->ClientProfileID;?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;"><i class="si si-eye text-gray"></i>  View</a>&nbsp;
                            <a href="#" class="btn btn-3 red" style="margin:10px 0px 5px 0px;" onclick="document.getElementById('dialog').style.display='none'"><i class="si si-close text-gray"></i>  Cancel</a><br><br>
                        </div>
                    </div>
                </div>

<!--                 <div class="modal fade" id="myModal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body" style="width: 600px;">
                                <div id="dialog-form" class="text-center">
                                    <div class="alert alert-danger fade in">You are 1 step away to get started please complete your profile!</div>
                                    <a href="/users/profile" class="btn btn-3 blue" style="margin:10px 0px 5px 0px;"><i class="fa fa-pencil text-gray"></i>  Complete Profile</a>&nbsp;
                                    <a href="/clients/view/<?php echo $project->ClientProfileID;?>" class="btn btn-3 green" style="margin:10px 0px 5px 0px;"><i class="si si-eye text-gray"></i>  View</a>&nbsp;
                                    <a href="#" class="btn btn-3 red" style="margin:10px 0px 5px 0px;" data-dismiss="modal"><i class="si si-close text-gray"></i>  Cancel</a><br><br>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>  -->               
                <?php } ?>

            </div>

        </article>
    </section>

<?php View::footer(); ?>
<script type="text/javascript">
    $(document).ready(function() {
        // $('#projlist').easyPaginate({
        //     paginateElement : 'article',
        //     elementsPerPage: 15,
        //     effect: 'fade',
        //     slideOffset : 100,
        //     nextButtonText : "Next",
        //     prevButtonText : "Prev",
        //     lastButtonText : "Last",
        //     firstButtonText: "First"
        // });

        $('.nonverified').click(function(){
            var refTop, diaHeight;
            $('#dialog').css('display', 'block');
            diaHeight = $('#dialog').height();
            refTop = $('#popular').offset().top;
            $('#dialog').css('padding-top', (refTop - 85) + 'px');
            $('#myModal').css('padding-top', (refTop / 2) + 'px');
        });

        // init Isotope
        var $grid = $('.project-grid').isotope({
            itemSelector: '.project-cnt',
            layoutMode: 'fitRows',
            getSortData: {
              topfunded: '.percent-funded parseInt',
              lowfunded: '.percent-funded parseInt',
              dateupdated : '.date-updated parseInt'
            }
        });

        // bind filter button click
        $('#categories').on('change', function() {
            var filterValue = $( this ).val();

            // use filterFn if matches value
            $grid.isotope({ filter: filterValue });
        });

        $('#projsortby').on('change', function() {

            var $optvalue = $(this).val();
            var $vals = $optvalue.split('|');
            var filterByValue = $vals[1];

            if($vals[0] == 'sort'){
                if($vals[1] == 'lowfunded'){ 
                    $sorting = true; 
                }else{ 
                    $sorting = false; 
                }
              $grid.isotope({ filter: '*', sortBy: filterByValue, sortAscending: $sorting });
            }else{
              $grid.isotope({ filter: filterByValue });
            }
        });

    });
</script>