<?php
View::$title = 'Investors';
View::$bodyclass = User::info('Sidebar');
View::header();
?>
<section class="header-bottom">
    <article>
        <div class="container"><h1>Investors</h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url('users/dashboard'); ?>"><?php echo View::$title; ?></a></li>
                </ul>
            </div>
            <div class="col-lg-6 align-right sub-menu">

            </div>
        </div>
    </article>
</section>

<!-- ************************ Page Content ************************ -->
<!-- Page Content -->
<div class="container">
    <!-- Full Table -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">View TT Receipt</h3>
            <div class="pull-right">
                <a href="/clients/investors">Back to Investors List</a>
            </div>
        </div>
        <div class="block-content">
            <?php if ($fileReceipt):
                        $ext = substr($fileReceipt[0]->FileSlug,-3);

                ?>
            <div class="form-group">
                <label><?php echo $fileReceipt[0]->FileDescription;?></label>

                <?php if (in_array($ext, ['jpg','png','jpeg'])) : //show as image?>
                    <?php echo View::photo('files/'.$fileReceipt[0]->FileSlug,"TT Receipt",""); ?>
                <?php endif; ?>

                <?php if ($ext=='pdf'): ?>
                    <iframe src="<?php echo '/assets/files'. $fileReceipt[0]->FileSlug?>" style="width: 100%;height: 600px;border: none;"></iframe>
                <?php endif; ?>
            </div>
            <?php endif;?>
        </div>
    </div>
</div><!--end container-->

<?php View::footer(); ?>

<script type="text/javascript">

</script>