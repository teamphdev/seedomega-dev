<?php 
View::$title = 'Upload TT Receipt';
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
  <article class="container">
    <div class="row">
      <div class="col-lg-6">
        <ul>
          <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
          <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
          <li class="fa fa-angle-right"></li>
          <li><a href="javascript:void(0);"><?php echo View::$segments[0]; ?></a></li>
          <?php if( isset(View::$segments[1]) ) { ?>
            <li class="fa fa-angle-right"></li>
            <?php if( isset(View::$segments[2]) ) { ?>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$segments[1]; ?></a></li>
              <li class="fa fa-angle-right"></li>              
              <li><?php echo View::$title; ?></a></li>
            <?php } else { ?>
              <li><?php echo View::$title; ?></a></li>
          <?php } } ?>
        </ul>
      </div>
      <div class="col-lg-6 align-right sub-menu">

      </div>
    </div>
  </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="white filter">
    <article class="container">
      <form action="projects-2.html">
        <fieldset>
          <div class="form-group">
            <span class="filter-title">Search Projects</span>
          </div>
          <div class="form-group">
            <label for="categories"><strong>Categories</strong></label>
            <select id="categories" class="form-control arrow-down">
              <option value="">All Categories</option>
              <option value="Animals">Animals</option>
              <option value="Community">Community</option>
              <option value="Education">Education</option>
              <option value="Environment">Environment</option>
              <option value="Food">Food</option>
              <option value="Health">Health</option>
            </select>
          </div>
          <div class="form-group">
            <label for="city"><strong>City</strong></label>
            <input type="text" class="form-control" id="city" placeholder="City name">
          </div>
          <div class="form-group">
            <label for="country"><strong>Country</strong></label>
            <select id="country" class="form-control arrow-down">
              <option value="">All Countries</option>
              <option value="Australia">Australia</option>
              <option value="Canada">Canada</option>
              <option value="United Kingdom">United Kingdom</option>
              <option value="United States">United States</option>
            </select>
          </div>
          <div class="form-group">
            <label><strong>Percent Funded</strong></label>
            <div class="radio-inputs">
              <div class="form-group">
                <input type="radio" value="all" name="percent-funded" id="all" checked="checked" >
                <label for="all">All</label>
              </div>
              <div class="form-group">
                <input type="radio" value="50-75" name="percent-funded" id="5075" >
                <label for="5075">50% - 75%</label>
              </div>
              <div class="form-group">
                <input type="radio" value="75-100" name="percent-funded" id="75100" >
                <label for="75100">75% - 100%</label>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-4 green" style="margin-top: 32px;">Submit</button>
        </fieldset>
      </form>
    </article>
  </section>

    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <input type="hidden" name="action" value="uploadttreceipt">
                <input type="hidden" name="InvoiceID" value="<?php echo $invoice->InvoiceID;?>">
                <input type="hidden" name="UserID" value="<?php echo $invoice->UserID;?>">
                <div class="form-group">
                    <label>Invoice ID:</label>
                    <strong><?php echo $invoice->InvoiceID;?></strong>
                </div>
                <div class="form-group">
                    <label>Balance Due:</label>
                    <strong>$<?php echo $invoice->BalanceDue;?></strong>
                </div>
                <div class="form-group">
                    <label>Upload TT Receipt</label>
                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="TTReceipt" data-show-upload="false" data-allowed-file-extensions='["doc","docx","pdf","jpeg","png","jpg"]' multiple>
                    <span class="text-muted">Allowed file types: pdf, doc, image</span>
                </div>

                <div class="form-group">
                  <a href="<?php echo View::url('clients/invoices'); ?>" class="btn btn-rounded btn-danger"><span class="si si-action-undo"></span> Go Back</a>
                  <button class="btn btn-rounded btn-primary" type="submit">Save File</button>
                </div>
            </form>
            </div>

        </div>
    </div>
    <!-- END Page Content -->

<?php View::footer(); ?>