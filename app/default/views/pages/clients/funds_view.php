<?php 
  View::$title = 'View Booking';
  View::$bodyclass = '';
  View::header(); 
?>
<?php 
switch( $bookdata->BookingStatus ){
  case 'Booked':
  case 'Pending':
    $link1 = "current";
    $link2 = "";
    $link3 = "";
    $link4 = "";
    break;

  case 'Verified':
  case 'TT Receipt':
    $link1 = "current";
    $link2 = "current";
    $link3 = "";
    break;

  case 'Approved':
    $link1 = "current";
    $link2 = "current";
    $link3 = "current";
    break;
}
?>
<section class="header-bottom pad-0 pad-20-t">
    <article>
      <div class="container animated fadeInDown">

          <div class="start-project profile-tabs profile-tabs-line" id="profile_wizard">
          
              <div class="title">
                  <ul id="profileTabs">
                      <?php if( User::info( 'UserLevel' ) == "Client" && $bookdata->BookingStatus == 'Pending' && $bookdata->BookingType == 'Interest' ){ ?>
                        <li class="<?php echo $link1; ?>"><a href="javascript:void();"><i class="si si-clock"></i><span>Registered Interest</span></a></li>
                      <?php } else { ?>
                        <li class="<?php echo $link1; ?>"><a href="javascript:void();"><i class="si si-clock"></i><span>Pending</span></a></li>
                        <li class="<?php echo $link2; ?>"><a href="javascript:void();"><i class="si si-user-following"></i><span>TT Verified</span></a></li>
                        <li class="<?php echo $link3; ?>"><a href="javascript:void();"><i class="si si-check"></i><span>TT Acknowledged</span></a></li>
                      <?php } ?>
                  </ul>
              </div>
          </div>

      </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset( View::$segments[1] ) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
  <article class="container project-single">

    <form class="form-ui input_mask" enctype="multipart/form-data" method="post" style="padding: 0;">
    <?php if( $hide != "hide" ){ ?>
    <input type="hidden" name="action" value="updatebooking">
    <?php } ?>
    <input type="hidden" name="bookid" value="<?php echo isset( $bookdata->InvestmentBookingID ) ? $bookdata->InvestmentBookingID : ""; ?>">
    <input type="hidden" name="usrid" value="<?php echo isset( $bookdata->UserID ) ? $bookdata->UserID : ""; ?>">

    <div class="row about-project">
      <!-- ************************ Left Side Area ************************ -->
      <div class="col-md-8">
          <div class="booking-summary">
              <div class="block-header">
                  <div class="block-title">INTEREST INFORMATION</div>
              </div>
          </div>
          <div class="block-content">
              <?php echo View::getMessage(); ?>
              <div id="basic-data" class="form-wizard active">
              <?php 
              if( $bookdata->BookingStatus == "Approved" || $bookdata->BookingStatus == "Verified" ){
                $readonly = 'readonly';
                $disabled = 'disabled';
                $hide = 'hide';
                if( User::role() == "Administrator"){
                  $readonly = '';
                  $disabled = '';
                  $hide = '';
                }
              } else {
                $readonly = 'readonly';
                $disabled = 'disabled';
                $hide = '';
              } ?>

                <div class="booking-id">
                    <span>
                        Booking ID: BK-533D0M364-<?php echo isset($bookdata->InvestmentBookingID) ? $bookdata->InvestmentBookingID : '-' ?>
                    </span>
                    <?php if($bookdata->BookingStatus == 'Approved') { ?>
                    <a class="btn btn-xs btn-default pull-right" href="<?php echo View::url('bookings/goprint/'.$bookdata->InvestmentBookingID); ?>" target="_blank"><i class="si si-printer"></i> Print Form</a>
                    <?php } ?>
                </div>

                <?php switch( $bookdata->ReceiptOption ){
                  case 'Cheque': ?>
                    <div class="form-group">
                       <div class="form-left">
                          <label for="">Drawer</label>
                          <input type="text" value="<?php echo isset( $bookdata->ChequeDrawer ) ? $bookdata->ChequeDrawer : ""; ?>" class="form-control" name="book[ChequeDrawer]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="form-right">
                        <label for="">Bank</label>
                        <input type="text" value="<?php echo isset( $bookdata->ChequeBank ) ? $bookdata->ChequeBank : ""; ?>" class="form-control" name="book[ChequeBank]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="clear"></div>
                    </div>

                    <div class="form-group">
                       <div class="form-left">
                          <label for="">Branch</label>
                          <input type="text" value="<?php echo isset( $bookdata->ChequeBranch ) ? $bookdata->ChequeBranch : ""; ?>" class="form-control" name="book[ChequeBranch]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="form-right">
                        <label for="">Amount</label>                   
                        <input type="text" value="<?php echo isset( $bookdata->ChequeAmount ) ? $bookdata->ChequeAmount : ""; ?>" class="form-control" name="book[ChequeAmount]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="clear"></div>
                    </div>

                  <?php break;
                  case 'TTPhoto': ?>
                  <!-- javascript:void(0); -->                
                    <?php if( $bookdata->TTReceiptPhoto == 0 && User::role() != 'Client' ){ ?>
                      <div class="alert alert-danger push-10-t fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Please Uploaded The TT Receipt Photo!</div>
                    <?php } ?>

                     <?php if( $bookdata->TTReceiptPhoto != 0) : ?>
                        <?php echo AppUtility::getBookedTTReceipt( $bookdata ); ?>
                    <?php endif; ?>

                    <?php if( User::role() != 'Client' ){ ?>
                    <div class="form-group">
                      <label for="">TT Receipt Photo <small class="text-muted">(Transaction Receipt)</small></label>
                      <input type="file" value="" class="file form-control" name="TTReceiptPhoto" placeholder="">
                    </div>
                    <?php } ?>
                    
                  <?php 
                    break;
                } ?>

                <div class="form-group">
                   <div class="form-left">
                    <label for="">Total Shares at <span>$<?php echo isset( $bookdata->Price ) ? number_format( $bookdata->Price, 2 ) : "0.00"; ?></span> per Share</label>
                      <input type="hidden" id="shareprice" value="<?php echo isset( $bookdata->Price ) ? number_format( $bookdata->Price, 2 ) : "0.00"; ?>">
                      <input type="hidden" id="minimumbid" value="<?php echo isset( $bookdata->MinimumBid ) ? $bookdata->MinimumBid : "0"; ?>">
                      <div class="input-group">
                        <input type="text" id="SharesToApply" value="<?php echo isset( $bookdata->SharesToApply ) ? number_format( $bookdata->SharesToApply ) : "0"; ?>" class="form-control" name="book[SharesToApply]" placeholder="0" required="required" <?php echo $readonly; ?>>
                        <span class="input-group-addon">Shares</span>
                      </div>
                   </div>
                   <div class="form-right">
                    <label for="">Total application monies of</label>
                      <div class="input-group">
                        <span class="input-group-addon">USD</span>
                        <input type="text" id="TotalAmountAttached" value="<?php echo isset( $bookdata->TotalAmountAttached ) ? number_format( $bookdata->TotalAmountAttached, 2 ) : "0"; ?>" class="form-control" name="book[TotalAmountAttached]" placeholder="0.00" readonly="readonly">
                      </div>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="form-group">
                   <div class="form-left">
                    <label for="">Client Subscription Rate</label>                    
                      <div class="input-group">
                        <input type="text" value="<?php echo isset( $bookdata->SubscriptionRate ) ? number_format( $bookdata->SubscriptionRate, 2 ) : "0.00"; ?>" class="form-control" placeholder="0.00" <?php echo $readonly; ?>>
                        <span class="input-group-addon">Percent</span>
                      </div>
                   </div>
                   <div class="form-right">
                    <label for="">Amount Due for SeedOmega</label>
                      <div class="input-group">
                        <span class="input-group-addon">USD</span>
                        <input type="text" value="<?php echo $bookdata->Data['SOComm']; ?>" class="form-control" placeholder="0.00" readonly="readonly">
                      </div>
                   </div>
                   <div class="clear"></div>
                </div>

                <?php if( $bookdata->BookingStatus =='Approved' ){ ?>

                <div class="block-header-2 pd">Personal Details</div>
                <div class="form-group">
                  <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                  <input type="text" value="<?php echo isset( $bookdata->FirstName ) ? $bookdata->FirstName : ""; ?> <?php echo isset( $bookdata->LastName ) ? $bookdata->LastName : ""; ?>" class="form-control" name="book[Name]" placeholder="" <?php echo $readonly; ?>>
                </div>
                
                <div class="block-header-2 pd">POSTAL ADDRESS </div>
                <div class="form-group">
                   <div class="form-left">
                    <label for="">Address</label>
                    <input type="text" value="<?php echo isset( $bookdata->Address ) ? $bookdata->Address : ""; ?>" class="form-control" name="book[Address]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Suburb</label>                    
                    <input type="text" value="<?php echo isset( $bookdata->Suburb ) ? $bookdata->Suburb : ""; ?>" class="form-control" name="book[Suburb]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">State</label>
                      <input type="text" value="<?php echo isset( $bookdata->State ) ? $bookdata->State : ""; ?>" class="form-control" name="book[State]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Postcode</label>                   
                    <input type="text" value="<?php echo isset( $bookdata->PostCode ) ? $bookdata->PostCode : ""; ?>" class="form-control" name="book[PostCode]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="block-header-2 pd">CONTACT DETAILS </div>
                <div class="form-group">
                   <div class="form-left">
                      <label for="">Telephone Number</label>
                      <input type="text" value="<?php echo isset( $bookdata->Phone ) ? $bookdata->Phone : ""; ?>" class="form-control" name="book[Phone]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Email Address</label>                   
                    <input type="email" value="<?php echo isset( $bookdata->Email ) ? $bookdata->Email : ""; ?>" class="form-control" name="book[Email]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>
                <hr><br>                

                <p class="text-muted text-left" style="text-align: left;"><b>IMPORTANT NOTICE AND ACKNOWLEDGEMENTS</b> - I/We, by lodging this form, hereby acknowledge that we qualify as sophisticated investors for the purposes of s.708 of the Corporations Act 2001 (Cth), or are otherwise person(s) to whom offers of Shares may be made without a Prospectus under the Corporations Act. I/We understand that there are inherent risks associated with an investment in Global Asset Resources Ltd (‘Company’) and therefore apply for the Shares in the knowledge and acceptance of such risks. I/We acknowledge that I/We have had an opportunity to obtain a copy of the Constitution of the Company and obtain independent professional advice in relation to this investment. I/We acknowledge that the issue of Shares is at the discretion of the Board of the Company. I/We hereby authorise the Directors of the Company to register me/us as the holder(s) of the Shares issued to me/us. I/We acknowledge that the Shares or part thereof may be classified as restricted securities by the ASX and subject to escrow conditions upon the Company undertaking a listing on the ASX. I/We undertake and agree to sign a standard form ASX Restriction Agreement to acknowledge the restrictions imposed on such Shares as determined by the ASX. </p>
                  <div class="clear"></div>

                <?php } ?>                

                <?php if($hide != "hide"){ ?>
                <div class="form-group text-center"></div>
                <?php } ?>
             </div>
          </div>
      </div> 
      
      <!-- ************************ Right Side Area ************************ -->
      <div class="col-lg-4">
        <div class="sidebar">

          <?php if(User::info('UserLevel') == "Administrator" || User::can('Verify Booking')){ ?>
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="w-title">Change Status</div>
            <div class="pcontent">
              <select name="book[BookingStatus]" class="form-control">
                <option value="Pending" <?php echo ($bookdata->BookingStatus == 'Pending') ? "selected" : ""; ?>>Pending</option>
                <option value="Verified" <?php echo ($bookdata->BookingStatus == 'Verified') ? "selected" : ""; ?>>Verified</option>
                <?php if(User::info('UserLevel') == "Administrator"){ ?>
                <option value="Approved" <?php echo ($bookdata->BookingStatus == 'Approved') ? "selected" : ""; ?>>Approved</option>
                <?php } ?>
              </select>

              <div class="text-center push-10-t">
                <button type="submit" class="btn btn-rounded btn-primary btn-block">Save Changes</button>
              </div>

            </div>
          </div>
          <?php } ?>

          <div class="sidebar-item">
              <div class="w-title">What Can I Do Next?</div>
              <div class="w-content">
                  <div class="popular-posts-widget viewbookhelp userhelp">
                      <?php if( $bookdata->BookingStatus=='Approved' && $bookdata->BookingType=='Booking' ){ ?>
                      <div class="side-nav-wcidn">
                          <span class="num">1</span>
                          <div class="num-content">
                              <h4 class="wcidn-title sopink">View Seeder Information</h4>
                              <p class="wcidn-text">It is good to know who trusted you and confident enough to share funds to your success.</p>
                              <a href="<?php View::url( 'clients/seederinfo/'.$bookdata->UserID, true ) ?>" class="btn btn-xs btn-rounded btn-default showDetails">Seeder Info</a>
                          </div>
                      </div>
                      <?php } ?>

                      <?php if(User::info('UserLevel') == "Administrator"){ ?>
                      <div class="side-nav-wcidn push-0-l">
                          <div class="single-content">
                              <h4 class="wcidn-title sopink">Change Status</h4>
                              <select name="book[Status]" class="form-control">
                                <option value="Pending" <?php echo ($bookdata->BookingStatus == 'Pending') ? "selected" : ""; ?>>Pending</option>
                                <option value="Verified" <?php echo ($bookdata->BookingStatus == 'Verified') ? "selected" : ""; ?>>Verified</option>
                                <option value="Approved" <?php echo ($bookdata->BookingStatus == 'Approved') ? "selected" : ""; ?>>Approved</option>
                              </select>
                          </div>
                      </div>
                      <?php } ?>

                      <?php if( ( User::info('UserLevel') == "Client" || User::info('Code') == "ASST" ) && $bookdata->BookingStatus=='Verified' && $bookdata->BookingType=='Booking' ){ ?>
                      <div class="side-nav-wcidn push-0-l">
                          <div class="single-content">
                              <h4 class="wcidn-title sopink">Change Status</h4>
                              <p class="wcidn-text <?php echo $exempted; ?>">I agree to pay the previously approved subscription rate <strong>(<?php echo isset( $bookdata->SubscriptionRate ) ? number_format( $bookdata->SubscriptionRate, 2 ) : "0.00"; ?>%</strong>) to SeedOmega amounting <strong>$<?php echo $bookdata->Data['SOComm']; ?></strong></p>
                              <a href="javascript:void();" thelink="<?php echo View::url('clients/funds/acknowledge/'.$bookdata->InvestmentBookingID); ?>" data-toggle="modal" data-target="#acknowledge-modal" BookingID="<?php echo $bookdata->InvestmentBookingID; ?>" class="btn btn-xs btn-rounded btn-default" title="Acknowledge" onclick="acknowledge(this)">Acknowledge Fund</a>
                          </div>
                      </div>
                      <?php } ?>

                      <?php if( ( User::info('UserLevel') == "Client" || User::info('Code') == "ASST" ) && $bookdata->BookingStatus=='Pending' && $bookdata->BookingType=='Interest' ){ ?>
                      <div class="side-nav-wcidn push-0-l">
                        <div class="single-content">
                            <h4 class="wcidn-title sopink">Change Interest Status</h4>
                            <p class="wcidn-text">I agree to accept this interest.</p>
                            <a href="/clients/funds/interestaccept/<?php echo $bookdata->InvestmentBookingID?>" class="btn btn-xs btn-rounded btn-default" title="Click to Accept and mark this as booking">Accept Interest</a>
                        </div>
                      </div>
                      <?php } ?>
                      
                  </div>
              </div>
          </div>

      </div> 
             
    </div>
    </form>
  </article>
</section>

<!-- <div class="modal" id="acknowledge-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content nobg">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Please Confirm</h3>
                </div>
                <div class="block-content form-ui">
                    <p>Are you sure you already checked and verified the fund transfer?</p>
                </div>
            </div>
            <div class="modal-footer bg-white">
                <p class="text-center">I agree to pay the previously approved commission rate <strong>(<?php echo isset( $bookdata->SubscriptionRate ) ? number_format( $bookdata->SubscriptionRate, 2 ) : "0.00"; ?>%</strong>) to SeedOmega amounting <strong>$<?php echo $bookdata->Data['SOComm']; ?></strong></p><hr class="push-10">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal">Close</button>
                <a id="acknowledge-btn" href="" class="btn btn-primary btn-rounded"><i class="fa fa-check"></i> Acknowledge</a>
            </div>
        </div>
    </div>
</div> -->

<div class="modal" id="acknowledge-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content nobg">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Please Confirm</h3>
                </div>
                <div class="block-content form-ui">
                    <p>Are you sure you already checked and verified the fund transfer?</p>
                    <hr class="push-10 <?php echo $exempted; ?>">
                    <p id="exemptionwrap" class="<?php echo $exempted; ?>">I agree to pay the previously approved subscription rate (<strong id="commRate"><?php echo isset( $bookdata->SubscriptionRate ) ? number_format( $bookdata->SubscriptionRate, 2 ) : "0.00"; ?>%</strong>) to SeedOmega amounting <strong id="commAmt">$<?php echo $bookdata->Data['SOComm']; ?></strong></p>
                </div>
            </div>
            <div class="modal-footer bg-white">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal">Close</button>
                <a id="acknowledge-btn" href="" class="btn-rounded btn btn-primary"><i class="fa fa-check"></i> Acknowledge</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal-global" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Seeder Info</h3>
                </div>
                <div class="block-content"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="url" value="<?php echo View::url( 'clients/downloadPDF/' . $bookdata->InvestmentBookingID ); ?>">

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
      $( '.download-pdf' ).click( function(){
        var pdf_url = $( '#url' ).val();
        $.ajax({
            type: "POST",
            url: pdf_url,
            success: function( response, status, request ){
                var disp = request.getResponseHeader( 'Content-Disposition' );
                var hdr = request.getResponseHeader( 'Cache-Control' );
                //alert(response);
                // if( disp && disp.search( 'attachment' ) != -1 ){
                //     var form = $('<form method="POST" action="' + url + '">');
                //     $.each( params, function( k, v ){
                //         form.append($('<input type="hidden" name="' + k +
                //                 '" value="' + v + '">'));
                //     });
                //     $('body').append( form );
                //     form.submit();
                // }
            }
        });
      });
    });

    function acknowledge( fieldVal ){
        var thelink = $( fieldVal ).attr( 'thelink' );
        var field2 = $( fieldVal ).attr( 'ChequeDrawer' );
        var field3 = $( fieldVal ).attr( 'ChequeBank' );
        var field4 = $( fieldVal ).attr( 'ChequeBranch' );
        var field5 = $( fieldVal ).attr( 'ChequeAmount' );

        $( '#acknowledge-btn' ).attr( 'href' , thelink);
    }
</script>