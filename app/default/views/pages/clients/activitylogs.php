<?php 
View::$title = 'Activity Logs';
View::$bodyclass = User::info('Sidebar').' client-logs';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?>
                <table class="table table-divide table-vcenter js-dataTable-full-pagination table-header-bg table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr class="headings">
                            <th class="sort-this text-center no-wrap" style="min-width:15%;">Date Time</th>
                            <th class="text-center no-wrap">Activity Description</th>
                            <th class="text-center no-wrap" style="min-width:15%;">User Name</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php $cntr = 0;
                    if( isset( $logs ) && count( $logs ) ){
                    foreach( $logs as $log ){ $cntr++; ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer text-center">
                            <td class="text-left"><?php echo $log->ActivityDate; ?></td>
                            <td class="text-left"><?php echo $log->ActivityDescription; ?></td>
                            <td class="text-left"><?php echo isset($log->FirstName) ? $log->FirstName: ''; ?> <?php echo isset($log->LastName) ? $log->LastName: ''; ?></td>
                        </tr>
                    <?php } } else { ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="3">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                    <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( "#logstable" ).DataTable({
        pageLength: 20,
        pagingType: "full_numbers"
    });
</script>