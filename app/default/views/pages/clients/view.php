<?php 
  View::$title = isset( $clientdata->CompanyName ) ? $clientdata->CompanyName : "No Title";
  View::$bodyclass = '';
  View::header(); 
?>

<style type="text/css">
  .write-comment-image img{margin-top: -17px;}
  .comment-image img{margin-top: -11px;}
</style>

<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <?php echo $breadcrumb; ?>
            <!-- <?php if( isset(View::$segments[1]) ) { ?>
              <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <li class="fa fa-angle-right"></li>
              <li><?php echo View::$title; ?></li>
            <?php }else{ ?>
              <li><?php echo View::$segments[0]; ?></li>
            <?php } ?> -->
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
  <article class="container project-single">
    <div class="row about-project">
      <?php echo View::getMessage(); ?>

      <!-- ************************ Left Side Area ************************ -->      
      <div class="col-lg-8">
        <div class="embed-responsive embed-responsive-16by9">
          <?php echo $clientdata->Video; ?>
        </div>

          <div class="block" style="margin-top: 20px;">

            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active">
                    <a href="#client-info">Info</a>
                </li>
                <?php if( $enabledTabs ){ ?>
                  <li class="">
                      <a href="#client-faq">FAQ</a>
                  </li>
                  <li class="">
                      <a href="#client-blogs">Blogs</a>
                  </li>
                  <li class="" id="client-comments-tab">
                      <a href="#client-comments">Comments</a>
                  </li>
                  <li class="">
                      <a href="#client-community">Community</a>
                  </li>
                <?php } ?>
            </ul>
            
            <div class="block-content tab-content bg-white">

              <div class="tab-pane fade fade-up in active" id="client-info">
                <p class="text-muted"><i><?php echo isset( $clientdata->VideoDescription ) ? $clientdata->VideoDescription : "No Description"; ?></i></p>
                <h5>Offer Overview</h5>
                <p><?php echo isset( $clientdata->OfferOverview ) ? $clientdata->OfferOverview : ""; ?></p>

                <h5>Business Model</h5>
                <p><?php echo isset( $clientdata->BusinessModel ) ? $clientdata->BusinessModel : ""; ?></p>

                <?php if( $enabledTabs ){ ?>
                  <h5>Key Invest Highlights</h5>
                  <p><?php echo isset($clientdata->KeyInvestHighlights) ? $clientdata->KeyInvestHighlights : ""; ?></p>

                  <h5>Strategy Vision</h5>
                  <p><?php echo isset( $clientdata->StrategyVision ) ? $clientdata->StrategyVision : ""; ?></p>

                  <h5>Market Demand</h5>
                  <p><?php echo isset( $clientdata->MarketDemand ) ? $clientdata->MarketDemand : ""; ?></p>

                  <h5>Board Management</h5>
                  <p><?php echo isset( $clientdata->BoardManagement ) ? $clientdata->BoardManagement : ""; ?></p>

                  <h5>Usage Of Funds</h5>
                  <p><?php echo isset( $clientdata->UsageOfFunds ) ? $clientdata->UsageOfFunds : ""; ?></p>

                  <h5>Financial Summary</h5>
                  <p><?php echo isset( $clientdata->FinancialSummary ) ? $clientdata->FinancialSummary : ""; ?></p>

                  <h5>Press Coverage</h5>
                  <p><?php echo isset( $clientdata->PressCoverage ) ? $clientdata->PressCoverage : ""; ?></p>

                  <h5>Disclosure</h5>
                  <p><?php echo isset( $clientdata->Disclosure ) ? $clientdata->Disclosure : ""; ?></p>
                <?php }else{ ?>
                  <br><br>
                  <h4 class="text-divider text-muted text-left"><span>Please complete your investor profile to view this company's full information!</span></h4>
                  <br>
                  <h5>Strategy Vision</h5>
                  <p>....</p>

                  <h5>Market Demand</h5>
                  <p>....</p>

                  <h5>Board Management</h5>
                  <p>....</p>

                  <h5>Usage Of Funds</h5>
                  <p>....</p>

                  <h5>Financial Summary</h5>
                  <p>....</p>

                  <h5>Press Coverage</h5>
                  <p>....</p>

                  <h5>Disclosure</h5>
                  <p>....</p>
                <?php } ?>

                <?php if( isset( $teams ) && count( $teams ) ){ ?>

                <!-- <div class="content"> -->
                  <hr><br>
                  <h2 class="content-heading text-center push-0-t">Company Team</h2>
                  <div class="row">

                    <?php foreach( $teams as $team ){ ?>
                    <div class="col-sm-6 col-lg-4">
                      <a class="block block-rounded block-link-hover3" href="javascript:void(0)">
                        <div class="block-content block-content-full clearfix">
                          <div class="pull-right">
                            <?php echo View::photo( $team->FileSlug ? 'files'.$team->FileSlug : 'images/user.png', false, 'img-avatar', false, false ); ?>
                          </div>
                          <div class="pull-left push-10-t">
                            <div class="font-w600 push-5"><?php echo isset( $team->Name ) ? $team->Name : ''; ?></div>
                            <div class="text-muted"><?php echo isset( $team->Position ) ? $team->Position : ''; ?></div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <?php } ?>

                  </div>
                <!-- </div> -->

                <?php } ?>
              </div>

              <div class="tab-pane fade fade-up" id="client-blogs">
                <!--<h4>Blogs</h4> <br><br>-->
                <!--blog section-->
                <div class="blog-page">
                    <!-- Dynamic Table Full Pagination -->
                    <ul id="bloglist" class="blog-list row">
                        <?php
                          $cntr = 0;
                          if( isset( $recentPosts ) ){
                            if( count( $recentPosts ) ){
                              foreach( $recentPosts as $article ){ $cntr++;
                                  ?>
                                  <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <div class="blog-item">
                                          <figure class="blog-image blog-user-view-l">
                                              <figcaption><a href="<?php echo View::url('clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID); ?>">Read Article</a></figcaption>
                                              <?php if( $article->FeaturedImage ): ?>
                                                  <?php $featuredImage = View::common()->getUploadedFiles( $article->FeaturedImage ); ?>
                                                  <?php echo View::photo( isset($featuredImage[0] ) ? 'files'.$featuredImage[0]->FileSlug : '/images/user.png', $article->BlogTitle, "articleFeaturedImage" ); ?>

                                              <?php else: ?>
                                                  <img src="<?php echo View::url( 'assets/images' ); ?>/blog/blog-3.jpg" alt="img" />
                                              <?php endif; ?>
                                          </figure>
                                          <div class="popular-content blog-user-view-r">
                                              <div class="blog-desc" style="margin-left: 0;">
                                                  <h5 style="margin-top: 0;"><a href="<?php echo View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID ); ?>"><?php echo mb_strimwidth(isset( $article->BlogTitle ) ? $article->BlogTitle: "No Category Title", 0, 55, ""); ?></a><span class="line-green"></span></h5>
                                                  <?php echo AppUtility::excerptText( $article->BlogContent, 200, '...' ); ?>
                                              </div>
                                          </div>
                                          <div class="blog-byDate clear">
                                              <div class="pmeta">
                                                  <div class="met"><i class="fa fa-user"></i> <?php echo isset( $article->FirstName ) ? $article->FirstName : ""; ?> <?php echo isset( $article->LastName ) ? $article->LastName : ""; ?></div>
                                                  <div class="met"><i class="fa fa-calendar"></i> <?php echo isset( $article->BlogDatePublished ) && $article->BlogDatePublished != '0000-00-00' ? date( 'j M, Y', strtotime( $article->BlogDatePublished ) ) : '-' ?></div>
                                                  <div class="met"><i class="fa fa-tags"></i> <?php echo isset( $article->CategoryName ) ? $article->CategoryName : ""; ?></div>
                                              </div>
                                              <div class="clear"></div>
                                          </div>
                                      </div>
                                  </li>
                        <?php } } } ?>
                    </ul>
                </div>
                <!--end blog section-->
              </div>

              <div class="tab-pane fade fade-up" id="client-faq">
                  <!--<h4>FAQ</h4>-->
                  <p class="text-muted">
                  <?php 
        				  	$ctr = 0;
        				  	if ( isset( $faqs ) ){
          						if ( count( $faqs ) ){
          							foreach( $faqs as $faq ){ $ctr++; ?>
                          <div class="clearfix"></div>
                          <div class="panel-group accordion" id="caq-a" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                  <div class="panel-heading" role="tab">
                                      <h4 class="panel-title">
                                          <a role="button" class="collapsed" data-toggle="collapse" data-parent="#caq-a" href="#caq-a-<?php echo $ctr; ?>" aria-expanded="true">
                                              <i class="fa fa-plus toggle-icon"></i>
                                              <i class="fa fa-minus toggle-icon"></i>
                                              Q<?php echo $ctr.'. '.$faq->FaqTitle; ?>
                                          </a>
                                      </h4>
                                  </div>
                                  <div id="caq-a-<?php echo $ctr; ?>" class="panel-collapse collapse in" role="tabpanel">
                                      <div class="panel-body faq">
                                          <p><?php echo $faq->FaqContent; ?></p>
                                      </div>
                                  </div>
                              </div>
                          </div>
          				<?php	} }	} ?>
                  </p>
              </div>

              <div class="tab-pane fade fade-up" id="client-comments">
                  <!--<h4>Comments</h4>-->
                  <div id="client-comments-content" class="comments-tab">
                    <div class="actionBox">
                      <ul class="commentList">

                      <!-- <?php 
            				  	$ctr = 0;
            				  	if( isset( $comments ) ){
              						if( count( $comments ) ){
              							foreach( $comments as $comment ){ $ctr++; ?>
                              <li class="role<?php echo $comment->Level; ?>">
                                <div class="commenterImage">
                                  <?php echo $comment->AvatarLink; ?>
                                  <?php echo ($comment->Level == 6) ? '<i class="fa fa-star"></i>' : '';?>
                                </div>
                                <div class="commentText">
                                  <p class=""><b><?php echo $comment->FirstName.' '.$comment->LastName; ?></b>:&nbsp; <?php echo $comment->CommentContent; ?></p> <span class="date sub-text">on <?php echo date( 'M jS, Y', strtotime( $comment->CommentDate ) ) ?></span>
                                </div>
                              </li>
                      <?php } } } ?> -->

                      <?php 
                      $ctr = 0;
                      if( isset( $newcomments ) ){
                          if ( count( $newcomments ) ){
                              foreach( $newcomments as $comment ){ $ctr++; ?>
                              <li class="role<?php echo $comment->Level; ?>">
                                  <div class="commenterImage">
                                      <?php echo $comment->AvatarLink; ?>
                                      <?php echo ($comment->Level == 4) ? '<i class="fa clnt-icon fa-star"></i>' : ''; ?>
                                      <?php echo ($comment->Level == 6) ? '<i class="fa ast-icon fa-circle text-warning"></i>' : '';?>
                                  </div>
                                  <div class="commentText">
                                      <div class=""><strong><?php echo $comment->FirstName.' '.$comment->LastName; ?></strong>:&nbsp; <?php echo $comment->CommentContent; ?>
                                      </div>
                                      <span class="date sub-text"><a href="#" data-id="<?php echo isset( $comment->CommentID ) ? $comment->CommentID : '0'; ?>" class="comment-reply">Reply</a> <span>·</span> <?php echo date('M jS, Y', strtotime( $comment->CommentDate ) ) ?>
                                      </span>
                                  </div>
                                  <ul>
                                  <?php if( isset( $comment->Children ) && count( $comment->Children ) ){
                                  foreach( $comment->Children as $reply ){ ?>
                                      <li class="role<?php echo $reply->Level; ?>">
                                          <div class="commenterImage">
                                              <?php echo $reply->AvatarLink; ?>
                                              <?php echo ($reply->Level == 4) ? '<i class="fa clnt-icon fa-star"></i>' : ''; ?>
                                              <?php echo ($reply->Level == 6) ? '<i class="fa ast-icon fa-circle text-warning"></i>' : '';?>
                                          </div>
                                          <div class="commentText">
                                              <div class="">
                                                  <strong><?php echo $reply->FirstName.' '.$reply->LastName; ?></strong>:&nbsp; <?php echo $reply->CommentContent; ?>
                                              </div>
                                              <span class="date sub-text">on <?php echo date('M jS, Y', strtotime( $reply->CommentDate ) ) ?></span>
                                          </div>
                                      </li>
                                  <?php } } else { ?>
                                      <li></li>
                                  <?php } ?>
                                  </ul>
                              </li>
                      <?php } } } ?>

                      </ul>
                    </div>
                  </div>

                  <form id="formComments" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="action" value="sendcommentview" />
                      <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
                      <input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">
                      <div class="form-group role<?php echo $currUser->Level; ?>">
                          <div class="commenterImage write-comment-image comment-image inline-block">
                              <?php echo $currUser->AvatarLink; ?>
                              <?php echo $currUser->Indicator; ?>
                          </div>
                          <div class="write-comment inline-block">
                              <input id="CommentContent" type="text" value="" class="form-control radiusx" placeholder="Write a comment..." name="CommentContent" required="required">
                              <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>
                          </div>
                          <div class="clear"></div>
                      </div>
                  </form>
                  <input type="hidden" id="currName" value="<?php echo $currUser->Name; ?>">
                  <input type="hidden" id="Level" value="<?php echo $userinfo->Level; ?>">
                  <input type="hidden" id="AvatarLink" value="<?php echo $currUser->AvatarLinkConverted; ?>">
                  <input type="hidden" id="Indicator" value="<?php echo $currUser->Indicator; ?>">

                  <!-- <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                      <input type="hidden" name="action" value="sendcommentview" />
                      <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
                      <input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">
                      <div class="form-group">
                          <div class="">
                              <input type="text" value="" class="form-control<?php echo $disabled; ?>" placeholder="Write a comment..." name="CommentContent" required="required">
                              <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>
                          </div>
                          <div class="clear"></div>
                      </div>
                  </form> -->
              </div>

              <div class="tab-pane fade fade-up" id="client-community">
                <!-- <h4>Community</h4> -->

                <div class="block <?php echo $hidden; ?>" style="min-height: 446px;">
                  <div class="block-content block-content-full" id="forums-content">
                    <table class="table   table-vcenter">
                    <?php if( isset( $forums ) ){
                    if( count( $forums ) ){
                    foreach( $forums as $k => $v ){ ?>
                      
                      <thead>
                        <tr>
                          <th colspan="2"><?php echo isset( $v[0] ) ? $v[0]->CatName : ''; ?></th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Topics</th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Posts</th>
                          <th class="hidden-xs hidden-sm" style="width: 200px;">Last Post</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if( isset( $v ) ){ foreach( $v as $f ){ ?>
                        <tr>
                          <td class="text-center" style="width: 75px;">
                            <i class="<?php echo isset( $f->SubCatIconClass ) ? $f->SubCatIconClass : ''; ?>"></i>
                          </td>
                          <td style="text-align: left;">
                            <h4 class="h5 font-w600 push-5">
                              <a href="<?php echo View::url( 'forums/category/'.$k.'/'.$f->SubCatID.'/'.htmlspecialchars( $f->SubCatName, ENT_COMPAT ) ); ?>"><?php echo isset( $f->SubCatName ) ? $f->SubCatName : ''; ?></a>
                            </h4>
                            <div class="font-s13 text-muted"><?php echo isset( $f->SubCatDescription ) ? $f->SubCatDescription : ''; ?></div>
                          </td>
                          <td class="text-center hidden-xs hidden-sm">
                            <a class="font-w600" href="javascript:void(0)"><?php echo isset( $f->Topics ) ? $f->Topics : 0; ?></a>
                          </td>
                          <td class="text-center hidden-xs hidden-sm">
                            <a class="font-w600" href="javascript:void(0)"><?php echo isset( $f->Posts ) ? $f->Posts : 0; ?></a>
                          </td>
                          <td class="hidden-xs hidden-sm">
                            <span class="font-s13">
                              <?php 
                              echo isset( $f->Data['PostedBy'] ) ? 'by <a href="javascript:void(0)">'.$f->Data['PostedBy'].'</a>' : '-';
                              echo isset( $f->Data['DatePosted'] ) ? '<br>'.date('F j, Y', strtotime( $f->Data['DatePosted'] ) ) : ''; 
                              ?>
                            </span>
                          </td>
                        </tr>
                        <?php } } ?>
                      </tbody>

                    <?php } } else { ?>

                      <thead>
                        <tr>
                          <th colspan="2"></th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Topics</th>
                          <th class="text-center hidden-xs hidden-sm" style="width: 100px;">Posts</th>
                          <th class="hidden-xs hidden-sm" style="width: 200px;">Last Post</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="5">
                            <div class="h5 hidden-xs text-left">No data found! Contact the administrator to create Categories and Sub-Categories.</div>
                          </td>
                          <td class="hidden"></td>
                          <td class="hidden"></td>
                          <td class="hidden"></td>
                          <td class="hidden"></td>
                        </tr>
                      </tbody>

                    <?php } } ?>

                    </table>
                  </div>
                </div>

                <h2 class="content-heading text-center push-0-t">Our Backers</h2>

                  <?php
                  $ctr = 0;
                  if( isset( $communities ) ){
                    if( count( $communities ) ){
                      foreach( $communities as $community ){ $ctr++; ?>
                        <?php if($clientdata->UserID == User::info('UserID')){ ?>
                          <div class="col-sm-6 col-lg-4">
                            <a class="block block-rounded block-link-hover3 showDetails" href="<?php View::url('clients/teammemberdetails/'.$community->UserID,true)?>" >
                              <div class="block-content block-content-full clearfix">
                              <div class="pull-right">
                                <?php View::photo( $community->FileSlug ? 'files'.$community->FileSlug : 'images/user.png', false, 'img-avatar ', false, true ); ?>
                              </div>
                              <div class="pull-left push-10-t">
                                <?php $backer = Apputility::maskText( $community->FirstName, 'x', 'x' ).' '.$community->LastName; ?>
                                <div class="font-w600 push-5"><?php echo Apputility::excerptAsNeeded( $backer, 11, '..' ); ?></div>
                                <div class="text-muted"><?php echo Apputility::excerptAsNeeded( $community->JobTitle, 13, '..' ); ?></div>
                              </div>
                              </div>
                            </a>
                          </div>
                        <?php }else{ ?>
                          <div class="col-sm-6 col-lg-4">
                            <a class="block block-rounded block-link-hover3" href="javascript:void(0);" >
                              <div class="block-content block-content-full clearfix">
                              <div class="pull-right">
                                <?php View::photo( $community->FileSlug ? 'files'.$community->FileSlug : 'images/user.png', false, 'img-avatar ', false, true ); ?>
                              </div>
                              <div class="pull-left push-10-t">
                                <?php $backer = Apputility::maskText( $community->FirstName, 'x', 'x' ).' '.$community->LastName; ?>
                                <div class="font-w600 push-5"><?php echo Apputility::excerptAsNeeded( $backer, 11, '..' ); ?></div>
                                <div class="text-muted"><?php echo Apputility::excerptAsNeeded( $community->JobTitle, 13, '..' ); ?></div>
                              </div>
                              </div>
                            </a>
                          </div>
                        <?php } ?>
                <?php } } } ?>
              </div>

                <div class="modal" id="modal-teamMember" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="block block-themed block-transparent remove-margin-b">
                                <div class="block-header bg-primary-dark">
                                    <ul class="block-options">
                                        <li>
                                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Member Details</h3>
                                </div>
                                <div class="block-content">

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!--end block-content-->

          </div><!--end block-->
          
      </div>
      
      <!-- ************************ Right Side Area ************************ -->      
      <div class="col-lg-4">
        <div class="sidebar">
          
          <!-- Project Progress -->
          <div class="sidebar-item">
            <div class="project-progress">
              <?php 
              $percentage = 0;
              if( $clientdata->TargetGoal > 0){
                $percentage = (int)( ( $clientdata->TotalRaisedAmt / $clientdata->TargetGoal ) * 100 + .5 );
                $percentage = $percentage > 100 ? 100 : $percentage;
              ?>

              <div class="popular-data data-single"> 
                <!-- <img src="<?php echo View::url() ?>/assets/images/funder_1.jpg" alt="Funder" /> -->
                
                <div class="pie_progress" role="progressbar" data-goal="<?php echo $percentage; ?>" data-barcolor="#ef6342" data-barsize="7.1">
                  <div class="pie_progress__number"><?php echo $percentage; ?>%</div>
                  <div class="pie_progress__label">Completed</div>
                </div>
              </div>

              <?php } ?>              

              <div class="offer-details" style="padding: 15px;">
      
                <!-- <div class="project-progressbar progress active">
                    <div class="progress-bar progress-bar-warning progress-bar-striped" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($percentage == 0) ? "1" : $percentage; ?>%"><?php echo $percentage; ?>%</div>
                </div> -->
                <div class="row text-center">
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset( $clientdata->TotalRaisedAmt ) ? "$".number_format( $clientdata->TotalRaisedAmt, 2 ) : "0.00"; ?><br>
                        <small class="text-muted">Raised</small>
                    </div>                                        
                    <div class="col-xs-6 push-10 offer-data">
                        <?php echo isset( $clientdata->TotalInvestor) ? number_format( $clientdata->TotalInvestor ) : "0"; ?><br>
                        <small class="text-muted">Investors</small>
                    </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo isset( $clientdata->TypeOfOffer ) ? $clientdata->TypeOfOffer : "-"; ?><small>Type of Offer</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data"><?php echo isset( $clientdata->DaysLeft ) ? number_format( $clientdata->DaysLeft ) : 0; ?><small>Days Left</small></div>
                </div>

                <div class="row">
                  <div class="col-xs-6 offer-open offer-data">
                    <?php echo $clientdata->OfferOpening != "0000-00-00" ? date( 'j M Y', strtotime( $clientdata->OfferOpening ) ) : "-"; ?><small>Offer Open</small>
                  </div>
                  <div class="col-xs-6 offer-close offer-data">
                    <?php echo $clientdata->OfferClosing != "0000-00-00" ? date( 'j M Y', strtotime( $clientdata->OfferClosing ) ) : "-"; ?><small>Offer Closing</small>
                  </div>
                </div>
                <?php /*
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset( $clientdata->TargetGoal ) ? number_format( $clientdata->TargetGoal, 2 ) : "-"; ?><small>Target Goal</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    <?php echo isset( $clientdata->SizeOfOffer ) ? $clientdata->SizeOfOffer : "-"; ?><small>Size of Offer</small>
                  </div>
                </div>
                */ ?>                
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset( $clientdata->Price ) ? number_format( $clientdata->Price, 2 ) : "0.00"; ?><small>Price</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset( $clientdata->TargetGoal ) ? number_format( $clientdata->TargetGoal, 2 ) : "-"; ?><small>Target Goal</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset( $clientdata->MinimumBid ) ? number_format( $clientdata->MinimumBid, 2 ) : "-"; ?><small>Minimum Investment</small>
                  </div>
                  <div class="col-xs-6 col-sm-6 offer-size offer-data">
                    $<?php echo isset( $clientdata->MaximumBid ) ? number_format( $clientdata->MaximumBid, 2 ) : "-"; ?><small>Maximum Investment</small>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 offer-lead offer-data">
                    <?php if( $enabledTabs ){ ?>
                    <?php echo isset( $clientdata->LeadManager ) ? ucwords( $clientdata->LeadManager ) : "-"; ?>
                    <small><a href="<?php echo isset( $clientdata->Website ) ? $clientdata->Website : '#'; ?>"><?php echo isset( $clientdata->Website ) ? $clientdata->Website : ''; ?></a></small>
                    <?php } else { echo '...'; } ?>
                    <small>Lead Manager</small>
                  </div>
                </div>

              </div>
              <div class="clear"></div>
            </div>
          </div>

            <!--upload docuemnts-->
            <?php if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' || $loggedClient->ClientProfileID == $clientdata->ClientProfileID ): ?>
            <?php if ($documents): ?>
            <div class="sidebar-item">
                <div class="w-title">Documents</div>
                <div class="text-left ul-content" style="">
                  <?php /* ?>
                    <UL>
                        <?php foreach($documents as $doc):
                            $ext = pathinfo($doc->FileSlug, PATHINFO_EXTENSION);
                            switch ($ext) {
                                case 'pdf':
                                    $fileSlugUrl = view::url()."/assets/images/pdf.png";
                                    break;
                                case 'docx':
                                    $fileSlugUrl = view::url()."/assets/images/word.jpg";
                                    break;
                                case 'doc':
                                    $fileSlugUrl = view::url()."/assets/images/word.jpg";
                                    break;
                                default:
                                    $fileSlugUrl = View::url().'/assets/files'.$doc->FileSlug;
                                    break;
                            }
                            ?>
                            <li><a href="<?php echo View::url(($doc) ? 'assets/files'.$doc->FileSlug : '#');?>" class="html5lightbox" data-group="set1" data-thumbnail="<?php echo $fileSlugUrl;?>" title="<?php echo $doc->FileDescription;?>">  <?php echo $doc->FileName;?></a></li>
                        <?php endforeach;?>
                    </UL>
                    <?php */ ?>
                    <?php if( isset( $documents ) ){
                    if( count( $documents ) ){ ?>
                    <div class="listdownload brochure-sm brochure-list ">

                      <?php 
                      foreach($documents as $doc) {
                          $ext = pathinfo($doc->FileSlug, PATHINFO_EXTENSION);
                          switch ($ext) {
                              case 'pdf':
                                  $fileSlugUrl = View::asset("images/pdf3.png");
                                  break;
                              case 'docx':
                                  $fileSlugUrl = view::asset("images/word.jpg");
                                  break;
                              case 'doc':
                                  $fileSlugUrl = view::asset("images/word.jpg");
                                  break;
                              default:
                                  $fileSlugUrls = isset( $doc->Link ) ? $doc->Link : '';
                                  $fileSlugUrl = getimagesize($fileSlugUrls) !== false ? $fileSlugUrls : view::asset("images/blank-icon.jpg");
                                  break;
                          }
                      ?>
                      <div class="col-lg-12 col-sm-12 col-xs-12">
                          <a href="<?php echo View::asset(($doc) ? 'files'.$doc->FileSlug : '#');?>" class="brochure push-20" download>
                              <div class="pull-left ico">
                                  <img src="<?php echo $fileSlugUrl; ?>" alt="icon" width="42" height="60">
                              </div>
                              <div class="brochure-body">
                                  <small class="text-muted"><?php echo isset( $doc->DocumentName ) ? $doc->DocumentName : ''; ?></small>
                                  <p><?php echo isset( $doc->FileDescription ) ? $doc->FileDescription : ''; ?></p>
                              </div>
                          </a>
                      </div> 
                      <?php } ?>
                        
                    </div>
                    <?php } } ?>
                </div>
            </div>
            <?php endif; endif;?>

            <!--end uploaded documents-->
          
          <!-- We Need Volunteers -->          
          <?php if( Apputility::investorinfo( 'InvestorStatus' ) == 'Approved' ){ ?>
          <div class="sidebar-item">
            <div class="w-title"><?php echo $clientdata->Data['title']; ?></div>
            <div class="text-center" style="padding:10px 0 15px;">
              <a href="<?php echo View::url( 'bookings/book/'.$clientdata->ClientProfileID ); ?>" class="btn btn-3 green<?php echo $clientdata->Data['disabled']; ?>" style="margin:10px 0px 5px 0px;"><?php echo $clientdata->Data['button']; ?></a>
            </div>
          </div>
          <?php } ?>
          
        </div>
      </div>
    </div>
  </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  $( document ).ready( function(){
    $( '#client-comments-tab' ).click( function( e ) {
      e.preventDefault();
      setTimeout( function(){ autoScroll(); }, 800);
    });
  });

  $( '.comment-reply' ).click( function(e){
      e.preventDefault();
      $( '#formCommentsReply' ).remove(); //remove first the previous form
      $( 'li' ).removeClass( 'referenceComment' ); //remove first the previous classname
      $( this ).closest( 'li' ).find( 'ul li' ).last().addClass( 'referenceComment' );
      commentid = $(this).attr( 'data-id' );
      replyWrapper = '<form id="formCommentsReply" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">';
      replyWrapper += '<input type="hidden" name="action" value="sendcommentview" />';
      replyWrapper += '<input type="hidden" name="ParentID" value="'+commentid+'">';
      replyWrapper += '<input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">';
      replyWrapper += '<input type="hidden" name="ClientProfileID" value="<?php echo $clientdata->ClientProfileID; ?>">';
      replyWrapper += '<div class="form-group role<?php echo $currUser->Level; ?>">';
      replyWrapper += '   <div class="write-comment-image inline-block">';
      replyWrapper += '       <?php echo $currUser->AvatarReplyLink; ?>';
      replyWrapper += '       <?php echo $currUser->Indicator; ?>';
      replyWrapper += '   </div>';
      replyWrapper += '   <div class="write-comment inline-block">';
      replyWrapper += '       <input id="ReplyContent" type="text" value="" class="form-control" placeholder="Write a reply..." name="CommentContent" required="required">';
      replyWrapper += '       <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>';
      replyWrapper += '   </div>';
      replyWrapper += '   <div class="clear"></div>';
      replyWrapper += '</div>';
      replyWrapper += '</form>';

      if( $( this ).closest( 'li' ).find( 'ul li' ) ){
          $( this ).closest( 'li' ).find( 'ul li' ).last().after( replyWrapper );
      } else {
          $( this ).parent().append( replyWrapper );
      }
      $( '#ReplyContent' ).focus();
  });

  $( document ).keypress( function(e){
      if( e.keyCode == 13 ){
          e.preventDefault();
          var reply = $('#ReplyContent').val();
          var comment = $('#CommentContent').val();
          if( comment.trim().length > 0 ){
              saveComment( '#formComments' );
              appendHTML( comment, 'comment' );
          }
          if( reply.trim().length > 0 ){
              saveComment( '#formCommentsReply' );
              appendHTML( reply, 'reply' );
          }
      }
  });

  //save comment to database
  function saveComment( formid ){
      $.ajax({
          type: 'POST',
          url: '/clients/comments',
          data: $( formid ).serialize(),
          success: function(){}
      });
  }

  //append comment/reply for comment box
  function appendHTML( msg, type ){
      var now = new Date();
      now = monthNames[ now.getMonth() ] + ' ' + getGetOrdinal( now.getDate() ) + ', ' + now.getFullYear();

      var name = $('#currName').val();
      var leve = $('#Level').val();
      var link = $('#AvatarLink').val();
      var indi = $('#Indicator').val();
      var html = '<li class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <span class="date sub-text">on '+now+'</span></div></li>';

      if( type == 'comment' ){
          $( '.actionBox > ul' ).append( html );
          $( '#CommentContent' ).val( '' );
      } else {
          $( '.referenceComment' ).removeClass( 'referenceComment' ).after( html );
          $( '#ReplyContent' ).val( '' );
      }
      autoScroll();
  }

  function getGetOrdinal( n ){
      var s = ["th","st","nd","rd"], v = n%100;
      return n+(s[(v-20)%10]||s[v]||s[0]);
  }

  function autoScroll(){
    $( '#client-comments-content' ).animate( { scrollTop: $( '.commentList' )[0].scrollHeight }, 2000);
  }
</script>