<?php 
View::$title = 'Trashed Team Members';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/manage">Manage Team</a></li>
                <li class="fa fa-angle-right"></li>
                <li><?php echo View::$title; ?></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<form method="post" action="">
<input type="hidden" name="doemptytrashbin">

<section class="gray">
    <!-- Page Content -->
        
    <div class="container">
        <div class="block-header">
            <h3 class="block-title">All <?php echo View::$title; ?><small></small>
                
            </h3>
        </div>

        <div class="block">
            <div class="">
                <?php echo View::getMessage(); ?> 
                <table class="table table-divide js-dataTable-full-pagination dt-responsive table-hover table-vcenter" width="100%" addbutton='<button type="submit" class="btn btn-danger btn-rounded" onclick="return confirm(&#39;Are you sure you want to delete trashed team members permanently? If you continue you will be able to undo this action!&#39;);"><i class="fa fa-trash"></i> <?php echo Lang::get("USR_TRSH_EMPBTN"); ?></button>'>
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-center">Photo</th>
                            <th class="text-center"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th class="text-center"><?php echo Lang::get('COMM_INFT_TR3TH2'); ?></th>
                            <th><?php echo Lang::get('USR_PRF_RMRKS'); ?></th>
                            <th class="no-sorting text-center" style="width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if( isset( $teams ) && count( $teams ) ){
                        foreach( $teams as $team ){ $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td class="text-center">
                                <div class="profile-image"><?php echo $team->Data['PhotoLink']; ?></div>
                                <input type="hidden" name="uids[]" value="<?php echo $team->ClientTeamID; ?>" />
                            </td>
                            <td><?php echo $team->Name; ?></td>
                            <td><?php echo $team->Position; ?></td>
                            <td><?php echo $team->Bio; ?></td>
                            <td style="text-align:center;">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Team Members' ) ){ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'clients/team/restore/'.$team->ClientTeamID ); ?>" title="Restore Member" data-toggle="tooltip"><i class="fa fa-undo pull-right"></i> Restore</a></li>
                                            <li><a href="<?php echo View::url( 'clients/team/delete/'.$team->ClientTeamID ); ?>" title="Permanently Delete Member" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete team member <?php echo $team->Name; ?>? If you continue you will be able to undo this action!' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'clients/team/restore/'.$team->ClientTeamID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Restore Member" data-toggle="tooltip"><i class="fa fa-undo"></i>  Restore</a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr>
                            <td colspan="5">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

</form>

<!-- /page content -->
<?php View::footer(); ?>