<?php 
    View::$title = 'Manage Team Members';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
?>

<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage();  ?> 
                <table class="table js-dataTable-full-pagination table-divide dt-responsive table-header-bg table-hover table-vcenter" width="100%" addbutton='<a href="<?php echo View::url('clients/team/add'); ?>" class="btn btn-rounded text-uppercase btn-success push-10-r" data-toggle="tooltip" title=""><i class="si si-user-follow"></i> Add Member</a>
                    <?php if( $userinfo->Code == 'CLN' ){ ?>
                    <a href="<?php echo View::url('clients/team/trashbin'); ?>" class="btn btn-rounded text-uppercase btn-warning" data-toggle="tooltip" title=""><i class="si si-trash"></i> Trash Bin</a>
                    <?php }?>'>
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-align">Photo</th>
                            <th width="200"><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th width="200"><?php echo Lang::get('COMM_INFT_TR3TH2'); ?></th>
                            <th><?php echo Lang::get('USR_PRF_RMRKS'); ?></th>
                            <th class="no-sorting text-center" style="min-width: 150px;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if( count( $teams ) ){
                            foreach( $teams as $team ){ $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td class="text-center">
                                <div class="profile-image"><?php echo $team->Data['PhotoLink']; ?></div>
                            </td>
                            <td class="valign-t"><?php echo $team->Name; ?></td>
                            <td class="valign-t"><?php echo $team->Position; ?></td>
                            <td data-toggle="modal" data-target="#modal-team-bio" onclick="viewBio( '<?php echo $team->ClientTeamID; ?>' );">
                                <div class="hiddenMCE font-12">
                                    <?php $excerpt = AppUtility::excerptAsNeeded( $team->Bio, 200, '... <span class="text-info">(click to view more)</span>' ); ?>
                                    <?php echo $excerpt; ?>
                                </div>
                            </td>
                            <td class="text-center">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Team Members' ) ){ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'clients/team/edit/'.$team->ClientTeamID ); ?>" title="Edit Member" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'clients/team/trash/'.$team->ClientTeamID ); ?>" title="Trash Member" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to move your member <?php echo $team->Name; ?> to trash bin?' );"><i class="fa fa-trash-o pull-right"></i> Trash</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'clients/team/edit/'.$team->ClientTeamID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit Member" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                            } else { ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="5">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <!-- Team Bio Modal -->
                <div class="modal fade" id="modal-team-bio" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="modal-title"></span></h3>
                          </div>
                          <div class="block-content">
                            <div id="modal-content" class="hiddenMCE"></div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END Team Bio Modal -->

            </div>
        </div>
    </div>
</section>
<input type="hidden" id="jteams" value="<?php echo $jdata; ?>">
<!-- /page content -->

<?php View::footer(); ?>

<script type="text/javascript">
    var teams = [];
    $( document ).ready( function(){
        $teams = $( '#jteams' ).val();
        teams = jQuery.parseJSON( $teams );

        // initialize tinyMCE on contents having .hiddenMCE class
        hiddenMCEconfig.inline = true;
        hiddenMCEconfig.readonly = 1;
        tinymce.init( hiddenMCEconfig ); // hiddenmce.js
    });

    function viewBio( tid ){
        $( '#modal-title' ).text( teams[tid]['FullName'] );
        $( '#modal-content' ).text( teams[tid]['Bio'] );
        tinymce.get( 'modal-content' ).setContent( teams[tid]['Bio'] );
    }
</script>