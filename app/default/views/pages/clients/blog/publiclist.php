<?php 
    View::$title = 'Blogs';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1>What's New!!!!</h1>
            <p>The new way to invest in innovative business. We want to ensure our clients have the best opportunity for success, so we offer support before, during and after the offer for companies and investors.</p>
        </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container blog-page animated zoomIn">
        <!-- Dynamic Table Full Pagination -->
        <ul id="bloglist" class="blog-list row">
        
        <?php 
        $cntr = 0;
        if( isset( $blogs ) && count( $blogs ) ){
            foreach( $blogs as $blog ){ $cntr++; ?>
                <li class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="blog-item">
                        <figure class="blog-image">
                            <figcaption><a href="<?php echo $blog->Link; ?>">Read More</a></figcaption>
                            <?php if( $blog->Data['FImage'] ): ?>
                                <?php echo View::photo( isset( $blog->Data['FImage'][0] ) ? '/files'.$blog->Data['FImage'][0]->FileSlug : '/images/user.png' , $blog->BlogTitle, "articleFeaturedImage", false, 'width:358px;height:165px;' ); ?>
                            <?php else: ?>
                                <img src="<?php echo View::asset('images'); ?>/blog/blog-3.jpg" alt="img" />
                            <?php endif; ?>
                        </figure>
                        <div class="popular-content">
                            <div class="blog-desc" style="height: 150px;">
                                <?php $excerptTitle = AppUtility::excerptAsNeeded( $blog->BlogTitle, 31 ); ?>
                                <h5><a href="<?php echo $blog->Link; ?>" title="<?php echo $blog->BlogTitle; ?>"><?php echo mb_strimwidth( isset( $excerptTitle ) ? $excerptTitle : "No Category Title", 0, 55, "" ); ?></a><span class="line-green"></span></h5>
                                <?php echo AppUtility::excerptText( $blog->BlogContent, 195,' <a href="'.$blog->Link.'" class="item_button1">...read more</a>' );?>
                            </div>
                        </div>
                        <div class="blog-byDate">
                            <div class="pmeta">
                                <div class="met"><i class="fa fa-user"></i> <?php echo $blog->FirstName ? AppUtility::excerptText( $blog->FirstName, 6 ) : ""; ?></div>
                                <div class="met"><i class="fa fa-calendar"></i> <?php echo ( $blog->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime( $blog->BlogDatePublished ) ) : '-' ?></div>
                                <div class="met"><i class="fa fa-tags"></i> <a href="/clients/blog/<?php echo $blog->CategorySlug; ?>" title="<?php echo $blog->CategoryName; ?>"><?php echo $blog->CategoryName ? AppUtility::excerptText( $blog->CategoryName, 6 ) : ""; ?></a></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </li>
            <?php }
        } ?>
          
          <div class="clear"></div>

      </ul>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#bloglist').easyPaginate({
            paginateElement : 'li',
            elementsPerPage: 9,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });
    });
</script>