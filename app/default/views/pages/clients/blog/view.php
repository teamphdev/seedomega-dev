<?php
  View::$title = 'Blog';
  View::$ptitle = $blogdata->BlogTitle;
  View::$bodyclass = User::info('Sidebar');
  View::header();
?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1>
              <?php if( isset(View::$segments[1]) ){ ?>
                <a href="<?php echo View::url( View::$segments[0] ).'/'.View::$segments[1]; ?>" class="text-white"><?php echo $title; ?></a>
              <?php } ?>
            </h1>
        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/<?php echo $blogdata->BlogID;?>"><?php echo $client->CompanyName;?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
   <article class="container projects-page animated zoomIn" id="popular">
      <div class="row">
         <!-- ************************ Left Side Area ************************ -->
         <div class="col-lg-3 sidebar-container">
           <div class="sidebar">

              <div class="sidebar-item">
                  <div class="block-content">
                    <div class="bw-heading push-0">Recent Posts</div>
                  </div>
                  <div class="text-left block-content">
                      <?php
                      $ctrl = 0;
                      if( isset( $recentPosts ) && count( $recentPosts ) ){
                          foreach( $recentPosts as $post ){ $ctrl++; ?>
                              <div class="blog-item widget">
                                  <a href="<?php echo View::url('clients/blog/view/'.str_ireplace( '?', '', $post->BlogSlug ).'/'.$post->BlogID); ?>">
                                      <div class="row">
                                          <div class="col-lg-4 col-xs-4">
                                              <div class="blog-img">
                                                  <?php if ($post->FeaturedImage): ?>
                                                      <?php $featuredImage = View::common()->getUploadedFiles($post->FeaturedImage); ?>
                                                      <?php echo View::photo((isset($featuredImage[0]) ? '/files/'.$featuredImage[0]->FileSlug : '/images/user.png'),$post->BlogTitle,""); ?>
                                                  <?php else: ?>
                                                      <img src="http://via.placeholder.com/85x60">
                                                  <?php endif;?>
                                              </div>
                                          </div>
                                          <div class="col-lg-8 col-xs-8 no-padding">
                                              <div class="blog-content">
                                                  <?php $excerptTitle = AppUtility::excerptAsNeeded( $post->BlogTitle, 57, '' ); ?>
                                                  <div class="blog-title"><?php echo mb_strimwidth( isset( $excerptTitle ) ? $excerptTitle : "No Category Title", 0, 55, "" ); ?></div>
                                                  <div class="blog-footer">
                                                      <span class="text-muted"><?php echo ( $post->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime( $post->BlogDatePublished ) ) : '-' ?></span></div>
                                              </div>
                                          </div> 
                                      </div>
                                  </a>
                              </div>
                              
                          <?php }
                      } ?>
                  </div>
              </div>

              <div class="sidebar-item">
                 <div class="block-content">
                    <div class="bw-heading push-0">Popular Categories</div>
                  </div>
                 <div class="block-content block-content-full">
                    <ul class="popular-cat-widget">
                      <?php if( $categories ){ ?>
                          <?php foreach( $categories as $cat ){ ?> 
                              <li><a href="<?php echo View::url( 'clients/blog/'.$cat->CategorySlug ); ?>"><?php echo $cat->CategoryName; ?></a></li>
                          <?php } ?>
                      <?php } ?>
                    </ul>
                 </div>
              </div>             

           </div>
         </div>

         <div class="col-lg-9 col-md-12 content-cnts">
            <div class="blog-single">
               <figure>
                   <?php if ($blogdata->FeaturedImage): ?>
                   <?php $featuredImage = View::common()->getUploadedFiles($blogdata->FeaturedImage); ?>
                       <?php echo View::photo((isset($featuredImage[0]) ? '/files/'.$featuredImage[0]->FileSlug : '/images/user.png'),"img",""); ?>
                    <?php endif;?>
               </figure>
               <div class="blog-byDate">
                  <div class="pmeta">
                     <div class="met"><i class="fa fa-user"></i> <?php echo ($client->FirstName) ? $client->FirstName : ""; ?> <?php echo ($client->LastName) ? $client->LastName : ""; ?></div>
                     <div class="met"><i class="fa fa-calendar"></i> <?php echo ($blogdata->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime($blogdata->BlogDatePublished)) : '-' ?></div>
                     <div class="met"><i class="fa fa-tags"></i> <a href="<?php echo View::url( 'clients/blog/'.$blogdata->CategorySlug ); ?>"><?php echo ($blogdata->CategoryName) ? $blogdata->CategoryName : ""; ?></a></div>
                  </div>
                  <div class="clear"></div>
               </div>
               <div class="blog-description ul-content">
                    <h3 class="b-title"><?php echo ($blogdata->BlogTitle) ? ucwords($blogdata->BlogTitle) : "No Title"; ?></h3>
                    <?php if($owner) : ?>
                        <?php echo '<p><a class="b-edit" href="/clients/blog/edit/'.$blogdata->BlogID.'">Edit</a></p>';?>
                    <?php endif; ?>
                    <?php echo ($blogdata->BlogContent) ? $blogdata->BlogContent : "No Content"; ?>
               </div>
            </div>
         </div>

      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>