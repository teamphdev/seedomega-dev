<?php 
View::$title = 'Add New Post';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <article class="container">
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        <form id="frmAdd" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
            <input type="hidden" name="action" value="addnewpost">
            <input type="hidden" name="microtime" value="<?php echo( microtime() ); ?>">
            <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
            <input type="hidden" name="ClientProfileID" value="<?php echo $client->ClientProfileID; ?>">

        <div class="block-header"><h5 class="text-uppercase"><?php echo View::$title; ?></h5></div>
        <div class="block-content">
            
            <div class="row">
                <?php echo View::getMessage(); ?>
                <div class="col-sm-9">
                    <div class="push-30-r">
                        <div class="form-group">
                            <label>Title <span class="text-danger">*</label>
                            <input type="text" value="" name="BlogTitle" class="form-control col-xs-12" required="required">
                        </div>

                        <div class="form-group">
                            <label for="BlogContent">Content <span class="text-danger">*</label>
                            <textarea class="form-control tinyMCE" name="BlogContent" cols="30" rows="10"></textarea>
                        </div>
                    </div>             
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Status <span class="text-danger">*</label>
                        <select class="form-control" name="BlogStatus" required="required">
                            <option value="">Please Select</option>
                            <option value="Published">Published</option>
                            <option value="Pending">Pending</option>
                            <option value="Draft">Draft</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Date Published</label>                                 
                        <input type="text" class="form-control" value="<?php echo date('d M Y'); ?> @ <?php echo date('h:i:a'); ?>" readonly="">
                    </div>                   

                    <div class="form-group">
                        <label>Featured Image</label>
                        <input id="file-0a" type="file" name="FeaturedImage" class="form-control file" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                        <span class="text-muted">Allowed file types: jpeg, jpg, png</span><br>
                        <span class="text-muted">Recommended image size: <b>width(<i>832px</i>) x height(<i>380px</i>)</b></span>
                    </div>

                    <div class="form-group">
                        <label>Category</label>
                        <select name="BlogCatID" class="form-control">
                        <?php if( $categories ){ ?>
                            <?php foreach( $categories as $cat ){ ?>
                                <option value="<?php echo $cat->BlogCatID; ?>"><?php echo $cat->CategoryName; ?></option>                                
                            <?php } ?>
                        <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Accessibility</label>
                        <select class="form-control" name="PublicView">
                            <option value="1">Public</option>
                            <option value="0">Seeders</option>
                        </select>
                    </div>

                    <div class="form-group text-center">                          
                        <button type="submit" class="btn btn-success btn-rounded text-uppercase blog-btn" style="min-width: 120px;">Publish</button>                            
                        <a id="preview" href="javascript:void(0);" class="btn btn-rounded btn-danger btn-warning text-uppercase" title="Preview this Blog Post!" data-toggle="tooltip" target"_blank">Preview</a>
                    </div>
                    
                </div>
            </div>
            
        </div>            

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){

        $( '#preview' ).click( function( e ){
            e.preventDefault();
            doSubmitForm();
        });

    });

    function doSubmitForm(){
        var url = "<?php echo View::url( 'clients/savepreview' ); ?>";
        var previewURL = "<?php echo View::url( 'clients/blog/preview/' ); ?>";
        var fdata = new FormData( $('#frmAdd')[0] );

        // Attach file
        if( $( '#file-0a' ).val() != '' ){
            fdata.append( 'FeaturedImage', $( 'input[type=file]' )[0].files[0] );
        }

        $.ajax({
            method: 'POST',
            url: url,
            data: fdata,
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function( data ){
                window.open( previewURL + data );
            },
            error: function( xhr, desc, err ){
                console.log( err );
            }
        });
    }

    $( "#file-0a" ).fileinput({
        allowedFileExtensions: [ "jpeg", "jpg", "png" ],
        maxImageWidth: 832,
        maxImageHeight: 380
    });
</script>