<?php 
    View::$title = 'Manage Blog';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container animated zoomIn">

        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage();  ?> 
                <table class="table js-dataTable-full-pagination dt-responsive table-divide table-header-bg table-hover table-vcenter" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url( 'clients/blog/add' ); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Create Blog</a>'>
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-center"><?php echo Lang::get('BLOG_MNG_DATE_PUBLISHED'); ?></th>
                            <th class="text-center"><?php echo Lang::get('BLOG_MNG_TITLE'); ?></th>
                            <th class="text-center"><?php echo Lang::get('BLOG_MNG_STATUS'); ?></th>
                            <th class="text-center">Category</th>
                            <th class="text-center">Accessibility</th>
                            <th class="text-center no-sorting" style="min-width:130px;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if( count( $blogPosts ) ){
                            foreach( $blogPosts as $article ){ $cntr++;
                                switch( $article->BlogStatus ){
                                    case 'Published':
                                        $bstatus = '<span class="label label-success">Published</span>';
                                        break;
                                    case 'Pending':
                                        $bstatus = '<span class="label label-warning">Pending</span>';
                                        break;
                                    case 'Draft':
                                        $bstatus = '<span class="label label-info">Draft</span>';
                                        break;
                                    default: $bstatus = ''; break;
                                }
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> ">
                            <td><?php echo isset( $article->BlogDatePublished ) && $article->BlogDatePublished != "0000-00-00" ? date( 'j M Y', strtotime( $article->BlogDatePublished ) ) : "-"; ?></td>
                            <td><a href="<?php echo View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID ); ?>" class="font-13"><?php echo isset( $article->BlogTitle ) ? $article->BlogTitle : ''; ?></a></td>
                            <td><?php echo $bstatus; ?></td>
                            <td><?php echo isset( $article->CategoryName ) ? $article->CategoryName : ''; ?></td>
                            <td><?php echo isset( $article->PublicView ) && $article->PublicView == 1 ? 'Public' : 'Seeders Only'; ?></td>
                            <td class="text-center">
                                <?php /*
                                <a href="<?php echo View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID ); ?>" title="View" class="btn btn-xs btn-danger" data-toggle="tooltip"><i class="fa fa-eye fa-2x"></i></a>
                                <a href="<?php echo View::url( 'clients/blog/edit/'.$article->BlogID ); ?>" title="Edit" class="btn btn-sm btn-default" data-toggle="tooltip"><i class="fa fa-pencil fa-1x"></i></a>
                                <?php if( $userinfo->Code == 'CLN' ){ ?>
                                <a href="<?php echo View::url( 'clients/blog/delete/'.$article->BlogID ); ?>" title="Delete" class="btn btn-sm btn-danger" data-toggle="tooltip" onclick="return confirm('Are you sure you want to put this Blog Post to trash bin?');"><i class="fa fa-times fa-1x"></i></a>
                                <?php }?>
                                */ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'clients/blog/view/'.$article->BlogSlug.'/'.$article->BlogID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-eye pull-right"></i> View</a></li>
                                            <li><a href="<?php echo View::url( 'clients/blog/edit/'.$article->BlogID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                            <li><a href="<?php echo View::url( 'clients/blog/delete/'.$article->BlogID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this blog?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php } 
                            } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="6">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>