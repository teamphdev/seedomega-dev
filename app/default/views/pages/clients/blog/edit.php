<?php 
View::$title = 'Edit Post';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  ?>
<!-- page content -->

<section class="breadcrumb">
    <article class="container animated fadeInDown">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/manage">Blog Manage</a></li>
                  <li class="fa fa-angle-right"></li>
                  <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/edit/"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <article class="container animated zoomIn">
        <div class="block-header">
            <h3 class=""><?php echo View::$title; ?></h3>
        </div>
        <input name="image" type="file" id="upload" class="hidden" onchange="">
        <form id="frmEdit" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
            <input type="hidden" id="action" name="action" value="editpost">
            <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
            <input type="hidden" name="BlogID" value="<?php echo $blogdata->BlogID; ?>">
            <input type="hidden" name="ClientProfileID" value="<?php echo $client->ClientProfileID; ?>">
            <input type="hidden" name="OldFeaturedImage" value="<?php echo isset( $blogdata->FeaturedImage ) ? $blogdata->FeaturedImage : '0'; ?>">

            <div class="block-content">
                
                <div class="row">
                    <?php echo View::getMessage(); ?>
                    <div class="col-sm-9">
                        <div class="push-30-r">
                            <div class="form-group">
                                <label>Title <span class="text-danger">*</label>
                                <input type="text" value="<?php echo $blogdata->BlogTitle;?>" name="BlogTitle" class="form-control col-xs-12" required="required">
                                <div>Permalink: <a href="<?php echo View::url( 'clients/blog/view/'.$blogdata->BlogSlug.'/'.$blogdata->BlogID ); ?>" target="_blank"><?php echo View::url( 'view/'.$blogdata->BlogSlug.'/'.$blogdata->BlogID ); ?></a></div>
                            </div>

                            <div class="form-group">
                                <label for="BlogContent">Content <span class="text-danger">*</label>
                                <textarea class="form-control tinyMCE" name="BlogContent" cols="30" rows="10"><?php echo $blogdata->BlogContent; ?></textarea>
                            </div>
                        </div>             
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Status <span class="text-danger">*</label>
                            <?php
                                $statusOptions = array('Published','Pending','Draft');
                            ?>

                            <select class="form-control" name="BlogStatus" required="">
                                <option value="">Please Select</option>
                                <?php foreach($statusOptions as $key):
                                        $selected = ($key == $blogdata->BlogStatus)? 'selected': '';
                                    ?>
                                    <option value="<?php echo $key; ?>" <?php echo $selected;?>><?php echo $key;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Date Published</label>                                 
                            <input type="text" class="form-control" value="<?php echo date('d M Y'); ?>" readonly="">
                        </div>

                        <div class="btn-group btn-group-justified push-20 push-20-t">
                            <div class="btn-group">
                                 <button id="save" type="submit" class="btn btn-sm btn-rounded btn-success text-uppercase">Save Changes</button>
                            </div>
                            <div class="btn-group">
                                <a id="preview" href="javascript:void(0);" class="btn btn-sm btn-rounded btn-danger text-uppercase" title="Preview this Blog Post!" data-toggle="tooltip" target"_blank"><i class="fa fa-eye"></i>&nbsp;Preview</a>
                            </div>
                        </div>
                        
                        <?php if( $userinfo->Code == 'CLN' ){ ?>
                            <a href="<?php echo View::url( 'clients/blog/delete/'.$blogdata->BlogID ); ?>" title="Delete this Blog Post!" class="btn btn-sm btn-danger btn-block btn-rounded text-uppercase push-20 push-10-t" data-toggle="tooltip" onclick="return confirm('Are you sure you want to put this Blog Post to trash bin?');"><i class="fa fa-times"></i>&nbsp;Delete</a>
                        <?php }?>
                        <div class="clear"></div>
                        

                        <div class="form-group">
                            <label>Featured Image</label>
                            <?php if ($blogdata->FeaturedImage): ?>
                                <?php $featuredImage = View::common()->getUploadedFiles($blogdata->FeaturedImage); ?>
                                <?php echo View::photo((isset($featuredImage[0]) ? '/files/'.$featuredImage[0]->FileSlug : '/images/user.png'),"img",""); ?>
                            <?php endif;?>
                            
                            <br>
                            <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="FeaturedImage" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                            <span class="text-muted">Allowed file types: jpeg, jpg, png</span><br>
                            <span class="text-muted">Recommended image size: <b>width(<i>832px</i>) x height(<i>380px</i>)</b></span>
                        </div>

                        <div class="form-group">
                            <label>Category</label>
                            <select name="BlogCatID" class="form-control">
                            <?php if( $categories ){ ?>
                                <?php foreach( $categories as $cat ){
                                    $selected = $cat->BlogCatID == $blogdata->BlogCatID ? 'selected': '';
                                    ?>
                                    <option value="<?php echo $cat->BlogCatID; ?>" <?php echo $selected?>><?php echo $cat->CategoryName; ?></option>
                                <?php } ?>
                            <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Accessibility</label>
                            <?php $publicViewOptions = array( 1 => 'Public', 0 => 'Seeders' ); ?>
                            <select class="form-control" name="PublicView" required="">
                                <?php foreach( $publicViewOptions as $key => $val ):
                                    $selected = $key == $blogdata->PublicView ? 'selected': '';
                                    ?>
                                    <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        
                    </div>
                </div>
                
            </div>

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){

        $( '#preview' ).click( function( e ){
            e.preventDefault();
            doSubmitForm();
        });

    });

    function doSubmitForm(){
        var url = "<?php echo View::url( 'clients/savepreview' ); ?>";
        var previewURL = "<?php echo View::url( 'clients/blog/preview/'.$blogdata->BlogID ); ?>";
        var fdata = new FormData( $('#frmEdit')[0] );

        // Attach file
        if( $( '#file-0a' ).val() != '' ){
            fdata.append( 'FeaturedImage', $( 'input[type=file]' )[0].files[0] );
        }

        $.ajax({
            method: 'POST',
            url: url,
            data: fdata,
            dataType: "json",
            contentType: false,
            cache: false,
            processData: false,
            success: function( data ){
                window.open( previewURL );
            },
            error: function( xhr, desc, err ){
                console.log( err );
            }
        });
    }

    $( "#file-0a" ).fileinput({
        allowedFileExtensions: [ "jpeg", "jpg", "png" ],
        maxImageWidth: 832,
        maxImageHeight: 380
    });
</script>