<?php
View::$title = 'Investors';
View::$bodyclass = User::info('Sidebar');
View::header();
?>
    <section class="header-bottom">
        <article>
            <div class="container"><h1>Investors</h1></div>
        </article>
    </section>

    <section class="breadcrumb">
        <article class="container">
            <div class="row">
                <div class="col-lg-6">
                    <ul>
                        <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                      <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
                      <li class="fa fa-angle-right"></li>
                      <li><a href="<?php echo View::url('users/dashboard'); ?>"><?php echo View::$title; ?></a></li>
                    </ul>
                </div>
                <div class="col-lg-6 align-right sub-menu">

                </div>
            </div>
        </article>
    </section>

    <!-- ************************ Page Content ************************ -->
<!-- Page Content -->
<div class="container">
    <!-- Full Table -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Investors</h3>
        </div>
        <div class="block-content">
            <div class="table-responsive">
                <table class="table  table-vcenter">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 120px;"><i class="si si-user"></i></th>
                        <th>Name</th>
                        <th style="width: 20%;">Shares</th>
                        <th style="width: 10%;">Total Amount</th>
                        <th>Signed Date</th>

                        <th style="width: 15%;">Status</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($investors): foreach($investors as $inv): ?>
                        <tr>
                            <td class="text-center">
                                <?php $avatar = View::common()->getUploadedFiles($inv->Avatar); ?>
                                <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar","img-avatar img-avatar48"); ?>
                            </td>
                            <td class="font-w600"><?php echo $inv->InvestorName;?></td>
                            <td><?php echo $inv->SharesToApply;?>%</td>
                            <td>$<?php echo $inv->TotalAmountAttached;?></td>
                            <td><?php echo $inv->SignedDate;?></td>

                            <td>
                                <?php
                                switch($inv->Status){
                                    case 'Approved':
                                    case 'approved':
                                        $statusClass = 'label-success';
                                        break;
                                    case 'Verified':
                                    case 'verified':
                                        $statusClass = 'label-info';
                                        break;
                                    case 'Pending':
                                    case 'pending':
                                        $statusClass = 'label-warning';
                                        break;default;


                                }
                                ?>
                                <span class="label <?php echo $statusClass;?>"><?php echo $inv->Status;?></span>
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="/clients/viewttreceipt/<?php echo $inv->InvestmentBookingID;?>/<?php echo $inv->TTReceiptPhoto;?>" class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="View TT Receipt"><i class="fa fa-file fa-1x"></i></a>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; endif; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!--end container-->

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 4,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });

    });
</script>