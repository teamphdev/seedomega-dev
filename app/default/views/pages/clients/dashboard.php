<?php
View::$title = 'Dashboard';
View::$bodyclass = User::info('Sidebar').' client-dashboard-page';
View::header();
?>

    <section class="header-bottom">
        <article>
            <div class="container animated fadeInDown">
                <h1>Create opportunities</h1>
                <p>The eligibility of potential investors to invest in the companies below will be clearly outlined within each investment's page.</p>
            </div>
        </article>
    </section>

    <!-- content start -->
    <section class="gray">
        <article class="container projects-page" id="clientdashboard">
            <div class="row">
                <?php echo View::getMessage();  ?>

                <div class="col-md-8 animated fadeInLeft">
                    <div class="block">
                        <!-- Booking Summary -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="booking-summary">

                                    <div class="block-header">
                                        <div class="block-title">Booking Summary</div>
                                    </div>
                                    <?php /*
                                    <div class="block-content block-content-full text-center">
                                        <!-- Lines Chart Container -->
                                        <div style="height: 188px;"><canvas class="js-dash-chartjs-lines"></canvas></div>
                                    </div>
                                    */?>
                                    <div class="block-content text-center">
                                        <div class="row items-push text-center">
                                            <div class="col-xs-12 col-lg-4">
                                                <div class="box-circle">
                                                    <?php echo $percentFundFinal;?>%
                                                </div>
                                                <div class="push-10 icon"><i class="si si-bar-chart fa-1x"></i></div>
                                                <label for="">Percent Funded</label>
                                                
                                            </div>
                                            <div class="col-xs-12 col-lg-4">
                                                <div class="box-circle">
                                                    <?php echo number_format($totalBooked);?>
                                                </div>
                                                <div class="push-10 icon"><i class="fa fa-bookmark fa-1x"></i></div>
                                                <label for="">Booked</label>
                                            </div>                                        
                                            <div class="col-xs-12 col-lg-4">
                                                <div class="box-circle">
                                                    <?php echo number_format($investorsCount); ?>
                                                </div>
                                                <div class="push-10 icon"><i class="fa fa-user fa-1x"></i></div>
                                                <label for="">Seeders</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END Booking Summary -->

                        <!-- Activity List -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="block-content">

                                    <div class="block-header-2">Activity List</div>
                                    <div class="">
                                        <?php $ctr = 0;
                                        if( isset( $activities ) && count( $activities ) ){ ?>
                                        <ul class="activity-list">
                                        <?php foreach( $activities as $activity ){
                                            $mydate = isset( $activity->TransDate ) ? date( 'M j Y', strtotime( $activity->TransDate ) ) : '-';
                                            $amount = isset( $activity->TotalAmountAttached ) ? number_format( $activity->TotalAmountAttached, 2 ) : '0.00';
                                            $balance = isset( $activity->BalanceDue ) ? number_format( $activity->BalanceDue, 2 ) : '0.00';

                                            switch( $activity->Transaction ){
                                                case 'BookingPending':
                                                    echo '<li><i class="si-paper-clip"></i> Booking application has been submitted by <b>'.$activity->FullName.'</b> on '.$mydate.', with an amount of <b>$'.$amount.'</b>. <a href="/clients/funds/view/'.$activity->ID.'" class="btn btn-success btn-rounded">View</a></li>';
                                                    break;
                                                case 'BookingVerified':
                                                    echo '<li>Booking application by <b>'.$activity->FullName.'</b> on '.$mydate.', with an amount of <b>$'.$amount.'</b> has been verified. <a href="/clients/funds/view/'.$activity->ID.'" class="btn btn-rounded">view</a></li>';
                                                    break;
                                                case 'Replies':
                                                    echo '<li>New reply for topic ('.$activity->Title.') has been posted by <b>'.$activity->FullName.'</b>. <a href="/forums/topic/'.$activity->CatID.'/'.$activity->SubCatID.'/'.$activity->ID.'/'.$activity->SubCatName.'/'.$activity->Title.'" class="btn btn-success btn-rounded">view</a></li>';
                                                    break;
                                                case 'Invoices':
                                                    echo '<li>New invoice with ID#'.$activity->ID.' has been created. Waiting for TT Receipt upload. Booked by <b>'.$activity->FullName.'</b>, with an amount of <b>$'.$amount.'</b> and balance due of <b>$'.$balance.'</b>. <a href="/clients/invoices/uploadtt/'.$activity->ID.'" class="btn btn-success btn-rounded">view</a></li>';
                                                    break;
                                                
                                                default: break;
                                            }
                                        } ?>
                                        </ul>
                                        <?php } ?>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END Activity List -->

                        <!-- Quickly Posting -->
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="quick-post block-content">
                                    <div class="block-header-2 pd">Quick Posting</div>
                                    <div class="">
                                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                            <input type="hidden" name="action" value="addtopic">
                                            <input type="hidden" name="topic[UserID]" value="<?php echo isset( $userinfo->UserID ) ? $userinfo->UserID : ''; ?>">
                                            <input type="hidden" name="topic[Epigram]" value="<?php echo $epigram; ?>">
                                            
                                            <div class="form-group">
                                                <div class="form-left">
                                                    <label>Forum Category</label>
                                                    <select class="js-select2 form-control select2-hidden-accessible-show <?php echo $disabled; ?>" id="topic[IDs]" name="topic[IDs]" style="width: 100%;" data-placeholder="Please select a category" tabindex="-1" aria-hidden="true">
                                                        <option></option>
                                                        <?php if( isset( $options ) && count( $options ) ){
                                                        foreach( $options as $k => $v){ ?>
                                                            <?php if( strpos( $k, 'disabled' ) !== false ){ ?>
                                                                    <option value="" disabled=""><?php echo isset( $v ) ? $v : ''; ?></option>
                                                            <?php } else { ?>                                                    
                                                                    <option value="<?php echo isset( $k ) ? $k : ''; ?>"><?php echo isset( $v ) ? $v : ''; ?></option>
                                                            <?php } ?>
                                                        <?php } } ?>
                                                    </select>
                                                </div>
                                                <div class="form-right">
                                                    <label for="topic-title">Topic Title</label>
                                                    <input class="form-control <?php echo $disabled; ?>" type="text" id="topic-title" name="topic[Title]" placeholder="Please enter a title">
                                                </div>
                                            </div>                                        
                                            
                                            <?php /*
                                            <div class="form-group bottom-zero">
                                                <div class="form-left inline-block">
                                                    <div class="form-material form-material-primary">
                                                        <select class="js-select2 form-control select2-hidden-accessible-show <?php echo $disabled; ?>" id="topic[IDs]" name="topic[IDs]" style="width: 100%;" data-placeholder="Please select a category" tabindex="-1" aria-hidden="true">
                                                            <option></option>
                                                            <?php if( isset( $options ) && count( $options ) ){
                                                            foreach( $options as $k => $v){ ?>
                                                                <?php if( strpos( $k, 'disabled' ) !== false ){ ?>
                                                                        <option value="" disabled=""><?php echo isset( $v ) ? $v : ''; ?></option>
                                                                <?php } else { ?>                                                    
                                                                        <option value="<?php echo isset( $k ) ? $k : ''; ?>"><?php echo isset( $v ) ? $v : ''; ?></option>
                                                                <?php } ?>
                                                            <?php } } ?>
                                                        </select>
                                                        <label for="topic-forum">Forum Category <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="form-right inline-block">
                                                    <div class="form-material form-material-primary">
                                                        <input class="form-control <?php echo $disabled; ?>" type="text" id="topic-title" name="topic[Title]" placeholder="Please enter a title">
                                                        <label for="topic-title">Topic Title <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                            */ ?>

                                            <div class="form-group bottom-zero">
                                                <textarea class="form-control quick-textarea <?php echo $disabled; ?> tinyMCE" name="topic[Content]" cols="30" rows="5"><?php echo $disabled == '' ? '' : 'No category yet, click the button below to configure!'; ?></textarea>
                                            </div>
                                            <div class="form-group text-center push-20-t">
                                                <div class="col-xs-12">
                                                <?php if( $disabled == '' ){ ?>
                                                    <button class="btn btn-rounded btn-default" type="submit">Submit Topic</button>
                                                <?php } else { ?>
                                                    <a class="btn btn-4 btn-rounded btn-primary" href="<?php echo View::url( 'forums/categories' ); ?>">Configure Categories</a>
                                                <?php } ?>
                                                    <button class="btn btn-sm btn-default btn-rounded hidden" type="button">Preview</button>
                                                    <button class="btn btn-sm btn-default btn-rounded hidden" type="button">Draft</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END Quickly Posting -->
                    </div>
                </div> 

                <div class="col-lg-4 animated fadeInRight">
                    <div class="sidebar">
                        <div class="sidebar-item">
                            <div class="w-title">What Can I Do Next?</div>
                            <div class="w-content">
                                <div class="popular-posts-widget viewbookhelp">
                                    <div class="side-nav-wcidn">
                                      <span class="num">1</span>
                                      <div class="num-content">
                                          <h4 class="wcidn-title"><a>Preview my Profile</a></h4>
                                          <p class="wcidn-text">Need to see how your profile will look when a user read your profile? Click preview below:</p>
                                          <a href="<?php echo View::url('clients/profile'); ?>" class="btn btn-xs btn-rounded btn-default " target="_blank">Edit</a>
                                          <a href="<?php echo View::url('project/view'); ?>" class="btn btn-xs btn-rounded btn-default " target="_blank">Preview</a>
                                      </div>
                                    </div>
                                    <div class="side-nav-wcidn">
                                      <span class="num">2</span>
                                      <div class="num-content">
                                          <h4 class="wcidn-title"><a>Pending Fund Acknowledgement</a></h4>                                          
                                          <p class="wcidn-text">Check out how many seeder's booking fund transfer that awaits confirmation.</p>
                                            <div class="row">
                                                <div class="col-xs-6 text-center">
                                                    <div class="h1 font-thin"><a href="/clients/funds"><?php echo $verifiedFundsCount; ?></a></div>
                                                    <div class="text-muted"><small><i class="si si-clock fa-1x push-5-r"></i> PENDING</small></div>
                                                </div>
                                                <div class="col-xs-6 text-center">
                                                    <div class="h1 font-thin"><a href="/clients/booked/registeredinterest"><?php echo $interestFundsCount; ?></a></div>
                                                    <div class="text-muted"><small><i class="si si-feed fa-1x push-5-r"></i> INTEREST</small></div>
                                                </div>
                                            </div>
                                      </div>
                                    </div>
                                    <div class="side-nav-wcidn">
                                      <span class="num">3</span>
                                      <div class="num-content">
                                          <h4 class="wcidn-title"><a>Invoices</a></h4>
                                          <p class="wcidn-text">Check out how many invoices needed to be paid.</p>
                                          <div class="row">
                                                <div class="col-xs-6 text-center">
                                                    <div class="h1 font-thin"><a href="/clients/invoices"><?php echo $invoicesNoTTReceiptCount; ?></a></div>
                                                    <div class="text-muted"><small><i class="fa fa-calendar-times-o fa-1x push-5-r"></i> OUTSTANDING</small></div>
                                                </div>
                                                <div class="col-xs-6 text-center">
                                                    <div class="h1 font-thin"><a href="/clients/invoices"><?php echo $invoicesOverdueCount;?></a></div>
                                                    <div class="text-muted"><small><i class="si si-clock fa-1x push-5-r"></i> OVERDUE</small></div>
                                                </div>
                                            </div>
                                      </div>
                                    </div>
                                    <div class="side-nav-wcidn">
                                      <span class="num">4</span>
                                      <div class="num-content">
                                          <h4 class="wcidn-title"><a>Manage my Community</a></h4>
                                          <p class="wcidn-text">While waiting for the seeders to swarm your project you can add something helpful for them.</p>
                                          <ul>
                                            <?php if( $userinfo->Code == 'CLN' ){ ?>
                                              <li><a class="btn btn-xs btn-rounded btn-default" href="<?php echo View::url('clients/assistant/manage/'); ?>">Assistants</a></li>
                                            <?php } ?>
                                              <li><a class="btn btn-xs btn-rounded btn-default" href="<?php echo View::url('clients/blog/manage/'); ?>">Blogs</a></li>
                                              <li><a class="btn btn-xs btn-rounded btn-default" href="<?php echo View::url('clients/faq/manage/'); ?>">FAQs</a></li>
                                              <li><a class="btn btn-xs btn-rounded btn-default" href="<?php echo View::url('forums/dashboard/'); ?>">Forums</a></li>
                                          </ul>
                                      </div>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         
                <!--------------------sidenav end --------------------> 
            </div>
        </article>
    </section>
    <!-- content end -->

    <!-- comments start -->     
    <div class="block comments" id="cln-comments">
        <a href="#" class="minmax" id="minmax"><i class="si si-speech"></i></a>
        <div class="block-header">
            <div class="block-title">Comments</div>
        </div>
        <div id="client-comments" class="block-content hidden">
            <div id="client-comments-content" class="comments-tab client-page">
                
                <div id="CommentsWrapper" class="actionBox">
                  <ul class="commentList">
                    <?php $ctr = 0; $cctr = 0;
                    if( isset( $clientcomments ) ){
                        if ( count( $clientcomments ) ){
                            foreach( $clientcomments as $comment ){ $ctr++; ?>
                            <li id="parent<?php echo $ctr; ?>" class="role<?php echo $comment->Level; ?>">
                                <div class="commenterImage">
                                    <?php echo ($comment->Level == 4) ? '<div class="sicon">'.$comment->AvatarLink.'</div>' : $comment->AvatarLink; ?>
                                    <?php echo ($comment->Level == 4) ? '<i class="fa clnt-icon fa-star"></i>' : ''; ?>
                                    <?php echo ($comment->Level == 6) ? '<i class="fa ast-icon fa-circle text-warning"></i>' : '';?>
                                </div>
                                <div class="commentText">
                                    <div class=""><strong><?php echo $comment->FirstName.' '.$comment->LastName; ?></strong>:&nbsp; <?php echo $comment->CommentContent; ?>
                                    </div>
                                    <span class="date sub-text">
                                        <a href="#" data-id="<?php echo isset( $comment->CommentID ) ? $comment->CommentID : '0'; ?>" class="comment-reply">Reply</a> <span>·</span>
                                        <a href="#" data-id="<?php echo isset( $comment->CommentID ) ? $comment->CommentID : '0'; ?>" data-cid="parent<?php echo $ctr; ?>" class="comment-delete">Delete</a> <span>·</span>
                                        <?php echo date('M jS, Y', strtotime( $comment->CommentDate ) ); ?>
                                    </span>
                                </div>
                                <ul>
                                <?php $cctr = 0;
                                if( isset( $comment->Children ) && count( $comment->Children ) ){
                                foreach( $comment->Children as $reply ){ $cctr++; ?>
                                    <li id="child<?php echo $ctr.'-'.$cctr; ?>" class="role<?php echo $reply->Level; ?>">
                                        <div class="commenterImage">
                                            <?php echo ($reply->Level == 4) ? '<div class="sicon">'.$reply->AvatarLink.'</div>' : $reply->AvatarLink; ?>
                                            <?php echo ($reply->Level == 4) ? '<i class="fa clnt-icon fa-star"></i>' : ''; ?>
                                            <?php echo ($reply->Level == 6) ? '<i class="fa ast-icon fa-circle text-warning"></i>' : '';?>
                                        </div>
                                        <div class="commentText">
                                            <div class="">
                                                <strong><?php echo $reply->FirstName.' '.$reply->LastName; ?></strong>:&nbsp; <?php echo $reply->CommentContent; ?>
                                            </div>
                                            <a href="#" data-id="<?php echo isset( $reply->CommentID ) ? $reply->CommentID : '0'; ?>" data-cid="child<?php echo $ctr.'-'.$cctr; ?>" class="comment-delete">Delete</a> <span>·</span>
                                            <?php echo date('M jS, Y', strtotime( $reply->CommentDate ) ); ?>
                                            <?php /* <span class="date sub-text">on <?php echo date('M jS, Y', strtotime( $reply->CommentDate ) ) ?></span> */ ?>
                                        </div>
                                    </li>
                                <?php } } else { ?>
                                    <li></li>
                                <?php } ?>
                                </ul>
                            </li>
                    <?php } } } ?>
                  </ul>
                </div>
            </div>
            <form id="formComments" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                <input type="hidden" name="action" value="sendcommentdashboard" />
                <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
                <input type="hidden" name="ClientProfileID" value="<?php echo $client->ClientProfileID; ?>">
                <div class="form-group role<?php echo $currUser->Level; ?>">
                    <div class="commenterImage write-comment-image inline-block">
                        <?php echo $currUser->AvatarLink; ?>
                        <?php echo $currUser->Indicator; ?>
                    </div>
                    <div class="write-comment inline-block">
                        <input id="CommentContent" type="text" value="" class="form-control radiusx" placeholder="Write a comment..." name="CommentContent" required="required">
                        <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>
                    </div>
                    <div class="clear"></div>
                </div>
            </form>
            <input type="hidden" id="currName" value="<?php echo $currUser->Name; ?>">
            <input type="hidden" id="Level" value="<?php echo $userinfo->Level; ?>">
            <input type="hidden" id="AvatarLink" value="<?php echo $currUser->AvatarLinkConverted; ?>">
            <input type="hidden" id="Indicator" value="<?php echo $currUser->Indicator; ?>">
        </div>
    </div>
    <!-- comments end --> 
<?php View::footer(); ?>

<script type="text/javascript">
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    $('#client-comments').hide();
    $( document ).ready( function(){
        $('#client-comments').removeClass( 'hidden' );
        // autoScroll();
    });

    $( '#minmax' ).click(function(){
        toggleComments();
    });

    $( '.block-header' ).click(function(){
        toggleComments();
    });

    $( '.comment-reply' ).click( function(e){
        e.preventDefault();
        $( '#formCommentsReply' ).remove(); //remove first the previous form
        $( 'li' ).removeClass( 'referenceComment' ); //remove first the previous classname
        $( this ).closest( 'li' ).find( 'ul li' ).last().addClass( 'referenceComment' );
        commentid = $(this).attr( 'data-id' );
        replyWrapper = '<form id="formCommentsReply" class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">';
        replyWrapper += '<input type="hidden" name="action" value="sendcommentdashboard" />';
        replyWrapper += '<input type="hidden" name="ParentID" value="'+commentid+'">';
        replyWrapper += '<input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">';
        replyWrapper += '<input type="hidden" name="ClientProfileID" value="<?php echo $client->ClientProfileID; ?>">';
        replyWrapper += '<div class="form-group role<?php echo $currUser->Level; ?>">';
        replyWrapper += '   <div class="write-comment-image inline-block">';
        replyWrapper += '       <?php echo $currUser->AvatarReplyLink; ?>';
        replyWrapper += '       <?php echo $currUser->Indicator; ?>';
        replyWrapper += '   </div>';
        replyWrapper += '   <div class="write-comment inline-block">';
        replyWrapper += '       <input id="ReplyContent" type="text" value="" class="form-control" placeholder="Write a reply..." name="CommentContent" required="required">';
        replyWrapper += '       <span id="comment-icon" class="icon fa fa-comment transition-color pull-right" title="Comments"></span>';
        replyWrapper += '   </div>';
        replyWrapper += '   <div class="clear"></div>';
        replyWrapper += '</div>';
        replyWrapper += '</form>';

        if( $( this ).closest( 'li' ).find( 'ul li' ) ){
            $( this ).closest( 'li' ).find( 'ul li' ).last().after( replyWrapper );
        } else {
            $( this ).parent().append( replyWrapper );
        }
        $( '#ReplyContent' ).focus();
    });

    $( '.comment-delete' ).click( function(e){
        e.preventDefault();
        var id = $( this ).attr( 'data-id' );
        var cid = $( this ).attr( 'data-cid' );
        deleteComment( id, cid );
    });

    $( document ).keypress( function(e){
        if( e.keyCode == 13 ){
            e.preventDefault();
            var reply = $('#ReplyContent').val();
            var comment = $('#CommentContent').val();
            refid = 'REFID'+new Date().getTime();

            if( comment.trim().length > 0 ){
                saveComment( '#formComments', refid );
                appendHTML( comment, 'comment', refid );
            }
            if( reply.trim().length > 0 ){
                saveComment( '#formCommentsReply', refid );
                appendHTML( reply, 'reply', refid );
            }
        }
    });

    function toggleComments(){
        $('#client-comments').slideToggle();
        autoScroll();
    }

    //save comment to database
    function saveComment( formid, refid ){
        $.ajax({
            type: 'POST',
            url: '/clients/comments/'+refid,
            data: $( formid ).serialize(),
            success: function(){}
        });
    }

    //delete comment from database
    function deleteComment( id, cid ){
        var checkstr =  confirm('are you sure you want to delete this comment?');
        if( checkstr == true ){
            $.ajax({
                type: 'POST',
                url: '/clients/delcomm/'+id,
                data: '',
                success: function(){}
            });
            $( '#'+cid ).remove();
        } else { return false; }
    }

    //append comment/reply for comment box
    function appendHTML( msg, type, refid ){
        var now = new Date();
        now = monthNames[ now.getMonth() ] + ' ' + getGetOrdinal( now.getDate() ) + ', ' + now.getFullYear();

        var name = $('#currName').val();
        var leve = $('#Level').val();
        var link = $('#AvatarLink').val();
        var indi = $('#Indicator').val();
        var html = '<li class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <span class="date sub-text">on '+now+'</span></div></li>';
        html = '<li id="'+refid+'" class="role'+leve+' referenceComment"><div class="commenterImage">'+link+indi+'</div><div class="commentText"><div class=""><strong>'+name+'</strong>:&nbsp; '+msg+'</div> <a href="#" data-id="'+refid+'" class="append-delete">Delete</a> <span>·</span> '+now+'</div></li>';

        if( type == 'comment' ){
            $( '.actionBox > ul' ).append( html );
            $( '#CommentContent' ).val( '' );
        } else {
            $( '.referenceComment' ).removeClass( 'referenceComment' ).after( html );
            $( '#ReplyContent' ).val( '' );
        }

        $( '.append-delete' ).click( function(e){
            e.preventDefault();
            var id = $( this ).attr( 'data-id' );
            deleteComment( id, id );
        });
        autoScroll();
    }

    function getGetOrdinal( n ){
        var s = ["th","st","nd","rd"], v = n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }

    function autoScroll(){
        $( '#client-comments-content' ).scrollTop( $('#client-comments-content')[0].scrollHeight );
        // $('#client-comments-content').animate({scrollTop:$('#client-comments-content')[0].scrollHeight}, 2000);
        // setTimeout(function(){ $('#client-comments').slideToggle(); },2500);
    }

/*
 *  Document   : base_pages_dashboard.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Dashboard Page
 */
var BasePagesDashboard = function(){
    // Chart.js Chart, for more examples you can check out http://www.chartjs.org/docs
    var initDashChartJS = function(){
        // Get Chart Container
        var $dashChartLinesCon  = jQuery('.js-dash-chartjs-lines')[0].getContext('2d');

        // Set Chart and Chart Data variables
        var $dashChartLines, $dashChartLinesData;

        // Lines Chart Data
        var $dashChartLinesData = {
            labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'],
            datasets: [
                {
                    label: 'This Week',
                    fillColor: 'rgba(44, 52, 63, .07)',
                    strokeColor: 'rgba(44, 52, 63, .25)',
                    pointColor: 'rgba(44, 52, 63, .25)',
                    pointStrokeColor: '#fff',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(44, 52, 63, 1)',
                    data: [1000, 4000, 1400, 7000, 800, 5000, 4000]
                },
                // {
                //     label: 'Last Week',
                //     fillColor: 'rgba(44, 52, 63, .1)',
                //     strokeColor: 'rgba(44, 52, 63, .55)',
                //     pointColor: 'rgba(44, 52, 63, .55)',
                //     pointStrokeColor: '#fff',
                //     pointHighlightFill: '#fff',
                //     pointHighlightStroke: 'rgba(44, 52, 63, 1)',
                //     data: [18, 19, 20, 35, 23, 28, 50]
                // }
            ]
        };

        // Init Lines Chart
        $dashChartLines = new Chart($dashChartLinesCon).Line($dashChartLinesData, {
            scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            scaleFontColor: '#999',
            scaleFontStyle: '600',
            tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
            tooltipCornerRadius: 3,
            maintainAspectRatio: false,
            responsive: true
        });
    };

    return {
        init: function () {
            // Init ChartJS chart
            initDashChartJS();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ 
    BasePagesDashboard.init();
});
</script>