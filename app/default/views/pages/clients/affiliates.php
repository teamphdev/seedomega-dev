<?php 
    View::$title = 'Manage Affiliates';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
                    <?php if( isset(View::$segments[1]) ) { ?>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-6 align-right sub-menu">
               
            </div>
        </div>
    </article>
</section>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage();  ?> 
                <table class="table js-table-checkable js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" cellspacing="0" width="100%">
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-align"><?php echo Lang::get('USR_MNG_IMG'); ?></th>
                            <th class="sort-this"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_EML'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_STATUS'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if( count( $affiliates ) ){
                            foreach( $affiliates as $user ){ $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <?php $avatar = View::common()->getUploadedFiles($user->Avatar); ?>
                            <td class="text-center"><img src="<?php echo View::url('assets/'. ( ($avatar) ? 'files/'.$avatar[0]->FileSlug : 'images/user.png' )); ?>" style="border-radius:50%; height:30px; width:30px;" /></td>
                            <td><?php echo $user->UserID; ?></td>
                            <td><?php echo $user->Email; ?></td>
                            <td><?php echo $user->AffiliateName; ?></td>
                            <td>
                                <?php $labelClass = ($user->Status == 'Active') ? 'success' : 'warning'; ?>
                                <span class="label label-<?php echo $labelClass;?>"><?php echo $user->Status; ?></span>
                            </td>
                        </tr>
                        <?php } 
                            } else { ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="5">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>