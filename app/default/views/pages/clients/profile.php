<?php
    View::$title = 'Company Profile';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header();
?>

    <section class="header-bottom no-padding">
        <article>
            <div class="container animated fadeInDown">

                <div class="start-project profile-tabs profile-tabs-4" id="profile_wizard">
                
                    <div class="title">
                        <ul id="profileTabs">
                            <li data-link="company-info" class="" id="tab-company-info"><a href="<?php echo View::url( 'clients/profile/company-info'.$ClientProfileID ); ?>"><i class="fa fa-pagelines"></i><span>1: Company Info</span></a></li>
                            <li data-link="offer" class="" id="tab-offer"><a href="<?php echo View::url( 'clients/profile/offer'.$ClientProfileID ); ?>" ><i class="fa fa-link"></i><span>2: Offer</span></a></li>
                            <li data-link="documents" class="" id="tab-documents"><a href="<?php echo View::url( 'clients/profile/documents'.$ClientProfileID ); ?>"><i class="fa fa-tags"></i><span>3: Documents</span></a></li>
                            <li data-link="bank-accounts" class="" id="tab-bank-accounts"><a href="<?php echo View::url( 'clients/profile/bank-accounts'.$ClientProfileID ); ?>"><i class="fa fa-bank"></i><span>4: Bank Accounts</span></a></li>
                            <li data-link="summary" class="" id="tab-summary"><a href="<?php echo View::url( 'clients/profile/summary'.$ClientProfileID ); ?>"><i class="fa fa-file-image-o"></i><span>5: Summary</span></a></li>
                            <li data-link="teams" class="" id="tab-teams"><a href="<?php echo View::url( 'clients/profile/teams'.$ClientProfileID ); ?>"><i class="si si-users"></i><span>6: Teams</span></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </article>
    </section>

    <section class="gray min-height-20 no-padding" style="margin-top:-20px;">
        <div class="container animated bounceIn">

            <div class="profile-progress">
                <div class="pp-percentage"><?php echo isset( $percentcomplete ) ? $percentcomplete : 0; ?>%</div>
                <div class="pp-label">Completed</div>
                <div class="pp-progress">
                    <div class="pp-line" style="width: <?php echo isset( $percentcomplete ) ? $percentcomplete : 0; ?>%;"></div>
                </div>
            </div>

        </div>
    </section>

    <!-- ************************ Page Content ************************ -->
    <script>
        <?php if( $tab != '' ){ ?>
        var selectedTab = '<?php echo $tab; ?>';
        <?php } ?>
    </script>

    <section class="gray no-padding">
        <div class="text-right">
            <div class="container animated zoomIn">
                <ul class="push-20 push-20-t" style="list-style: none;">
                    <?php if( ($userinfo->Code == 'CLN' || $userinfo->Code == 'ASST' ) && $client->Status == 'Approved' && $tab != 'teams' ){ ?>
                    <li><a href="<?php echo View::url( 'clients/changeprofile/'.$tab ); ?>" class="btn btn-default green btn-rounded text-uppercase" data-toggle="tooltip"><i class="fa fa-edit"></i> Request Profile Update</a></li>

                    <?php } ?>
                </ul>
            </div>
        </div>
        <article class="container project-single animated slideInUp">
            <div class="start-project nobg" >
                <!-- Main Content -->
                <?php echo View::getMessage(); ?>
                <?php echo $notice; ?>
                <div class="start-content no-padding push-40">
                    <!-- Company Tab -->
                    <div id="company-info" class="form-wizard">
                        <input name="image" type="file" id="upload" class="hidden" onchange="">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formCompanyInfo">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo isset( $client->ClientProfileID ) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo isset( $client->UserID ) ? $client->UserID : '0'; ?>" />
                            <input type="hidden" name="section" value="companyinfo" />
                            <input type="hidden" name="next" value="offer" id="companytab" />
                            <input type="hidden" name="preview" value="" id="companypreview" />

                            <div class="row">
                                <div class="col-lg-8">

                                    <div class="block-content push-20">

                                        <?php echo $info; ?>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('CLN_PRF_COMPNAME'); ?> <span class="text-danger">*</span></label>
                                            <input type="text" value="<?php echo (isset($client->CompanyName))?$client->CompanyName:''; ?>" id="CompanyName" name="client[CompanyName]" class="form-control<?php echo $client->Disabled; ?>" required="required">
                                        </div>
                                        <div class="parent-image">
                                            <label>Company Logo <span class="text-danger">*</span></label>
                                            <input id="file-0a" class="file form-control company-logo<?php echo $client->Disabled; ?>" type="file" data-min-file-count="0" name="CompanyLogo" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]' required="required">
                                            <span class="text-muted">Allowed file types: jpeg, jpg, png<br>Recommended image size: <b>width (<i>350px</i>) x height (<i>350px</i>)</b></span>
                                        </div>

                                        <div class="form-group">
                                            <div class="parent-image">
                                                <label>Company Image <span class="text-danger">*</span></label>
                                                <input id="file-0b" class="file form-control company-photo<?php echo $client->Disabled; ?>" type="file" data-min-file-count="0" name="CompanyPhoto" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]' required="required">
                                                <span class="text-muted">Allowed file types: jpeg, jpg, png<br>Recommended image size: <b>width (<i>1900px</i>) x height (<i>450px</i>)</b></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="">
                                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('CLN_PRF_VIDEO'); ?><br>Required Dimension: Width = 560 pixels, Height = 315 pixels</label>
                                            <textarea class="form-control<?php echo $client->Disabled; ?>" name="client[Video]" rows="2" style="height:75px !important"><?php echo (isset($client->Video))?$client->Video:''; ?></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo Lang::get('CLN_PRF_VIDEODESC'); ?></label>
                                            <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[VideoDescription]" id="VideoDescription"><?php echo @$client->VideoDescription; ?></textarea>
                                        </div>
                                        <div class="ln_solid"></div>

                                        <div class="form-group <?php echo $approvehide; ?>">
                                            <div class="text-center">
                                                <div class="next-btn">
                                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="document.getElementById( 'companytab' ).value='company-info'; document.getElementById('btnFirstNext').click();">Save</button>
                                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return savepreview( 'companytab', 'company-info', 'companypreview', 'btnFirstNext' );">Save and Preview</button>
                                                    <button type="button" class="btn btn-rounded btn-warning" data-link="offer" onClick="" id="btnFirstNext">Save and Next <i class="si si-action-redo"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="block-header">
                                        Uploaded Files
                                    </div>
                                    <div class="block-content">

                                        <div class="form-group">
                                            <label>Company Logo</label><br>
                                            <?php $logo = View::common()->getUploadedFiles($client->CompanyLogo); ?>                                            
                                            <a href="<?php echo isset($logo[0]) ? View::url('assets/'.'files'.$logo[0]->FileSlug) : View::url('assets'.'/images/backers/1.png'); ?>" class="html5lightbox" data-thumbnail="<?php echo isset($logo[0]) ? View::url('assets/'.'files'.$logo[0]->FileSlug) : View::url('assets/files'.'/images/backers/1.png'); ?>"><?php echo View::photo((isset($logo[0]) ? 'files'.$logo[0]->FileSlug : '/images/backers/1.png'),"Avatar"); ?></a>
                                        </div>

                                        <div class="form-group">
                                            <label>Company Photo</label><br>
                                            <?php $avatar = View::common()->getUploadedFiles($client->CompanyPhoto); ?>
                                            
                                            <a href="<?php echo isset($avatar[0]) ? View::url('assets/'.'files'.$avatar[0]->FileSlug) : View::url('assets'.'/images/backers/1.png'); ?>" class="html5lightbox" data-thumbnail="<?php echo isset($avatar[0]) ? View::url('assets/'.'files'.$avatar[0]->FileSlug) : View::url('assets/files'.'/images/backers/1.png'); ?>"><?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar"); ?></a>
                                        </div>

                                        <?php if (isset($client->Video) && $client->Video!='') { ?>
                                            <div class="form-group">
                                                <label>Company View</label><br>                                                
                                                    <?php echo $client->Video; ?>                                                
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>

                            

                        </form>
                    </div>
                    <!-- END Company Tab -->

                    <!-- Offer Tab -->
                    <div id="offer" class="form-wizard client-profile">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formOffer">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo isset( $client->ClientProfileID ) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo isset( $client->UserID ) ? $client->UserID : '0'; ?>" />
                            <input type="hidden" name="section" value="offer" />
                            <input type="hidden" name="next" value="documents" id="offertab" />
                            <input type="hidden" name="preview" value="" id="offerpreview" />

                            <div class="block-content push-20"><?php echo $info; ?></div>

                            <div class="block-content push-20">
                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_TYPEOFFER'); ?> <span class="text-danger">*</span></label>
                                    <select id="TypeOfOffer" name="client[TypeOfOffer]" class="form-control<?php echo $client->Disabled; ?>" required="required">
                                        <option value="">Select</option>
                                        <option value="Open" <?php echo $client->TypeOfOffer == 'Open' ? 'selected' : ""; ?>>Open</option>
                                        <option value="Limited" <?php echo $client->TypeOfOffer == 'Limited' ? 'selected' : ""; ?>>Limited</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('CLN_PRF_OFFEROPENING'); ?> <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->OfferOpening ) ? $client->OfferOpening : ''; ?>" id="OfferOpening" name="client[OfferOpening]" class="form-control jsdate<?php echo $client->Disabled; ?>" required="required">
                                    </div>
                                    <div class="form-right">
                                        <label><?php echo Lang::get('CLN_PRF_OFFERCLOSING'); ?> <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->OfferClosing ) ? $client->OfferClosing : ''; ?>" id="OfferClosing" name="client[OfferClosing]" class="form-control jsdate<?php echo $client->Disabled; ?>" required="required">
                                    </div>
                                </div>

                                <?php /*
                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                    <div class="prefix">
                                        <span class="currency text-muted">$</span>
                                        <input type="number" value="<?php echo isset( $client->Price ) ? $client->Price : '1'; ?>" id="Price" name="client[Price]" class="price-input <?php echo $client->Disabled; ?> form-control" min="1" required="required" step="0.01">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('CLN_PRF_SZEOFFER'); ?> <span class="text-danger">*</span></label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="text" value="<?php echo isset( $client->SizeOfOffer ) ? $client->SizeOfOffer : ''; ?>" id="SizeOfOffer" name="client[SizeOfOffer]" class="form-control transparent <?php echo $client->Disabled; ?>" required="required">
                                        </div>
                                    </div>
                                    <div class="form-right">
                                        <label>Target Goal</label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="number" value="<?php echo isset( $client->TargetGoal ) ? $client->TargetGoal : ''; ?>" id="TargetGoal" name="client[TargetGoal]" class="form-control transparent <?php echo $client->Disabled; ?>" placeholder="$" required="required">
                                        </div>
                                    </div>
                                </div>
                                */ ?>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('CLN_PRF_PRICE'); ?></label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="number" value="<?php echo isset( $client->Price ) ? $client->Price : '1'; ?>" id="Price" name="client[Price]" class="price-input <?php echo $client->Disabled; ?> form-control" min="1" required="required" step="0.01">
                                        </div>
                                    </div>
                                    <div class="form-right">
                                        <label>Target Goal</label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="number" value="<?php echo isset( $client->TargetGoal ) ? $client->TargetGoal : ''; ?>" id="TargetGoal" name="client[TargetGoal]" class="form-control transparent <?php echo $client->Disabled; ?>" placeholder="$" required="required">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('CLN_PRF_MINBID'); ?> <span class="text-danger">*</span></label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="number" value="<?php echo isset( $client->MinimumBid ) ? $client->MinimumBid : ''; ?>" id="MinimumBid" name="client[MinimumBid]" class="bid-input<?php echo $client->Disabled; ?> form-control" required="required">
                                        </div>
                                    </div>
                                    <div class="form-right">
                                        <label>Maximum Bid <span class="text-danger">*</span></label>
                                        <div class="prefix">
                                            <span class="currency text-muted">$</span>
                                            <input type="number" value="<?php echo isset( $client->MaximumBid ) ? $client->MaximumBid : ''; ?>" id="MaximumBid" name="client[MaximumBid]" class="bid-input<?php echo $client->Disabled; ?> form-control" required="required">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label><?php echo Lang::get('CLN_PRF_LDMNGR'); ?> <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->LeadManager ) ? $client->LeadManager : ''; ?>" id="LeadManager" name="client[LeadManager]" class="form-control <?php echo $client->Disabled; ?>" required="required">
                                    </div>
                                    <div class="form-right">
                                        <label>Website (optional)</label>
                                        <input type="text" value="<?php echo isset( $client->Website ) ? $client->Website : ''; ?>" id="Website" name="client[Website]" class="form-control <?php echo $client->Disabled; ?>">
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="next-btn <?php echo $approvehide; ?>">
                                    <button type="button" class="btn btn-rounded btn-info back" data-link="company-info" onClick="moveform(this,'company-info')"><i class="si si-action-undo"></i> Back</button>
                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="document.getElementById('offertab').value='offer'; document.getElementById('btnSecondNext').click();">Save</button>
                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return savepreview( 'offertab', 'offer', 'offerpreview', 'btnSecondNext' );">Save and Preview</button>
                                    <button type="button" class="btn btn-rounded btn-warning" data-link="documents" onClick="" id="btnSecondNext">Save and Next <i class="si si-action-redo"></i></button>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Offer Tab -->

                    <!-- Files Tab -->
                    <div id="documents" class="form-wizard ">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formDocuments">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo isset( $client->ClientProfileID ) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo isset( $client->UserID ) ? $client->UserID : '0'; ?>" />
                            <input type="hidden" name="section" value="documents" />
                            <input type="hidden" name="next" value="bank-accounts" id="filetab" />
                            <input type="hidden" name="preview" value="" id="filepreview" />

                            <div class="block-content push-20"><?php echo $info; ?></div>

                            <div class="block-content push-20">
                                <div class="upload-documents">
                                    <div id="file-wrapper" class="file-wrapper">
                                        <?php /*echo AppUtility::getClientFileList( $client, true );*/ ?>
                                        <?php
                                        $ctr = 0;
                                        if( isset( $profiles ) && count( $profiles ) ){
                                            foreach( $profiles as $profile ){
                                                $fgID = isset( $profile->FileGroupID ) ? $profile->FileGroupID : '0';
                                                $upload = $profile->FileID == NULL || strlen( $profile->FileID ) == 0 ? true : false;
                                                echo AppUtility::getCustomFileList( $fgID.'-ClientProfileDocs'.$ctr, $profile, $upload );
                                                $ctr++;
                                            }
                                            if( $profiles[0]->FileID != NULL && $profiles[0]->Disabled == '' ){
                                                echo AppUtility::getCustomFileList( '0-ClientProfileDocs'.$ctr++, false, true );
                                            }
                                        } else {
                                            echo AppUtility::getCustomFileList( '0-ClientProfileDocs'.$ctr++, false, true );
                                        }
                                        ?>
                                        <?php echo '<a href="javascript:void(0);" class="btn btn-4 blue'.$client->Disabled.'" id="addDocForm">Create Upload Form</a>'; ?>
                                        <?php echo '<input type="hidden" id="counter" value="'.$ctr.'" />'; ?>
                                    </div>
                                </div>
                                <br>

                                <div class="ln_solid"></div>
                                <div class="next-btn <?php echo $approvehide; ?>">
                                    <br/><br/>
                                    <button type="button" class="btn btn-rounded btn-info back" data-link="offer" onClick="moveform(this,'offer')"><i class="si si-action-undo"></i> Back</button>
                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return validateForm( 'documents' );">Save</button>

                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return savepreview( 'filetab', 'documents', 'filepreview', 'btnThirdNext' );">Save and Preview</button>

                                    <button type="button" class="btn btn-rounded btn-warning green" data-link="company-info" onClick="return validateForm( 'bank-accounts' );">Save and Next <i class="si si-action-redo"></i></button>

                                    <button type="button" class="btn btn-rounded btn-warning hidden" data-link="bank-accounts" id="btnThirdNext">Save and Next <i class="si si-action-redo"></i></button>
                                    <div class="clear"></div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- END Files Tab -->

                    <!-- Bank Tab -->
                    <div id="bank-accounts" class="form-wizard ">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formBankAccounts">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo (isset($client->ClientProfileID)) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo isset( $client->UserID ) ? $client->UserID : '0'; ?>" />
                            <input type="hidden" name="bankid" value="<?php echo isset( $client->BankAccountID ) ? $client->BankAccountID : '0'; ?>" />
                            <input type="hidden" name="section" value="bankaccounts" />
                            <input type="hidden" name="next" value="summary" id="banktab" />
                            <input type="hidden" name="preview" value="" id="bankpreview" />

                            <div class="block-content push-20"><?php echo $info; ?></div>

                            <div class="block-content push-20">
                                <div class="form-group">
                                    <div class="form-left">
                                        <label>Bank Name <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->Name ) ? $client->Name : ''; ?>" id="Name" name="bank[Name]" class="form-control<?php echo $client->ACCTDisabled; ?>" required="required">
                                    </div>
                                    <div class="form-right">
                                        <label>Swift Code <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->SwiftCode ) ? $client->SwiftCode : ''; ?>" id="SwiftCode" name="bank[SwiftCode]" class="form-control<?php echo $client->ACCTDisabled; ?>" required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Bank Address <span class="text-danger">*</span></label>
                                    <input type="text" value="<?php echo isset( $client->Address ) ? $client->Address : ''; ?>" id="Address" name="bank[Address]" class="form-control<?php echo $client->ACCTDisabled; ?>">
                                </div>

                                <div class="form-group">
                                    <div class="form-left">
                                        <label>Account Name <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->AccountName ) ? $client->AccountName : ''; ?>" id="AccountName" name="bank[AccountName]" class="form-control<?php echo $client->ACCTDisabled; ?>">
                                    </div>
                                    <div class="form-right">
                                        <label>Account Number <span class="text-danger">*</span></label>
                                        <input type="text" value="<?php echo isset( $client->AccountNumber ) ? $client->AccountNumber : ''; ?>" id="AccountNumber" name="bank[AccountNumber]" class="form-control<?php echo $client->ACCTDisabled; ?>">
                                    </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="next-btn <?php echo $approvehide; ?>">
                                    <button type="button" class="btn btn-rounded btn-info back" data-link="documents" onClick="moveform(this,'documents')"><i class="si si-action-undo"></i> Back</button>
                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="document.getElementById('banktab').value='bank-accounts'; document.getElementById('btnFourthNext').click();">Save</button>
                                    <!-- <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return savepreview( 'banktab', 'bankaccounts', 'bankpreview', 'btnFourthNext' );">Save and Preview</button> -->
                                    <button type="button" class="btn btn-rounded btn-warning green" data-link="summary" onClick="" id="btnFourthNext">Save and Next <i class="si si-action-redo"></i></button>
                                    <div class="clear"></div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- END Banks Tab -->

                    <!-- Summary Tab -->
                    <div id="summary" class="form-wizard">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="formSummary">
                            <input type="hidden" name="action" value="updateclientprofile" />
                            <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                            <input type="hidden" name="clientid" value="<?php echo (isset($client->ClientProfileID)) ? $client->ClientProfileID : '0'; ?>" />
                            <input type="hidden" name="userid" value="<?php echo isset( $client->UserID ) ? $client->UserID : '0'; ?>" />
                            <input type="hidden" name="section" value="summary"  />
                            <input type="hidden" name="next" value="teams" id="summarytab" />
                            <input type="hidden" name="preview" value="" id="summarypreview" />

                            <div class="block-content push-20">

                                <div class="form-group">
                                    <?php echo $info; ?>
                                </div>

                                <div class="form-group">
                                    <label>Executive Summary <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[ExecutiveSummary]" id="clientExecutiveSummary"><?php echo (isset($client->ExecutiveSummary)) ? $client->ExecutiveSummary : ''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_OFFVIEW'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[OfferOverview]" id="clientOfferOverview"><?php echo (isset($client->OfferOverview)) ? $client->OfferOverview : ''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_BUSSMODEL'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[BusinessModel]" id="clientBusinessModel"><?php echo (isset($client->BusinessModel))?$client->BusinessModel:''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_KEYINVST'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[KeyInvestHighlights]" id="clientKeyInvestHighlights"><?php echo (isset($client->KeyInvestHighlights))?$client->KeyInvestHighlights:''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_STRATVISION'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[StrategyVision]" id="clientStrategyVision"><?php echo (isset($client->StrategyVision))?$client->StrategyVision:''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_MARKTDEMAND'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[MarketDemand]" id="clientMarketDemand"><?php echo (isset($client->MarketDemand))?$client->MarketDemand:''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_BRDMNGT'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[BoardManagement]" id="clientBoardManagement"><?php echo (isset($client->BoardManagement))?$client->BoardManagement:''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_USEFUNDS'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[UsageOfFunds]" id="clientUsageOfFunds"><?php echo (isset($client->UsageOfFunds))?$client->UsageOfFunds : ''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_FINSUMMRY'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[FinancialSummary]" id="clientFinancialSummary"><?php echo (isset($client->FinancialSummary))? $client->FinancialSummary : ''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_PRESS'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[PressCoverage]" id="clientPressCoverage"><?php echo (isset($client->PressCoverage))?$client->PressCoverage :''; ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label><?php echo Lang::get('CLN_PRF_DISCLOSURE'); ?> <span class="text-danger">*</span></label>
                                    <textarea class="form-control profileMCE<?php echo $client->Disabled; ?>" name="client[Disclosure]" id="clientDisclosure"><?php echo (isset($client->Disclosure))?$client->Disclosure : ''; ?></textarea>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="next-btn <?php echo $approvehide; ?>">
                                    <button type="button" class="btn btn-rounded btn-info back" data-link="bank-accounts" onClick="moveform(this,'bank-accounts')"><i class="si si-action-undo"></i> Back</button>
                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="document.getElementById('summarytab').value='summary'; document.getElementById('btnFifthNext').click();">Save</button>
                                    <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return savepreview( 'summarytab', 'summary', 'summarypreview', 'btnFifthNext' );">Save and Preview</button>
                                    <button type="button" class="btn btn-rounded btn-warning green" data-link="teams" onClick="" id="btnFifthNext">Save and Next <i class="si si-action-redo"></i></button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- END Summary Tab -->

                    <!-- Teams Tab -->
                    <div id="teams" class="form-wizard">

                        <div class="row">
                            <?php $ctr = 0;
                            if( isset( $teams ) && count( $teams ) ){
                            foreach( $teams as $team ){ $ctr++;
                                $bio = isset( $team->Bio ) ? $team->Bio : '';
                                $position = isset( $team->Position ) ? $team->Position : ''; ?>

                                <div class="col-sm-6 col-lg-4">
                                    <div class="block block-themed">
                                        <div class="block-header bg-primary ribbon ribbon-modern ribbon-crystal">
                                            <div class="ribbon-box font-w600 text-uppercase">
                                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Team Members' ) ){ ?>
                                                    <div class="">
                                                        <div class="dropdown more-opt">
                                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-danger push-10-t" data-toggle="dropdown">More Options</a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="javascript:void();" title="Edit Member" data-toggle="modal" data-target="#team-edit-modal" data-locations="<?php echo $team->Data; ?>" onclick="editmember(this)"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                                <li><a href="<?php echo View::url( 'clients/team/trash/'.$team->ClientTeamID.'/profile' ); ?>" title="Delete Member" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to move member <?php echo $team->Name; ?> to trash bin?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="push-10-t">
                                                        <a href="javascript:void();" class="dropdown-toggle btn btn-sm btn-danger btn-rounded" title="Edit Member" data-toggle="modal" data-target="#team-edit-modal" data-locations="<?php echo $team->Data; ?>" onclick="editmember(this)">Edit</a>
                                                    </div>
                                                <?php } ?>
                                            </div>

                                            <h3 class="block-title pointer" data-toggle="modal" data-target="#modal-team-member" data-locations="<?php echo $team->Data; ?>" onclick="biomember(this)"><?php echo isset( $team->Name ) ? $team->Name : ''; ?></h3>
                                        </div>
                                        <div class="block-content block-content-full text-center btn-default">
                                            <div class="pointer" data-toggle="modal" data-target="#modal-team-member" data-locations="<?php echo $team->Data; ?>" onclick="biomember(this)">
                                                <?php echo $team->AvatarLink; ?>
                                            </div>
                                            <div class="text-muted push-15-t"><b><?php echo isset( $team->Position ) ? $team->Position : ''; ?></b></div>
                                        </div>
                                    </div>
                                </div>

                            <?php } } ?>
                        </div>

                        <div class="clear"></div>

                        <div class="push-20">

                            <form class="form-horizontal form-ui no-padding form-label-left input_mask" enctype="multipart/form-data" method="post" id="formTeams">
                                <input type="hidden" name="action" value="updateclientprofile" />
                                <input type="hidden" name="status" value="<?php echo isset( $client->Status ) ? $client->Status : ''; ?>" />
                                <input type="hidden" name="clientid" value="<?php echo isset( $client->ClientProfileID ) ? $client->ClientProfileID : '0'; ?>" />
                                <input type="hidden" name="userid" value="<?php echo isset( $client->UserID ) ? $client->UserID : '0'; ?>" />
                                <input type="hidden" name="section" value="teams" />
                                <input type="hidden" name="next" value="teams" id="teamstab" />
                                <input type="hidden" name="preview" value="" id="teamspreview" />

                                <div class="">
                                    <div class="block block-themed">                                    
                                        <div class="block-header bg-primary">
                                            <h3 class="block-title">Add Team Member</h3>
                                        </div>

                                        <div class="block-content text-left">

                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">                                
                                                        <label class=""><?php echo Lang::get('TEAM_EDIT_PPICTURE'); ?></label>
                                                        <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Photo" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                                        <span>* Allowed file types: jpeg, jpg, png</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>
                                                            <?php echo Lang::get('TEAM_MNG_NAME'); ?> <span class="required">*</span>
                                                        </label>
                                                        <input type="text" value="" id="lname" name="team[Name]" required="required" class="form-control uppercase">
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>
                                                            <?php echo Lang::get('TEAM_MNG_POSN'); ?> <span class="required">*</span>
                                                        </label>
                                                        <input type="text" value="" id="fname" name="team[Position]" required="required" class="form-control uppercase">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="text-divider font-w600 push-40-t text-left text-uppercase"><span><?php echo Lang::get('TEAM_MNG_BIO'); ?></span></div>
                                                <textarea class="form-control tinyMCE" name="team[Bio]"></textarea>
                                            </div>

                                            <div class="ln_solid"></div>

                                            <div class="next-btn">
                                                <button type="button" class="btn btn-rounded btn-info back" data-link="summary" onClick="moveform(this,'summary')"><i class="si si-action-undo"></i> Back</button>
                                                <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="document.getElementById('teamstab').value='teams'; document.getElementById('btnSubmitData').click();">Save</button>
                                                <!-- <button type="button" class="btn btn-rounded btn-primary save" data-link="company-info" onClick="return savepreview( 'teamstab', 'teams', 'teamspreview', 'btnSubmitData' );">Save and Preview</button> -->
                                                <button type="submit" class="btn btn-rounded btn-warning green" id="btnSubmitData">Save and Exit <i class="si si-action-redo"></i></button>
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>
                                
                            </form>  

                        </div>

                    </div>
                    <!-- END Teams Tab -->
                </div>

                <!-- END Main Content -->
                <div class="modal fade" id="modalWizardSuccess" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="block block-themed block-transparent remove-margin-b">
                                <div class="block-header bg-success">
                                    <ul class="block-options">
                                        <li>
                                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Saving Profile...</h3>
                                </div>
                                <div class="block-content">
                                    <p id="alertmessage">.</p>
                                </div>
                            </div>
                            <div class="modal-footer text-center">
                                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Info Modal -->
                <div class="modal fade" id="modal-team-member" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="full-name"></span></h3>
                          </div>
                          <div class="block-content">
                            <div class="block text-center">
                              <div id="avatar"></div>
                              <div id="position" class="text-uppercase push-5-t text-info"></div>
                            </div>
                            <hr><br>
                            <span><b>Bio:</b></span><br>
                            <div id="bio"></div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END Team Info Modal -->

                <!-- Team Edit Modal -->
                <div class="modal" id="team-edit-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" id="">
                            <input type="hidden" name="action" value="updateclientteam" />
                            <input type="hidden" id="client-team-id" name="team[ClientTeamID]" value="" />
                            <input type="hidden" id="client-user-id" name="team[ClientUserID]" value="" />
                            <input type="hidden" id="photo" name="OldPhoto" value="" />
                            <input type="hidden" name="section" value="teams" />
                            <input type="hidden" name="next" value="teams" id="teamstab" />

                            <div class="modal-content">
                                <div class="block block-themed block-transparent remove-margin-b">
                                    <div class="block-header bg-primary-dark">
                                        <ul class="block-options">
                                            <li>
                                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                            </li>
                                        </ul>
                                        <h3 class="block-title">Edit Team Member</h3>
                                    </div>
                                    <div class="block-content form-ui">

                                        <div class="block-content text-left">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12 bottom-align-text" for="fname"></label>
                                                <div class="col-md-7 col-sm-7 col-xs-12">
                                                    <div id="photo-link-edit" class="avatar-container personal"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="text-divider font-w600 push-40-t text-left text-uppercase"><span><?php echo Lang::get('TEAM_ADD_PPICTURE'); ?></span><span class="text-danger">*</span></div>
                                                <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Photo" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                                <span>* Allowed file types: jpeg, jpg, png</span>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-left">
                                                    <div class="text-divider font-w600 push-40-t text-left text-uppercase"><span><?php echo Lang::get('TEAM_MNG_NAME'); ?></span><span class="text-danger">*</span></div>
                                                    <input id="full-name-edit" type="text" value="" name="team[Name]" required="required" class="form-control uppercase col-md-7 col-xs-12">
                                                </div>
                                                <div class="form-right">
                                                    <div class="text-divider font-w600 push-40-t text-left text-uppercase"><span><?php echo Lang::get('TEAM_MNG_POSN'); ?></span><span class="text-danger">*</span></div>
                                                    <input id="position-edit" type="text" value="" name="team[Position]" required="required" class="form-control uppercase col-md-7 col-xs-12">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="text-divider font-w600 push-40-t text-left text-uppercase"><span><?php echo Lang::get('TEAM_MNG_BIO'); ?></span></div>
                                                <textarea id="bio-edit" class="form-control tinyMCE" name="team[Bio]"></textarea>
                                            </div>                                        
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <hr class="push-10">
                                    <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> Close</button>
                                    <button type="submit" href="" class="text-success btn btn-primary btn-rounded"><i class="fa fa-check"></i> Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END Team Edit Modal -->

            </div>
        </article>
    </section>

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        $(".mce-content-body p").attr( "disabled", "<?php echo str_replace( ' ', '', $client->Disabled); ?>" );

        var $preview = "<?php echo $preview; ?>";
        var previewURL = "<?php echo View::url( 'project/preview'.$ClientProfileID ); ?>";
        if( $preview == 'preview' ){
            window.open( previewURL );
        }

        $(".remove-form").removeClass( 'hidden' );
        $( '.remove-form' ).click( function(){
            var cfid = $( this ).attr( 'data-id' );
            removeForm( cfid );
        });
    });

    $( '#addDocForm' ).click( function( e ){
        e.preventDefault();
        var count = $( '#counter' ).val();
        var fileArray = '0-ClientProfileDocs' + count;

        $.ajax({
            type: "GET",
            url: '/clients/createFileForm/'+fileArray,
            dataType: 'json',
            success: function( data ){
                $( '#addDocForm' ).before( data );
                $( '.remove-form' ).removeClass( 'hidden' );
                $( '.remove-form' ).click( function(){
                    var cfid = $( this ).attr( 'data-id' );
                    removeForm( cfid );
                });

                var $input = $('input.file[type=file]');
                if( $input.length ){
                    $input.fileinput({ maxFileSize: 8000 });
                }
            }
        });

        $( '#counter' ).val( ++count );
    });

    function savepreview( nextid, next, previewid, btn ){
        document.getElementById( nextid ).value = next;
        document.getElementById( previewid ).value = 'preview';
        document.getElementById( btn ).click();
        
        return true;
    }

    function removeForm( cfid ){
        var count = $( '#counter' ).val();
        $( '#counter' ).val( --count );
        $( '#'+cfid ).remove();
    }

    function validateForm( next ){
        var proceed = true;
        document.getElementById('filetab').value = next;
        $( ".docs-input" ).each( function( index ){
            var name = $( this ).val().trim(); //get document name value
            var thisname = $( this ).attr( 'name' ); //get the name of this input
            var stripped = thisname.replace( 'DN-', '' ); //process this name for file input reference
            var hide = $("input[id="+thisname+"]").val(); //get hidden input value
            try{
                var file = $("input[name="+stripped+"]").val(); //get file input value
                var split = hide.split( '|' ); //do split to avoid multiple appending of document name, always preserved index 0 as FileGroupID

                $("input[id="+thisname+"]").val( split[0] + '|' + name ); //append document name to hidden input value

                //var h = $("input[id="+thisname+"]").val();
                //alert( file.length );

                if( file.length > 0 && name.length == 0 ){
                    proceed = false;
                    alert( 'Please provide a document name!' );
                    return false;
                }
            } catch( err ){}
        });
        if( proceed ){ document.getElementById('btnThirdNext').click() };

        return true;
    }

    // popout member info
    function biomember( el ){
        var json = $( el ).attr( 'data-locations' );
        var data = jQuery.parseJSON( json );

        $( '#full-name' ).text( data.Name );
        $( '#avatar' ).html( data.Avatar );
        $( '#position' ).html( '<b>'+data.Position+'</b>' );
        $( '#bio' ).html( data.Bio );

        return true;
    }

    function editmember( el ){
        var json = $( el ).attr( 'data-locations' );
        var data = jQuery.parseJSON( json );
        
        $( '#client-team-id' ).val( data.ClientTeamID );
        $( '#client-user-id' ).val( data.ClientUserID );
        $( '#full-name-edit' ).val( data.Name );
        $( '#photo' ).val( data.Photo );
        $( '#photo-link-edit' ).html( data.PhotoLink );
        $( '#position-edit' ).val( data.Position );
        $( '#bio-edit' ).text( data.Bio );
        tinymce.get('bio-edit').setContent( data.Bio );

        return true;
    }

    $( ".company-logo" ).fileinput({
        allowedFileExtensions: [ "jpeg", "jpg", "png" ],
        maxImageWidth: 350,
        maxImageHeight: 350
    });
    $( ".company-photo" ).fileinput({
        allowedFileExtensions: [ "jpeg", "jpg", "png" ],
        maxImageWidth: 1900,
        maxImageHeight: 450
    });

<?php /*
    //setTimeout( function(){ $('textarea,input').introducing(); }, 1000);
*/ ?>
</script>