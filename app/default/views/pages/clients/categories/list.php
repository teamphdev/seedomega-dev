<?php 
View::$title = 'Categories';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">All Categories</h3>
                
            </div>
            <div class="">
                <?php echo View::getMessage(); ?> 
                <table id="role-table" class="table table-divide table-vcenter js-dataTable-full-pagination" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('clients/categories/add'); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Add Category</a>'>
                    <thead>
                        <tr class="headings">
                            <!-- <th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th> -->
                            <th width="200" class="text-center no-wrap">Date Added</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th class="no-sorting text-center" style="max-width:150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        foreach($categories as $cat) { $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer category-id-<?php echo $cat->ClientCatID; ?>">                            
                            <td class="text-center"><?php echo date('Y-m-d H:i', strtotime($cat->CCatDateAdded)); ?></td>
                            <td><?php echo isset($cat->CCatName) ? ucwords($cat->CCatName) : '-'; ?></td>
                            <td><?php echo isset($cat->CCatDescription) ? ucwords($cat->CCatDescription) : '-'; ?></td>
                            <td class="text-center">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                    <div class="">
                                        <div class="dropdown more-opt">
                                            <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="<?php echo View::url( 'clients/categories/edit/'.$cat->ClientCatID ); ?>" title="Edit Category" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                                <li><a href="<?php echo View::url( 'clients/categories/delete/'.$cat->ClientCatID ); ?>" title="Delete Category" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete this cetegory?');"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'clients/categories/edit/'.$cat->ClientCatID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit Category" data-toggle="tooltip">Edit</a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</section>


<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    // $(document).ready(function() {

    // });
</script>