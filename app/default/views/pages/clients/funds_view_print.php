<?php 
  View::$title = 'View Booking';
  View::$bodyclass = '';
  View::header( false, 'header_print' ); 
?>
<section class="gray">
  <article class="container project-single">

    <div class="client-company text-center">
      <h3>SHARE APPLICATION FORM </h3>
      <p>GLOBAL ASSET RESOURCES LTD (ACN 618 792 877) – APPLICATION FORM </p>
      <hr>
    </div>
    <form class="form-ui input_mask" enctype="multipart/form-data" method="post" style="padding: 0;">
    <?php if( $hide != "hide" ){ ?>
    <input type="hidden" name="action" value="updatebooking">
    <?php } ?>
    <input type="hidden" name="bookid" value="<?php echo isset( $bookdata->InvestmentBookingID ) ? $bookdata->InvestmentBookingID : ""; ?>">
    <input type="hidden" name="usrid" value="<?php echo isset( $bookdata->UserID ) ? $bookdata->UserID : ""; ?>">

    <div class="row about-project">
      
      <!-- ************************ Left Side Area ************************ -->
      <div class="col-lg-12">
        
        <div class="pcontent">
          <?php echo View::getMessage(); ?>
             <div id="basic-data" class="form-wizard active">
              <?php 
              if( $bookdata->BookingStatus == "Approved" || $bookdata->BookingStatus == "Verified" ){
                $readonly = 'readonly';
                $disabled = 'disabled';
                $hide = 'hide';
                if( User::role() == "Administrator"){
                  $readonly = '';
                  $disabled = '';
                  $hide = '';
                }
              } else {
                $readonly = 'readonly';
                $disabled = 'disabled';
                $hide = '';
              } ?>
                <?php switch( $bookdata->ReceiptOption ){
                  case 'Cheque': ?>
                    <b>INVESTMENT INFORMATION – cheque details </b> <b class="pull-right text-success">Booking ID: <?php echo isset( $bookdata->InvestmentBookingID ) ? $bookdata->InvestmentBookingID : '-' ?> </b><br><br>

                    <div class="form-group">
                       <div class="form-left">
                          <label for="">Drawer</label>
                          <input type="text" value="<?php echo isset( $bookdata->ChequeDrawer ) ? $bookdata->ChequeDrawer : ""; ?>" class="form-control" name="book[ChequeDrawer]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="form-right">
                        <label for="">Bank</label>
                        <input type="text" value="<?php echo isset( $bookdata->ChequeBank ) ? $bookdata->ChequeBank : ""; ?>" class="form-control" name="book[ChequeBank]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="clear"></div>
                    </div>

                    <div class="form-group">
                       <div class="form-left">
                          <label for="">Branch</label>
                          <input type="text" value="<?php echo isset( $bookdata->ChequeBranch ) ? $bookdata->ChequeBranch : ""; ?>" class="form-control" name="book[ChequeBranch]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="form-right">
                        <label for="">Amount</label>                   
                        <input type="text" value="<?php echo isset( $bookdata->ChequeAmount ) ? $bookdata->ChequeAmount : ""; ?>" class="form-control" name="book[ChequeAmount]" placeholder="" <?php echo $readonly; ?>>
                       </div>
                       <div class="clear"></div>
                    </div>

                  <?php break;
                  case 'TTPhoto': ?>
                    <b>INVESTMENT INFORMATION </b> <b class="pull-right text-success">Booking ID: <?php echo isset( $bookdata->InvestmentBookingID ) ? $bookdata->InvestmentBookingID : '-' ?> </b><br>
                
                    <?php if( $bookdata->TTReceiptPhoto == 0 && User::role() != 'Client' ){ ?>
                      <div class="alert alert-danger push-10-t fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Please Uploaded The TT Receipt Photo!</div>
                    <?php } ?>

                     <?php if( $bookdata->TTReceiptPhoto != 0) : ?>
                        <?php echo AppUtility::getBookedTTReceipt( $bookdata ); ?>
                    <?php endif; ?>

                    <?php if( User::role() != 'Client' ){ ?>
                    <div class="form-group">
                      <label for="">TT Receipt Photo <small class="text-muted">(Transaction Receipt)</small></label>
                      <input type="file" value="" class="file form-control" name="TTReceiptPhoto" placeholder="">
                    </div>
                    <?php } ?>
                    
                  <?php 
                    break;
                } ?>

                <div class="form-group">
                   <div class="form-left">
                    <label for="">Total Shares at <span>$<?php echo isset( $bookdata->Price ) ? number_format( $bookdata->Price, 2 ) : "0.00"; ?></span> per Share</label>
                      <input type="hidden" id="shareprice" value="<?php echo isset( $bookdata->Price ) ? number_format( $bookdata->Price, 2 ) : "0.00"; ?>">
                      <input type="hidden" id="minimumbid" value="<?php echo isset( $bookdata->MinimumBid ) ? $bookdata->MinimumBid : "0"; ?>">
                      <div class="input-group">
                        <input type="text" id="SharesToApply" value="<?php echo isset( $bookdata->SharesToApply ) ? number_format( $bookdata->SharesToApply ) : "0"; ?>" class="form-control" name="book[SharesToApply]" placeholder="0" required="required" <?php echo $readonly; ?>>
                        <span class="input-group-addon">Shares</span>
                      </div>
                   </div>
                   <div class="form-right">
                    <label for="">Total application monies of</label>
                      <div class="input-group">
                        <span class="input-group-addon">USD</span>
                        <input type="text" id="TotalAmountAttached" value="<?php echo isset( $bookdata->TotalAmountAttached ) ? number_format( $bookdata->TotalAmountAttached, 2 ) : "0"; ?>" class="form-control" name="book[TotalAmountAttached]" placeholder="0.00" readonly="readonly">
                      </div>
                   </div>
                   <div class="clear"></div>
                </div>

                 <?php if( $bookdata->BookingStatus =='Approved' ){ ?>

                <hr><br>
                <b>PERSONAL DETAILS</b>
                <div class="form-group">
                  <label for="">Title, Given Name(s) (no initials) & Surname or Company Name</label>
                  <input type="text" value="<?php echo isset( $bookdata->FirstName ) ? $bookdata->FirstName : ""; ?> <?php echo isset( $bookdata->LastName ) ? $bookdata->LastName : ""; ?>" class="form-control" name="book[Name]" placeholder="" <?php echo $readonly; ?>>
                </div>

                <hr><br>
                <b>POSTAL ADDRESS </b>

                <div class="form-group">
                   <div class="form-left">
                    <label for="">Address</label>
                    <input type="text" value="<?php echo isset( $bookdata->Address ) ? $bookdata->Address : ""; ?>" class="form-control" name="book[Address]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Suburb</label>                    
                    <input type="text" value="<?php echo isset( $bookdata->Suburb ) ? $bookdata->Suburb : ""; ?>" class="form-control" name="book[Suburb]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">State</label>
                      <input type="text" value="<?php echo isset( $bookdata->State ) ? $bookdata->State : ""; ?>" class="form-control" name="book[State]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Postcode</label>                   
                    <input type="text" value="<?php echo isset( $bookdata->PostCode ) ? $bookdata->PostCode : ""; ?>" class="form-control" name="book[PostCode]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <hr><br>
                <b>CONTACT DETAILS </b>

                <div class="form-group">
                   <div class="form-left">
                      <label for="">Telephone Number</label>
                      <input type="text" value="<?php echo isset( $bookdata->Phone ) ? $bookdata->Phone : ""; ?>" class="form-control" name="book[Phone]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="form-right">
                    <label for="">Email Address</label>                   
                    <input type="email" value="<?php echo isset( $bookdata->Email ) ? $bookdata->Email : ""; ?>" class="form-control" name="book[Email]" placeholder="" <?php echo $readonly; ?>>
                   </div>
                   <div class="clear"></div>
                </div>

                <hr><br>                

                <div class="row">
                  <p class="text-muted text-left" style="text-align: left;"><b>IMPORTANT NOTICE AND ACKNOWLEDGEMENTS</b> - I/We, by lodging this form, hereby acknowledge that we qualify as sophisticated investors for the purposes of s.708 of the Corporations Act 2001 (Cth), or are otherwise person(s) to whom offers of Shares may be made without a Prospectus under the Corporations Act. I/We understand that there are inherent risks associated with an investment in Global Asset Resources Ltd (‘Company’) and therefore apply for the Shares in the knowledge and acceptance of such risks. I/We acknowledge that I/We have had an opportunity to obtain a copy of the Constitution of the Company and obtain independent professional advice in relation to this investment. I/We acknowledge that the issue of Shares is at the discretion of the Board of the Company. I/We hereby authorise the Directors of the Company to register me/us as the holder(s) of the Shares issued to me/us. I/We acknowledge that the Shares or part thereof may be classified as restricted securities by the ASX and subject to escrow conditions upon the Company undertaking a listing on the ASX. I/We undertake and agree to sign a standard form ASX Restriction Agreement to acknowledge the restrictions imposed on such Shares as determined by the ASX. </p>
                   <div class="clear"></div>
                </div>

                <?php } ?>
             </div>
          
        </div>
      </div>
      
      <!-- ************************ Right Side Area ************************ -->
      <div class="col-lg-12">
      </div>
      <a id="downloadPDF" href="//pdfcrowd.com/url_to_pdf/"></a>
             
    </div>
    </form>
  </article>
</section>

<!-- <?php View::footer(); ?> -->

<script type="text/javascript">
    $( document ).ready( function(){
      //$('#downloadPDF')[0].click();
      var tags = '<input type="button" class="btn btn-danger">back</button';
      $( document ).append( tags );
    });
</script>