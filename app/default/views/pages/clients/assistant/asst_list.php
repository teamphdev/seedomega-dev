<?php 
    View::$title = 'Manage Assistants';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<section class="gray">
    <!-- Page Content -->
    <div class="container animated zoomIn">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="">
                <?php echo View::getMessage();  ?> 
                <table class="table table-divide js-table-checkable js-dataTable-full-pagination dt-responsive table-header-bg table-hover table-vcenter" cellspacing="0" width="100%" addbutton='<a href="<?php echo View::url('clients/assistant/add'); ?>" class="btn btn-rounded text-uppercase btn-success push-10-r" data-toggle="tooltip" title="Add Assistant"><i class="si si-user-follow"></i> Add Assistant</a><a href="<?php echo View::url('clients/assistant/trashbin'); ?>" class="btn btn-rounded text-uppercase btn-warning" data-toggle="tooltip" title="Trash Bin"><i class="si si-trash"></i> Trash Bin</a>'>
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-align"><?php echo Lang::get('USR_MNG_IMG'); ?></th>
                            <th class="sort-this"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_EML'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_GNDR'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_PHNE'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_LVL'); ?></th>
                            <th class="no-sorting text-center" style="text-align:center; width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if( count( $assistants ) ){
                            foreach( $assistants as $user ){ $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td class="text-center">
                                <div class="profile-image">
                                <?php echo $user->Data['AvatarLink']; ?>
                                </div>
                            </td>
                            <td><?php echo $user->Code; ?>-<?php echo $user->UserID; ?></td>
                            <td><?php echo $user->Email; ?></td>
                            <td><?php echo $user->LastName; ?><?php echo ', '.$user->FirstName; ?></td>
                            <td><?php echo $user->Gender; ?></td>
                            <td><?php echo $user->Phone; ?></td>
                            <td><?php echo $user->Name; ?></td>
                            <td class="text-center">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Assistant' ) ){ ?>

                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'clients/assistant/edit/'.$user->UserID ); ?>" title="" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'clients/assistant/trash/'.$user->UserID ); ?>" title="" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to put <?php echo $user->FirstName; ?> <?php echo $user->LastName; ?> to trash bin?' );"><i class="fa fa-trash-o pull-right"></i> Trash</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'clients/assistant/edit/'.$user->UserID ); ?>" class="btn btn-sm btn-default btn-rounded" title="" data-toggle="tooltip">Edit</a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php }
                            } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="8">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>