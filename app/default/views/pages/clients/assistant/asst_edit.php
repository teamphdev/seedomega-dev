<?php 
    View::$title = 'Edit Assistant';
    View::$bodyclass = User::info('Sidebar');
    View::header();
    $name = isset( $asst[0]->FirstName ) ? $asst[0]->FirstName.' ' : '';
    $name .= isset( $asst[0]->LastName ) ? $asst[0]->LastName : '';
    $name = $name == '' ? 'Unknown' : $name;
?>

<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <div class="text-center overflow-hidden">
                <div class="push-30-t push animated fadeInDown">
                    <div class="img-wrap img-avatar img-avatar128 img-avatar-thumb"><?php echo $asst[0]->Data['AvatarLink']; ?></div>
                </div>
                <div class="push-20-t animated fadeInUp">            
                    <h4 class="h3 push-10-t text-white"><?php echo $asst[0]->FirstName; ?> <?php echo $asst[0]->LastName; ?></h4>
                </div>
            </div>

        </div>
    </article>
</section>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/manage">Manage Assistants</a></li>
                <li class="fa fa-angle-right"></li>
                <li><?php echo $name; ?></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <article class="container project-single animated zoomIn">
        <div class="start-project centered">
            <!-- Main Content -->        
            <div class="block items-push">
                <div class="block-header">
                    <?php echo View::$title; ?>
                </div>
                <div class="block-content tab-content">
                    <?php echo View::getMessage(); ?>
                    <div class="">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                            <input type="hidden" name="action" value="edit" />
                            <input type="hidden" name="userid" value="<?php echo $asst[0]->UserID; ?>" />
                            <input type="hidden" name="metaid" value="<?php echo $asst[0]->UserMetaID; ?>" />
                            <input type="hidden" name="avatarid" value="<?php echo $asst[0]->Avatar; ?>" />                            

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_EDIT_LN'); ?> <span class="required">*</span></label>
                                    <input type="text" value="<?php echo $asst[0]->LastName; ?>" id="lname" name="meta[LastName]" required="required" class="form-control uppercase">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_EDIT_FN'); ?> <span class="required">*</span></label>
                                    <input type="text" value="<?php echo $asst[0]->FirstName; ?>" id="fname" name="meta[FirstName]" required="required" class="form-control uppercase">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_EDIT_NN'); ?></label>
                                    <input type="text" value="<?php echo $asst[0]->NickName; ?>" id="nname" name="meta[NickName]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_EDIT_EML'); ?> <span class="required">*</span></label>
                                    <input type="email" id="EmailChecker2" name="user[Email]" class="form-control" value="<?php echo isset($asst[0]->Email) ? $asst[0]->Email : ''; ?>" required="required" rel="<?php echo View::url('ajax/checkemail2/'); ?><?php echo isset($asst[0]->UserID) ? $asst[0]->UserID : ''; ?>/" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_EDIT_PHNE'); ?> <span class="required">*</span></label>
                                    <input type="phone" value="<?php echo $asst[0]->Phone; ?>" id="phone" name="meta[Phone]" required="required" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_EDIT_GNDR'); ?> <span class="required">*</span></label>
                                    <div class="push-10-t">
                                        <label class="css-input css-radio css-radio-lg css-radio-info push-10-r">
                                            <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $asst[0]->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                        </label>
                                        <label class="css-input css-radio css-radio-lg css-radio-info">
                                            <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $asst[0]->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address"><?php echo Lang::get('USR_EDIT_ADDS'); ?></label>
                                <input type="text" value="<?php echo $asst[0]->Address; ?>" id="address" name="meta[Address]" class="form-control">
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_EDIT_CTY'); ?></label>
                                    <input type="text" value="<?php echo $asst[0]->City; ?>" id="city" name="meta[City]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_EDIT_STATES'); ?></label>
                                    <input type="text" value="<?php echo $asst[0]->State; ?>" id="state" name="meta[State]" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_EDIT_CNTRY'); ?></label>
                                    <select id="Country" name="meta[Country]" class="form-control">
                                        <?php foreach(AppUtility::getCountries() as $country) { ?>
                                        <option value="<?php echo $country; ?>" <?php echo $asst[0]->Country == $country ? 'selected' : ''; ?>><?php echo $country; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_EDIT_PCODE'); ?></label>
                                    <input type="text" value="<?php echo isset($asst[0]->PostalCode) ? $asst[0]->PostalCode : ''; ?>" id="postal" name="meta[PostalCode]" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="bottom-align-text"><?php echo Lang::get('USR_EDIT_PPCTURE'); ?></label>
                                <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                <span>* Allowed file types: jpeg, jpg, png</span>
                            </div>

                            <div class="form-group">
                                <label for="phone">
                                <?php echo Lang::get('USR_EDIT_STATUS'); ?> <span class="required">*</span>
                                </label>
                                <div class="">
                                    <p><?php $active = $asst[0]->Active; ?>
                                        <label class="css-input css-radio css-radio-info push-10-r">
                                            <input type="radio" name="user[Active]" value="1" <?php echo $active == '1' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_EDIT_STATUSA'); ?>
                                        </label>
                                        <label class="css-input css-radio css-radio-info">
                                            <input type="radio" name="user[Active]" value="0" <?php echo $active == '0' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_EDIT_STATUSI'); ?>
                                        </label>
                                    </p>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <a href="<?php echo View::url('clients/assistant/asst_list'); ?>" class="btn btn-rounded btn-danger"><i class="si si-action-undo"></i> <?php echo Lang::get('USR_ADD_CANBTN'); ?></a>
                                    <button type="submit" class="btn btn-rounded btn-success"><?php echo Lang::get('USR_EDIT_SVBTN'); ?></button>

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- END Main Content -->
        </div>        
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>