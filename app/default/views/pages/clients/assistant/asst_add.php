<?php 
    View::$title = 'Add Assistant';
    View::$bodyclass = User::info('Sidebar');
    View::header();
?>

<?php /*
<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/manage">Manage Assistants</a></li>
                <li class="fa fa-angle-right"></li>
                <li><?php echo View::$title; ?></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<section class="gray">
    <article class="container project-single animated zoomIn">
        <div class="start-project centered">
            <!-- Main Content -->        
            <div class="block items-push">
                <div class="block-header"><h5 class="text-uppercase"><?php echo View::$title; ?></h5></div>
                <div class="block-content tab-content">
                    <?php echo View::getMessage(); ?>
                    <div class="">
                        <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                            <input type="hidden" name="action" value="add" />

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_ADD_LN'); ?> <span class="required">*</span></label>
                                    <input type="text" value="" id="lname" name="meta[LastName]" required="required" class="form-control uppercase">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_ADD_FN'); ?> <span class="required">*</span></label>
                                    <input type="text" value="" id="fname" name="meta[FirstName]" required="required" class="form-control uppercase">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_ADD_NN'); ?></label>
                                    <input type="text" value="" id="nname" name="meta[NickName]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_ADD_EML'); ?> <span class="required">*</span></label>
                                    <input type="email" id="EmailChecker" name="user[Email]" class="form-control" value="" required="required" rel="<?php echo View::url('ajax/checkemail'); ?>" checkmessage="<?php echo Lang::get('EMAIL_VALIDATION'); ?>" invalidmessage="<?php echo Lang::get('EMAIL_INVALID'); ?>"><span id="emailloading" class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_ADD_PHNE'); ?> <span class="required">*</span></label>
                                    <input type="phone" value="" id="phone" name="meta[Phone]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_ADD_GNDR'); ?> <span class="required">*</span></label>
                                    <div class="push-10-t">
                                        <label class="css-input css-radio css-radio-lg css-radio-info push-10-r">
                                            <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" required checked /><span></span> <?php echo Lang::get('USR_ADD_GNDRM'); ?>
                                        </label>
                                        <label class="css-input css-radio css-radio-lg css-radio-info">
                                            <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" required /><span></span> <?php echo Lang::get('USR_ADD_GNDRF'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address" class=""><?php echo Lang::get('USR_ADD_ADDS'); ?></label>
                                <input type="text" value="" id="address" name="meta[Address]" class="form-control">
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_ADD_CTY'); ?></label>
                                    <input type="text" value="" id="city" name="meta[City]" class="form-control">
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_ADD_STATES'); ?></label>
                                    <input type="text" value="" id="state" name="meta[State]" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('USR_ADD_CNTRY'); ?></label>
                                    <select id="Country" name="meta[Country]" class="form-control">
                                        <?php foreach( AppUtility::getCountries() as $country ){ ?>
                                        <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('USR_ADD_PCODE'); ?></label>
                                    <input type="text" value="" id="postal" name="meta[PostalCode]" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">                                
                                <label class="bottom-align-text" for="fname"><?php echo Lang::get('USR_EDIT_PPCTURE'); ?></label>
                                <div class="">
                                    <input id="file-0a" class="file form-control" type="file" data-min-file-count="0" name="Avatar" data-show-upload="false" data-allowed-file-extensions='["jpeg","png","jpg"]'>
                                    <small>* Allowed file types: jpeg, jpg, png</small>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="text-center">
                                    <p>* <?php echo Lang::get('USR_ADD_PWINF'); ?></p>
                                </div>
                            </div>
                            <div class="ln_solid"></div>                        

                            <div class="form-group">
                                <div class="text-center">
                                    <a href="<?php echo View::url('clients/assistant/asst_list'); ?>" class="btn btn-rounded btn-danger"><?php echo Lang::get('USR_ADD_CANBTN'); ?></a>
                                    <button type="submit" class="btn btn-rounded btn-success"><?php echo Lang::get('USR_ADD_ADDBTN'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>   
            </div>
            <!-- END Main Content -->
        </div>
    </div>
</section>

<!-- /page content -->
<?php View::footer(); ?>