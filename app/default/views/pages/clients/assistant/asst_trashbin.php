<?php 
View::$title = 'Trashed Assistants';
View::$bodyclass = User::info('Sidebar').' dev';
View::header(); 
?>
<?php $userinfo = User::info(); ?>
<!-- page content -->

<?php /*
<section class="breadcrumb">
    <article class="container animated fadeInDown">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo View::url(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo  View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo  View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/manage">Manage Assistants</a></li>
                <li class="fa fa-angle-right"></li>
                <li><?php echo View::$title; ?></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>
*/ ?>

<form method="post" action="">
<input type="hidden" name="doemptytrashbin">

<section class="gray animated zoomIn">
    <!-- Page Content -->
    
    <div class="container">
        <div class="block-header">
            <h3 class="block-title">All <?php echo View::$title; ?><small></small>
                <button type="submit" class="btn btn-sm btn-danger btn-rounded pull-right" onclick="return confirm('Are you sure you want to delete trashed users permanently? If you continue you will be able to undo this action!');"><i class="fa fa-trash"></i> <?php echo Lang::get('USR_TRSH_EMPBTN'); ?></button></h3>
        </div>

        <div class="block">
            <div class="block">
                <?php echo View::getMessage(); ?> 
                <table class="table table-divide js-dataTable-full-pagination dt-responsive table-hover table-vcenter" cellspacing="0" width="100%">
                    <thead>
                        <tr class="headings">
                            <!--th class="no-sorting"><input type="checkbox" id="check-all" class="flat"></th-->
                            <th class="text-align"><?php echo Lang::get('USR_MNG_IMG'); ?></th>
                            <th class="sort-this"><?php echo Lang::get('USR_MNG_UID'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_EML'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_NAME'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_GNDR'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_PHNE'); ?></th>
                            <th><?php echo Lang::get('USR_MNG_LVL'); ?></th>
                            <th class="no-sorting text-center" style="text-align:center; width: 5%;"><?php echo Lang::get('USR_MNG_ACTN'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $cntr = 0;
                        if( isset( $users ) && count( $users ) ){
                        foreach($users as $user) { $cntr++;
                        ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td><div class="profile-image"><?php echo $user->Data['AvatarLink']; ?></div></td>
                            <td><?php echo $user->Code; ?>-<?php echo $user->UserID; ?><input type="hidden" name="uids[]" value="<?php echo $user->UserID; ?>" /></td>
                            <td><?php echo $user->Email; ?></td>
                            <td><?php echo $user->LastName; ?> <?php echo $user->FirstName; ?></td>
                            <td><?php echo $user->Gender; ?></td>
                            <td><?php echo $user->Phone; ?></td>
                            <td><?php echo $user->Name; ?></td>
                            <td style="text-align:center;">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Assistant' ) ){ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'clients/assistant/restore/'.$user->UserID ); ?>" title="Restore Assistant" data-toggle="tooltip"><i class="fa fa-undo pull-right"></i> Restore</a></li>
                                            <li><a href="<?php echo View::url( 'clients/assistant/delete/'.$user->UserID ); ?>" title="Permanently Delete Assistant" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete user <?php echo $user->FirstName; ?> <?php echo $user->LastName; ?>? If you continue you will be able to undo this action!' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="btn-group view-info">
                                        <a href="<?php echo View::url( 'clients/assistant/restore/'.$user->UserID ); ?>" class="btn btn-sm btn-default" title="" data-toggle="tooltip">Restore</a>
                                    </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                        } else {?>
                        <tr>
                            <!--td class="a-center"><input type="checkbox" name="table_records" value="<?php //echo $user->UserID; ?>" class="flat"></td-->
                            <td colspan="9">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

</form>

<!-- /page content -->
<?php View::footer(); ?>