<?php
View::$title = 'Pending Funds Acknowledgement';
View::$bodyclass = 'client-funds';
View::header();
?>
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<?php /* View::template('users/breadcrumb'); */ ?>

<!-- ************************ Page Content ************************ -->

<section class="gray">
    <article class="container projects-page animatedx slideInUp">
        <?php echo View::getMessage(); ?>
        <?php /*
        <!-- Stats -->
        <div class="content bg-white border-b push-20">
            <div class="row items-push text-uppercase">
                <div class="col-xs-12 col-sm-4">
                    <div class="font-w700 text-gray-darker animated fadeIn">Total Funds</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-suitcase"></i> Only Total Approved</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);">$<?php echo number_format($totals->TotalAmountInvestment); ?></a>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="font-w700 text-gray-darker animated fadeIn">Approved</div>
                    <div class="text-muted animated fadeIn"><small><i class="fa fa-check"></i> Total Approved</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo number_format($totals->TotalApproved); ?></a>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="font-w700 text-gray-darker animated fadeIn">Pendings</div>
                    <div class="text-muted animated fadeIn"><small><i class="si si-clock"></i> Total Verified</small></div>
                    <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0);"><?php echo number_format($totals->TotalVerified); ?></a>
                </div>
            </div>
        </div>
        <!-- END Stats -->
        */ ?>
        <div class="block">
            <div class="tab-content">
                
                <!-- Users -->
                <div class="tab-pane fade fade-up in active" id="bookings-pending">
                    <table class="table table-divide table-hover table-header-bg table-vcenter js-dataTable-full-pagination" style="width:100%;">
                        <thead>
                        <tr>
                            <th class="text-center"><i class="fa fa-calendar text-eye"></i> Date</th>
                            <th class="no-wrap"><i class="fa fa-suitcase text-gray"></i> Investor Name</th>
                            <th class="text-center"><i class="fa fa-edit text-gray"></i> Notes</th>
                            <th class="text-right"><i class="fa fa-money text-gray"></i> Investment</th>
                            <th class="text-center no-wrap">Receipt / Cheque</th>
                            <th class="text-center"><i class="fa fa-legal text-gray"></i> Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($bookings)){
                            foreach($bookings as $proj):

                                switch ($proj->BookingStatus) {
                                    case 'Pending':
                                        $statusfield = '<small class="text-danger">Please Uploaded The TT Receipt Photo!</small>';
                                        break;
                                    case 'Verified':
                                        $statusfield = '<small class="text-info">Awaiting fund acknowledgement.</small>';
                                        break;
                                } 

                                $receiptfileslug = View::asset( 'files'.$proj->TTReceiptFileSlug ); //View::url( 'assets/files'.$proj->TTReceiptFileSlug );
                                switch( $proj->ReceiptOption ){
                                    case 'TTPhoto':
                                        $receiptBTN = '<a href="'.$receiptfileslug.'" class="html5lightbox text-muted"><i class="si si-picture fa-2x"></i></a>';
                                        break;
                                    case 'Cheque':
                                        $receiptBTN = '<a href="javascript:void();" class="text-muted" data-toggle="modal" data-target="#cheque-modal" BookingID="'.$proj->InvestmentBookingID.'" ChequeDrawer="'.$proj->ChequeDrawer.'" ChequeBank="'.$proj->ChequeBank.'" ChequeBranch="'.$proj->ChequeBranch.'" ChequeAmount="'.$proj->ChequeAmount.'" onclick="getCheque(this)"><i class="si si-credit-card fa-2x"></i></a>';
                                        break;
                                } ?>
                                    
                                    <tr>
                                        <td class="hidden-xs hidden-sm">
                                            <?php echo date('d M Y', strtotime($proj->CreatedAt)); ?>
                                        </td>
                                        <td>
                                            <h3 class="h5 ">
                                                <a class="" href="/bookings/info/<?php echo $proj->InvestmentBookingID;?>"><?php echo isset($proj->FirstName) ? $proj->FirstName : "-"; ?> <?php echo isset($proj->LastName) ? $proj->LastName : "-"; ?></a>
                                            </h3>
                                            <div class="push-10 visible-xs">
                                                <?php echo $statusfield; ?><br>
                                                <?php echo $receiptBTN; ?>
                                            </div>
                                        </td>
                                        <td class="text-center hidden-xs">
                                            <?php echo $statusfield; ?>
                                        </td>

                                        <td class="h4 text-right text-primary">
                                            <?php echo isset($proj->TotalAmountAttached) ? "$".number_format($proj->TotalAmountAttached,2) : "-"; ?>
                                        </td>
                                        <td class="text-center hidden-xs"><?php echo $receiptBTN; ?></td>
                                        <td class="text-center hidden-xs">
                                            <div class="dropdown more-opt">
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-rounded btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">                                                
                                                    <li> <a href="javascript:void();" thelink="<?php echo View::url('clients/funds/acknowledge/'.$proj->InvestmentBookingID); ?>" data-toggle="modal" data-target="#acknowledge-modal" BookingID="<?php echo $proj->InvestmentBookingID; ?>" commrate="<?php echo isset( $proj->SubscriptionRate ) ? number_format( $proj->SubscriptionRate, 2 ) : "0.00"; ?>" commamount="<?php echo $proj->Data['SOComm']; ?>" exemption="<?php echo isset($proj->ExemptedCommission) ? $proj->ExemptedCommission : ''; ?>" class="" title="" onclick="acknowledge(this)"><i class="si si-check pull-right"></i> Acknowledge</a></li>
                                                    <li><a href="/clients/funds/view/<?php echo $proj->InvestmentBookingID;?>" class="" ><i class="si si-book-open pull-right"></i> View Info</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="6">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Users -->

            </div>
        </div>

    </article>
</section>

<!-- Cheque Modal -->
<div class="modal" id="cheque-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Booked Details</h3>
                </div>
                <div class="block-content form-ui">                    

                    <div id="basic-data" class="form-wizard active">
                       <b>INVESTMENT INFORMATION – cheque details </b> <b class="pull-right text-success">#ID.<span id="cm-InvestmentBookingID"></span> </b><br><br>
                       <div class="form-group">
                          <div class="form-left">
                             <label for="">Drawer</label>
                             <input id="cm-ChequeDrawer" value="" class="form-control" name="book[ChequeDrawer]" placeholder="" readonly="" type="text">
                          </div>
                          <div class="form-right">
                             <label for="">Bank</label>
                             <input id="cm-ChequeBank" value="" class="form-control" name="book[ChequeBank]" placeholder="" readonly="" type="text">
                          </div>
                          <div class="clear"></div>
                       </div>
                       <div class="form-group">
                          <div class="form-left">
                             <label for="">Branch</label>
                             <input id="cm-ChequeBranch" value="" class="form-control" name="book[ChequeBranch]" placeholder="" readonly="" type="text">
                          </div>
                          <div class="form-right">
                             <label for="">Amount</label>                   
                             <input id="cm-ChequeAmount" value="" class="form-control" name="book[ChequeAmount]" placeholder="" readonly="" type="text">
                          </div>
                          <div class="clear"></div>
                       </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal">Close</button>
                <button class="text-success btn-rounded btn btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Cheque Modal -->
<div class="modal" id="acknowledge-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content nobg">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Please Confirm</h3>
                </div>
                <div class="block-content form-ui">
                    <p>Are you sure you already checked and verified the fund transfer?</p>
                    <hr class="hrack push-10">
                    <p id="exemptionwrap">I agree to pay the previously approved subscription rate (<strong id="commRate"><?php echo isset( $proj->SubscriptionRate ) ? number_format( $proj->SubscriptionRate, 2 ) : "0.00"; ?>%</strong>) to SeedOmega amounting <strong>$</strong><strong id="commAmt"><?php echo $proj->Data['SOComm']; ?></strong></p>
                </div>
            </div>
            <div class="modal-footer bg-white">
                <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal">Close</button>
                <a id="acknowledge-btn" href="" class="btn-rounded btn btn-primary"><i class="fa fa-check"></i> Acknowledge</a>
            </div>
        </div>
    </div>
</div>

<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
    });

    function getCheque( fieldVal ){

        var field1 = $( fieldVal ).attr( 'bookingid' );
        var field2 = $( fieldVal ).attr( 'ChequeDrawer' );
        var field3 = $( fieldVal ).attr( 'ChequeBank' );
        var field4 = $( fieldVal ).attr( 'ChequeBranch' );
        var field5 = $( fieldVal ).attr( 'ChequeAmount' );

        $( '#cm-InvestmentBookingID' ).text( field1 );
        $( '#cm-ChequeDrawer' ).val( field2 );
        $( '#cm-ChequeBank' ).val( field3 );
        $( '#cm-ChequeBranch' ).val( field4 );
        $( '#cm-ChequeAmount' ).val( field5 );
    }

    function acknowledge( fieldVal ){
        var thelink = $( fieldVal ).attr( 'thelink' );
        var field2 = $( fieldVal ).attr( 'ChequeDrawer' );
        var field3 = $( fieldVal ).attr( 'ChequeBank' );
        var field4 = $( fieldVal ).attr( 'ChequeBranch' );
        var field5 = $( fieldVal ).attr( 'ChequeAmount' );
        var field6 = $( fieldVal ).attr( 'commrate' );
        var field7 = $( fieldVal ).attr( 'commamount' );
        var field8 = $( fieldVal ).attr( 'exemption' );

        $( '#commRate' ).text( field6+'%' );
        $( '#commAmt' ).text( field7 );

        if( field8 == 1 ){
            $( '.hrack' ).hide();
            $( '#exemptionwrap' ).hide();
        }else{
            $( '.hrack' ).show();
            $( '#exemptionwrap' ).show();
        }

        $( '#acknowledge-btn' ).attr( 'href' , thelink);
    }
</script>