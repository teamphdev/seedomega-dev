<?php 
    View::$title = 'Affiliate Details';
    View::$bodyclass = User::info('Sidebar');
    View::header(); 
    ?>
<?php $userinfo = User::info(); ?>
<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>
<section class="breadcrumb">
    <article class="container">
        <div class="row">
            <div class="col-lg-6">
                <ul>
                    <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
                    <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
                    <li class="fa fa-angle-right"></li>
                    <li><a href="<?php echo View::url(View::$segments[0]); ?>/list"><?php echo View::$segments[0]; ?></a></li>
                    <?php if( isset(View::$segments[1]) ) { ?>
                        <li class="fa fa-angle-right"></li>
                        <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>">Affiliates</a></li>
                    <?php } ?>
                    <?php if( isset(View::$segments[3]) ) { ?>
                        <li class="fa fa-angle-right"></li>
                        <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/view/<?php echo View::$segments[3];?>"><?php echo View::$title; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-6 align-right sub-menu">
               
            </div>
        </div>
    </article>
</section>
<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="block-content">
                <?php echo View::getMessage();  ?>

                <!-- Account Validation -->
                <div class="row items-push">
                    <div class="col-sm-8 col-sm-offset-2">
                        <form class="form-horizontal form-ui form-label-left input_mask" method="post">
                            <input type="hidden" name="action" value="updatecommrate" />
                            <input type="hidden" name="UserID" value="<?php echo $affiliate->UserID; ?>" />
                            <input type="hidden" name="ReferrerUserID" value="<?php echo $affiliate->ReferrerUserID; ?>" />

                            <div class="form-group">
                                <div class="form-left">
                                    <div class="avatar-container">
                                        <?php $avatar = View::common()->getUploadedFiles($affiliate->Avatar); ?>
                                        <?php echo View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar"); ?>
                                    </div>
                                </div>
                                <div class="form-right">

                                </div>
                            </div>



                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_CS_NAME'); ?> <span class="required">*</span></label>
                                    <input type="text" id="" name="Name" class="form-control" value="<?php echo $affiliate->AffiliateName;?>" disabled>

                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('CLN_CS_EML'); ?> <span class="required">*</span></label>
                                    <input type="text" value="<?php echo $affiliate->Email; ?>" id="phone" name="Email" required="required" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('USR_PRF_GNDR'); ?> <span class="required">*</span></label>
                                <div>
                                    <label class="css-input css-radio css-radio-primary push-10-r">
                                        <input type="radio" class="flat" name="meta[Gender]" id="genderM" value="M" <?php echo $affiliate->Gender == 'M' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRM'); ?>
                                    </label>
                                    <label class="css-input css-radio css-radio-primary">
                                        <input type="radio" class="flat" name="meta[Gender]" id="genderF" value="F" <?php echo $affiliate->Gender == 'F' ? 'checked' : ''; ?> required /><span></span> <?php echo Lang::get('USR_PRF_GNDRF'); ?>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-left">
                                    <label>Occupation</label>
                                    <input type="text" value="<?php echo $affiliate->Occupation; ?>" name="meta[Occupation]" class="form-control" disabled>
                                </div>
                                <div class="form-right">
                                    <label>JobTitle</label>
                                    <input type="text" value="<?php echo $affiliate->JobTitle; ?>" name="meta[JobTitle]" class="form-control" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?php echo Lang::get('USR_PRF_RMRKS'); ?></label>
                                <div><?php echo $affiliate->Bio; ?></div>
                            </div>

                            <hr/>

                            <div class="form-group">
                                <div class="form-left">
                                    <label><?php echo Lang::get('CLN_VIEW_STATS'); ?> <span class="required">*</span></label>
                                    <input type="text" id="" name="Status" class="form-control" value="<?php echo $affiliate->Status;?>" disabled>

                                </div>
                                <div class="form-right">
                                    <label><?php echo Lang::get('CLN_CS_RATE'); ?> <span class="required">*</span></label>
                                    <input type="text" value="<?php echo $affiliate->CommissionRate; ?>" id="CommissionRate" name="CommissionRate" required="required" class="form-control">
                                </div>
                            </div>




                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-4 blue green font-18" type="submit"><i class="fa fa-check push-5-r"></i> Save Changes</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
                <!-- END Account Validation -->
            </div>
        </div>
    </div>
</section>
<!-- /page content -->
<?php View::footer(); ?>