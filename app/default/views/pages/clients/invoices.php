<?php
View::$title = $title;
View::$bodyclass = 'client-invoices';
View::header();
?>
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page">
        <?php echo View::getMessage(); ?>
        <!-- Stats -->
        <div class="push-20">
            <div class="row items-push text-uppercase">

                <div class="col-xs-12 col-sm-4 col-md-2 animated zoomIn">
                    <div class="info-box">
                        <div class="info-title">All Invoices</div>
                        <div class="info-value text-primary"><?php echo count( $invoices ); ?></div>
                        <div class="info-subtitle">Total</div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-2 animated zoomIn">
                    <div class="info-box">
                        <div class="info-title">Paid</div>
                        <div class="info-value text-primary"><?php echo count( $invoicesPaid );?></div>
                        <div class="info-subtitle">Total</div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-2 animated zoomIn">
                    <div class="info-box">
                        <div class="info-title">TT Receipt</div>
                        <div class="info-value text-primary"><?php echo count( $invoicesTTReceiptAll );?></div>
                        <div class="info-subtitle">Total</div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2 animated zoomIn">
                    <div class="info-box">
                        <div class="info-title">Subscription</div>
                        <div class="info-value text-primary"><?php echo isset( $invoices[0]->SubscriptionRate ) ? number_format( $invoices[0]->SubscriptionRate, 2 ) : '0.00'; ?>%</div>
                        <div class="info-subtitle">Rate</div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4 animated zoomIn">
                    <div class="info-box">
                        <div class="info-title">My Wallet</div>
                        <?php $wallet = isset( $mybalance ) ? '$'.number_format( $mybalance, 2 ) : '$0.00'; $wallet = str_ireplace( '$-', '-$', $wallet ); ?>
                        <div class="info-value text-primary"><label id="running-balance" class="text-primary"><?php echo $wallet; ?></label></div>
                        <div class="info-subtitle">Running Balance</div>
                    </div>
                </div>
                <!-- <small class="text-muted"><i>computation:</i>&nbsp;&nbsp;<b>INVESTMENT * ( SUBSCRIPTION RATE / 100 )</b></small>
                <label class="label label-danger" id="lblwarning"></label> -->

            </div>
        </div>
        <!-- END Stats -->

        <div class="block animatedx zoomIn">
            <ul class="nav nav-tabs tabs-bg" data-toggle="tabs">
                <li class="active">
                    <a href="#invoice-outstanding" class="tablia">Outstanding <span class="badge"><?php echo count( $invoicesOutstanding ); ?></span></a>
                </li>
                <li class="">
                    <a href="#invoice-overdue" class="tablia">Overdue <span class="badge"><?php echo count( $invoicesOverdue ); ?></span></a>
                </li>
                <li class="">
                    <a href="#invoice-paid" class="tablia">Paid <span class="badge"><?php echo count( $invoicesPaid ); ?></span></a>
                </li>
            </ul>
            <div class="tab-content invoice-wrap">
                <!-- Invoices Outstanding-->
                <div class="tab-pane fade fade-up in active" id="invoice-outstanding">
                    <form id="form-out" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="payout" />
                        <table id="tab-outstanding" class="table table-divide table-hover table-vcenter js-dataTable-full-pagination" style="width: 100%;">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <div class="checkall"><label class="css-input css-checkbox css-checkbox-success"><input class="checkbox pointer" type="checkbox" name="" id="checkallout"><span></span></label></div>
                                </th>
                                <th class="text-center hidden-xs" style="min-width:180px;">Invoice<br><span class="text-warning text-uppercase"><b>(Outstanding)</b></span></th>
                                <th class="text-center hidden-xs">Invoice Type</th>
                                <th class="text-center hidden-xs">Investor</th>
                                <th class="text-center hidden-xs">Investment</th>
                                <th class="text-center hidden-xs">Total</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $cntr = 0;
                            if( count( $invoicesOutstanding ) ){
                                foreach( $invoicesOutstanding as $inv ):
                                    $cntr++;
                                    $startdate = '';
                                    $closedate = '';
                                    $interval = '';
                                    $uploadtt = true;
                                    //$notes = '<span class="label label-danger"><i class="fa fa-times"></i> Please upload the TT Receipt photo!</span>';
                                    $notes = '<span class="label label-danger"><i class="fa fa-times"></i> No TT Receipt photo!</span>';
                                    $uploadhref = 'invoices/uploadtt/' . $inv->InvoiceID;

                                    if( isset ( $inv->TTReceipt ) && $inv->TTReceipt > 0 ){
                                        $uploadtt = false;
                                        $notes = '<span class="label label-warning"><i class="si si-clock"></i> TT Receipt uploaded</span>';
                                        if( isset( $inv->Active ) ){
                                            if( $inv->Active == 1 ){
                                                $notes = '<span class="label label-success"><i class="fa fa-check"></i> TT Receipt uploaded</span>';
                                            }
                                        }
                                    }
                                    //$notes .= '<br><small class="text-muted text-gray">computation: '.number_format( $inv->InvestmentAmount, 2 ).'[invested] * ( '.number_format( $inv->SubscriptionRate, 2 ).'[rate] / 100 ) = '.'$'.number_format( $inv->Total, 2 ).'</small>';
                                    //$notes .= '<br><br><small class="text-muted text-gray">computation: INVESTED * ( RATE / 100 )</small>';

                                    switch( $inv->Status ){
                                        case 'Paid':
                                            $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Paid</span>';
                                            break;
                                        case 'Outstanding':
                                            $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> '.$inv->Status.'</span>';
                                            break;
                                        case 'TT Receipt':
                                            $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> Outstanding</span>';
                                            break;
                                    } ?>
                                    <tr>
                                        <td class="text-center">
                                            <?php if( $uploadtt ){ ?>
                                                <label class="css-input css-checkbox css-checkbox-success"><input class="checkbox pointer checkout" type="checkbox" id="id<?php echo $cntr; ?>" name="iIDs[]" value="<?php echo $inv->InvoiceID; ?>" data-amount="<?php echo $inv->Total; ?>"> <span></span></label>
                                            <?php } else { ?>
                                                <label class="css-input css-checkbox css-checkbox-success" disabled><input type="checkbox" value="" disabled> <span></span></label>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <h3 class="push-10">
                                                <a class=" font-14" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?> </a>                     
                                            </h3>
                                            <div class="push-10"><?php echo $notes; ?></div>
                                            <span class="font-13 text-muted">DUE DATE : <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></span>
                                        </td>
                                        <td class="h5 text-center hidden-xs">
                                            <?php echo isset( $inv->InvoiceType ) ? $inv->InvoiceType : ''; ?>
                                        </td>
                                        <td class="h5 text-center hidden-xs">
                                            <?php echo isset( $inv->Investor ) ? $inv->Investor : ''; ?>
                                        </td>
                                        <td class="h4 text-right hidden-xs">
                                            $<?php echo isset( $inv->InvestmentAmount ) ? number_format( $inv->InvestmentAmount, 2 ) : '0.00'; ?>
                                        </td>
                                        <td class="h4 text-right text-primary" title="INVESTED * ( RATE / 100 )">
                                            <?php echo isset( $inv->Total ) ? "$".number_format( $inv->Total, 2 ) : "-"; ?>
                                        </td>
                                        <td class="h5 text-primary text-center hidden-xs">
                                            <?php if( $uploadtt ){ ?>
                                                <div class="">
                                                    <div class="dropdown more-opt">
                                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default btn-rounded" data-toggle="dropdown">More Options</a>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="<?php echo $uploadhref; ?>" data-toggle="tooltip" title=""><i class="fa fa-upload pull-right"></i> Upload TT Receipt</a></li>
                                                            <li><a href="javascript:void(0);" data-locations="<?php echo isset( $inv->PayData ) ? $inv->PayData: ''; ?>" onclick="singlePayment(this)"><i class="si si-wallet pull-right"></i> Pay</a></li>
                                                            <li><a href="/invoices/view/<?php echo $inv->InvoiceID; ?>"><i class="si si-book-open pull-right"></i> View Details</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="">
                                                    <a href="/invoices/view/<?php echo $inv->InvoiceID; ?>" class="btn btn-sm btn-default btn-rounded"><i class="fa fa-eye"></i> View Details</a>
                                                </div>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="7">No Data</td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- END Invoices Outstanding-->

                <!-- Invoices Overdue -->
                <div class="tab-pane fade fade-up" id="invoice-overdue">
                    <form id="form-due" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="action" value="paydue" />
                        <table id="tab-overdue" class="table table-divide table-hover table-vcenter dt-responsive js-dataTable-full-pagination" style="width:100%;">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <div class="checkall"><label class="css-input css-checkbox css-checkbox-success"><input class="checkbox pointer" type="checkbox" name="" id="checkalldue"><span></span></label></div>
                                </th>
                                <th class="text-center hidden-xs" style="min-width:180px;"><!-- <i class="fa fa-suitcase text-gray"></i> --> Invoice<br><span class="text-danger text-uppercase"><b>(Overdue)</b></span></th>
                                <th class="text-center hidden-xs">Invoice Type</th>
                                <th class="text-center hidden-xs">Investor</th>
                                <th class="text-center hidden-xs">Investment</th>
                                <th class="text-center hidden-xs">Total</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( count( $invoicesOverdue ) ){
                                foreach( $invoicesOverdue as $inv ):
                                    $startdate = '';
                                    $closedate = '';
                                    $interval = '';
                                    $uploadtt = true;
                                    $notes = '<span class="label label-danger"><i class="fa fa-times"></i> No TT Receipt photo!</span>';
                                    $uploadhref = 'invoices/uploadtt/' . $inv->InvoiceID;
                                    
                                    if( isset ( $inv->TTReceipt ) && $inv->TTReceipt > 0 ){
                                        $uploadtt = false;
                                        $notes = '<span class="label label-warning"><i class="si si-clock"></i> TT Receipt uploaded</span>';
                                        if( isset( $inv->Active ) ){
                                            if( $inv->Active == 1 ){
                                                $notes = '<span class="label label-success"><i class="fa fa-check"></i> TT Receipt uploaded</span>';
                                            }
                                        }
                                    }
                                    //$notes .= '<br><br><small class="text-muted text-gray">computation: INVESTED * ( RATE / 100 )</small>';

                                    switch( $inv->Status ){
                                        case 'Paid':
                                            $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Paid</span>';
                                            break;
                                        case 'Outstanding':
                                            $statusfield = '<span class="label label-danger"><i class="si si-clock"></i> Overdue</span>';
                                            break;
                                        case 'TT Receipt':
                                            $statusfield = '<span class="label label-danger"><i class="si si-clock"></i> Overdue</span>';
                                            break;
                                    } ?>
                                    <tr>
                                        <td class="text-center">
                                            <?php if( $uploadtt ){ ?>
                                                <label class="css-input css-checkbox css-checkbox-success"><input class="checkbox pointer checkdue" type="checkbox" id="id<?php echo $cntr; ?>" name="iIDs[]" value="<?php echo $inv->InvoiceID; ?>" data-amount="<?php echo $inv->Total; ?>"><span></span></label>
                                            <?php } else { ?>
                                                <label class="css-input css-checkbox css-checkbox-success" disabled><input type="checkbox" value="" disabled> <span></span></label>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <h3 class="push-10">                                                
                                                <a class=" font-14" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?> </a>
                                            </h3>
                                            <div class="push-10"><?php echo $notes; ?></div>
                                            <span class="font-13 text-muted">DUE DATE : <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></span>

                                            <div class="push-10 visible-xs">
                                                <?php echo $statusfield; ?>
                                            </div>
                                        </td>
                                        <td class="h5 text-center hidden-xs">
                                            <?php echo isset( $inv->InvoiceType ) ? $inv->InvoiceType : ''; ?>
                                        </td>
                                        <td class="h5 text-center hidden-xs">
                                            <?php echo isset( $inv->Investor ) ? $inv->Investor : ''; ?>
                                        </td>
                                        <td class="h4 text-right hidden-xs">
                                            $<?php echo isset( $inv->InvestmentAmount ) ? number_format( $inv->InvestmentAmount, 2 ) : '0.00'; ?>
                                        </td>
                                        <td class="h4 text-right text-primary">
                                            <?php echo isset( $inv->Total ) ? "$".number_format( $inv->Total, 2 ) : "-"; ?>
                                        </td>
                                        <td class="h5 text-primary text-center hidden-xs">
                                            <?php if( $uploadtt ){ ?>
                                                <div class="">
                                                    <div class="dropdown more-opt">
                                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default btn-rounded" data-toggle="dropdown">More Options</a>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="<?php echo $uploadhref; ?>" data-toggle="tooltip" title=""><i class="fa fa-upload pull-right"></i> Upload TT Receipt</a></li>
                                                            <li><a href="javascript:void(0);" data-locations="<?php echo isset( $inv->PayData ) ? $inv->PayData: ''; ?>" onclick="singlePayment(this)"><i class="si si-wallet pull-right"></i> Pay</a></li>
                                                            <li><a href="/invoices/view/<?php echo $inv->InvoiceID; ?>"><i class="si si-book-open pull-right"></i> View Details</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="">
                                                    <a href="/invoices/view/<?php echo $inv->InvoiceID; ?>" class="btn btn-sm btn-default btn-rounded"><i class="fa fa-eye"></i> View Details</a>
                                                </div>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="7">No Data</td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                    </form>
                </div>
                <!-- END Invoices Overdue-->

                <!-- Invoices Paid -->
                <div class="tab-pane fade fade-up" id="invoice-paid">
                    <table class="table table-divide table-hover table-vcenter dt-responsive js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center hidden-xs" style="min-width:180px;">Invoice</th>
                            <th class="text-center hidden-xs">Invoice Type</th>
                            <th class="text-center hidden-xs">Investor</th>
                            <th class="text-center hidden-xs">Investment</th>
                            <th class="text-center hidden-xs">Notes</th>
                            <th class="text-center hidden-xs hidden-sm">Total</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( count( $invoicesPaid ) ){
                            foreach( $invoicesPaid as $inv ):

                                $startdate = '';
                                $closedate = '';
                                $interval = '';
                                $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Paid</span>';
                                $notes = '<span class="label label-danger"><i class="fa fa-times"></i> No TT Receipt photo!</span>';

                                if( isset ( $inv->TTReceipt ) && $inv->TTReceipt > 0 ){
                                    $notes = '<span class="label label-success"><i class="fa fa-check"></i> TT Receipt uploaded.</span>';
                                }
                                //$notes .= '<br><br><small class="text-muted text-gray">computation: INVESTED * ( RATE / 100 )</small>';
                                ?>
                                <tr>
                                    <?php /* 
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?> </a>
                                        </h3>
                                        <?php echo $statusfield; ?>
                                        <div class="push-10 visible-xs">
                                            <?php echo $statusfield; ?>
                                        </div>

                                        <div class="font-s13 hidden-xs">
                                            <div class="project-info row">
                                                <div class="col-xs-4 col-sm-6 col-md-4 text-center" style="min-width:100px;">
                                                    <span><?php echo isset( $inv->InvoiceDate ) ? $inv->InvoiceDate : "-"; ?> <br><small class="text-muted">INVOICE DATE</small></span>
                                                </div>
                                                <div class="col-xs-4 col-sm-6 col-md-4 text-center" style="min-width:100px;">
                                                    <span><?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?> <br><small class="text-muted">DUE DATE</small></span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </td>
                                    */ ?>
                                    <td class="pad-20-l">
                                        <h3 class="push-10">
                                            <a class="font-14" href="/invoices/view/<?php echo $inv->InvoiceID;?>">Invoice #<?php echo isset( $inv->InvoiceID ) ? $inv->InvoiceID: "-"; ?> </a>                   
                                        </h3>
                                        <div class="push-10"><?php echo $statusfield; ?></div>
                                        <span class="font-13 text-muted">DUE DATE : <?php echo isset( $inv->DueDate ) ? $inv->DueDate : "-"; ?></span>
                                    </td>

                                    <td class="h5 text-center hidden-xs">
                                        <?php echo isset( $inv->InvoiceType ) ? $inv->InvoiceType : ''; ?>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo isset( $inv->Investor ) ? $inv->Investor : ''; ?>
                                    </td>
                                    <td class="h4 text-right hidden-xs">
                                        $<?php echo isset( $inv->InvestmentAmount ) ? number_format( $inv->InvestmentAmount, 2 ) : '0.00'; ?>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $notes; ?>
                                    </td>
                                    <td class="h4 text-right text-primary">
                                        <?php echo isset( $inv->Total ) ? "$".number_format( $inv->Total, 2 ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="">
                                            <a href="/invoices/view/<?php echo $inv->InvoiceID;?>" class="btn btn-sm btn-default btn-rounded"><i class="fa fa-eye"></i> View Details</a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="7">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Invoices Paid -->

            </div>
        </div>

        <!-- Single Pay Modal -->
        <div id="dialog" class="w3-modal modal-content">
            <div class="w3-modal-content block block-themed remove-margin-b max600">
                <div id="dialog-form" class="text-center">
                    <button type="button" class="close" aria-label="Close" onclick="document.getElementById('dialog').style.display='none'">
                        <i class="si si-close text-red"></i>
                    </button>
                    <hr>                    
                </div>

                <div id="dialog-form" class="block-content text-center comm-rate">

                    <div class="block block-themed centered max600">
                        <div class="block-content push-10-t">

                            <form class="form-horizontal form-ui form-label-left input_mask" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="action" value="paysingle" />
                                <input type="hidden" name="InvoiceID" value="" id="InvoiceID" />

                                <table class="table remove-margin-b text-muted font-s13 table-pop">
                                    <tr>
                                        <td class="text-left">Invoice ID</td>
                                        <td class="text-right"><label id="invoice-id"></label></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Invoice Type</td>
                                        <td class="text-right"><label id="invoice-type"></label></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Invoice Date</td>
                                        <td class="text-right"><label id="invoice-date"></label></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Due Date</td>
                                        <td class="text-right"><label id="due-date"></label></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Investor</td>
                                        <td class="text-right"><label id="investor"></label></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Investment Amount</td>
                                        <td class="text-right">$ <label id="investment-amount"></label></td>
                                    </tr>
                                </table>

                                <div class="form-group push-20-t">
                                    <div class="col-xs-12 col-lg-6 push-10">
                                        <label class="font-w400 text-muted animated fadeIn text-left"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label>
                                        <input type="text" value="$ <?php echo number_format( $mybalance, 2 ); ?>" class="form-control" readonly="">
                                    </div>
                                    <div class="col-xs-12 col-lg-6 push-10">
                                        <label class="font-w400 text-muted animated fadeIn text-left text-uppercase">AMOUNT DUE</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input id="amt-due" type="text" value="" required="required" class="form-control" placeholder="0.00" readonly="">
                                        </div>
                                    </div>
                                </div>

                                <label class="label label-danger" id="single-pay-warning"></label><br>
                                <button id="close-single-pay" type="button" class="btn btn-rounded btn-danger push-20-t hidden" aria-label="Close" onclick="document.getElementById('dialog').style.display='none'">
                                    <i class="fa fa-close text-red"></i> CLOSE
                                </button>
                                
                                <div class="form-group submit-group">
                                    <div class="col-xs-12 text-center">
                                        <label class="css-input css-checkbox css-checkbox-info">
                                            <input type="checkbox" value="" required="required"> <span></span> I/We Read and verified the above data
                                        </label><br>
                                        <button id="submit-single-pay" class="btn btn-danger blue green" type="submit"><i class="fa fa-check push-5-r"></i> SUBMIT PAYMENT</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
                <div id="dialog-form" class="text-center">
                    <div class="text-reminder text-white"><?php echo View::$title; ?> : Transaction Details</div>
                </div>
            </div>
        </div>
        <!-- END Single Pay Modal -->

        <!-- Multi Pay Modal -->
        <div class="modal" id="multipay-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Please Confirm</h3>
                        </div>
                        <div class="block-content form-ui">
                            <p>Are you sure you want to pay the following invoices?</p>

                            <div id="data-wrapper"></div>

                            <div class="form-group push-20-t">
                                <div class="col-xs-12 col-lg-6 push-10">
                                    <label class="font-w400 text-muted animated fadeIn text-left"><i class="si si-wallet push-5-r"></i> RUNNING BALANCE</label>
                                    <input type="text" value="$ <?php echo number_format( $mybalance, 2 ); ?>" class="form-control" readonly="">
                                </div>
                                <div class="col-xs-12 col-lg-6 push-10">
                                    <label class="font-w400 text-muted animated fadeIn text-left text-uppercase">TOTAL AMOUNT DUE</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input id="amt-due-total" type="text" value="" required="required" class="form-control" placeholder="0.00" readonly="">
                                    </div>
                                </div>
                            </div>

                            <!-- <label class="css-input css-checkbox css-checkbox-info text-center">
                                <input id="cb-confirm" type="checkbox" value="" required="required"> <span></span> I/We read and verified the above data.
                            </label><br> -->

                        </div>
                    </div>
                    <div class="ln-solid push-100-t"></div>
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">CLOSE</button>
                        <a id="submit-multi-pay" href="" class="text-success btn btn-danger btn-sm"><i class="fa fa-check"></i> SUBMIT PAYMENT</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Multi Pay Modal -->

    </article>
</section>

<?php View::footer(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#projlist').easyPaginate({
            paginateElement : 'article',
            elementsPerPage: 12,
            effect: 'fade',
            slideOffset : 100,
            nextButtonText : "Next",
            prevButtonText : "Prev",
            lastButtonText : "Last",
            firstButtonText: "First"
        });

        $( '.tablia' ).click( function(){
            var $balance = <?php echo $mybalance; ?>;
            var $balance = '$' + addCommas( $balance.toFixed( 2 ) );
            $( '#running-balance' ).text( $balance.replace( '$-', '-$' ) );
            $( 'input:checkbox' ).prop( 'checked', false );
        });
    
        var tags = '<div class="form-right inline-block">';
                tags += '<div class="input-group payxx-form">';
                    tags += '<b><input type="text" id="xxTotalAmount" value="$ 0.00" class="form-control text-right disabled"></b>';
                    tags += '<span class="input-group-addon">Total</span>';
                tags += '</div>';
                //tags += '&nbsp;&nbsp;<button type="submit" class="btn btn-danger inline disabled text-uppercase" data-target="#multipay-modal" id="process-payxx"><i class="si si-wallet"></i> Pay</button>';
                tags += '&nbsp;&nbsp;<a href="javascript:void();" data-toggle="modal" data-target="#multipay-modal" class="btn btn-danger btn-rounded inline disabled text-uppercase" title="Pay" onclick="processMultiPay(\'#form-xx\',\'.checkxx\',\'#xxTotalAmount\')" id="process-payxx"><i class="si si-wallet"></i> Pay Now</a>';
            tags += '</div>';

        $( '#tab-outstanding_filter' ).append( tags.replace( /xx/g, 'out') );
        $( '#tab-overdue_filter' ).append( tags.replace( /xx/g, 'due') );

        $( "#checkallout" ).click( function(){
            $( '.checkout' ).not( this ).prop( 'checked', this.checked );
            computeTotalAmount( '.checkout', '#outTotalAmount', '#process-payout' );
        });

        $( "#checkalldue" ).click( function(){
            $( '.checkdue' ).not( this ).prop( 'checked', this.checked );
            computeTotalAmount( '.checkdue', '#dueTotalAmount', '#process-paydue' );
        });

        $( ".checkout" ).on( 'change keyup keypress', function(){
            var balance = $( '#running-balance' ).text().replace( '$', '' ).replace( /,/g, '' );
            var val = $( this ).attr( 'data-amount' );
            var amount = parseFloat( val.replace( /,/g, '' ) );
            if( balance >= 0 ){
                if( $( this ).is(":checked") ){
                    balance = parseFloat( balance ) - amount;
                } else {
                    balance = parseFloat( balance ) + amount;
                }
                balance = '$' + addCommas( balance.toFixed( 2 ) );
                $( '#running-balance' ).text( balance.replace( '$-', '-$' ) );
            } else {
                if( $( this ).is(":checked") ){
                    alert( 'You can not pay anymore if your projected balance is (-) value!' );
                }
                $( this ).prop( 'checked', false );                
            }
            computeTotalAmount( '.checkout', '#outTotalAmount', '#process-payout' );
        });

        $( ".checkdue" ).on( 'change keyup keypress', function(){
            var balance = $( '#running-balance' ).text().replace( '$', '' ).replace( /,/g, '' );
            var val = $( this ).attr( 'data-amount' );
            var amount = parseFloat( val.replace( /,/g, '' ) );
            if( balance >= 0 ){
                if( $( this ).is(":checked") ){
                    balance = parseFloat( balance ) - amount;
                } else {
                    balance = parseFloat( balance ) + amount;
                }
                balance = '$' + addCommas( balance.toFixed( 2 ) );
                $( '#running-balance' ).text( balance.replace( '$-', '-$' ) );
            } else {
                if( $( this ).is(":checked") ){
                    alert( 'You can not pay anymore if your projected balance is (-) value!' );
                }
                $( this ).prop( 'checked', false );                
            }
            computeTotalAmount( '.checkdue', '#dueTotalAmount', '#process-paydue' );
        });

        processCheckable( '.checkout', '#lcout', '#checkallout' );
        processCheckable( '.checkdue', '#lcdue', '#checkalldue' );
    });

    function processCheckable( cb, lcID, checkallID ){
        var balance = <?php echo $mybalance; ?>;
        if( computeTotalPayable( cb ) > parseFloat( balance ) ){            
            $( lcID ).addClass( "disabled" );
            $( lcID ).attr( "disabled", "disabled" );
            $( checkallID ).addClass( "disabled" );
            $( checkallID ).attr( "disabled", "disabled" );
            $( "#lblwarning" ).text( 'Your total payable exceeds your running balance. Checkall is now disabled!' );
        } else {
            $( lcID ).removeClass( "disabled" );
            $( lcID ).removeAttr( "disabled" );
            $( checkallID ).removeClass( "disabled" );
            $( checkallID ).removeAttr( "disabled" );
            $( "#lblwarning" ).text( '' );
        }
    }
    function computeTotalPayable( cb ){
        var amount = 0;
        $( cb ).each( function(){
            var val = $( this ).attr( 'data-amount' );
            amount += parseFloat( val.replace( ',', '' ) );
        });

        return parseFloat( amount );
    }

    function computeTotalAmount( chkClass, amountID, buttonID ){
        var amount = 0;
        var $balance = <?php echo $mybalance; ?>;
        $( amountID ).val( '$ 0.00' );
        $( chkClass ).each( function(){
            if( $( this ).prop( 'checked' ) ){
                var val = $( this ).attr( 'data-amount' );
                amount += parseFloat( val.replace( /,/g, '' ) );
            }
        });
        var runningbalance = $balance - amount;

        amount = addCommas( amount.toFixed( 2 ) );
        $( amountID ).val( '$ ' + amount );

        runningbalance = '$' + addCommas( runningbalance.toFixed( 2 ) );
        $( '#running-balance' ).text( runningbalance.replace( '$-', '-$' ) );

        if( amount == 0 ){
            $( buttonID ).addClass( "disabled" );
        } else {
            $( buttonID ).removeClass( "disabled" );
        }
    }

    function processMultiPay( form, cbClass, amountID ){
        var total = $( amountID ).val().replace( '$', '');
        $( '#amt-due-total' ).val( total );

        var multipaypopdata = '<table class="table remove-margin-b text-muted font-s13 table-pop text-center">';
        multipaypopdata += '<tr><td><b>Invoice ID</b></td><td><b>Amount Due</b></td></tr>';
        $( cbClass ).each( function(){
            if( $( this ).prop( 'checked' ) ){
                var id = $( this ).val();
                var val = $( this ).attr( 'data-amount' );
                amount = parseFloat( val.replace( /,/g, '' ) );
                multipaypopdata += '<tr><td>'+id+'</td><td>$'+amount.toFixed( 2 )+'</td></tr>';
            }
        });
        multipaypopdata += '</table>';
        $( '#data-wrapper' ).html( multipaypopdata );

        $( '#submit-multi-pay' ).click( function( e ){
            $( form ).submit();
            e.preventDefault();
        });
    }

    function addCommas( nStr )
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function singlePayment( el ){
        var json = $( el ).attr( 'data-locations' );
        var data = jQuery.parseJSON( json );
        var $mybalance = <?php echo $mybalance; ?>;
        
        $( '#InvoiceID' ).val( data.InvoiceID );
        $( '#invoice-id' ).text( data.InvoiceID );
        $( '#invoice-type' ).text( data.InvoiceType );
        $( '#invoice-date' ).text( data.InvoiceDate );
        $( '#due-date' ).text( data.DueDate );
        $( '#investor' ).text( data.Investor );
        $( '#investment-amount' ).text( data.InvestmentAmount );
        $( '#amt-due' ).val( data.Total );

        if( parseFloat( $mybalance ) < 0 ){
            $( '.submit-group' ).css( 'display', 'none' );
            $( '#close-single-pay' ).removeClass( 'hidden' );
            $( '#submit-single-pay' ).attr( 'disabled', 'disabled' );
            $( '#single-pay-warning' ).text( 'Your running balance is below $0.00!' );
        }

        $( '#dialog' ).css( 'display', 'block' );
    }
</script>