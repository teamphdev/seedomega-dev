<?php 
View::$title = 'Add Client';
View::$bodyclass = '';
View::header(); 
?>
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="index.html">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
            <?php if( isset(View::$segments[1]) ) { ?>
              <li class="fa fa-angle-right"></li>
              <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>"><?php echo View::$title; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
    <article class="container">
      
      <!-- ************************ Form Area Start ************************ -->
      <div class="start-project">
        <div class="title">
          <ul>
            <li data-link="basic-data" class="current"><a href="#"><i class="fa fa-pagelines"></i><span>Baisc Information</span></a></li>
            <li data-link="funding" class=""><a href="#"><i class="fa fa-bar-chart"></i><span>Funding Details</span></a></li>
            <li data-link="documents" class=""><a href="#"><i class="fa fa-folder-open"></i><span>Documents</span></a></li>
            <li data-link="overview" class=""><a href="#"><i class="fa fa-eye"></i><span>Overview</span></a></li>
          </ul>
        </div>
        <hr>
        <div class="start-content">
          <form method="post" enctype="multipart/form-data">
            
            <div id="basic-data" class="form-wizard active">
                        
              <div class="form-group">
                <input type="text" value="" class="form-control" placeholder="Company Name" name="">
              </div>

              <div class="form-group">
                <input type="text" value="" class="form-control" placeholder="https://" name="">
                <small class="text-muted">Video (Upload embed link; youtube or wistia, with a description line below)</small>              

              <div class="form-group">
                <textarea name="" id="" cols="30" rows="10" class="form-control" placeholder="Description"></textarea>
              </div>
              
              <div class="next-btn">
                <button type="button" class="btn btn-4 blue" data-link="funding" onClick="moveform(this,'funding')">Next</button>
              </div>              
            </div>
            
            <div id="funding" class="form-wizard">

              2
                            
              <div class="next-btn">
                <button type="button" class="btn btn-5 blue" data-link="basic-data" onClick="moveform(this,'basic-data')">Back</button>
                <button type="button" class="btn btn-4 blue" data-link="documents" onClick="moveform(this,'documents')">Next</button>
              </div>
            </div>
            
            <div id="documents" class="form-wizard">

              3
              
              <div class="next-btn">
                <button type="button" class="btn btn-5 blue" data-link="funding" onClick="moveform(this,'funding')">Back</button>
                <button type="button" class="btn btn-4 blue" data-link="overview" onClick="moveform(this,'overview')">Next</button>
                <div class="clear"></div>
              </div>
            </div>
            
            <div id="overview" class="form-wizard">
              4
              
              <div class="next-btn">
                <button type="button" class="btn btn-5 blue" data-link="documents" onClick="moveform(this,'documents')">Back</button>
                <button type="submit" class="btn btn-4 blue">Submit</button>
              </div>
            </div>
            
          </form>
        </div>
      </div>      
      <!-- ************************ Form Area End ************************ -->
      
    </article>
</section>

<?php View::footer(); ?>