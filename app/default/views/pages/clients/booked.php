<?php
View::$title = $title;
View::$bodyclass = 'client-booked';
View::header();
?>
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>

<?php /* View::template('users/breadcrumb'); */ ?>

<!-- ************************ Page Content ************************ -->
<section class="gray">
    <article class="container projects-page animated fadeIn">
        <?php echo View::getMessage(); ?>
        <!-- Stats -->

        <div class="info-box text-left">
            <div class="info-title">Total Funds</div>
            <div class="info-value text-primary">$<?php echo isset($bookingtotals->TotalAmountAttached) ? number_format($bookingtotals->TotalAmountAttached) : '0'; ?></div>
            <div class="info-subtitle">Total</div>
        </div>
        <!-- END Stats -->

        <div class="block ">
<!--             <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active">
                    <a href="#bookings-pending">Pending <span class="badge badge-warning"><?php echo count( $bookingsPending ); ?></span></a>
                </li>
                <li class="">
                    <a href="#bookings-verified">Verified <span class="badge badge-info"><?php echo count($bookingsVerified); ?></span></a>
                </li>
                <li class="">
                    <a href="#bookings-approved">Approved <span class="badge badge-success"><?php echo count( $bookingsApproved ); ?></span></a>
                </li>
            </ul> -->
            <div class="tab-content">

                <?php /*
                <!-- Projects -->
                <!-- <div class="tab-pane fade fade-up" id="bookings-pending">
                    <table class="table table-hover table-vcenter  js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th><i class="fa fa-suitcase text-gray"></i> Investor</th>
                            <th><i class="fa fa-cash text-gray"></i> Invested</th>
                            <th><i class="fa fa-cash text-gray"></i> Shares</th>
                            <th class="text-center hidden-xs" style="width: 40%;"><i class="fa fa-ticket text-gray"></i> Status</th>
                            <th class="text-center" style="width: 15%; min-width: 110px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( isset( $bookingsPending ) && count( $bookingsPending ) ){
                            foreach( $bookingsPending as $book ):
                                switch( $book->ReceiptOption ){
                                    case 'TTPhoto':
                                        if( $book->TTReceiptPhoto != 0 && $book->TTReceiptPhoto != '' ){
                                            $statusfield = '<small class="text-info">TT Receipt Photo : Awaiting for verification!</small>';
                                        } else {
                                            $statusfield = '<small class="text-danger">No TT Receipt Photo!</small>';
                                        } break;
                                    case 'Cheque':
                                        if( $book->ChequeDrawer != '' && $book->ChequeBank != '' && $book->ChequeAmount != '' ){
                                            $statusfield = "<small class='text-info'>Cheque Details : Awaiting for verification!</small>";
                                        } else {
                                            $statusfield = "<small class='text-danger'>Incomplete Checks Details!</small>";
                                        } break;
                                }

                               ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/clients/funds/view/<?php echo $book->InvestmentBookingID;?>"><?php echo isset( $book->FirstName ) ? $book->FirstName: ""; ?> <?php echo isset( $book->LastName ) ? $book->LastName: ""; ?> </a>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <?php echo $statusfield; ?>
                                        </div>
                                    </td>
                                    <td class="text-right">$<?php echo isset( $book->TotalAmountAttached) ? number_format( $book->TotalAmountAttached, 2 ) : "-"; ?></td>
                                    <td class="text-right"><?php echo isset( $book->SharesToApply ) ? number_format( $book->SharesToApply ) : "-"; ?></td>
                                    <td class="text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="btn-group">
                                            <a href="/clients/funds/view/<?php echo $book->InvestmentBookingID;?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="View Details"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td>No Data</td>
                                <td class="h5 text-center hidden-xs"></td>
                                <td class="h4 text-center text-primary"></td>
                                <td class="h5 text-primary text-center hidden-xs"></td>
                                <td class="h5 text-primary text-center hidden-xs"></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div> -->
                <!-- END Pending -->
                */ ?>

                <?php /* 
                <!-- Verified bookings -->
                <div class="tab-pane fade fade-up" id="bookings-verified">
                    <table class="table table-hover table-vcenter  js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th><i class="fa fa-suitcase text-gray"></i> Investor</th>
                            <th><i class="fa fa-cash text-gray"></i> Invested</th>
                            <th><i class="fa fa-cash text-gray"></i> Shares</th>
                            <th class="text-center hidden-xs" style="width: 40%;"><i class="fa fa-ticket text-gray"></i> Status</th>
                            <th class="text-center" style="width: 15%; min-width: 110px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(count($bookingsVerified)){
                            foreach($bookingsVerified as $book):
                                $statusfield = '<span class="label label-info"><i class="fa fa-check"></i> Verified</span>';
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/clients/funds/view/<?php echo $book->InvestmentBookingID;?>"><?php echo isset($book->FirstName) ? $book->FirstName: ""; ?> <?php echo isset($book->LastName) ? $book->LastName: ""; ?> </a>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <?php echo $statusfield; ?>
                                        </div>
                                    </td>
                                    <td class="text-right">$<?php echo isset($book->TotalAmountAttached) ? number_format($book->TotalAmountAttached,2) : "-"; ?></td>
                                    <td class="text-right"><?php echo isset($book->SharesToApply) ? $book->SharesToApply : "-"; ?></td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="btn-group">
                                            <a href="/clients/funds/view/<?php echo $book->InvestmentBookingID;?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="View Details"><i class="fa fa-eye"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td>No Data</td>
                                <td class="h5 text-center hidden-xs"></td>
                                <td class="h4 text-center text-primary"></td>
                                <td class="h5 text-primary text-center hidden-xs"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Verified Bookings -->
                */ ?>

                <!-- Approved bookings -->
                <div class="tab-pane fade fade-up active in" id="bookings-approved">
                    <div class="table-responsive">
                        <table class="table table-divide table-hover table-vcenter table-header-bg js-dataTable-full-pagination" style="width:100%; min-width: 960px;">
                            <thead>
                            <tr>
                                <th class="text-center hidden-xs"><i class="fa fa-suitcase text-gray"></i> Seeder</th>
                                <th class="text-center hidden-xs" style="width:15%;><i class="fa fa-cash text-gray"></i> Amount</th>
                                <th class="text-center hidden-xs" style="width:12%;><i class="fa fa-cash text-gray"></i> Shares</th>
                                <th class="text-center hidden-xs" style="width:20%;><i class="fa fa-ticket text-gray"></i> Status</th>
                                <th class="text-center hidden-xs" style="width:12%;><i class="fa fa-calendar text-gray"></i> Approved Date</th>
                                <th class="text-center hidden-xs"><i class="fa fa-file-image-o text-gray"></i> TT Receipt</th>
                                <th class="text-center hidden-xs" style="min-width:110px;">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if( isset( $bookings ) && count( $bookings ) ){
                                foreach( $bookings as $book ):
                                    switch ($book->BookingStatus) {
                                        case 'Pending':
                                            $statusfield = '<span class="label label-warning"><i class="si si-clock"></i> Pending!</span>';
                                            break;
                                        case 'Approved':
                                            $statusfield = '<span class="label label-success"><i class="fa fa-check"></i> Approved!</span>';
                                            break;
                                        case 'Verified':
                                            $statusfield = '<span class="label label-info"><i class="fa fa-check"></i> Verified!</span>';
                                            break;
                                        
                                        default: break;
                                    } ?>
                                    <tr>
                                        <td>
                                            <h3 class="h5">
                                                <a class="showDetails" href="<?php View::url('clients/seederinfo/'.$book->UserID,true)?>"><?php echo isset( $book->FirstName ) ? $book->FirstName: ""; ?> <?php echo isset( $book->LastName ) ? $book->LastName: ""; ?> </a>
                                            </h3>
                                            <div class="push-10 visible-xs">
                                                <?php echo $statusfield; ?>
                                            </div>
                                        </td>
                                        <td class="h4 text-right">$<?php echo isset( $book->TotalAmountAttached ) ? number_format( $book->TotalAmountAttached, 2 ) : "-"; ?></td>
                                        <td class="h4 text-right"><?php echo isset( $book->SharesToApply ) ? number_format( $book->SharesToApply ) : "-"; ?></td>
                                        <td class="h5 text-center hidden-xs">
                                            <?php echo $statusfield; ?>
                                        </td>
                                        <td class="h5 text-center">
                                            <?php echo isset( $book->AcknowledgeDate ) && $book->AcknowledgeDate != "0000-00-00 00:00:00" ? date( 'j M Y', strtotime( $book->AcknowledgeDate ) ) : '-'; ?>
                                        </td>
                                        <td class="text-center"><?php echo isset( $book->ReceiptBTN ) ? $book->ReceiptBTN : ''; ?></td>
                                        <td class="h5 text-primary text-center">
                                            <div class="dropdown more-opt">
                                                <!-- <a href="/clients/funds/view/<?php echo $book->InvestmentBookingID;?>" class="btn btn-sm btn-default" type="button">View Info</i></a> -->
                                                <a href="javascript:void(0);" class="dropdown-toggle btn btn-rounded btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                                <ul class="dropdown-menu dropdown-menu-right">                                                
                                                    <li><a href="/clients/funds/view/<?php echo $book->InvestmentBookingID;?>"><i class="si si-book-open pull-right"></i> Booking Info</a></li>
                                                    <li><a href="<?php View::url('clients/seederinfo/'.$book->UserID,true)?>" class="showDetails"><i class="si si-user pull-right"></i> Seeder Info</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else{ ?>
                                <tr>
                                    <td colspan="7">No Data</td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                    <td class="hidden"></td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END Approved Bookings -->

                <?php /*
                <!-- Interest -->
                <div class="tab-pane fade fade-up" id="interests">
                    <table class="table table-hover table-vcenter  js-table-checkable js-dataTable-full-pagination" style="width: 100%;">
                        <thead>
                        <tr>
                            <th class="text-center hidden-xs"><i class="fa fa-suitcase text-gray"></i> Investor</th>
                            <th class="text-center hidden-xs" style="width:15%;><i class="fa fa-cash text-gray"></i> Invested</th>
                            <th class="text-center hidden-xs" style="width:12%;><i class="fa fa-cash text-gray"></i> Shares</th>
                            <th class="text-center hidden-xs" style="width:20%;><i class="fa fa-ticket text-gray"></i> Status</th>
                            <th class="text-center hidden-xs" style="width:12%;><i class="fa fa-calendar text-gray"></i> Registration Date</th>
                            <th class="text-center hidden-xs" style="min-width:110px;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if( isset( $interests ) && count( $interests ) ){
                            foreach( $interests as $interest ):
                                $statusfield = '<span class="label label-warning"><i class="fa fa-check"></i> Pending!</span>';
                                ?>
                                <tr>
                                    <td>
                                        <h3 class="h5 font-w600 push-10">
                                            <a class="link-effect" href="/clients/funds/view/<?php echo $interest->InvestmentBookingID;?>"><?php echo isset( $interest->FirstName ) ? $interest->FirstName: ""; ?> <?php echo isset( $interest->LastName ) ? $interest->LastName: ""; ?> </a>
                                        </h3>
                                        <div class="push-10 visible-xs">
                                            <?php echo $statusfield; ?>
                                        </div>
                                    </td>
                                    <td class="text-right">$<?php echo isset( $interest->TotalAmountAttached ) ? number_format( $interest->TotalAmountAttached, 2 ) : "-"; ?></td>
                                    <td class="text-right"><?php echo isset( $interest->SharesToApply ) ? number_format( $interest->SharesToApply ) : "-"; ?></td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $statusfield; ?>
                                    </td>
                                    <td class="h5 text-center hidden-xs">
                                        <?php echo $book->CreatedAt != "0000-00-00" ? date( 'j M Y', strtotime( $book->CreatedAt ) ) : "-"; ?>
                                    </td>
                                    <td class="h5 text-primary text-center hidden-xs">
                                        <div class="btn-group">
                                            <a href="/clients/funds/view/<?php echo $interest->InvestmentBookingID;?>" class="btn btn-sm btn-default" data-toggle="tooltip" title="View Details"><i class="si si-eye fa-1x"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php }else{ ?>
                            <tr>
                                <td colspan="6">No Data</td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                                <td class="hidden"></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
                <!-- END Interest -->
                */ ?>

            </div>
        </div>
    </article>
</section>

<div class="modal" id="modal-teamMember" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Seeder Info</h3>
                </div>
                <div class="block-content"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-rounded" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
            </div>
        </div>
    </div>
</div>

<?php View::footer(); ?>