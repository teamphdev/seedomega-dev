<?php 
View::$title = 'Edit Faq';
View::$bodyclass = User::info('Sidebar');
View::header(); 
?>
<?php $userinfo = User::info();  ?>

<?php View::page('clients/faq/head'); ?>

<section class="gray">
    <article class="container">
        <form class="form-horizontal form-label-left input_mask" enctype="multipart/form-data" method="post">
        <input type="hidden" name="action" value="update">
        <input type="hidden" name="UserID" value="<?php echo $userinfo->UserID; ?>">
            <input type="hidden" name="FaqID" value="<?php echo $faqPost->FaqID; ?>">
            <input type="hidden" name="ClientProfileID" value="<?php echo $client->ClientProfileID; ?>">

            <div class="block-header">Edit Faq</div>
            <div class="block-content">
                
                <div class="row">
                    <?php echo View::getMessage();  ?>
                    <div class="col-sm-9">
                        <div class="push-30-r">
                            <div class="form-group">
                                <label>Question</label>
                                <input type="text" value="<?php echo $faqPost->FaqTitle;?>" name="FaqTitle" class="form-control col-xs-12" required>
                            </div>

                            <div class="form-group">
                                <label for="BlogContent">Answer</label>
                                <textarea class="form-control hiddenMCE" name="FaqContent" cols="30" rows="10"><?php echo $faqPost->FaqContent;?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Status</label>
                            <?php
                                $statusOptions = array('Published','Pending','Draft');
                                View::form('select',array('name'=>'FaqStatus','options'=>$statusOptions,'value'=>$faqPost->FaqStatus,'class'=>'form-control'));
                            ?>
                        </div>

                        <div class="form-group">
                            <label>Date Published</label>                                 
                            <input type="text" class="form-control" value="<?php echo date('d M Y',strtotime($faqPost->FaqDate)); ?> @ <?php echo date('h:i:a',strtotime($faqPost->FaqDate)); ?>" readonly="">
                        </div>                    

                        <div class="form-group">
                            <label>Public View?</label>
                            <?php
                                $publicViewOptions = array(1=>'Yes',0=>'No');
                                View::form('selecta',array('name'=>'PublicView','options'=>$publicViewOptions,'value'=>$faqPost->PublicView,'class'=>'form-control'));
                            ?>
                        </div>

                        <div class="form-group">                          
                            <button type="submit" class="btn btn-rounded btn-primary blog-btn" style="min-width: 120px;">Save Changes</button>
                        </div>
                        
                    </div>
                </div>
                
            </div>            

        </form>
    </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    $( document ).ready( function(){
        tinymce.init( hiddenMCEconfig );
    });
</script>