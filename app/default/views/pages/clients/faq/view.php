<?php 
$userinfo = User::info();

View::$title = 'FAQ';
View::$bodyclass = User::info('Sidebar');
View::header(); 


?>
<!-- page content -->
<section class="header-bottom">
    <article>
      <div class="container"><h1><?php echo View::$title; ?></h1></div>
    </article>
</section>

<section class="breadcrumb">
    <article class="container">
      <div class="row">
        <div class="col-lg-6">
          <ul>
            <li><span class="fa fa-home"></span>&nbsp; You are here:</li>
            <li><a href="<?php echo User::dashboardLink(); ?>">Home</a></li>
            <li class="fa fa-angle-right"></li>
            <li><a href="<?php echo View::url(View::$segments[0]); ?>"><?php echo View::$segments[0]; ?></a></li>
              <?php if( isset(View::$segments[1]) ) { ?>
                <li class="fa fa-angle-right"></li>
                <li><a href="<?php echo View::url(View::$segments[0]).'/'.View::$segments[1]; ?>/<?php echo $blogdata->BlogID;?>"><?php echo $client->CompanyName;?></a></li>
              <?php } ?>
          </ul>
        </div>
      </div>
    </article>
</section>

<section class="gray">
   <article class="container projects-page" id="popular">
      <div class="row">
         <!-- ************************ Left Side Area ************************ -->
         <div class="col-lg-9 col-md-12 content-cnts">
            <div class="blog-single">
               <figure>
                   <?php if ($blogdata->FeaturedImage): ?>
                   <?php $featuredImage = View::common()->getUploadedFiles($blogdata->FeaturedImage); ?>
                       <?php echo View::photo((isset($featuredImage[0]) ? '/files/'.$featuredImage[0]->FileSlug : '/images/user.png'),"img",""); ?>
                    <?php endif;?>
               </figure>
               <div class="blog-byDate">
                  <div class="pmeta">
                     <div class="met"><i class="fa fa-user"></i> <?php echo ($client->FirstName) ? $client->FirstName : ""; ?> <?php echo ($client->LastName) ? $client->LastName : ""; ?></div>
                     <div class="met"><i class="fa fa-calendar"></i> <?php echo ($blogdata->BlogDatePublished != '0000-00-00') ? date('j M, Y', strtotime($blogdata->BlogDatePublished)) : '-' ?></div>
                     <div class="met"><i class="fa fa-tags"></i> <?php echo ($blogdata->CategoryName) ? $blogdata->CategoryName : ""; ?></div>
                  </div>
                  <div class="clear"></div>
               </div>
               <div class="blog-description ul-content">
                    <h3 class="b-title"><?php echo ($blogdata->BlogTitle) ? ucwords($blogdata->BlogTitle) : "No Title"; ?></h3>
                    <?php if($owner) : ?>
                        <?php echo '<p><a class="b-edit" href="/clients/blog/edit/'.$blogdata->BlogID.'">Edit</a></p>';?>
                    <?php endif; ?>
                    <?php echo ($blogdata->BlogContent) ? $blogdata->BlogContent : "No Content"; ?>
               </div>
            </div>
         </div>

         <div class="col-lg-3 sidebar-container">
           <div class="sidebar">
              <div class="sidebar-item">
                 <div class="w-title">Popular Posts</div>
                 <div class="w-content">
                    <ul class="popular-posts-widget">
                        <?php foreach($recentPosts as $post): ?>
                       <li>
                          <figure>
                              <?php if ($post->FeaturedImage): ?>
                              <a href="<?php echo View::url('clients/blog/view/'.$post->BlogSlug.'/'.$post->BlogID); ?>">
                                  <?php $featuredImage = View::common()->getUploadedFiles($post->FeaturedImage); ?>
                                  <?php echo View::photo((isset($featuredImage[0]) ? '/files/'.$featuredImage[0]->FileSlug : '/images/user.png'),$post->BlogTitle,""); ?>
                              </a>
                              <?php else: ?>
                                  <img src="http://via.placeholder.com/85x60">
                              <?php endif;?>



                             <figcaption><a href="<?php echo View::url('clients/blog/view/'.$post->BlogSlug.'/'.$post->BlogID); ?>"><?php echo $post->BlogTitle;?><span><i class="fa fa-calendar"></i> <?php echo date("d M Y", strtotime($post->BlogDatePublished));?></span></a></figcaption>
                          </figure>
                          <span class="clear"></span>
                       </li>
                        <?php endforeach;?>

                    </ul>
                 </div>
              </div>
              <div class="sidebar-item">
                 <div class="w-title">Popular Categories</div>
                 <div class="w-content">
                    <ul class="popular-cat-widget">
                        <?php foreach($categories as $cat): ?>
                            <li><a href="/clients/blog/<?php echo $client->ClientProfileID;?>/categories/<?php echo $cat->BlogCatID;?>"><?php echo $cat->CategoryName;?></a></li>
                        <?php endforeach; ?>

                    </ul>
                 </div>
              </div>

           </div>
        </div>

      </div>
   </article>
</section>

<!-- /page content -->
<?php View::footer(); ?>