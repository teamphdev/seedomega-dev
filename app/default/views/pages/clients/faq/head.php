<?php 
$szero = isset(View::$segments[0]) ? View::$segments[0] : false; 
$sone = isset(View::$segments[1]) ? View::$segments[1] : false; 
$stwo = isset(View::$segments[2]) ? View::$segments[2] : false; 
?>

<!-- page content -->
<section class="header-bottom">
    <article>
        <div class="container animated fadeInDown">
            <h1><?php echo View::$title; ?></h1>
        </div>
    </article>
</section>