<?php 
    View::$title = 'FAQ List';
    View::$bodyclass = User::info('Sidebar').' dev';
    View::header();
    View::page('clients/faq/head');
?>

<section class="gray">
    <!-- Page Content -->
    <div class="container">
        <!-- Dynamic Table Full Pagination -->
        <div class="block">
            <div class="table-responsive">
                <?php echo View::getMessage();  ?> 
                <table class="table js-table-checkable js-dataTable-full-pagination table-divide table-header-bg table-hover table-vcenter" cellspacing="0" width="100%" addbutton='<a class="btn btn-rounded btn-success text-uppercase" href="<?php echo View::url('clients/faq/add'); ?>"><i class="fa fa-fw fa-plus-circle push-5-r"></i> Add FAQ</a>' style="min-width: 700px;">
                    <thead>
                        <tr class="headings">
                            <th class="text-align">Info</th>
                            <th class="text-center">Status</th>
                            <th class="text-center sort-this">Public</th>
                            <th class="text-center no-sorting" style="min-width: 150px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $cntr = 0;
                            if(count($faqPosts)) {
                            foreach($faqPosts as $article) { $cntr++;
                            ?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td data-toggle="modal" data-target="#modal-faq-content" onclick="viewContent( '<?php echo $article->FaqID; ?>' );">
                                <div class="faq-title"><?php echo $article->FaqTitle; ?></div>
                                <div class="hiddenMCE font-12 text-muted">
                                    <?php $excerpt = AppUtility::excerptAsNeeded( $article->FaqContent, 200, '... <span class="text-info">(click to view more)</span>' ); ?>
                                    <?php echo $excerpt; ?>
                                </div>
                            </td>

                            <td><span class="label label-success"><?php echo $article->FaqStatus; ?></span></td>
                            <td class="text-center"><?php echo ($article->PublicView==1)? 'Yes':'No'; ?></td>

                            <td class="text-center">
                                <?php if( $userinfo->Code == 'CLN' || User::can( 'Delete Categories' ) ){ ?>
                                <div class="">
                                    <div class="dropdown more-opt">
                                        <a href="javascript:void(0);" class="dropdown-toggle btn btn-sm btn-default" data-toggle="dropdown">More Options</a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo View::url( 'clients/faq/edit/'.$article->FaqID ); ?>" title="Edit FAQ" data-toggle="tooltip"><i class="fa fa-edit pull-right"></i> Edit</a></li>
                                            <li><a href="<?php echo View::url( 'clients/faq/delete/'.$article->FaqID ); ?>" title="Delete FAQ" data-toggle="tooltip" onclick="return confirm( 'Are you sure you want to delete this FAQ?' );"><i class="fa fa-close pull-right"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <?php } else { ?>
                                    <div class="">
                                        <a href="<?php echo View::url( 'clients/faq/edit/'.$article->FaqID ); ?>" class="btn btn-sm btn-default btn-rounded" title="Edit FAQ" data-toggle="tooltip"><i class="fa fa-edit"></i> Edit</a>
                                    </div>                                    
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } 
                            } else {?>
                        <tr class="<?php echo ($cntr % 2) == 0 ? 'even' : 'odd'; ?> pointer">
                            <td colspan="4">No Data</td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                            <td class="hidden"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <!-- FAQ Content Info Modal -->
                <div class="modal fade" id="modal-faq-content" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
                    <div class="modal-dialog modal-dialog-popout">
                      <div class="modal-content nobg">
                        <div class="block block-themed block-transparent remove-margin-b">
                          <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                              <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                              </li>
                            </ul>
                            <h3 class="block-title"><span id="modal-title"></span></h3>
                          </div>
                          <div class="block-content">
                            <div id="modal-content" class="hiddenMCE"></div>
                          </div>
                        </div>
                        <div class="modal-footer bg-white">
                          <button class="btn btn-rounded btn-danger" type="button" data-dismiss="modal"><i class="si si-logout"></i> CLOSE</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- END FAQ Content Info Modal -->

            </div>
        </div>
    </div>
</section>
<input type="hidden" id="jfaqs" value="<?php echo $jdata; ?>">
<!-- /page content -->
<?php View::footer(); ?>

<script type="text/javascript">
    var faqs = [];
    $( document ).ready( function(){
        $faqs = $( '#jfaqs' ).val();
        faqs = jQuery.parseJSON( $faqs );

        // initialize tinyMCE on contents having .hiddenMCE class
        hiddenMCEconfig.inline = true;
        hiddenMCEconfig.readonly = 1;
        tinymce.init( hiddenMCEconfig ); // hiddenmce.js
    });

    function viewContent( fid ){
        $( '#modal-title' ).text( faqs[fid]['Question'] );
        $( '#modal-content' ).text( faqs[fid]['Answer'] );
        tinymce.get( 'modal-content' ).setContent( faqs[fid]['Answer'] );
    }
</script>