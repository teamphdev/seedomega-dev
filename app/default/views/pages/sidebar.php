<?php $userinfo = User::info(); $agencyID = AppUtility::getAgencyOf($userinfo->UserID); ?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <?php $avatar = View::common()->getUploadedFiles($userinfo->Avatar); ?>
                <?php View::photo((isset($avatar[0]) ? 'files'.$avatar[0]->FileSlug : '/images/user.png'),"Avatar",'img-circle profile_img'); ?>
            </div>
            <div class="profile_info">
                <span><?php echo $userinfo->LastName; ?> <?php echo $userinfo->FirstName; ?>,</span>
                <h2><?php echo $userinfo->UserLevel; ?></h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <div class="clearfix"></div>
        <br />

        <!-- sidebar menu --> 
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3><?php echo Lang::get('MENU_GENERAL_TITLE'); ?></h3>
                <ul class="nav side-menu">
                    <li><a href="<?php echo User::dashboardLink(); ?>"><i class="fa fa-home"></i> <?php echo Lang::get('MENU_DASHBOARD'); ?></a></li>

                    <?php if(User::can('View Casefiles')) { ?>
                        <?php if(User::is('Client')) { ?>
                        <li><a href="<?php echo View::url('casefiles/'); ?>"><i class="fa fa-edit"></i> <?php echo Lang::get('MENU_CF'); ?></a></li>
                        <?php } else { ?>
                        <li><a><i class="fa fa-edit"></i> <?php echo Lang::get('MENU_CF'); ?> <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="<?php echo View::url('casefiles/'); ?>"><?php echo Lang::get('MENU_CF_MANAGE'); ?></a></li>
                                <?php if(User::can('Add Casefiles')) { ?>
                                <li><a href="<?php echo View::url('casefiles/create/'); ?>"><?php echo Lang::get('MENU_CF_ADD'); ?></a></li>
                                <?php } ?>
                                <?php if(User::can('Delete Casefiles') && User::can('Administer All')) { ?>
                                <li><a href="<?php echo View::url('casefiles/trashbin/'); ?>"><?php echo Lang::get('MENU_CF_TRASH'); ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                    <?php } ?>

                    <?php if(User::can('Administer All') || $userinfo->UserLevel == "Administrator" || $userinfo->UserLevel == "GA Admin") { ?>
                    <li><a><i class="fa fa-pencil"></i> Blogs <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('blogs/'); ?>">Manage Blogs</a></li>
                            <li><a href="<?php echo View::url('blogs/list'); ?>">Published Blogs</a></li>
                            <li><a href="<?php echo View::url('blogs/categories'); ?>">Categories</a></li>
                        </ul>
                    </li>
                    <?php }else{ ?>
                        <li><a href="<?php echo View::url('blogs/list'); ?>"><i class="fa fa-pencil"></i> Blogs</a></li>
                    <?php } ?>
                    
                    <?php if($userinfo->UserLevel != "Client"){ ?>
                        <li><a><i class="fa fa-users"></i> <?php echo Lang::get('MENU_CLIENT'); ?> <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <!--li><a href="<?php echo View::url('clients/search/'); ?>"><?php echo Lang::get('MENU_CLIENT_SEARCH'); ?></a></li-->
                                <?php if(User::can('View Clients')) { ?>
                                <li><a href="<?php echo View::url('clients/'); ?>"><?php echo Lang::get('MENU_CLIENT_MY'); ?></a></li>
                                <?php } ?>                                   

                                <?php if($userinfo->UserLevel == "Administrator"){ ?>
                                    <li><a href="<?php echo View::url('client/'); ?>">Withdrawals Requests</a></li>
                                    <li><a href="<?php echo View::url('client/profilereqslist'); ?>">Profile Requests</a></li>
                                    <li><a href="<?php echo View::url('client/allstatements/'); ?>">E-Statements</a></li>
                                <?php }else{ ?> 
                                    <li><a href="<?php echo View::url('client/statement/'.$userinfo->UserID); ?>">E-Statement</a></li>
                                <?php } ?>

                                <?php if($userinfo->UserLevel == "Client"){ ?>
                                    <li><a href="<?php echo View::url('client/profile'); ?>">Update Profile</a></li>
                                <?php } ?>                         
                            </ul>
                        </li>
                    <?php } ?> 
					<?php if($userinfo->UserLevel == "Client"){ ?>
	                    <li><a href="<?php echo View::url('client/profile'); ?>"><i class="fa fa-pencil-square-o"></i> Request Info Update</a></li>
                    	<!-- <li><a href="<?php echo View::url('client/statement/'.$userinfo->UserID); ?>"><i class="fa fa-line-chart"></i> E-Statement</a></li>   -->                                                      
                    <?php } ?>  
                    
                    <?php if(User::can('Edit Agents')) { ?>
                    <li><a><i class="fa fa-user-secret"></i> <?php echo Lang::get('MENU_AGT'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('agent/'); ?>"><?php echo Lang::get('MENU_AGT_MANAGE'); ?></a></li>
                            <?php if(User::can('Add Agents')) { ?>
                            <li><a href="<?php echo View::url('agent/register/'); ?>"><?php echo Lang::get('MENU_AGT_ADD'); ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>

                    <?php 
                    
                    if(User::can('Edit Agency') || (User::is('Assistant') && User::can('Edit Agency',$agencyID)) ) { ?>
                    <li><a><i class="fa fa-building"></i> <?php echo Lang::get('MENU_AGY'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('agency/'); ?>"><?php echo Lang::get('MENU_AGY_MANAGE'); ?></a></li>
                            <?php if(User::can('Add Agency') || User::can('Add Agency',$agencyID)) { ?>
                            <li><a href="<?php echo View::url('agency/register/'); ?>"><?php echo Lang::get('MENU_AGY_ADD'); ?></a></li>
                            <?php } ?>
                            <!--li><a href="<?php echo View::url('agency/commission/'); ?>">Commission Scheme</a></li-->
                        </ul>
                    </li>
                    <?php } ?>

                    <?php if(User::can('Edit Assistant')) { ?>
                    <li><a><i class="fa fa-building"></i> <?php echo Lang::get('MENU_MNG'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('assistant/'); ?>"><?php echo Lang::get('MENU_MNG_MANAGE'); ?></a></li>
                            <?php if(User::can('Add Assistant')) { ?>
                            <li><a href="<?php echo View::url('assistant/register/'); ?>"><?php echo Lang::get('MENU_MNG_ADD'); ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>

                    <?php if(User::can('View Reports')) { ?>
                    <li><a><i class="fa fa-bar-chart-o"></i> <?php echo Lang::get('MENU_RPT'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('reports/sales/'); ?>"><?php echo Lang::get('MENU_RPT_SALES'); ?></a></li>
                            <li><a href="<?php echo View::url('reports/groupsales/'); ?>"><?php echo Lang::get('MENU_RPT_GSALES'); ?></a></li>
                            <li><a href="<?php echo View::url('reports/networkhierarchy/'); ?>"><?php echo Lang::get('MENU_RPT_NETWORK'); ?></a></li>
                            <?php if(User::can('View Commissions') && User::can('Administer All')) { ?>
                            <li><a href="<?php echo View::url('reports/commissions/'); ?>"><?php echo Lang::get('SIDEBAR_COMM'); ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>                                       
                    
                    <?php if(User::can('Administer All')) { ?>
                    <li><a href="<?php echo View::url('downloads'); ?>"><i class="fa fa-download"></i> <?php echo Lang::get('SIDEBAR_DOWNLOADS'); ?></a></li>
                    <?php } ?>

                    <!-- <?php if(User::is('Administrator')) { ?>
                    <li><a href="<?php echo View::url('claims'); ?>"><i class="fa fa-list-alt"></i> Claims</a></li>
                    <?php } ?> -->

                    <?php if(User::is('Administrator') || (User::is('Agency') && User::can('Generate Demand Note',$userinfo->UserID)) || ((User::is('Agent') || User::is('Assistant')) && User::can('Generate Demand Note',AppUtility::getAgencyOf($userinfo->UserID)))) { ?>
                        <li><a href="<?php echo View::url('dngenerate'); ?>"><i class="fa fa-paper-plane"></i> <?php echo Lang::get('MENU_GENDEMAND_NOTE_GA'); ?></a></li>
                        <li><a href="<?php echo View::url('dngenerate/mta'); ?>"><i class="fa fa-paper-plane"></i> <?php echo Lang::get('MENU_GENDEMAND_NOTE_MTA'); ?></a></li>
                    <?php } ?>
                </ul>
            </div>

            <?php if(User::is('Administrator')) { ?>
            <div class="menu_section">
                <h3><?php echo Lang::get('MENU_ADMIN_TITLE'); ?></h3>
                <ul class="nav side-menu">
                    <?php if(User::can('Edit Media')) { ?>
                    <li><a><i class="fa fa-film"></i> <?php echo Lang::get('MENU_MEDIA'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('mediamanager/'); ?>"><?php echo Lang::get('MENU_MEDIA_MY'); ?></a></li>
                            <li><a href="<?php echo View::url('mediamanager/add/'); ?>"><?php echo Lang::get('MENU_MEDIA_ADD'); ?></a></li>
                            <li><a href="<?php echo View::url('mediamanager/categories/'); ?>">Categories</a></li>
                            <?php /*<li><a href="<?php echo View::url('mediamanager/subcategories/'); ?>">Sub Categories</a></li> */ ?>
                        </ul>
                    </li>
                    <?php } ?>

                    <?php if(User::can('Edit Product')) { ?>
                    <li><a><i class="fa fa-qrcode"></i> <?php echo Lang::get('MENU_PRD'); ?> <span class="fa fa-chevron-down"></span></a> 
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('products/'); ?>"><?php echo Lang::get('MENU_PRD_MANAGE'); ?></a></li>
                            <li><a href="<?php echo View::url('products/add/'); ?>"><?php echo Lang::get('MENU_PRD_ADD'); ?></a></li>
                            <li><a href="<?php echo View::url('products/trashbin/'); ?>"><?php echo Lang::get('MENU_PRD_TRASH'); ?></a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-list-alt"></i> <?php echo Lang::get('MENU_PRDI'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('productitems/'); ?>"><?php echo Lang::get('MENU_PRDI_MANAGE'); ?></a></li>
                            <li><a href="<?php echo View::url('productitems/add/'); ?>"><?php echo Lang::get('MENU_PRDI_ADD'); ?></a></li>
                            <li><a href="<?php echo View::url('productitems/trashbin/'); ?>"><?php echo Lang::get('MENU_PRDI_TRASH'); ?></a></li>
                        </ul>
                    </li>
                    <?php } ?> 

                    <?php if(User::can('Edit Users')) { ?>
                    <li><a><i class="fa fa-user"></i> <?php echo Lang::get('MENU_USER'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('users/'); ?>"><?php echo Lang::get('MENU_USER_MANAGE'); ?></a></li>
                            <li><a href="<?php echo View::url('users/add/'); ?>"><?php echo Lang::get('MENU_USER_ADD'); ?></a></li>
                            <li><a href="<?php echo View::url('users/trashbin/'); ?>"><?php echo Lang::get('MENU_USER_TRASH'); ?></a></li>
                        </ul>
                    </li>
                    <?php }
                    if(User::can('Manage Roles')) { ?>
                    <li><a><i class="fa fa-lock"></i> <?php echo Lang::get('MENU_ROLES'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('role/'); ?>"><?php echo Lang::get('MENU_ROLES_MANAGE'); ?></a></li>
                            <li><a href="<?php echo View::url('role/add/'); ?>"><?php echo Lang::get('MENU_ROLES_ADD'); ?></a></li>
                        </ul>
                    </li>
                    <?php }
                    if(User::can('Manage Capabilities')) { ?>
                    <li><a><i class="fa fa-lock"></i> <?php echo Lang::get('MENU_CPB'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo View::url('capability/'); ?>"><?php echo Lang::get('MENU_CPB_MANAGE'); ?></a></li>
                            <li><a href="<?php echo View::url('capability/add/'); ?>"><?php echo Lang::get('MENU_CPB_ADD'); ?></a></li>
                            <li><a href="<?php echo View::url('capabilitygroups/'); ?>"><?php echo Lang::get('MENU_CPB_GRP'); ?></a></li>
                            <li><a href="<?php echo View::url('capabilitygroups/add/'); ?>"><?php echo Lang::get('MENU_CPB_GRP_ADD'); ?></a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if(User::can('Manage Settings')) { ?>
                    <li><a><i class="fa fa-gear"></i> <?php echo Lang::get('MENU_STNGS'); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            
                            <li><a href="<?php echo View::url('options/'); ?>"><?php echo Lang::get('MENU_STNGS_SOPT'); ?></a></li>
                            <li><a href="<?php echo View::url('export/'); ?>"><?php echo Lang::get('MENU_STNGS_IE'); ?></a></li>
                            <li><a href="<?php echo View::url('manageoptions/'); ?>"> <?php echo Lang::get('MENU_STNGS_MNG_OPT'); ?></a></li>
                            <li><a href="<?php echo View::url('settings/'); ?>"><?php echo Lang::get('MENU_STNGS_CONF'); ?></a></li>
                            
                            <li><a href="<?php echo View::url('activitylogs/'); ?>"><?php echo Lang::get('MENU_ACTYLOGS'); ?></a></li>
                        </ul>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php View::url('user/logout', true) ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>