<?php

class EmailTemplates_model extends Model 
{
    function __construct() {
        parent::__construct();

        //$this->load->model('history',false,true);
        
        View::$segments = $this->segment;
    }
       
    function getEmailTemplate($ID)
    {
        $sql = "SELECT * FROM email_templates WHERE id = ".$ID." LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    } 
    
    function getEmailTemplates($inactive = '')
    {
        $sql = "SELECT * FROM email_templates";
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    }
    
    function doSave()
    {
        //$data = json_decode(file_get_contents('php://input'), true);
        $inputJSON = file_get_contents('php://input');
        if($inputJSON){
            $input = json_decode( $inputJSON); 

            App::setFileContents('emails/'.$input->filename, $input->filecontent);      

            $data = ['filename'=>$input->filename,
                'description' => $input->description,
                'name'=> $input->name,
                'updated_at' => date('Y-m-d H:i:s')
            ]; 

            $id = $this->db->update("email_templates", $data, array('id' => $input->id));   

            $history = App::load()->model('history',true,true);
            $hid = $history->log(User::info('UserID'), 'User edited email template','emailtemplate',$data);
            header('Content-type: application/json');
            echo json_encode( array('status'=>1,'html'=>'ok'));
        }

        if($this->post) { 
            if(isset($this->post['action'])) {
                switch($this->post['action']) {                   
                    case "addemailtemplate": {

                        $data = $this->post;
                        unset($data['action']);
                        $data['created_at'] = date('Y-m-d H:i:s');
                        $data['updated_at'] = date('Y-m-d H:i:s');

                        $prodID = $this->db->insert("email_templates", $data);

                        if($prodID) {
                            $history = App::load()->model('history',true,true);
                            $hid = $history->log(User::info('UserID'), 'User added email template','emailtemplate',$data);
                            $this->setSession('message',"New email template has been registered!");
                        }

                        //View::redirect('agency');
                    } break;
                } 
            }
            return (object) $this->post;
        } else {
           
        }
    }   
    
    function doDelete($ID)
    {
        $where = array( 'id' => $ID );
        $this->setSession( 'message',"Email Template has been deleted!" );
        $rowCount = $this->db->delete( "email_templates", $where );
    }
}