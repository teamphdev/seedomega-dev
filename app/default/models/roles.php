<?php
class Roles_model extends Model
{
    function __construct() {
        parent::__construct();

        View::$segments = $this->segment;
    }
    
    function getLevel($ID)
    {
        $sql = "SELECT * FROM user_levels WHERE UserLevelID = ".$ID." LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    } 
    
    function getLevels()
    {
        $data = array();
        $sql = "SELECT * FROM user_levels";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    } 
    
    function getCapabilities()
    {
        // WHERE uc.UserCapabilityGroupID > 1
        $data = array();
        $sql = "SELECT uc.*, ucg.Name as GroupName FROM user_capabilities uc LEFT JOIN user_capability_groups ucg ON uc.UserCapabilityGroupID = ucg.UserCapabilityGroupID ORDER BY ucg.Name";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->GroupName][$row->UserCapabilityID] = $row;
        }
        unset($query);
        
        return $data;
    } 
    
    function doSave()
    {
        if($this->post) { 
            if(isset($this->post['action'])) {
                switch($this->post['action']) {
                    case "updaterole": {
                        $data = $this->post;                    
                        
                        $data['Capability'] = $this->arrayToString(array('none'));
                        
                        if(isset($data['capabilities']) && count($data['capabilities'])) {
                            $data['Capability'] = $this->arrayToString($data['capabilities']);
                        }
                        
                        $where = array('UserLevelID' => $this->post['roleid']);

                        unset($data['action']);
                        unset($data['roleid']);
                        unset($data['capabilities']);
                        unset($data['applyRoleToUSsers']);
                        
                        $rowCount = $this->db->update("user_levels", $data, $where);
                        
                        if(isset($this->post['applyRoleToUSsers'])) {
                            $rowCount = $this->db->update("users", array('Capability'=>$data['Capability']), array('Level' => $this->post['roleid']));
                        }
                        
                        $this->setSession('message',"User role data has been saved.");                        
                        App::activityLog("Updated Role #".$this->post['roleid'].'.');

                    } break;
                    case "addrole": {
                        $data = $this->post;                    
                        
                        unset($data['action']);
                        
                        $levelID = $this->db->insert("user_levels", $data);
                        
                        if($levelID) {                            
                            $this->setSession('message',"New user role has been added!");
                        }
                        
                        App::activityLog("Added Role #".$levelID.'.');
                    } break;
                } 
            }
            return (object) $this->post;
        } else {            
        }
    } 
    
    function doApplyToUsers($ID=NULL) {
        if($ID) {
            $capa = Level::info('Capability',$ID);
            $rowCount = $this->db->update("users", array('Capability'=>$capa), array('Level' => $ID));              
        }
    }
    
    function doDelete($UserLevelID)
    {
        $where = array('UserLevelID' => $UserLevelID);
        $rowCount = $this->db->delete("user_levels", $where);
        
        App::activityLog("Deleted Role #".$UserLevelID.'.');
    }
}