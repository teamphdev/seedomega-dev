<?php
class Ajax_model extends Model
{    
    function getClientIDs()
    {
        $sql = "SELECT u.UserID, a.AccountID 
        FROM accounts a 
        LEFT JOIN users u ON a.UserID = u.UserID 
        LEFT JOIN user_meta um ON u.UserID = um.UserID 
        WHERE u.Level = 5 AND a.Active = 1";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    } 

    function clearSoaTable()
    {
        $sql = "DELETE FROM `client_soa`";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        return true;
    } 

    function updateSoa($data)
    {
        $return = false;
        $return = $this->db->insert("client_soa", $data);      
        return $return;
    } 

    function updateSoainfo($id,$data)
    {
        $return = false;
        $return = $this->db->update("accounts", $data, array('UserID'=>$id));      
        return $return;
    } 
}