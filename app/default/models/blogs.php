<?php
class Blogs_model extends Model
{
    var $levelID = 4;

    function __construct() {
        parent::__construct();

        View::$segments = $this->segment;
    }
       
    function getBlog( $ID, $so = false )
    {
        $where = $so ? " AND b.`ClientProfileID` = 0 " : "";
        $sql = "SELECT b.*, bc.*, ul.Name as LevelName, ul.Code as LevelCode, um.FirstName, um.LastName 
            FROM blogs b 
            LEFT JOIN blog_categories bc ON bc.BlogCatID = b.BlogCatID 
            LEFT JOIN users u ON b.UserID = u.UserID 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE b.BlogID = ".$ID.$where;

        $data = $this->db->get_row($sql);

        return $data;
    } 
    
    function getBlogs( $status = false, $limit = false, $catname = false, $SOblogs = NULL, $publiconly = false, $searchString = null, $clientProfileID = null )
    {
        $sql = "SELECT b.*, bc.*, ul.Name as LevelName, ul.Code as LevelCode, um.FirstName, um.LastName,
                CASE WHEN b.ClientProfileID = 0 THEN 'SeedOmega' ELSE cp.CompanyName END AS CompanyName
            FROM blogs b 
            LEFT JOIN client_profiles cp ON cp.ClientProfileID = b.ClientProfileID
            LEFT JOIN blog_categories bc ON bc.BlogCatID = b.BlogCatID 
            LEFT JOIN users u ON b.UserID = u.UserID 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE b.UserID > 0";

        if( $publiconly ){
            $sql .= " AND b.PublicView = 1";
        }

        if( $status ){
            $sql .= " AND b.BlogStatus = '$status'";
        }

        if( $catname ){
            $sql .= " AND bc.CategorySlug = '$catname'";
        }

        switch( $SOblogs ){
            case 'both':
                $sql .= " AND b.ClientProfileID IS NOT NULL";
                break;
            case 'so':
                $sql .= " AND b.ClientProfileID = 0";
                break;
            
            default:
                $sql .= " AND b.ClientProfileID > 0";
                break;
        }

        if( $clientProfileID ){
            $sql .= " AND b.ClientProfileID = '$clientProfileID'";
        }

        if( $searchString ){
            $sql .= " AND b.BlogTitle LIKE '%$searchString%' ";
        }

        $sql .= " ORDER BY BlogDatePublished DESC ";
        if( $limit ){
            $sql .= " LIMIT 0,".$limit;
        }
        // echo $sql.' (SOblogs: '.$SOblogs.')';
        
        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );
        
        return $data;
    }

    function getCategories( $clientProfileID = 0, $orderby = 'CategoryName', $public = false )
    {
        // $sql = "SELECT * FROM blog_categories WHERE ClientProfileID = $clientProfileID ";

        $where = $public ? " AND b.`PublicView` = ".$public : "";
        $sql = "SELECT DISTINCT bc.*
        FROM `blogs` b
        LEFT JOIN `blog_categories` bc ON bc.`BlogCatID` = b.`BlogCatID`
        WHERE b.`ClientProfileID` = $clientProfileID AND bc.`BlogCatID` IS NOT NULL".$where;
        $sql .= " ORDER BY bc.`$orderby`";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getAllCategories( )
    {
        $sql = "SELECT * FROM `blog_categories` GROUP BY CategorySlug ORDER BY CategoryName ASC ";
        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getCategory($ID)
    {
        $sql = "SELECT * 
            FROM blog_categories bc WHERE bc.BlogCatID = ".$ID;

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getMyClients($UserID)
    {
        $sql = "SELECT b.ClientProfileID,cp.ClientProfileID, cp.UserID, cp.CompanyName, (SELECT count(*) as blogcount FROM blogs WHERE BlogStatus = 'Published' AND ClientProfileID = cp.ClientProfileID ) as blogCount FROM `bookings` as b LEFT JOIN client_profiles as cp ON cp.ClientProfileID = b.ClientProfileID WHERE b.BookingStatus = 'Approved' AND b.UserID = '$UserID' GROUP BY b.ClientProfileID";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }
    
    function getUserLevels()
    {
        $sql = "SELECT UserLevelID,Name FROM user_levels";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserLevelID] = $row->Name;			
        }
        unset($query);
        
        return $data;
    }

    function getCapabilities()
    {
        // WHERE uc.UserCapabilityGroupID > 0
        $data = array();
        $sql = "SELECT uc.*, ucg.Name as GroupName FROM user_capabilities uc LEFT JOIN user_capability_groups ucg ON uc.UserCapabilityGroupID = ucg.UserCapabilityGroupID WHERE uc.Active = 1 ORDER BY ucg.UserCapabilityGroupID ASC";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->GroupName][$row->UserCapabilityID] = $row;
        }
        unset($query);
        
        return $data;
    }    
    
    function doSave()
    {
        $blogpostID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){
                case "updatepost": {
                    $blogdata = $this->post;
                    $postid = $blogdata['blogid'];
                    unset( $blogdata['action'] );        
                    $blogdata['BlogSlug'] = str_replace(" ", "-", strtolower($blogdata['BlogTitle']));

                    if( $blogdata['BlogDatePublished'] == '0000-00-00 00:00:00' && $blogdata['BlogStatus'] == "Published" ){
                        $blogdata['BlogDatePublished'] = date('Y-m-d H:i:s');
                    }

                    $fileData = $this->fileUpload( $this->file,User::info('UserID'),'',false,6,0,false,'x' );

                    if( $fileData ){
                        $blogdata['FeaturedImage'] = $fileData['FeaturedImage'];
                    }

                    $blogID = $this->db->update( "blogs", $blogdata, array( 'BlogID' => $postid ) );
                    
                    $this->setSession( 'message',"Blog has been updated!" );
                    App::activityLog( "Updated Blog BlogID#-".$blogID.'.' );
                } break;

                case "addnewpost": {
                    $userid = User::info('UserID');
                    $blogdata = $this->post;
                    unset( $blogdata['action'] );

                    if( $blogdata['BlogStatus'] == "Published" ){
                        $blogdata['BlogDatePublished'] = date('Y-m-d H:i:s');
                    }

                    $blogdata['BlogSlug'] = str_replace( " ", "-", strtolower( $blogdata['BlogTitle'] ) );
                    $blogdata['BlogSlug'] = AppUtility::cleanstring($blogdata['BlogSlug']);

                    $fileData = $this->fileUpload($this->file,$userid,'',false,6,0,false,'x');

                    if( $fileData ){
                        $blogdata['FeaturedImage'] = $fileData['FeaturedImage'];
                    }

                    $blogdata['UserID'] = $userid;
                    $blogID = $this->db->insert( "blogs", $blogdata );

                    $this->setSession( 'message', "New Blog has been added!" );
                    App::activityLog( "Added blog BlogID#-".$blogID.'.' );
                } break;

                case "updatecategory": {
                    $catdata = $this->post;
                    $catid = $catdata['catid'];
                    unset( $catdata['action'] );
                    unset( $catdata['catid'] );                    

                    $catdata['CategorySlug'] = str_replace( " ", "-", strtolower( $catdata['CategoryName'] ) );
                    $catID = $this->db->update( "blog_categories", $catdata, array( 'BlogCatID' => $catid ) );
                    
                    $this->setSession( 'message', "Blog category has been updated!" );
                    App::activityLog( "Updated blog category CatID#-".$catid.'.' );
                } break;

                case "addnewcategory": {
                    $catdata = $this->post;
                    unset( $catdata['action'] );

                    $catdata['CategorySlug'] = str_replace( " ", "-", strtolower( $catdata['CategoryName'] ) );
                    $catID = $this->db->insert( "blog_categories", $catdata );

                    $this->setSession('message',"New blog category has been added!");
                    App::activityLog("Added blog category CatID#-".$catID.'.');
                } break;
            }

            return $blogpostID;
        }        
    }
    
    function doCatDelete($ID)
    {
        $where = array( 'BlogCatID' => $ID );
        $rowCount = $this->db->delete( "blog_categories", $where );

        $this->setSession( 'message', "Blog category has been deleted!" );        
        App::activityLog( "Deleted Category BlogCatID#-".$ID.'.' );
    }

    function doDelete( $ID )
    {
        $where = array( 'BlogID' => $ID );
        $rowCount = $this->db->delete( "blogs", $where );

        $this->setSession( 'message', "Blog has been deleted!" );
        App::activityLog( "Deleted Blog BlogID#-".$ID.'.' );
    }
}