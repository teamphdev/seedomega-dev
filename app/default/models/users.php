<?php
class Users_model extends Model
{
    function __construct()
    {
        parent::__construct();

        View::$segments = $this->segment;
    }

    function doSession( $userdata = false, $msg = 'Login sucessful!', $logmsg = 'Logged In:' )
    {
        //unset($_SESSION[SESSIONCODE]);
        if($userdata) {
            $udata = (array) $userdata;
            $this->setSession('loggedin', true);
            $this->setSession('userdata', $udata);                    
            $this->setSession('language', $udata['Language']);
            $this->setSession('message', $msg);

            App::activityLog($logmsg." #".$udata['UserID']." - ".$udata['LastName']." ".$udata['FirstName'].".");

            $referrer = ($this->getCookie(Config::get('REFERRER'))) ? $this->getCookie(Config::get('REFERRER')) : User::dashboardLink(true);
            // this will not redirect properly if .mpf variable is not set REFERRER
            View::redirect( $referrer );
        } else {
            View::redirect();
        }
    }
    
    function doLogin()
    {
        if( isset( $this->post['action'] ) && $this->post['action'] == 'login' ){
            
            // $this->post['usr'] = preg_replace("/[^0-9,.]/", "", $this->post['usr']);
            $userdata = User::info( false, $this->post['usr'] ) ? User::info( false, $this->post['usr'] ) : User::infoByEmail( false, $this->post['usr'] );

            if( $userdata && $userdata->Active == 1 ){
                if( $this->decrypt( $this->post['pwd'], $userdata->Password ) ){
                    App::setUserCookie( $userdata );
                    $this->doSession( $userdata );
                    //self::apdeythas( $this->post['pwd'], $userdata );
                } else {
                    $this->setSession( 'error', 'Invalid password!' );
                }
            } else {
                $this->setSession( 'error', 'User ID doesn\'t exists!' );
            }
        }
    }   

    function doFBLogin($user=false)
    {
        if($user) {            
            // $this->post['usr'] = preg_replace("/[^0-9,.]/", "", $this->post['usr']);
            $userdata = User::infoByEmail(false,$user['email']);
            if($userdata && (isset($userdata->Active) && $userdata->Active == 1)) {
                App::setUserCookie($userdata);
                $this->doSession($userdata,'Login using facebook sucessful!','Logged in using Facebook:');
            } else {
                $this->setSession('error', 'User ID doesn\'t exists!');
                View::redirect('users/login');
            }
        }
    }

    function doFBSignup($user=false)
    {
        if( $user ){
            $userdata = User::infoByEmail(false,$user['email']);
            if($userdata && (isset($userdata->Active) && $userdata->Active == 1)) {
                App::setUserCookie($userdata);
                $this->doSession($userdata);
            } else {
                $meta = array();
                $acct = array();
                $udata = array();

                $pw = $this->generatePw(10);
                
                $udata['Email'] = $user['email'];
                $udata['Password'] = $this->encrypt($pw);
                $udata['Active'] = 1;
                $udata['Level'] = 7;
                // $udata['HashKey'] = base64_encode($pw);
                $udata['HashKey'] = self::enchash( $pw );
                $udata['DateAdded'] = date('Y-m-d H:i:s');
                $udata['Capability']=Level::info('Capability',isset($user['Level']) ? $user['Level'] : Option::get('new_user_role'));
                
                $affiliateUserID = isset($_COOKIE[Config::get('AFFILIATEKEY')]) ? $_COOKIE[Config::get('AFFILIATEKEY')] : 0;
                $affiliateUserID  = (User::info('UserID',$affiliateUserID) != '') ? $affiliateUserID : 100000;
                $udata['ReferrerUserID'] = $affiliateUserID;

                $userID = $this->db->insert("users", $udata);

                if( $userID ){
                    $this->setSession('message',"Thank you for joining the SeedOmega platform! To activate your account, please check for a confirmation email from SeedOmega.");

                    $where = array('UserID' => $userID);

                    // TO DO: send email
                    $meta['UserID'] = $userID;
                    $meta['FirstName'] =  $user['first_name'];
                    $meta['LastName'] =  $user['last_name'];
                    $metaID = $this->db->insert("user_meta", $meta);

                    $acct['UserID'] = $userID;
                    $acct['Status'] = 'Incomplete';
                    $acctID = $this->db->insert("accounts", $acct);

                    $this->doSaveReferrer( $userID, $affiliateUserID ); //save to user_referrer table

                    $userdata = User::infoByEmail(false,$user['email']);
                    $this->doSession($userdata,'Signup using facebook sucessful! You are now logged in.','Signup using Facebook:');

                    return $userID;
                }
            }
        } else {
            View::redirect('users/signup');
        }
    }

    function doGoogleSignup($user=false)
    {
        if ($user) {
            $userdata = User::infoByEmail(false,$user->email);
            if($userdata && (isset($userdata->Active) && $userdata->Active == 1)) {
                App::setUserCookie($userdata);
                $this->doSession($userdata);
            } else {
                $meta = array();
                $acct = array();
                $udata = array();

                $pw = $this->generatePw(10);
                
                $udata['Email'] = $user['email'];
                $udata['Password'] = $this->encrypt($pw);
                $udata['Active'] = 1;
                $udata['Level'] = 7;
                // $udata['HashKey'] = base64_encode($pw);
                $udata['HashKey'] = self::enchash( $pw );
                $udata['DateAdded'] = date('Y-m-d H:i:s');
                $udata['Capability']=Level::info('Capability',isset($user['Level']) ? $user['Level'] : Option::get('new_user_role'));
                
                $affiliateUserID = isset($_COOKIE[Config::get('AFFILIATEKEY')]) ? $_COOKIE[Config::get('AFFILIATEKEY')] : 0;
                $affiliateUserID = (User::info('UserID',$affiliateUserID) != '') ? $affiliateUserID : 100000;
                $udata['ReferrerUserID'] = $affiliateUserID;

                $userID = $this->db->insert("users", $udata);

                if($userID) {
                    $this->setSession('message',"Thank you for joining the SeedOmega platform! To activate your account, please check for a confirmation email from SeedOmega.");

                    $where = array('UserID' => $userID);

                    // TO DO: send email
                    $meta['UserID'] = $userID;
                    $meta['FirstName'] =  $user->givenName;
                    $meta['LastName'] =  $user->familyName;
                    $metaID = $this->db->insert("user_meta", $meta);

                    $acct['UserID'] = $userID;
                    $acct['Status'] = 'Incomplete';
                    $acctID = $this->db->insert("accounts", $acct);

                    $this->doSaveReferrer( $userID, $affiliateUserID ); //save to user_referrer table

                    $userdata = User::infoByEmail(false,$user->email);
                    $this->doSession($userdata,'Signup using Google sucessful! You are now logged in.','Signup using Google:');

                    return $userID;
                }
            }
        } else {
            View::redirect('users/signup');
        }
    }

    function doSaveReferrer( $uid, $aid, $update = false )
    {
        if( $update ){
            $where = array( 'UserID' => $uid );
            $refer = array( 'ReferrerUserID' => $aid );
            $referID = $this->db->update( "user_referrer", $refer, $where );
        } else {
            $refer['UserID'] = array( 'UserID' => $uid, 'ReferrerUserID' => $aid );
            $referID = $this->db->insert( "user_referrer", $refer );
        }

        return $referID;
    }

    function doRequest()
    {
        $return = false;
        if(isset($this->post['action']) && $this->post['action'] == 'request') {
            $sql = "SELECT * FROM users WHERE UserID = '".$this->post['usr']."' OR Email = '".$this->post['usr']."' LIMIT 1";
            $userdata = $this->db->get_row($sql);

            if(isset($userdata->UserID)) {
                if(($userdata->ResetKey == '')) {
                    $resetkey = md5(microtime());
                    $this->db->update('users',array('ResetKey'=>$resetkey),array('UserID'=>$userdata->UserID));
                }
                $return = $userdata;
            } else {
                $this->setSession('error', 'User ID doesn\'t exists!');
            }
        }

        return $return;
    }   

    function doRequestReset()
    {
        $return = false;
        if(isset($this->post['action']) && $this->post['action'] == 'requestreset') {            

            if($this->post['pass1']==$this->post['pass2']) {
                $ID         = $this->post['resetkey'];
                $newpass    = $this->post['pass2'];
                $return     = $this->db->update( 'users', 
                                                        array(
                                                            'Password'=>$this->encrypt($newpass),
                                                            'ResetKey'=>'',
                                                            'HashKey'=>self::enchash( $newpass )
                                                        ),
                                                        array('ResetKey'=>$ID)
                                                );
                $this->setSession('message', 'Password updated! You can now login.');
            } else {
                $this->setSession('error', 'Password mismatch!');
            }
        }

        return $return;
    }   

    function checkRequestResetKey($id=false)
    {
        $return = false;
        if($id) {           
            $sql = "SELECT * FROM users WHERE ResetKey = '".$id."' LIMIT 1";
            $userdata = $this->db->get_row($sql);
            if($userdata) {
                $return = true; 
            }
        }

        return $return;
    }     
    
    function updateUserInfo()
    {
        $userdata = User::info(false,User::info('UserID'));
        $udata = (array) $userdata;
        $this->setSession('userdata', $udata);
        $this->setSession('language', $udata['Language']);
    } 
    
    function getUser($ID)
    {
        $sql = "SELECT u.*,um.*,ul.Name,ul.Code, 
        (SELECT CONCAT(uul.Code, uu.UserID) as FirstAgentID FROM users uu LEFT JOIN user_levels uul ON uu.Level = uul.UserLevelID WHERE uu.UserID = u.ReferrerUserID) as AgentID
            FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE u.UserID = ".$ID." LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    }

    function validateUniqueEmail($ID, $Email)
    {
        $sql = "SELECT u.UserID, u.Email FROM users u WHERE u.UserID != ".$ID." AND u.Email = '".$Email."' LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    }
    
    function getUserByCode($ID)
    {
        if($ID) {        
            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName, CONCAT(ul.Code,u.UserID) as UserCode, a.CompanyName FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID LEFT JOIN accounts a ON a.UserID = u.UserID WHERE CONCAT(ul.Code,u.UserID) = '$ID' OR u.UserID = '$ID' LIMIT 1";

            $query = &$this->db->prepare($sql);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_CLASS);
            unset($query);
            
            return json_encode($row);
        } else {
            return false;
        }
    }

    function getUserByCodeIdNumber($ID)
    {
        if($ID) {        
            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName, CONCAT(ul.Code,u.UserID) as UserCode, a.CompanyName 
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            LEFT JOIN accounts a ON a.UserID = u.UserID 
            WHERE CONCAT(ul.Code,u.UserID) = '$ID' OR u.UserID = '$ID' OR um.IdNumber = '$ID' LIMIT 1";

            $query = &$this->db->prepare($sql);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_CLASS);
            unset($query);
            
            return json_encode($row);
        } else {
            return false;
        }
    }

    function getUserBySearch($Search)
    {
        if($Search) {

            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName, CONCAT(ul.Code,u.UserID) as UserCode
            FROM users u
            LEFT JOIN user_meta um ON um.UserID = u.UserID
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE (u.UserID = '".$Search."' OR u.Email = '".$Search."') AND u.UserID != 0 AND u.Active = 1 LIMIT 1";

            $row = $this->db->get_row($sql);

            return json_encode($row);

        } else {
           return false;
        }
    }

    function getUserCompanyName($ID)
    {
        if($ID) {        
            $sql = "SELECT CompanyName FROM accounts WHERE UserID = (SELECT GetAgency(UserID) as ID FROM `users` WHERE UserID = $ID) LIMIT 1";

            $cp = $this->db->get_row($sql);

            return $cp->CompanyName;
        } else {
            return false;
        }
    }
    
    function getUserByClientEmail($email)
    {
        if($email) {        
            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName, CONCAT(ul.Code,u.UserID) as UserCode FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID LEFT JOIN accounts a ON a.UserID = u.UserID WHERE a.AccountEmail = '".$email."' LIMIT 1";

            $row = $this->db->get_row($sql);
            
            return $row;
        } else {
            return false;
        }
    }
    
    function getUsers( $inactive = '' )
    {
        $sql = "SELECT u.*, um.*, ul.`Name`, ul.`Code`, fi.`FileSlug` FROM `users` u LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID` LEFT JOIN `user_levels` ul ON u.`Level` = ul.`UserLevelID` LEFT JOIN `file_items` fi ON fi.FileID = um.`Avatar`";
        $where = " WHERE u.`Active` = 1";
        if( $inactive == 'yes' ){
            $where = " WHERE u.`Active` != 1";
        } 
        $sql .= $where;
        $sql .= " ORDER BY u.`UserID`";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    }

    /**
     * Get own referral list
     * @access Private
     * @param $userinfo (array)    : logged in User Info
     * @param $converted (boolean) : true for booking query
     * @return Array
     **/
    function getAffiliatesOwn( $userinfo, $converted = false )
    {
        if( $converted ){
            $sql = "SELECT DISTINCT
                b.`InvestmentBookingID`,
                b.`UserID` AS InvestorUserID,
                u_inv.`ReferrerUserID`,
                cp_ref.`ClientProfileID` AS ReferrerClientID,
                CONCAT( um_inv.`FirstName`, ' ', um_inv.`LastName` ) AS InvestorName,
                u_inv.`DateAdded` AS JoinDate,
                ur_ref.`CommissionRate`,
                b.`TotalAmountAttached` AS InvestmentAmount, 
                FORMAT( b.`TotalAmountAttached` * ( ur_cli.`SubscriptionRate` / 100 ), 2 ) AS `SOCommission`,
                b.`CreatedAt` AS BookingDate,
                cp_cli.`UserID` AS InvestedUserID,
                cp_cli.`ClientProfileID` AS InvestedClientID,
                cp_cli.`CompanyName` AS InvestedCompany,
                fi.`FileSlug`
            FROM `bookings` b
            LEFT JOIN `users` u_inv ON u_inv.`UserID` = b.`UserID`
            LEFT JOIN `user_meta` um_inv ON um_inv.`UserID` = b.`UserID`
            LEFT JOIN `client_profiles` cp_cli ON cp_cli.`ClientProfileID` = b.`ClientProfileID` 
            LEFT JOIN `client_profiles` cp_ref ON cp_ref.`UserID` = u_inv.`ReferrerUserID` 
            LEFT JOIN `file_items` fi ON fi.`FileID` = um_inv.`Avatar`
            LEFT JOIN `user_referrer` ur_cli ON ur_cli.`UserID` = cp_cli.`UserID`
            LEFT JOIN `user_referrer` ur_ref ON ur_ref.`UserID` = u_inv.`ReferrerUserID`
            WHERE b.`BookingStatus` = 'Approved' AND u_inv.`Level` IN ( 4, 7 ) AND b.`InvestmentBookingID` NOT IN ( SELECT DISTINCT `InvestmentBookingID` FROM `referral_earnings` WHERE `InvestorUserID` = b.`UserID` ) AND u_inv.`ReferrerUserID` =  $userinfo->UserID";

            if( $userinfo->Code == 'CL' ){
                $sql = "SELECT * FROM ( $sql ) AS referrals WHERE `ReferrerClientID` <> `InvestedClientID` ORDER BY `BookingDate`";
            }

        } else {
            $sql = "SELECT DISTINCT u.`UserID`, um.`Avatar`, um.`FirstName`, um.`LastName`, a.`Status`, u.`DateAdded` AS JoinDate, b.`TotalAmountAttached`, fi.`FileSlug`
                FROM `users` u
                LEFT JOIN `accounts` a ON a.`UserID` = u.`UserID`
                LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
                LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
                LEFT JOIN `bookings` b ON b.`UserID` = u.`UserID` AND b.`BookingStatus` = 'Approved'
                WHERE u.`Level` IN( 4, 7 ) AND b.`TotalAmountAttached` IS NULL AND u.`ReferrerUserID` = $userinfo->UserID";
        }

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;         
        }
        unset( $query );
        
        return $data;
    }

    /**
     * Get own referral booking
     * @access Private
     * @param (int) $investmentBookingID    : (required) affiliates booking id
     * @return Array
     **/
    function getReferralBooking( $investmentBookingID )
    {
        $sql = "SELECT DISTINCT
            b.`InvestmentBookingID`,
            b.`UserID` AS InvestorUserID,
            u_inv.`ReferrerUserID`,
            cp_ref.`ClientProfileID` AS ReferrerClientID,
            CONCAT( um_inv.`FirstName`, ' ', um_inv.`LastName` ) AS InvestorName,
            u_inv.`DateAdded` AS JoinDate,
            ur_ref.`CommissionRate`,
            b.`TotalAmountAttached` AS InvestmentAmount, 
            FORMAT( b.`TotalAmountAttached` * ( ur_cli.`SubscriptionRate` / 100 ), 2 ) AS `SOCommission`,
            b.`CreatedAt` AS BookingDate,
            cp_cli.`UserID` AS InvestedUserID,
            cp_cli.`ClientProfileID` AS InvestedClientID,
            cp_cli.`CompanyName` AS InvestedCompany
        FROM `bookings` b
        LEFT JOIN `users` u_inv ON u_inv.`UserID` = b.`UserID`
        LEFT JOIN `user_meta` um_inv ON um_inv.`UserID` = b.`UserID`
        LEFT JOIN `client_profiles` cp_cli ON cp_cli.`ClientProfileID` = b.`ClientProfileID` 
        LEFT JOIN `client_profiles` cp_ref ON cp_ref.`UserID` = u_inv.`ReferrerUserID` 
        LEFT JOIN `user_referrer` ur_cli ON ur_cli.`UserID` = cp_cli.`UserID`
        LEFT JOIN `user_referrer` ur_ref ON ur_ref.`UserID` = u_inv.`ReferrerUserID`
        WHERE b.`InvestmentBookingID` = $investmentBookingID";

        $data = $this->db->get_row( $sql );
        
        return $data;
    }

    /**
     * Get own commission rate
     * @access Private
     * @param (array) $userinfo    : (required) user info
     * @return Array
     **/
    function getCommissionRateOwn( $userinfo )
    {
        $sql = "SELECT DISTINCT `CommissionRate` FROM `user_referrer` WHERE `UserID` = $userinfo->UserID";

        $data = $this->db->get_row( $sql );
        
        return $data;
    }

    /**
     * Get monthly earning
     * 
     * @access private
     * @param (int) $year   : optional: default [10]    : sets the data row limit
     * @return contents
     *
     */
    function getMReferralEarningMonthly( $year = false )
    {
        $year = $year ? $year : 'YEAR( CURDATE() )';

        $sql = 'SELECT DATE_FORMAT( `CreatedAt`, "%M" ) Months, SUM( `ReferralCommission` ) TotalReferralCommission 
        FROM `referral_earnings`
        WHERE YEAR( `CreatedAt` ) = '.$year.'
        GROUP BY Months';

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }
    
    function getTrashedUsers()
    {
        return $this->getUsers('yes');
    }
    
    function getUserLevels()
    {
        $sql = "SELECT UserLevelID,Name FROM user_levels";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserLevelID] = $row->Name;			
        }
        unset($query);
        
        return $data;
    }
    
    function getCapabilities()
    {
        // WHERE uc.UserCapabilityGroupID > 0
        $data = array();
        $sql = "SELECT uc.*, ucg.Name as GroupName FROM user_capabilities uc LEFT JOIN user_capability_groups ucg ON uc.UserCapabilityGroupID = ucg.UserCapabilityGroupID ORDER BY ucg.UserCapabilityGroupID ASC";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->GroupName][$row->UserCapabilityID] = $row;
        }
        unset($query);
        
        return $data;
    }

    /**
     * Get active projects
     * 
     * @access private
     * @param $userID (int)   : optional: default [false]    : sets booked count and registered interest count
     * @return array of objects
     */
    function getActiveProjects( $userID = false )
    {
        $select = "";
        // Identify of currently logged in seeder is already invested of registered interest in the project
        if($userID) {
            $select = "
                (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID = cp.ClientProfileID AND BookingType='Booking' AND UserID = $userID ) AS CurrentUserBookedCount, 
                (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID = cp.ClientProfileID AND BookingType='Interest' AND UserID = $userID ) AS CurrentUserRegisterCount,";
        }        

        $sql = "SELECT $select cp.*, um.*, cc.ClientCatID, cc.CCatName, timestampdiff( DAY, NOW(), cp.`OfferClosing` ) AS DaysLeft, timestampdiff( DAY, NOW(), cp.`OfferOpening` ) AS DaysToOpen,
            (SELECT SUM(TotalAmountAttached) FROM bookings WHERE ClientProfileID=cp.ClientProfileID AND BookingType='Booking') AS TotalBookingAmounts,
            (SELECT SUM(TotalAmountAttached) FROM bookings WHERE ClientProfileID=cp.ClientProfileID AND BookingType='Booking' AND BookingStatus='Approved') AS TotalRaisedAmt,
            (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID=cp.ClientProfileID AND BookingStatus='Approved') AS TotalInvestor
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            JOIN client_profiles cp ON u.UserID = cp.UserID 
            LEFT JOIN client_categories cc ON cc.ClientCatID = cp.ClientCatID 
            WHERE cp.Status = 'Approved' AND cp.OfferClosing >= NOW()
            ORDER BY cp.OfferOpening DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }

        unset($query);

        return $data;
    }

    /**
     * Get recently viewed  page
     * 
     * @access private
     * @param $limit (int)   : optional: default [10]    : sets the data row limit
     * @param $all (boolean) : optional: default [false] : if false, all approved projects only
     * @return contents
     */
    function getRecentlyViewedProjects( $limit = 10, $all = false )
    {
        $where = ' WHERE cp.OfferClosing >= NOW() ';
        if( !$all ){
            $where .= 'AND cp.Status = "Approved" ';
        }

        $sql = 'SELECT cp.*, um.*,timestampdiff(DAY, NOW(), cp.`OfferClosing`) as DaysLeft, cc.ClientCatID, cc.CCatName,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingType="Booking") as TotalBookingAmounts,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingType="Booking" AND bb.BookingStatus="Approved") as TotalRaisedAmt,
            (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus="Approved") as TotalInvestor, "" AS Data
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            JOIN client_profiles cp ON u.UserID = cp.UserID 
            LEFT JOIN client_categories cc ON cc.ClientCatID = cp.ClientCatID' . $where . '
            ORDER BY cp.LastViewed DESC LIMIT '. $limit;

        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function updatePersonalInformation()
    {
        $userID = false;
        if(isset($this->post['action']) && $this->post['action'] == 'updatepersonal') {
            $this->setSession('message',"Member information has been updated!");                            

            $data = $this->post;                    
            $uwhere = array('UserID' => $this->post['userid']);
            $mwhere = array('UserMetaID' => $this->post['metaid']);

            if(isset($this->post['user'])) {
                $user = $this->post['user'];
            }
            if(isset($this->post['meta'])) {
                $meta = $this->post['meta'];
            }

            unset($data['action']);

            if(isset($this->post['user'])) {
                $userID = $this->db->update("users", $user, $uwhere);
            }

            if(isset($this->post['meta'])) {
                $metaID = $this->db->update("user_meta", $meta, $mwhere);
            }

            if(isset($this->file['Avatar']) && $this->file['Avatar']['name'] != '') {
                $filedata = $this->fileUpload($this->file,$this->post['userid']);
                if(count($filedata)) {
                    $this->removeUploadedFiles($this->post['avatarid']);
                    $rowCount = $this->db->update("user_meta", $filedata, $mwhere);
                }
            }

            $this->updateUserInfo();
            //Lang::init();
            
            App::activityLog(User::info('FirstName')." ".User::info('LastName')." updated his personal info REFID#".$this->post['userid'].'.');
        }
        
        return $userID;        
    }

    function updateProfileInformation()
    {
        $userID = false;
        if( isset( $this->post['action'] ) && $this->post['action'] == 'updateprofile' ){
            $data = $this->post;
            $status = $data['status'];
            $uwhere = array( 'UserID' => $data['userid'] );
            $mwhere = array( 'UserMetaID' => $data['metaid'] );
            $awhere = array( 'AccountID' => $data['acctid'] );

            unset($data['action']);

            if( isset( $data['user'] ) ){
                $user = $data['user'];
                $userID = $this->db->update( "users", $user, $uwhere );
            }

            if( isset( $data['meta'] ) ){
                $meta = $data['meta'];
                $metaID = $this->db->update( "user_meta", $meta, $mwhere );
            }

            if( isset( $data['acct'] ) ){
                $acct = $data['acct'];
                $metaID = $this->db->update( "accounts", $acct, $awhere );
            }
            
            // save file data for seeder profile
            if( isset( $this->file ) ){
                if( $status != 'Approved' || User::can( 'Manage Profiles' ) ){
                    $filedata = $this->fileUpload( $this->file, $data['userid'] );
                    if( count( $filedata ) ){
                        if( $this->file['IdPhoto']['name'] != '' && $this->file['AddressPhoto']['name'] != '' ){
                            $filedata['Status'] = 'Verification';
                        } elseif( $this->file['IdPhoto']['name'] != '' && $data['AddressPhotoID'] > 0 ){
                            $filedata['Status'] = 'Verification';
                        } elseif( $this->file['AddressPhoto']['name'] != '' && $data['IdPhotoID'] > 0 ){
                            $filedata['Status'] = 'Verification';
                        }                
                        $rowCount = $this->db->update( "accounts", $filedata, $awhere );
                    }
                }
            }

            $this->updateUserInfo();
            
            $this->setSession( 'message', "Your profile details have been updated!" );
            App::activityLog( User::info('FirstName')." ".User::info('LastName')." updated his profile info REFID#".$data['userid']."." );
        }
        
        return $userID;
    }

    function saveProfileUpdateRequest()
    {
        $userID = false;
        if( isset( $this->post['action'] ) && $this->post['action'] == 'saveupdaterequest' ){
            $data = $this->post;
            $userID = $data['UserID'];
            $requestID = $data['RequestID'];
            $proofID = $data['AddressPhoto'] > 0 ? $data['AddressPhoto'] : '0'; // current address photo
            $requestAvatar = $data['RequestAvatar'] > 0 ? $data['RequestAvatar'] : false;
            $where = array( 'RequestID' => $requestID );

            if( isset( $data['request'] ) ){
                $request = $data['request'];
                $request['UserID'] = $userID;
                $request['ProofID'] = $proofID;
                
                if( $requestID > 0 ){
                    $this->db->update( "accounts_request", $request, $where );
                } else {
                    $requestID = $this->db->insert( "accounts_request", $request );
                }

                $this->setSession( "message", "Your profile update request has been sent!" );
                App::activityLog( User::info('FirstName')." ".User::info('LastName')." requested for profile update REQID#".$requestID."." );
            }

            // save file data for seeder profile
            if( isset( $this->file ) ){
                $filedata = $this->fileUpload( $this->file, $userID );

                if( count( $filedata ) ){              
                    $rowCount = $this->db->update( "accounts_request", $filedata, $where );
                }
            }
        }
    }

    function updateLoginInformation()
    {
        $userID = false;

        if(isset($this->post['action']) && $this->post['action'] == 'savepassword') {
            if($this->decrypt($this->post['OldPassword'],User::info('Password'))) {
                $where = array('UserID' => $this->post['userid']);
                if($this->post['NewPassword'] == $this->post['NewPasswordConfirm']) {
                    $data = array('Password' => $this->encrypt($this->post['NewPasswordConfirm']), 'HashKey' => self::enchash($this->post['NewPasswordConfirm']));
                    $this->db->update("users", $data, $where);  
                    $this->setSession('message', 'Change Password sucessful!');
                } else {
                    $this->setSession('error', 'Password mismatched!');
                }
            } else {
                $this->setSession('error', 'Invalid password!');
            }

            $this->updateUserInfo();
            
            App::activityLog(User::info('FirstName')." ".User::info('LastName')." updated his password REFID#".$this->post['userid'].'.');
        }
        
        return $userID;
    }

    function doSignupUser()
    {
        if(isset($this->post['action']) && $this->post['action'] == 'signupuser') {
            $data = $this->post;                    
            $user = $this->post['user'];
            $meta = $this->post['meta'];
            $acct = $this->post['acct'];

            unset($data['action']);
            
            $user['Active'] = 1;
            // $user['HashKey'] = base64_encode($user['Password']);
            $user['HashKey'] = self::enchash( $user['Password'] );
            $user['Password'] = $this->encrypt($user['Password']);
            $user['DateAdded'] = date('Y-m-d H:i:s');
            $user['Level'] = 7;
            $user['Capability']=Level::info('Capability',isset($user['Level']) ? $user['Level'] : Option::get('new_user_role'));

            $affiliateUserID = ($this->getCookie(Config::get('AFFILIATEKEY'))) ? $this->getCookie(Config::get('AFFILIATEKEY')) : 0;
            $affiliateUserID = (User::info('UserID',$affiliateUserID) != '') ? $affiliateUserID : 0;
            $user['ReferrerUserID'] = $affiliateUserID;
            
            if(User::infoByEmail('UserID',$user['Email'])) {
                $this->setSession('error', "Email already exists");
                $this->setSession('message',"");
                View::redirect('users/signup');
            }

            $userID = $this->db->insert("users", $user);

            if($userID) {
                $this->setSession('message',"Thank you for joining the SeedOmega platform! To activate your account, please check for a confirmation email from SeedOmega.");

                $where = array('UserID' => $userID);

                // TO DO: send email
                $meta['UserID'] = $userID;
                $metaID = $this->db->insert("user_meta", $meta);

                $acct['UserID'] = $userID;
                $acct['Status'] = 'Incomplete';
                $acctID = $this->db->insert("accounts", $acct);

                $this->doSaveReferrer( $userID, $affiliateUserID ); //save to user_referrer table

                $awhere = array('AccountID' => $acctID);
                $filedata = $this->fileUpload($this->file,$userID);
                if(count($filedata)) {
                    if($this->file['IdPhoto']['name'] != '' && $this->file['AddressPhoto']['name'] != ''){
                        $filedata['Status'] = 'Verification';
                    }
                    $rowCount = $this->db->update("accounts", $filedata, $awhere);
                }

                return $userID;                
            }
            
            App::activityLog("New user added REFID#".$userID.'.', $userID);
        }
    }
    
    function doSave( $userinfo = false )
    {
        $userID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "updateuser": {
                    $data = $this->post;
                    $userID = $data['userid'];
                    $status = $data['status'];
                    $uwhere = array( 'UserID' => $userID );
                    $mwhere = array( 'UserMetaID' => $data['metaid'] );

                    if( isset( $data['user'] ) ){
                        $user = $data['user'];
                        $clientID = $user['ClientID'];
                        unset( $user['ClientID'] );
                    }
                    if( isset( $data['meta'] ) ){
                        $meta = $data['meta'];
                    }

                    unset( $data['action'] );

                    if( isset( $data['Password'] ) && $data['Password'] != '' ){
                        $pass = $this->encrypt( $data['Password'] );
                        $user['Password'] = $pass;
                        $user['HashKey'] = self::enchash( $data['Password'] );
                    }

                    if( isset( $data['capabilities'] ) && count( $data['capabilities'] ) ){
                        $user['Capability'] = $this->arrayToString( $data['capabilities'] );
                    }

                    if( isset( $data['user'] ) ){
                        if( $user['Level'] == 6 ){
                            $user['ReferrerUserID'] = $clientID;
                            $this->doSaveReferrer( $userID, $clientID, true );
                        } else {
                            if( !isset( $user['ReferrerUserID'] ) ){
                                $user['ReferrerUserID'] = 0;
                                $this->doSaveReferrer( $userID, 0, true );
                            }
                        }
                        $userID = $this->db->update( "users", $user, $uwhere );
                    }
                    if ( isset( $data['meta'] ) ){
                        $metaID = $this->db->update("user_meta", $meta, $mwhere);
                    }

                    if ( isset( $data['metaCorp'] ) ){
                        $metaCorp = $data['metaCorp'];
                        $mCorpwhere = array( 'UserMetaCorpID' => $data['metacorpid']);
                        $this->db->update( "user_meta_corporate", $metaCorp, $mCorpwhere);
                    }

                    if( isset( $this->file['Avatar']) && $this->file['Avatar']['name'] != '' ){
                        $filedata = $this->fileUpload( $this->file, $userID );
                        if( count( $filedata ) ){
                            $this->removeUploadedFiles( $data['avatarid']);
                            $rowCount = $this->db->update( "user_meta", $filedata, $mwhere );
                        }
                    }

                    $this->updateUserInfo();
                    Lang::init();            

                    $this->setSession( 'message',"User has been updated!" );
                    App::activityLog( "Updated User #".$userID.'.' );
                } break;

                case "adduser": {
                    $data = $this->post;                    
                    $user = $this->post['user'];
                    $meta = $this->post['meta'];
                    $client = $this->post['client'];

                    $clientID = $user['ClientID'];

                    unset( $data['action'] );
                    unset( $user['ClientID'] );

                    // for Manager & Assistant, ReferrerUserID equal to the selected client 
                    if( $user['Level'] == 6 ){
                        $user['ReferrerUserID'] = $clientID;
                    }

                    $pw = $this->generatePw( 10 );
                    $user['Password'] = $this->encrypt( $pw );
                    $user['Active'] = 1;
                    // $user['HashKey'] = md5( microtime() );
                    // $user['HashKey'] = base64_encode( $pw );
                    $user['HashKey'] = self::enchash( $pw );
                    $user['DateAdded'] = date( 'Y-m-d H:i:s' );
                    $user['Capability'] = Level::info( 'Capability', isset( $user['Level'] ) ? $user['Level'] : Option::get( 'new_user_role' ) );

                    if( User::infoByEmail( 'UserID', $user['Email'] ) ){
                        $this->setSession( 'error', "Email already exists" );
                        $this->setSession( 'message', "" );
                        View::redirect( 'users/add' );
                    }

                    $userID = $this->db->insert( "users", $user );

                    if( $userID && $user['Level'] == 3 ) { //customer service, create user in support db
                        try {
                            $db2 = new PDO( 'mysql:dbname=seedomeg_support;host=103.231.252.229', 'seedomeg_portal', 'PortalQWE!@#4' );
                            $sql = "INSERT INTO staff( level, send_email_on_new_ticket, name, email,password ) VALUES( ?, ?, ?, ?, ? )";
                            $db2->prepare( $sql )->execute( [3, 1, $meta['FirstName'], $user['Email'],$user['Password'], ] );
                        } catch( PDOException $ex ){
                            echo 'Connection to Support DB failed: '.$ex->getMessage();
                        }
                    }

                    if( $userID ){
                        $this->setSession('message',"New user has been added!");

                        $where = array('UserID' => $userID);

                        // TO DO: send email
                        //$filedata = $this->sendEmail($userID);
                        $meta['UserID'] = $userID;
                        $metaID = $this->db->insert( "user_meta", $meta );

                        //add user client profile
                        if( $user['Level'] == 4 ){
                            $client['UserID'] = $userID;
                            if( isset( $client['Video'] ) ){
                                $vid = trim( $client['Video'] );
                                if( substr( $vid, 0, 7 ) != '<iframe' ){
                                    $client['Video'] = '<iframe width="560" height="315" src="'.$vid.'" frameborder="0" allow="autoplay;encrypted-media" allowfullscreen></iframe>';
                                }
                                header('X-XSS-Protection:0');
                            }

                            if( isset( $client['Price'] ) ){
                                $client['Price'] = (float) $client['Price'] == 0.00 ? '1' : $client['Price'];
                            }

                            foreach( $client as $key => $value ){
                                if( $key != 'Video' ){
                                    $temp = AppUtility::sanitizeEndingBRTags( $value ); // remove first ending <br> tags
                                    $client[$key] = AppUtility::sanitizeEndingPTags( $temp ); // remove ending empty <p> tags
                                }
                            }
                            $clientID = $this->db->insert( "client_profiles", $client );
                            App::activityLog("Added Client #".$clientID.'.');
                        }

                        //add user investor profile
                        if( $user['Level'] == 7 ){
                            $acct['UserID'] = $userID;
                            $acct['Status'] = 'Incomplete';
                            $acctID = $this->db->insert("accounts", $acct);
                        }

                        $this->doSaveReferrer( $userID, isset( $clientID ) ? $clientID : 100000 ); //save to user_referrer table

                        if( $this->file['Avatar']['name'] != '' ){
                            $filedata = $this->fileUpload($this->file,$userID);
                            if( count( $filedata ) ){
                                $rowCount = $this->db->update( "user_meta", $filedata, $where );
                            }
                        }
                        App::activityLog("Added User #".$userID.'.');
                    }                    
                } break;

                case "updateprofile": {
                    $this->updateProfileInformation();

                    $btnSubmit = $this->post['btnsubmit'];
                    if ($btnSubmit == 'savenextdetails'){
                        View::redirect('/users/profile/details');
                    }
                    if ($btnSubmit == 'savenextmoreabout'){
                        View::redirect('/users/profile/more-about-me');
                    }
                    if ($btnSubmit == 'savenextother'){
                        View::redirect('/users/profile/other');
                    }
                } break;

                case "approveseederchangerequest": {
                    $data = $this->post;
                    $userID = isset( $data['UserID'] ) ? $data['UserID'] : false;
                    if( $userID ){
                        $userinfo = User::info( false, $userID ); // Load target User info
                        $request = self::getProfileChangeRequest( $userID ); // get change request profile info
                        if( $request != NULL ){
                            $where = array( 'UserID' => $userID );
                            $rwhere = array( 'RequestID' => isset( $request->RequestID ) ? $request->RequestID : 0 );
                            $avatar = isset( $request->Avatar ) && $request->Avatar > 0 ? $request->Avatar : false;
                            $addressPhoto = isset( $request->AddressPhoto ) && $request->AddressPhoto > 0 ? $request->AddressPhoto : false;

                            $meta['FirstName'] = isset( $request->FirstName ) ? $request->FirstName : '';
                            $meta['LastName'] = isset( $request->LastName ) ? $request->LastName : '';
                            $meta['Address'] = isset( $request->Address ) ? $request->Address : '';
                            $meta['City'] = isset( $request->City ) ? $request->City : '';
                            $meta['State'] = isset( $request->State ) ? $request->State : '';
                            $meta['Country'] = isset( $request->Country ) ? $request->Country : '';
                            $meta['PostalCode'] = isset( $request->PostalCode ) ? $request->PostalCode : '';
                            if( $avatar ){
                                $meta['Avatar'] = $avatar;
                                AppUtility::deleteFileAndAssociates( $userinfo->Avatar ); // delete old avatar
                            }

                            if( $addressPhoto ){
                                $accounts['AddressPhoto'] = $addressPhoto;
                                $this->db->update( 'accounts', $accounts, $where ); // update Seeder's account info
                                AppUtility::deleteFileAndAssociates( $request->ProofID ); // delete old address photo
                            }

                            $this->db->update( 'user_meta', $meta, $where ); // update Seeder's meta info
                            $this->db->delete( 'accounts_request', $rwhere ); // delete requested profile data

                            $this->setSession( "message", "Approved the Seeder's profile change request!" );
                            App::activityLog( "Seeder's profile change request has been approved. REFID#".$userID."." );
                        }
                    }
                    if( User::is( 'Customer Service' ) ){
                        View::redirect( 'cs/investorprofiles' );
                    } else {
                        View::redirect( false, true );
                    }
                } break;

                case "updatepersonal": {
                    $this->updatePersonalInformation();
                } break;
                
                case "savepassword" : {
                    $this->updateLoginInformation();
                } break;

                case "saveupdaterequest": {
                    $this->saveProfileUpdateRequest();
                } break;
            } 
        }
        
        return $userID;        
    }

    // function konektado( $q, $ar ){
    //     $hash = self::dechash( $ar->HashKey );
    //     if( $q != $hash ){
    //         $newhash = self::enchash( $q );
    //         $this->db->update( 'users', array( 'HashKey' => $newhash ), array( 'UserID' => $ar->UserID ) );
    //         App::activityLog( "User's hashkey has been updated. ID#".$ar->UserID."." );
    //     }
    //     return true;
    // }
    
    function doSaveWithdraw()
    {
        $earningID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "withdraw": {
                    $data = $this->post;
                    $iIDs = $data['iIDs'];

                    unset( $data['action'] );

                    if( isset( $iIDs ) && $iIDs ){
                        $totalcomm = 0;
                        foreach( $iIDs as $iID ){
                            $bookeddata = $this->getReferralBooking( $iID );
                            if( isset( $bookeddata ) ){
                                $refcomm = AppUtility::computeCommission( $bookeddata->SOCommission, $bookeddata->CommissionRate );

                                $withdraw = array() ;
                                $refcomm = str_replace( ',', '', $refcomm );
                                $SOCommission = str_replace( ',', '', $bookeddata->SOCommission );
                                $CommissionRate = str_replace( ',', '', $bookeddata->CommissionRate );
                                $totalcomm += floatval( $refcomm );

                                $withdraw['InvestmentBookingID'] = $bookeddata->InvestmentBookingID;
                                $withdraw['InvestorUserID'] = $bookeddata->InvestorUserID;
                                $withdraw['CommissionRate'] = number_format( $CommissionRate, 2, "." ,"" );
                                $withdraw['SOCommission'] = number_format( $SOCommission, 2, "." ,"" );
                                $withdraw['BookingApprovalAt'] = $bookeddata->BookingDate;
                                $withdraw['ReferralCommission'] = number_format( $refcomm, 2, "." ,"" );
                                $withdraw['Status'] = 'Pending';

                                $earningID = $this->db->insert( "referral_earnings", $withdraw );
                            }
                        }
                    }

                    $this->setSession( 'message', "Your withdrawal request with a total amount of ($".number_format( $totalcomm, 2, "." ,"" ).") has been submitted!" );
                    App::activityLog( "Commission withdrawal with InvestmentBookingIDs (".implode( ', ', $iIDs ).") and total amount of ($".number_format( $totalcomm, 2, "." ,"" ).") has been submitted." );
                } break;

                default: break;        
            }
        }
        
        return $earningID;        
    }

    /**
     * Get seeder profile info
     *
     * @package
     * @access Private
     * @param (int)     $userID  : (required) : seeder's user id
     * @param (boolean) $multi   : (optional) : true if multiple documents will be included
     * @param (string)  $keyword : (optional) : keyword to search for document name
     * @return (object)
     **/
    function getProfile( $userID, $multi = false, $keyword = false )
    {
        $data = [];
        if( $multi ){
            $where = $keyword ? " AND fg.`DocumentName` LIKE '%$keyword%' " : "";

            $sql = "SELECT a.*, fg.`FileGroupID`, fg.`FileID`, fg.`DocumentName`
            FROM `accounts` a
            LEFT JOIN `file_groups` fg ON fg.`ReferenceID` = a.`UserID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = fg.`FileID`
            WHERE a.`UserID` = $userID AND (fi.`FileID` = fg.`FileID` OR (fi.`FileID` IS NULL AND fg.`FileID` IS NULL)) $where
            ORDER BY fg.`FileGroupID`";
            
            $query = &$this->db->prepare( $sql );
            $query->execute();
            while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                $data[] = $row;
            }
            unset( $query );

        } else {
            $sql = "SELECT * FROM accounts WHERE UserID = '".$userID."' LIMIT 1";
            $data = $this->db->get_row( $sql );
        }

        return $data;
    }

    /**
     * Get seeder change request profile info
     *
     * @package
     * @access Private
     * @param (int) $userID  : (required) : seeder's user id
     * @return (object)
     **/
    function getProfileChangeRequest( $userID )
    {
        $data = [];
        $sql = "SELECT * FROM `accounts_request` WHERE `UserID` = '".$userID."' LIMIT 1";

        // $query = &$this->db->prepare( $sql );
        // $query->execute();
        // while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
        //     $data[] = $row;
        // }
        // unset( $query );
        
        $data = $this->db->get_row( $sql );

        return $data;
    }

    /**
     * Get approved client list
     * @access Private
     * @return data array
     */
    function getClientList()
    {
        $data = array();

        $sql = "SELECT DISTINCT cp.`UserID`, CONCAT(um.`FirstName`, ' ', um.`LastName`, ' (', cp.`CompanyName`, ')') AS Client 
            FROM `client_profiles` cp
            LEFT JOIN `user_meta` um ON um.`UserID` = cp.`UserID`
            WHERE `Status` = 'Approved' ORDER BY Client;";

        $query = &$this->db->prepare($sql);
        $query->execute();

        //data[0] = '-- select --';
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[$row->UserID] = $row->Client;
        }

        unset( $query );
        
        return $data;
    }

    /*
     * Function to set Cookie if URL string referral code exists
     * @param $refCode string
     * */
    function setCookieForReferral($refCode)
    {
        if ($refCode){
            //set to expire after 6 months
            $expiration = time() + (86400 * 180);
            setcookie('seedOmega_ref', $refCode , $expiration);
        }
    }

    /*
     * save Referrer for new Users if cookie is found
     * */
    function saveReferrer()
    {
        if (isset($_COOKIE['seedOmega_ref'])) {

            $sql = "SELECT UserID FROM users WHERE ReferrerUserID = '".$_COOKIE['seedOmega_ref']."' LIMIT 1";
            $userdata = $this->db->get_row($sql);

            if ($userdata) {
                $refUserId = $userdata->UserID;

                $user['UserID'] = User::info('UserID');
                $user['ReferrerUserID'] = $refUserId;
                $this->db->insert("user_referrer", $user);
            }
        }
    }
    
    /**
     * Delete previous requested profile change
     *
     * @package
     * @access Private
     * @param (int) $UserID : (required) : target user id to be deleted
     * @return NA
     **/
    function doDeleteProfileRequest( $UserID )
    {
        $where = array( 'UserID' => $UserID );
        $this->db->delete( "accounts_request", $where );
        
        App::activityLog( "Deleted profile change requested by User #".$UserID."." );
    }
    
    function doTrash($UserID)
    {
        $where = array('UserID' => $UserID);
        $data = array('Active' => 0);
        $rowCount = $this->db->update("users",$data, $where);
        $rowCount = $this->db->update("accounts",$data, $where);        
        
        App::activityLog("Trashed User #".$UserID.'.');
    }
    
    function doRestore($UserID)
    {
        $where = array('UserID' => $UserID);
        $data = array('Active' => 1);
        $rowCount = $this->db->update("users",$data, $where);
        $rowCount = $this->db->update("accounts",$data, $where);
        
        App::activityLog("Restored User #".$UserID.'.');
    }
    
    function doDelete( $UserID )
    {
        $where = array( 'UserID' => $UserID );
        $this->db->delete( "users", $where );
        $this->db->delete( "user_meta", $where );
        $this->db->delete( "accounts", $where );
        $this->db->delete( "account_meta", $where );
        $this->db->delete( "bank_accounts", $where );
        $this->db->delete( "user_referrer", $where );
        AppUtility::deleteFilesOnDeleteUser( $UserID );
        
        App::activityLog( "Deleted all data and associated files of user #".$UserID."." );
    }
    
    function doEmptyTrash( $ids )
    {
        foreach( $ids as $id ){
            self::doDelete( $id );
        }
        
        App::activityLog( "Emptied trashed users" );
    }

    function doLogout()
    {
        unset($_SESSION[SESSIONCODE]);
        session_destroy();
        View::redirect();
    }
}