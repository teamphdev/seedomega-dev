<?php
class Admin_model extends Model
{
    public function __construct()
    {
        parent::__construct();
        
        View::$segments = $this->segment;
    }
	
	function getActivityLogs()
    {
        $sql = "SELECT * FROM activity_logs ORDER BY ActivityDate DESC";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
		
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);
        
        return $data;		
    } 

    function getUserLevels()
    {
        $sql = "SELECT UserLevelID,Name FROM user_levels";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserLevelID] = $row->Name;			
        }
        unset($query);
        
        return $data;
    }

    function getOptionGroups()
    {
        $sql = "SELECT OptionGroupID, GroupName FROM option_groups";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->OptionGroupID] = $row->GroupName;          
        }
        unset($query);
        
        return $data;
    }

    function getOptionsList()
    {
        $sql = "SELECT o.*,og.GroupName FROM options o LEFT JOIN option_groups og ON o.OptionGroupID = og.OptionGroupID";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);
        
        return $data;
    }

    function getOption($ID)
    {
        $sql = "SELECT * FROM options o LEFT JOIN option_groups og ON o.OptionGroupID = og.OptionGroupID WHERE o.OptionID = ".$ID." LIMIT 1";
        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function doSave()
    {
        if(isset($this->post['action'])) {
            switch($this->post['action']) {
                case "updateoptions": {
                    if(isset($this->post['action'])) {
                        switch($this->post['action']) {
                            case "updateoptions": {

                                $this->setSession('message',"Options has been updated!"); 

                                $settings = $this->post['settings'];
                                // $settings['Redirects'] = $this->arrayToString($settings['Redirects']);

                                $optdata = $settings;

                                $filedata = $this->fileUpload($this->file,$this->post['userid']);
                                if(count($filedata)) {
                                    $optdata = array_merge($settings,$filedata);
                                }

                                Option::multiUpdate($optdata);                                        
                            }
                        }
                    }
                    return (object) $this->post;
                } break;
                case "addoption": {

                    $data = $this->post;
                    unset($data['action']);

                    $optionID = $this->db->insert("options", $data);

                    if($optionID) {
                        $this->setSession('message',"New product item has been registered!");
                    }

                    //View::redirect('agency');
                } break;
                case "editoption": {
                    $optionID = $this->post['optionID'];
                    $data = $this->post;
                    unset($data['action']);
                    unset($data['optionID']);

                    $this->setSession('message',"Option has been updated!");

                    $opID = $this->db->update("options", $data, array('OptionID' => $optionID));

                } break;
            }
        }
        return (object) $this->post;

    }

    function doDeleteOption($ID)
    {
        $where = array('OptionID' => $ID);        
        $this->setSession('message',"Option has been deleted!");        
        $rowCount = $this->db->delete("options", $where);
    }
}