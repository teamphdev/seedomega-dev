<?php
class Manage_model extends Model 
{
    // Investor Role ID
    var $userLevelID = 7;

    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }

    function getUsers()
    {
        $sql = "SELECT u.*,um.*,ul.Name as UserLevel,ul.Code FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserLevel][] = $row;         
        }
        unset($query);
        
        return $data;
    } 

    function getUser($ID)
    {
        $sql = "SELECT u.*,um.*,ul.Name,ul.Code
            FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE u.UserID = ".$ID." LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    }

    function getClientData($ID)
    {
        $sql = "SELECT cp.* FROM client_profiles cp WHERE cp.ClientProfileID = ".$ID;

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getClientDataByUserID( $ID )
    {
        $sql = "SELECT cp.* FROM `client_profiles` cp WHERE cp.`UserID` = ".$ID;

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getAllClients( $status = false )
    {
        $where = "";
        if( $status ){
            $where = " WHERE cp.`Status` = '$status' ";
        }
        $sql = "SELECT cp.*, um.FirstName, um.LastName, 
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingType = 'Booking') as TotalBookingAmounts,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingType = 'Booking' AND bb.BookingStatus='Approved') as TotalRaisedAmt,
            (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus='Approved') as TotalInvestor   
        FROM client_profiles cp            
        LEFT JOIN user_meta um ON um.UserID = cp.UserID
        $where";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);
        
        return $data;
    }

    function getAllInvestorsByStatus()
    {
        $sql = "SELECT u.*, a.AccountID,a.AccountType,a.IdPhoto, a.AddressPhoto,a.Status, um.FirstName, um.LastName, um.Avatar
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            LEFT JOIN accounts a ON a.UserID = u.UserID 
            WHERE u.Level = ".$this->userLevelID." AND u.Active = 1 ORDER BY u.DateAdded DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->Status][] = $row;
        }
        unset($query);
        
        return $data;
    }

    function getAllInvestors($limit = '')
    {
        $sql = "SELECT u.*, a.AccountID,a.AccountType,a.IdPhoto, a.AddressPhoto,a.Status, um.FirstName, um.LastName, um.Avatar 
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            LEFT JOIN accounts a ON a.UserID = u.UserID 
            WHERE u.Level = ".$this->userLevelID." AND u.Active = 1 ORDER BY u.DateAdded DESC";

        if($limit){
            $sql .= " LIMIT ".$limit;
        }

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getAllTrashUsers()
    {
        $sql = "SELECT u.*, a.AccountID,a.AccountType,a.IdPhoto, a.AddressPhoto,a.Status, um.FirstName, um.LastName, um.Avatar 
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            LEFT JOIN accounts a ON a.UserID = u.UserID 
            WHERE u.Level = ".$this->userLevelID." AND u.Active = 0 AND u.Level = ".$this->userLevelID."  ORDER BY u.DateAdded DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }
       
    function getBookedData($ID)
    {
        $sql = "SELECT b.*, cp.*, 
        (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingStatus = 'Approved') as TotalRaisedAmt,
        (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingStatus = 'Approved') as TotalInvestor 
            FROM bookings b 
            LEFT JOIN client_profiles cp ON b.ClientProfileID = cp.ClientProfileID 
        WHERE b.InvestmentBookingID = ".$ID;

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getUserLevels()
    {
        $sql = "SELECT UserLevelID,Name FROM user_levels";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserLevelID] = $row->Name;          
        }
        unset($query);
        
        return $data;
    }

    function getInvoiceTypes( $typeID = false )
    {
        $data = [];
        $where = $typeID ? "WHERE `InvoiceTypeID` = $typeID" : "";
        $sql = "SELECT DISTINCT * FROM invoice_type $where ORDER BY `Description`";

        if( $typeID ){
            $data = $this->db->get_row( $sql );
        } else {
            $query = &$this->db->prepare( $sql );
            $query->execute();
            while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                $data[] = $row;
            }
            unset( $query );
        }
        
        return $data;
    }
    
    function getCapabilities()
    {
        $data = array();
        $sql = "SELECT uc.*, ucg.Name as GroupName FROM user_capabilities uc LEFT JOIN user_capability_groups ucg ON uc.UserCapabilityGroupID = ucg.UserCapabilityGroupID ORDER BY ucg.UserCapabilityGroupID ASC";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->GroupName][$row->UserCapabilityID] = $row;
        }
        unset($query);
        
        return $data;
    }
    
    function getSliders( $sliderID = NULL)
    {
        $data = array();
        $where = '';

        if( $sliderID != NULL ){
            $where = ' WHERE s.`SlideID` = ' . $sliderID . ' ';
        }

        $sql = "SELECT DISTINCT s.*,cl.`CompanyName` FROM `slides` s LEFT JOIN `client_profiles` cl ON cl.`ClientProfileID` = s.`ClientProfileID`$where ORDER BY `Date` DESC;";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );
        
        return $data;
    }
    
    function getTestimonials( $testimonialID = NULL)
    {
        $data = array();
        $where = '';

        if( $testimonialID != NULL ){
            $where = ' WHERE TestimonialID = ' . $testimonialID . ' ';
        }

        $sql = "SELECT DISTINCT * FROM `testimonials`$where ORDER BY `Date` DESC;";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );
        
        return $data;
    }

    /**
     * Get core team member list
     * 
     * @package
     * @access private
     * @param (string) $ID : (required) : client user id
     * @return object
     **/
    function getTeams( $public = false )
    {
        $sql = "SELECT ct.* FROM `core_team` ct WHERE ct.`Active` = 1 ORDER By `Name`";
        if( $public ){
            $sql = "SELECT ct.*, fi.`FileSlug` FROM `core_team` ct LEFT JOIN `file_items` fi ON fi.`FileID` = ct.`Photo` WHERE ct.`Active` = 1 ORDER By `Name`";
        }

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get core team member list
     * 
     * @package
     * @access private
     * @param (string) $memberID : (required) : team member user id
     * @param (string) $public   : (optional) : user id
     * @return object
     **/
    function getTeamsByMemberID( $memberID, $public = false )
    {
        $where = " AND ct.`CoreTeamID` = $memberID ";
        $sql = "SELECT ct.* FROM `core_team` ct WHERE ct.`Active` = 1 $where ORDER By `Name`";
        if( $public ){
            $sql = "SELECT ct.*, fi.`FileSlug` FROM `core_team` ct LEFT JOIN `file_items` fi ON fi.`FileID` = ct.`Photo` WHERE ct.`Active` = 1 $where ORDER By `Name`";
        }

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get trashed team members
     * 
     * @package
     * @access private
     * @return object
     **/
    function getTrashedTeams()
    {
        $sql = "SELECT ct.*, fi.`FileSlug` FROM `core_team` ct LEFT JOIN `file_items` fi ON fi.`FileID` = ct.`Photo` WHERE ct.`Active` = 0 ORDER By `Name`";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }
    
    /**
     * Get all client and user
     * @access Private
     * @param $options [boolean] : indicates if the results will be use for select option values
     * @return data array
     */
    function getCompleteClientData( $options = false, $userID = NULL )
    {
        $data = [];
        $where = '';

        if( $userID != NULL ){ $where = " AND u.`UserID` = $userID "; }

        $sql = "SELECT DISTINCT u.*,um.*,ul.*,cp.*
            FROM `client_profiles` cp
            LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
            LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
            LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level`
            WHERE u.`Active` = 1 AND ul.`Code` = 'CLN' $where ORDER BY um.`FirstName`, um.`LastName`;";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        switch( $options ){
            case 'testimonial':
                $data[0] = '-- select --';
                while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                    $data[$row->UserID] = $row->FirstName.' '.$row->LastName;
                } break;

            case 'slider':
                $data[0] = '-- select --';
                while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                    $data[$row->ClientProfileID] = $row->CompanyName;
                } break;
            
            default:
                while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                    $data[] = $row;
                } break;
        }

        unset( $query );
        
        return $data;
    }
    
    function doSave()
    {
        $dataID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

               case "updatebooking": {
                    $data = $this->post;
                    $bookdata = $this->post['book'];
                    $uwhere = array( 'InvestmentBookingID' => $this->post['bookid'] );
                    $usrid = $this->post['usrid'];
                    unset($data['action']);

                    $bookeddata = $bookdata;
                    $filedata = $this->fileUpload( $this->file, $usrid );
                    if( count( $filedata ) ){
                        $bookeddata = array_merge( $bookeddata, $filedata );
                    }

                    $dataID = $this->db->update( "bookings", $bookeddata, $uwhere );
                    if( $dataID ){
                        $this->setSession('message',"Booking has been updated!");
                        App::activityLog("Updated Booking <a href='".View::url('users/booked')."/".$this->post['bookid']."'>#BC-ID-".$this->post['bookid']."</a>.");
                    }
                } break;

                case "book": {
                    $data = $this->post;
                    $book = $this->post['book'];
                    if( $book['SharesToApply'] == 0 ){
                        $this->setSession('error',"ERROR: Shares Should not be 0!");
                    } elseif( $book['TotalAmountAttached'] == 0 ){
                        $this->setSession( 'error', "ERROR: Total Application Amount Should not be 0!" );
                    } elseif( $data['minimumbid'] > $book['TotalAmountAttached'] ){
                        $this->setSession( 'error', "ERROR: The Minimum Bid is ".number_format( $data['minimumbid'] )."!");
                    } else {
                        $bookID = $this->db->insert( "bookings", $book );
                        if( $bookID ){
                            $this->setSession( 'message', "Booking has been successfully submitted!" );
                            App::activityLog( "New Booking #BC-ID-".$bookID."." );
                            View::redirect( 'bookings/thankyou/'.$bookID.'/' );
                        }
                    }
                } break;

                case "updateuser": {
                    $this->setSession( 'message', "User has been updated!" );

                    $data = $this->post;
                    $uwhere = array( 'UserID' => $this->post['userid'] );
                    $mwhere = array( 'UserMetaID' => $this->post['metaid'] );

                    if( isset( $this->post['user'] ) ){
                        $user = $this->post['user'];
                    }
                    if( isset( $this->post['meta'] ) ){
                        $meta = $this->post['meta'];
                    }

                    unset( $data['action'] );

                    if( isset( $this->post['Password'] ) && $this->post['Password'] != '' ){
                        $pass = $this->encrypt( $this->post['Password'] );
                        $user['Password'] = $pass;
                    }

                    if( isset( $this->post['capabilities'] ) && count( $this->post['capabilities'] ) ){
                        $user['Capability'] = $this->arrayToString( $this->post['capabilities'] );
                    }

                    if( isset( $this->post['user'] ) ){
                        $userID = $this->db->update( "users", $user, $uwhere );
                    }
                    if( isset( $this->post['meta'] ) ){
                        $metaID = $this->db->update( "user_meta", $meta, $mwhere );
                    }

                    if( isset( $this->file['Avatar'] ) && $this->file['Avatar']['name'] != '' ){
                        $filedata = $this->fileUpload( $this->file,$this->post['userid'] );
                        if( count( $filedata ) ){
                            $this->removeUploadedFiles( $this->post['avatarid'] );
                            $rowCount = $this->db->update( "user_meta", $filedata, $mwhere );
                        }
                    }
                    
                    App::activityLog( "Updated User #".$this->post['userid'].'.' );
                } break;

                case "adduser": {
                    $data = $this->post;                    
                    $user = $this->post['user'];
                    $meta = $this->post['meta'];
                    $client = $this->post['client'];

                    unset( $data['action'] );

                    $pw = $this->generatePw(10);
                    $user['Active'] = 1;
                    $user['Password'] = $this->encrypt($pw);
                    $user['HashKey'] = self::enchash( $pw );
                    $user['DateAdded'] = date( 'Y-m-d H:i:s' );
                    $user['Capability'] = Level::info( 'Capability', isset( $user['Level'] ) ? $user['Level'] : Option::get( 'new_user_role' ) );

                    if( !User::is( 'Administrator' ) && $user['Level'] == 1 ){
                        // prevent non-admin user to create admin account
                        // DO NOTHING
                    } else {
                        $userID = $this->db->insert( "users", $user );
                    }

                    if( $userID ){
                        $dataID = $userID;
                        $this->setSession( 'message', "New user has been added!" );

                        $where = array( 'UserID' => $userID );

                        // TO DO: send email
                        $meta['UserID'] = $userID;
                        $metaID = $this->db->insert( "user_meta", $meta );

                        if( $user['Level'] == 4 ){
                            $client['UserID'] = $userID;
                            $clientID = $this->db->insert( "client_profiles", $where );
                            if( isset( $client['Video'] ) ){
                                $vid = trim( $client['Video'] );
                                if( substr( $vid, 0, 7 ) != '<iframe' ){
                                    $client['Video'] = '<iframe width="560" height="315" src="'.$vid.'" frameborder="0" allow="autoplay;encrypted-media" allowfullscreen></iframe>';
                                }
                                header('X-XSS-Protection:0');
                            }

                            if( isset( $client['Price'] ) ){
                                $client['Price'] = (float) $client['Price'] == 0.00 ? '1' : $client['Price'];
                            }

                            foreach( $client as $key => $value ){
                                if( $key != 'Video' ){
                                    $temp = AppUtility::sanitizeEndingBRTags( $value ); // remove first ending <br> tags
                                    $client[$key] = AppUtility::sanitizeEndingPTags( $temp ); // remove ending empty <p> tags
                                }
                            }
                            $this->db->update( "client_profiles", $client, $where );
                            App::activityLog( "Added Client #".$clientID.'.' );
                        }

                        //add user investor profile
                        if( $user['Level'] == 7 ){
                            $acct['UserID'] = $userID;
                            $acct['Status'] = 'Incomplete';
                            $acctID = $this->db->insert( "accounts", $acct );
                        }

                        if( isset( $this->file['Avatar']['name'] ) && $this->file['Avatar']['name'] != '' ){
                            $filedata = $this->fileUpload( $this->file, $userID );
                            if( count( $filedata ) ){
                                $rowCount = $this->db->update( "user_meta", $filedata, $where );
                            }
                        }
                        App::activityLog("Added User #".$userID.'.');
                    }
                } break;

                case "addproject": {
                    $data = $this->post;                    
                    $user = $this->post['user'];
                    $meta = $this->post['meta'];
                    $client = $this->post['client'];

                    unset( $data['action'] );
                    $pw = $this->generatePw(10);
                    $user['Active'] = 1;
                    $user['Password'] = $this->encrypt($pw);
                    $user['HashKey'] = self::enchash( $pw );
                    $user['DateAdded'] = date( 'Y-m-d H:i:s' );
                    $user['Capability'] = Level::info( 'Capability', isset( $user['Level'] ) ? $user['Level'] : Option::get( 'new_user_role' ) );

                    if( !User::is( 'Administrator' ) && $user['Level'] == 1 ){
                        // prevent non-admin user to create admin account
                        // DO NOTHING
                    } else {
                        $userID = $this->db->insert( "users", $user );
                    }

                    if( $userID ){
                        $dataID = $userID;
                        $this->setSession( 'message', "New user has been added!" );

                        $where = array( 'UserID' => $userID );

                        // TO DO: send email
                        $meta['UserID'] = $userID;
                        $metaID = $this->db->insert( "user_meta", $meta );

                        if( $user['Level'] == 4 ){
                            if( isset( $client['Video'] ) ){
                                header('X-XSS-Protection:0');
                                if( stripos( $client['Video'], '<iframe' ) !== true ){
                                    $client['Video'] = '<iframe width="560" height="315" src="'.$client['Video'].'" frameborder="0" allow="autoplay;encrypted-media" allowfullscreen></iframe>';
                                }
                            }

                            if( isset( $client['Price'] ) ){
                                $client['Price'] = (float) $client['Price'] == 0.00 ? '1.00' : $client['Price'];
                            }

                            $client['UserID'] = $userID;
                            $clientID = $this->db->insert( "client_profiles", $client );
                            App::activityLog( "Added Client #".$clientID.'.' );
                        }

                        //add user investor profile
                        if( $user['Level'] == 7 ){
                            $acct['UserID'] = $userID;
                            $acct['Status'] = 'Incomplete';
                            $acctID = $this->db->insert( "accounts", $acct );
                        }

                        if( isset( $this->file['Avatar']['name'] ) && $this->file['Avatar']['name'] != '' ){
                            $filedata = $this->fileUpload( $this->file, $userID );
                            if( count( $filedata ) ){
                                $rowCount = $this->db->update( "user_meta", $filedata, $where );
                            }
                        }
                        App::activityLog("Added User #".$userID.'.');
                    }
                } break;
                
                case "addtestimonial": {

                    $data = $this->post;

                    unset( $data['action'] );

                    $testimonialID = $this->db->insert( 'testimonials', $data );
                    $this->setSession( 'message', 'Added Testimonial #ID-' . $testimonialID . '.' );
                    App::activityLog( 'Added Testimonial #ID-' . $testimonialID . '.' );
                } break;
                
                case "updatetestimonial": {

                    $data = $this->post;
                    $testimonialID = $this->post['TestimonialID'];

                    unset( $data['action'] );
                    unset( $data['TestimonialID'] );

                    $where = array( 'TestimonialID' => $testimonialID );

                    $this->db->update( 'testimonials', $data, $where );
                    $this->setSession( 'message', 'Updated Testimonial #ID-' . $testimonialID . '.');
                    App::activityLog( 'Updated Testimonial #ID-' . $testimonialID . '.' );
                } break;
                
                case "addslider": {

                    $data = $this->post;

                    unset( $data['action'] );

                    if( isset( $this->file['SliderPhoto']['name'] ) && $this->file['SliderPhoto']['name'] != '' ){
                        $userID = $this->post['UserID'];
                        $fileData = $this->fileUpload( $this->file, $userID, '', false, 6, 0, false, 'x' );
                        $fileID = $fileData['SliderPhoto'];
                        
                        unset( $data['UserID'] );
                        unset( $data['SliderPhoto'] );

                        $data['ImageID'] = $fileID;

                        $slideID = $this->db->insert( 'slides', $data );

                        $this->setSession('message', "File has been uploaded !");
                    }

                    $this->setSession( 'message', 'Added Slider #ID-' . $slideID . '.' );
                    App::activityLog( 'Added Slider #ID-' . $slideID . '.' );
                } break;
                
                case "updateslider": {

                    $data = $this->post;
                    $userID = $data['UserID'];
                    $slideID = $data['SlideID'];
                    $sliderPhotoOld = isset( $data['SliderPhotoOld'] ) ? $data['SliderPhotoOld'] : false;

                    if( isset( $this->file['SliderPhoto']['name'] ) && $this->file['SliderPhoto']['name'] != '' ){
                        $fileData = $this->fileUpload( $this->file, $userID, '', false, 6, 0, false, 'x' );
                        $fileID = $fileData['SliderPhoto'];
                    }

                    unset( $data['action'] );
                    unset( $data['UserID'] );
                    unset( $data['SlideID'] );
                    unset( $data['SliderPhoto'] );
                    unset( $data['SliderPhotoOld'] );
                    
                    $data['ImageID'] = $fileID;
                    $where = array( 'SlideID' => $slideID );
                    $this->db->update( 'slides', $data, $where );

                    if( $sliderPhotoOld  && $sliderPhotoOld > 0 ) AppUtility::deleteFileAndAssociates( $sliderPhotoOld );

                    $this->setSession( 'message', 'Updated Slider #ID' . $slideID . '.' );
                    App::activityLog( 'Updated Slider #ID-' . $slideID . '.' );
                } break;
            } 
        }
        return $dataID;
    }
    
    function doSaveInvoiceType()
    {
        $dataID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

               case "addinvoicetype": {
                    $data = $this->post;
                    unset( $data['action'] );

                    if( isset( $data ) ){
                        $dataID = $this->db->insert( "invoice_type", $data );
                        if( $dataID ){
                            $this->setSession( 'message', "New invoice type has been added!" );
                            App::activityLog( "New invoice type has been added, InvoiceTypeID#".$dataID );
                        }
                    }

                } break;

                case "editinvoicetype": {
                    $data = $this->post;
                    $invoiceTypeID = $data['InvoiceTypeID'];
                    $where = array( 'InvoiceTypeID' => $invoiceTypeID );

                    unset( $data['action'] );
                    unset( $data['InvoiceTypeID'] );
    
                    if( isset( $data ) ){
                        $this->db->update( "invoice_type", $data, $where );                        
                        $this->setSession( 'message', "Invoice type has been updated!" );
                        App::activityLog( "Invoice type has been updated, InvoiceTypeID#".$invoiceTypeID );
                    }
                } break;

                default: break;
            } 
        }
        return $dataID;
    }

    /**
     * Insert / Update / Delete team members
     * 
     * @package
     * @access private
     * @param (string) $userID : (required) current userid
     * @param (string) $ID     : (optional) core teamid
     * @return int
     **/
    function doSaveTeam( $userID, $ID = false )
    {
        $teamID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "add": {
                    $data = $this->post;
                    $team = $this->post['team'];

                    unset( $data['action'] );

                    $teamID = $this->db->insert( "core_team", $team );

                    if( $teamID ){
                        $where = array( 'CoreTeamID' => $teamID );

                        // TO DO: send email
                        //$filedata = $this->sendEmail($teamID);
                        if( isset( $this->file['Photo']['name'] ) && $this->file['Photo']['name'] != '' ){
                            $filedata = $this->fileUpload( $this->file, $userID );
                            if( count( $filedata ) ){
                                $rowCount = $this->db->update("core_team", $filedata, $where );
                            }
                        }
                        $this->setSession( 'message', "New core team member has been added!" );
                        App::activityLog( "Added core team member ID#$teamID" );
                    }                    
                } break;

                case "edit": {
                    $data = $this->post;
                    $where = array( 'CoreTeamID' => $ID );

                    unset( $data['action'] );

                    if( isset( $this->post['team'] ) ){
                        $team = $this->post['team'];
                        $teamID = $this->db->update( "core_team", $team, $where );
                        if( $teamID ){
                            $this->setSession( 'message', 'Core team member has been updated!' );
                            App::activityLog( "Updated core team member ID#$ID" );
                        }
                    }

                    if( isset( $this->file['Photo'] ) && $this->file['Photo']['name'] != '' ){
                        $filedata = $this->fileUpload( $this->file, $userID );
                        if( count( $filedata ) ){
                            $this->removeUploadedFiles( $this->post['photoid'] );
                            $rowCount = $this->db->update( "core_team", $filedata, $where );
                        }
                    }
                } break;   
            } 
        }
        
        return $teamID;
    }
    
    function doTrashTeam( $ID )
    {
        $where = array( 'CoreTeamID' => $ID );
        $data = array( 'Active' => 0 );
        $rowCount = $this->db->update( "core_team", $data, $where );
        if( $rowCount ){
            $this->setSession( 'message', "Trashed core team member ID#$ID" );
            App::activityLog( "Trashed core team member ID#$ID" );
        }
    }
    
    function doRestoreTeam( $ID )
    {
        $where = array( 'CoreTeamID' => $ID );
        $data = array( 'Active' => 1 );
        $rowCount = $this->db->update( "core_team", $data, $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Restored core team member ID#'.$ID.' from trash bin.' );
            App::activityLog( 'Restored team member #ID-'.$ID.' from trash bin.' );
        }
    }
    
    function doDeleteTeam( $ID )
    {
        $where = array( 'CoreTeamID' => $ID );
        $rowCount = $this->db->delete( "core_team", $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Permanently deleted core team member ID#'.$ID.' from trash bin.' );
            App::activityLog( 'Permanently deleted core team member ID#'.$ID.' from trash bin.' );
        }
    }    
    
    function doEmptyTrashTeam( $ids )
    {
        foreach( $ids as $id ){
            $where = array( 'CoreTeamID' => $id );
            $rowCount = $this->db->delete( "core_team", $where );            
            if( $rowCount ){
                App::activityLog( 'Permanently deleted core team member ID#'.$ID.' from trash bin.' );
            }
        }
        $this->setSession( 'message', 'Permanently deleted core team members from trash bin.' );
    }

    function doTrash($UserID)
    {
        $where = array( 'UserID' => $UserID );
        $data = array( 'Active' => 0 );
        $rowCount = $this->db->update( "users", $data, $where );
        App::activityLog( "Trashed User #".$UserID.'.' );
    }
    
    function doRestore($UserID)
    {
        $where = array('UserID' => $UserID);
        $data = array('Active' => 1);
        $rowCount = $this->db->update("users",$data, $where);
        App::activityLog("Restored User #".$UserID." from trash bin.");
    }
    
    /**
     * Delete user account
     * 
     * @package
     * @access Private
     * @param (int) @ID       : (required) : user id to be deleted
     * @return NA
     **/
    function doDelete( $ID )
    {
        $pre = User::info( false, $ID );
        $where = array( 'UserID' => $ID );
        
        $this->db->delete( "users", $where );
        $this->db->delete( "user_meta", $where );
        $this->db->delete( "accounts", $where );
        $this->db->delete( "client_profiles", $where );
        AppUtility::deleteFilesOnDeleteUser( $ID );

        $post = User::info( false, $ID );
        if( !is_object( $post ) ){
            $msg = "Permanently deleted user ".$pre->FirstName.' '.$pre->LastName." (ID#".$ID.").";
            $this->setSession( 'message', $msg );
            App::activityLog( $msg );
            return true;
        }

        return false;
    }

    function doManageDelete( $deletea )
    {
        foreach( $deletea as $k => $v ){
            $where = array( $v['CN'] => $v['ID'] );            
            $rowCount = $this->db->delete( $v['TN'], $where );
            App::activityLog( "Permanently deleted item #".$v['ID']." on `".$v['TN']."`." );
            $this->setSession( 'message',"Permanently deleted item #".$v['ID']." on `".$v['TN']."`." );
        }
    }

    function doDeleteImageFile( $ID )
    {
        foreach( $this->getUploadedFiles( $ID ) as $f ){
            unlink( $f->FilePath );
        }

        $where = array( 'FileID' => $ID );
        $rowCount = $this->db->delete( "files", $where );
        $rowCount = $this->db->delete( "file_items", $where );
        $rowCount = $this->db->delete( "file_groups", $where );

        $this->setSession('message',"Image and associated data has been deleted!");       
        App::activityLog("Deleted IMAGE FileID #" . $ID . '.');
    }
    
    function doEmptyTrash($ids)
    {
        foreach( $ids as $id ){
            self::doDelete( $id );
        }
        
        App::activityLog( "Empty trashed bin." );
    }
}