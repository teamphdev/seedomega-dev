<?php
class Faq_model extends Model
{
    var $levelID = 4;

    function __construct()
    {
        parent::__construct();

        View::$segments = $this->segment;
    }
       
    function getFaq( $ID )
    {
        $sql = "SELECT * FROM faqs WHERE FaqID = '$ID' LIMIT 1";
        $data = $this->db->get_row($sql);

        return $data;
    }

    function getFaqs( $viewing = false, $status = NULL )
    {
        $where = "";
        if( $status != NULL ){
            $where .= " AND f.FaqStatus = '$status' ";
        } 

        if( $viewing ){
            $where .= " AND f.PublicView = $viewing ";
        }

        $sql = "SELECT *
            FROM faqs f             
            LEFT JOIN users u ON f.UserID = u.UserID 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE f.ClientProfileID = 0 ".$where.
            "ORDER BY f.FaqID DESC";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function doSave()
    {
        $faqID = false;

        if(isset($this->post['action'])) {
            switch($this->post['action']) { 
                case "addnew": {

                    $data = $this->post;
                    unset($data['action']);

                    $this->setSession('message',"New faq has been added!");

                    $data['FaqDate'] = date('Y-m-d H:i:s');

                    $ID = $this->db->insert("faqs", $data);

                    App::activityLog("Added faq #ID-".$ID.'.');
                } break;
                case "update":

                    $data = $this->post;

                    $FaqID = $data['FaqID'];

                    unset($data['action']);
                    unset($data['FaqID']);
                    unset($data['UserID']);
                    unset($data['ClientProfileID']);

                    $where = array('FaqID' => $FaqID);
                    $this->db->update("faqs", $data, $where);

                    $this->setSession('message',"Faq changes have been saved!");
                    App::activityLog("Edited faq #ID-".$FaqID .'.');
                 break;
            }
        }
    }
}