<?php
class CoreBankAccounts_model extends Model 
{
    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }
       
    function getBankAccountID($ID)
    {
        $sql = "SELECT cba.*, ul.Code as AddedBy_RoleCode, um.FirstName as AddedBy_FirstName, um.LastName as AddedBy_LastName  
        FROM core_bank_accounts cba 
        LEFT JOIN users u ON cba.UserID = u.UserID
        LEFT JOIN user_meta um ON cba.UserID = um.UserID 
        LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE cba.`CoreBankAccountID` = ".$ID." LIMIT 1";

        $data = $this->db->get_row($sql);
        
        return $data;
    } 
    
    function getAllBankAccounts()
    {
        $sql = "SELECT cba.*, ul.Code as AddedBy_RoleCode, um.FirstName as AddedBy_FirstName, um.LastName as AddedBy_LastName  
        FROM core_bank_accounts cba 
        LEFT JOIN users u ON cba.UserID = u.UserID
        LEFT JOIN user_meta um ON cba.UserID = um.UserID 
        LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID";    
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    }
    
    function doSave()
    {
        $bankID = false;
        if(isset($this->post['action'])) {
            switch($this->post['action']) {
                case "updatebank": {                    

                    $data = $this->post;
                    $bank = $this->post['bank'];
                    $cbankID = $this->post['bankid'];

                    unset($data['action']);

                    $bankID = $this->db->update("core_bank_accounts", $bank, array('CoreBankAccountID' => $cbankID));

                    if($bankID) {
                        App::activityLog("Core Bank Account: Updated bank details ('".$bank['Name']."', '".$bank['AccountName']."', '".$bank['AccountNumber']."').");
                        $this->setSession('message',"Your changes has been successfully submited!");
                    }

                } break;
                case "addbank": {

                    $data = $this->post;
                    $bank = $this->post['bank'];

                    $bank['UserID'] = User::info('UserID');

                    unset($data['action']);

                    $bankID = $this->db->insert("core_bank_accounts", $bank);

                    if($bankID) {
                        App::activityLog("Core Bank Account: New Added ('".$bank['Name']."', '".$bank['AccountName']."', '".$bank['AccountNumber']."').");
                        $this->setSession('message',"You have successfully added New Bank Account");
                    }
                } break;
            } 
        }
        return $bankID;
    }   
        
    function doDelete($ID)
    {
        $where = array('CoreBankAccountID' => $ID);
        $this->setSession('message',"Successfully Deleted"); 
        App::activityLog("Core Bank Account: deleted bank account ID.".$ID.".");       
        $rowCount = $this->db->delete("core_bank_accounts", $where);
    }
}