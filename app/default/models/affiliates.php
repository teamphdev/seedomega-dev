<?php
class Affiliates_model extends Model
{
    function __construct()
    {
        parent::__construct();
        View::$segments = $this->segment;
    }

    public function getAffiliates( $inactive = '' )
    {
        // $sql = "SELECT r.CommissionRate, r.Status, u.*, um.*, ul.Name
        //     FROM user_referrer as r 
        //     LEFT JOIN user_meta um ON um.UserID = r.UserID 
        //     LEFT JOIN users as u ON u.UserID = r.UserID
        //     LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        //     WHERE u.Level IN ( 4, 7 )";

        // $sql = "SELECT DISTINCT u.`UserID`, u.`ReferrerUserID`, um.`Avatar`, um.`FirstName`, um.`LastName`, u.`Email`,
        //     CASE WHEN cp.`Status` IS NOT NULL THEN cp.`Status` ELSE a.`Status` END AS Status,
        //     CASE WHEN cp.`UserID` IS NOT NULL THEN cp.`CommissionRate` ELSE a.`CommissionRate` END AS CommissionRate, ul.`Name`, '' AS StatusHTML, '' AS AvatarData, '' AS LabelClass, '' AS IClass
        //     FROM `users` u
        //     LEFT JOIN `accounts` a ON a.`UserID` = u.`UserID`
        //     LEFT JOIN `client_profiles` cp ON cp.`UserID` = u.`UserID`
        //     LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID` 
        //     LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level` 
        //     WHERE u.`Level` IN ( 4, 7 ) ORDER BY `LastName`, `FirstName`";

        // $sql = "SELECT DISTINCT u.`UserID`, u.`ReferrerUserID`, um.`Avatar`, um.`FirstName`, um.`LastName`, u.`Email`, a.`Status`, a.`CommissionRate`, ul.`Name`, '' AS StatusHTML, '' AS AvatarData, '' AS LabelClass, '' AS IClass
        //     FROM `users` u
        //     LEFT JOIN `accounts` a ON a.`UserID` = u.`UserID`
        //     LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID` 
        //     LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level` 
        //     WHERE u.`Level` = 7 AND a.`UserID` IS NOT NULL
        //     UNION ALL            
        //     SELECT DISTINCT u.`UserID`, u.`ReferrerUserID`, um.`Avatar`, um.`FirstName`, um.`LastName`, u.`Email`, cp.`Status`, cp.`CommissionRate`, ul.`Name`, '' AS StatusHTML, '' AS AvatarData, '' AS LabelClass, '' AS IClass
        //     FROM `users` u
        //     LEFT JOIN `client_profiles` cp ON cp.`UserID` = u.`UserID`
        //     LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID` 
        //     LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level` 
        //     WHERE u.`Level` = 4 AND cp.`UserID` IS NOT NULL;";

        // $sql = "SELECT DISTINCT u.`UserID`, u.`ReferrerUserID`, um.`Avatar`, um.`FirstName`, um.`LastName`, u.`Email`, ul.`Name`, fi.`FileSlug`, ur.`CommissionRate`, ur.`SubscriptionRate`, ur.`Exemption`, CASE WHEN cp.`Status` IS NOT NULL THEN cp.`Status` ELSE a.`Status` END AS Status
        //     FROM `users` u
        //     LEFT JOIN `accounts` a ON a.`UserID` = u.`UserID`
        //     LEFT JOIN `client_profiles` cp ON cp.`UserID` = u.`UserID`
        //     LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
        //     LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level`
        //     LEFT JOIN `user_referrer` ur ON ur.`UserID` = u.`UserID`
        //     LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
        //     WHERE u.`Level` IN ( 4, 7 ) AND u.`UserID` IS NOT NULL;";

        $sql = "SELECT DISTINCT u.`UserID`, u.`ReferrerUserID`, um.`Avatar`, um.`FirstName`, um.`LastName`, u.`Email`, cp.`CompanyName` , ul.`Name`, fi.`FileSlug`, ur.`CommissionRate`, ur.`SubscriptionRate`, ur.`Exemption`, CASE WHEN cp.`Status` IS NOT NULL THEN cp.`Status` ELSE a.`Status` END AS Status
            FROM `users` u
            LEFT JOIN `accounts` a ON a.`UserID` = u.`UserID`
            LEFT JOIN `client_profiles` cp ON cp.`UserID` = u.`UserID`
            LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
            LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level`
            LEFT JOIN `user_referrer` ur ON ur.`UserID` = u.`UserID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
            WHERE u.`Level` IN ( 4, 7 ) AND u.`UserID` IS NOT NULL AND ( a.`Status` = 'Approved' OR cp.`Status` = 'Approved' );";

        /*$where = " WHERE u.Active = 1";
        if($inactive == 'yes') {
            $where = " WHERE u.Active != 1";
        }
        $sql .= $where . " AND u.ReferrerUserID <>'' AND (u.Level = 4 OR u.Level = 7)";
        $sql .= " ORDER BY u.UserID";*/

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            //$data[]= $row;
            $data[$row->Name][] = $row;
        }
        unset( $query );

        return $data;
    }

    function doSave()
    {
        if( isset( $this->post['action'] ) ){
            $data = $this->post;
            $action = $data['action'];
            $level = $data['Level'];
            $rate = $data['rate'];
            $rate['UpdatedAt'] = date('Y-m-d H:i:s');

            $userid = $rate['UserID'];

            unset( $data['action'] );
            unset( $data['Level'] );

            $where = array( 'UserID' => $userid );
            $table = isset( $level ) && $level == 'Client' ? 'client_profiles' : 'accounts';
            //$this->db->update( $table, $data, $where );

            if( AppUtility::hasCommissionRate( $userid ) ){
                unset( $rate['UserID'] );
                $this->db->update( 'user_referrer', $rate, $where );
            } else {
                $this->db->insert( 'user_referrer', $rate );
            }

            $this->setSession( 'message', 'Commission/Subscription Rate have been updated!' );
            App::activityLog( User::info('UserID').': Updated the Commission/Subscription Rate #ID-'.$userid.'.' );
        }

        return true;
    }
}