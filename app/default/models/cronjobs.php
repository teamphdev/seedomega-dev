<?php
class CronJobs_model extends Model
{
    var $levelID = 5;
    
    function __construct() {
        parent::__construct();
        View::$segments = $this->segment;
    }

    function getAllBookings()
    {
        $sql = "SELECT b.*, um.FirstName, um.LastName 
            FROM bookings b            
            LEFT JOIN user_meta um ON um.UserID = b.UserID";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            if($row->BookingStatus == 'Pending' && $row->TTReceiptPhoto != 0){
                $data[$row->BookingStatus]['Pending'][] = $row;
            }elseif($row->BookingStatus == 'Pending' && $row->TTReceiptPhoto == 0){
                $data[$row->BookingStatus]['Incomplete'][] = $row;
            }else{
                $data[$row->BookingStatus][] = $row;  
            }
            
        }
        unset($query);

        return $data;
    }

    function deleteExpiredBookings()
    {
        $sql = "DELETE FROM `bookings` WHERE NOW() > `BookingExpiryDate` AND (BookingStatus = 'Pending' OR BookingStatus = '')";
        $this->db->query($sql);
        return true;
    }
}