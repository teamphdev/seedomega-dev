<?php
class Capabilitygroups_model extends Model 
{
    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }
       
    function getCapabilityGroup($ID)
    {
        $sql = "SELECT * FROM user_capability_groups WHERE UserCapabilityGroupID = ".$ID." LIMIT 1";
        $groupsdata = $this->db->get_row($sql);
        
        return $groupsdata;
    } 
    
    function getCapabilityGroups($inactive = '')
    {
        $sql = "SELECT * FROM user_capability_groups";
        $where = " WHERE Active = 1";
        if($inactive == 'yes') {
            $where = " WHERE Active != 1";
        }
        $sql .= $where;        
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    }
    
    function doSave()
    {
        if(isset($this->post['action'])) {
            switch($this->post['action']) {
                case "updategroup": {
                    $groupid = $this->post['groupid'];
                    $data = $this->post;
                    unset($data['action']);
                    unset($data['groupid']);

                    
                    $this->setSession('message',"Group has been updated!");

                    $grID = $this->db->update("user_capability_groups", $data, array('UserCapabilityGroupID' => $groupid));

                } break;
                case "addgroup": {

                    $data = $this->post;
                    unset($data['action']);

                    $groupid = $this->db->insert("user_capability_groups", $data);

                    if($groupid) {
                        
                        $this->setSession('message',"New group has been registered!");
                    }

                    //View::redirect('agency');
                } break;
            } 
        }
        return (object) $this->post;
    }   
        
    function doDelete($ID)
    {
        $where = array('UserCapabilityGroupID' => $ID);        
        $this->setSession('message',"Group has been deleted!");        
        $rowCount = $this->db->delete("user_capability_groups", $where);
        $rowCount = $this->db->delete("user_capabilities", $where);
    }
}