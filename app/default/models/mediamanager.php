<?php
class MediaManager_model extends Model 
{
    function __construct() {
        parent::__construct();
        
        View::$footerscripts = array(
            'vendor/jquery/dist/jquery.min.js',
            'vendor/bootstrap/dist/js/bootstrap.min.js',
            'vendor/fastclick/lib/fastclick.js',
            'vendor/nprogress/nprogress.js'
            );
        View::$styles = array('vendor/bootstrap/dist/css/bootstrap.min.css','vendor/font-awesome/css/font-awesome.min.css','assets/css/custom.css');
        View::$segments = $this->segment;
    }
       
    function getFile($ID)
    {
        $sql = "SELECT fi.*, f.* FROM file_items fi JOIN files f ON f.FileID = fi.FileID WHERE f.FileID = ".$ID." LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    } 
    
    function getFiles($inactive = '')
    {
        $sql = "SELECT fsc.*, fc.*, fi.*, f.* FROM files f LEFT JOIN file_items fi ON fi.FileID = f.FileID LEFT JOIN file_categories fc ON fc.FileCategoryID = f.FileCategoryID LEFT JOIN file_sub_categories fsc ON f.FileSubCategoryID = fsc.FileSubCategoryID WHERE f.FileCategoryID != 0 ORDER BY f.DateAdded DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getFileCategories()
    {
        $sql = "SELECT fc.* FROM file_categories fc ORDER BY fc.Order ASC";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getCategory($ID)
    {
        $sql = "SELECT * FROM file_categories WHERE FileCategoryID = ".$ID." LIMIT 1";
        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getSubCategories()
    {
        $sql = "SELECT fc.*,fsc.*, (SELECT fc1.FileCategoryName FROM file_categories fc1 WHERE fc1.FileCategoryID = fsc.FileCategoryID) as ParentCatName FROM file_sub_categories fsc LEFT JOIN file_categories fc ON fc.FileCategoryID = fsc.FileCategoryID ORDER BY fsc.Order ASC";
 
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getSubCategory($ID)
    {
        $sql = "SELECT * FROM file_sub_categories WHERE FileSubCategoryID = ".$ID." LIMIT 1";
        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getSubCategoriesRelated($ID)
    {
        $sql = "SELECT fc.*, fsc.*, (SELECT fc1.FileCategoryName FROM file_categories fc1 WHERE fc1.FileCategoryID = fsc.FileCategoryID) as ParentCatName FROM file_sub_categories fsc LEFT JOIN file_categories fc ON fc.FileCategoryID = fsc.FileCategoryID WHERE fc.FileCategoryID = ".$ID." ORDER BY fsc.Order ASC";
 
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function doSave()
    {
        if($this->post) { 
            if(isset($this->post['action'])) {
                switch($this->post['action']) {
                    case "updatemedia": {
                        $fileid = $this->post['fileid'];
                        $fileitemid = $this->post['fileitemid'];
                        $fileitem = $this->post['fi'];
                        $data = $this->post;
                        unset($data['action']);
                        unset($data['fileid']);
                        unset($data['fileitemid']);
                        unset($data['fi']);
                        
                        $data['FileAccess'] = $this->arrayToString($this->post['FileAccess']);
                        
                        $this->setSession('message',"File has been updated!");

                        $fileID = $this->db->update("files", $data, array('FileID' => $fileid));
                        $fileitemID = $this->db->update("file_items", $fileitem, array('FileItemID' => $fileitemid));
                        
                        App::activityLog("Updated File #".$fileid.'.');

                    } break;
                    case "addmedia": {
                        $data = $this->post;
                        unset($data['action']);
                        
                        $filedata = $this->fileUpload($this->file,User::info('UserID'),DS.'downloads',$data['FileName'],$data['FileCategoryID'],$data['FileSubCategoryID'],$data['FileNameC'],$data['FileAccess']);       
                        
                        $this->setSession('message',"New Media has been added!");

                        App::activityLog("Added Media File".'.');
                    } break;
                    case "savefileorder": {
                        $savedata = array();
                        $data = $this->post['order'];
                        unset($data['action']);

                        foreach($data as $k => $v) {
                            $this->db->update("files", array('Order'=>$v), array('FileID'=>$k));
                        }

                        $this->setSession('message',"File order has been saved!");

                        App::activityLog("Update File Order");
                    } break;
                    case "updatecategory": {
                        $catid = $this->post['catid'];
                        $data = $this->post;
                        unset($data['action']);
                        unset($data['catid']);
                        
                        
                        $this->setSession('message',"Category has been updated!");

                        $catsID = $this->db->update("file_categories", $data, array('FileCategoryID' => $catid));
                        
                        App::activityLog("Updated Media Category #FileCategoryID".$catid.'.');
                        
                    } break;
                    case "addcategory": {

                        $data = $this->post;
                        unset($data['action']);

                        $catID = $this->db->insert("file_categories", $data);

                        if($catID) {
                            
                            $this->setSession('message',"New Category has been registered!");
                        }

                        App::activityLog("Added Media Category #FileCategoryID".$catID.'.');
                    } break;
                    case "savecategoryorder": {
                        $savedata = array();
                        $data = $this->post['order'];
                        unset($data['action']);

                        foreach($data as $k => $v) {
                            $this->db->update("file_categories", array('Order'=>$v), array('FileCategoryID'=>$k));
                        }

                        $this->setSession('message',"Category order has been saved!");

                        App::activityLog("Update Category Order");
                    } break;
                    case "editsubcategory": {
                        $subcatid = $this->post['subcatid'];
                        $data = $this->post;
                        unset($data['action']);
                        unset($data['subcatid']);
                        
                        $this->setSession('message',"Sub Category has been updated!");

                        $subcatsID = $this->db->update("file_sub_categories", $data, array('FileSubCategoryID' => $subcatid));
                        
                        App::activityLog("Updated Media Sub Category #FileSubCategoryID".$subcatid.'.');
                    } break;
                    case "addsubcategory": {

                        $data = $this->post;
                        unset($data['action']);

                        $subcatID = $this->db->insert("file_sub_categories", $data);

                        if($subcatID) {                            
                            $this->setSession('message',"New Sub Category has been registered!");
                        }

                        App::activityLog("Added Media Category #FileSubCategoryID".$subcatID.'.');

                        return $subcatID;
                        
                    } break;
                    case "savesubcategoryorder": {
                        $savedata = array();
                        $data = $this->post['order'];
                        unset($data['action']);

                        foreach($data as $k => $v) {
                            $this->db->update("file_sub_categories", array('Order'=>$v), array('FileSubCategoryID'=>$k));
                        }

                        $this->setSession('message',"Sub category order has been saved!");

                        App::activityLog("Update Sub category Order");
                    } break;
                } 
            }
            return (object) $this->post;
        } else {
            
        }
    }
    
    function doDelete($ID)
    {
        $where = array('FileID' => $ID);        
        $this->setSession('message',"File has been deleted!");
        
        foreach($this->getUploadedFiles($ID) as $f) {
            unlink($f->FilePath);
        }
        
        $rowCount = $this->db->delete("files", $where);
        $rowCount = $this->db->delete("file_items", $where);
        $rowCount = $this->db->delete("file_groups", $where);
        
        App::activityLog("Deleted File #".$ID.'.');
    }

    function doDeleteCategory($ID)
    {
        $where = array('FileCategoryID' => $ID);        
        $this->setSession('message',"Category has been deleted!");        
        $rowCount = $this->db->delete("file_categories", $where);
        $rowCount = $this->db->delete("file_sub_categories", $where);
        
        App::activityLog("Deleted Media Category #FileCategoryID-".$ID.'.');
    }

    function doDeleteSubCategory($ID)
    {
        $where = array('FileSubCategoryID' => $ID);        
        $this->setSession('message',"Sub category has been deleted!");    
        $rowCount = $this->db->delete("file_sub_categories", $where);
        
        App::activityLog("Deleted Media Sub category #FileSubCategoryID-".$ID.'.');
    }
}