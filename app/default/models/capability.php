<?php
class Capability_model extends Model
{
    function __construct() {
        parent::__construct();
        View::$segments = $this->segment;
    }
    
    function getCapability($ID)
    {
        $sql = "SELECT * FROM user_capabilities WHERE UserCapabilityID = ".$ID." LIMIT 1";
        $data = $this->db->get_row($sql);
        
        return $data;
    } 
    
    function getCapabilityGroups()
    {
        $sql = "SELECT * FROM user_capability_groups";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserCapabilityGroupID] = $row->Name;			
        }
        unset($query);
        
        return $data;
    } 
    
    function getCapabilities()
    {
        $sql = "SELECT uc.*, ucg.Name as GroupName FROM user_capabilities uc LEFT JOIN user_capability_groups ucg ON uc.UserCapabilityGroupID = ucg.UserCapabilityGroupID ORDER BY ucg.UserCapabilityGroupID ASC";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    } 
    
    function doSave()
    {
        if(isset($this->post['action'])) {
            switch($this->post['action']) {
                case "updatecapability": {
                    $data = $this->post;                    
                    $where = array('UserCapabilityID' => $this->post['capabilityid']);

                    unset($data['action']);
                    unset($data['capabilityid']);

                    $rowCount = $this->db->update("user_capabilities", $data, $where);

                    
                    $this->setSession('message',"User capability data has been saved.");
                    
                    App::activityLog("Updated Capability #".$this->post['capabilityid'].'.');

                } break;
                case "addcapability": {
                    $data = $this->post;                    

                    unset($data['action']);

                    $levelID = $this->db->insert("user_capabilities", $data);

                    if($levelID) {
                        
                        $this->setSession('message',"New user capability has been added!");
                    }
                    
                    App::activityLog("Added Capability #".$levelID.'.');

                    //View::redirect('capability');
                } break;
            } 
        }
        return (object) $this->post;
    }   
    
    function doDelete($UserCapabilityID)
    {
        $where = array('UserCapabilityID' => $UserCapabilityID);
        $rowCount = $this->db->delete("user_capabilities", $where);
        
        App::activityLog("Deleted Capability #".$UserCapabilityID.'.');
    }
}