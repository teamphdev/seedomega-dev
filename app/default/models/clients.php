<?php
class Clients_model extends Model
{
    function __construct()
    {
        parent::__construct();
        View::$segments = $this->segment;
    }
    
    function getActivityLogs($userID = false)
    {
        $userinfo = User::info();
        if(!$userID){
            $userID = $userinfo->UserID;
        }
        // $sql = "SELECT DISTINCT al.*, um.`FirstName`, um.`LastName` 
        // FROM `activity_logs` al 
        // LEFT JOIN `users` u ON u.`ReferrerUserID` = $userinfo->UserID 
        // LEFT JOIN `user_meta` um ON um.`UserID` = al.`UserID` 
        // WHERE ( al.`UserID` = $userID OR ( al.`UserID` = u.`UserID` AND u.`Level` = 6 ) ) AND al.`ActivityDescription` NOT LIKE '%Logged%'
        // ORDER BY al.`ActivityDate` DESC";

        $sql = "SELECT DISTINCT al.*, um.`FirstName`, um.`LastName` 
        FROM `activity_logs` al 
        LEFT JOIN `users` u ON u.`ReferrerUserID` = $userinfo->UserID 
        LEFT JOIN `user_meta` um ON um.`UserID` = al.`UserID` 
        WHERE ( al.`UserID` = $userID OR al.`UserID` IN ( SELECT UserID FROM users WHERE ReferrerUserID = $userID AND `Level` = 6) ) AND al.`ActivityDescription` NOT LIKE '%Logged%'
        ORDER BY al.`ActivityDate` DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);
        
        return $data;        
    }

    function getProfile( $userID, $multi = false )
    {
        $data = [];
        if( $multi ){
            $sql = "SELECT cp.*, fg.`FileGroupID`, fg.`FileID`, fg.`DocumentName`
            FROM `client_profiles` cp
            LEFT JOIN `file_groups` fg ON fg.`ReferenceID` = cp.`ClientProfileID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = fg.`FileID`
            WHERE cp.`UserID` = $userID AND (fi.`FileID` = fg.`FileID` OR (fi.`FileID` IS NULL AND fg.`FileID` IS NULL))
            ORDER BY fg.`FileGroupID`";
            
            $query = &$this->db->prepare( $sql );
            $query->execute();            
            while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                $data[] = $row;
            }
            unset( $query );

        } else {
            $sql = "SELECT * FROM client_profiles WHERE UserID = '".$userID."' LIMIT 1";
            $data = $this->db->get_row( $sql );
        }

        return $data;
    }

    /**
     * Get profile change request data
     * @package
     * @access public
     * @param (int) $clientProfileID : (required) client profile id
     * @param (string) $section      : (optional) section name
     * @return object
     **/
    function getProfileChangeRequest( $clientProfileID, $section = false )
    {
        $data = [];
        $where = $section ? " AND `Section` = '$section'" : "";

        $sql = "SELECT DISTINCT * FROM `client_profiles_request` WHERE `ClientProfileID` = $clientProfileID $where";
        //$data = $this->db->get_row( $sql );         
            
        $query = &$this->db->prepare( $sql );
        $query->execute();            
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[$row->FieldName] = $row;
        }
        unset( $query );

        return $data;
    }

    function getClientData( $ID, $userID = false )
    {
        $select = "";
        // Identify of currently logged in seeder is already invested of registered interest in the project
        if( $userID ){
            $select = "
                (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID = cp.ClientProfileID AND BookingType='Booking' AND UserID = $userID ) AS CurrentUserBookedCount, 
                (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID = cp.ClientProfileID AND BookingType='Interest' AND UserID = $userID ) AS CurrentUserRegisterCount,";
        }  

        $sql = "SELECT $select cp.*, ur.`CommissionRate` AS CommRate, ur.`SubscriptionRate` AS SubsRate,
            timestampdiff( DAY, NOW(), cp.`OfferClosing` ) AS DaysLeft, '' AS Data,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingType = 'Booking') as TotalBookingAmounts,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingType = 'Booking' AND bb.BookingStatus='Approved') as TotalRaisedAmt,
            (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus='Approved') as TotalInvestor,
            GetFileSlug( cp.`CompanyLogo` ) AS LogoSlug,
            GetFileSlug( cp.`CompanyPhoto` ) AS PhotoSlug,
            GetFileSlug( cp.`CompanyReleaseFileID` ) AS ReleaseSlug,
            GetFileSlug( cp.`CompanyResearchFileID` ) AS ResearchSlug,
            GetFileSlug( cp.`CompanyDueDiligenceFileID` ) AS DueSlug
        FROM client_profiles cp            
        LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        WHERE cp.ClientProfileID = $ID";

        $data = $this->db->get_row( $sql );

        return $data;
    }

    function getBookingsOwn($clientProfileID)
    {
        $sql = "SELECT b.*, um.FirstName,um.LastName, um.Avatar, u.Email
            FROM bookings b             
            LEFT JOIN user_meta um ON um.UserID = b.UserID
            LEFT JOIN users u ON u.UserID = b.UserID
            WHERE b.ClientProfileID=$clientProfileID";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getTotalsBookingsOwn($clientProfileID)
    {
        $sql = "SELECT 
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.BookingType='Booking' AND bb.BookingStatus = 'Approved' AND bb.ClientProfileID = $clientProfileID) as TotalAmountInvestment, 
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingType='Booking' AND bb.BookingStatus = 'Approved' AND bb.ClientProfileID = $clientProfileID) as TotalApproved,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingType='Booking' AND bb.BookingStatus = 'Verified' AND bb.ClientProfileID = $clientProfileID) as TotalPendings,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingType='Booking' AND bb.BookingStatus = 'Verified' AND bb.ClientProfileID = $clientProfileID) as TotalVerified,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingType='Booking' AND bb.BookingStatus IN('Verified', 'Pending') AND bb.ClientProfileID = $clientProfileID) as TotalNotApprove  
        FROM bookings b 
        WHERE b.ClientProfileID=$clientProfileID";
        
        $data = $this->db->get_row($sql);

        return $data;
    }

    function getClientCategories()
    {
        $sql = "SELECT c.*, um.FirstName,um.LastName, um.Avatar, u.Email
            FROM client_categories c             
            LEFT JOIN user_meta um ON um.UserID = c.UserID
            LEFT JOIN users u ON u.UserID = c.UserID";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientCategoryByID($ClientCatID)
    {
        $sql = "SELECT c.*, um.FirstName,um.LastName, um.Avatar, u.Email
            FROM client_categories c             
            LEFT JOIN user_meta um ON um.UserID = c.UserID
            LEFT JOIN users u ON u.UserID = c.UserID 
            WHERE c.ClientCatID = ".$ClientCatID;

        $data = $this->db->get_row($sql);

        return $data;
    }

    function getBookingsByStatus( $clientProfileID, $status, $type = 'Booking' )
    {
        // $sql = "SELECT b.*, um.FirstName, um.LastName, (SELECT FileSlug FROM file_items WHERE `fileID` = b.`TTReceiptPhoto` LIMIT 1) as TTReceiptFileSlug 
        //         FROM bookings b             
        //         LEFT JOIN user_meta um ON um.UserID=b.UserID
        //         WHERE b.ClientProfileID=$clientProfileID AND b.BookingType='$type' AND b.BookingStatus='$status' ORDER BY b.CreatedAt DESC";

        $sql = "SELECT b.*, um.`FirstName`, um.`LastName`, (SELECT `FileSlug` FROM `file_items` WHERE `fileID` = b.`TTReceiptPhoto` LIMIT 1) as TTReceiptFileSlug,
            CASE WHEN ur1.`ReferrerUserID` = ur2.`UserID` AND ur1.`Exemption` = 1 THEN '0' ELSE ur2.`SubscriptionRate` END AS SubscriptionRate
        FROM `bookings` b
        LEFT JOIN `user_meta` um ON um.`UserID` = b.`UserID`
        LEFT JOIN `user_referrer` ur1 ON ur1.`UserID` = b.`UserID`
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        LEFT JOIN `users` u2 ON u2.`UserID` = cp.`UserID`
        LEFT JOIN `user_referrer` ur2 ON ur2.`UserID` = cp.`UserID`
        WHERE b.`ClientProfileID` = $clientProfileID AND b.`BookingType` = '$type' AND b.`BookingStatus` = '$status' 
        ORDER BY b.`CreatedAt` DESC";
 
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getInvestorsByStatus($clientProfileID, $status, $type='Booking')
    {
        $sql = "SELECT b.*, um.FirstName, um.LastName, (SELECT FileSlug FROM file_items WHERE `fileID` = b.`TTReceiptPhoto` LIMIT 1) as TTReceiptFileSlug 
                FROM bookings b             
                LEFT JOIN user_meta um ON um.UserID=b.UserID
                WHERE b.ClientProfileID=$clientProfileID AND b.BookingType='$type' AND b.BookingStatus='$status' GROUP BY b.UserID ORDER BY b.CreatedAt DESC";
 
        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getBookingsTotalsByStatus($clientProfileID, $status, $type='Booking')
    {
        $sql = "SELECT SUM(b.`SharesToApply`) as TotalShares, SUM(b.`TotalAmountAttached`) as TotalAmountAttached
                FROM bookings b             
                LEFT JOIN user_meta um ON um.UserID=b.UserID
                WHERE b.ClientProfileID=$clientProfileID AND b.BookingType='$type' AND b.BookingStatus='$status' LIMIT 1";
 
        $data = $this->db->get_row($sql);

        return $data;
    }

    function getBookedData( $ID, $clientProfileID = '' )
    {
        // $sql = "SELECT b.*, um.FirstName, um.LastName
        //     FROM bookings b            
        //     LEFT JOIN user_meta um ON um.UserID=b.UserID
        // WHERE b.InvestmentBookingID=$ID";

        // $sql = "SELECT b.*, um.`FirstName`, um.`LastName`, ur.`SubscriptionRate`, cp.CompanyName, cp.UserID as ClientUserID,  
        // (SELECT umm.Address FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientAddress, 
        // (SELECT umm.Address2 FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientAddress2,
        // (SELECT umm.City FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientCity,
        // (SELECT umm.State FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientState, 
        // (SELECT umm.Country FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientCountry,
        // (SELECT umm.PostalCode FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientPostal,
        // (SELECT umm.Phone FROM user_meta umm WHERE cp.UserID = umm.UserID) as ClientPhone
        //     FROM `bookings` b
        //     LEFT JOIN `user_meta` um ON um.`UserID` = b.`UserID`
        //     LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        //     LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        //     WHERE b.`InvestmentBookingID` =  $ID";

        $sql = "SELECT b.*, um1.`FirstName`, um1.`LastName`, ur1.`ReferrerUserID`, cp.`UserID` AS ClientUserID, cp.`CompanyName`, um2.`Address` AS ClientAddress, um2.`Address2` AS ClientAddress2, um2.`City` AS ClientCity, um2.`State` AS ClientState, um2.`Country` AS ClientCountry, um2.`PostalCode` AS ClientPostal, um2.`Phone` AS ClientPhone, u2.`Email` AS ClientEmail, um2.`FirstName` AS ClientFirstName, um2.`LastName` AS ClientLastName, CASE WHEN ur1.`ReferrerUserID` = ur2.`UserID` AND ur1.`Exemption` = 1 THEN '0' ELSE ur2.`SubscriptionRate` END AS SubscriptionRate
        FROM `bookings` b
        LEFT JOIN `user_meta` um1 ON um1.`UserID` = b.`UserID`
        LEFT JOIN `user_referrer` ur1 ON ur1.`UserID` = b.`UserID`
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        LEFT JOIN `users` u2 ON u2.`UserID` = cp.`UserID`
        LEFT JOIN `user_meta` um2 ON um2.`UserID` = cp.`UserID`
        LEFT JOIN `user_referrer` ur2 ON ur2.`UserID` = cp.`UserID`
        WHERE b.`InvestmentBookingID` =  $ID";

        $data = $this->db->get_row($sql);

        return $data;
    }

    /*
     * Get documents upload by a client/company
     *
     * */
    function getClientFiles( $clientData, $all = false ){
        // $userID = $clientData->UserID;
        // $compReleaseFileID = $clientData->CompanyReleaseFileID;
        // $compResearchFileID = $clientData->CompanyResearchFileID;
        // $compDueDeligenceFileID = $clientData->CompanyDueDiligenceFileID;
        // $fileIDs = [$compReleaseFileID, $compResearchFileID, $compDueDeligenceFileID];

        // $sql = "SELECT UserID, FileName,FileSlug,FileDescription  FROM file_items WHERE UserID = ". $userID . " AND FileID IN (".implode(',',$fileIDs).") ";
        
        $where = $all ? "" : " AND fi.Active = 1 ";
        $sql = "SELECT fi.UserID, fi.FileName, fi.FileSlug, fi.FileDescription, fg.DocumentName
        FROM file_items fi
        LEFT JOIN file_groups fg ON fg.FileID = fi.FileID
        WHERE fg.ReferenceID = $clientData->ClientProfileID ".$where."
        ORDER BY fg.DocumentName";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getBookedItemData( $ID )
    {
        // $sql = "SELECT b.*, cp.*, 
        //     (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus='Approved') as TotalRaisedAmt,
        //     (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus='Approved') as TotalInvestor 
        //     FROM bookings b 
        //     LEFT JOIN client_profiles cp ON b.ClientProfileID=cp.ClientProfileID 
        //     WHERE b.InvestmentBookingID=$ID";

        $sql = "SELECT b.*, cp.*, SUM( b1.`TotalAmountAttached` ) AS TotalRaisedAmt, COUNT( b1.`UserID` ) AS TotalInvestor
        FROM `bookings` b 
        LEFT JOIN `client_profiles` cp ON b.`ClientProfileID` = cp.`ClientProfileID` 
        LEFT JOIN `bookings` b1 ON b1.`ClientProfileID` = cp.`ClientProfileID` AND b1.`BookingStatus` = 'Approved'
        WHERE b.`InvestmentBookingID` = $ID";

        $data = $this->db->get_row( $sql );
        
        return $data;
    }

    function markBookedData( $ID, $clientProfileID )
    {
        // Marked as approved
        $where = array( 'ClientProfileID' => $clientProfileID, 'InvestmentBookingID' => $ID );
        $data = array( 'BookingStatus' => 'Approved','AcknowledgeDate'=>date('Y-m-d H:i:s') );
        $this->db->update( 'bookings', $data, $where );

        return true;
    }

    function createAcknowledgeInvoice( $bookdata )
    {
        $invoiceID = false;

        // Create Invoice
        if( $bookdata ){
            $duedate = date('Y-m-d');

            $invoice = array();
            $invoice['ClientID'] = $bookdata->ClientProfileID;
            $invoice['UserID'] = $bookdata->ClientUserID;
            $invoice['InvestorUserID'] = $bookdata->UserID;
            $invoice['InvestmentBookingID'] = $bookdata->InvestmentBookingID;
            $invoice['InvoiceItem'] = $bookdata->CompanyName;
            $invoice['InvestmentAmount'] = $bookdata->TotalAmountAttached;
            $invoice['Total'] = AppUtility::computeCommission( $bookdata->TotalAmountAttached, $bookdata->SubscriptionRate );
            $invoice['SubTotal'] = $invoice['Total'];
            $invoice['BalanceDue'] = $invoice['Total'];
            $invoice['InvoiceDate'] = date('Y-m-d');
            $invoice['DueDate'] = date( 'Y-m-d', strtotime( $duedate.'+7 days' ) );
            $invoice['Status'] = 'Outstanding';
            $invoiceID = $this->db->insert( "invoices", $invoice );

            if( $invoiceID ){
                //insert data on items table
                $invItem['InvoiceID'] = $invoiceID;                            
                $invItem['InvestmentBookingID'] = $bookdata->InvestmentBookingID;
                $invItem['Type'] = 'Booking Commission';
                $invItem['Price'] = $invoice['Total'];
                $invItem['ItemAmount'] = $invoice['Total'];
                $invItem['Qty'] = 1;
                $this->db->insert( "invoice_items", $invItem );

                //update invoice status for table bookings
                $bwhere = array( 'InvestmentBookingID' => $bookdata->InvestmentBookingID );
                $bdata = array( 'InvoiceStatus' => 1, 'UpdatedAt' => date('Y-m-d') );
                $this->db->update( "bookings", $bdata, $bwhere );

                $investor = isset( $bookdata->FirstName ) ? $bookdata->FirstName.' ' : '-';
                $investor .= isset( $bookdata->LastName ) ? $bookdata->LastName : '-';
                $from = ' from ' . $investor;
                $amount = isset( $bookdata->TotalAmountAttached ) ? number_format( $bookdata->TotalAmountAttached, 2 ) : '0.00';
                $bookon = isset( $bookdata->CreatedAt ) ? date( 'Y-m-d', strtotime( $bookdata->CreatedAt ) ) : '-';

                App::activityLog( 'Acknowledged funding'.$from.' with an amount of $'.$amount.', booked on '.$bookon.'. Created an invoice ID#'.$invoiceID.'.' );

                $wallet['TransactionStatus'] = "Approved";
                $wallet['UserID'] = User::info( 'UserID' );
                $wallet['Method'] = '3'; //Wallet Transfer
                $wallet['TransactionDate'] = date('Y-m-d H:i:s');
                $wallet['ApproveDate'] = date('Y-m-d H:i:s');
                $wallet['Description'] = 'Auto-Debit: InvoiceID#'.$invoiceID.', BookingID#'.$bookdata->InvestmentBookingID.', Investment Amount($'.number_format( $bookdata->TotalAmountAttached, 2 ).')';
                $wallet['TransactionType'] = 'Debit';
                $wallet['TransactionAmount'] = str_replace(',', '', $invoice['Total'] );

                $mybalance = AppUtility::getwalletbalance( $bookdata->ClientUserID );
                if( isset( $mybalance ) ){
                    if( floatval( $mybalance ) >= 0 ){
                        $walletID = $this->db->insert( "wallet", $wallet ); //auto-debit from wallet
                    }

                    if( $walletID ){
                        //update invoice table
                        $invoice = array( 'Status' => 'Paid', 'UpdatedAt' => date('Y-m-d H:i:s') );
                        $where = array( 'InvoiceID' => $invoiceID );
                        $updateInvoice = $this->db->update( "invoices", $invoice, $where );

                        App::activityLog( "Auto-Debit : Invoice payment InvoiceID#".$invoiceID.", BookingID#".$bookdata->InvestmentBookingID." Amounting ($".number_format( $invoice['Total'], 2).") has been paid." );
                    }
                }
            }
        }

        return $invoiceID;
    }

    function markInterestBookedData( $ID, $clientProfileID )
    {
        $where = array( 'ClientProfileID' => $clientProfileID, 'InvestmentBookingID' => $ID );
        $data = array( 'BookingType' => 'Booking' );
        $this->db->update( 'bookings', $data, $where );

        return true;
    }

    function getTotalFundsReceived($clientProfileID)
    {
        $sql = "SELECT SUM(TotalAmountAttached) as TotalFundsReceived FROM bookings WHERE ClientProfileID=$clientProfileID";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data = $row;
        }
        unset($query);

        return $data;
    }
    
    function doSave( $disabled = false )
    {
        $acctID = false;

        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "updateclientprofile": {
                    $data = $this->post;
                    $status = $data['status'];

                    unset( $data['action'] );
                    unset( $data['status'] );

                    $clientuserid = isset( $data['userid'] ) ? $data['userid'] : '0';
                    $clientprofileid = isset( $data['clientid'] ) ? $data['clientid'] : '0';
                    
                    // save client profiles
                    if( isset( $data['client'] ) ){
                        $client = $data['client'];

                        if( isset( $client['Price'] ) ){
                            $client['Price'] = (float) $client['Price'] == 0.00 ? '1' : $client['Price'];
                        }
                        
                        if( isset( $client['Video'] ) ){
                            $vid = trim( $client['Video'] );
                            if( substr( $vid, 0, 7 ) != '<iframe' ){
                                $client['Video'] = '<iframe width="560" height="315" src="'.$vid.'" frameborder="0" allow="autoplay;encrypted-media" allowfullscreen></iframe>';
                            }
                            header('X-XSS-Protection:0');
                        }

                        if( $status != 'Approved' || User::can( 'Manage Profiles' ) ){                            
                            if( $clientprofileid == '0' ){
                                //insert new entry in client_profiles
                                $clientprofileid = $this->db->insert( 'client_profiles', ['UserID' => $clientuserid] );
                            } else {
                                foreach( $client as $key => $value ){
                                    if( $key != 'Video' ){
                                        $temp = AppUtility::sanitizeEndingBRTags( $value ); // remove first ending <br> tags
                                        $client[$key] = AppUtility::sanitizeEndingPTags( $temp ); // remove ending empty <p> tags
                                    }
                                }
                                $uwhere = array( 'ClientProfileID' => $clientprofileid );
                                $this->db->update( "client_profiles", $client, $uwhere );
                            }
                        }
                    }
                    
                    // save bank data
                    if( isset( $data['bank'] ) ){
                        $bank = $data['bank'];
                        if( $status != 'Approved' || User::can( 'Manage Bank Accounts' ) ){
                            if( $data['bankid'] == '0' ){
                                //insert new entry in bank_accounts
                                $bank['UserID'] = $clientuserid;
                                $this->db->insert( 'bank_accounts', $bank );
                            } else {
                                $bwhere = array( 'UserID' => $clientuserid );
                                $this->db->update( "bank_accounts", $bank, $bwhere );
                            }
                        }
                    }

                    // save file data for client profile
                    if( isset( $this->file ) && !isset( $data['team'] ) ){
                        if( $status != 'Approved' || User::can( 'Manage Profiles' ) ){
                            $filedata = $this->fileUpload( $this->file, $clientuserid );
                            if( count( $filedata ) ){
                                if( $data['section'] == 'documents' ){
                                    foreach( $filedata as $key => $value ){
                                        $fdata = [];
                                        $fdata['FileID'] = $value;
                                        $fdata['ReferenceID'] = $clientprofileid;
                                        $fdata['GroupName'] = 'Clients';
                                        $fdata['DocumentName'] = $data['DN-'.$key];
                                        $rowCount = $this->db->insert( "file_groups", $fdata );
                                    }
                                } else {
                                    $rowCount = $this->db->update( "client_profiles", $filedata, $uwhere );
                                }
                            }
                            
                            //update if documment name only was changed
                            $docNames = isset( $data['docNames'] ) ? $data['docNames'] : false;
                            if( $docNames ){
                                foreach( $docNames as $docName ){
                                    $data = explode( '|', $docName );
                                    $where = array( 'FileGroupID' => $data[0] );
                                    $fdata = array( 'DocumentName' => $data[1] );
                                    $rowCount = $this->db->update( "file_groups", $fdata, $where );
                                }
                            }
                        }
                    }

                    // save team members
                    if( isset( $data['team'] ) ){
                        $update = false;
                        $team = $data['team'];
                        $oldphoto = $data['OldPhoto'];
                        $teamID = (isset( $team['ClientTeamID'] ) && $team['ClientTeamID'] != '0') ? $team['ClientTeamID'] : false;

                        $team['ClientUserID'] = (isset( $team['ClientUserID'] ) && $team['ClientUserID'] != '0') ? $team['ClientUserID'] : $clientuserid;
                        if( $teamID ){
                            $update = true;
                            unset( $team['ClientTeamID'] );
                        } else {
                            $teamID = $this->db->insert( "clients_team", $team );
                            $this->setSession( 'message', "New client team member has been added!" );
                            App::activityLog( "Added client team member ID#$teamID." );
                        }
                        if( $teamID ){
                            $where = array( 'ClientTeamID' => $teamID );
                            if( $update ){
                                $this->db->update( "clients_team", $team, $where );

                                $this->setSession( 'message', "Client team member has been updated!" );
                                App::activityLog( "Updated client team member ID#$teamID." );
                            }

                            // TO DO: send email
                            // $filedata = $this->sendEmail($teamID);
                            if( isset( $this->file['Photo']['name'] ) && $this->file['Photo']['name'] != '' ){
                                $filedata = $this->fileUpload( $this->file, $team['ClientUserID'] );
                                if( count( $filedata ) ){
                                    $this->db->update( "clients_team", $filedata, $where );
                                    if( $update ){ AppUtility::deleteFileAndAssociates( $oldphoto ); } // delete old photo
                                }
                            }
                        }
                    }
                    
                    //$this->updateUserInfo();
                    Lang::init();
                    $this->setSession( 'message', "Client profile data has been updated!" );
                    App::activityLog( "Updated client profile data ID#$clientprofileid." );
                } break;

                case "updateclientteam": {
                    $data = $this->post;
                    unset( $data['action'] );
                    
                    // save team members
                    if( isset( $data['team'] ) ){
                        $team = $data['team'];
                        $oldphoto = $data['OldPhoto'];
                        $teamID = isset( $team['ClientTeamID'] ) ? $team['ClientTeamID'] : '0';

                        unset( $team['ClientTeamID'] );
                        unset( $data['OldPhoto'] );
                        
                        $team['ClientUserID'] = isset( $team['ClientUserID'] ) ? $team['ClientUserID'] : '0';
                        $where = array( 'ClientTeamID' => $teamID );                        
                        $this->db->update( "clients_team", $team, $where );

                        // TO DO: send email
                        // $filedata = $this->sendEmail($teamID);
                        if( isset( $this->file['Photo']['name'] ) && $this->file['Photo']['name'] != '' ){
                            $filedata = $this->fileUpload( $this->file, $team['ClientUserID'] );
                            if( count( $filedata ) ){
                                $this->db->update( "clients_team", $filedata, $where );
                                AppUtility::deleteFileAndAssociates( $oldphoto ); // delete old photo
                            }
                        }
                    }

                    $this->setSession( 'message', "Client team member has been updated!" );
                    App::activityLog( "Updated client team member ID#$teamID." );
                } break;

                case "addnewpost": {
                    $blogdata = $this->post;
                    $microtime = isset( $blogdata['microtime'] ) ? $blogdata['microtime'] : '0';

                    unset( $blogdata['action'] );
                    unset( $blogdata['microtime'] );

                    if( mb_stripos( $microtime, 'microtime:' ) !== false ){
                        $previewdata = self::getBlogPostBySlug( $microtime );
                    }
                    $blogID = isset( $previewdata->BlogID ) ? $previewdata->BlogID : false;
                    $FImage = isset( $previewdata->FeaturedImage ) ? $previewdata->FeaturedImage : false;
                    $where = array( 'BlogID' => $blogID );

                    if( $blogdata['BlogStatus'] == "Published" ){
                        $blogdata['BlogDatePublished'] = date('Y-m-d H:i:s');
                    }

                    if( !$FImage ){
                        if( isset( $this->file['FeaturedImage']['name'] ) && $this->file['FeaturedImage']['name'] != '' ){
                            //$this->setSession('message',"File has been uploaded !");
                            $userID = $this->post['UserID'];
                            $fileData = $this->fileUpload( $this->file,$userID,'',false,6,0,false,'x' );
                            $fileID = $fileData['FeaturedImage'];
                            $blogdata['FeaturedImage'] = $fileID;
                        }
                    } else {
                        $blogdata['FeaturedImage'] = $previewdata->FeaturedImage;
                    }

                    $blogdata['BlogSlug'] = str_replace( " ", "-", strtolower( $blogdata['BlogTitle'] ) );
                    $blogdata['BlogSlug'] = AppUtility::cleanstring($blogdata['BlogSlug']);
                    if( $blogID ){
                        $blogID = $this->db->update( "blogs", $blogdata, $where );
                    } else {
                        $blogID = $this->db->insert( "blogs", $blogdata );
                    }

                    $this->setSession( 'message', 'New Blog Post has been added!' );
                    App::activityLog("Added blog #BlogID-".$blogID.'.');
                } break;

                case "editpost": {
                    $blogdata = $this->post;
                    $blogID = isset( $blogdata['BlogID'] ) ? $blogdata['BlogID'] : '0';
                    $oldFeaturedImage = isset( $blogdata['OldFeaturedImage'] ) ? $blogdata['OldFeaturedImage'] : '0';

                    unset( $blogdata['action'] );
                    unset( $blogdata['OldFeaturedImage'] );

                    $where = array('BlogID' => $blogID);

                    if( isset( $this->file['FeaturedImage']['name'] ) && $this->file['FeaturedImage']['name'] != '' ){
                        $this->setSession( 'message', "File has been uploaded!" );
                        $userID = $this->post['UserID'];
                        $fileData = $this->fileUpload( $this->file,$userID,'',false,6,0,false,'x' );
                        $fileID = $fileData['FeaturedImage'];
                        $blogdata['FeaturedImage'] = $fileID;
                        AppUtility::deleteFileAndAssociates( $oldFeaturedImage ); //delete old featured image and its associated files
                    }

                    $blogdata['BlogSlug'] = str_replace(" ", "-", strtolower($blogdata['BlogTitle']));
                    $this->db->update( "blogs", $blogdata, $where );

                    $this->setSession( 'message', "Blog post changes have been saved!" );
                    App::activityLog( "Edited blog article #BlogID-".$blogID.'.' );
                } break;

                case "updatecommrate": {
                    $data = $this->post;
                    $where = array( 'UserID' => $data['UserID'] );

                    $commData['CommissionRate'] = $data['CommissionRate'];
                    $commData['SubscriptionRate'] = $data['SubscriptionRate'];
                    $commData['UpdatedAt'] = date('Y-m-d H:i:s');
                    $this->db->update("user_referrer", $commData, $where);

                    $this->setSession( 'message', "Commission/Subscription Rate has been updated!" );
                    App::activityLog( "Updated commission/subscription rate for #UserID-".$data['UserID'].'.' );
                } break;

                case "uploadttreceipt": {
                    $data = $this->post;

                    if( isset( $this->file['TTReceipt']['name']) && $this->file['TTReceipt']['name'] != '' ){
                        $this->setSession( 'message', "File has been uploaded!" );
                        $userID = $this->post['UserID'];
                        $fileData = $this->fileUpload( $this->file, $userID, '', false, 6, 0, false, 'x' );
                        $fileID = $fileData['TTReceipt'];

                        $where = array( 'UserID' => $userID, 'InvoiceID' => $data['InvoiceID'] );
                        $invData['TTReceipt'] = $fileID;
                        $invData['UpdatedAt'] = date( 'Y-m-d H:i:s' );
                        $invData['Status'] = 'TT Receipt';
                        $this->db->update( "invoices", $invData, $where );
                    }

                    $this->setSession( 'message',"Invoice TT Receipt has been uploaded!" );
                    App::activityLog( "Uploaded TT receipt for #InvoiceID-".$data['InvoiceID'].'.' );

                    View::redirect( 'clients/invoices' );
                } break;

                case "sendcommentview":
                case "sendcommentdashboard": {
                    $commentdata = $this->post;

                    unset( $commentdata['action'] );

                    $commentdata['ReferenceID'] = $disabled;
                    $commentdata['CommentDate'] = date( 'Y-m-d H:i:s' );
                    if( $disabled ){
                        $commentID = $this->db->insert( "comments", $commentdata );
                    }

                    $this->setSession( 'message',"New comment has been added!" );
                } break;

                case "addclientcategory": {
                    $data = $this->post;
                    $catdata = $this->post['category'];

                    unset( $data['action'] );

                    $catdata['UserID'] = User::info('UserID');
                    $catID = $this->db->insert( "client_categories", $catdata );

                    if( $catID ){
                        $this->setSession( 'message', "New client category has been added!" );
                        App::activityLog( "Added client category #ID-".$catID.'.' );
                    }
                } break;

                case "updateclientcategory": {
                    $data = $this->post;
                    $categoryid = $data['categoryid'];
                    $catdata = $this->post['category'];

                    unset( $data['action'] );

                    $where = array( 'ClientCatID' => $categoryid );
                    $catID = $this->db->update( "client_categories", $catdata, $where );

                    if( $catID ){
                        $this->setSession( 'message', "Client category has been updated!" );
                        App::activityLog( "Updated client category #ID-".$categoryid.'.' );
                    }
                } break;

                default: break;
            }
        }

        return $acctID;
    }

    function doSaveAjax()
    {
        if( $this->post['action'] == 'updateclientprofile' ){

            $data = $this->post;
            if( $this->post['clientid']=='0' ){
                //insert new entry in client_profiles
                $newClientID = $this->db->insert( 'client_profiles',['UserID'=> $this->post['userid']] );
                $uwhere = array('ClientProfileID' => $newClientID);
            } else {
                $uwhere = array('ClientProfileID' => $this->post['clientid']);
            }

            //unset($data['action']);

            if( isset( $this->post['client'] ) ){
                $client = $this->post['client'];

                if( isset($this->post['client']['Video'] ) ){
                    header( 'X-XSS-Protection:0' );
                }
                if( $this->post['section']=='companyinfo' ){
                    $data = ['CompanyName'=>$client['CompanyName'],
                        'VideoDescription'=>$client['VideoDescription'],
                        'Video'=>$client['Video']
                    ];
                } elseif( $this->post['section'] == 'offer' ){
                    $data = [
                        'TypeOfOffer'=>$client['TypeOfOffer'],
                        'OfferOpening'=>$client['OfferOpening'],
                        'OfferClosing'=>$client['OfferClosing'],
                        'Price'=>$client['Price'],
                        'TargetGoal'=>$client['TargetGoal'],
                        'SizeOfOffer'=>$client['SizeOfOffer'],
                        'MinimumBid'=>$client['MinimumBid'],
                        'MaximumBid'=>$client['MaximumBid'],
                        'LeadManager'=>$client['LeadManager'],
                    ];

                } elseif( $this->post['section'] == 'summary' ){
                    $data = [
                        'ExecutiveSummary'=>$client['ExecutiveSummary'],
                        'OfferOverview'=>$client['OfferOverview'],
                        'BusinessModel'=>$client['BusinessModel'],
                        'KeyInvestHighlights'=>$client['KeyInvestHighlights'],
                        'StrategyVision'=>$client['StrategyVision'],
                        'MarketDemand'=>$client['MarketDemand'],
                        'BoardManagement'=>$client['BoardManagement'],
                        'UsageOfFunds'=>$client['UsageOfFunds'],
                        'FinancialSummary'=>$client['FinancialSummary'],
                        'PressCoverage'=>$client['PressCoverage'],
                        'Disclosure'=>$client['Disclosure'],
                    ];
                }
                $res = $this->db->update( "client_profiles",  $data, $uwhere );
            }
            header( 'Content-type: application/json' );
            //$array = $this->post;
            $array['section'] = $this->post['section'];
            if( $res ){
                $array['success'] = true;
                $array['message'] = 'You have successfully saved your profile!';
            } else {
                $array['error'] = true;
                $array['message'] = 'Failed saving your profile! No changes made. Please try again';
            }

            echo json_encode( $array );
        }
    }
    
    function doSavePreview( $previewa = [] )
    {
        $blogID = false;        
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "addnewpost": {
                    $data = isset( $this->post ) ? $this->post : false;
                    $microtime = $data['microtime'];

                    unset( $data['action'] );
                    unset( $data['microtime'] );
                    
                    if( $data ){
                        foreach( $data as $k => $v ){
                            $previewa[$k] = $v;
                        }
                    }
                    if( isset( $this->file['FeaturedImage']['name']) && $this->file['FeaturedImage']['name'] != '' ){
                        $userID = $data['UserID'];
                        $fileData = $this->fileUpload( $this->file, $userID, '', false, 6, 0, false, 'x' );
                        $fileID = $fileData['FeaturedImage'];
                        $previewa['FeaturedImage'] = $fileID;
                    }

                    if( count( $previewa ) > 0 ){
                        $previewa['BlogSlug'] = AppUtility::cleanstring($previewa['BlogSlug']);
                        $blog = array( 'BlogSlug' => 'microtime:'.$microtime, 'Preview' => json_encode( $previewa ) );                 

                        $blogID = $this->db->insert( "blogs", $blog );
                    }
                    
                    App::activityLog( "Submitted blog post preview with BlogID#$blogID." );
                } break;

                case "editpost": {
                    $data = isset( $this->post ) ? $this->post : false;
                    $blogID = isset( $data['BlogID'] ) ? $data['BlogID'] : false;

                    unset( $data['action'] );
                    unset( $data['BlogID'] );
                    
                    if( $data ){
                        foreach( $data as $k => $v ){
                            $previewa[$k] = $v;
                        }
                    }
                    if( $data['BlogStatus'] == "Published" ){
                        $previewa['BlogDatePublished'] = date('Y-m-d H:i:s');
                    }
                    if( isset( $this->file['FeaturedImage']['name']) && $this->file['FeaturedImage']['name'] != '' ){
                        $userID = $data['UserID'];
                        $fileData = $this->fileUpload( $this->file, $userID, '', false, 6, 0, false, 'x' );
                        $fileID = $fileData['FeaturedImage'];
                        $previewa['FeaturedImage'] = $fileID;
                    }

                    if( count( $previewa ) > 0 ){
                        $where = array( 'BlogID' => $blogID );
                        $blog = array( 'Preview' => json_encode( $previewa ) );
                        $this->db->update( "blogs", $blog, $where );
                    }
                    
                    App::activityLog( "Submitted blog post preview with BlogID#$blogID." );
                } break;

                case 'publishpreviewpost': {
                    $blog = [];
                    $data = isset( $this->post ) ? $this->post : false;
                    $blogID = isset( $data['BlogID'] ) ? $data['BlogID'] : false;
                    $microtime = isset( $data['microtime'] ) ? $data['microtime'] : '0';
                    $oldFeaturedImage = isset( $previewa['OldFeaturedImage'] ) ? $previewa['OldFeaturedImage'] : '0';

                    unset( $data['action'] );
                    unset( $data['BlogID'] );
                    unset( $data['microtime'] );
                    unset( $previewa['OldFeaturedImage'] );

                    $where = array( 'BlogID' => $blogID );
                    foreach( $previewa as $k => $v ){
                        $blog[$k] = $v;
                    }
                    if( $blog['BlogStatus'] == "Published" ){
                        $blog['BlogDatePublished'] = date('Y-m-d H:i:s');
                    }
                    if( isset( $previewa['FeaturedImage'] ) && isset( $previewa['OldFeaturedImage'] ) ){
                        AppUtility::deleteFileAndAssociates( $oldFeaturedImage ); //delete old featured image and its associated files
                    }
                    $blog['Preview'] = '';
                    $blog['BlogSlug'] = str_replace( " ", "-", strtolower( $blog['BlogTitle'] ) );
                    $blog['BlogSlug'] = AppUtility::cleanstring($blog['BlogSlug']);
                    $this->db->update( "blogs", $blog, $where );

                    View::redirect( 'clients/blog/manage' );
                } break;

                default: break;
            }
        }

        return $blogID;
    }
    
    function doSaveChangeProfile( $requesta, $clientprofileid )
    {
        $acctID = false;        
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "clientchangerequestOLD": {
                    $data = $this->post;
                    $client = isset( $data['client'] ) ? $data['client'] : false;
                    $video = isset( $data['client']['Video'] ) ? $data['client']['Video'] : false;
                    $bank = isset( $data['bank'] ) ? $data['bank'] : false;
                    $bankid = isset( $data['bankid'] ) ? $data['bankid'] : '0';

                    unset( $data['action'] );
                    unset( $data['status'] );
                    if( isset( $client['CompanyName'] ) ) unset( $client['CompanyName'] );

                    $clientuserid = isset( $data['userid'] ) ? $data['userid'] : '0';
                    
                    if( $client ){
                        if( $video ){ header('X-XSS-Protection:0'); }
                        foreach( $client as $k => $v ){
                            $requesta['client'][$k] = $v;
                        }
                    }

                    $filedata = $this->fileUpload( $this->file, $data['userid'] );
                    if( count( $filedata ) ){
                        if( $data['section'] == 'documents' ){
                            foreach( $filedata as $key => $value ){
                                $fdata = [];
                                $fdata['FileID'] = $value;
                                $fdata['ReferenceID'] = $clientprofileid;
                                $fdata['GroupName'] = 'Clients';
                                $fdata['DocumentName'] = $data['DN-'.$key];
                                $rowCount = $this->db->insert( "file_groups", $fdata );
                            }
                        } else {
                            foreach( $filedata as $k => $v ){
                                $requesta['client'][$k] = $v;
                            }
                        }
                    }
                    
                    //update if documment name only was changed
                    $docNames = isset( $data['docNames'] ) ? $data['docNames'] : false;
                    if( $docNames ){
                        foreach( $docNames as $docName ){
                            $data = explode( '|', $docName );
                            $where = array( 'FileGroupID' => $data[0] );
                            $fdata = array( 'DocumentName' => $data[1] );
                            $rowCount = $this->db->update( "file_groups", $fdata, $where );
                        }
                    }

                    if( $bank ){
                        $requesta['bank']['BankAccountID'] = $bankid;
                        foreach( $bank as $k => $v ){
                            $requesta['bank'][$k] = $v;
                        }
                    }

                    if( count( $requesta ) > 1 )
                    {
                        $requesta['RequestDate'] =  date('Y-m-d H:i:s');
                        $data = array( 'ChangeRequest' => json_encode( $requesta ) );
                        $this->db->update( "client_profiles", $data, $where );

                        $this->setSession( 'message', "Client profile change request has been submitted!" );
                        App::activityLog( "Submitted client profile change request ID#".$clientprofileid."." );
                    }                    
                    Lang::init();
                } break;

                case "clientchangerequest": {
                    $data = $this->post;
                    $client = isset( $data['client'] ) ? $data['client'] : false;
                    $video = isset( $data['client']['Video'] ) ? $data['client']['Video'] : false;
                    $bank = isset( $data['bank'] ) ? $data['bank'] : false;

                    unset( $data['action'] );
                    unset( $data['status'] );
                    if( isset( $client['CompanyName'] ) ) unset( $client['CompanyName'] );

                    $clientuserid = isset( $data['userid'] ) ? $data['userid'] : '0';
                    $request = array( 'ClientProfileID' => $clientprofileid );
                    $dateAt = array(
                        'ClientProfileID' => $clientprofileid,
                        'FieldName' => 'RequestDate',
                        'FieldValue' => date('Y-m-d H:i:s')
                    );
                    
                    if( $client ){
                        if( $video ){ header('X-XSS-Protection:0'); }
                        $request['Section'] = 'client';
                        foreach( $client as $k => $v ){
                            $request['FieldName'] = $k;
                            $request['FieldValue'] = $v;
                            $this->db->insert( "client_profiles_request", $request );
                        }
                        $this->db->insert( "client_profiles_request", $dateAt );
                    }

                    $filedata = $this->fileUpload( $this->file, $data['userid'] );
                    if( count( $filedata ) ){
                        if( $data['section'] == 'documents' ){
                            foreach( $filedata as $key => $value ){
                                $fdata = [];
                                $fdata['FileID'] = $value;
                                $fdata['ReferenceID'] = $clientprofileid;
                                $fdata['GroupName'] = 'Clients';
                                $fdata['DocumentName'] = $data['DN-'.$key];
                                $this->db->insert( "file_groups", $fdata );
                            }
                        } else {
                            $request['Section'] = 'client';
                            foreach( $filedata as $k => $v ){
                                $request['FieldName'] = $k;
                                $request['FieldValue'] = $v;
                                $this->db->insert( "client_profiles_request", $request );
                            }
                            if( !$client ){
                                $this->db->insert( "client_profiles_request", $dateAt );
                            }
                        }
                    }
                    
                    //update if documment name only was changed
                    $docNames = isset( $data['docNames'] ) ? $data['docNames'] : false;
                    if( $docNames ){
                        foreach( $docNames as $docName ){
                            $data = explode( '|', $docName );
                            $where = array( 'FileGroupID' => $data[0] );
                            $fdata = array( 'DocumentName' => $data[1] );
                            $this->db->update( "file_groups", $fdata, $where );
                        }
                    }

                    if( $bank ){
                        $request['Section'] = 'bank';
                        foreach( $bank as $k => $v ){
                            $request['FieldName'] = $k;
                            $request['FieldValue'] = $v;
                            $this->db->insert( "client_profiles_request", $request );
                        }

                        //insert request date
                        $this->db->insert( "client_profiles_request", $dateAt );
                    }

                    $this->setSession( 'message', "Client profile change request has been submitted!" );
                    App::activityLog( "Submitted client profile change request ID#$clientprofileid." );

                    //delete any duplicate
                    $sql = "DELETE cpr1 FROM `client_profiles_request` cpr1, `client_profiles_request` cpr2
                    WHERE cpr1.`ClientProfileRequestID` < cpr2.`ClientProfileRequestID` AND cpr1.`FieldName` = cpr2.`FieldName` AND cpr1.`ClientProfileID` = $clientprofileid";
                    $this->db->query( $sql );
                    
                    Lang::init();
                } break;

                case 'approveclientchangerequestOLD': {
                    $bank = [];
                    $client = [];

                    if( User::can( 'Manage Profiles' ) ){
                        $where = array( 'ClientProfileID' => $clientprofileid );

                        foreach( $requesta['client'] as $k => $v ){
                            $client[$k] = $v;
                        }
                        unset( $requesta['client'] );
                        $client['ChangeRequest'] = json_encode( $requesta );
                        $this->db->update( "client_profiles", $client, $where );
                    }

                    if( User::can( 'Manage Bank Accounts' ) ){
                        $bankid = isset( $requesta['bank']['BankAccountID'] ) ? $requesta['bank']['BankAccountID'] : '0';
                        $where = array( 'BankAccountID' => $bankid );
                        unset( $requesta['bank']['BankAccountID'] );

                        foreach( $requesta['bank'] as $k => $v ){
                            $bank[$k] = $v;
                        }
                        $this->db->update( "bank_accounts", $bank, $where );

                        unset( $requesta['bank'] ); //remove bank details from request array
                        $client = [];
                        $where = array( 'ClientProfileID' => $clientprofileid );
                        $client['ChangeRequest'] = json_encode( $requesta );
                        $this->db->update( "client_profiles", $client, $where );

                        $this->setSession( 'message', "Client bank info change request has been saved!" );
                        App::activityLog( "Approved client's bank info change request #ID-".$clientprofileid."." );
                        if( User::role( 'Accounting' ) ) { View::redirect( 'invoices/banks' ); }
                    }

                    $this->setSession( 'message', "Client profile change request has been approved!" );
                    App::activityLog( "Approved client profile change request ID#".$clientprofileid."." );
                    View::redirect( 'cs/clientprofiles' );
                } break;

                case 'approveclientchangerequest': {
                    $bank = [];
                    $client = [];
                    $requests = self::getProfileChangeRequest( $clientprofileid ); // get data from request table
                    // $bankrequests = self::getProfileChangeRequest( $clientprofileid, 'bank' ); // get data from request table
                    // $clientrequests = self::getProfileChangeRequest( $clientprofileid, 'client' ); // get data from request table

                    if( User::can( 'Manage Profiles' ) ){
                        $where = array( 'ClientProfileID' => $clientprofileid );
                        if( isset( $requests ) && count( $requests ) ){
                            $client['UpdatedAt'] = date( 'Y-m-d H:i:s' );
                            foreach( $requests as $k => $request ){
                                if( $request->Section == 'client' ){ $client[$k] = $request->FieldValue; }
                            }
                            $this->db->update( "client_profiles", $client, $where ); // update client profiles

                            // delete client profile change request
                            $where['Section'] = 'client';
                            $this->db->delete( "client_profiles_request", $where );

                            $this->setSession( 'message', "Client profile change request has been approved!" );
                            App::activityLog( "Approved client profile change request ID#$clientprofileid." );
                        }
                    }

                    if( User::can( 'Manage Bank Accounts' ) ){
                        $bankid = false;
                        if( isset( $requests ) && count( $requests ) ){
                            foreach( $requests as $k => $request ){
                                if( $request->Section == 'bank' ){
                                    if( $request->FieldName == 'BankAccountID' ){
                                        $bankid = $request->FieldValue;
                                    } else {
                                        $bank[$k] = $request->FieldValue;
                                    }
                                }
                            }
                            if( $bankid == '0' ){
                                $bankid = false;
                                $bankid = $this->db->insert( "bank_accounts", $bank );
                                if( $bankid ){
                                    // delete client bank info request
                                    $where = array( 'ClientProfileID' => $clientprofileid, 'Section' => 'bank' );
                                    $this->db->delete( "client_profiles_request", $where );

                                    $this->setSession( 'message', "Client($clientprofileid) bank info has been added!" );
                                    App::activityLog( "Added client's($clientprofileid) bank info - BankAccountID#$bankid." );
                                }
                            } else {
                                $where = array( 'BankAccountID' => $bankid );
                                $this->db->update( "bank_accounts", $bank, $where ); // update client bank info

                                // delete client bank info request
                                $where = array( 'ClientProfileID' => $clientprofileid, 'Section' => 'bank' );
                                $this->db->delete( "client_profiles_request", $where );

                                $this->setSession( 'message', "Client bank info change request has been approved!" );
                                App::activityLog( "Approved client's bank info change request ID#$clientprofileid." );
                            }
                        }
                        if( User::can( 'Manage Invoices' ) ) { View::redirect( 'invoices/banks' ); }
                    }
                    View::redirect( 'cs/clientprofiles' );
                } break;

                default:
                    View::redirect( 'cs/clientprofiles' );
                    break;
            }
        }

        return $acctID;
    }

    /**
     * Save booking commission payment
     *
     * @package
     * @access Private
     * @param (int) $clientUserID : (required) client user id
     * return NA
     **/
    function doSaveInvoicePayment( $clientUserID )
    {
        $invoiceID = false;        
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "payout":
                case 'paydue': {
                    $data = $this->post;
                    $iIDs = $data['iIDs'];
                    unset( $data['action'] );

                    $totalcomm = 0.00;
                    if( isset( $iIDs ) && $iIDs ){
                        foreach( $iIDs as $iID ){                            
                            if( isset( $iID ) ){
                                $totalcomm += $this->doSaveInvoicePaymentProcess( $iID, $clientUserID );
                            }
                        }
                    }

                    $this->setSession( 'message', "Your invoice payment with InvoiceIDs (".implode( ', ', $iIDs ).") and total amount of ($".number_format( $totalcomm, 2, "." ,"" ).") has been submitted! An Auto-Debit has been processed also from your wallet." );
                    
                } break;
                
                case 'paysingle': {
                    $data = $this->post;
                    $invoiceID = $data['InvoiceID'];
                    unset( $data['action'] );
                    
                    $totalcomm = 0.00;
                    if( isset( $invoiceID ) ){
                        $totalcomm += $this->doSaveInvoicePaymentProcess( $invoiceID, $clientUserID );
                    }

                    $this->setSession( 'message', "Your invoice payment with InvoiceID#".$invoiceID." and an amount of ($".number_format( $totalcomm, 2, "." ,"" ).") has been submitted! An Auto-Debit has been processed also from your wallet." );
                    
                } break;

                default: break;
            }
        }

        return $invoiceID;
    }

    /**
     * Save booking commission payment
     *
     * @package
     * @access Private
     * @param (int) $invoiceID : (required) invoice id number
     * return NA
     **/
    function doSaveInvoicePaymentProcess( $invoiceID, $clientUserID )
    {
        $walletID = false;
        $totalcomm = 0.00;
        $invoice = $this->getClientInvoiceDetails( $invoiceID );
        if( isset( $invoice->InvoiceID ) ){
            $balancedue = isset( $invoice->BalanceDue ) ? $invoice->BalanceDue : '0.00';
            $invoiceID = isset( $invoice->InvoiceID ) ? $invoice->InvoiceID : '0';
            $investmentBookingID = isset( $invoice->InvestmentBookingID ) ? $invoice->InvestmentBookingID : '0';
            $investmentAmount = isset( $invoice->InvestmentAmount ) ? $invoice->InvestmentAmount : '0.00';

            $wallet['TransactionStatus'] = "Approved";
            $wallet['UserID'] = User::info( 'UserID' );
            $wallet['Method'] = '3'; //Wallet Transfer
            $wallet['TransactionDate'] = date('Y-m-d H:i:s');
            $wallet['ApproveDate'] = date('Y-m-d H:i:s');
            $wallet['Description'] = 'Auto-Debit: InvoiceID#'.$invoiceID.', BookingID#'.$investmentBookingID.', Investment Amount($'.number_format( $investmentAmount, 2 ).')';
            $wallet['TransactionType'] = 'Debit';
            $wallet['TransactionAmount'] = str_replace(',', '', $balancedue );

            $mybalance = AppUtility::getwalletbalance( $clientUserID );
            if( isset( $mybalance ) ){
                if( floatval( $mybalance ) >= 0 ){
                    $walletID = $this->db->insert( "wallet", $wallet ); //auto-debit from wallet
                }

                if( $walletID ){
                    $totalcomm += floatval( $balancedue );

                    //update invoice table
                    $invoice = array( 'Status' => 'Paid', 'UpdatedAt' => date('Y-m-d H:i:s') );
                    $where = array( 'InvoiceID' => $invoiceID );
                    $invoiceID = $this->db->update( "invoices", $invoice, $where );

                    App::activityLog( "Invoice payment with InvoiceID#".$invoiceID." and total amount of ($".number_format( $totalcomm, 2, "." ,"" ).") has been submitted. Auto-Debit has been processed also." );
                }
            }
        }

        return $totalcomm;
    }

    function getRecentPosts( $clientProfileID, $userID = NULL, $categoryID = null, $searchString = null )
    {
		$sql = "SELECT * FROM (
            SELECT DISTINCT bl.*, GetFileSlug( bl.`FeaturedImage` ) AS FISlug
            FROM `blogs` bl
			LEFT JOIN `bookings` bo ON ( bo.`ClientProfileID` = bl.`ClientProfileID` AND bo.`BookingStatus` = 'Approved' AND bo.`UserID` = '$userID' )
			LEFT JOIN blog_categories as bc ON bc.`BlogCatID` = bl.`BlogCatID` 
			WHERE bl.`ParentBlogID` = 0 AND bl.`ClientProfileID` = $clientProfileID
			AND bl.`BlogStatus` = 'Published'
			AND bl.`PublicView` = 1
			AND bo.`UserID` IS NOT NULL";
		if( $categoryID ){
            $sql .= " AND bl.BlogCatID = $categoryID ";
        }
        if( $searchString ){
            $sql .= " AND bl.BlogTItle LIKE '%$searchString%' ";
        }

		$sql .= " UNION ALL 
            SELECT DISTINCT *, GetFileSlug( `FeaturedImage` ) AS FISlug
            FROM `blogs`
			WHERE `ParentBlogID` = 0 AND `ClientProfileID` = $clientProfileID
			AND `BlogStatus` = 'Published'
			AND `PublicView` = 0 ";
        if( $categoryID ){
            $sql .= " AND BlogCatID = $categoryID ";
        }
        if( $searchString ){
            $sql .= " AND BlogTItle LIKE '%$searchString%' ";
        }

        $sql .= "
        	 ) clientview
		ORDER BY `BlogDatePublished` DESC";
 
        // set user to null if owner or admins
        if( $userID == NULL ){
            $sql = "SELECT *, GetFileSlug( `FeaturedImage` ) AS FISlug FROM blogs WHERE `ParentBlogID` = 0 AND ClientProfileID = $clientProfileID AND BlogStatus = 'Published'";

            if( $categoryID ){
                $sql .= " AND BlogCatID = '$categoryID'";
            }

            if( $searchString ){
                $sql .= " AND BlogTItle LIKE '%$searchString%'";
            }

            $sql .= " ORDER BY BlogDatePublished DESC";
        }
        
        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getCategoryIDBySlug( $category, $clientProfileID = 0 )
    {
        $sql = "SELECT * FROM blog_categories WHERE CategorySlug = '$category' AND ClientProfileID = '$clientProfileID'";
        $data = $this->db->get_row($sql);

        return $data->BlogCatID;
    }

    function doFileUploadAjax()
    {
        $userID = User::info('UserID');

        $res = $this->fileUpload($this->file,$userID,'',false,6,0,false,'x');
        foreach ($res as $key => $value) {
            $fileID = $value;
        }
        $filedata = $this->getUploadedFiles($fileID);
        $files['files'] = $filedata;
        $array['result'] = $files;
        $array['success'] = true;
        $array['message'] = 'uploaded!';
        echo json_encode($array);
    }

    function doClientCatDelete( $ClientCatID )
    {
        $where = array( 'ClientCatID' => $ClientCatID );
        $rowCount = $this->db->delete( "client_categories", $where );
        if( $rowCount ){
            $this->setSession( 'message', "Client category has been deleted!" );
            App::activityLog(" Deleted client category #ID".$ClientCatID.'.' ); 
        }
    }
    
    function doApprove( $UserID )
    {
        $where = array( 'AccountID' => $UserID );
        $rowCount = $this->db->update( "accounts", array( 'AccountStatus' => 'Approved' ), $where );
        if( $rowCount ){
            App::activityLog( "Upproved account #ID-".$UserID.'.' );
        }
    }
    
    function doTrash( $UserID )
    {
        $where = array( 'AccountID' => $UserID );
        $data = array( 'Active' => 0 );
        $rowCount = $this->db->update( "accounts", $data, $where );
        if( $rowCount ){
            App::activityLog( "Trashed account #ID-".$UserID.'.' );
        }        
    }

    function doDeleteFaqPost( $ID )
    {
        $where = array( 'FaqID' => $ID );
        $rowCount = $this->db->delete( "faqs", $where );
        if( $rowCount ){
            App::activityLog( "Deleted Faq #ID-".$ID.'.' );
        }
    }

    function doDeleteBlogPost( $blogPostID )
    {
        $where = array( 'BlogID' => $blogPostID );
        $rowCount = $this->db->delete( "blogs", $where );
        if( $rowCount ){            
            App::activityLog( "Deleted blog post #ID-".$blogPostID.'.' );
        }
    }
    
    function doRestore( $UserID )
    {
        $where = array( 'AccountID' => $UserID );
        $data = array( 'Active' => 1 );
        $rowCount = $this->db->update( "accounts", $data, $where );
        if( $rowCount ){
            App::activityLog( "Restored account #ID-".$UserID.' from trash bin.' );
        }
    }
    
    function doDelete( $ID )
    {
        $where = array( 'AccountMetaID' => $ID );        
        $rowCount = $this->db->delete( "accounts", $where );
        $rowCount = $this->db->delete( "account_meta", $where );
        if( $rowCount ){
            App::activityLog( "Permanently deleted account #ID-".$UserID.'.' );
        }
    }

    function paymentdelete( $PID )
    {
        $where = array( 'PaymentID' => $PID );        
        $rowCount = $this->db->delete( "payments", $where );
        if( $rowCount ){
            App::activityLog( "Permanently deleted payment #PaymentID-".$PID.'.' );
        }
    }
    
    function doEmptyTrash( $ids )
    {
        foreach( $ids as $id ){
            $where = array( 'AccountMetaID' => $id );
            $uwhere = array( 'UserID' => $id );
            $rowCount = $this->db->delete( "accounts", $where );
            $rowCount = $this->db->delete( "account_meta", $where );
            // $rowCount = $this->db->delete( "users", $uwhere );
            // $rowCount = $this->db->delete( "user_meta", $uwhere );
            if( $rowCount ){
                App::activityLog( "Permanently deleted account #ID-".$id.'.' );
            }
        }
    }
    
    function doRejectFile( $ID )
    {
        $where = array( 'FileItemID' => $ID );
        $rowCount = $this->db->update( "file_items", array( 'Active' => 2 ), $where ); 
        if( $rowCount ){
            App::activityLog( "Rejected client file #ID-".$ID.'.' );
        }
    }
    
    function doApproveFile( $ID )
    {
        $where = array( 'FileItemID' => $ID );
        $rowCount = $this->db->update( "file_items", array( 'Active' => 1 ), $where );
        if( $rowCount ){
            App::activityLog( "Approved client file #ID-".$ID.'.' );
        }
    }
    
    function doDeleteFile( $ID )
    {
        // delete first the associated data in `file_groups` table, be sure that the passed $ID is referring to `FileItemID` of `file_items` table
        AppUtility::deleteFileGroupsData( $ID );

        $where = array( 'FileItemID' => $ID );
        $rowCount = $this->db->delete( "file_items", $where );

        if( $rowCount ){
            App::activityLog( "Deleted client file #ID-".$ID.'.' );
        }
    }
    
    function doEmailUpdate( $ID )
    {
        $where = array( 'AccountID' => $ID );
        $data = array( 'EmailSent' => date( 'Y-m-d H:i:s' ), 'EmailSentBy' => User::info( 'UserID' ) );
        $rowCount = $this->db->update( "accounts", $data, $where );
        if( $rowCount ){
            App::activityLog( "Sent client commencement letter #ID-".$ID.'.' );
        }
    }
    
    function getChildrenRecursive($ID)
    {
        $data = array();
        $sql = "SELECT u.UserID, u.Level, u.ReferrerUserID, um.FirstName, um.LastName, ul.Code, a.ApplicationDate, a.Commission, (SELECT SUM(aa.DepositedAmount) FROM accounts aa LEFT JOIN users uu ON aa.UserID = uu.UserID WHERE uu.ReferrerUserID = u.UserID) as Total FROM users u LEFT JOIN user_meta um ON u.UserID = um.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID LEFT JOIN accounts a ON a.UserID = u.UserID WHERE u.Active = 1 AND u.Level IN(2,3,4) AND u.ReferrerUserID = $ID";        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[$row->UserID] = $row;
        }
        unset($query);
        
        return $data;
    }
    
    function getChildren($ID)
    {
        $data = array();
        $childs = $this->getChildrenRecursive($ID);
        
        foreach( $childs as $child ){
            $data[$child->UserID]['data'] = $child;
            $data[$child->UserID]['children'] = $this->getChildren( $child->UserID );
        }
        
        $idArr = $this->getChildrenIds( $ID );
        
        return implode( ',', (array)$idArr );
    }
    
    function getChildrensData($ID)
    {
        $data = array();
        $childs = $this->getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $data[$child->UserID]['data'] = $child;
            $data[$child->UserID]['children'] = $this->getChildren($child->UserID);
        }
        
        return (count($data)) ? $data : '';
    }
    
    function getChildrenIds($ID)
    {
        $data = array();
        $childs = $this->getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $ch = $this->getChildrenIds($child->UserID);
            $data[] = $child->UserID;
            if(is_array($ch)) {
                $data[] = implode(',',$ch);
            }            
        }
        
        return (count($data)) ? $data : '';
    }
    
    function getChildrenIdArray($ID)
    {
        $data = array();
        $childs = $this->getChildrenRecursive($ID);
        
        foreach($childs as $child) {
            $data[$child->UserID] = $this->getChildrenIdArray($child->UserID);
            
        }
        
        return (count($data)) ? $data : '';
    }

    function getUserProjects()
    {
        $data = array();
        $sql = "SELECT ClientProfileID, UserID, VideoDescription, OfferOpening, OfferClosing, ExecutiveSummary, OfferOverview FROM client_profiles ORDER BY OfferOpening DESC";
        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientInvestors($userID)
    {
        $data = array();
        $sql = "SELECT b.InvestmentBookingID,b.TTReceiptPhoto, b.ClientProfileID, b.UserID, CONCAT(um.FirstName, ' ',um.LastName) as InvestorName, um.Avatar, u.Email, b.Status,b.SignedDate, b.TotalAmountAttached, b.SharesToApply 
                FROM bookings as b
                LEFT JOIN user_meta as um ON um.UserID = b.UserID
                LEFT JOIN users as u ON u.UserID = b.UserID
                WHERE b.clientProfileID
                ORDER BY um.FirstName ASC, um.LastName ASC";
        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientAffiliates($reffererUserID)
    {
        $data = array();
        $sql = "SELECT u.*, CONCAT(um.FirstName, ' ',um.LastName) as AffiliateName,um.FirstName,um.LastName,um.Avatar,um.Gender,um.Phone, ref.Status, ref.CommissionRate FROM users as u
                LEFT JOIN user_meta as um ON um.UserID = u.UserID
                LEFT JOIN user_referrer as ref ON ref.UserID = u.UserID
                WHERE u.ReferrerUserID = '$reffererUserID' AND u.Level IN ('4', '7')
                ORDER BY FirstName ASC, um.LastName ASC";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientActiveAffiliates($reffererUserID)
    {
        $data = array();
        $sql = "SELECT u.*, CONCAT(um.FirstName, ' ',um.LastName) as AffiliateName,um.FirstName,um.LastName,um.Avatar,um.Gender,um.Phone, ref.Status, ref.CommissionRate FROM users as u
                LEFT JOIN user_meta as um ON um.UserID = u.UserID
                LEFT JOIN user_referrer as ref ON ref.UserID = u.UserID
                WHERE u.ReferrerUserID = '$reffererUserID' AND ref.Status = 'Active' AND u.Level IN ('4', '7')
                ORDER BY FirstName ASC, um.LastName ASC";
        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getAffiliateDetails($userID)
    {
        $sql = "SELECT u.*, CONCAT(um.FirstName, ' ',um.LastName) as AffiliateName,um.FirstName,um.LastName,um.Avatar,um.Gender,um.Phone,um.Occupation,um.JobTitle,um.Bio, ref.Status, ref.CommissionRate,ref.ReferrerUserID FROM users as u
                LEFT JOIN user_meta as um ON um.UserID = u.UserID
                LEFT JOIN user_referrer as ref ON ref.UserID = u.UserID
                WHERE u.UserID = '$userID'
                ORDER BY FirstName ASC, um.LastName ASC";
        $data = $this->db->get_row($sql);

        return $data;
    }

    function getClientInvoices( $clientProfileID )
    {
        $data = array();
        $sql = "SELECT i.*, cp.`CompanyName`, fi.`Active`, ur.`SubscriptionRate`, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor
        FROM `invoices` i 
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
        LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
        LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        WHERE `ClientID` = $clientProfileID";
                
        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientInvoicesByStatus($clientProfileID, $status)
    {
        if( $status == 'Outstanding' ){
            // $sql = "SELECT i.*, c.`CompanyName`, fi.`Active`
            //     FROM `invoices` i 
            //     LEFT JOIN `client_profiles` c ON c.`ClientProfileID` = i.`ClientID`
            //     LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            //     WHERE `ClientID` = " . $clientProfileID . " AND (i.`Status` = 'Outstanding' OR i.`Status` = 'TT Receipt')";

            $sql = "SELECT i.*, cp.`CompanyName`, fi.`Active`, ur.`SubscriptionRate`, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor
            FROM `invoices` i 
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
            LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
            WHERE `ClientID` = $clientProfileID AND i.`Status` IN ( 'Outstanding', 'TT Receipt' ) AND i.`InvestmentBookingID` IS NOT NULL";

        } else {
            // $sql = "SELECT i.*, c.`CompanyName`, fi.`Active`
            //     FROM `invoices` i 
            //     LEFT JOIN `client_profiles` c ON c.`ClientProfileID` = i.`ClientID`
            //     LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            //     WHERE `ClientID` = " . $clientProfileID . " AND i.`Status` = '" . $status . "'";

            $sql = "SELECT i.*, cp.`CompanyName`, fi.`Active`, ur.`SubscriptionRate`, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor
            FROM `invoices` i 
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
            LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
            WHERE `ClientID` = $clientProfileID AND i.`Status` = '$status' AND i.`InvestmentBookingID` IS NOT NULL";
        }

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientInvoicesWithTTReceipt($clientProfileID)
    {
        $data = array();
        $sql = "SELECT i.* , c.CompanyName
                FROM invoices as i 
                LEFT JOIN client_profiles as c ON c.ClientProfileID = i.ClientID
                WHERE ClientID = " . $clientProfileID . " AND i.TTReceipt != '0'";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = [];
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientInvoicesWithNoTTReceipt( $clientProfileID )
    {
        $data = array();
        $sql = "SELECT i.*, cp.`CompanyName`, fi.`Active`, ur.`SubscriptionRate`, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor
            FROM `invoices` i 
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
            LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
            WHERE `ClientID` = $clientProfileID AND i.`TTReceipt` IS NULL";                

        $query = &$this->db->prepare($sql);
        $query->execute();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientInvoicesOverdue( $clientProfileID )
    {
        // $sql = "SELECT i.*, c.`CompanyName`, fi.`Active`
        //         FROM `invoices` i 
        //         LEFT JOIN `client_profiles` c ON c.`ClientProfileID` = i.`ClientID`
        //         LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        //         WHERE `ClientID` = ".$clientProfileID." AND i.`DueDate` < CURDATE() AND i.`Status` !='Paid'";

        $sql = "SELECT i.*, cp.`CompanyName`, fi.`Active`, ur.`SubscriptionRate`, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor
        FROM `invoices` i 
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
        LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
        LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        WHERE `ClientID` = $clientProfileID AND i.`DueDate` < CURDATE() AND i.`Status` !='Paid'";
                
        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientInvoiceDetails($InvoiceID)
    {
        $sql = "SELECT * from invoices WHERE InvoiceID = $InvoiceID";
        
        $data = $this->db->get_row($sql);

        return $data;
    }

    function getInfo( $userID, $IsNotClient = false )
    {
        $where = "";
        if( $IsNotClient ){
            $where = " WHERE cp.`ClientProfileID` = $IsNotClient ";
        } else {
            $where = " WHERE cp.`UserID` = $userID ";
        }

        $sql = "SELECT ba.*, cp.*, um.FirstName, um.LastName, '' AS Disabled
                FROM client_profiles as cp
                LEFT JOIN users u ON cp.UserID = u.UserID
                LEFT JOIN user_meta um ON um.UserID = u.UserID
                LEFT JOIN bank_accounts ba ON ba.UserID = u.UserID
                $where LIMIT 1";

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getClientInfo($ID)
    {

        $sql = "SELECT * FROM client_profiles WHERE ClientProfileID = $ID LIMIT 1";
        $userdata = $this->db->get_row($sql);
        return $userdata;
    }

    public function getUploadedFilesByDesc($fileDescription = NULL)
    {
        $sql = "SELECT fi.*, f.DateAdded FROM file_items fi LEFT JOIN files f ON fi.FileID = f.FileID WHERE fi.FileDescription = '".$fileDescription."'";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Get the latest activities from different transactions
     * @package
     * @access private
     * @param (int) $ClientProfileID : (required) client profile id
     * @param (string) $transaction  : (optional)[default: ''] type of transaction
     * @param (int) $nthDays         : (optional)[default: 3] number of days past up to the present
     * @param (int) $limit           : (optional)[default: 5] number of data to return
     * @return object
     **/
    function getLatestActivitiesByTransaction( $ClientProfileID, $transaction = '', $nthDays = 3, $limit = 5 )
    {
        switch( $transaction ){
            case 'BookingPending':
                $sql = "SELECT b.*, um.FirstName, um.LastName, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                FROM `bookings` b
                LEFT JOIN `user_meta` um ON um.UserID = b.UserID
                WHERE `ClientProfileID` = $ClientProfileID AND BookingStatus = 'Pending'
                AND timestampdiff(DAY, NOW(), `CreatedAt`) >= -$nthDays
                ORDER BY `CreatedAt` DESC
                LIMIT $limit"; break;

            case 'BookingVerified':
                $sql = "SELECT b.*, um.FirstName, um.LastName, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                FROM `bookings` b
                LEFT JOIN `user_meta` um ON um.UserID = b.UserID
                WHERE `ClientProfileID` = $ClientProfileID AND BookingStatus = 'Verified'
                AND timestampdiff(DAY, NOW(), `UpdatedAt`) >= -$nthDays
                ORDER BY `UpdatedAt` DESC
                LIMIT $limit"; break;

            case 'Replies':
                $sql = "SELECT fr.*, um.FirstName, um.LastName, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                FROM `forum_replies` fr
                LEFT JOIN `forum` f ON f.ForumID = fr.ForumID
                LEFT JOIN `forum_categories` fc ON fc.CatID = f.CatID
                LEFT JOIN `forum_sub_categories` fsc ON fsc.CatID = f.CatID
                LEFT JOIN `user_meta` um ON um.UserID = fr.UserID
                WHERE fc.ClientProfileID = $ClientProfileID AND timestampdiff(DAY, NOW(), fr.`DatePosted`) >= -$nthDays
                ORDER BY fr.DatePosted DESC
                LIMIT $limit"; break;

            case 'Invoices':
                $sql = "SELECT i.*, um.FirstName, um.LastName, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                FROM `invoices` i
                LEFT JOIN `user_meta` um ON um.UserID = i.InvestorUserID
                WHERE i.TTReceipt IS NULL AND i.ClientID = $ClientProfileID AND timestampdiff(DAY, NOW(), i.`InvoiceDate`) >= -$nthDays
                ORDER BY i.InvoiceDate DESC
                LIMIT $limit"; break;
            
            default:
                $sql = "SELECT * FROM (
                    SELECT * FROM (SELECT 'BookingPending' AS `Transaction`, b.InvestmentBookingID AS ID, b.TotalAmountAttached, 0 As BalanceDue, '' AS Title, '' AS SubCatName, '' AS CatID, '' AS SubCatID, b.CreatedAt AS TransDate, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                    FROM `bookings` b
                    LEFT JOIN `user_meta` um ON um.UserID = b.UserID
                    WHERE `ClientProfileID` = $ClientProfileID AND BookingStatus = 'Pending'
                    AND timestampdiff(DAY, NOW(), `CreatedAt`) >= -$nthDays
                    ORDER BY `CreatedAt` DESC
                    LIMIT $limit) pending
                    UNION
                    SELECT * FROM (SELECT 'BookingVerified' AS `Transaction`, b.InvestmentBookingID AS ID, b.TotalAmountAttached, 0 As BalanceDue, '' AS Title, '' AS SubCatName, '' AS CatID, '' AS SubCatID, b.CreatedAt AS TransDate, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                    FROM `bookings` b
                    LEFT JOIN `user_meta` um ON um.UserID = b.UserID
                    WHERE `ClientProfileID` = $ClientProfileID AND BookingStatus = 'Verified'
                    AND timestampdiff(DAY, NOW(), `UpdatedAt`) >= -$nthDays
                    ORDER BY `UpdatedAt` DESC
                    LIMIT $limit) verified
                    UNION
                    SELECT * FROM (SELECT 'Replies' AS `Transaction`, fr.ForumID AS ID, 0 AS TotalAmountAttached, 0 As BalanceDue, f.Title, fsc.SubCatName, f.CatID, f.SubCatID, fr.DatePosted AS TransDate, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                    FROM `forum_replies` fr
                    LEFT JOIN `forum` f ON f.ForumID = fr.ForumID
                    LEFT JOIN `forum_categories` fc ON fc.CatID = f.CatID
                    LEFT JOIN `forum_sub_categories` fsc ON fsc.CatID = f.CatID
                    LEFT JOIN `user_meta` um ON um.UserID = fr.UserID
                    WHERE fc.ClientProfileID = $ClientProfileID AND timestampdiff(DAY, NOW(), fr.`DatePosted`) >= -$nthDays
                    ORDER BY fr.DatePosted DESC
                    LIMIT $limit) replies
                    UNION
                    SELECT * FROM (SELECT 'Invoices' AS `Transaction`, i.InvoiceID AS ID, i.InvestmentAmount AS TotalAmountAttached, i.BalanceDue, '' AS Title, '' AS SubCatName, '' AS CatID, '' AS SubCatID, i.`InvoiceDate` AS TransDate, CONCAT(um.FirstName, ' ',um.LastName) AS FullName
                    FROM `invoices` i
                    LEFT JOIN `user_meta` um ON um.UserID = i.InvestorUserID
                    WHERE i.TTReceipt IS NULL AND i.ClientID = $ClientProfileID AND timestampdiff(DAY, NOW(), i.`InvoiceDate`) >= -$nthDays
                    ORDER BY i.InvoiceDate DESC
                    LIMIT $limit) invoices
                ) activities 
                ORDER BY TransDate DESC"; break;
        }

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    public static function showVal($key)
    {
        return ($key)? $key : '';
    }

    function getFaq($ID)
    {
        $sql = "SELECT * FROM faqs WHERE FaqID = '$ID' LIMIT 1";
        $data = $this->db->get_row($sql);
        return $data;
    }

    function getFaqs($clientProfileID, $status = NULL, $userID = NULL)
    {
        $where = "";
        if( $status != NULL ){
            $where = " AND f.FaqStatus = '$status'";
        }

        $sql = "SELECT *
            FROM faqs f             
            LEFT JOIN users u ON f.UserID = u.UserID 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE f.ClientProfileID = '$clientProfileID'".$where." 
            ORDER BY f.FaqID DESC";

        if( $userID != NULL ){
            $sql = "SELECT DISTINCT f.* FROM faqs f 
                    LEFT JOIN `bookings` b ON b.`ClientProfileID` = f.`ClientProfileID` AND b.`UserID` = '101228' AND b.`BookingStatus` = 'Approved'
                    WHERE f.`ClientProfileID` = '$clientProfileID' AND f.`PublicView` = '0' AND f.`FaqStatus` = 'Published'
                    UNION ALL SELECT DISTINCT  * FROM faqs WHERE `ClientProfileID` = '$clientProfileID' AND `FaqStatus` = 'Published' AND `PublicView` = '1'
                    ORDER BY FaqID DESC";
        }

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function doSaveFaq()
    {
        $acctID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){                           

                case "addnew":
                    $data = $this->post;
                    unset( $data['action'] );

                    $data['FaqDate'] = date( 'Y-m-d H:i:s' );
                    $ID = $this->db->insert( "faqs", $data );
                    if( $ID ){
                        $this->setSession( 'message', "New FAQ has been added!" );
                        App::activityLog( "Added FAQ #ID-".$ID.'.' );
                    }
                    break;

                case "update":
                    $data = $this->post;
                    $FaqID = $data['FaqID'];

                    unset( $data['action'] );
                    unset( $data['FaqID'] );
                    unset( $data['UserID'] );
                    unset( $data['ClientProfileID'] );

                    $where = array( 'FaqID' => $FaqID );
                    $this->db->update( "faqs", $data, $where );

                    $this->setSession( 'message', "FAQ changes has been saved!" );
                    App::activityLog( "Edited FAQ #ID-".$FaqID.'.' );
                    break;
            }
        }
    }

    function getBlogs( $clientProfileID, $status = false, $access = false, $catslug = false, $limit = false )
    {
        $sql = "SELECT b.*, bc.*, ul.Name as LevelName, ul.Code as LevelCode, um.FirstName, um.LastName 
            FROM blogs b             
            LEFT JOIN blog_categories bc ON bc.BlogCatID = b.BlogCatID 
            LEFT JOIN users u ON b.UserID = u.UserID 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE b.ParentBlogID = 0 AND b.ClientProfileID = '$clientProfileID'";

        if( $access ){
            $sql .= " AND b.PublicView = $access";
        }

        if( $status ){
            $sql .= " AND b.BlogStatus = '$status'";
        }

        if( $catslug ){
            $sql .= " AND bc.CategorySlug = '$catslug'";
        }

        $sql .= " ORDER BY BlogDatePublished DESC";
        if( $limit ){
            $sql .= " LIMIT 0,$limit";
        }

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );
        
        return $data;
    }

    function getblog( $ID )
    {
        $sql = "SELECT b.*, b.ClientProfileID as ClientPID, bc.CategoryName,bc.CategorySlug, ul.Name as LevelName, ul.Code as LevelCode, um.FirstName, um.LastName 
            FROM blogs b
                LEFT JOIN blog_categories bc ON bc.BlogCatID = bp.BlogCatID
                LEFT JOIN users u ON bp.UserID = u.UserID
                LEFT JOIN user_meta um ON um.UserID = u.UserID 
                LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            WHERE b.BlogPostID = ".$ID;

        $data = $this->db->get_row( $sql );

        return $data;
    }

    function getBlogInfo( $clientProfileID )
    {
        $sql = "SELECT * FROM blogs WHERE ParentBlogID = 0 AND ClientProfileID = $clientProfileID";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data=  $row;
        }
        unset( $query );

        return $data;
    }

    function getBlogPostInfo( $blogID )
    {
        $sql = "SELECT b.*, bc.CategoryName, bc.CategorySlug FROM blogs as b
                LEFT JOIN blog_categories bc ON b.BlogCatID = bc.BlogCatID  
                WHERE BlogID = ".$blogID." LIMIT 1";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data=  $row;
        }
        unset( $query );

        return $data;
    }

    function getBlogPostBySlug( $slug )
    {
        $sql = "SELECT b.*, bc.CategoryName, bc.CategorySlug FROM blogs as b
                LEFT JOIN blog_categories bc ON b.BlogCatID = bc.BlogCatID  
                WHERE BlogSlug = '$slug' LIMIT 1";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data=  $row;
        }
        unset( $query );

        return $data;
    }

    function getComments( $clientProfileID, $userID = false )
    {
        //$sql = "SELECT * FROM `comments` WHERE `ClientProfileID` = $clientProfileID ORDER BY `CommentDate`";

        // $sql = "SELECT DISTINCT c.*,um.`FirstName`,um.`LastName`, fi.`FileSlug`, u.`Level` 
        // FROM `comments` c
        // LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
        // LEFT JOIN `users` u ON u.`UserID` = c.`UserID`
        // LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
        // WHERE `ParentID` = 0 AND c.`ClientProfileID` = $clientProfileID
        // ORDER BY c.`CommentID`";

        $sql = "SELECT DISTINCT c.*,um.`FirstName`,um.`LastName`, fi.`FileSlug`, u.`Level`, cp.`UserID` AS ClientUserID
        FROM `comments` c
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = c.`ClientProfileID`
        LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
        LEFT JOIN `users` u ON u.`UserID` = c.`UserID`
        LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
        WHERE c.`ClientProfileID` = $clientProfileID
        ORDER BY c.`CommentID`";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getComment( $field, $value )
    {
        $sql = "SELECT c.*, cp.`UserID` AS ClientUserID
        FROM `comments` c
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = c.`ClientProfileID`
        WHERE c.`$field` = '$value'
        LIMIT 1";

        $data = $this->db->get_row( $sql );

        return $data;
    }

    function getCommunities( $clientProfileID )
    {
        //$sql = "SELECT * FROM `comments` WHERE `ClientProfileID` = $clientProfileID ORDER BY `CommentDate`";
        $sql = "SELECT DISTINCT b.`UserID`, um.`FirstName`, um.`LastName`, um.`JobTitle`, um.`Avatar`, fi.`FileSlug`
        FROM `bookings` b
        LEFT JOIN `user_meta` um ON um.`UserID` = b.`UserID`
        LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
        WHERE b.`ClientProfileID` = $clientProfileID
        ORDER BY b.`InvestmentBookingID`";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getCategories( $clientProfileID = 0 )
    {
        // $sql = "SELECT * FROM blog_categories WHERE ClientProfileID = ".$clientProfileID;

        // $sql = "SELECT * FROM `blog_categories` WHERE `ClientProfileID` = $clientProfileID ORDER BY `CategoryName`";

        $sql = "SELECT DISTINCT bc.*
        FROM `blogs` b
        LEFT JOIN `blog_categories` bc ON bc.`BlogCatID` = b.`BlogCatID`
        WHERE b.`ClientProfileID` = $clientProfileID AND bc.`BlogCatID` IS NOT NULL
        ORDER BY bc.`CategoryName`";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);
        
        return $data;
    }

    function getCategory( $ID )
    {
        $sql = "SELECT * FROM blog_categories bc WHERE bc.BlogCatID = ".$ID;

        $data = $this->db->get_row($sql);

        return $data;
    }

    /**
     * Get assistant list
     * 
     * @package
     * @access private
     * @param (string) $ID : (required) : client user id
     * @return object
     **/
    function getAssistants( $ID )
    {
        $sql = "SELECT u.*, um.*, ul.*
            FROM `users` u
            LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
            LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level`
            WHERE u.`Active` = 1 AND ul.`Code` = 'ASST' AND u.`ReferrerUserID` = $ID 
            ORDER BY um.`FirstName`, um.`LastName`";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get trashed assistant
     * 
     * @package
     * @access private
     * @param (string) $ID : (required) : client user id
     * @return object
     **/
    function getTrashedAssistants( $ID )
    {
        $sql = "SELECT u.*, um.*, ul.*
            FROM `users` u
            LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
            LEFT JOIN `user_levels` ul ON ul.`UserLevelID` = u.`Level`
            WHERE u.`Active` = 0 AND ul.`Code` = 'ASST' AND u.`ReferrerUserID` = $ID
            ORDER BY um.`FirstName`, um.`LastName`";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Insert / Update / Delete assistant
     * 
     * @package
     * @access private
     * @param (string) $ID : (required) client profile id
     * @return int
     **/
    function doSaveAssistant( $ID )
    {
        $userID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "add": {
                    $data = $this->post;
                    $user = $this->post['user'];
                    $meta = $this->post['meta'];

                    unset( $data['action'] );

                    $pw = $this->generatePw( 10 );
                    $user['Password'] = $this->encrypt( $pw );
                    $user['ReferrerUserID'] = $ID;
                    $user['Level'] = 6;
                    $user['Active'] = 1;
                    // $user['HashKey'] = md5( microtime() );
                    // $user['HashKey'] = base64_encode( $pw );
                    $user['HashKey'] = self::enchash( $pw );
                    $user['DateAdded'] = date( 'Y-m-d H:i:s' );
                    $user['Capability'] = Level::info( 'Capability', isset( $user['Level'] ) ? $user['Level'] : Option::get('new_user_role') );

                    if( User::infoByEmail( 'UserID', $user['Email'] ) ){
                        $this->setSession( 'error', "Email already exists" );
                        $this->setSession( 'message', "" );
                        View::redirect( 'clients/assistant/asst_add' );
                    }

                    $userID = $this->db->insert( "users", $user );

                    if( $userID ){
                        $where = array( 'UserID' => $userID );

                        // TO DO: send email
                        //$filedata = $this->sendEmail($userID);
                        $meta['UserID'] = $userID;
                        $metaID = $this->db->insert( "user_meta", $meta );

                        if( $this->file['Avatar']['name'] != '' ){
                            $filedata = $this->fileUpload( $this->file, $userID );
                            if( count( $filedata ) ){
                                $rowCount = $this->db->update("user_meta", $filedata, $where );
                            }
                        }
                        $this->setSession( 'message', "New assistant has been added!" );
                        App::activityLog( "Added assistant #ID-".$userID.'.' );
                    }
                } break;

                case "edit": {
                    $data = $this->post;
                    $uwhere = array('UserID' => $this->post['userid']);
                    $mwhere = array('UserMetaID' => $this->post['metaid']);

                    if( isset( $this->post['user'] ) ){
                        $user = $this->post['user'];
                    }
                    if( isset( $this->post['meta'] ) ){
                        $meta = $this->post['meta'];
                    }

                    unset( $data['action'] );

                    if( isset( $this->post['Password'] ) && $this->post['Password'] != '' ){
                        $pass = $this->encrypt( $this->post['Password'] );
                        $user['Password'] = $pass;
                    }

                    if( isset( $this->post['capabilities'] ) && count( $this->post['capabilities'] ) ){
                        $user['Capability'] = $this->arrayToString( $this->post['capabilities'] );
                    }

                    if( isset( $this->post['user'] ) ){
                        $userID = $this->db->update( "users", $user, $uwhere );
                    }
                    if( isset( $this->post['meta'] ) ){
                        $metaID = $this->db->update( "user_meta", $meta, $mwhere );
                    }

                    if ( isset( $this->post['metaCorp'] ) ){
                        $metaCorp = $this->post['metaCorp'];
                        $mCorpwhere = array( 'UserMetaCorpID' => $this->post['metacorpid'] );
                        $this->db->update( "user_meta_corporate", $metaCorp, $mCorpwhere );
                    }

                    if( isset( $this->file['Avatar'] ) && $this->file['Avatar']['name'] != '' ){
                        $filedata = $this->fileUpload( $this->file,$this->post['userid'] );
                        if( count( $filedata ) ){
                            $this->removeUploadedFiles( $this->post['avatarid'] );
                            $rowCount = $this->db->update( "user_meta", $filedata, $mwhere );
                        }
                    }

                    $this->updateUserInfo();
                    Lang::init();
                    
                    if( $userID ){
                        $this->setSession( 'message', 'Assistant has been updated!' );
                        App::activityLog( 'Updated assistant #ID-'.$this->post['userid'].'.' );
                    }
                } break;   
            } 
        }
        
        return $userID;
    }

    /**
     * Update Session Info
     * @access Private 
     * @return NA
     **/
    function updateUserInfo()
    {
        $userdata = User::info( false, User::info('UserID') );
        $udata = (array) $userdata;
        $this->setSession( 'userdata', $udata );
        $this->setSession( 'language', $udata['Language'] );
    }
    
    function doTrashAssistant( $UserID )
    {
        $where = array( 'UserID' => $UserID );
        $data = array( 'Active' => 0 );
        $rowCount = $this->db->update( "users", $data, $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Trashed assistant manager #ID-'.$UserID.'.' );
            App::activityLog( 'Trashed assistant manager #ID-'.$UserID.'.' );
        }
    }
    
    function doRestoreAssistant( $UserID )
    {
        $where = array( 'UserID' => $UserID );
        $data = array( 'Active' => 1 );
        $rowCount = $this->db->update( "users", $data, $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Restored assistant manager #ID-'.$UserID.' from trash bin.' );
            App::activityLog( 'Restored assistant manager #ID-'.$UserID.' from trash bin.' );
        }
    }
    
    function doDeleteAssistant( $UserID )
    {
        $where = array( 'UserID' => $UserID );
        $rowCount = $this->db->delete( "users", $where );
        $rowCount = $this->db->delete( "user_meta", $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Permanently deleted assistant manager #ID-'.$UserID.'.' );
            App::activityLog( 'Permanently deleted assistant manager #ID-'.$UserID.' from trash bin.' );
        }
    }    
    
    function doEmptyTrashAssistant( $ids )
    {
        foreach( $ids as $id ){
            $where = array( 'UserID' => $id );
            $rowCount = $this->db->delete( "users", $where );
            $rowCount = $this->db->delete( "user_meta", $where );
            if( $rowCount ){
                App::activityLog( 'Permanently deleted assistant manager #ID-'.$id.' from trash bin.' );
            }
        }
        $this->setSession( 'message', 'Emptied trashed assistant managers' );
    }

    /**
     * Get team members list
     * 
     * @package
     * @access private
     * @param (string) $ID : (required) : client user id
     * @return object
     **/
    function getTeams( $ID, $public = false )
    {
        $sql = "SELECT ct.* FROM `clients_team` ct WHERE ct.`Active` = 1 AND ct.`clientUserID` = $ID ORDER By `Name`";
        if( $public ){
            $sql = "SELECT ct.*, fi.`FileSlug` FROM `clients_team` ct LEFT JOIN `file_items` fi ON fi.`FileID` = ct.`Photo` WHERE ct.`Active` = 1 AND ct.`clientUserID` = $ID ORDER By `Name`";
        }

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get team members list
     * 
     * @package
     * @access private
     * @param (string) $ID       : (required) : client user id
     * @param (string) $memberID : (required) : team member user id
     * @return object
     **/
    function getTeamsByMemberID( $ID, $memberID, $public = false )
    {
        $where = " AND ct.`ClientTeamID` = $memberID ";
        $sql = "SELECT ct.* FROM `clients_team` ct WHERE ct.`Active` = 1 AND ct.`ClientUserID` = $ID $where ORDER By `Name`";
        if( $public ){
            $sql = "SELECT ct.*, fi.`FileSlug` FROM `clients_team` ct LEFT JOIN `file_items` fi ON fi.`FileID` = ct.`Photo` WHERE ct.`Active` = 1 AND ct.`ClientUserID` = $ID $where ORDER By `Name`";
        }

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get trashed team members
     * 
     * @package
     * @access private
     * @param (string) $ID : (required) : client team id
     * @return object
     **/
    function getTrashedTeams( $ID )
    {
        $sql = "SELECT ct.*, fi.`FileSlug` FROM `clients_team` ct LEFT JOIN `file_items` fi ON fi.`FileID` = ct.`Photo` WHERE ct.`Active` = 0 AND ct.`clientUserID` = $ID ORDER By `Name`";

        $query = $this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Insert / Update / Delete team members
     * 
     * @package
     * @access private
     * @param (string) $ID     : (required) client teamid
     * @param (string) $userID : (required) current userid
     * @return int
     **/
    function doSaveTeam( $ID, $userID )
    {
        $teamID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "add":
                case "updateclientprofile": {
                    $data = $this->post;
                    $team = $this->post['team'];
                    $team['ClientUserID'] = $ID;

                    unset( $data['action'] );

                    $teamID = $this->db->insert( "clients_team", $team );

                    if( $teamID ){
                        $where = array( 'ClientTeamID' => $teamID );

                        // TO DO: send email
                        //$filedata = $this->sendEmail($teamID);
                        if( isset( $this->file['Photo']['name'] ) && $this->file['Photo']['name'] != '' ){
                            $filedata = $this->fileUpload( $this->file, $userID );
                            if( count( $filedata ) ){
                                $rowCount = $this->db->update("clients_team", $filedata, $where );
                            }
                        }
                        $this->setSession( 'message', "New team member has been added!" );
                        App::activityLog( "Added client team member #ID-".$teamID.'.' );
                    }                    
                } break;

                case "edit": {
                    $data = $this->post;
                    $where = array( 'ClientTeamID' => $ID );

                    unset( $data['action'] );

                    if( isset( $this->post['team'] ) ){
                        $team = $this->post['team'];
                        $teamID = $this->db->update( "clients_team", $team, $where );
                        if( $teamID ){
                            $this->setSession( 'message', 'Team member has been updated!' );
                            App::activityLog( 'Updated team member #ID-'.$ID.'.' );
                        }
                    }

                    if( isset( $this->file['Photo'] ) && $this->file['Photo']['name'] != '' ){
                        $filedata = $this->fileUpload( $this->file, $userID );
                        if( count( $filedata ) ){
                            $this->removeUploadedFiles( $this->post['photoid'] );
                            $rowCount = $this->db->update( "clients_team", $filedata, $where );
                        }
                    }
                } break;   
            } 
        }
        
        return $teamID;
    }
    
    function doTrashTeam( $ID )
    {
        $where = array( 'ClientTeamID' => $ID );
        $data = array( 'Active' => 0 );
        $rowCount = $this->db->update( "clients_team", $data, $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Trashed team member #ID-'.$ID.'.' );
            App::activityLog( 'Trashed team member #ID-'.$ID.'.' );
        }
    }
    
    function doRestoreTeam( $ID )
    {
        $where = array( 'ClientTeamID' => $ID );
        $data = array( 'Active' => 1 );
        $rowCount = $this->db->update( "clients_team", $data, $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Restored team member #ID-'.$ID.' from trash bin.' );
            App::activityLog( 'Restored team member #ID-'.$ID.' from trash bin.' );
        }
    }
    
    function doDeleteTeam( $ID )
    {
        $where = array( 'ClientTeamID' => $ID );
        $rowCount = $this->db->delete( "clients_team", $where );
        if( $rowCount ){
            $this->setSession( 'message', 'Permanently deleted team member #ID-'.$ID.' from trash bin.' );
            App::activityLog( 'Permanently deleted team member #ID-'.$ID.' from trash bin.' );
        }
    }    
    
    function doEmptyTrashTeam( $ids )
    {
        foreach( $ids as $id ){
            $where = array( 'ClientTeamID' => $id );
            $rowCount = $this->db->delete( "clients_team", $where );            
            if( $rowCount ){
                App::activityLog( 'Permanently deleted team member #ID-'.$ID.' from trash bin.' );
            }
        }
        $this->setSession( 'message', 'Permanently deleted team members from trash bin.' );
    }
}