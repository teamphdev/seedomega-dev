<?php
class Wallet_model extends Model 
{
    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }
       
    function getInquiryByID($ID)
    {
        $sql = "SELECT cu.*, ul.Code as ProccessBy_RoleCode, um.FirstName as ProccessBy_FirstName, um.LastName as ProccessBy_LastName  
        FROM contact_us cu 
        LEFT JOIN users u ON cu.ProccessBy = u.UserID
        LEFT JOIN user_meta um ON cu.ProccessBy = um.UserID 
        LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE InquiryID = ".$ID." LIMIT 1";

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getUsersByRoles($roleID = 4)
    {
        $sql = "SELECT u.UserID,u.Email,um.FirstName,um.LastName FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID WHERE u.Level IN(".$roleID.") AND u.Active = 1";
        $sql .= " ORDER BY u.UserID";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    } 

    function verifyIfOwn($ID, $status = 'Pending')
    {        
        $loggedUserID = User::info('UserID');

        $sql = "SELECT w.* 
                FROM wallet w 
                WHERE w.`WalletID` = ".$ID." AND w.`UserID` = ".$loggedUserID." AND w.`TransactionStatus` IN (".$status.") LIMIT 1";

        $data = $this->db->get_row($sql);

        if($data){
            return true;
        }else{
            return false;
        }
    }

    function getCashInPendingByID($walletID)
    {
        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, wm.`MethodName`, w.* 
                FROM wallet w 
                LEFT JOIN bank_accounts b ON w.`BankAccountID` = b.`BankAccountID` 
                LEFT JOIN wallet_method wm ON wm.`WalletMethodID` = w.`Method` 
                WHERE w.`WalletID` = ".$walletID." AND w.`TransactionStatus` IN ('Pending') LIMIT 1 ";    
        
        $data = $this->db->get_row($sql);
        
        return $data;
    }
    
    function getBankAccounts($UserID = false)
    {
        $where = '';
        if($UserID){
            $where = 'WHERE b.UserID = '.$UserID;
        }else{
            $where = 'WHERE b.UserID = '.User::info('UserID');
        }

        $sql = "SELECT b.*
        FROM bank_accounts b ".$where;    
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);
        
        return $data;
    }

    function getAccountStatements($UserID = false, $date=false, $type=false)
    {
        $userID = ($UserID) ? $UserID : User::info('UserID');

        $searchWhere = "";

        if($date) {
            $dates = explode(' - ',$date);
            $from = date( 'Y-m-d', strtotime($dates[0]) );
            $to = date( 'Y-m-d', strtotime($dates[1]) );

            $searchWhere .= " AND w.`ApproveDate` BETWEEN '$from' AND '$to'";
        }

        if($type) {
            $searchWhere .= " AND w.`TransactionType` = '".$type."'";
        }

        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, w.* 
                FROM wallet w 
                LEFT JOIN bank_accounts b ON w.BankAccountID = b.BankAccountID 
                WHERE w.UserID = ".$userID." AND w.`TransactionStatus` = 'Approved' ".$searchWhere." ORDER BY w.`ApproveDate` DESC";    
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getPendingTransactions($UserID = false)
    {
        $userID = ($UserID) ? $UserID : User::info('UserID');

        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, w.*
                FROM wallet w 
                LEFT JOIN bank_accounts b ON w.BankAccountID = b.BankAccountID 
                WHERE w.UserID = ".$userID." AND w.`TransactionStatus` IN ('Pending', 'Verified', 'Rejected') ORDER BY w.`TransactionDate` DESC";
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getTransactionLog($UserID = false, $limit = false)
    {
        $userID = ($UserID) ? $UserID : User::info('UserID');
        $limit = ($limit) ? 'LIMIT '.$limit : '';

        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, wm.`MethodName`, w.* 
                FROM wallet w 
                LEFT JOIN bank_accounts b ON w.`BankAccountID` = b.`BankAccountID` 
                LEFT JOIN wallet_method wm ON wm.`WalletMethodID` = w.`Method` 
                WHERE w.`UserID` = ".$userID." AND w.`TransactionStatus` IN ('Pending','Approved','Verified','Rejected') ORDER BY w.`TransactionDate` DESC ".$limit; 
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function countPendingTransactions($UserID = false)
    {
        $userID = ($UserID) ? $UserID : User::info('UserID');

        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, w.*
                FROM wallet w 
                LEFT JOIN bank_accounts b ON w.`BankAccountID` = b.`BankAccountID`
                WHERE w.UserID = ".$userID." AND w.`TransactionStatus` IN ('Pending', 'Verified') ORDER BY w.`TransactionDate` DESC";    
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getTransactionsByStatus($method = false, $status = false)
    {
        $where = "";
        if($status){
            $where .= " AND w.`TransactionStatus` IN (".$status.")";
        }

        if($method){
            $where .= " AND w.`Method` IN (".$method.")";
        }

        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, w.*, wm.`MethodName`, (SELECT `FileSlug` FROM `file_items` WHERE `fileID` = w.`TTreceipt` LIMIT 1) as TTReceiptFileSlug, um.FirstName, um.LastName, CONCAT(um.FirstName, ' ', um.LastName) as FullName
                FROM wallet w 
                LEFT JOIN user_meta um ON w.`UserID` = um.`UserID` 
                LEFT JOIN wallet_method wm ON wm.`WalletMethodID` = w.`Method` 
                LEFT JOIN bank_accounts b ON w.`BankAccountID` = b.`BankAccountID`
                WHERE 1 ".$where;    
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getTransactionByID($walletID, $status = false)
    {
        $where = "";
        if($status){
            $where .=" AND w.`TransactionStatus` IN (".$status.")";
        }

        $sql = "SELECT b.`Name`, b.`Address`, b.`AccountNumber`, b.`AccountName`, b.`SwiftCode`, b.`DateAdded`, wm.`MethodName`, w.*, um.FirstName, um.LastName, CONCAT(um.FirstName, ' ', um.LastName) as FullName 
                FROM wallet w 
                LEFT JOIN user_meta um ON w.`UserID` = um.`UserID` 
                LEFT JOIN bank_accounts b ON w.`BankAccountID` = b.`BankAccountID` 
                LEFT JOIN wallet_method wm ON wm.`WalletMethodID` = w.`Method` 
                WHERE w.`WalletID` = ".$walletID.$where." LIMIT 1 ";    
        
        $data = $this->db->get_row($sql);
        
        return $data;
    }
    
    function doSave()
    {
        $walletID = false;
        if(isset($this->post['action'])) {
            switch($this->post['action']) {
                case "uploadtt": {

                    $data = $this->post;
                    $wallet = [];
                    $whereID = isset($data['walletid']) ? $data['walletid'] : '';

                    unset($data['action']);

                    $wallet['TransactionStatus'] = "Verified";

                    $walletdata = $wallet;
                    if(isset($data['ttupload']) && $this->file['TTreceipt']['name'] != ''){
                        
                        $filedata = $this->fileUpload($this->file, $data['usrid']);
                        if(count($filedata)) {
                            $walletdata = array_merge($wallet,$filedata);
                        }                        

                        $walletID = $this->db->update("wallet", $walletdata, array('WalletID' => $whereID));

                        if($walletID) {
                            App::activityLog("Upload TT : ".$data['description']." #ID.".$whereID.".");
                            $this->setSession('message',"You have successfully submited your TT receipt and awaiting for verification!");
                        }
                    }else{
                        $this->setSession('error',"Please upload TT Receipt!");
                    }

                } break;
                case "cashin": {
                    $data = $this->post;
                    $wallet = $this->post['wallet'];

                    unset($data['action']);

                    $wallet['TransactionStatus'] = "Pending";
                    $wallet['UserID'] = isset($wallet['UserID']) ? $wallet['UserID'] : User::info('UserID');
                    $wallet['TransactionDate'] = date('Y-m-d H:i:s');
                    $wallet['Method'] = 1;
                    $wallet['Description'] = 'Cash In: via Bank Deposit.';
                    $wallet['TransactionType'] = 'Credit';
                    $wallet['CoreBankAccountID'] = Option::get('primary_bank_account');
                    $wallet['TransactionAmount'] = str_replace(',', '', $wallet['TransactionAmount']);

                    $walletdata = $wallet; 

                    if(isset($data['ttupload']) && $this->file['TTreceipt']['name'] != ''){
                        $filedata = $this->fileUpload($this->file, $wallet['UserID']);
                        if(count($filedata)) {
                            $walletdata = array_merge($wallet,$filedata);
                        }
                        $walletdata['TransactionStatus'] = "Verified";
                    }

                    if($wallet['TransactionAmount'] <= 0){
                        $this->setSession('error', 'Please enter amount');
                    }else{
                        $walletID = $this->db->insert("wallet", $walletdata);
                    }                    

                    if($walletID) {
                        App::activityLog("Wallet: ".$wallet['Description']." #ID.".$walletID.".");
                        $this->setSession('message',"You have successfully submited your Cash In!");
                    }
                } break;
                case "cashout": {
                    $data = $this->post;
                    $wallet = $this->post['wallet'];
                    $bank = isset($this->post['bank']) ? $this->post['bank'] : '';

                    unset($data['action']);                    

                    $wallet['TransactionStatus'] = "Pending";
                    $wallet['UserID'] = isset($wallet['UserID']) ? $wallet['UserID'] : User::info('UserID');
                    $wallet['TransactionDate'] = date('Y-m-d H:i:s');

                    switch ($wallet['Method']) {
                        case '2':
                            $description = 'Cash Out : via Cash Pickup';
                            break;
                        case '4':
                            $description = 'Cash Out : via Bank Transfer';
                            break;
                        
                        default:
                            $description = 'undefined';
                            break;
                    }
                    $wallet['Description'] = $description;
                    $wallet['TransactionType'] = 'Debit';
                    $wallet['TransactionAmount'] = str_replace(',', '', $wallet['TransactionAmount']);

                    $mybalance = AppUtility::getwalletbalance();
                    if($wallet['TransactionAmount'] > $mybalance){
                        $this->setSession('error','You exceed the running balance '.number_format($mybalance,2));
                    }elseif($mybalance <= 0){
                        $this->setSession('error', 'Insufficient Balance');
                    }elseif($wallet['TransactionAmount'] <= 0){
                        $this->setSession('error', 'Please enter amount');
                    }else{
                        if(!isset($wallet['BankAccountID'])) {
                            // Bank Information
                            $bank['UserID'] = $wallet['UserID'];
                            $bankID = $this->db->insert("bank_accounts", $bank);

                            $wallet['BankAccountID'] = $bankID;
                        }

                        $walletID = $this->db->insert("wallet", $wallet);
                    }                    

                    if($walletID) {
                        App::activityLog("Wallet: ".$wallet['Description']." #ID.".$walletID.".");
                        $this->setSession('message',"You have successfully submited your Cash In!");
                    }
                } break;
                case "transfer": {
                    $data = $this->post;
                    $wallet = $this->post['wallet'];

                    unset($data['action']);

                    $ToUserID   = ($wallet['ToUserID']) ? $wallet['ToUserID'] : true;
                    $receiver   = User::info(false, $ToUserID);
                    $sender     = User::info();

                    $wallet['TransactionStatus'] = "Approved";
                    $wallet['UserID'] = $sender->UserID;
                    $wallet['FromUserID'] = $sender->UserID;
                    $wallet['TransactionDate'] = date('Y-m-d H:i:s');
                    $wallet['ApproveDate'] = date('Y-m-d H:i:s');

                    $wallet['Method'] = 3;
                    $wallet['Description'] = 'Sent Money to : '.$receiver->FirstName.' '.$receiver->LastName.'.';
                    $wallet['TransactionType'] = 'Debit';
                    $wallet['TransactionAmount'] = str_replace(',', '', $wallet['TransactionAmount']);

                    $mybalance = AppUtility::getwalletbalance();

                    
                    if($wallet['TransactionAmount'] > $mybalance){
                        // check the amount to not exceed the current balance.
                        $this->setSession('error','You exceed the running balance '.number_format($mybalance,2));

                    }elseif($mybalance <= 0){
                        // Check if have enough balance to transfer.
                        $this->setSession('error', 'Insufficient Balance');

                    }elseif(!$receiver){
                        // Check if Recipient UserID is exists
                        $this->setSession('error', 'Recipient Not found, please check User ID or Email Address');

                    }else{
                        //$walletID = $this->db->insert("wallet", $wallet);
                    }                    

                    if($walletID) {
                        $receiverData = array();

                        $receiverData['TransactionStatus'] = "Approved";
                        $receiverData['UserID'] = $receiver->UserID;
                        $receiverData['ToUserID'] = $receiver->UserID;
                        $receiverData['FromUserID'] = $sender->UserID;
                        $receiverData['TransactionDate'] = date('Y-m-d H:i:s');
                        $receiverData['ApproveDate'] = date('Y-m-d H:i:s');
                        $receiverData['Method'] = 3;
                        $receiverData['Description'] = 'Received Money from : '.$sender->FirstName.' '.$sender->LastName.'.';
                        $receiverData['TransactionType'] = 'Credit';
                        $receiverData['TransactionAmount'] = str_replace(',', '', $wallet['TransactionAmount']);

                        //$receiverWalletID = $this->db->insert("wallet", $receiverData);

                        App::activityLog("Wallet Transfer to : ".$receiver->FirstName.' '.$receiver->LastName." Amounting ($".number_format($wallet['TransactionAmount'],2).").");
                        $this->setSession('message',"You have successfully transfered $".number_format($wallet['TransactionAmount'],2)." to <b>".$receiver->FirstName.' '.$receiver->LastName."<b>!");
                    }
                } break;
            } 
        }
        return $walletID;
    }

    function manageDoSave()
    {
        $walletID = false;
        if(isset($this->post['action'])) {
            switch($this->post['action']) {
                case "updatedata": {

                    $data = $this->post;
                    $wallet = $this->post['wallet'];
                    $whereID = isset($data['walletid']) ? $data['walletid'] : '';

                    $walletData = $this->getTransactionByID($whereID);
                    $mybalance = AppUtility::getwalletbalance($walletData->UserID);

                    unset($data['action']);

                    switch ($data['submit-btn']) {
                        case 'Approved':
                            if( User::can( 'Approve Cash In' || 'Approve Cash Out') ){
                                $wallet['ApproveDate'] = date('Y-m-d H:i:s');
                                $wallet['ApproveUserID'] = User::info('UserID');
                                $wallet['TransactionStatus'] = "Approved";

                                $filewhere = array('FileID' => $data['ttfileid']);
                                $updatefile = $this->db->update("file_items",array('Active' => 1), $filewhere);
                            }
                            break;
                        case 'Update':
                            break;
                        case 'Pending':
                            $wallet['TransactionStatus'] = "Pending";
                            break;
                        case 'Verified':
                            $wallet['TransactionStatus'] = "Verified";
                            break;
                        case 'Reject':
                            $wallet['TransactionStatus'] = "Rejected";
                            break;
                    }

                    $walletdata = $wallet;

                    if(isset($this->file['TTreceipt']) && $this->file['TTreceipt']['name'] != ''){
                        $filedata = $this->fileUpload($this->file, $wallet['UserID']);
                        if(count($filedata)) {
                            $walletdata = array_merge($wallet,$filedata);
                        }
                    }

                    switch (true) {

                        case ($walletData->Method == 2 AND $data['submit-btn'] == 'Approved'):
                            // CASH OUT : CASHPICK UP APPROVED
                            if( floatval( $walletData->TransactionAmount ) > floatval( $mybalance ) )
                            {
                                $this->setSession('error',"Insufficient Balance To Approved ");
                            }else{
                                $walletID = $this->db->update("wallet", $walletdata, array('WalletID' => $whereID));
                            }
                            break;

                        case ($walletData->Method == 4 AND $data['submit-btn'] == 'Approved'):
                            // CASH OUT : BANK TRANSFER APPROVED
                            if( floatval( $walletData->TransactionAmount ) > floatval( $mybalance ) )
                            {
                                $this->setSession('error',"Insufficient Balance To Approved ");
                            }else{
                                $walletID = $this->db->update("wallet", $walletdata, array('WalletID' => $whereID));
                            }
                            break;
                        
                        default:
                            $walletID = $this->db->update("wallet", $walletdata, array('WalletID' => $whereID));
                            break;
                    }                    

                    if($walletID) {
                        App::activityLog("Updated Wallet : ".$data['description']." #ID.".$whereID.".");
                        $this->setSession('message',"You have successfully Updated #ID.".$whereID.".");
                    }

                } break;
            } 
        }
        return $walletID;
    }
        
    // function doDelete($ID)
    // {
    //     $where = array('InquiryID' => $ID);
    //     $this->setSession('message',"successfully Deleted"); 
    //     App::activityLog("Contact Us: deleted inquiry <a href='#'>#".$ID."</a>.");       
    //     $rowCount = $this->db->delete("contact_us", $where);
    // }

    function doRejectFile($ID, $walletID)
    {
        $where = array('FileItemID' => $ID);
        $rowCount = $this->db->update("file_items",array('Active' => 2), $where);

        $rowCount = $this->db->update("wallet",array('TransactionStatus' => 'Pending'), array('WalletID' => $walletID));
        
        App::activityLog("Rejected wallet file ID.".$walletID."</a>.");
    }
    
    function doApproveFile($ID, $walletID)
    {
        $where = array('FileItemID' => $ID);
        $rowCount = $this->db->update("file_items",array('Active' => 1), $where);
        App::activityLog("Approved wallet file ID.".$walletID."</a>.");
    }
    
    function doDeleteFile($ID, $walletID)
    {
        // delete first the associated data in `file_groups` table, be sure that the passed $ID is referring to `FileItemID` of `file_items` table
        AppUtility::deleteFileGroupsData( $ID );

        $where = array('FileItemID' => $ID);
        $rowCount = $this->db->delete("file_items", $where);

        $rowCount = $this->db->update("wallet",array('TransactionStatus' => 'Pending', 'TTreceipt' => '0'), array('WalletID' => $walletID));
        
        App::activityLog("Deleted wallet file ID.".$walletID."</a>.");
    }

    public function indexAssets()
    {
        // jQuery (necessary for JavaScript plugins)
        View::$footerscripts[] = 'vendor/jquery/dist/jquery.min.js';
        View::$footerscripts[] = "assets/js/migrate.js";

        // Bootstrap Javascript 
        View::$footerscripts[] = "assets/js/bootstrap.min.js";

        // EXTENSION PLUGINS
        View::$footerscripts[] = "assets/js/pieprogress/scripts/rainbow.min.js";
        View::$footerscripts[] = "assets/js/pieprogress/scripts/jquery-asPieProgress.js";
        View::$footerscripts[] = "assets/js/slider-revolution/rs-plugin/js/jquery.themepunch.plugins.min.js";
        View::$footerscripts[] = "assets/js/slider-revolution/rs-plugin/js/jquery.themepunch.revolution.min.js";
        View::$footerscripts[] = "assets/js/bxslider/jquery.bxslider.min.js";
        View::$footerscripts[] = "assets/js/maps.js";
        View::$footerscripts[] = "assets/js/maps/scripts/jquery.gmap.min.js";
        View::$footerscripts[] = "assets/js/jquery.scroll.js";
        View::$footerscripts[] = "assets/js/jquery.hoverizr.min.js";
        View::$footerscripts[] = "assets/js/jquery.placeholder.min.js";

        View::$footerscripts[] = "vendor/datatables.net/js/jquery.dataTables.min.js";
        View::$footerscripts[] = "vendor/datatables.net-bs/js/dataTables.bootstrap.min.js";
        View::$footerscripts[] = "vendor/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js";
        View::$footerscripts[] = "vendor/datatables.net-keytable/js/dataTables.keyTable.min.js";
        View::$footerscripts[] = "vendor/datatables.net-responsive/js/dataTables.responsive.min.js";
        View::$footerscripts[] = "vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js";
        View::$footerscripts[] = "vendor/datatables.net-scroller/js/datatables.scroller.min.js";        
        View::$footerscripts[] = "vendor/bootstrap-wizard/jquery.bootstrap.wizard.min.js";
        View::$footerscripts[] = "vendor/jquery-validation/jquery.validate.min.js";
        View::$footerscripts[] = 'vendor/html5lightbox/html5lightbox.js';
        View::$footerscripts[] = "vendor/jQuery-Mask-Plugin/src/jquery.mask.js";
        View::$footerscripts[] = 'assets/js/moment/moment.min.js';
        View::$footerscripts[] = 'assets/js/datepicker/daterangepicker.js';
        View::$footerscripts[] = 'assets/js/fileinput.js';


        View::$footerscripts[] = "assets/js/base_tables_datatables.js";

        // Custom Settings
        View::$footerscripts[] = "assets/js/custom.js";
        View::$footerscripts[] = "assets/js/pages/wallet.js";

        View::$footerscripts[] = "assets/js/plugin.js";
        View::$footerscripts[] = "assets/js/countdown.js";
        View::$footerscripts[] = "assets/js/retina.min.js";

        // Bootstrap CSS 
        View::$styles[] = "assets/css/bootstrap.min.css";

        // Basic Stylesheets 
        View::$styles[] = "assets/css/style.css";
        View::$styles[] = "assets/css/custom-style.css";
        View::$styles[] = "assets/css/responsive.css";

        // EXTENSION STYLES
        View::$styles[] = "assets/js/pieprogress/css/rainbow.css";
        View::$styles[] = "assets/js/pieprogress/css/progress.css";
        View::$styles[] = "assets/js/slider-revolution/css/style.css";
        View::$styles[] = "assets/js/slider-revolution/rs-plugin/css/settings.css";
        View::$styles[] = "assets/js/bxslider/jquery.bxslider.css";

        // DEFAULT CSS
        View::$styles[] = "assets/css/font-awesome.min.css";
        View::$styles[] = "assets/css/icons.css";

        View::$styles[] = "vendor/datatables.net-bs/css/dataTables.bootstrap.min.css";
        View::$styles[] = "vendor/datatables.net-buttons-bs/css/buttons.bootstrap.min.css";
        View::$styles[] = "vendor/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css";
        View::$styles[] = "vendor/datatables.net-responsive-bs/css/responsive.bootstrap.min.css";
        View::$styles[] = "vendor/datatables.net-scroller-bs/css/scroller.bootstrap.min.css";
        View::$styles[] = "vendor/bootstrap-daterangepicker/daterangepicker.css";
        View::$styles[] = 'assets/css/fileinput.css';

        // Your Custom Styles 
        View::$styles[] = "assets/css/my-styles.css";
    }
}