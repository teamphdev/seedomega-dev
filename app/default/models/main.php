<?php
class Main_model extends Model
{
    public function __construct()
    {
        parent::__construct();        
        View::$segments = $this->segment;
    }

    /**
     * Get recent active sliders
     * 
     * @package
     * @access private
     * @param (int) $limit : (optional) default[10] : sets the limit of returned rows.
     *
     * @return object
     */
    function getRecentSliders( $limit = 10 )
    {
        $sql = "SELECT DISTINCT s.*, cp.`CompanyName`, cp.`OfferOverview`, cp.`OfferOpening`, cp.`TargetGoal`,um.`Avatar`,
            timestampdiff(DAY, NOW(), cp.`OfferClosing`) 'DaysLeft',
            (SELECT SUM(`TotalAmountAttached`) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `ClientProfileID` = cp.`ClientProfileID`) 'Funded',
            (SELECT COUNT(`UserID`) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `ClientProfileID` = cp.`ClientProfileID`) 'Backers', TRIM(CONCAT(s.`Title`,' ',s.`SubTitle`)) AS FullTitle, '' AS Data
            FROM `slides` s
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = s.`ClientProfileID` AND cp.`Status` = 'Approved' 
            LEFT JOIN `user_meta` um ON um.`UserID` = cp.`UserID`
            WHERE s.`Status` = 1 AND cp.`OfferOpening` <= NOW() AND cp.`OfferClosing` >= NOW()
            ORDER BY s.`Date` DESC LIMIT $limit";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );
        
        return $data;
    }

    /**
     * Get currently fundings (active projects)
     * 
     * @package
     * @access private
     *
     * @return object
    **/
    function getCurrentlyFundings($limit = false)
    {
        $now = date( 'Y-m-d' );
        $where = "WHERE cp.`Status` = 'Approved' AND cp.`OfferOpening` <= NOW() AND cp.`OfferClosing` >= NOW()";

        //$where = ""; //for debugging

        $sql = "SELECT DISTINCT cp.*,um.`Avatar`,timestampdiff(DAY, NOW(), cp.`OfferClosing`) 'DaysLeft',
            (SELECT SUM(`TotalAmountAttached`) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `ClientProfileID` = cp.`ClientProfileID`) 'Funded',
            (SELECT COUNT(`UserID`) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `ClientProfileID` = cp.`ClientProfileID`) 'Backers', '' AS Data
            FROM `client_profiles` cp
            LEFT JOIN `user_meta` um ON um.`UserID` = cp.`UserID`
            $where
            ORDER BY DaysLeft";

        if($limit){
            $sql .= " LIMIT ".$limit;
        }

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Get upcoming projects
     * 
     * @package
     * @access private
     *
     * @return object
    **/
    function getUpcomingEvents()
    {
        $sql = "SELECT * FROM `client_profiles` WHERE `Status` = 'Approved' AND `OfferOpening` > NOW() ORDER BY `OfferOpening`";

        //$sql = "SELECT * FROM `client_profiles` WHERE `Status` = 'Approved' AND `OfferOpening` > '2017-01-01' ORDER BY `OfferOpening`"; //for debugging
            
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getCounts($ID = false)
    {
        if( $ID ){
            $info = User::info( false, $ID );

            if($info->UserLevel == 'Agency') {
                $groupIDs = AppUtility::getChildren($ID);
                $sql = "SELECT a.AccountStatus, COUNT(*) AS 'count'  FROM accounts a LEFT JOIN users u ON u.UserID = a.UserID WHERE a.AccountStatus IN ('Approved', 'Pending', 'On Hold', 'Rejected') AND a.Active = 1 AND (u.ReferrerUserID IN($groupIDs)) GROUP BY a.AccountStatus";

            } else {         
                $sql = "SELECT a.AccountStatus, COUNT(*) AS 'count'  FROM accounts a LEFT JOIN users u ON u.UserID = a.UserID WHERE a.AccountStatus IN ('Approved', 'Pending', 'On Hold', 'Rejected') AND a.Active = 1 AND (u.ReferrerUserID = $ID) GROUP BY a.AccountStatus";
            }
        } else {
            $sql = "SELECT a.AccountStatus, COUNT(*) AS 'count'  FROM accounts a WHERE a.AccountStatus IN ('Approved', 'Pending', 'On Hold', 'Rejected') AND a.Active = 1 GROUP BY a.AccountStatus";
        }
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->AccountStatus] = $row;
        }
        unset($query);
        
        return $data;        
    }

    function getActiveProjects()
    {
        $now = date( 'Y-m-d' );
        $sql = "SELECT cp.*, um.*,timestampdiff(DAY, NOW(), cp.`OfferClosing`) 'DaysLeft', cc.ClientCatID, cc.CCatName, 
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingType='Booking') as TotalBookingAmounts,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingType='Booking' AND bb.BookingStatus='Approved') as TotalRaisedAmt,
            (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus='Approved') as TotalInvestor 
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
            JOIN client_profiles cp ON u.UserID = cp.UserID 
            LEFT JOIN client_categories cc ON cc.ClientCatID = cp.ClientCatID 
            WHERE cp.Status = 'Approved' AND cp.OfferClosing >= '$now'
            ORDER BY cp.OfferOpening DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientData($ID)
    {
        $sql = "SELECT cp.*,timestampdiff(DAY, NOW(), cp.`OfferClosing`) 'DaysLeft',
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingType = 'Booking') as TotalBookingAmounts,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingType = 'Booking' AND bb.BookingStatus='Approved') as TotalRaisedAmt,
            (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID=cp.ClientProfileID AND bb.BookingStatus='Approved') as TotalInvestor  
            FROM client_profiles cp
            WHERE cp.ClientProfileID=$ID";

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getFaqs($clientProfileID, $status = NULL, $userID = NULL)
    {
        $where = "";
        if($status != NULL){
            $where = " AND f.FaqStatus = '$status'";
        }

        $sql = "SELECT *
            FROM faqs f             
            LEFT JOIN users u ON f.UserID = u.UserID 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID
            WHERE f.ClientProfileID = '$clientProfileID'".$where." 
            ORDER BY f.FaqID DESC";

        if($userID != NULL){
            $sql = "SELECT DISTINCT f.* FROM faqs f 
                    LEFT JOIN `bookings` b ON b.`ClientProfileID` = f.`ClientProfileID` AND b.`UserID` = '101228' AND b.`BookingStatus` = 'Approved'
                    WHERE f.`ClientProfileID` = '$clientProfileID' AND f.`PublicView` = '0' AND f.`FaqStatus` = 'Published'
                    UNION ALL SELECT DISTINCT  * FROM faqs WHERE `ClientProfileID` = '$clientProfileID' AND `FaqStatus` = 'Published' AND `PublicView` = '1'
                    ORDER BY FaqID DESC";
        }

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getBlogInfo($clientProfileID)
    {
        $sql = "SELECT * FROM blogs WHERE ClientProfileID = ".$clientProfileID;

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data=  $row;
        }
        unset($query);

        return $data;
    }

    function getRecentPosts($clientProfileID, $userID = NULL)
    {
        $sql = "SELECT * FROM (SELECT DISTINCT bl.* FROM `blogs` bl
                LEFT JOIN `bookings` bo ON (bo.`ClientProfileID`=bl.`ClientProfileID` AND bo.`BookingStatus`='Approved' AND bo.`UserID`='$userID')
                WHERE bl.`ClientProfileID`='$clientProfileID'
                AND bl.`BlogStatus`='Published'
                AND bl.`PublicView`=0
                AND bo.`UserID` IS NOT NULL
                UNION ALL SELECT DISTINCT * FROM `blogs`
                WHERE `ClientProfileID`='$clientProfileID'
                AND `BlogStatus`='Published'
                AND `PublicView`=1) clientview
                ORDER BY `BlogDatePublished` DESC";

        if ($userID == NULL){
            $sql = "SELECT * FROM blogs WHERE ClientProfileID='$clientProfileID' AND BlogStatus='Published' ORDER BY BlogDatePublished DESC ";
        }

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getComments($clientProfileID)
    {
        //$sql = "SELECT * FROM `comments` WHERE `ClientProfileID` = $clientProfileID ORDER BY `CommentDate`";
        $sql = "SELECT DISTINCT c.*,um.`FirstName`,um.`LastName` FROM `comments` c
                LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
                WHERE c.`ClientProfileID` = $clientProfileID ORDER BY c.`CommentID`";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getCommunities($clientProfileID)
    {
        //$sql = "SELECT * FROM `comments` WHERE `ClientProfileID` = $clientProfileID ORDER BY `CommentDate`";
        $sql = "SELECT DISTINCT b.`UserID`,um.`FirstName`,um.`LastName`,um.`JobTitle`,um.`Avatar` FROM `bookings` b
                LEFT JOIN `user_meta` um ON um.`UserID` = b.`UserID`
                WHERE b.`ClientProfileID` = $clientProfileID
                ORDER BY b.`InvestmentBookingID`";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }    

    function getRecentsBlogs($limit = 5)
    {
        $sql = "SELECT bp.*, bc.*, ul.Name as LevelName, ul.Code as LevelCode, um.FirstName, um.LastName 
            FROM blog_posts bp 
            LEFT JOIN blog_categories bc ON bc.BlogCatID = bp.BlogCatID
            LEFT JOIN users u ON bp.UserID = u.UserID
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID WHERE bp.BlogStatus = 'Published' ORDER BY bp.BlogDatePublished DESC LIMIT ".$limit;

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }

    function getRecentTestimonials( $limit = 3 )
    {
        $sql = "SELECT DISTINCT t.*,um.* FROM `testimonials` t
            LEFT JOIN `user_meta` um ON um.`UserID` = t.`UserID`
            WHERE t.`Status` = 1
            ORDER BY t.`Date` DESC LIMIT $limit";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );
        
        return $data;
    }

    function setPw()
    {
        $new = array();
        $sql = "SELECT * FROM users";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();

        while( $row = $query->fetch( PDO::FETCH_CLASS )){
            $new[$row->UserID] = $this->encrypt( $row->Password );
        }
        unset( $query );        

        $newsql = '';
        foreach( $new as $k => $v ){
            $newsql .= "UPDATE users SET Password = '".$v."' WHERE UserID = ".$k.";";
        }

        $newquery = $this->db->prepare($newsql);
        $newquery->execute();
        unset($newquery);
    }
    
    function getUserByID($ID)
    {
        if($ID) {            
        
            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID WHERE  u.UserID = $ID LIMIT 1";

            $query = &$this->db->prepare($sql);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_CLASS);
            unset($query);
            
            return json_encode($row);

        } else {
            return false;
        }
    }

    function getUserByCode($ID)
    {
        if($ID) {            
        
            $sql = "SELECT u.UserID, um.FirstName, um.LastName, ul.Name as LevelName, CONCAT(ul.Code,u.UserID) as UserCode FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID WHERE CONCAT(ul.Code,u.UserID) = '$ID' OR u.UserID = '$ID' LIMIT 1";

            $query = &$this->db->prepare($sql);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_CLASS);
            unset($query);
            
            return json_encode($row);

        } else {
            return false;
        }
    }
	
    function getUsersCount($level = false, $date = false)
    {
        $dateWhere = '';
        switch ($date) {
            case 'thisweek':
                $from = date( 'Y-m-d H:i:s', strtotime( 'monday this week' ) );
                $to = date( 'Y-m-d H:i:s', strtotime( 'sunday this week' ) );

                $dateWhere = " AND DateAdded BETWEEN '$from' AND '$to'";
                break;
            case 'thismonth':
                $from = date('Y-m-01 H:i:s');
                $to = date('Y-m-t H:i:s');

                $dateWhere = " AND DateAdded BETWEEN '$from' AND '$to'";
                break;
            default:
                $dateWhere = "";
                break;
        }

        $where = "WHERE Active = 1";
        switch ($level) {
            case 'agent':
                $where .= " AND Level = 4";
                break;
            case 'agency':
                $where .= " AND Level = 3";
                break;
            default:
                break;
        }

		$where .= $dateWhere;

        $sql = "SELECT SUM(Active) as HeadCount FROM users ".$where;

        $data = $this->db->get_row($sql);

        return isset($data->HeadCount) ? $data->HeadCount : 0;
    }
    
    function updateLanguage($ID)
    {
        $this->db->update('user_meta',array('Language'=>$ID),array('UserID'=>User::info('UserID')));
    }
	
	function getActivityLogs()
    {
        $sql = "SELECT * FROM activity_logs ORDER BY ActivityDate DESC";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
		
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);
        
        return $data;		
    } 

    function getAccounts()
    {
        $sql = "SELECT a.* FROM accounts a WHERE a.Active = 1";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
		
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);
        
        return $data;		
    } 
	
    function getUserLevels()
    {
        $sql = "SELECT UserLevelID,Name FROM user_levels";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->UserLevelID] = $row->Name;			
        }
        unset($query);
        
        return $data;
    }

    function getOptionGroups()
    {
        $sql = "SELECT OptionGroupID, GroupName FROM option_groups";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->OptionGroupID] = $row->GroupName;          
        }
        unset($query);
        
        return $data;
    }

    function getOptionsList()
    {
        $sql = "SELECT o.*,og.GroupName FROM options o LEFT JOIN option_groups og ON o.OptionGroupID = og.OptionGroupID";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;          
        }
        unset($query);
        
        return $data;
    }

    function getOption($ID)
    {
        $sql = "SELECT * FROM options o LEFT JOIN option_groups og ON o.OptionGroupID = og.OptionGroupID WHERE o.OptionID = ".$ID." LIMIT 1";
        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getTAC()
    {
        $sql = "SELECT * FROM terms_and_conditions LIMIT 1";
        $data = $this->db->get_row( $sql );
        
        return $data;
    }

    function getOurBackers()
    {
        $sql = "SELECT a.*, um.`Avatar`, '' AS AvatarData
            FROM `accounts` a
            LEFT JOIN `user_meta` um ON um.`UserID` = a.`UserID`
            WHERE a.`Status` = 'Approved'
            ORDER BY a.`AccountID`";
            
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)) {
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getCompanies()
    {
        $data = array();
        if(isset($this->post['action']) && $this->post['action'] == 'sendsearch') {
            $postdata = $this->post;
            $keyword = trim($postdata['keyword']);

            $sql = "SELECT DISTINCT cp.`ClientProfileID`,cp.`CompanyName`,cp.`TypeOfOffer`,cp.`Status`,cp.`OfferOverview`,
                (SELECT COUNT(`UserID`) FROM `bookings` WHERE `ClientProfileID`=cp.`ClientProfileID` AND `BookingStatus`='Approved') as TotalInvestor,
                (SELECT SUM(`TotalAmountAttached`) FROM `bookings` WHERE `ClientProfileID`=cp.`ClientProfileID` AND `BookingStatus`='Approved') as TotalFunds
                FROM `client_profiles` cp
                WHERE cp.`CompanyName` LIKE '%$keyword%' 
                AND cp.Status = 'Approved'
                ORDER BY cp.`CompanyName`";

            $query = &$this->db->prepare($sql);
            $query->execute();
            while ($row = $query->fetch(PDO::FETCH_CLASS)) {
                $data[] = $row;
            }

            unset($query);
        }

        return $data;
    }

    function doSave()
    {
        if( isset( $this->post['action'] ) ){
            $data = $this->post;
            $action = isset( $data['action'] ) ? $data['action'] : false;
            unset( $data['action'] );

            switch( $action ){

                case "updateoptions": {
                    if( isset( $action ) ){
                        switch( $action ){
                            case "updateoptions": {
                                $this->setSession('message',"Options has been updated!"); 
                                $settings = $this->post['settings'];
                                // $settings['Redirects'] = $this->arrayToString($settings['Redirects']);

                                $optdata = $settings;
                                $filedata = $this->fileUpload( $this->file, $this->post['userid'] );
                                if( count( $filedata ) ){
                                    $optdata = array_merge( $settings, $filedata );
                                }

                                Option::multiUpdate( $optdata );
                            }
                        }
                    }
                    return (object)$data;
                } break;

                case "addoption": {
                    $optionID = $this->db->insert( "options", $data );
                    if( $optionID ){
                        $this->setSession( 'message', "New product item has been registered!" );
                    }

                    //View::redirect('agency');
                } break;

                case "editoption": {
                    $optionID = $data['optionID'];
                    unset( $data['optionID'] );

                    $this->setSession( 'message', "Option has been updated!" );
                    $opID = $this->db->update( "options", $data, array( 'OptionID' => $optionID ) );

                } break;

                case "saveTAC": {
                    $ID = $data['ID'];
                    unset( $data['ID'] );

                    if( $ID == '0' ){ 
                        $opID = $this->db->insert( "terms_and_conditions", $data );
                        $this->setSession( 'message', "Terms and conditions has been added!" );
                    } else {
                        $this->db->update( "terms_and_conditions", $data, array( 'ID' => $ID ) );
                        $this->setSession( 'message', "Terms and conditions has been updated!" );
                    }

                } break;
            }
        }

        return (object) $this->post;
    }

    function doDeleteOption($ID)
    {
        $where = array('OptionID' => $ID);        
        $this->setSession('message',"Option has been deleted!");        
        $rowCount = $this->db->delete("options", $where);
    }
}