<?php
class Api_model extends Model
{
       
    function apiConnect($id,$key) {
        $sql = "SELECT UserID FROM `users` WHERE UserID = '".$id."' AND HashKey = '".$key."' LIMIT 1";
        $usr = $this->db->get_row($sql);

        if(count($usr)) {
            return $usr->UserID;
        } else {
            return false;
        }
    }

    function getIDs($acctid) {
        $sql = "SELECT a.AccountID, a.UserID, a.BankAccountID, um.UserMetaID FROM `accounts` a LEFT JOIN user_meta um ON a.UserID = um.UserID WHERE a.UserID ='".$acctid."'";
        $ids = $this->db->get_row($sql);

        return $ids;
    }
    
    function getAccounts($out='object')
    {
        $sql = "SELECT 
                a.AccountID as CaseFileID, 
                u.UserID as ClientID, 
                CONCAT(um.LastName,' ',um.FirstName) as Name,
                um.DateOfBirth,
                um.Gender,
                um.CivilStatus as MaritalStatus,
                a.AccountEmail as Email,
                um.Phone,
                IF(LENGTH(um.Address4)>0, um.Address4, um.Address) as Address,
                IF(LENGTH(um.Address4)>0, um.Address5, um.Address2) as Address2,
                IF(LENGTH(um.Address4)>0, um.Address6, um.Address3) as Address3,
                um.City,
                um.State,
                um.Country,
                um.PostalCode,
                um.Language as PreferredLanguage,
                a.ApplicationDate as JoinDate,
                GetAgency(u.UserID) as Agen,
                (SELECT CompanyName FROM accounts WHERE UserID = GetAgency(u.UserID) LIMIT 1) as Agency,
                (SELECT CONCAT(umm.LastName,' ',umm.FirstName) as RName FROM `user_meta` umm WHERE umm.UserID = u.ReferrerUserID) as ReferrerName,                
                a.AccountID as PolicyID,
                a.PaymentReceived as FundReceivedDate,
                a.ApprovedDate as ApprovalDate,
                a.ApplicationDate as JoinDate,
                a.CommencementDate,
                a.MaturityDate,
                a.ProductID,
                a.ProductItemID,
                p.ProductName,
                pi.InvestmentAmount,
                a.DepositedAmount,
                a.ReceivedAmount as Contribution,
                (a.ReceivedAmount - pi.InvestmentAmount) as CashAmount,                
                a.ManagementFee,
                a.ManagementFeeWaiver,
                a.OpeningFee,
                a.OpeningFeePromo,
                b.Name as BankName,
                b.Address as BankAddress,
                b.AccountNumber as BankAccountNumber,
                b.AccountName as BankAccountName,
                b.SwiftCode as BankSwiftCode
				       
                FROM accounts a LEFT 
                JOIN account_meta am ON a.AccountMetaID = am.AccountMetaID 
                LEFT JOIN products p ON a.ProductID = p.ProductID
                LEFT JOIN product_items pi ON a.ProductItemID = pi.ProductItemID
                LEFT JOIN bank_accounts b ON a.BankAccountID = b.BankAccountID 
                LEFT JOIN users u ON a.UserID = u.UserID 
                LEFT JOIN user_meta um ON u.UserID = um.UserID
                WHERE a.Active = 1 AND a.AccountStatus='Approved'";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            if($out == 'array') {
                $data[] = (array)$row;
            } else {
                $data[] = $row;			
            }
        }
        unset($query);
        
        return $data;
    } 

    function updateAccounts($cid, $table=false)
    {
        $return = false;

        $ids = $this->getIDs($cid);

        $userid = $cid;
        $metaid = $ids->UserMetaID;
        $bankid = $ids->BankAccountID;
        $acctid = $ids->AccountID;

        if($table) {
            switch($table) {
                case 'user_table':
                    $return = $this->db->update("user_meta",$this->post, array('UserMetaID'=>$metaid));
                break; 
                case 'bank_table':
                    $return = $this->db->update("bank_accounts",$this->post, array('BankAccountID'=>$bankid));
                break; 
                case 'account_table':
                    $return = $this->db->update("accounts",$this->post, array('UserID'=>$userid));
                break; 
            }

        } else {    
            if(isset($this->post['user'])){
                $meta = $this->post['user'];
            }

            if(isset($this->post['bank'])){
                $bank = $this->post['bank'];
            }
            if(isset($this->post['account'])){
                $acct = $this->post['account'];
            }      

            if(isset($meta)){
                $metaID = $this->db->update("user_meta", $meta, array('UserMetaID'=>$metaid));
            }

            if(isset($bank)){
                $bankID = $this->db->update("bank_accounts", $bank, array('BankAccountID'=>$bankid));
            }

            if(isset($acct)){
                $acctID = $this->db->update("accounts", $acct, array('AccountID'=>$acctid));
            }

            $desc = "Updated Client info from  API #GA-CF-".$acctid."\r\n";
            App::activityLog($desc);

            $return = true;
            
        }
        
        return $return;
    } 
}