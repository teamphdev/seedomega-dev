<?php
class Forums_model extends Model
{
    function __construct()
    {
        parent::__construct();
        View::$segments = $this->segment;
    }

    /**
     * Get client info
     *
     * @access Private 
     * @param (int) $userID          : (required) : user id
     * @param (boolean) $IsNotClient : (optional) : if login is not client
     * @return na
     **/
    function getClientInfo( $userID, $IsNotClient = false )
    {
        $where = '';
        if( $IsNotClient ){
            $where = ' WHERE cp.`ClientProfileID` = '.$IsNotClient.' ';
        } else {
            $where = ' WHERE cp.`UserID` = '.$userID.' ';
        }

        $sql = "SELECT cp.*, ba.*, um.FirstName, um.LastName, '' AS Disabled
                FROM client_profiles as cp
                LEFT JOIN users u ON cp.UserID = u.UserID
                LEFT JOIN user_meta um ON um.UserID = u.UserID
                LEFT JOIN bank_accounts ba ON ba.UserID = u.UserID
                $where LIMIT 1";

        $data = $this->db->get_row($sql);

        return $data;
    }

    /**
     * Get client info by user id
     *
     * @access Private 
     * @param (int) $userID          : (required) : user id
     * @return na
     **/
    function getClientDataByUser( $userID )
    {
        // $sql = "SELECT DISTINCT cp.*, ba.*, um.`FirstName`, um.`LastName`, fi_avatar.`FileSlug` AvatarSlug, fi_photo.`FileSlug` PhotoSlug,
        //     timestampdiff(DAY, NOW(), cp.`OfferClosing`) 'DaysLeft',
        //     (SELECT SUM(`TotalAmountAttached`) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `ClientProfileID` = cp.`ClientProfileID`) 'Funded',
        //     (SELECT COUNT(`UserID`) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `ClientProfileID` = cp.`ClientProfileID`) 'Backers'
        // FROM `bookings` b
        // LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        // LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
        // LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
        // LEFT JOIN `bank_accounts` ba ON ba.`UserID` = u.`UserID`
        // LEFT JOIN `file_items` fi_photo ON fi_photo.`FileID` = cp.`CompanyPhoto`
        // LEFT JOIN `file_items` fi_avatar ON fi_avatar.`FileID` = um.`Avatar`
        // WHERE cp.`ClientProfileID` IS NOT NULL AND b.`BookingStatus` = 'Approved' AND b.`UserID` = $userID";

        // $sql = "SELECT DISTINCT cp.*, um.`FirstName`, um.`LastName`, fi_avatar.`FileSlug` AvatarSlug, fi_photo.`FileSlug` PhotoSlug,
        //     (SELECT SUM(forum.`Views`) FROM `forum_categories` fc_view LEFT JOIN `forum` ON forum.`CatID` = fc_view.`CatID` WHERE fc_view.`ClientProfileID` = b.`ClientProfileID`) AS Views,
        //     (SELECT COUNT(`ForumID`) FROM `forum` INNER JOIN `forum_categories` fc_topic ON fc_topic.`CatID` = `forum`.`CatID` WHERE fc_topic.`ClientProfileID` = b.`ClientProfileID`) AS Topics,
        //     (SELECT COUNT(fr_rep.`ForumID`) FROM `forum_replies` fr_rep INNER JOIN `forum` f_rep ON f_rep.`ForumID`= fr_rep.`ForumID` INNER JOIN `forum_categories` fc_rep ON fc_rep.`CatID` = f_rep.`CatID` WHERE fc_rep.`ClientProfileID` = b.`ClientProfileID`) AS Replies
        // FROM `bookings` b
        // LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        // LEFT JOIN `forum_categories` fc ON fc.`ClientProfileID` = cp.`ClientProfileID`
        // LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
        // LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
        // LEFT JOIN `file_items` fi_photo ON fi_photo.`FileID` = cp.`CompanyPhoto`
        // LEFT JOIN `file_items` fi_avatar ON fi_avatar.`FileID` = um.`Avatar`
        // WHERE cp.`ClientProfileID` IS NOT NULL AND b.`BookingStatus` = 'Approved' AND b.`UserID` = $userID";

        $sql = "SELECT DISTINCT cp.*, um.`FirstName`, um.`LastName`, fi_avatar.`FileSlug` AvatarSlug, fi_photo.`FileSlug` PhotoSlug,
            GetTotalViewsPerClient( b.`ClientProfileID` ) AS Views,
            GetTotalTopicsPerClient( b.`ClientProfileID` ) AS Topics,
            GetTotalRepliesPerClient( b.`ClientProfileID` ) AS Replies,
            GetLatestReplyPerClient( b.`ClientProfileID` ) AS LastPost
        FROM `bookings` b
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        LEFT JOIN `forum_categories` fc ON fc.`ClientProfileID` = cp.`ClientProfileID`
        LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
        LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
        LEFT JOIN `file_items` fi_photo ON fi_photo.`FileID` = cp.`CompanyPhoto`
        LEFT JOIN `file_items` fi_avatar ON fi_avatar.`FileID` = um.`Avatar`
        WHERE cp.`ClientProfileID` IS NOT NULL AND b.`BookingStatus` = 'Approved' AND b.`UserID` = $userID";

        //$data = $this->db->get_row( $sql );

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);
            
        return $data;
    }

    /**
     * Get client info by client proile id
     *
     * @access Private 
     * @param (int) $clientID          : (required) : client profie id
     * @return na
     **/
    function getClientDataByClientID( $clientID )
    {
        $sql = "SELECT DISTINCT cp.*
        FROM `client_profiles` cp
        LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
        LEFT JOIN `user_meta` um ON um.`UserID` = u.`UserID`
        WHERE cp.`ClientProfileID` = $clientID
        LIMIT 1";

        $data = $this->db->get_row( $sql );

        return $data;
    }

    /**
     * Get forum category list
     *
     * @access Private 
     * @param (int) $ID : (optional) default[false] : forum category id
     * @return NA
     **/
    function getForumCategory( $ClientProfileID, $ID = false )
    {
        $where = '';
        if( $ID ){ $where = " AND `CatID` = $ID "; }
        $sql = "SELECT DISTINCT * FROM `forum_categories` WHERE `ClientProfileID` = $ClientProfileID $where ORDER BY `CatID`";

        //$data = $this->db->get_row( $sql );

        $data = [];
        $query = &$this->db->prepare( $sql );
        $query->execute();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get forum category list
     *
     * @access Private 
     * @param (int) $ClientProfileID : (required) default[false] : client profile id
     * @return NA
     **/
    function getForumCategoryOnlyForOptions( $ClientProfileID )
    {
        $sql = "SELECT DISTINCT * FROM `forum_categories` WHERE `ClientProfileID` = $ClientProfileID ORDER BY `CatID`";

        $data = [];
        $query = &$this->db->prepare( $sql );
        $query->execute();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[$row->CatID] = $row->CatName;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get forum sub-category list
     *
     * @access Private 
     * @param (int) $categoryid : (required) default[false] : forums category id
     * @return NA
     **/
    function getForumSubCategoryForOptions( $categoryid )
    {
        $sql = "SELECT DISTINCT `SubCatID`, `SubCatName` FROM `forum_sub_categories` WHERE `CatID` = $categoryid ORDER BY `SubCatID`";

        $data = [];
        $query = &$this->db->prepare( $sql );
        $query->execute();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get forum sub-category list
     *
     * @access Private 
     * @param (int) $ID : (optional) default[false] : forum category id
     * @return NA
     **/
    function getForumSubCategory( $ID = false )
    {
        $where = ' WHERE `CatID` IN ( SELECT DISTINCT `CatID` FROM `forum_categories` ) ';
        if( $ID ){
            $where = ' WHERE `SubCatID` = '.$ID.' ';
        }
        $sql = "SELECT * FROM `forum_sub_categories` $where ORDER BY `SubCatID`";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            if( $ID ){
                $data[] = $row;
            } else {
               $data[ $row->CatID ][] = $row;
            }
        }
        unset($query);

        return $data;
    }

    /**
     * Get forum category by forum id
     *
     * @access Private 
     * @param (int) $forumID : (required) : forum id
     * @return NA
     **/
    function getForumCategoryByForumID( $forumID )
    {
        $sql = "SELECT fc.*
        FROM `forum` f
        LEFT JOIN `forum_categories` fc ON fc.`CatID` = f.`CatID`
        WHERE f.`ForumID` = $forumID
        ORDER BY f.`DatePosted` DESC";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Get forum sub-category by forum id
     *
     * @access Private 
     * @param (int) $forumID : (required) : forum id
     * @return NA
     **/
    function getForumSubCategoryByForumID( $forumID )
    {
        $sql = "SELECT fsc.*
        FROM `forum` f
        LEFT JOIN `forum_sub_categories` fsc ON fsc.`SubCatID` = f.`SubCatID`
        WHERE f.`ForumID` = $forumID";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Get forum summary list
     *
     * @access Private 
     * @param (int) $ClientProfileID : (required) : client profile id
     * @return NA
     **/
    function getForumSummary( $ClientProfileID )
    {
        $sql = "SELECT DISTINCT fc.`CatName`, fsc.*, '' AS Data,
                ( SELECT COUNT(*) FROM `forum` WHERE `CatID` = fsc.`CatID` AND `SubCatID` = fsc.`SubCatID` ) AS Topics,
                ( SELECT COUNT(*) FROM `forum_replies` fr1 LEFT JOIN `forum` f1 ON f1.`ForumID` = fr1.`ForumID` WHERE f1.`CatID` = fsc.`CatID` AND f1.`SubCatID` = fsc.`SubCatID` ) AS Posts,
                ( SELECT CONCAT( um.`FirstName`, ' ', um.`LastName`, '|', fr2.`DatePosted`, '|', fi.`FileSlug` )
                FROM `forum_replies` fr2
                LEFT JOIN `forum` f2 ON f2.`ForumID` = fr2.`ForumID`
                LEFT JOIN `user_meta` um ON um.`UserID` = fr2.`UserID`
                LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
                WHERE f2.`CatID` = fsc.`CatID` AND f2.`SubCatID` = fsc.`SubCatID` ORDER BY fr2.`DatePosted` DESC LIMIT 1 ) As LastPost
            FROM `forum_categories` fc
            LEFT JOIN `forum_sub_categories` fsc ON fsc.`CatID` = fc.`CatID`
            WHERE fc.`ClientProfileID` = $ClientProfileID AND fsc.`SubCatID` IS NOT NULL
            ORDER BY `CatID`, `SubCatID`";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[ $row->CatID ][] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Get forum by category list
     *
     * @access Private 
     * @return NA
     **/
    function getForumByCategory( $catid, $subcatid )
    {
        // $sql = "SELECT DISTINCT
        //             f.*,
        //             fc.`CatName` AS Category,
        //             fsc.`SubCatName` AS SubCategory,
        //             fsc.`SubCatNameSlug`,
        //             CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Author,
        //             fi.`FileSlug` AS AuthorFileSlug,
        //             CONCAT( um1.`FirstName`, ' ', um1.`LastName` ) AS LastPoster,
        //             fi1.`FileSlug` AS LastPosterFileSlug,
        //             fr.`DatePosted` AS LastPosterDate,
        //             ( SELECT COUNT(`ForumID`) FROM `forum_replies` WHERE `ForumID` = f.`ForumID` ) AS Replies
        //     FROM `forum` f
        //         LEFT JOIN `forum_categories` fc ON fc.`CatID` = f.`CatID`
        //         LEFT JOIN `forum_sub_categories` fsc ON fsc.`SubCatID` = f.`SubCatID`
        //         LEFT JOIN `user_meta` um ON um.`UserID` = f.`UserID`
        //         LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`    
        //         LEFT JOIN `forum_replies` fr ON fr.`ForumID` = f.`ForumID` AND fr.`DatePosted` = ( SELECT MAX(`DatePosted`) FROM `forum_replies` WHERE `ForumID` = f.`ForumID` )
        //         LEFT JOIN `user_meta` um1 ON um1.`UserID` = fr.`UserID`
        //         LEFT JOIN `file_items` fi1 ON fi1.`FileID` = um1.`Avatar`
        //     WHERE f.`CatID` = $catid AND f.`SubCatID` = $subcatid
        //     ORDER BY f.`Pinned` DESC, f.`CatID`, f.`SubCatID`;";

        $sql = "SELECT DISTINCT
                    f.*,
                    fc.`CatName` AS Category,
                    fsc.`SubCatName` AS SubCategory,
                    fsc.`SubCatNameSlug`,
                    CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Author,
                    fi.`FileSlug` AS AuthorFileSlug,
                    CONCAT( um1.`FirstName`, ' ', um1.`LastName` ) AS LastPoster,
                    fi1.`FileSlug` AS LastPosterFileSlug,
                    fr.`DatePosted` AS LastPosterDate,
                    GetTotalReply( f.`ForumID` ) AS Replies
            FROM `forum` f
                LEFT JOIN `forum_categories` fc ON fc.`CatID` = f.`CatID`
                LEFT JOIN `forum_sub_categories` fsc ON fsc.`SubCatID` = f.`SubCatID`
                LEFT JOIN `user_meta` um ON um.`UserID` = f.`UserID`
                LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`    
                LEFT JOIN `forum_replies` fr ON fr.`ForumID` = f.`ForumID` AND fr.`DatePosted` = GetLatestReplyDate( f.`ForumID` )
                LEFT JOIN `user_meta` um1 ON um1.`UserID` = fr.`UserID`
                LEFT JOIN `file_items` fi1 ON fi1.`FileID` = um1.`Avatar`
            WHERE f.`SubCatID` = $subcatid
            ORDER BY f.`Pinned` DESC, f.`DatePosted` DESC;";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[ $row->Pinned ][] = $row;
        }
        unset($query);
        
        return $data;
    }

    /**
     * Get forum category for options list
     *
     * @access Private 
     * @param (int) $ClientProfileID : (required) : client profile id
     * @return NA
     **/
    function getForumCategoryForOptions( $ClientProfileID )
    {
        $ctr = 0;
        $category = '';
        $sql = "SELECT CONCAT( fc.`CatID`, '-', fsc.`SubCatID` ) AS IDs, fc.`CatName` AS Category, CONCAT( '- ', fsc.`SubCatName` ) AS SubCategory
            FROM `forum_categories` fc
            LEFT JOIN `forum_sub_categories` fsc ON fsc.`CatID` = fc.`CatID`
            WHERE fc.`ClientProfileID` =  $ClientProfileID  AND fsc.`SubCatName` IS NOT NULL
            ORDER BY fc.`CatID`, fsc.`SubCatID`";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $ctr++;
            if( $ctr == 1 ){ $data[ 'disabled'.$ctr ] = $row->Category; }
            if( $row->Category != $category ){
                $data[ 'disabled'.$ctr ] = $row->Category;
            }
            $data[ $row->IDs ] = $row->SubCategory;
            $category = $row->Category;
        }
        unset($query);

        return $data;
    }

    /**
     * Get topic
     *
     * @access Private 
     * @return NA
     **/
    function getForumTopic( $forumid )
    {
        $sql = "SELECT * FROM ( SELECT `ForumID`, f.`CatID`, f.`SubCatID`, `SubCatName`, `SubCatNameSlug`, `Title`, `Description`, `Epigram`, `Content`, `DatePosted`, f.`UserID`, um.`FirstName`, um.`LastName`, fi.`FileSlug`, '' AS Data,
                ( GetTotalPost( f.`UserID` ) ) AS TotalPost
            FROM `forum` f
            LEFT JOIN `forum_sub_categories` fsc ON fsc.`SubCatID` = f.`SubCatID`
            LEFT JOIN `user_meta` um ON um.`UserID` = f.`UserID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
            WHERE f.`ForumID` = $forumid
            UNION ALL
            SELECT `ForumID`, '0' AS `CatID`, '0' AS `SubCatID`, '' AS SubCatName, '' AS SubCatNameSlug, '' AS Title, '' AS Description, `Epigram`, `Content`, `DatePosted`, fr.`UserID`, um.`FirstName`, um.`LastName`, fi.`FileSlug`, '' AS Data,
                ( GetTotalPost( fr.`UserID` ) ) AS TotalPost
            FROM `forum_replies` fr
            LEFT JOIN `user_meta` um ON um.`UserID` = fr.`UserID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
            WHERE fr.`ForumID` = $forumid ) AS topics
            ORDER BY `DatePosted`";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    /**
     * Get topics
     *
     * @access Private 
     * @param (int) $clientProfileID : (required) : Client profile ID
     * @param (int) $forumID         : (optional) : Forum ID
     * @return object
     **/
    function getForumTopics( $clientProfileID, $forumID = false )
    {
        $where = $forumID ? " AND f.`ForumID` = $forumID " : "";
        $sql = "SELECT f.*, fc.`CatName`, fsc.`SubCatName`, um.`FirstName`, um.`LastName`, fi.`FileSlug`
        FROM `forum` f
        LEFT JOIN `forum_categories` fc ON fc.`CatID` = f.`CatID`
        LEFT JOIN `forum_sub_categories` fsc ON fsc.`SubCatID` = f.`SubCatID`
        LEFT JOIN `user_meta` um ON um.`UserID` = f.`UserID`
        LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
        WHERE fc.`ClientProfileID` = $clientProfileID $where
        ORDER BY f.`ForumID` DESC";
    
        $data = [];    
        if( $forumID ){
            $data = $this->db->get_row( $sql );
        } else {
            $query = &$this->db->prepare( $sql );
            $query->execute();
            while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                $data[] = $row;
            }
            unset( $query );
        }
        
        return $data;
    }

    /**
     * Get total posts
     *
     * @access Private 
     * @return NA
     **/
    function getForumTotalPostOwn( $userID )
    {
        // $sql = "SELECT SUM( Total ) AS Total FROM (
        //     SELECT COUNT(`UserID`) AS Total 
        //     FROM `forum` 
        //     WHERE `UserID` = $userID
        //     UNION ALL
        //     SELECT COUNT( `UserID` ) AS Total
        //     FROM `forum_replies`
        //     WHERE `UserID` = $userID ) AS total;";

        $sql = "SELECT GetTotalPost( $userID ) AS Total;";

        $data = $this->db->get_row( $sql );

        // $query = &$this->db->prepare( $sql );
        // $query->execute();
        // $data = array();
        // while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
        //     $data[] = $row;
        // }
        // unset($query);

        return $data;
    }

    /**
     * Get latest epigram by user id
     *
     * @access Private 
     * @param (int) $userID          : (required) : user id
     * @return na
     **/
    function getLatestEpigram( $userID )
    {
        $sql = "SELECT Epigram FROM (
            SELECT Epigram, DatePosted FROM `forum` WHERE UserID = $userID
            UNION
            SELECT Epigram, DatePosted FROM `forum_replies` WHERE UserID = $userID) AS Epigram
        ORDER BY DatePosted DESC
        LIMIT 1";

        $data = $this->db->get_row( $sql );

        return $data;
    }
    
    function doSaveCategory( $clientProfileID = false )
    {
        $catID = false;

        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "add": {
                    $data = $this->post;
                    $catdata = $this->post['category'];

                    unset( $data['action'] );

                    $catdata['ClientProfileID'] = $clientProfileID;
                    $catdata['CatNameSlug'] = str_ireplace( " ", "-", strtolower( $catdata['CatName'] ) );
                    $catID = $this->db->insert( "forum_categories", $catdata );

                    if( $catID ){
                        $this->setSession('message',"New forum category has been added!");
                        App::activityLog("Added forum category ID#".$catID.'.');
                    }
                    
                } break;

                case "edit": {
                    $data = $this->post;
                    $categoryid = $data['categoryid'];
                    $catdata = $this->post['category'];

                    unset( $data['action'] );

                    $where = array( 'CatID' => $categoryid );

                    $catdata['CatNameSlug'] = str_ireplace( " ", "-", strtolower( $catdata['CatName'] ) );
                    $catID = $this->db->update( "forum_categories", $catdata, $where );

                    if( $catID ){
                        $this->setSession('message',"Forum category has been updated!");
                        App::activityLog("Forum category has been updated ID#".$categoryid.'.');
                    }                    
                } break;
            }
        }

        return $catID;
    }
    
    function doSaveSubCategory()
    {
        $catID = false;

        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "add": {
                    $data = $this->post;
                    $catdata = $this->post['category'];
                    
                    $iclass = $catdata['SubCatIconClass'];
                    $timestwo = $catdata['TimesTwo'];
                    $timestwo = isset( $timestwo ) ? ' '.$timestwo : '';

                    unset( $data['action'] );
                    unset( $catdata['TimesTwo'] );
                    unset( $catdata['SubCatIconClass'] );

                    $catdata['SubCatIconClass'] = $iclass . $timestwo;
                    $catdata['SubCatNameSlug'] = str_ireplace( " ", "-", strtolower( $catdata['SubCatName'] ) );
                    $catID = $this->db->insert( "forum_sub_categories", $catdata );

                    if( $catID ){
                        $this->setSession('message',"New forum sub-category has been added!");
                        App::activityLog("Added forum sub-category ID#".$catID.'.');
                    }
                    
                } break;

                case "edit": {
                    $data = $this->post;
                    $subcatid = $data['subcatid'];
                    $catdata = $this->post['category'];

                    $iclass = $catdata['SubCatIconClass'];
                    $timestwo = $catdata['TimesTwo'];
                    $timestwo = isset( $timestwo ) ? ' '.$timestwo : '';

                    unset( $data['action'] );
                    unset( $catdata['TimesTwo'] );
                    unset( $catdata['SubCatIconClass'] );

                    $catdata['SubCatIconClass'] = $iclass . $timestwo;
                    $where = array( 'SubCatID' => $subcatid );
                    $catdata['SubCatNameSlug'] = str_ireplace( " ", "-", strtolower( $catdata['SubCatName'] ) );
                    $catID = $this->db->update( "forum_sub_categories", $catdata, $where );

                    if( $catID ){
                        $this->setSession('message',"Forum sub-category has been updated!");
                        App::activityLog("Forum sub-category has been updated ID#".$subcatid.'.');
                    }                    
                } break;
            }
        }

        return $catID;
    }
    
    function doSaveTopic()
    {
        $forumID = false;

        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "addtopic": {
                    $data = $this->post;
                    $topic = $this->post['topic'];
                    $ids = explode( '-', $topic['IDs'] );
                    $topic['CatID'] = $ids[0];
                    $topic['SubCatID'] = $ids[1];
                    $userinfo = User::info();

                    unset( $data['action'] );
                    unset( $topic['IDs'] );

                    $topic['UserID'] = $userinfo->UserID;
                    if( isset( $topic['Content'] ) ){
                        $content = trim( $topic['Content'] );
                        if( strlen( $content ) ){
                            $topic['TitleSlug'] = str_ireplace( " ", "-", strtolower( $topic['Title'] ) );
                            $forumID = $this->db->insert( "forum", $topic );                            
                            if( $forumID ){
                                $this->setSession( 'message', "New topic has been added!" );
                                App::activityLog( "Added new topic ID#$forumID" );
                            }
                        } else {
                            $this->setSession( 'message', "Please provide your topic content! ");
                        }
                    } else {
                        $this->setSession( 'message', "Please provide your topic content! ");
                    }
                } break;

                case "fulledit": {
                    $data = $this->post;
                    $topic = $this->post['topic'];
                    $ids = explode( '-', $topic['IDs'] );
                    $topic['CatID'] = $ids[0];
                    $topic['SubCatID'] = $ids[1];                    
                    $forumID = isset( $topic['ForumID'] ) ? $topic['ForumID'] : '0';

                    unset( $data['action'] );
                    unset( $topic['IDs'] );
                    unset( $topic['ForumID'] );

                    if( isset( $topic['Content'] ) ){
                        $content = trim( $topic['Content'] );
                        if( strlen( $content ) ){
                            $where = array( 'ForumID' => $forumID );
                            $topic['TitleSlug'] = str_ireplace( " ", "-", strtolower( $topic['Title'] ) );

                            $this->db->update( "forum", $topic, $where );
                            
                            $this->setSession( 'message', "Topic has been updated!" );
                            App::activityLog( "Updated topic ID#$forumID" );
                        } else {
                            $this->setSession( 'message', "Please provide your topic content! ");
                        }
                    } else {
                        $this->setSession( 'message', "Please provide your topic content! ");
                    }
                } break;

                case "edittopic": {
                    $data = $this->post;
                    $topic = $this->post['topic'];
                    $forumID = isset( $topic['ForumID'] ) ? $topic['ForumID'] : '0';

                    unset( $data['action'] );
                    unset( $topic['ForumID'] );

                    if( isset( $topic['Content'] ) ){
                        $content = trim( $topic['Content'] );
                        if( strlen( $content ) ){
                            $where = array( 'ForumID' => $forumID );
                            $this->db->update( "forum", $topic, $where );                            
                            $this->setSession( 'message',"Topic content has been saved!" );
                            App::activityLog( "Topic content has been saved, ID#$forumID" );
                        } else {
                            $this->setSession( 'message', "Please provide your topic content!" );
                        }
                    } else {
                        $this->setSession( 'message', "Please provide your topic content!" );
                    }
                } break;

                case "reply": {
                    $data = $this->post;
                    $topic = $this->post['topic'];

                    unset( $data['action'] );

                    if( isset( $topic['Content'] ) ){
                        $content = trim( $topic['Content'] );
                        if( strlen( $content ) ){
                            $forumID = $this->db->insert( "forum_replies", $topic );
                            if( $forumID ){
                                $this->setSession( 'message',"Topic reply has been added!" );
                                App::activityLog( "Topic reply has been added ID#$forumID" );
                            }
                        } else {
                            $this->setSession( 'message', "Please provide your topic reply!" );
                        }
                    } else {
                        $this->setSession( 'message', "Please provide your topic reply!" );
                    }
                } break;
            }
        }

        return $forumID;
    }
    
    function doDeleteTopic( $fid )
    {
        $this->db->delete( 'forum', array( 'ForumID' => $fid ) );
        $this->db->delete( 'forum_replies', array( 'ForumID' => $fid ) );
        App::activityLog( "Forum topic and replies has been deleted. ID#$fid." );

        return $fid;
    }
    
    function doSavePin( $forum )
    {
        $forumID = false;
        $pinval = $forum['pinval'] == '1' ? $forum['pinval'] : '0';
        $pin = array( 'Pinned' => $pinval );
        $where = array( 'ForumID' => $forum['forumid'] );
        $forumID = $this->db->update( "forum", $pin, $where );

        return $forumID;
    }

    function doSaveTopicView( $forumid )
    {
        $query = 'UPDATE `forum` SET `Views` = `Views`+1 WHERE `ForumID` = '. $forumid;        
        $this->db->query( $query );
    }

    function doDeleteCategory( $tblname, $colname, $id )
    {
        $where = array( $colname => $id );
        $rowCount = $this->db->delete( $tblname, $where );
        if( $rowCount ){
            $this->setSession('message',"Forum category has been deleted!");
            App::activityLog("Deleted Forum Category #ID".$UserLevelID.'.'); 
        }

        if( $tblname == 'forum_categories' ){
            $this->db->delete( 'forum_sub_categories', $where );
        }
    }
    
    /**
     * Get own referral list
     * @access   Private
     * @param    (int) $ID   : (required) : the user's ID
     * @return   Object
     **/
    function getSeeder($ID)
    {
        $sql = "SELECT u.*,um.*,ul.Name,ul.Code, 
        (SELECT CONCAT(uul.Code, uu.UserID) as FirstAgentID FROM users uu LEFT JOIN user_levels uul ON uu.Level = uul.UserLevelID WHERE uu.UserID = u.ReferrerUserID) as AgentID
            FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE u.UserID = ".$ID." AND u.Level = 7 LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    }

    /**
     * Get seeder info
     * @access   Private 
     * @param    (string) $inactive : (optional) default[NULL] : Active condition 
     * @return   Array of Object
     **/
    function getSeeders($inactive = '')
    {
        $sql = "SELECT u.*,um.*,ul.Name,ul.Code FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID";
        $where = " WHERE u.Active = 1";
        if($inactive == 'yes') {
            $where = " WHERE u.Active != 1";
        } 
        $sql .= $where;
        $sql .= " AND u.Level = 7 ORDER BY u.UserID";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    }
}