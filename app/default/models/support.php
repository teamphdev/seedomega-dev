<?php
class Support_model extends Model 
{
    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }

    function doSave()
    {
        if( isset( $this->post['action'] ) ) {
            $ticket = isset($this->post['ticket']) ? $this->post['ticket'] : '';
            switch ($this->post['action']) {

                case "saveticket":
                    $ticket['CreatedAt']= date('Y-m-d H:i:s');
                    $newTicket = $this->db->insert('tickets',$ticket);

                    if ($this->file) {
                        $filedata = $this->fileUpload($this->file, $ticket['UserId']);
                        if (count($filedata)) {
                            $rowCount = $this->db->update("tickets", $filedata, ['TicketId' => $newTicket]);
                        }
                    }

                    $this->setSession( 'message',"Your ticket has been submitted!" );
                    App::activityLog( "User submitted ticket #".$newTicket.'.' );

                    View::redirect( 'support/dashboard' );
                    break;

                case "savereply":
                    $reply = $this->post['reply'];
                    $event = [
                        'Type'=>'COMMENT',
                        'Content'=>$reply['Content'],
                        'File'=>0,
                        'EventDate'=>date('Y-m-d H:i:s'),
                        'TicketId'=>$reply['TicketId'],
                        'UserId'=>$reply['UserId']
                    ];

                    $newEvent = $this->db->insert('ticket_events',$event);

                    $this->setSession( 'message',"Your reply has been submitted!" );
                    View::redirect( 'support/dashboard' );
                    break;

                case "viewsavereply":
                    $reply = $this->post['reply'];
                    $event = [
                        'Type'=>'COMMENT',
                        'Content'=>$reply['Content'],
                        'File'=>0,
                        'EventDate'=>date('Y-m-d H:i:s'),
                        'TicketId'=>$reply['TicketId'],
                        'UserId'=>$reply['UserId']
                    ];

                    $newEvent = $this->db->insert('ticket_events',$event);

                    $this->setSession( 'message',"Your reply has been submitted!" );
                    break;
            }
        }


    }

    function getTickets($status='', $priority ='')
    {
        if ($status !=''){

                $sql = "SELECT t.*, um.FirstName, um.LastName,
                (SELECT COUNT(e.TicketId) FROM ticket_events e WHERE e.TicketId = t.TicketId AND e.Type = 'COMMENT') as TotalReplies
                FROM tickets t
                LEFT JOIN user_meta um ON um.UserID=t.UserID
                WHERE Status = '$status'
                ORDER BY CreatedAt DESC";

        } else {
            $sql = "SELECT t.*, um.FirstName, um.LastName,
                (SELECT COUNT(e.TicketId) FROM ticket_events e WHERE e.TicketId = t.TicketId AND e.Type = 'COMMENT') as TotalReplies
                FROM tickets t
                LEFT JOIN user_meta um ON um.UserID=t.UserID
                ORDER BY CreatedAt DESC";
        }

        if ($priority =='URGENT'){
            $sql = "SELECT t.*, um.FirstName, um.LastName,
                (SELECT COUNT(e.TicketId) FROM ticket_events e WHERE e.TicketId = t.TicketId AND e.Type = 'COMMENT') as TotalReplies
                FROM tickets t
                LEFT JOIN user_meta um ON um.UserID=t.UserID
                WHERE Priority = 'URGENT'
                ORDER BY CreatedAt DESC";
        }

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getClientTickets($status, $userId)
    {
        $sql = "SELECT t.*, um.FirstName, um.LastName,
                (SELECT COUNT(e.TicketId) FROM ticket_events e WHERE e.TicketId = t.TicketId AND e.Type = 'COMMENT') as TotalReplies
                FROM tickets t
                LEFT JOIN user_meta um ON um.UserID=t.UserID
                
                 WHERE t.Status = '$status' AND t.UserId = '$userId'
                ORDER BY t.CreatedAt DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getTicketReplies($ticket_id)
    {
        $sql = "SELECT r.*, um.FirstName,um.LastName, um.Avatar, f.FileSlug as avatarImg 
                FROM ticket_events r 
                LEFT JOIN user_meta um ON um.UserID=r.UserId 
                LEFT JOIN file_items as f ON f.FileID = um.Avatar                   
                WHERE r.TicketId = ".$ticket_id ." AND r.Type = 'COMMENT'";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    public function getTicketDetails($ticket_id)
    {
        $sql = "SELECT t.*, um.FirstName,um.LastName, um.Avatar, f.FileSlug as avatarImg,
                    (CASE WHEN ul.Name = 'User' THEN 'Seeder' ELSE ul.Name END) AS UserLevel
                FROM tickets t 
                LEFT JOIN users u ON u.UserID=t.UserID
                LEFT JOIN user_levels ul ON ul.UserLevelID=u.Level
                LEFT JOIN user_meta um ON um.UserID=t.UserID 
                LEFT JOIN file_items as f ON f.FileID = um.Avatar                   
                WHERE t.TicketId = ".$ticket_id;

        $data = $this->db->get_row($sql);

        return $data;
    }

    public function getTotalResponses()
    {
        $sql = "SELECT count(EventId) as replies FROM ticket_events WHERE type = 'COMMENT'";
        $data = $this->db->get_row($sql);

        return $data->replies;
    }

    public function markTicket($ticket_id, $action)
    {
        if ($action =='resolved'){
            $this->db->update("tickets", ['Status'=>'CLOSED'], ['TicketId' => $ticket_id]);
        }

        if ($action =='urgent'){
            $this->db->update("tickets", ['Priority'=>'URGENT'], ['TicketId' => $ticket_id]);
        }
    }
}