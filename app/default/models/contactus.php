<?php
class Contactus_model extends Model 
{
    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }
       
    function getInquiryByID($ID)
    {
        $sql = "SELECT cu.*, ul.Code as ProccessBy_RoleCode, um.FirstName as ProccessBy_FirstName, um.LastName as ProccessBy_LastName  
        FROM contact_us cu 
        LEFT JOIN users u ON cu.ProccessBy = u.UserID
        LEFT JOIN user_meta um ON cu.ProccessBy = um.UserID 
        LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE InquiryID = ".$ID." LIMIT 1";

        $data = $this->db->get_row($sql);
        
        return $data;
    }
       
    function getInquiryByViewkey( $key )
    {
        $sql = "SELECT cu.*, ul.Code as ProccessBy_RoleCode, um.FirstName as ProccessBy_FirstName, um.LastName as ProccessBy_LastName  
        FROM contact_us cu 
        LEFT JOIN users u ON cu.ProccessBy = u.UserID
        LEFT JOIN user_meta um ON cu.ProccessBy = um.UserID 
        LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID 
        WHERE ViewKey = '".$key."' LIMIT 1";

        $data = $this->db->get_row($sql);
        
        return $data;
    }
    
    function getAllInquiries()
    {
        $sql = "SELECT cu.*, ul.Code as ProccessBy_RoleCode, um.FirstName as ProccessBy_FirstName, um.LastName as ProccessBy_LastName  
        FROM contact_us cu 
        LEFT JOIN users u ON cu.ProccessBy = u.UserID
        LEFT JOIN user_meta um ON cu.ProccessBy = um.UserID 
        LEFT JOIN user_levels ul ON u.Level = ul.UserLevelID";    
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    }
    
    function doSave()
    {
        $dataID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){
                case "updateinquiry": {
                    $data = $this->post;
                    $maildata = $data['mail'];
                    $inqryID = $data['InquiryID'];
                    $oldReply = $data['oldReply'];

                    unset( $data['action'] );

                    if( $maildata['Status'] == "Closed" ){
                        $maildata['ProccessBy'] = $data['ProccessByUserID'];
                    }

                    $dataID = $this->db->update( "contact_us", $maildata, array( 'InquiryID' => $inqryID ) );
                    if( $dataID ){
                        App::activityLog( "Contact Us: set status inquiry <a href=".View::url( 'contactus/edit' )."/".$inqryID.">#".$inqryID."</a> to <b>".$maildata['Status']."</b>.");
                        $this->setSession( 'message', "Your changes successfully submited!" );
                    }
                    $dataID = $oldReply == $maildata['Reply'] ? false : true;

                } break;

                case "contactus": {
                    $data = $this->post;
                    $maildata = $this->post['mail'];

                    unset( $data['action'] );

                    $maildata['Status'] = "Pending";
                    $maildata['ViewKey'] = md5( microtime() );

                    $dataID = $this->db->insert( "contact_us", $maildata );
                    if( $dataID ){
                        App::activityLog( "Contact Us: New inquiry <a href=".View::url( 'contactus/edit' )."/".$dataID.">#".$dataID."</a>." );
                        $this->setSession( 'message', "<h5>Thank you for contacting us.</h5><br>You are very important to us, all information received will always remain confidential. We will contact you as soon as we review your message!" );
                    }
                } break;
            } 
        }
        return $dataID;
    }   
        
    function doDelete($ID)
    {
        $where = array('InquiryID' => $ID);
        $this->setSession('message',"successfully Deleted"); 
        App::activityLog("Contact Us: deleted inquiry <a href='#'>#".$ID."</a>.");       
        $rowCount = $this->db->delete("contact_us", $where);
    }
}