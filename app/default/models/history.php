<?php
class History_model extends Model 
{
    function __construct() {
        parent::__construct();

        View::$segments = $this->segment;
    }
       
    function getHistory($ID)
    {
        $sql = "SELECT * FROM history WHERE id = ".$ID." LIMIT 1";
        $userdata = $this->db->get_row($sql);
        
        return $userdata;
    } 
    
    function getHistoryLogs()
    {
        $sql = "SELECT * FROM history";
       
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    } 
    

 
    
    public function log($user_id, $note, $module = '', $moredata=null)
    {
        $moredata = ($moredata) ? print_r($moredata,true) : '';
        $data = [
            'user_id'=>$user_id,
            'note' => $note,
            'module' =>$module,
            'more_data' =>$moredata,
            'created_at'=>date('Y-m-d H:i:s')
        ];
        $history_id = $this->db->insert("history", $data);

        return $history_id;
    }   
    
     function doDelete($ID)
    {
        $where = array('id' => $ID);        
        $this->setSession('message',"Email Template has been deleted!");        
        $rowCount = $this->db->delete("email_templates", $where);
        
    }
  
    
    public function commonAssets()
    {
        View::$footerscripts[] = "assets/js/jquery.js";
        View::$footerscripts[] = "assets/js/migrate.js";
        View::$footerscripts[] = "assets/js/bootstrap.min.js";
        View::$footerscripts[] = "assets/js/fundz.js";
        View::$footerscripts[] = "assets/js/custom.js";

        View::$styles[] = "assets/css/bootstrap.min.css";
        View::$styles[] = "assets/css/style.css";
        View::$styles[] = "assets/css/custom-style.css";
        View::$styles[] = "assets/css/responsive.css";
        View::$styles[] = "assets/css/font-awesome.min.css";
        View::$styles[] = "assets/css/icons.css";

        // Your Custom Styles 
        View::$styles[] = "assets/css/my-styles.css";
    }

    public function indexAssets()
    {
        // jQuery (necessary for JavaScript plugins)
        View::$footerscripts[] = 'vendor/jquery/dist/jquery.min.js';
        View::$footerscripts[] = "assets/js/migrate.js";

        // Bootstrap Javascript 
        View::$footerscripts[] = "assets/js/bootstrap.min.js";

        // EXTENSION PLUGINS
        View::$footerscripts[] = "assets/js/pieprogress/scripts/rainbow.min.js";
        View::$footerscripts[] = "assets/js/pieprogress/scripts/jquery-asPieProgress.js";
        View::$footerscripts[] = "assets/js/slider-revolution/rs-plugin/js/jquery.themepunch.plugins.min.js";
        View::$footerscripts[] = "assets/js/slider-revolution/rs-plugin/js/jquery.themepunch.revolution.min.js";
        View::$footerscripts[] = "assets/js/bxslider/jquery.bxslider.min.js";
        View::$footerscripts[] = "assets/js/maps.js";
        View::$footerscripts[] = "assets/js/maps/scripts/jquery.gmap.min.js";
        View::$footerscripts[] = "assets/js/jquery.scroll.js";
        View::$footerscripts[] = "assets/js/jquery.hoverizr.min.js";
        

        // Custom Settings
        View::$footerscripts[] = "assets/js/custom.js";

        View::$footerscripts[] = "assets/js/plugin.js";
        View::$footerscripts[] = "assets/js/countdown.js";
        View::$footerscripts[] = "assets/js/retina.min.js";

        // Bootstrap CSS 
        View::$styles[] = "assets/css/bootstrap.min.css";

        // Basic Stylesheets 
        View::$styles[] = "assets/css/style.css";
        View::$styles[] = "assets/css/custom-style.css";
        View::$styles[] = "assets/css/responsive.css";

        // EXTENSION STYLES
        View::$styles[] = "assets/js/pieprogress/css/rainbow.css";
        View::$styles[] = "assets/js/pieprogress/css/progress.css";
        View::$styles[] = "assets/js/slider-revolution/css/style.css";
        View::$styles[] = "assets/js/slider-revolution/rs-plugin/css/settings.css";
        View::$styles[] = "assets/js/bxslider/jquery.bxslider.css";

        // DEFAULT CSS
        View::$styles[] = "assets/css/font-awesome.min.css";
        View::$styles[] = "assets/css/icons.css";

        // Your Custom Styles 
        View::$styles[] = "assets/css/my-styles.css";

    }
}