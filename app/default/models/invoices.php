<?php
class Invoices_model extends Model
{
    function __construct()
    {
        parent::__construct();
        View::$segments = $this->segment;
    }    
    
    function doSave()
    {
        $invoiceID = false;

        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){

                case "addinvoice":
                    $invoice = $this->post['invoice'];
                    $invoice['BalanceDue'] = $invoice['Total'];
                    $invoice['Status'] = 'Outstanding';
                    $invoiceID = $this->db->insert( "invoices", $invoice );
                    
                    //update invoice status for table bookings
                    $bwhere = array( 'InvestmentBookingID' => $this->post['InvestmentBookingID'] );
                    $this->db->update( "bookings", array( 'InvoiceStatus' => 1 ), $bwhere );

                    $this->setSession( 'message', "New invoice has been created!" );
                    App::activityLog( "Added invoice ID#$invoiceID." );
                    View::redirect( 'invoices' );
                    break;

                case "approvewithdrawal":
                    $withdraw = $this->post['withdraw'];
                    $earningID = $withdraw['EarningID'];
                    $withdraw['Status'] = 'Approved';

                    $where = array( 'EarningID' => $earningID );
                    
                    $this->db->update( "referral_earnings", $withdraw, $where );
                    $invoiceID = array( 'Status' => 'Approved', 'EarningID' => $earningID );

                    $this->setSession( 'message', "Withdrawal request has been approved! ID#".$earningID . "." );
                    App::activityLog( "Approved withdrawal request EarningID#" . $earningID . "." );
                    break;

                case "paidwithdrawal":
                    $withdraw = $this->post['withdraw'];
                    $earningID = $withdraw['EarningID'];
                    $withdraw['Status'] = 'Paid';
                    $where = array( 'EarningID' => $earningID );
                    
                    $this->db->update( "referral_earnings", $withdraw, $where );
                    $invoiceID = array( 'Status' => 'Paid', 'EarningID#' => $earningID );

                    $this->setSession( 'message', "Payment for withdrawal request has been processed! ID#".$earningID . "." );
                    App::activityLog( "Processed payment for the approved withdrawal request EarningID#" . $earningID . "." );
                    break;

                case "createinvoice":
                    $invoice = $this->post['invoice'];
                    $invoice['BalanceDue'] = 0;
                    $invoice['Status'] = 'Outstanding';
                    $invoice['InvoiceDate'] = date( 'Y-m-d' );
                    $invoice['DueDate'] = date( 'Y-m-d', strtotime( "+7 days" ) );
                    $invoiceID = $this->db->insert( "invoices", $invoice );
                    // $invoiceDraftID = $this->db->insert( "invoices_draft", $invoice );

                    //items
                    if( $invoiceID ){
                        $total = 0.00;
                        $balance = 0.00;
                        $data = $this->post['group'];
                        $types = $data['Type'];
                        foreach( $types as $item => $val ){
                            $invItem['InvoiceID'] = $invoiceID;                            
                            $invItem['InvestmentBookingID'] = isset( $data['InvestmentBookingID'][$item] ) ? $data['InvestmentBookingID'][$item] : '0';
                            $invItem['Type'] = $val;
                            $invItem['Price'] = $data['Price'][$item];
                            $invItem['Qty'] = $data['Quantity'][$item];
                            $invItem['Description'] = $data['Details'][$item];
                            $total = floatval( $invItem['Qty'] ) * floatval( $invItem['Price'] );
                            $invItem['ItemAmount'] = $total;
                            $balance += $total;

                            $invoiceItemID = $this->db->insert( "invoice_items", $invItem );
                            if( $val == 'Booking Commission' ){ //update invoice status for table bookings
                                $bdata = array( 'InvoiceStatus' => 1, 'UpdatedAt' => date('Y-m-d') );
                                $bwhere = array( 'InvestmentBookingID' => $invItem['InvestmentBookingID'] );
                                $this->db->update( "bookings", $bdata, $bwhere );
                            }
                        }
                        $where = array( 'InvoiceID' => $invoiceID );
                        $update['Total'] = $balance;
                        $update['SubTotal'] = $balance;
                        $update['BalanceDue'] = $balance;
                        $this->db->update( "invoices", $update, $where );
                    }

                    $this->setSession( 'message', "New invoice template has been created!" );
                    App::activityLog( "Added invoice template ID#$invoiceID." );
                    View::redirect( 'invoices' );
                    break;

                default: break;
            }
        }

        return $invoiceID;
    }

    function getInvoiceTypes()
    {
        $sql = "SELECT * FROM invoice_type ORDER BY Description ASC";
        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getAllInvoices()
    {
        $data = array();
        $sql = "SELECT i.*, c.`CompanyName`, fi.`Active`
                FROM `invoices` i 
                LEFT JOIN `client_profiles` c ON c.`ClientProfileID` = i.`ClientID`
                LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getAllPendingsCount()
    {
        $data = [];
        $sql = "SELECT
            (SELECT COUNT(DISTINCT `ClientProfileID`) FROM `client_profiles_request` WHERE `Section` = 'bank') AS Clients,
            (SELECT COUNT(*) FROM `bookings` WHERE `BookingStatus` = 'Approved' AND `InvoiceStatus` <> 1) AS Invoices,
            (SELECT COUNT(*) FROM `referral_earnings` WHERE `Status` = 'Pending') AS Earnings,
            (SELECT COUNT(i.`TTReceipt`) FROM `invoices` i LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt` WHERE fi.`Active` = 0) AS Documents";

        $data = $this->db->get_row( $sql );

        return $data;
    }

    function getUserInvoicesByStatus( $status )
    {
        $where = '';
        if( $status == 'Outstanding' ){
            $where = " WHERE i.`DueDate` >= CURDATE() AND i.`Status` IN ( 'Outstanding', 'TT Receipt' ) ";
        } else {
            $where = " WHERE i.`Status` = '$status' ";
        }
        // $sql = "SELECT DISTINCT i.*, c.`CompanyName`, c.`CompanyPhoto`, fi.`Active`
        //     FROM `invoices` i 
        //     LEFT JOIN `client_profiles` c ON c.`ClientProfileID` = i.`ClientID`
        //     LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        //     $where
        //     ORDER BY i.`CreatedAt` DESC";

        // $sql = "SELECT DISTINCT i.*, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor, cp.`CompanyName`, cp.`CompanyPhoto`, fi.`Active`, ur.`SubscriptionRate`
        //     FROM `invoices` i 
        //     LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
        //     LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        //     LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
        //     LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        //     $where
        //     ORDER BY i.`CreatedAt` DESC";

        $sql = "SELECT DISTINCT i.*, cp.`CompanyName`, cp.`CompanyPhoto`, fi.`Active`, ur.`SubscriptionRate`,
                ( SELECT SUM(`TotalAmountAttached`)
                FROM `bookings`
                WHERE `InvestmentBookingID` IN (SELECT DISTINCT `InvestmentBookingID` FROM `invoice_items` WHERE `InvoiceID` = i.`InvoiceID`) ) AS Invested
            FROM `invoices` i 
            LEFT JOIN `invoice_items` ii ON ii.`InvoiceID` = i.`InvoiceID`
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
            $where
            ORDER BY i.`CreatedAt` DESC";

        $query = &$this->db->prepare( $sql );
        $query->execute();
        $data = [];

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getInvoicesOverdue()
    {
        // $sql = "SELECT DISTINCT i.*, c.`CompanyName`, fi.`Active`
        //         FROM `invoices` i 
        //         LEFT JOIN `client_profiles` c ON c.`ClientProfileID` = i.`ClientID`
        //         LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        //         WHERE i.`DueDate` < CURDATE() AND i.`Status` !='Paid'";

        // $sql = "SELECT DISTINCT i.*, CONCAT( um.`FirstName`, ' ', um.`LastName` ) AS Investor, cp.`CompanyName`, cp.`CompanyPhoto`, fi.`Active`, ur.`SubscriptionRate`
        // FROM `invoices` i 
        // LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
        // LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        // LEFT JOIN `user_meta` um ON um.`UserID` = i.`InvestorUserID`
        // LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        // WHERE i.`DueDate` < CURDATE() AND i.`Status` !='Paid'
        // ORDER BY i.`CreatedAt` DESC";

        $sql = "SELECT DISTINCT i.*, cp.`CompanyName`, cp.`CompanyPhoto`, fi.`Active`, ur.`SubscriptionRate`,
                ( SELECT SUM(`TotalAmountAttached`)
                FROM `bookings`
                WHERE `InvestmentBookingID` IN (SELECT DISTINCT `InvestmentBookingID` FROM `invoice_items` WHERE `InvoiceID` = i.`InvoiceID`) ) AS Invested
        FROM `invoices` i 
        LEFT JOIN `invoice_items` ii ON ii.`InvoiceID` = i.`InvoiceID`
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
        LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
        LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        WHERE i.`DueDate` < CURDATE() AND i.`Status` != 'Paid'
        ORDER BY i.`CreatedAt` DESC";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getDetails( $invoiceID )
    {
        // $sql = "SELECT i.*, cp.`CompanyName`, ur.`SubscriptionRate`, u.`Email`, um1.`FirstName`, um1.`LastName`, um1.`Address`, um1.`Address2`, um1.`City`, um1.`State`, um1.`Country`, um1.`PostalCode`, um1.`Phone`, CONCAT( um2.`FirstName`, ' ', um2.`LastName` ) AS Investor
        // FROM `invoices` i 
        // LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
        // LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
        // LEFT JOIN `user_meta` um1 ON um1.`UserID` = cp.`UserID`
        // LEFT JOIN `user_referrer` ur ON ur.`UserID` = i.`UserID`
        // LEFT JOIN `user_meta` um2 ON um2.`UserID` = i.`InvestorUserID`
        // WHERE i.`InvoiceID` = $invoiceID";

        $sql = "SELECT i.*, cp.`CompanyName`, ur.`SubscriptionRate`, u.`Email`, um1.`FirstName`, um1.`LastName`, um1.`Address`, um1.`Address2`, um1.`City`, um1.`State`, um1.`Country`, um1.`PostalCode`, um1.`Phone`, b.`UserID` AS InvestorID, CONCAT( um2.`FirstName`, ' ', um2.`LastName` ) AS Investor, b.`InvestmentBookingID` AS BookingID, b.`TotalAmountAttached`, ii.`Type`, ii.`Qty`, ii.`ItemAmount`
        FROM `invoices` i
        LEFT JOIN `invoice_items` ii ON ii.`InvoiceID` = i.`InvoiceID`
        LEFT JOIN `bookings` b ON b.`InvestmentBookingID` = ii.`InvestmentBookingID`
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`        
        LEFT JOIN `users` u ON u.`UserID` = cp.`UserID`
        LEFT JOIN `user_meta` um1 ON um1.`UserID` = cp.`UserID`
        LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`        
        LEFT JOIN `user_meta` um2 ON um2.`UserID` = b.`UserID`
        WHERE i.`InvoiceID` = $invoiceID";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get Clients data for creating invoices
     *
     * @access private
     * @param all (boolean) : default [false] : if false, all client with bookings and no created invoices only will be returned
     * @return data array
     **/
    function getClients( $all = false )
    {
        $sql = "SELECT DISTINCT c.`ClientProfileID`, c.`UserID`, um.`FirstName`, um.`LastName`, c.`CompanyName`
                FROM `client_profiles` c
                LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
                LEFT JOIN `users` u ON u.`UserID` = c.`UserID`
                WHERE u.`Level` = 4
                ORDER BY um.`FirstName` ASC, um.`LastName` ASC";

        if( !$all ){
            // version 1
            // $sql = "SELECT DISTINCT c.`ClientProfileID`, c.`UserID`, um.`FirstName`, um.`LastName`, c.`CompanyName`
            //         FROM `client_profiles` c
            //         LEFT JOIN `bookings` b ON (b.`ClientProfileID` = c.`ClientProfileID` AND b.`BookingStatus` = 'Approved')
            //         LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
            //         LEFT JOIN `users` u ON u.`UserID` = c.`UserID`
            //         WHERE u.`Level` = 4 AND b.`InvestmentBookingID` NOT IN (SELECT `InvestmentBookingID` FROM `invoices` WHERE `InvestmentBookingID` IS NOT NULL AND `ClientID` = b.`ClientProfileID`)
            //         ORDER BY um.`FirstName`, um.`LastName`";

            //version 2
            $sql = "SELECT DISTINCT c.`ClientProfileID`, c.`UserID`, um.`FirstName`, um.`LastName`, c.`CompanyName`
                    FROM `client_profiles` c
                    LEFT JOIN `bookings` b ON b.`ClientProfileID` = c.`ClientProfileID` AND b.`BookingStatus` = 'Approved'
                    LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
                    LEFT JOIN `users` u ON u.`UserID` = c.`UserID`
                    WHERE u.`Level` = 4 AND b.`InvoiceStatus` <> 1 AND b.`ExemptedCommission` = 0
                    ORDER BY um.`FirstName`, um.`LastName`";
        }

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getClientDetails( $clientProfileID )
    {
        $sql = "SELECT c.UserID, c.ClientProfileID, c.CompanyName, um.FirstName, um.LastName, um.Address, um.Address2, um.City, um.State, um.Country, um.PostalCode, um.Phone
            FROM client_profiles as c
            LEFT JOIN user_meta as um ON um.UserID = c.UserID 
            WHERE c.ClientProfileID = ".$clientProfileID;

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data = $row;
        }
        unset( $query );

        return $data;
    }

    function getBookingsOwn( $UserID )
    {
        $sql = "SELECT `InvestmentBookingID`, `TotalAmountAttached` as Amount, `BookingStatus`, DATE_FORMAT( `CreatedAt`, \"%b %d, %Y\" ) as BookingDate
            FROM bookings
            WHERE `InvoiceStatus` = 0 AND `ExemptedCommission` = 0 AND `BookingStatus` = 'Approved' AND `ClientProfileID` = ".$UserID;

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = array();
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getBookedData( $ID )
    {
        // $sql = "SELECT InvestmentBookingID, ClientProfileID, TotalAmountAttached FROM bookings WHERE InvestmentBookingID = ".$ID;
        //$data = $this->db->get_row( $sql );

        // $sql = "SELECT b.`InvestmentBookingID`, b.`UserID` AS `InvestorUserID`, CONCAT( um1.`FirstName`, ' ', um1.`LastName` ) AS Investor, b.`TotalAmountAttached`, ur.`SubscriptionRate`,
        //     FORMAT( b.`TotalAmountAttached` * ( ur.`SubscriptionRate` / 100 ), 2 ) AS `TotalInvoiceAmount`, cp.`UserID`, cp.`ClientProfileID`, cp.`CompanyName`, um2.`FirstName`, um2.`LastName`, um2.`Address`, um2.`Address2`, um2.`City`, um2.`State`, um2.`Country`, um2.`PostalCode`, um2.`Phone`
        // FROM `bookings` b
        // LEFT JOIN `user_meta` um1 ON um1.`UserID` = b.`UserID`
        // LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID` 
        // LEFT JOIN `user_meta` um2 ON um2.`UserID` = cp.`UserID`
        // LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        // WHERE b.`InvestmentBookingID` = $ID";

        // $sql = "SELECT b.`InvestmentBookingID`, b.`UserID` AS `InvestorUserID`, CONCAT( um1.`FirstName`, ' ', um1.`LastName` ) AS Investor, b.`TotalAmountAttached`, cp.`UserID`, cp.`ClientProfileID`, cp.`CompanyName`, um2.`FirstName`, um2.`LastName`, um2.`Address`, um2.`Address2`, um2.`City`, um2.`State`, um2.`Country`, um2.`PostalCode`, um2.`Phone`,
        //     (CASE WHEN ur.`SubscriptionRate` = 0 OR ur.`SubscriptionRate` IS NULL THEN 0.5 ELSE ur.`SubscriptionRate` END) AS SubscriptionRate,
        //     FORMAT( b.`TotalAmountAttached` * ( (CASE WHEN ur.`SubscriptionRate` = 0 OR ur.`SubscriptionRate` IS NULL THEN 0.5 ELSE ur.`SubscriptionRate` END) / 100 ), 2 ) AS `TotalInvoiceAmount`
        // FROM `bookings` b
        // LEFT JOIN `user_meta` um1 ON um1.`UserID` = b.`UserID`
        // LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID` 
        // LEFT JOIN `user_meta` um2 ON um2.`UserID` = cp.`UserID`
        // LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        // WHERE b.`InvestmentBookingID` = $ID";

        $sql = "SELECT b.`InvestmentBookingID`, b.`UserID` AS `InvestorUserID`, CONCAT( um1.`FirstName`, ' ', um1.`LastName` ) AS Investor, b.`TotalAmountAttached`, cp.`UserID`, cp.`ClientProfileID`, cp.`CompanyName`, um2.`FirstName`, um2.`LastName`, um2.`Address`, um2.`Address2`, um2.`City`, um2.`State`, um2.`Country`, um2.`PostalCode`, um2.`Phone`,
            (CASE WHEN ur.`SubscriptionRate` = 0 OR ur.`SubscriptionRate` IS NULL THEN 0.5 ELSE ur.`SubscriptionRate` END) AS SubscriptionRate,
            FORMAT( GetCommissionPerBooking( b.`ClientProfileID`, b.`TotalAmountAttached`, b.`ExemptedCommission` ), 2 ) AS `TotalInvoiceAmount`
        FROM `bookings` b
        LEFT JOIN `user_meta` um1 ON um1.`UserID` = b.`UserID`
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID` 
        LEFT JOIN `user_meta` um2 ON um2.`UserID` = cp.`UserID`
        LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
        WHERE b.`InvestmentBookingID` = $ID";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data = $row;
        }
        unset( $query );

        return $data;
    }

    function getPendingDocuments()
    {
        $data = [];
        $sql = "SELECT * FROM (
            SELECT i.`InvoiceID` AS TableID, cp.`CompanyName` AS 'Name', cp.`UserID` AS ReferenceID, 'Invoices' AS GroupName, 'TT Receipt' AS DocumentName, fi.`FileID`, fi.`FileSlug`, fi.`FileName`, i.`UpdatedAt` AS DateAdded
            FROM `invoices` i
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = i.`ClientID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = i.`TTReceipt`
            WHERE fi.`Active` = 0
        ) AS docs
        ORDER BY DateAdded";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    /**
     * Get withdrawals data
     *
     * @access private
     * @param (string) $status : (optional) : withdrawal status
     * @param (int) $earningID : (optional) : earning id
     * @return data array
     **/
    function getAllWithdrawals( $status = false, $earningID = false )
    {
        $data = array();

        $WHERE = "";
        if( $status ){
            $WHERE = " WHERE re.`Status` = '$status' ";
            $WHERE .= $earningID ? " AND re.`EarningID` = $earningID " : "";
        } else {
            $WHERE = $earningID ? " WHERE re.`EarningID` = $earningID " : "";
        }

        $sql =  "SELECT re.*,
            b.`TotalAmountAttached`,
            cli_cp.`ClientProfileID`,
            cli_cp.`CompanyName`,
            CONCAT(inv_um.`FirstName`, ' ', inv_um.`LastName`) InvestorName,
            CONCAT(ref_um.`FirstName`, ' ', ref_um.`LastName`) ReferrerName,
            inv_u.`ReferrerUserID`
        FROM `referral_earnings` re
        LEFT JOIN `users` inv_u ON inv_u.`UserID` = re.`InvestorUserID`
        LEFT JOIN `user_meta` inv_um ON inv_um.`UserID` = inv_u.`UserID`
        LEFT JOIN `user_meta` ref_um ON ref_um.`UserID` = inv_u.`ReferrerUserID`
        LEFT JOIN `bookings` b ON b.`InvestmentBookingID` = re.`InvestmentBookingID`
        LEFT JOIN `client_profiles` cli_cp ON cli_cp.`ClientProfileID` = b.`ClientProfileID`
        $WHERE
        ORDER BY re.`CreatedAt` DESC";
        
        $data = [];
        if( $earningID ){
            $data = $this->db->get_row( $sql );
        } else {
            $query = &$this->db->prepare($sql);
            $query->execute();
            while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
                $data[$row->Status][] = $row;
            }
            unset( $query );
        }

        return $data;
    }
}