<?php
class Bookings_model extends Model 
{
    function __construct() {
        parent::__construct();        
        View::$segments = $this->segment;
    }

    function getClientData( $ID )
    {
        $userinfo = User::info();
        $select = $userinfo->UserID ? ", (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID=cp.ClientProfileID AND BookingType='Booking' AND UserID='".$userinfo->UserID."') AS CurrentUserBookedCount" : "";
        $select .= $userinfo->UserID ? ", (SELECT COUNT(UserID) FROM bookings WHERE ClientProfileID=cp.ClientProfileID AND BookingType='Interest' AND UserID='".$userinfo->UserID."') AS CurrentUserRegisterCount " : "";

        $sql = "SELECT cp.*, u.*, 
        (SELECT SUM(TotalAmountAttached) FROM bookings WHERE ClientProfileID = cp.ClientProfileID AND BookingType = 'Booking') as TotalBookingAmounts $select
        FROM client_profiles cp
        LEFT JOIN users u ON u.UserID = cp.UserID 
        WHERE cp.ClientProfileID = ".$ID;

        $data = $this->db->get_row($sql);

        return $data;
    }
       
    function getBookedData($ID)
    {
        $sql = "SELECT b.`UserID` AS BookingUserID, b.*, timestampdiff(DAY, NOW(), b.`BookingExpiryDate`) as DaysLeft, cp.*, 
        (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingStatus = 'Approved') as TotalRaisedAmt,
        (SELECT COUNT(bb.UserID) FROM bookings bb WHERE bb.ClientProfileID = cp.ClientProfileID AND bb.BookingStatus = 'Approved') as TotalInvestor, 
            ba.Name as BankName,ba.Address as BankAddress,ba.AccountNumber as BankAccountNumber,ba.AccountName as BankAccountName,ba.SwiftCode
            FROM bookings b 
            LEFT JOIN client_profiles cp ON b.ClientProfileID = cp.ClientProfileID 
            LEFT JOIN bank_accounts ba ON ba.UserID = cp.UserID 
            LEFT JOIN users u ON u.UserID = b.UserID
        WHERE b.InvestmentBookingID = ".$ID;

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getBookings()
    {
        $sql = "SELECT b.*, timestampdiff(DAY, NOW(), b.`BookingExpiryDate`) as DaysLeft, um.FirstName, um.LastName, cp.* ,
        ba.Name as BankName,ba.Address as BankAddress,ba.AccountNumber as BankAccountNumber,ba.AccountName as BankAccountName,ba.SwiftCode  
            FROM bookings b 
            JOIN client_profiles cp ON cp.ClientProfileID=b.ClientProfileID 
            LEFT JOIN bank_accounts ba ON ba.UserID = cp.UserID 
            LEFT JOIN user_meta um ON um.UserID=b.UserID";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getBookingsOwn($UserID)
    {
        $sql = "SELECT b.*, timestampdiff(DAY, NOW(), b.`BookingExpiryDate`) as DaysLeft, um.FirstName, um.LastName, cp.* 
            FROM bookings b 
            JOIN client_profiles cp ON cp.ClientProfileID=b.ClientProfileID 
            LEFT JOIN user_meta um ON um.UserID=b.UserID
            WHERE b.UserID=$UserID";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getBookingsOwnByProject( $ClientProfileID, $UserID )
    {
        $sql = "SELECT b.*, cp.`CompanyName`
        FROM `bookings` b
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        WHERE b.`BookingStatus` = 'Approved' AND b.`ClientProfileID` = $ClientProfileID AND b.`UserID` = $UserID
        ORDER BY b.`CreatedAt` DESC";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getTotalsBookingsOwn($UserID)
    {
        $sql = "SELECT 
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.BookingStatus='Approved' AND bb.UserID=$UserID) as TotalAmountInvestment, 
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.UserID=$UserID AND bb.BookingType='Interest') as TotalAmountRegistered, 
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Approved' AND bb.UserID=$UserID) as TotalApproved,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.UserID=$UserID AND bb.BookingType='Booking') as TotalPendings,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Verified' AND bb.UserID=$UserID) as TotalVerified,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.UserID=$UserID AND bb.BookingType='Interest') as TotalRegistered,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus IN('Verified','Pending') AND bb.BookingType='Booking' AND bb.UserID=$UserID) as TotalNotApprove  
        FROM bookings b 
        WHERE b.UserID=$UserID";

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getTotalsBookings()
    {
        $sql = "SELECT 
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.BookingStatus='Approved') as TotalAmountInvestment,
            (SELECT SUM(bb.TotalAmountAttached) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.BookingType='Interest') as TotalAmountInterested,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Approved') as TotalApproved,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending') as TotalPendings,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Verified') as TotalVerified,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus='Pending' AND bb.BookingType='Interest') as TotalRegistered,
            (SELECT count(bb.BookingStatus) FROM bookings bb WHERE bb.BookingStatus IN('Verified', 'Pending') AND bb.BookingType='Booking') as TotalNotApprove";

        $data = $this->db->get_row($sql);
        
        return $data;
    }

    function getBookedCompanies( $ID = false )
    {
        $userID = $ID ? $ID : User::info( 'UserID' );
        $sql = "SELECT * 
                FROM bookings b
                JOIN client_profiles cp ON cp.ClientProfileID=b.ClientProfileID
                WHERE b.UserID = $userID AND b.BookingStatus = 'Approved'
                GROUP BY cp.CompanyName";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function doSaveBooking()
    {
        $bookID = false;
        if( isset( $this->post['action'] ) ){
            $data = $this->post;
            $book = $this->post['book'];
            $book['UserID'] = User::info('UserID');

            if( $book['SharesToApply'] == 0 ){
                $this->setSession( 'error', "ERROR: Shares should not be 0!");
            } elseif( $book['TotalAmountAttached'] == 0 ){
                $this->setSession( 'error', "ERROR: Total application amount should not be 0!" );
            } elseif( $data['minimumbid'] > $book['TotalAmountAttached'] ){
                $this->setSession( 'error', "ERROR: The minimum bid is ".number_format( $data['minimumbid'] )."!" );
            } else {
                $currentdate = date('Y-m-d H:i:s');
                $book['BookingExpiryDate'] = date('Y-m-d', strtotime( $currentdate. ' +7 days' ) );
                $book['BookingType'] = 'Booking';
                $bookID = $this->db->insert( "bookings", $book );
                if( $bookID ){
                    $this->setSession( 'message', "Booking has been successfully submitted!" );
                    App::activityLog( "New Booking #BK-533D0M364-".$bookID."." );
                }
            }
        }

        return $bookID;
    }

    function doSaveInterest()
    {
        $bookID = false;
        if( isset( $this->post['action'] ) ){
            $data = $this->post;
            $book = $this->post['book'];
            $book['UserID'] = User::info('UserID');

            if( $book['SharesToApply'] == 0 ){
                $this->setSession( 'error', "ERROR: Shares should not be 0!");
            } elseif( $book['TotalAmountAttached'] == 0 ){
                $this->setSession( 'error', "ERROR: Total application amount should not be 0!" );
            } elseif( $data['minimumbid'] > $book['TotalAmountAttached'] ){
                $this->setSession( 'error', "ERROR: The minimum bid is ".number_format( $data['minimumbid'] )."!" );
            } else {
                $currentdate = date('Y-m-d H:i:s');
                $book['BookingExpiryDate'] = date('Y-m-d', strtotime( $currentdate. ' +7 days') );
                $book['BookingType'] = 'Interest';
                $bookID = $this->db->insert( "bookings", $book );
                if( $bookID ){
                    $this->setSession( 'message', "Your interest has been successfully submitted!" );
                    App::activityLog( "New Interest Booking #BK-533D0M364-".$bookID."." );
                }
            }
        }
        return $bookID;
    } 
    
    function doSave()
    {
        $bookID = false;
        if( isset( $this->post['action'] ) ){
            switch( $this->post['action'] ){
               case "updatebooking": {
                    $data = $this->post;
                    $bookdata = $this->post['book'];
                    $uwhere = array('InvestmentBookingID' => $this->post['bookid']);
                    $usrid = $this->post['usrid'];
                    unset($data['action']);
                    
                    $bookdata['ChequeAmount'] = str_ireplace( ',', '', $bookdata['ChequeAmount'] );
                    $bookdata['UpdatedAt'] = date('Y-m-d H:i:s');
                    $bookeddata = $bookdata;
                    $filedata = $this->fileUpload($this->file,$usrid);
                    if(count($filedata)) {
                        $bookeddata = array_merge($bookeddata,$filedata);
                    }

                    $dataID = $this->db->update("bookings", $bookeddata, $uwhere);

                    if( $dataID ){
                        $this->setSession('message',"Booking has been updated!");
                        App::activityLog("Updated Booking <a href='".View::url('users/booked')."/".$this->post['bookid']."'>#BC-ID-".$this->post['bookid']."</a>.");
                    }
                } break;

                case "book": {
                    $data = $this->post;
                    $book = $this->post['book'];
                    $book['UserID'] = User::info('UserID');

                    if( $book['SharesToApply'] == 0 ){
                        $this->setSession( 'error', "ERROR: Shares should not be 0!");
                    } elseif( $book['TotalAmountAttached'] == 0 ){
                        $this->setSession( 'error', "ERROR: Total application amount should not be 0!" );
                    } elseif( $data['minimumbid'] > $book['TotalAmountAttached'] ){
                        $this->setSession( 'error', "ERROR: The minimum bid is ".number_format( $data['minimumbid'] )."!" );
                    } else {                        
                        $currentdate = date('Y-m-d H:i:s');
                        $book['BookingExpiryDate'] = date('Y-m-d', strtotime($currentdate. ' + 7 days'));
                        $book['BookingType'] = 'Booking';
                        
                        $bookID = $this->db->insert( "bookings", $book );
                        if( $bookID ){
                            $this->setSession( 'message', "Booking has been successfully submitted!" );
                            App::activityLog( "New Booking #BK-533D0M364-".$bookID."." );
                        }                        
                    }
                } break;

                case "register": {
                    $data = $this->post;
                    $book = $this->post['book'];
                    $book['UserID'] = User::info('UserID');

                    if( $book['SharesToApply'] == 0 ){
                        $this->setSession( 'error', "ERROR: Shares should not be 0!");
                    } elseif( $book['TotalAmountAttached'] == 0 ){
                        $this->setSession( 'error', "ERROR: Total application amount should not be 0!" );
                    } elseif( $data['minimumbid'] > $book['TotalAmountAttached'] ){
                        $this->setSession( 'error', "ERROR: The minimum bid is ".number_format( $data['minimumbid'] )."!" );
                    } else {                        
                        $currentdate = date('Y-m-d H:i:s');
                        $book['BookingExpiryDate'] = date('Y-m-d', strtotime($currentdate. ' + 7 days'));
                        $book['BookingType'] = 'Interest';
                        
                        $bookID = $this->db->insert( "bookings", $book );
                        if( $bookID ){
                            $this->setSession( 'message', "Your interest has been successfully submitted!" );
                            App::activityLog( "New Interest Booking #BK-533D0M364-".$bookID."." );
                        }                        
                    }
                } break;
            } 
        }
        return $bookID;
    }   
        
    function doDelete($ID)
    {
        $where = array('InquiryID' => $ID);
        $this->setSession('message',"successfully Deleted"); 
        App::activityLog("Contact Us: deleted inquiry <a href='#'>#".$ID."</a>.");       
        $rowCount = $this->db->delete("contact_us", $where);
    }

    function doRejectFile($ID)
    {
        $where = array('FileItemID' => $ID);
        $rowCount = $this->db->update("file_items",array('Active' => 2), $where); 
        
        App::activityLog("Rejected TT Receipt file <a href='".View::url('bookings/view')."/".$ID."'>#BC-ID-".$ID."</a>.");
    }
    
    function doApproveFile($ID, $bookID = false)
    {
        if($bookID){
            $verified = $this->db->update("bookings",array('BookingStatus' => 'Verified'), array('InvestmentBookingID' => $bookID)); 
        }
        $where = array('FileItemID' => $ID);
        $rowCount = $this->db->update("file_items",array('Active' => 1), $where); 
        App::activityLog("Approved TT Receipt file <a href='".View::url('bookings/view')."/".$ID."'>#BC-ID-".$ID."</a>.");
    }
    
    function doDeleteFile($ID, $bookID = false)
    {
        if($bookID){
            $setZero = $this->db->update("bookings",array('TTReceiptPhoto' => 0, 'BookingStatus' => 'Pending'), array('InvestmentBookingID' => $bookID)); 
        }

        // delete first the associated data in `file_groups` table, be sure that the passed $ID is referring to `FileItemID`
        AppUtility::deleteFileGroupsData( $ID );

        $where = array('FileItemID' => $ID);        
        $rowCount = $this->db->delete("file_items", $where);

        App::activityLog("Deleted TT Receipt file <a href='".View::url('bookings/view')."/".$ID."'>#BC-ID-".$ID."</a>.");
    }
}