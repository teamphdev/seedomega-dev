<?php
class Cs_model extends Model
{
    var $levelID = 5;
    
    function __construct()
    {
        parent::__construct();
        View::$segments = $this->segment;
    }

    function getAllBookings()
    {
        $sql = "SELECT b.*, um.`FirstName`, um.`LastName`, cp.`CompanyName`
        FROM `bookings` b
        LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = b.`ClientProfileID`
        LEFT JOIN `user_meta` um ON um.`UserID` = b.`UserID`";

        $query = &$this->db->prepare($sql);
        $query->execute();

        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            if($row->BookingStatus == 'Pending' && $row->TTReceiptPhoto != 0 || $row->BookingStatus == 'Pending' && $row->ReceiptOption == 'Cheque' && $row->ChequeDrawer != '' && $row->ChequeBank != '' && $row->ChequeBranch != '' && $row->ChequeAmount != ''){
                $data[$row->BookingStatus]['Pending'][] = $row;
            }elseif($row->BookingStatus == 'Pending' && $row->TTReceiptPhoto == 0){
                $data[$row->BookingStatus]['Incomplete'][] = $row;
            }else{
                $data[$row->BookingStatus][] = $row;  
            }            
        }
        unset($query);

        return $data;
    }

    function getAllInvestors()
    {
        $sql = "SELECT a.*, um.`FirstName`, um.`LastName`, u.`DateAdded` AS JoinDate
            FROM `accounts` a
            LEFT JOIN `users` u ON u.`UserID` = a.`UserID`
            LEFT JOIN `user_meta` um ON um.`UserID` = a.`UserID`
            WHERE u.`UserID` IS NOT NULL";

        $query = &$this->db->prepare($sql);
        $query->execute();

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[$row->Status][] = $row;
        }
        unset($query);

        return $data;
    }

    function getSeedersRequest()
    {
        $sql = "SELECT DISTINCT a.*, ar.*, um.`FirstName`, um.`LastName`, ar.`CreatedAt` AS RequestDate
            FROM `accounts_request` ar
            LEFT JOIN `accounts` a ON a.`UserID` = ar.`UserID`
            LEFT JOIN `user_meta` um ON um.`UserID` = ar.`UserID`
            WHERE a.`UserID` IS NOT NULL
            ORDER BY ar.`CreatedAt`";

        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = [];

        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getAllClients()
    {
        $sql = "SELECT cp.*, um.`FirstName`, um.`LastName` 
            FROM `client_profiles` cp            
            LEFT JOIN `user_meta` um ON um.`UserID` = cp.`UserID`
            WHERE cp.`UserID` IS NOT NULL";

        $sql = "SELECT cp.*, um.`FirstName`, um.`LastName`, SUM( b.`TotalAmountAttached` ) AS TotalRaisedAmt, u.`DateAdded`
        FROM `client_profiles` cp
        LEFT JOIN `bookings` b ON b.`ClientProfileID` = cp.`ClientProfileID`
        LEFT JOIN `users` u ON u.`UserID` = cp.`UserID` 
        LEFT JOIN `user_meta` um ON um.`UserID` = cp.`UserID`
        WHERE cp.`UserID` IS NOT NULL
        GROUP BY cp.`ClientProfileID`";            

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getPendingClients( $request = false )
    {
        $where = $request ? " WHERE cp.`ChangeRequest` IS NOT NULL AND LENGTH( `ChangeRequest` ) > 0" : " WHERE cp.`Status` = 'Pending'";
        $sql = "SELECT cp.*, um.`FirstName`, um.`LastName`
            FROM `client_profiles` cp
            LEFT JOIN `user_meta` um ON um.`UserID` = cp.`UserID`
            $where";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getPendingDocuments()
    {
        $data = [];
        $sql = "SELECT * FROM (
            SELECT fg.`FileGroupID` AS TableID, cp.`CompanyName` AS 'Name', cp.`UserID` AS ReferenceID, fg.`GroupName`, fg.`DocumentName`, fi.`FileID`, fi.`FileSlug`, fi.`FileName`, f.`DateAdded`
            FROM `file_groups` fg
            LEFT JOIN `client_profiles` cp ON cp.`ClientProfileID` = fg.`ReferenceID`
            LEFT JOIN `files` f ON f.`FileID` = fg.`FileID`
            LEFT JOIN `file_items` fi ON fi.`FileID` = fg.`FileID` WHERE fi.`Active` = 0
            UNION SELECT b.`InvestmentBookingID` AS TableID, '' AS 'Name', b.`UserID` AS ReferenceID, 'Bookings' AS GroupName, 'TT Receipt' AS DocumentName, fi.`FileID`, fi.`FileSlug`, fi.`FileName`, b.`CreatedAt` AS DateAdded
            FROM `bookings` b
            LEFT JOIN `file_items` fi ON fi.`FileID` = b.`TTReceiptPhoto` AND b.`ReceiptOption` = 'TTPhoto'
            WHERE fi.`Active` = 0    
            UNION SELECT a.`AccountID` AS TableID, '' AS 'Name', a.`UserID` AS ReferenceID, 'Accounts' AS GroupName, 'Proof of Address' AS DocumentName, fi.`FileID`, fi.`FileSlug`, fi.`FileName`, a.`UpdatedAt` AS DateAdded
            FROM `accounts` a
            LEFT JOIN `file_items` fi ON fi.`FileID` = a.`AddressPhoto`
            WHERE fi.`Active` = 0    
            UNION SELECT a.`AccountID` AS TableID, '' AS 'Name', a.`UserID` AS ReferenceID, 'Accounts' AS GroupName, 'Government ID' AS DocumentName, fi.`FileID`, fi.`FileSlug`, fi.`FileName`, a.`UpdatedAt` AS DateAdded
            FROM `accounts` a
            LEFT JOIN `file_items` fi ON fi.`FileID` = a.`IdPhoto`
            WHERE fi.`Active` = 0
        ) AS docs
        ORDER BY DateAdded";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getClientChangeRequests( $section )
    {
        $sql = "SELECT DISTINCT cp.*, cpr.`FieldValue` AS RequestDate
        FROM `client_profiles` cp
        LEFT JOIN `client_profiles_request` cpr ON cpr.`ClientProfileID` = cp.`ClientProfileID` AND cpr.`FieldName` = 'RequestDate'
        WHERE ( SELECT COUNT(*) FROM `client_profiles_request` WHERE `ClientProfileID` = cp.`ClientProfileID` AND ( `Section` = '$section' OR `FieldName` = 'RequestDate' ) ) > 1
        ORDER BY cpr.`FieldValue`";

        $query = &$this->db->prepare( $sql );
        $query->execute();

        $data = [];
        while( $row = $query->fetch( PDO::FETCH_CLASS ) ){
            $data[] = $row;
        }
        unset( $query );

        return $data;
    }

    function getClientProfileData($ID)
    {
        $clientData = array();

        $sql = "SELECT cp.*, ur.`CommissionRate` AS CommRate, ur.`SubscriptionRate` AS SubsRate,
                (SELECT SUM(bb.`TotalAmountAttached`) FROM `bookings` bb WHERE bb.`ClientProfileID` = cp.`ClientProfileID` AND bb.`BookingStatus` = 'Approved') as TotalRaisedAmt,
                (SELECT COUNT(bb.`UserID`) FROM bookings bb WHERE bb.`ClientProfileID` = cp.`ClientProfileID` AND bb.`BookingStatus` = 'Approved') as TotalInvestor
            FROM `client_profiles` cp
            LEFT JOIN `user_referrer` ur ON ur.`UserID` = cp.`UserID`
            WHERE cp.`ClientProfileID` = ".$ID;

        $clientData = $this->db->get_row($sql);
        return $clientData;
    }

    function getInverstorProfileData($ID)
    {
        $investorData = array();

        $sql = "SELECT u.`ReferrerUserID`,u.`Email`,u.`Level`,u.`Capability`,u.`Active`,um.*,a.*,            
            (SELECT Active FROM `file_items` WHERE `UserID`=a.`UserID` AND `FileID`=a.`IdPhoto`) as IDPhotoStatus,
            (SELECT Active FROM `file_items` WHERE `UserID`=a.`UserID` AND `FileID`=a.`AddressPhoto`) as AddressPhotoStatus
            FROM `accounts` a
            LEFT JOIN `users` u ON u.`UserID`=a.`UserID`
            LEFT JOIN `user_meta` um ON um.`UserID`=a.`UserID`
            WHERE u.`UserID`=$ID LIMIT 1";

        $investorData = $this->db->get_row($sql);
        return $investorData;
    }

    function getAllPendingsCount()
    {
        $data = [];
        $sql = "SELECT
            (SELECT COUNT(*) FROM `client_profiles` WHERE `Status` = 'Pending') + (SELECT COUNT(DISTINCT `ClientProfileID`) FROM `client_profiles_request` WHERE `Section` = 'client') AS Clients,
            (SELECT COUNT(*) FROM `accounts` WHERE `Status` = 'Verification') + (SELECT COUNT(DISTINCT `UserID`) FROM `accounts_request`) AS Seeders,
            (SELECT COUNT(*) FROM `bookings` WHERE `BookingType` = 'Booking' AND `BookingStatus` = 'Pending' AND ((`ReceiptOption`='TTPhoto' AND `TTReceiptPhoto`>0) OR (`ReceiptOption`='Cheque' AND `ChequeAmount`>0 AND LENGTH(`ChequeDrawer`)>0 AND LENGTH(`ChequeBank`)>0 AND LENGTH(`ChequeBranch`)>0))) AS Bookings,
            (SELECT COUNT(fg.`FileID`) FROM `file_groups` fg LEFT JOIN `file_items` fi ON fi.`FileID` = fg.`FileID` WHERE fi.`Active` = 0) + (SELECT COUNT(b.`TTReceiptPhoto`) FROM `bookings` b LEFT JOIN `file_items` fi ON fi.`FileID` = b.`TTReceiptPhoto` AND b.`ReceiptOption` = 'TTPhoto' WHERE fi.`Active` = 0) + (SELECT COUNT(a.`AddressPhoto`) FROM `accounts` a LEFT JOIN `file_items` fi ON fi.`FileID` = a.`AddressPhoto` WHERE fi.`Active` = 0) + (SELECT COUNT(a.`IdPhoto`) FROM `accounts` a LEFT JOIN `file_items` fi ON fi.`FileID` = a.`IdPhoto` WHERE fi.`Active` = 0) AS Documents";

        $data = $this->db->get_row( $sql );

        return $data;
    }

    function getComments( $clientProfileID )
    {
        //$sql = "SELECT * FROM `comments` WHERE `ClientProfileID` = $clientProfileID ORDER BY `CommentDate`";
        $sql = "SELECT DISTINCT c.*,um.`FirstName`,um.`LastName`, fi.`FileSlug` FROM `comments` c
                LEFT JOIN `user_meta` um ON um.`UserID` = c.`UserID`
                LEFT JOIN `file_items` fi ON fi.`FileID` = um.`Avatar`
                WHERE c.`ClientProfileID` = $clientProfileID ORDER BY c.`CommentID`";

        $query = $this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;
        }
        unset($query);

        return $data;
    }

    function getFileID($FileItemID)
    {
        $data = array();

        $sql = "SELECT FileID FROM file_items WHERE FileItemID = ".$FileItemID;

        $data = $this->db->get_row($sql);
        return $data->FileID;
    }

    function updateInvestorAsApprove($ID, $investordata)
    {
        $where = array('UserID'=>$ID);
        $data = array('Status'=>'Approved');
        $this->db->update("accounts", $data, $where);

        $this->setSession('message',"Investor Profile of ".$investordata->FirstName.' '.$investordata->LastName." is now Approved!");
        return true;
    }

    function updateClientAsApprove( $currentUID, $clientdata )
    {
        $status = 'Approved';
        $data = array( 'Status' => $status );
        $where = array( 'ClientProfileID' => $clientdata->ClientProfileID );
        $this->db->update( 'client_profiles', $data, $where );

        $this->setSession( 'message', 'Client Profile of '.$clientdata->CompanyName.' is now Approved!' );
        App::activityLog( $currentUID.': Approved the Client Profile of '.$clientdata->CompanyName.'.' );

        return $status;
    }

    function updateInvoiceAsPaid( $fid, $invoiceID, $status = 'Paid' )
    {
        $fileID = self::getFileID( $fid );

        $where = array( 'TTReceipt' => $fileID );
        $update['Status'] = $status;
        $update['UpdatedAt'] = date( 'Y-m-d' );
        $this->db->update( "invoices", $update, $where );

        App::activityLog( "Updated invoice as $status - InvoiceID#$invoiceID." );
    }

    function doSave( $currentUID, $clientdata )
    {
        if( isset( $this->post['action'] ) ){
            $data = $this->post;
            $action = $data['action'];

            unset( $data['action'] );
            unset( $data['ClientProfileID'] );

            $where = array( 'ClientProfileID' => $clientdata->ClientProfileID );

            $this->db->update( "client_profiles", $data, $where );

            $data['UpdatedAt'] = date('Y-m-d H:i:s');
            $rwhere = array( 'UserID' => $clientdata->UserID );
            if( AppUtility::hasCommissionRate( $clientdata->UserID ) ){
                $this->db->update( "user_referrer", $data, $rwhere );
            } else {
                $data['UserID'] =  $clientdata->UserID;
                $this->db->insert( "user_referrer", $data );
            }

            $this->setSession( 'message', "Commission/Subscription Rate has been updated!" );
            App::activityLog( $currentUID.': Updated the Commission/Subscription Rate #ID-'.$clientdata->ClientProfileID.'.' );

            if( $action == 'approveclient' ){
                $this->updateClientAsApprove( $currentUID, $clientdata );

                //$data['Status'] = 'Active';
                //$data['UpdatedAt'] = date('Y-m-d H:i:s');
                //$this->db->update( "user_referrer", $data, $rwhere );
            }

            if( $action == 'sendcommentview' ){
                $commentdata = $this->post;

                unset($commentdata['action']);

                $commentdata['CommentDate'] = date('Y-m-d H:i:s');
                $commentID = $this->db->insert("comments", $commentdata);

                $this->setSession('message',"New comment has been added!");
            }
        }

        return true;
    }

    function doRejectFile( $ID, $userID )
    {
        $where = array( 'FileItemID' => $ID );
        $rowCount = $this->db->update( "file_items", array( 'Active' => 2 ), $where ); 
        
        App::activityLog( "Rejected user file <a href='".View::url( 'users/profile/documents/'.$userID )."'>ID#".$userID."</a>.");
    }
    
    function doApproveFile( $ID, $userID )
    {
        $where = array( 'FileItemID' => $ID );
        $rowCount = $this->db->update( "file_items", array( 'Active' => 1 ), $where ); 
        App::activityLog( "Approved user file <a href='".View::url( 'users/profile/documents/'.$userID )."'>ID#".$userID."</a>.");
    }
    
    function doDeleteFile($ID, $userID)
    {
        // delete first the associated data in `file_groups` table, be sure that the passed $ID is referring to `FileItemID` of `file_items` table
        AppUtility::deleteFileGroupsData( $ID );

        $where = array('FileItemID' => $ID);
        $rowCount = $this->db->delete("file_items", $where);
        
        App::activityLog("Deleted Investor file <a href='".View::url('users/profile')."/".$userID."'>#ID-".$userID."</a>.");
    }

    function doRedirect( $ID )
    {
        $usercode = User::info();
        switch( $usercode->Code ){
            case 'CSR':
                View::redirect('cs/investorprofiles/view/'.$ID);
                break;
            case 'ACCT':
                View::redirect('invoices/receipt');
                break;
            default:
                View::redirect('users/profile/documents/'.$ID.'/');
                break;
        }
    }
}