<?php
class Packages_model extends Model
{
    function __construct() {
        parent::__construct();

        View::$segments = $this->segment;
    }
    
    function getPackage($ID)
    {
        $sql = "SELECT * FROM subscription_packages WHERE PackageID = ".$ID." LIMIT 1";
        $package = $this->db->get_row($sql);
        
        return $package;
    } 
    
    function getPackages()
    {
        $data = array();
        $sql = "SELECT * FROM subscription_packages";
        $query = &$this->db->prepare($sql);
        $query->execute();
        
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;			
        }
        unset($query);
        
        return $data;
    } 
    

    function doSave()
    {
        if($this->post) { 
            if(isset($this->post['action'])) {
                switch($this->post['action']) {
                    case "updatepackage": {
                        $data = $this->post;                    

                        $where = array('PackageID' => $this->post['packageid']);

                        unset($data['action']);
                        unset($data['packageid']);
                        
                        $rowCount = $this->db->update("subscription_packages", $data, $where);
                        
                        $this->setSession('message',"Package data has been saved.");
                        
                        App::activityLog("Updated Package #".$this->post['packageid'].'.');

                    } break;
                    case "addpackage": {
                        $data = $this->post;                    
                        
                        unset($data['action']);
                        
                        $ID = $this->db->insert("subscription_packages", $data);
                        
                        if($ID) {
                            
                            $this->setSession('message',"New Package has been added!");
                        }
                        
                        App::activityLog("Added Package #".$ID.'.');
                    } break;
                } 
            }
            return (object) $this->post;
        } else {
            
        }
    } 

    
    function doDelete($ID)
    {
        $where = array('PackageID' => $ID);
        $rowCount = $this->db->delete("subscription_packages", $where);
        
        App::activityLog("Deleted Package #".$ID.'.');
    }
}