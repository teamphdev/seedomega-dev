<?php
class PdfEngine extends Model
{
    function generate($arg=array())
    {
        App::vendor('pdfcrowd');
        
        $opt = array(
            'filename' => isset($arg['filename']) ? $arg['filename'] : date('Y_m_d_H_i_s').".pdf",
            'html' => isset($arg['html']) ? $arg['html'] : false,
            'header' => isset($arg['header']) ? $arg['header'] : false,
            'footer' => isset($arg['footer']) ? $arg['footer'] : false,
            'width' => isset($arg['width']) ? $arg['width'] : "8.268in",
            'height' => isset($arg['height']) ? $arg['height'] : "11.693in",
            'top' => isset($arg['top']) ? $arg['top'] : "20pt",
            'right' => isset($arg['right']) ? $arg['right'] : "20pt",
            'bottom' => isset($arg['bottom']) ? $arg['bottom'] : "20pt",
            'left' => isset($arg['left']) ? $arg['left'] : "20pt",
        );
        
        $pdfId = (Option::get('pdf_id')) ? Option::get('pdf_id') : Config::get('PDFCROWDID'); 
        $pdfKey = (Option::get('pdf_key')) ? Option::get('pdf_key') : Config::get('PDFCROWDKEY');
        
        $filename = $opt['filename'];
        
        $root_path = APPPATH."views".DS."assets".DS.'pdf';
        
        $year = date('Y');
                
        if (!file_exists($root_path.DS.$year)) {
            mkdir($root_path.DS.$year, 0777, true);            
            chmod($root_path.DS.$year, 0777);
        }

        $month = date('m');

        if (!file_exists($root_path.DS.$year.DS.$month)) {
            mkdir($root_path.DS.$year.DS.$month.DS, 0777, true);  
            chmod($root_path.DS.$year.DS.$month.DS, 0777); 
        }

        $day = date('d');

        if (!file_exists($root_path.DS.$year.DS.$month.DS.$day)) {
            mkdir($root_path.DS.$year.DS.$month.DS.$day.DS, 0777, true);   
            chmod($root_path.DS.$year.DS.$month.DS.$day.DS, 0777); 
        }

        $subfolder = DS.$year.DS.$month.DS.$day.DS; 
        
        $path = $root_path.$subfolder;
        
        try
        {   
            // create an API client instance
            $pdfc = new Pdfcrowd($pdfId,$pdfKey);

            $pdfc->setPageWidth($opt['width']);
            $pdfc->setPageHeight($opt['height']);

            if($opt['header']) {
                $pdfc->setHeaderHtml($opt['header']);
            }

            if($opt['footer']) {
                $pdfc->setFooterHtml($opt['footer']);
            }
            
            $pdfc->setPageMargins($opt['top'], $opt['right'], $opt['bottom'], $opt['right']);
            $pdfc->usePrintMedia(true);
            $pdfc->enableImages(true);
            
            $pdf = $pdfc->convertHtml($opt['html']);
            file_put_contents($path.$filename, $pdf); 
            
            return $path.$filename;
        }
        catch(PdfcrowdException $why)
        {
            return "Pdfcrowd Error: " . $why;
        }
    }

    public function buildForm($caseID,$filename,$caseInfo)
    {
        // Start PDF
        $cff = App::load()->model('casefiles',true,true);
        // Beneficiary
        $beneficiaryHtml = '';
            $beneficiaries = $cff->getBeneficiaries($caseID);
            if(count($beneficiaries)) {
                foreach($beneficiaries as $beneficiary) {
                    $beneficiaryHtml .= '<div class="section small-top-bg">
                        <img src="http://drive.GASkylight.com/emails/bgs/gaform/CA/bgnohead.png" class="section-bg">

                        <div class="section-body">
                            <div class="heading">
                                <div class="left col-sm-6">
                                    <h3>Nominated Beneficiary</h3>
                                </div>
                                <div class="right col-sm-6">
                                    <h3>提名受益人</h3>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="content-body">
                                <div class="col-sm-6">
                                    <div class="left-c content-box">

                                        <div class="form-group-box">
                                            <label>Surname 姓</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABLastName.'</strong></span>
                                            </div>
                                        </div>

                                        <div class="form-group-box">
                                            <label>Given Name 名</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABFirstName.'</strong></span>
                                            </div>
                                        </div>

                                        <div class="form-group-box">
                                            <label>Relationship 關係</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABRelationship.'</strong></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="right-c content-box">

                                        <div class="form-group-box">
                                            <label>Contact Phone 聯絡電話</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABPhone.'</strong></span>
                                            </div>
                                        </div>

                                        <div class="form-group-box">
                                            <label>Email 電郵地址</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABEmail.'</strong></span>
                                            </div>
                                        </div>

                                        <div class="form-group-box">
                                            <label>Percentage (%) of Benefit 受益比例 %</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABPercentage.'</strong></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="full-c content-box">

                                        <div class="form-group-box">
                                            <label>Primary Correspondence Address 主要通訊地址</label>
                                            <div class="form-box">
                                               <span class="med"><strong>'.$beneficiary->ABAddress1.'</strong></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>';

                }
            }

            $emptyFieldsHTML = '<div class="section small-top-bg">
                    <img src="http://drive.GASkylight.com/emails/bgs/gaform/CA/bgnohead.png" class="section-bg">

                    <div class="section-body">
                        <div class="heading">
                            <div class="left col-sm-6">
                                <h3>Nominated Beneficiary</h3>
                            </div>
                            <div class="right col-sm-6">
                                <h3>提名受益人</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="content-body">
                            <div class="col-sm-6">
                                <div class="left-c content-box">

                                    <div class="form-group-box">
                                        <label>Given Name 名</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                    <div class="form-group-box">
                                        <label>Surname 姓</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                    <div class="form-group-box">
                                        <label>Relationship 關係</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="right-c content-box">

                                    <div class="form-group-box">
                                        <label>Contact Phone 聯絡電話</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                    <div class="form-group-box">
                                        <label>Email 電郵地址</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                    <div class="form-group-box">
                                        <label>Percentage (%) of Benefit 受益比例 %</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="full-c content-box">

                                    <div class="form-group-box">
                                        <label>Primary Correspondence Address 主要通訊地址</label>
                                        <div class="form-box">
                                           <span class="med"><strong>--</strong></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>';
        // End Beneficiary

        if(strpos($caseInfo->ProductName, 'Performance')){
            $prodSel = 'Performance';
        }

        if(strpos($caseInfo->ProductName, 'Platinum')){
            $prodSel = 'Platinum';
        }

        if(strpos($caseInfo->ProductName, 'Private')){
            $prodSel = 'Private';
        }

        if(strpos($caseInfo->ProductName, 'Others')){
            $prodSel = 'Others';
        }

        $shortcode = array(
            '[DateNow]',
            '[POADate]',
            '[POACompanyName]',
            '[POACompanyNumber]',
            '[POACompanyCountry]',
            '[POACompanyAddress]',
            '[POACompanyCity]',
            '[POACompanyState]',
            '[POAAppointor]',
            '[POAAppointorIdNumber]',
            '[POASecretaryName]',
            '[POADirectorName]',
            '[POASecretarySign]',
            '[POADirectorSign]',
            '[POACorporateSeal]',
            '[CompanyName]',
            '[RegistrationNo]',
            '[RegistrationAddress]',
            '[RegistrationCountry]',
            '[RegistrationTelephone]',
            '[BusinessAddress]',
            '[LastName]',
            '[IdNumber]',
            '[FirstName]',
            '[Country]',
            '[DateOfBirth]',
            '[BusinessCountry]',
            '[Occupation]',
            '[JobTitle]',
            '[Address]',
            '[Address2]',
            '[Address3]', 
            '[Address4]',
            '[Address5]',
            '[Address6]',
            '[Mobile]',
            '[Email]',
            '[Name]',
            '[AccountName]',
            '[AccountNumber]',        
            '[SwiftCode]',
            '[UBOName]',
            '[UBOAddress]',
            '[UBOSign]',
            '[PrimaryTaxResidency]',
            '[TaxIdNumber]',
            '[ApprovedBy]',
            '[ApprovedDate]',
            '[CAAuthorizedone]',
            '[CAAuthorizedonename]',
            '[CAAuthorizedonetitle]',
            '[CAAuthorizedtwo]',
            '[CAAuthorizedtwoname]',
            '[CAAuthorizedtwotitle]',
            '[CAAuthorizedthree]',
            '[CAAuthorizedthreename]',
            '[CAAuthorizedthreetitle]',
            '[CAAuthorizedfour]',
            '[CAAuthorizedfourname]',
            '[CAAuthorizedfourtitle]',
            '[AccountType'.$caseInfo->AccountType.']',
            '[Gender'.$caseInfo->Gender.']',
            '[CivilStatus]',
            '[NeedCoolingOff'.$caseInfo->NeedCoolingOff.']',
            '[Language'.$caseInfo->Language.']',
            '[UBOEmploymentIncomeY]',
            '[UBOCommissionY]',
            '[UBOBusinessY]',
            '[UBOInheritanceY]',
            '[UBOGiftY]',
            '[UBOSalesY]',
            '[UBOOtherY]',
            '[IAPhotoid]',
            '[IAProofresidency]',
            '[IABankstatement]',
            '[IASpecimensignOK]',
            '[IASpecimensign]',
            '[CACertincorporation]',
            '[CANamechange]',
            '[CAGoodstand]',
            '[CARegdirector]',
            '[CAProofbusadd]',
            '[CAMemorandumaa]',
            '[CARecentfinancialstatement]',
            '[CADirectorsid]',
            '[CACompanysign]',
            '[CAShareholders]',
            '[CADirectorsproof]',
            '[CACompanysignproof]',
            '[CAShareholdersproof]',
            '[CORPORATE]',
            '[INDIVIDUAL]',
            '[SubsDecName]',
            '[SubsDecDate]',
            '[SubsDecSignature]',
            '[HasNominatedBeneficiaries'.$caseInfo->HasNominatedBeneficiaries.']',
            '[Beneficiaries]',
            '[Phone]',
            '[Salutation]',
            '[CurrencyCode'.$caseInfo->Code.']',
            '[ProductSel'.$prodSel.']',
            '[ProductShow'.$prodSel.']',
            '[ProductHtmlEn]',
            '[ProductHtmlCn]',
            '[UBOOtherDescribe]',
            '[UBOOtherDescribeYES]'
        );



        $atts = array('height'=>'100%');
        $POASecretarySign = ($caseInfo->POASecretarySign) ? App::outputUploadedFile($caseInfo->POASecretarySign,$atts) : '';
        $POADirectorSign = ($caseInfo->POADirectorSign) ? App::outputUploadedFile($caseInfo->POADirectorSign,$atts) : '';
        $POACorporateSeal = ($caseInfo->POACorporateSeal) ? App::outputUploadedFile($caseInfo->POACorporateSeal,$atts) : '';
        $IASpecimensign = ($caseInfo->IASpecimensign) ? App::outputUploadedFile($caseInfo->IASpecimensign,$atts) : '';
        $SubsDecSignature = ($caseInfo->SubsDecSignature) ? App::outputUploadedFile($caseInfo->SubsDecSignature,$atts) : '';
        
        $CAAuthorizedone = ($caseInfo->CAAuthorizedone) ? App::outputUploadedFile($caseInfo->CAAuthorizedone,$atts) : '';
        $CAAuthorizedtwo = ($caseInfo->CAAuthorizedtwo) ? App::outputUploadedFile($caseInfo->CAAuthorizedtwo,$atts) : '';
        $CAAuthorizedthree = ($caseInfo->CAAuthorizedthree) ? App::outputUploadedFile($caseInfo->CAAuthorizedthree,$atts) : '';
        $CAAuthorizedfour = ($caseInfo->CAAuthorizedfour) ? App::outputUploadedFile($caseInfo->CAAuthorizedfour,$atts) : '';
        
        $UBOSign = ($caseInfo->AccountType == 'Individual') ? $IASpecimensign : $POADirectorSign;          

        
        $scvalues = array(
            date('d/m/Y'),
            ($caseInfo->AccountType == 'Individual') ? '' : App::date($caseInfo->POADate),
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POACompanyName,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POACompanyNumber,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POACompanyCountry,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POACompanyAddress,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POACompanyCity,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POACompanyState,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POAAppointor,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POAAppointorIdNumber,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POASecretaryName,
            ($caseInfo->AccountType == 'Individual') ? '' : $caseInfo->POADirectorName,
            ($caseInfo->AccountType == 'Individual') ? '' : $POASecretarySign,
            ($caseInfo->AccountType == 'Individual') ? '' : $POADirectorSign,
            ($caseInfo->AccountType == 'Individual') ? '' : $POACorporateSeal,
            $caseInfo->CompanyName,
            $caseInfo->RegistrationNo,
            $caseInfo->RegistrationAddress,
            $caseInfo->RegistrationCountry,
            $caseInfo->RegistrationTelephone,
            $caseInfo->BusinessAddress,
            $caseInfo->LastName,
            $caseInfo->IdNumber,
            $caseInfo->FirstName,
            $caseInfo->Country,
            App::date($caseInfo->DateOfBirth),
            $caseInfo->BusinessCountry,
            $caseInfo->Occupation,
            $caseInfo->JobTitle,
            $caseInfo->Address,
            $caseInfo->Address2,
            $caseInfo->Address3, 
            $caseInfo->Address4,
            $caseInfo->Address5,
            $caseInfo->Address6,
            $caseInfo->Mobile,
            $caseInfo->AccountEmail,
            $caseInfo->Name,
            $caseInfo->AccountName,
            $caseInfo->AccountNumber,        
            $caseInfo->SwiftCode,
            $caseInfo->UBOName,
            $caseInfo->UBOAddress,
            $UBOSign,
            $caseInfo->PrimaryTaxResidency,
            $caseInfo->TaxIdNumber,
            $caseInfo->ApprovedBy,
            ($caseInfo->ApprovedDate == "0000-00-00") ? " " : App::date($caseInfo->ApprovedDate),
            $CAAuthorizedone,
            $caseInfo->CAAuthorizedonename,
            $caseInfo->CAAuthorizedonetitle,
            $CAAuthorizedtwo,
            $caseInfo->CAAuthorizedtwoname,
            $caseInfo->CAAuthorizedtwotitle,
            $CAAuthorizedthree,
            $caseInfo->CAAuthorizedthreename,
            $caseInfo->CAAuthorizedthreetitle,
            $CAAuthorizedfour,
            $caseInfo->CAAuthorizedfourname,
            $caseInfo->CAAuthorizedfourtitle,
            'circled',
            'circled',
            $caseInfo->CivilStatus,
            'circled',
            'circled',
            ($caseInfo->UBOEmploymentIncome) ? 'circled': '',
            ($caseInfo->UBOCommission) ? 'circled': '',
            ($caseInfo->UBOBusiness) ? 'circled': '',
            ($caseInfo->UBOInheritance) ? 'circled': '',
            ($caseInfo->UBOGift) ? 'circled': '',
            ($caseInfo->UBOSales) ? 'circled': '',
            ($caseInfo->UBOOther) ? 'circled': '',
            ($caseInfo->IAPhotoid) ? 'circled': '',
            ($caseInfo->IAProofresidency) ? 'circled': '',
            ($caseInfo->IABankstatement) ? 'circled': '',
            ($caseInfo->IASpecimensign) ? 'circled': '',
            (isset($IASpecimensign)) ? $IASpecimensign : "None",
            ($caseInfo->CACertincorporation) ? 'circled': '',
            ($caseInfo->CANamechange) ? 'circled': '',
            ($caseInfo->CAGoodstand) ? 'circled': '',
            ($caseInfo->CARegdirector) ? 'circled': '',
            ($caseInfo->CAProofbusadd) ? 'circled': '',
            ($caseInfo->CAMemorandumaa) ? 'circled': '',
            ($caseInfo->CARecentfinancialstatement) ? 'circled': '',
            ($caseInfo->CADirectorsid) ? 'circled': '',
            ($caseInfo->CACompanysign) ? 'circled': '',
            ($caseInfo->CAShareholders) ? 'circled': '',
            ($caseInfo->CADirectorsproof) ? 'circled': '',
            ($caseInfo->CACompanysignproof) ? 'circled': '',
            ($caseInfo->CAShareholdersproof) ? 'circled': '',
            ($caseInfo->AccountType == 'Individual') ? 'hidden' : '',
            ($caseInfo->AccountType == 'Individual') ? '' : 'hidden',
            $caseInfo->SubsDecName,
            ($caseInfo->SubsDecDate == "0000-00-00") ? " " : App::date($caseInfo->SubsDecDate),
            $SubsDecSignature,
            ($caseInfo->HasNominatedBeneficiaries) ? 'circled': '',
            (count($beneficiaries)) ? $beneficiaryHtml : $emptyFieldsHTML,
            $caseInfo->Phone,
            $caseInfo->Salutation,
            'circled',
            'circled',
            'block',
            $caseInfo->ProductFullHtmlEn,
            $caseInfo->ProductFullHtmlCn,
            $caseInfo->UBOOtherDescribe,
            ($caseInfo->UBOOtherDescribe) ? "circled" : ''

        );        
        
        $formFormat = 'traditional.eml';
        switch($caseInfo->FormType) {
            case 'Traditional': 
                $formFormat = 'traditional.eml';
            break;
            case 'Simplified': 
                $formFormat = 'simplified.eml';
            break;
        }
        
        $opt = array(
            'filename' => $filename,
            'html' => str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'pdfcontent'.DS.$formFormat)),
            'top' => "0pt",
            'right' => "0pt",
            'bottom' => "0pt",
            'left' => "0pt",
        );
        
        return $this->generate($opt);
    }

    public function buildCommencement($caseID,$filename,$caseInfo)
    {
        $shortcode = array(
            '[DateNow]',
            '[CommencementDate]',
            '[FirstName]',
            '[LastName]',
            '[Address]',
            '[Address2]',
            '[Address3]',
            '[City]',
            '[State]',
            '[PostalCode]',
            '[ReceivedAmount]',
            '[PaymentReceived]',
            '[MaturityDate]',
            '[DepositedAmount]',
            '[ProductName]',
            '[InvestmentAmount]',
            '[TenureMonths]',
            '[AnnualInterest]',
            '[TotalReturn]',
            '[PrdCurrencyCode]',
            '[RMBCN1]',
            '[USDCN1]'
        );

        if(!empty($caseInfo->Address4) || !empty($caseInfo->Address5) || !empty($caseInfo->Address6)){
            $CommAddress1 = $caseInfo->Address4;
            $CommAddress2 = $caseInfo->Address5;
            $CommAddress3 = $caseInfo->Address6;
        }else{
            $CommAddress1 = $caseInfo->Address;
            $CommAddress2 = $caseInfo->Address2;
            $CommAddress3 = $caseInfo->Address3;
        }
        
        $scvalues = array(
            date('d-m-Y'),
            ($caseInfo->CommencementDate == "0000-00-00") ? " " : App::date($caseInfo->CommencementDate),
            $caseInfo->FirstName,
            $caseInfo->LastName,
            ($CommAddress1) ? $CommAddress1 : " ",
            ($CommAddress2) ? $CommAddress2 : " ",
            ($CommAddress3) ? $CommAddress3 : " ",
            $caseInfo->City,
            $caseInfo->State,
            $caseInfo->PostalCode,
            App::currency($caseInfo->ReceivedAmount),
            ($caseInfo->PaymentReceived == "0000-00-00") ? " " : App::date($caseInfo->PaymentReceived),
            ($caseInfo->MaturityDate == "0000-00-00") ? " " : App::date($caseInfo->MaturityDate),
            App::currency($caseInfo->DepositedAmount),
            $caseInfo->ProductName,
            App::currency($caseInfo->InvestmentAmount),
            $caseInfo->TenureMonths,
            $caseInfo->AnnualInterest,
            App::currency( ((($caseInfo->AnnualInterest / 100) * $caseInfo->DepositedAmount) * ($caseInfo->TenureMonths/12)) + $caseInfo->DepositedAmount),
            $caseInfo->Code,
            ($caseInfo->Code == 'RMB') ? '人民幣' : "",
            ($caseInfo->Code == 'USD') ? '美元' : ""

        );
        
        $theHtml = App::getFileContents('emails'.DS.'pdfcontent'.DS.'commencement.eml');
        $style = App::getTagContent('<style>','</style>',$theHtml,true);
        $content = App::extractTagContent('<header>','</header>',$theHtml);
        $footer = App::extractTagContent('<footer>','</footer>',$content['main'],true);
        
        $body = $footer['main'];
        $head = $style.'<div id="wrap-all">'.$content['fragment'].'</div>';
        $foot = $style.'<div id="wrap-all">'.$footer['fragment'].'</div>';
        
        $opt = array(
            'filename' => $filename,
            'header' => $head,
            'footer' => $foot,
            'top' => '0pt',
            'left' => '0pt',
            'right' => '0pt',
            'bottom' => '0pt',
            'html' => str_replace($shortcode,$scvalues,$body)
        );
        
        return $this->generate($opt);
    }

    public function buildTermSheet($caseID,$tsheetName,$filename,$caseInfo)
    {
        $shortcode = array(            
            '[FirstName]',
            '[LastName]',
            '[PrdCurrencyCode]',
            '[DepositedAmount]',            
            '[CommencementDate]',
            '[MaturityDate]'
        );
                
        $commDate = ($caseInfo->CommencementDate == "0000-00-00") ? "-" : date('d M-Y', strtotime($caseInfo->CommencementDate));
        $matuDate = ($caseInfo->MaturityDate == "0000-00-00") ? "-" : date('d M-Y', strtotime($caseInfo->MaturityDate));

        $scvalues = array(
            $caseInfo->FirstName,
            $caseInfo->LastName,
            $caseInfo->Code,
            App::currency($caseInfo->DepositedAmount),
            $commDate,
            $matuDate
        );
        
        $theHtml = App::getFileContents('emails'.DS.'terms'.DS.$filename);

        $style = App::getTagContent('<style>','</style>',$theHtml,true);
        $content = App::extractTagContent('<header>','</header>',$theHtml);
        $footer = App::extractTagContent('<footer>','</footer>',$content['main'],true);
        
        $body = $footer['main'];
        $head = $style.'<div id="wrap-all">'.$content['fragment'].'</div>';
        $foot = $style.'<div id="wrap-all">'.$footer['fragment'].'</div>';
        
        $opt = array(
            'filename' => $tsheetName,
            'header' => $head,
            'footer' => $foot,
            'top' => '0pt',
            'left' => '0pt',
            'right' => '0pt',
            'bottom' => '0pt',
            'html' => str_replace($shortcode,$scvalues,$body)
        );
        
        return $this->generate($opt);
    }
    
    public function buildDemandNote($caseID,$filename,$caseInfo)
    {
        $shortcode = array(
            '[DateNow]',
            '[AgreementYear]',
            '[AgreementMonth]',
            '[AgreementDay]',
            '[DemandNoteNo]',
            '[CommencementDate]',
            '[FirstName]',
            '[LastName]',
            '[Address]',
            '[Address2]',
            '[Address3]',
            '[City]',
            '[State]',
            '[PostalCode]',
            '[ReceivedAmount]',
            '[PaymentReceived]',
            '[MaturityDate]',
            '[DepositedAmount]',
            '[ProductName]',
            '[InvestmentAmount]',
            '[TenureMonths]',
            '[AnnualInterest]',
            '[TotalReturn]',
            '[Signature]',
            '[OpeningFee]'
        );
        
        $POADirectorSign = ($caseInfo->POADirectorSign) ? App::outputUploadedFile($caseInfo->POADirectorSign,$atts) : '';
        $IASpecimensign = ($caseInfo->IASpecimensign) ? App::outputUploadedFile($caseInfo->IASpecimensign,$atts) : '';        
        
        $UBOSign = ($caseInfo->AccountType == 'Individual') ? $IASpecimensign : $POADirectorSign;

        
        if($caseInfo->AccountStatus != "Approved"){
            $commDate = date('M-Y', strtotime($caseInfo->CommencementDate));
            $matuDate = date('M-Y', strtotime($caseInfo->MaturityDate));
        }else{
            $commDate = ($caseInfo->CommencementDate == "0000-00-00") ? " " : date('M-Y', strtotime($caseInfo->CommencementDate));
            $matuDate = ($caseInfo->MaturityDate == "0000-00-00") ? " " : date('M-Y', strtotime($caseInfo->MaturityDate));
        }

        if(!empty($caseInfo->Address4) || !empty($caseInfo->Address5) || !empty($caseInfo->Address6)){
            $CommAddress1 = $caseInfo->Address4;
            $CommAddress2 = $caseInfo->Address5;
            $CommAddress3 = $caseInfo->Address6;
        }else{
            $CommAddress1 = $caseInfo->Address;
            $CommAddress2 = $caseInfo->Address2;
            $CommAddress3 = $caseInfo->Address3;
        }
        
        $scvalues = array(
            date('d-M-Y'),
            date('Y'),
            date('F'),
            date('d'),
            'GA-DM-'. strtoupper(substr(md5(microtime(false)), 0, 10)),
            $commDate,
            $caseInfo->FirstName,
            $caseInfo->LastName,
            ($CommAddress1) ? $CommAddress1 : " ",
            ($CommAddress2) ? $CommAddress2 : " ",
            ($CommAddress3) ? $CommAddress3 : " ",
            $caseInfo->City,
            $caseInfo->State,
            $caseInfo->PostalCode,
            App::currency($caseInfo->ReceivedAmount),
            ($caseInfo->PaymentReceived == "0000-00-00") ? " " : date('j M-Y', strtotime($caseInfo->PaymentReceived)),
            $matuDate,
            App::currency($caseInfo->DepositedAmount),
            $caseInfo->ProductName,
            App::currency($caseInfo->InvestmentAmount),
            $caseInfo->TenureMonths,
            $caseInfo->AnnualInterest,
            App::currency( ((($caseInfo->AnnualInterest / 100) * $caseInfo->DepositedAmount) * ($caseInfo->TenureMonths/12)) + $caseInfo->DepositedAmount),
            $UBOSign,
            $caseInfo->OpeningFee
        );
        
        $theHtml = App::getFileContents('emails'.DS.'pdfcontent'.DS.'demandnote.eml');
                
        $opt = array(
            'filename' => $filename,
            'html' => str_replace($shortcode,$scvalues,$theHtml)
        );
        
        return $this->generate($opt);
    }

    public function buildBookingDemandNote($bookID,$filename,$bookInfo)
    {
        $shortcode = array(
            '[DateNow]',
            '[DemandNoteNo]',
            '[InvestmentBookingID]',
            '[ClientProfileID]',
            '[SharesToApply]',
            '[TotalAmountAttached]',
            '[Name]',
            '[AccoutName2]',
            '[AccuntName3]',
            '[Address]',
            '[Suburb]',
            '[State]',
            '[PostCode]',
            '[Phone]',
            '[Email]',
            '[BookingStatus]',
            '[CompanyName]',
            '[TypeOfOffer]',
            '[BankName]',
            '[BankAddress]',
            '[BankAccountNumber]',
            '[BankAccountName]',
            '[SwiftCode]'
        );
        
        $scvalues = array(
            date('d-M-Y'),
            'SO-DN-'. strtoupper(substr(md5(microtime(false)), 0, 10)),
            'BK-533D0M364-'.$bookInfo->InvestmentBookingID,
            $bookInfo->ClientProfileID,
            number_format($bookInfo->SharesToApply),
            number_format($bookInfo->TotalAmountAttached,2),
            ($bookInfo->Name) ? $bookInfo->Name : " ",
            ($bookInfo->AccoutName2) ? $bookInfo->AccoutName2 : " ",
            ($bookInfo->AccuntName3) ? $bookInfo->AccuntName3 : " ",
            ($bookInfo->Address) ? $bookInfo->Address : " ",
            ($bookInfo->Suburb) ? $bookInfo->Suburb : " ",
            ($bookInfo->State) ? $bookInfo->State : " ",
            ($bookInfo->PostCode) ? $bookInfo->PostCode : " ",
            ($bookInfo->Phone) ? $bookInfo->Phone : " ",
            ($bookInfo->Email) ? $bookInfo->Email : " ",
            ($bookInfo->BookingStatus) ? $bookInfo->BookingStatus : " ",
            ($bookInfo->CompanyName) ? $bookInfo->CompanyName : " ",
            ($bookInfo->TypeOfOffer) ? $bookInfo->TypeOfOffer : " ",
            $bookInfo->BankName,
            $bookInfo->BankAddress,
            $bookInfo->BankAccountNumber,
            $bookInfo->BankAccountName,
            $bookInfo->SwiftCode
        );
        
        $theHtml = App::getFileContents('emails'.DS.'pdfcontent'.DS.'bookingdemandnote.eml');
                
        $opt = array(
            'filename' => $filename,
            'html' => str_replace($shortcode,$scvalues,$theHtml)
        );
        
        return $this->generate($opt);
    }

    public function buildStatementOfAccount($userID, $datevalue, $filename)
    {
        $userinfo = User::info(false, $userID);
        $wallet = App::load()->model('wallet',true,true);

        if($datevalue) {
            $dates = explode(' - ',$datevalue);
            $datefrom = date( 'M Y', strtotime($dates[0]) );
            $dateto = date( 'M Y', strtotime($dates[1]) );
        }

        $cntr = 0;
        $statementHTML = '';
        $statements = $wallet->getAccountStatements($userinfo->UserID, $datevalue);
        if(count($statements)) {
            $runningbal = 0;
            $prevval = 0;            
            foreach(array_reverse($statements) as $data) { $cntr++;
                // Calculate Running Balance
                switch ($data->TransactionType) {
                    case 'Credit':
                        $runningbal = $prevval+str_replace('-', '', $data->TransactionAmount);
                        break;
                    case 'Debit':
                    $runningbal = $prevval-str_replace('-', '', $data->TransactionAmount);
                        break;
                }
                $prevval = $runningbal;

                $statementHTML .='<tr>';
                    $statementHTML .='<td class="text-muted">';
                        $statementHTML .= ($data->Description) ? $data->Description : "-";
                    $statementHTML .='</td>';
                    $statementHTML .='<td class="text-center">';
                        $statementHTML .= ($data->ApproveDate != '0000-00-00 00:00:00') ? date('M d Y', strtotime($data->ApproveDate)) : '-';
                    $statementHTML .='</td>';
                    $statementHTML .='<td class="text-right text-success">';
                        $statementHTML .=($data->TransactionType == 'Credit') ? '<strong>$ '.number_format($data->TransactionAmount,2).'</strong>' : '';
                    $statementHTML .='</td>';
                    $statementHTML .='<td class="text-right text-danger">';
                        $statementHTML .= ($data->TransactionType == 'Debit') ? '<strong>$ '.number_format($data->TransactionAmount,2).'</strong>' : '';
                    $statementHTML .='</td>';
                    $statementHTML .='<td class="text-right hidden-xs text-muted">';
                        $statementHTML .='<strong> $ '.number_format($runningbal,2).'</strong>';
                    $statementHTML .='</td>';
                $statementHTML .='</tr>';

            } 
        }

        $shortcode = array(
            '[UserID]',
            '[FirstName]',
            '[LastName]',
            '[DateRange]',
            '[StatementHTML]'

        );
        
        $scvalues = array(
            $userinfo->UserID, 
            $userinfo->FirstName,
            $userinfo->LastName,
            ($datevalue) ? $datefrom.' - '.$dateto : '-',
            ( count($statements) ) ? $statementHTML : '<tr><td colspan="5">No Records</td></tr>'
        );
        
        $theHtml = App::getFileContents('emails'.DS.'pdfcontent'.DS.'so_wallet_soa.eml');
                
        $opt = array(
            'filename' => $filename,
            'html' => str_replace($shortcode,$scvalues,$theHtml),
            'top' => "10pt",
            'right' => "10pt",
            'bottom' => "10pt",
            'left' => "10pt"
        );
        
        return $this->generate($opt);
    }
}