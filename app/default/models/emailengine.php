<?php
class EmailEngine extends Model
{
    function test(object $obj)
    {
        print_r($obj);
    }
    
    function send($to='',$name='',$subject='',$content = '',$attachment=false,$cc = false,$bcc = false,$from = false,$fromname = false)
    {
        try {                               
                                
            App::vendor('phpmailer/PHPMailerAutoload');
            //PHPMailer Object
            $mail = new PHPMailer(true);

            $frm = ($from != false) ? $from : Option::get('email_from');
            $frn = ($from != false) ? $fromname : Option::get('email_from_name');
            
            $mail->isSMTP(); // tell to use smtp
            //$mail->SMTPDebug  = 2; 
            $mail->CharSet = "utf-8"; // set charset to utf8
            $mail->SMTPAuth = true;  // use smpt auth
            $mail->SMTPSecure = Config::get('MAIL_ENCRYPTION');; // or ssl
            $mail->Host = Config::get('MAIL_HOST');
            $mail->Port = Config::get('MAIL_PORT'); // most likely something different for you. This is the mailtrap.io port i use for testing. 
            $mail->Username = Config::get('MAIL_USERNAME');
            $mail->Password = Config::get('MAIL_PASSWORD');
            $mail->setFrom($frm, $frn);
            $mail->addAddress($to, $name);
            
            //$mail->addCC(Option::get('email_cc'));
            if($cc) {
                foreach($cc as $ccc) {
                    $mail->addCC($ccc['email'],$ccc['name']);
                }
            }
            $mail->addBCC(Option::get('email_bcc'));
            if($bcc) {
                foreach($bcc as $bccc) {
                    $mail->addBCC($bccc['email'],$bccc['name']);
                }
            }
            $mail->Subject = $subject;
            $mail->MsgHTML($content);
            
            
            if($attachment) {           
                foreach($attachment as $att) {
                    $mail->AddAttachment($att['path'],$att['name']);
                }
            }
            
            //if(Config::get('ENVIRONMENT') === 'production') {
                if (!$mail->send()) {
                    return false;
                } else {
                    return true;
                }
            //}
        } catch(phpmailerException $e) {
            echo $e->getMessage();  
            $this->sendFallBack($to,$name,$subject,$content,$attachment,$cc,$bcc);          
        }
    }  

    function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) 
    {
        $file = $path.$filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $header = "From: ".$from_name." <".$from_mail.">\r\n";


        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
        $header .= $content."\r\n\r\n";
        $header .= "--".$uid."--";
        if (mail($mailto, $subject, "", $header)) {
            echo "mail send ... OK"; // or use booleans here
        } else {
            echo "mail send ... ERROR!";
        }
    }

    function sendFallBack($to='',$name='',$subject='',$contents = '',$attachment=false,$cc = false,$bcc = false)
    {
        $attcheader = '';
        if($attachment) {           
            foreach($attachment as $att) {
                //$mail->AddAttachment($att['path'],$att['name']);

                $file = $att['path'];
                $file_size = filesize($file);
                $handle = fopen($file, "r");
                $content = fread($handle, $file_size);
                fclose($handle);
                $content = chunk_split(base64_encode($content));

                $attcheader .= "Content-Type: application/octet-stream; name=\"".$att['name']."\"\r\n"; // use different content types here
                $attcheader .= "Content-Transfer-Encoding: base64\r\n";
                $attcheader .= "Content-Disposition: attachment; filename=\"".$att['name']."\"\r\n\r\n";
                $attcheader .= $content."\r\n\r\n";
            }
        }
        
        $uid = md5(uniqid(time()));
        $header = "From: ".$from_name." <".$from_mail.">\r\n";

        if($cc) {
            foreach($cc as $ccc) {
                $mail->addCC($ccc['email'],$ccc['name']);

                $header .= "Cc: ".$ccc['name']." <".$ccc['email'].">\r\n";
            }
        }
        
        if($bcc) {
            foreach($bcc as $bccc) {                    
                $header .= "Bcc: ".$bccc['name']." <".$bccc['email'].">\r\n";
            }
        }  

        $header .= "Reply-To: ".$replyto."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
        $header .= $content."\r\n\r\n";
        $header .= "--".$uid."--";

        mail($to, $subject, $contents, $header);
    }  

    function sendToMany($to = array(), $subject = '', $content = '', $attachment=false)
    {
        try {
            App::vendor('phpmailer/PHPMailerAutoload');
            //PHPMailer Object
            $mail = new PHPMailer;

            $mail->isSMTP(); // tell to use smtp
            $mail->CharSet = "utf-8"; // set charset to utf8
            $mail->SMTPAuth = true;  // use smpt auth
            $mail->SMTPSecure = Config::get('MAIL_ENCRYPTION');; // or ssl
            $mail->Host = Config::get('MAIL_HOST');
            $mail->Port = Config::get('MAIL_PORT'); // most likely something different for you. This is the mailtrap.io port i use for testing. 
            $mail->Username = Config::get('MAIL_USERNAME');
            $mail->Password = Config::get('MAIL_PASSWORD');
            $mail->setFrom(Option::get('email_from'), Option::get('email_from_name'));
            foreach($to as $t) {
                $mail->addAddress($t['email'], $t['name']);
            }
            $mail->addCC(Option::get('email_cc'));
            $mail->addBCC(Option::get('email_bcc'));
            $mail->Subject = $subject;
            $mail->MsgHTML($content);
            
            
            if($attachment) {           
                foreach($attachment as $att) {
                    $mail->AddAttachment($att['path'],$att['name']);
                }
            }
            
            if (!$mail->send()) {
                return false;
            } else {
                return true;
            }
        } catch(phpmailerException $e) {

        }
    }

    function getAdmins()
    {
        $sql = "SELECT u.UserID,u.Email,um.FirstName,um.LastName FROM users u LEFT JOIN user_meta um ON um.UserID = u.UserID WHERE u.Level = 1 AND u.Active = 1";
        $sql .= " ORDER BY u.UserID";
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $data[] = $row;         
        }
        unset($query);
        
        return $data;
    } 

    public function sendBookingEmail( $clientdata, $bookdata, $testEm = false )
    {
        $bookID = 'BK-533D0M364-'.$bookdata->InvestmentBookingID;
        // Start PDF
        $pdf = App::load()->model( 'pdfengine', true );
        // Commencement
        $bookingName = $bookdata->UserID.'_'.$bookID.'_demandnote.pdf';
        $bookingPdf = $pdf->buildBookingDemandNote( $bookdata->InvestmentBookingID, $bookingName, $bookdata );
        // End PDF        

        $to = $bookdata->Email;
        $name = $bookdata->Name;
        if( $testEm == 'test' ){
            $userinfo = User::info();
            $to = $userinfo->Email;
            $name = $userinfo->FirstName ? $userinfo->FirstName.' ' : '';
            $name .= $userinfo->LastName ? $userinfo->LastName : '';
        }
        $subject = "Your Booking has been Registered!";

        $shortcode = array(
            '[InvestmentBookingID]',
            '[Name]',
            '[CompanyName]',
            '[BookingInfoUrl]',
            '[LoginUrl]'
        );
        $scvalues = array(
            $bookID,
            $bookdata->Name,
            $bookdata->CompanyName,
            View::url( 'bookings/info/'.$bookdata->InvestmentBookingID ),
            View::url( 'users/login' )
        );

        $content = str_replace( $shortcode, $scvalues, App::getFileContents( 'emails'.DS.'bookings'.DS.'so_booking_successfuly.eml' ) );
        $root_path = APPPATH."emails".DS.'attachment'.DS;
        $attachment = array(
            array( 'path' => $bookingPdf, 'name' => $bookingName )
        );

        // Admins
        $bcc = array();
        $bcc[$clientdata->UserID]['email'] = $clientdata->Email;
        $bcc[$clientdata->UserID]['name'] = $clientdata->FirstName.' '.$clientdata->LastName;

        foreach( $this->getAdmins() as $admin ){
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if( $this->send( $to, $name, $subject, $content, $attachment, false, $bcc ) ){
        }
        // End Email sending

        App::activityLog( "Sent booking email letter. Booking ID#".$bookID."." );
    }

    public function sendInterestAcceptedEmail($bookdata,$testEm=false)
    {
        $bookID = 'BK-533D0M364-'.$bookdata->InvestmentBookingID;
        // Start PDF
        $pdf = App::load()->model('pdfengine',true);
            // Commencement
            $bookingName = $bookdata->UserID.'_'.$bookID.'_demandnote.pdf';
            $bookingPdf = $pdf->buildBookingDemandNote($bookdata->InvestmentBookingID,$bookingName,$bookdata);  
        // End PDF

        $to = ($testEm=='test') ? User::info('Email') : $bookdata->Email;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $bookdata->Name;
        $subject = "Your Interest has been Accepted!";

        $shortcode = array(
            '[InvestmentBookingID]',
            '[Name]',
            '[CompanyName]',
            '[LoginUrl]'
        );
        $scvalues = array(
            $bookID, 
            $bookdata->Name,
            $bookdata->CompanyName,
            View::url('users/login')
        );

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'bookings'.DS.'so_interest_accepted.eml'));

        $root_path = APPPATH."emails".DS.'attachment'.DS;
        $attachment =array(
            array('path' => $bookingPdf,'name' => $bookingName)
        );  

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if($this->send($to,$name,$subject,$content,$attachment,false,$bcc)) {
            App::activityLog("Sent Interest Acceptance email letter.");
        }
        // End Email sending        
    }

    public function sendInterestEmail($clientdata,$bookdata,$testEm=false)
    {
        $bookID = 'BK-533D0M364-'.$bookdata->InvestmentBookingID;
        $to = ($testEm=='test') ? User::info('Email') : $clientdata->Email;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $bookdata->Name;
        $subject = "Your Interest has been Registered!";

        $shortcode = array(
            '[InvestmentBookingID]',
            '[Name]',
            '[CompanyName]',
            '[LoginUrl]'
        );
        $scvalues = array(
            $bookID, 
            $bookdata->Name,
            $bookdata->CompanyName,
            View::url('users/login')
        );

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'bookings'.DS.'so_booking_interest.eml'));

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if($this->send($to,$name,$subject,$content,false,false,$bcc)) {
            App::activityLog("Sent Interest email letter.");
        }
        // End Email sending        
    }

    public function sendBookingAknowledgeEmail($bookdata, $testEm=false)
    {
        $to = ($testEm=='test') ? User::info('Email') : $bookdata->Email;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $bookdata->Name;
        $subject = "Your Booking has been Approved!";

        $shortcode = array(
            '[InvestmentBookingID]',
            '[Name]',
            '[CompanyName]',
            '[TotalShares]',
            '[TotalAmount]',
            '[LoginUrl]'
        );
        $scvalues = array(
            $bookdata->InvestmentBookingID, 
            $bookdata->Name,
            $bookdata->CompanyName,
            number_format($bookdata->SharesToApply),
            number_format($bookdata->TotalAmountAttached,2),
            View::url('bookings/info/'.$bookdata->InvestmentBookingID)
        );

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'bookings'.DS.'so_booking_approved.eml'));

        $root_path = APPPATH."emails".DS.'attachment'.DS;

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if( $this->send( $to, $name, $subject, $content, false, false, $bcc ) ){
            App::activityLog("Sent booking approved email letter.");
        }
        // End Email sending        
    }

    public function sendWithdrawalApprovalEmail( $wdata, $testEm = false )
    {
        $userinfo = User::info();
        $to = $testEm ? $testEm : $wdata->Email;
        $name = $testEm ? $userinfo->FirstName.' '.$userinfo->LastName : $wdata->ReferrerName;
        $subject = "Your Request for Commission Withdrawal has been Approved!";

        $shortcode = array(
            '[InvestmentBookingID]',
            '[Name]',
            '[CompanyName]',
            '[TotalAmount]',
            '[LoginUrl]'
        );

        $scvalues = array(
            $wdata->InvestmentBookingID,
            $wdata->ReferrerName,
            $wdata->CompanyName,
            number_format( $wdata->ReferralCommission, 2 ),
            View::url( 'bookings/info/'.$wdata->InvestmentBookingID )
        );

        $content = str_replace( $shortcode, $scvalues, App::getFileContents( 'emails'.DS.'users'.DS.'so_withdrawal_approved.eml' ) );

        $root_path = APPPATH."emails".DS.'attachment'.DS;

        //Admins
        $bcc = array();
        foreach( $this->getAdmins() as $admin ){
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if( $this->send( $to, $name, $subject, $content, false, false, $bcc ) ){
            App::activityLog( "Sent approval for withdrawal request email letter #EarningID-".$wdata->EarningID."." );
        }
        //End Email sending
    }

    public function sendWithdrawalPaymentEmail( $wdata, $testEm = false )
    {
        $userinfo = User::info();
        $to = $testEm ? $testEm : $wdata->Email;
        $name = $testEm ? $userinfo->FirstName.' '.$userinfo->LastName : $wdata->ReferrerName;
        $subject = "Your Commission Payment has been Deposited!";

        $shortcode = array(
            '[InvestmentBookingID]',
            '[Name]',
            '[CompanyName]',
            '[TotalAmount]',
            '[LoginUrl]'
        );

        $scvalues = array(
            $wdata->InvestmentBookingID,
            $wdata->ReferrerName,
            $wdata->CompanyName,
            number_format( $wdata->ReferralCommission, 2 ),
            View::url( 'bookings/info/'.$wdata->InvestmentBookingID )
        );

        $content = str_replace( $shortcode, $scvalues, App::getFileContents( 'emails'.DS.'users'.DS.'so_withdrawal_payment.eml' ) );

        $root_path = APPPATH."emails".DS.'attachment'.DS;

        //Admins
        $bcc = array();
        foreach( $this->getAdmins() as $admin ){
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if( $this->send( $to, $name, $subject, $content, false, false, $bcc ) ){
            App::activityLog( "Sent commission payment deposit email letter #EarningID-".$wdata->EarningID."." );
        }
        //End Email sending
    }

    public function sendBookingAknowledgeInvoiceEmail($invoiceID, $bookdata, $testEm=false)
    {
        $to = ($testEm=='test') ? User::info('Email') : $bookdata->ClientEmail;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $bookdata->ClientFirstName.' '.$bookdata->ClientLastName;
        $subject = "Invoice!";

        $shortcode = array(
            '[InvoiceID]',
            '[InvestmentBookingID]',
            '[ClientProfileID]',
            '[SharesToApply]',
            '[TotalAmountAttached]',
            '[Name]',
            '[CompanyName]',
            '[ClientAddress]',
            '[ClientAddress2]',
            '[ClientCity]',
            '[ClientState]',
            '[ClientCountry]',
            '[ClientPostal]',
            '[ClientPhone]',
            '[SubscriptionRate]',
            '[SOCommission]'

        );
        $scvalues = array(
            $invoiceID,
            $bookdata->InvestmentBookingID,
            $bookdata->ClientProfileID,
            number_format($bookdata->SharesToApply),
            number_format($bookdata->TotalAmountAttached,2),
            $bookdata->Name,
            $bookdata->CompanyName,
            ($bookdata->ClientAddress) ? $bookdata->ClientAddress : '-',
            ($bookdata->ClientAddress2) ? $bookdata->ClientAddress2 : '-',
            ($bookdata->ClientCity) ? $bookdata->ClientCity : '-',
            ($bookdata->ClientState) ? $bookdata->ClientState : '-',
            ($bookdata->ClientCountry) ? $bookdata->ClientCountry : '-',
            ($bookdata->ClientPostal) ? $bookdata->ClientPostal : '-',
            ($bookdata->ClientPhone) ? $bookdata->ClientPhone : '-',
            ($bookdata->SubscriptionRate) ? $bookdata->SubscriptionRate : '0.00',
            AppUtility::computeCommission( $bookdata->TotalAmountAttached, $bookdata->SubscriptionRate )

        );

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'bookings'.DS.'so_booking_invoice.eml'));

        $root_path = APPPATH."emails".DS.'attachment'.DS;

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if($this->send($to,$name,$subject,$content,false,false,$bcc)) {
            App::activityLog("Sent invoice email letter.");
        }
        // End Email sending        
    }

    public function sendBookingVerified($clientdata,$bookdata,$testEm=false)
    {
        // Start PDF
        $pdf = App::load()->model('pdfengine',true);
        // Commencement
        $bookingName = $bookdata->UserID.'_'.$bookdata->InvestmentBookingID.'_demandnote.pdf';
        $bookingPdf = $pdf->buildBookingDemandNote($bookdata->InvestmentBookingID,$bookingName,$bookdata);

        // End PDF


        $to = ($testEm=='test') ? User::info('Email') : $clientdata->Email;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $bookdata->Name;
        $subject = "Your booking has been verified!";

        $shortcode = array('[InvestmentBookingID]','[Name]');
        $scvalues = array($bookdata->InvestmentBookingID, $bookdata->Name);

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'bookings'.DS.'verified.eml'));

        $root_path = APPPATH."emails".DS.'attachment'.DS;
        $attachment =array(
            array('path' => $bookingPdf,'name' => $bookingName)
        );

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        if($this->send($to,$name,$subject,$content,$attachment,false,$bcc)) {
            App::activityLog("Sent verified booking email letter.");
        }
        // End Email sending
    }

    public function sendMTACron()
    {
        $sql = "SELECT * FROM `email_cron` WHERE `DateQueued` < DATE_SUB(NOW(),INTERVAL 30 MINUTE)";
        
        $query = &$this->db->prepare($sql);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_CLASS)){
            $attachment = $this->stringToArray($row);         
            $bcc = $this->stringToArray($row);         
            $this->send($row->MailTo,$row->MailToName,$row->Subject,$row->Content,$attachment,false,$bcc,$row->MailFrom,$row->MailFromName);

            $mailCron['EmailSent'] = 1;
            $mailCron['DateSent'] = date('Y-m-d H:i:s');
            $this->db->update("email_cron", $mailCron, array('EmailCronID'=>$row->EmailCronID));
        }
        
        unset($query);
        
        die();
    }

    public function sendAcknowledgement($caseID,$caseInfo,$testEm)
    {
        // Start PDF
        $cf = App::load()->model('casefiles',true,true);
            // Payments
        $paymentHtml = '';
        $paymentTotal = 0;
            $payments = $cf->getPayments($caseID);
            if(count($payments)) {
                foreach($payments as $payment) {
                    $paymentHtml .= '<tr>
                        <td class="text-center">'.$payment->PaymentDate.'</td>
                        <td class="text-center">'.$payment->PaymentType.'</td>
                        <td class="text-center">'.$payment->Description.'</td>
                        <td class="text-center">'.App::currency($payment->Amount).'</td>
                        <td class="text-center">'.$payment->Remarks.'</td>
                    </tr>';
                    
                    $paymentTotal += $payment->Amount;
                }
            }
        // End Payments

        // Start Email sending
        $shortcode = array(
            '[DateNow]',
            '[AgreementYear]',
            '[AgreementMonth]',
            '[AgreementDay]',
            '[DemandNoteNo]',
            '[CommencementDate]',
            '[Salutation]',
            '[FirstName]',
            '[LastName]',
            '[Address]',
            '[City]',
            '[State]',
            '[ReceivedAmount]',
            '[PaymentReceived]',
            '[MaturityDate]',
            '[DepositedAmount]',
            '[ProductName]',
            '[InvestmentAmount]',
            '[TenureMonths]',
            '[AnnualInterest]',
            '[TotalReturn]',
            '[Signature]',
            '[Payments]',
            '[TotalAmount]'
        );

        $POADirectorSign = ($caseInfo->POADirectorSign) ? App::outputUploadedFile($caseInfo->POADirectorSign,$atts) : '';
        $IASpecimensign = ($caseInfo->IASpecimensign) ? App::outputUploadedFile($caseInfo->IASpecimensign,$atts) : '';        

        $UBOSign = ($caseInfo->AccountType == 'Individual') ? $IASpecimensign : $POADirectorSign;

        $scvalues = array(
            date('d-m-Y'),
            date('Y'),
            date('F'),
            date('d'),
            'GA-DM-'. strtoupper(substr(md5(microtime(false)), 0, 10)),
            App::date($caseInfo->CommencementDate),
            $caseInfo->Salutation,
            $caseInfo->FirstName,
            $caseInfo->LastName,
            $caseInfo->Address,
            $caseInfo->City,
            $caseInfo->State,
            App::currency($caseInfo->ReceivedAmount),
            App::date($caseInfo->PaymentReceived),
            App::date($caseInfo->MaturityDate),
            App::currency($caseInfo->DepositedAmount),
            $caseInfo->ProductName,
            App::currency($caseInfo->InvestmentAmount),
            $caseInfo->TenureMonths,
            $caseInfo->AnnualInterest,
            App::currency( ((($caseInfo->AnnualInterest / 100) * $caseInfo->DepositedAmount) * ($caseInfo->TenureMonths/12)) + $caseInfo->DepositedAmount),
            $UBOSign,
            (count($payments)) ? $paymentHtml : '<tr><td colspan="99">No Payments</td></tr>',
            App::currency($paymentTotal)
        );
        
        

        $to = ($testEm=='test') ? User::info('Email') : $caseInfo->AccountEmail;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $caseInfo->FirstName .' '.$caseInfo->LastName;
        $subject = "Global Asset Funding Acknowledgement Receipt.";

        $emailFormat = 'ga_term_zh.eml';
        switch($caseInfo->FormType) {
            case 'Traditional': 
                $emailFormat = 'ga_term_zh.eml';
            break;
            case 'Simplified': 
                $emailFormat = 'ga_term_cn.eml';
            break;
        }

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'ga_term.eml'));
        // Agent
        $ref = User::info(false,$caseInfo->ReferrerUserID);
        $rto = ($testEm=='test') ? User::info('Email') : $ref->Email;
        $rname = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $ref->FirstName .' '.$ref->LastName;
        $cc = array(
           array('name'=>$rname,'email'=>$rto)
        );
        
        $bcc[$ref->UserID]['email'] = $rto;
        $bcc[$ref->UserID]['name'] = $rname;

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }
        
        $this->send($to,$name,$subject,$content,false,$cc,$bcc);
        // End Email sending

        App::activityLog("Sent acknowledment receipt email for casefile #GA-CF-".$caseID.'.');
    }  

    public function sendReceivedNote($caseID,$caseInfo,$testEm)
    {
        // Start PDF
            /* $pdf = App::load()->model('pdfengine',true);            
            // Traditional
            $traditionalName = $caseInfo->UserID.'_'.$caseID.'_traditional.pdf';
            switch($caseInfo->FormType) {
                case 'Traditional': 
                    $traditionalName = $caseInfo->UserID.'_'.$caseID.'_traditional.pdf';
                break;
                case 'Simplified': 
                    $traditionalName = $caseInfo->UserID.'_'.$caseID.'_simplified.pdf';
                break;
            }
            
            $traditionalPdf = $pdf->buildForm($caseID,$traditionalName,$caseInfo); */
        // End PDF
        $attachment = false;
        // Start Email sending            
        $shortcode = array( 
            '[Salutation]',               
            '[FirstName]',
            '[LastName]'
        );
        
        $scvalues = array(
            $caseInfo->Salutation,
            $caseInfo->FirstName,
            $caseInfo->LastName
        );
        
        // Client
        $to = ($testEm=='test') ? User::info('Email') : $caseInfo->AccountEmail;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $caseInfo->FirstName .' '.$caseInfo->LastName;
        $subject = "Your subscription for Global Asset was successful!";

        $emailFormat = 'ga_subscription_zh.eml';
        switch($caseInfo->FormType) {
            case 'Traditional': 
                $emailFormat = 'ga_subscription_zh.eml';
            break;
            case 'Simplified': 
                $emailFormat = 'ga_subscription_cn.eml';
            break;
        }

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.$emailFormat));
        // $attachment =array(
        //     array('path' => $traditionalPdf,'name' => $traditionalName)
        // );            
        
        // Agent
        // $ref = User::info(false,$caseInfo->ReferrerUserID);
        // $rto = ($testEm=='test') ? User::info('Email') : $ref->Email;
        // $rname = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $ref->FirstName .' '.$ref->LastName;
        // $cc = array(
        //     array('name'=>$rname,'email'=>$rto)
        // );
        
        $bcc[$ref->UserID]['email'] = $rto;
        $bcc[$ref->UserID]['name'] = $rname;

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send($to,$name,$subject,$content,$attachment,false,$bcc);
        // End Email sending
    }  

    public function sendRejectFileNotice($caseID,$caseInfo,$testEm)
    {
        $rdata = $this->post['reject'];
        // Start Email sending            
        $shortcode = array(                
            '[FirstName]',
            '[LastName]',
            '[FileName]',
            '[Reasons]',
            '[Notes]',
        );
        
        $scvalues = array(
            $caseInfo->FirstName,
            $caseInfo->LastName,
            $rdata['filename'],
            implode(',',$rdata['reasons']),
            $rdata['notes']
        );
        
        // Client
        $to = ($testEm=='test') ? User::info('Email') : $caseInfo->AccountEmail;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $caseInfo->FirstName .' '.$caseInfo->LastName;
        $subject = "Your file has been rejected!";
        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'rejectnote.eml'));
        
        $this->send($to,$name,$subject,$content);

         // Agent
        $ref = User::info(false,$caseInfo->ReferrerUserID);
        $rto = ($testEm=='test') ? User::info('Email') : $ref->Email;
        $rname = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $ref->FirstName .' '.$ref->LastName;
//            $cc = array(
//                    array('name'=>$rname,'email'=>$rto)
//                );
        
        $bcc[$ref->UserID]['email'] = $rto;
        $bcc[$ref->UserID]['name'] = $rname;

        // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send($to,$name,$subject,$content,$attachment,false,$bcc);
        // End Email sending

        App::activityLog("Sent reject notice email for casefile #GA-CF-".$caseID.' and file '.$rdata['filename']);
    }  
    
    public function sendApproveNote($caseID,$caseInfo)
    {
        // Start Email sending            
            $shortcode = array(                
                '[CaseID]',
                '[UserCompleteInfo]'
            );
            
            $scvalues = array(
                $caseID,
                User::info('UserID').': '.User::info('FirstName').' '.User::info('LastName')                
            );
            
            // Sender
            $to = User::info('Email');
            $name = User::info('FirstName').' '.User::info('LastName');
            $subject = "GA casefile has been approved!";
            $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'adminapprovenote.eml'));
            
            $this->send($to,$name,$subject,$content);

            // Agent
            $ref = User::info(false,$caseInfo->ReferrerUserID);
            $rto = $ref->Email;
            $rname = $ref->FirstName .' '.$ref->LastName;
            $cc = array(
                array('name'=>$rname,'email'=>$rto)
            );
            
            // Admins
            $bcc = array();
            foreach($this->getAdmins() as $admin) {
                $bcc[$admin->UserID]['email'] = $admin->Email;
                $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
            }

            $this->send($to,$name,$subject,$content,false,$cc,$bcc);
        // End Email sending
    } 

    public function sendUserRegistration($userID,$userInfo)
    {

        // Start Email sending            
        $shortcode = array( 
            '[Salutation]',               
            '[FirstName]',
            '[LastName]',
            '[LevelName]',
            '[UserID]',
            '[Email]',
            '[Password]',
            '[LoginURL]'
        );
        
        $scvalues = array(
            $userInfo->Salutation,
            $userInfo->FirstName,
            $userInfo->LastName,
            Level::info( 'Name',$userInfo->Level ),
            $userInfo->UserID,
            $userInfo->Email,
            AppUtility::decodeHash( $userInfo->HashKey ),
            // base64_decode( $userInfo->HashKey ),
            View::url('users/login')
        );
        
        // Client
        $to = $userInfo->Email;
        $name = $userInfo->FirstName .' '.$userInfo->LastName;
        $subject = "Welcome to SeedOmega!";

        $agencyID = AppUtility::getAgencyOf( $userID );                
        $ag = AppUtility::getAgencyInfo( ( $agencyID ? $agencyID : $userID ) );

        $emailFormat = 'so_user_signup.eml';

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails/users'.DS.$emailFormat));
        
        // Admins
        $bcc = array();
        foreach( $this->getAdmins() as $admin ){
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send($to,$name,$subject,$content,false,false,$bcc);
        // End Email sending
    }

    public function sendLostPasswordEmail($uinfo)
    {
        $userInfo = User::info(false,$uinfo->UserID);

        // Start Email sending            
        $shortcode = array( 
            '[Salutation]',               
            '[FirstName]',
            '[LastName]',
            '[ResetLink]'
        );
        
        $scvalues = array(
            $userInfo->Salutation,
            $userInfo->FirstName,
            $userInfo->LastName,
            View::url('users/resetpassword/'.$userInfo->ResetKey)
        );
        
        // Client
        $to = $userInfo->Email;
        $name = $userInfo->FirstName .' '.$userInfo->LastName;
        $subject = "Request Password Reset to SeedOmega!";

        $emailFormat = 'users/so_resetpassword.eml';

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.$emailFormat));
        
        // // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send($to,$name,$subject,$content,false,false,$bcc);
        // End Email sending
    }

    public function sendUserProfileApproved($userID,$userInfo)
    {

        // Start Email sending            
        $shortcode = array( 
            '[Salutation]',               
            '[FirstName]',
            '[LastName]',
            '[LevelName]',
            '[UserID]',
            '[LoginURL]'
        );
        
        $scvalues = array(
            $userInfo->Salutation,
            $userInfo->FirstName,
            $userInfo->LastName,
            Level::info('Name',$userInfo->Level),
            $userInfo->UserID,
            View::url('users/login')
        );
        
        // Client
        $to = $userInfo->Email;
        $name = $userInfo->FirstName .' '.$userInfo->LastName;
        $subject = "SeedOmega: Your Investor Profile has Been Approved !";

        $emailFormat = 'so_investor_profile_approved.eml';

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails/users'.DS.$emailFormat));
        
        // // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send($to,$name,$subject,$content,false,false,$bcc);
        // End Email sending
    }

    public function sendNewInquiry($inquiryID,$inquiryData)
    {
        // Start Email sending            
        $shortcode = array( 
            '[InquiryID]',
            '[FullName]',               
            '[Email]',
            '[Phonenumber]',
            '[Subject]',
            '[Message]',
            '[LoginURL]'
        );
        
        $scvalues = array(
            $inquiryData->InquiryID,
            $inquiryData->FullName,
            $inquiryData->Email,
            $inquiryData->Phonenumber,
            $inquiryData->Subject,
            $inquiryData->Message,
            View::url( 'contactus/view/'.$inquiryData->ViewKey )
        );
        
        // Client
        $to = $inquiryData->Email;
        $name = $inquiryData->FullName;
        $subject = "SeedOmega Inquiry : # ".$inquiryData->InquiryID." - ".$inquiryData->FullName;

        $emailFormat = 'so_contactus.eml';

        $content = str_replace( $shortcode, $scvalues, App::getFileContents( 'emails'.DS.$emailFormat ) );
        
        // // Admins
        $bcc = array();
        foreach($this->getAdmins() as $admin) {
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send($to,$name,$subject,$content,false,false,$bcc);
        // End Email sending
    }

    public function sendReplyInquiry( $inquiryData )
    {
        // start email sending
        $shortcode = array( 
            '[InquiryID]',
            '[FullName]',
            '[Email]',
            '[Phonenumber]',
            '[Subject]',
            '[Message]',
            '[Reply]',
            '[LoginURL]'
        );
        
        $scvalues = array(
            $inquiryData->InquiryID,
            $inquiryData->FullName,
            $inquiryData->Email,
            $inquiryData->Phonenumber,
            $inquiryData->Subject,
            $inquiryData->Message,
            $inquiryData->Reply,
            View::url( 'contactus/view/'.$inquiryData->ViewKey )
        );
        
        // Client
        $to = $inquiryData->Email;
        $name = $inquiryData->FullName;
        $subject = "SeedOmega Inquiry - Reply : # ".$inquiryData->InquiryID." - ".$inquiryData->FullName;

        $emailFormat = 'so_contactus_reply.eml';

        $content = str_replace( $shortcode, $scvalues, App::getFileContents( 'emails'.DS.$emailFormat ) );
        
        // admins
        $bcc = array();
        foreach( $this->getAdmins() as $admin ){
            $bcc[$admin->UserID]['email'] = $admin->Email;
            $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        }

        $this->send( $to, $name, $subject, $content, false, false, $bcc );
        // End Email sending
    }

    public function sendStatementOfAccount($userID, $datevalue, $testEm=false)
    {
        $userinfo = User::info(false, $userID);

        if($datevalue) {
            $dates = explode(' - ',$datevalue);
            $datefrom = date( 'M Y', strtotime($dates[0]) );
            $dateto = date( 'M Y', strtotime($dates[1]) );
        }

        // Start PDF
        $pdf = App::load()->model('pdfengine',true);
        $soaName = $userinfo->UserID.'_'.$userinfo->FirstName.'_'.$userinfo->LastName.'_soa.pdf';
        $soaPDF = $pdf->buildStatementOfAccount($userID, $datevalue, $soaName);
        // End PDF

        $to = ($testEm=='test') ? User::info('Email') : $userinfo->Email;
        $name = ($testEm=='test') ? User::info('FirstName').' '.User::info('LastName') : $userinfo->FirstName.' '.$userinfo->LastName;
        $subject = "Monthly Statement Of Account!";

        $shortcode = array(
            '[UserID]',
            '[FirstName]',
            '[LastName]',
            '[DateRange]',
            '[URL]'
        );
        $scvalues = array(
            $userinfo->UserID, 
            $userinfo->FirstName,
            $userinfo->LastName,
            ($datevalue) ? $datefrom.' - '.$dateto : '-',
            View::url('')
        );

        $content = str_replace($shortcode,$scvalues,App::getFileContents('emails'.DS.'wallet'.DS.'so_wallet_statement.eml'));

        $root_path = APPPATH."emails".DS.'attachment'.DS;
        $attachment =array(
            array('path' => $soaPDF,'name' => $soaName)
        );  

        // Admins
        $bcc = array();
        // foreach($this->getAdmins() as $admin) {
        //     $bcc[$admin->UserID]['email'] = $admin->Email;
        //     $bcc[$admin->UserID]['name'] = $admin->FirstName.' '.$admin->LastName;
        // }

        $this->send($to,$name,$subject,$content,$attachment,false,$bcc);
        //$this->send($to,$name,$subject,$content,false,false,$bcc);
        // End Email sending        
    }

    public function sendTest($to,$name,$subject,$content)
    {

        if($this->send($to,$name,$subject,$content)) {
            echo 'Good';
        }
    } 
}