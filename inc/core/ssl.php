<?php  
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location    /sys/core/ssl.php
 * @package     sys
 * @version     MyPHPFrame v3.0.0
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */


class SSL
{
    function __construct($path=false,$echo=false)
    {
        $serv 	= isset($_SERVER['HTTP_CF_VISITOR']) ? json_decode($_SERVER['HTTP_CF_VISITOR']) : false;
        $proto 	= ($serv) ? $serv->scheme : $_SERVER['REQUEST_SCHEME'];
    	if(Config::get('SITE_FORCE_HTTPS') == 'true' && $proto == "http") {
            header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
            exit;
        }
        if(Config::get('SITE_MAINTENANCE') == 'true') {
            $this->addDevelopmentLogin();
        }
    }

    function addDevelopmentLogin() 
    {
        $domain = $_SERVER['SERVER_NAME'];
        $serv 	= isset($_SERVER['HTTP_CF_VISITOR']) ? json_decode($_SERVER['HTTP_CF_VISITOR']) : false;
        $proto 	= ($serv) ? $serv->scheme : $_SERVER['REQUEST_SCHEME'];        
        if(isset($_POST['theusername']) && isset($_POST['thepassword']) && $_POST['theusername']=='seedomega' && strtoupper(md5($_POST['thepassword']))=='B6CF3B1EF6D2B797ABDC4E189A4D9CFF') {
            setcookie('siteloggedin','true',time()+ (30 * 24 * 3600),'/',$domain);
            header('location: '.$proto.'://'.$domain);
        }
        if(isset($_COOKIE['siteloggedin']) && $_COOKIE['siteloggedin'] == 'true') {             
        } else {
            echo '<div style="display: table; width: 300px; margin: 50px auto;padding: 20px;border: 1px solid #19a9b2; text-align:center;font-family:arial;"><img src="'.$proto.'://seedomega.com/app/default/views/assets/images/omegalogo.png" /><h1>Coming Soon!</h1><h3>Please login below:</h3><form method="post" action="">
            <input type="text" name="theusername" value="" placeholder="Username" style="width: 100%;padding: 10px;margin: 0 0 10px 0;"><br>
            <input type="password" name="thepassword" value="" placeholder="Password" style="width: 100%;padding: 10px;margin: 0 0 10px 0;"><br>
            <input type="submit" value="Login" style="width: 100px; margin: auto;padding: 10px;display:block;">
            </form></div>';
            exit;
        }
    }
}

new SSL();