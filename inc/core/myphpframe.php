<?php 
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location      /sys/core/MyPHPFrame.php
 * @package       sys
 * @version	      MyPHPFrame v3.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Start php session
session_start();

/**
 *  Define version
 */
define('APPNAME', 'MyPHPFrame');
define('APPVERSION', '3.0.0');

/**
 *  Define constants for directories
 */
define('APPDIR',  'app');
define('DS', DIRECTORY_SEPARATOR);
define('RDS', '/');
define('DOT', '.');
define('APPROOT',  INSTALLDIR . DS . APPDIR . DS);
define('CONFPATH', ROOTDIR . DS . 'conf' . DS);
define('COREPATH', ROOTDIR . DS . 'core' . DS);
define('DBSEPATH', ROOTDIR . DS . 'database' . DS);
define('FRMEPATH', ROOTDIR . DS . 'frame' . DS);
define('SQLPATH', ROOTDIR . DS . 'sql' . DS);

/**
 *  Include core class
 */
require_once COREPATH.'includes.php';

if(!class_exists('MyPHPFrame'))
{
    class MyPHPFrame extends Common
    {
        public $is_connected;
        public $is_reporting;

        function __construct()	
        {
            // Inherit parent construct
            parent::__construct();

            // @return 	true if success or null
            $this->is_reporting = Config::errorReporting();
            $this->pageRouter();	
        }
    }
}

/**
 * Load MyPHPFrame environment
 */
MyPHPFrame::initialize();