<?php
/**
 *  Include configuration class
 */
require_once CONFPATH.'configuration.php';

/**
 *  Include password functions
 */
require_once COREPATH.'ssl.php';

/**
 *  Include password functions
 */
require_once FRMEPATH.'password.php';   

/**
 *  Include database class
 */
require_once DBSEPATH.'db.php'; 

/**
 *  Include common class
 */
require_once COREPATH.'common.php'; 

/**
 *  Include option
 */
require_once FRMEPATH.'option.php';

/**
 *  Include loader
 */
require_once COREPATH.'loader.php'; 

/**
 *  Include controller
 */
require_once COREPATH.'controller.php';

/**
 *  Include model
 */
require_once COREPATH.'model.php';

/**
 *  Include menu
 */
require_once FRMEPATH.'menu.php';

/**
 *  Include assets
 */
require_once FRMEPATH.'assets.php';

/**
 *  Include view
 */
require_once FRMEPATH.'view.php';   

/**
 *  Include level
 */
require_once FRMEPATH.'level.php';

/**
 *  Include user
 */
require_once FRMEPATH.'user.php';

/**
 *  Include app
 */
require_once FRMEPATH.'app.php';

/**
 *  Include language
 */
require_once FRMEPATH.'language.php';