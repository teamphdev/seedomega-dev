<?php
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location      conf/database.php
 * @package       configuration
 * @version		  MyPHPFrame v3.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 
class Database 
{
    const development 	= 'development';
    const staging       = 'staging';
    const production 	= 'production';
    
    var $environment;
    var $host;
    var $user;
    var $pass;
    var $database;
    var $prefix;
    var $encoding;
    
    function __construct($environment='development') {
        //$this->environment = $env;
        //$db = $this->info();

        $env = strtoupper($environment);
        
        $this->host = Config::get($env.'_HOST','localhost');
        $this->user = Config::get($env.'_USER','test');
        $this->pass = Config::get($env.'_PASS','test');
        $this->database = Config::get($env.'_NAME','mpf');
        $this->prefix = Config::get($env.'_PREFIX','');
        $this->encoding = Config::get($env.'_ENCODING','utf8');        
    }
}