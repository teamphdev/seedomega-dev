<?php  
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location    /sys/core/install.php
 * @package     sys | Library | Intended for builtin utility methods
 * @version     MyPHPFrame v3.0.0
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

class Install
{
    static $views;
    static $language;
    static $emails;
    static $controllers;
    static $models;
    static $vendors;
    
    public static function init() 
    {        
        self::$views = APPPATH.'views'.DS;
        self::$language = APPPATH.'language'.DS;
        self::$emails = APPPATH.'emails'.DS;
        self::$controllers = APPPATH.'controllers'.DS;
        self::$models = APPPATH.'models'.DS;
        self::$vendors = APPPATH.'vendors'.DS;

        self::mpfNotInstalled();
    }    

    public static function load($filename = NULL, $init = false)
    {
        // self::fetch(APPPATH.$filename.DOT.Config::get('FILE_EXT'), 'ro');
        // if($init) {
        //     return new $filename();
        // }
        if(class_exists('Loader'))
        {
            return new Loader();        
        }
    }   

    /**
     * Redirect to the installer if MyPHPFrame is not installed.
     *
     * Dies with an error message.
     *
     * @access private
     */
    function mpfNotInstalled(){
        if( !self::isMPFInstalled() && !self::mpfInstalling() ){
            // nocache_headers();
            die( 'The site you have requested is not installed properly. Please contact the system administrator.' );
        }
    }

    /**
     * Check or set whether MyPHPFrame is in "installation" mode.
     *
     * If the `MPFINSTALLING` constant is defined during the bootstrap, `mpfInstalling()` will default to `true`.
     *
     * @staticvar bool $installing
     *
     * @param bool $is_installing Optional. True to set MPF into Installing mode, false to turn Installing mode off.
     *                            Omit this parameter if you only want to fetch the current status.
     * @return bool True if WP is installing, otherwise false. When a `$is_installing` is passed, the function will
     *              report whether MPF was in installing mode prior to the change to `$is_installing`.
     */
    function mpfInstalling( $is_installing = null ){
        static $installing = null;

        // Support for the `MPFINSTALLING` constant, defined before MPF is loaded.
        if( is_null( $installing ) ){
            $installing = defined( 'MPFINSTALLING' ) && MPFINSTALLING;
        }

        if( !is_null( $is_installing ) ){
            $old_installing = $installing;
            $installing = $is_installing;
            return (bool) $old_installing;
        }

        return (bool) $installing;
    }    

    /**
     * Test whether MyPHPFrame is already installed.
     *
     * The cache will be checked first. If you have a cache plugin, which saves
     * the cache values, then this will work. If you use the default MyPHPFrame
     * cache, and the database goes away, then you might have problems.
     *
     * Checks for the 'siteurl' option for whether MyPHPFrame is installed.
     *
     * @global db $db MyPHPFrame database abstraction object.
     *
     * @return bool Whether the site is already installed.
     */
    function isMPFInstalled(){
        $dbInfo = new Database( Config::get('ENVIRONMENT') );

        if( $dbInfo->database != 'mpf' ) return true;

        return false;
    }

    function createMPFCredentials(){
        ;
    }
}

Install::init();