<?php  
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location    /sys/core/menu.php
 * @package     sys
 * @version     MyPHPFrame v3.0.0
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
class Menu { 
    
    public static $segment;
    public static $uri;
    public static $menus;
    public static $default;
    public static $menuWrapper;

    static public function init() 
    {
        $common             = new Common();
        $segment            = ($common->segment) ? $common->segment : array();

        self::$segment      = $segment; 
        self::$uri          = implode('/',$segment);
        self::$menus        = array();
        self::$menuWrapper  = 'ul';
        self::$default      = array(
                                    'Tag'                   => 'li',
                                    'Id'                    => '',
                                    'Class'                 => '',
                                    'Slug'                  => '#',
                                    'LinkId'                => '',
                                    'LinkClass'             => '',            
                                    'Label'                 => '',            
                                    'Children'              => array(),
                                    'ChildrenMenuWrapper'   => 'ul',
                                    'Segment0'              => array(),
                                    'Segment1'              => array(),
                                    'Segment2'              => array()
                                    );
    }

    static public function sanitize($array = array()) 
    {
        $newArray = array();
        foreach ( self::$default as $key => $value) {
            $newArray[$key] = isset($array[$key]) ? $array[$key] : $value;
        }
        return $newArray;
    }

    static public function item( $ID = 'new', $args = array() ) 
    {
        $menuID = strtolower($ID);
        self::$menus[$menuID] = (object) self::sanitize($args);
    }

    static public function items( $array = array() ) 
    {
        if(count($array)) {
            foreach($array as $ID => $args) {
                self::item($ID, $args);
            }
        }
    }

    static public function subitem( $parentID = 'new', $ID = 'newsub', $args = array() ) 
    {
        $prntID = strtolower($parentID);
        $menuID = strtolower($ID);
        self::$menus[$prntID]->Children[$menuID] = (object) self::sanitize($args);
    }

    static public function subitems( $parentID = 'new' , $array = array() ) 
    {
        if(count($array)) {
            foreach($array as $ID => $args) {
                self::item($parentID, $ID, $args);
            }
        }
    }

    static public function subsubitem( $grandID = 'new', $parentID = 'new', $ID = 'newsub', $args = array() ) 
    {
        $grndID = strtolower($grandID);
        $prntID = strtolower($parentID);
        $menuID = strtolower($ID);

        self::$menus[$grndID]->Children[$prntID]->Children[$menuID] = (object) self::sanitize($args);
    }

    static public function subsubitems( $grandID = 'new', $parentID = 'new' , $array = array() ) 
    {
        if(count($array)) {
            foreach($array as $ID => $args) {
                self::item($grandID, $parentID, $ID, $args);
            }
        }
    }

    static public function render() 
    {
        $output = '';

        if( count(self::$menus) ) {

            // Loop menu items
            foreach( self::$menus as $id => $nav ) {
                $output .= '<'.$nav->Tag.' id="'. ( ($nav->Id) ? $nav->Id : $id ).'" class="'.$nav->Class.' '. (
                    (isset(self::$segment[0]) && self::$segment[0] == $nav->Slug) || self::$uri == $nav->Slug || (isset(self::$segment[0]) && in_array(self::$segment[0],$nav->Segment0)) || (isset(self::$segment[1]) && in_array(self::$segment[1],$nav->Segment1)) || (isset(self::$segment[2]) && in_array(self::$segment[2],$nav->Segment2)) ? 'active' : '') .'">';
                    $output .= '<a href="'. View::url( $nav->Slug ) .'" id="'.$nav->LinkId.'" class="'.$nav->LinkClass.'">'. $nav->Label .'</a>';

                    // Start sub menu
                    if( count($nav->Children) ) {
                        $output .= '<'.$nav->ChildrenMenuWrapper.'>';                    
                        foreach( $nav->Children as $ids => $subnav ) {
                            $output .= '<'.$subnav->Tag.' id="'. ( ($subnav->Id) ? $subnav->Id : $ids ).'" class="'.$subnav->Class.' '. (
                                (isset(self::$segment[1]) && self::$segment[1] == $subnav->Slug) || self::$uri == $subnav->Slug ? 'active' : '') .'">';
                                $output .= '<a href="'. View::url( $subnav->Slug ) .'" id="'.$subnav->LinkId.'" class="'.$subnav->LinkClass.'">'. 
                                                $subnav->Label .'</a>';

                                // Start sub sub menu
                                if( count($subnav->Children) ) {
                                    $output .= '<'.$subnav->ChildrenMenuWrapper.'>';                    
                                    foreach( $subnav->Children as $idss => $subsubnav ) {
                                        $output .= '<'.$subsubnav->Tag.' id="'. ( ($subsubnav->Id) ? $subsubnav->Id : $idss ).'" class="'.$subsubnav->Class.' '. ( (isset(self::$segment[1]) && self::$segment[1] == $subsubnav->Slug) || self::$uri == $subsubnav->Slug ? 'active' : '') .'">';
                                            $output .= '<a href="'. View::url( $subsubnav->Slug ) .'" id="'.$subsubnav->LinkId.'" class="'.$subsubnav->LinkClass.'">'. 
                                                            $subsubnav->Label .'</a>';
                                        $output .= '</'.$subsubnav->Tag.'>';
                                    }
                                    $output .= '</'.$nav->ChildrenMenuWrapper.'>';
                                }

                            $output .= '</'.$subnav->Tag.'>';
                        }
                        $output .= '</'.$nav->ChildrenMenuWrapper.'>';
                    }

                $output .= '</'.$nav->Tag.'>';
            }

        }

        return $output;
    }

    static public function output() 
    {
        return self::render();
    }
}

Menu::init();
?>