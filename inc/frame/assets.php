<?php  
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location    /sys/core/asset.php
 * @package     sys
 * @version     MyPHPFrame v3.0.0
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

require_once FRMEPATH.'css.php';
require_once FRMEPATH.'js.php';

class Assets { 
    
    static public function init() 
    {
        JS::init();
        CSS::init();
    }

    static public function inline( $identifier = '#MPF_general_rule', $rule = '' )
    {
        CSS::customRule( $identifier, $rule );
    }

	static public function js( $ID, $path = 'none.js', $inFooter = false )
    {
        JS::register( $ID, $path, $inFooter );
    }    

    static public function jsBulk( $array = array() )
    {
        JS::registers( $array );
    } 

    static public function css( $ID, $path = 'none.css', $inFooter = false )
    {
        CSS::register( $ID, $path, $inFooter );
    }

    static public function cssBulk( $array = array() )
    {
        CSS::registers( $array );
    }

    static public function enqueue( $type = 'js', $idArray = array(), $inFooter = false )
    {
    	switch(strtolower($type)) {
    		case 'css':
    			CSS::enqueue( $idArray, $inFooter );
    		break;
    		case 'js':
    		default:
    			JS::enqueue( $idArray, $inFooter );
    		break;
    	}
    }

    static public function clear( $type = 'js', $loc = 'all' ) 
    {
        switch(strtolower($type)) {
    		case 'css':
    			CSS::clear( $loc );
    		break;
    		case 'js':
    		default:
    			JS::clear( $loc );
    		break;
    	}
    }

    static public function clearList( $type = 'js', $loc = 'all' ) 
    {
        switch(strtolower($type)) {
    		case 'css':
    			CSS::clearEnqueue( $loc );
    		break;
    		case 'js':
    		default:
    			JS::clearEnqueue( $loc );
    		break;
    	}
    } 

    public static function header()
    {
        CSS::headerOut();
        JS::headerOut();
    }
    
    public static function footer()
    {        
        CSS::footerOut();
        JS::footerOut();
    }

}

Assets::init();
?>