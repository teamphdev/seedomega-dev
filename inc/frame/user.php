<?php
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location      conf/user.php
 * @package       core
 * @version       MyPHPFrame v3.0.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 
class User
{
    /**
     * Get user information
     * 
     * @access public
     * @static
     *
     * @return string or object
     */
    public static function info($key = false, $id = false)
    {
        $return = false;
        $common = new Common();
        
        if( $id ){
            $where = "WHERE CONCAT(ul.Code,u.UserID) = '$id' LIMIT 1";
            if( ctype_digit($id) ){
                $where = "WHERE u.UserID = '".$id."' LIMIT 1";
            }

            $sql = "SELECT u.*, um.*, ul.Name as UserLevel, ul.Code, ul.Link as DashboardURL
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON ul.UserLevelID = u.Level ".$where;
            
            $userdata = $common->db->get_row($sql);
            $userinfo = ($userdata) ? (array) $userdata : array();
        } else {
            $userinfo = $common->getSession('userdata');
        }
        
        if($key != false) {
            $return = isset($userinfo[$key]) ? $userinfo[$key] : false;
        } else {
            $return = ($userinfo) ? (object) $userinfo : array();
        }
        
        return $return;
    }
    
    /**
     * Get user information by email
     * 
     * @access public
     * @static
     *
     * @return string or object
     */
    public static function infoByEmail($key = false, $email = false)
    {
        $return = false;
        $common = new Common();
        
        if($email) {
            $sql = "SELECT u.*, um.*, ul.Name as UserLevel, ul.Code, ul.Link as DashboardURL 
            FROM users u 
            LEFT JOIN user_meta um ON um.UserID = u.UserID 
            LEFT JOIN user_levels ul ON ul.UserLevelID = u.Level 
            WHERE u.Email = '".$email."' LIMIT 1";
            
            $userdata = $common->db->get_row($sql);
            $userinfo = ($userdata) ? (array) $userdata : array();
        } else {
            $userinfo = $common->getSession('userdata');
        }
        
        if($key != false) {
            $return = isset($userinfo[$key]) ? $userinfo[$key] : false;
        } else {
            $return = (object) $userinfo;
        }
        
        return $return;
    }

    /**
     * Get user level
     * 
     * @access public
     * @static
     *
     * @return string or object
     */
    public static function role()
    {        
        return self::info('UserLevel');
    }

    /**
     * Get user level
     * 
     * @access public
     * @static
     *
     * @return string or object
     */
    public static function isLoggedIn()
    {        
        return self::info('UserLevel') ? true : false;
    }
    
    /**
     * Get user capability
     * 
     * @access public
     * @static
     *
     * @return array
     */
    public static function capabilities($id=false)
    {
        $return = false;
        $common = new Common();        
        $userinfo = self::info('Capability',$id);               
        $return = $common->stringToArray($userinfo); 
        
        return $return;
    }
    
    /**
     * Check if user has capability
     * 
     * @access public
     * @static
     *
     * @return array
     */
    public static function can($capability=false,$id =false)
    {
        $return = false;
        $capa = (array) self::capabilities($id);
        
        if($capability) {
            if(in_array($capability,$capa)) {
                $return = true;
            } else {
                $return = false;
            }
        }
        
        return $return;
    }
    
    /**
     * Check user role
     * 
     * @access public
     * @static
     *
     * @return boolean
     */
    public static function is($is=false,$id =false)
    {
        $return = false;
        $level = self::info('UserLevel',$id);

        if($level == $is) {
            $return = true;
        }
        
        return $return;
    }

    /**
     * Get user dashboard link
     * 
     * @access public
     * @static
     *
     * @return string
     */
    public static function dashboardLink( $urionly = false )
    {
        $return         = false;
        $level          = self::info('Level');
        $individualURL  = self::info('DashboardSlug');
        $userroleURL    = Level::info('Link',$level);
        
        $theUrl         = ($individualURL) ? $individualURL : $userroleURL;

        $return         = ($urionly) ? $theUrl : View::url($theUrl);
        
        return $return;
    }
    
    /**
     * Redirect user to his dashboard
     * 
     * @access public
     * @static
     *
     * @return void
     */
    public static function dashboardRedirect()
    {   
        View::redirect( self::dashboardLink(true), true );
    }
    
    /**
     * Get / Set cookie
     * 
     * @access public
     * @static
     *
     * @return void
     */
    public static function cookie( $name, $value = false)
    {
        $return = false;

        if( isset( $value ) ){
            setcookie($name, $value, 0, "/"); // 0: valid until the browser is closed.
        }
        
        if( isset( $_COOKIE[$name] ) ){ $return = $_COOKIE[$name]; }
        
        return $return;
    }
}