<?php  
/**
 * PHP 5++
 *
 * MyPHPFrame(tm) : Rapid Development Framework
 * Copyright (c) CuteArts Web Solutions (http://cutearts.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @location    /sys/core/view.php
 * @package     sys
 * @version     MyPHPFrame v3.0.0
 * @license     MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
class View
{
    public static $scripts = array();
    public static $styles = array();
    public static $footerscripts = array();
    public static $footerstyles = array();
    public static $iescripts = array();
    public static $iestyles = array();
    public static $title = '';
    public static $ptitle = '';
    public static $segments = array();
    public static $bodyclass = '';
    public static $viewpageslug = 'views/pages';

    public static function common()
    {            
        return new Common();
    }  

    public static function url( $path = false, $echo = false )
    {
        $outURL = Config::siteURL($path);

        if($echo) {
            echo $outURL;
        } else {
            return $outURL; 
        }
    }
    
    public static function photo($thefile=false,$a=false,$c=false,$i=false,$s=false,$e=true)
    {
        $file = ($thefile) ? $thefile : '';
        $fileUrl = self::url(APPURI.'/views/assets/'.$file);
        $return = file_exists(APPPATH.'/views/assets/'.$file) ? $fileUrl : false;

        if($return) {
            $alt = ($a) ? 'alt="'.$a.'"' : '';
            $cls = ($c) ? 'class="'.$c.'"' : '';
            $id = ($i) ? 'id="'.$i.'"' : '';
            $style = ($s) ? 'style="'.$s.'"' : '';
            
            if($e) {
                echo '<img src="'.$return.'" '.$alt.' '.$cls.' '.$id.' '.$style.'>';
            } else {
                return '<img src="'.$return.'" '.$alt.' '.$cls.' '.$id.' '.$style.'>';
            }
        } else {
            return false;
        }
    }

    public static function image($f=false,$a=false,$c=false,$i=false,$s=false,$e=true)
    {
        self::photo('images/'.$f,$a,$c,$i,$s,$e);
    }

    public static function asset($thefile=false,$echo=false)
    {
        $file = ($thefile) ? $thefile : '';
        $fileUrl = self::url(APPURI.'/views/assets/'.$file);
        $return = file_exists(APPPATH.'/views/assets/'.$file) ? $fileUrl : false;
        if($echo) {
            echo $return;
        } else {
            return $return;
        }
    }
    
    public static function header($folder=false,$headerfile ='')
    {        
        $fold = ($folder) ? DS.$folder : '';
        $headerFile = ($headerfile!='')?$headerfile:'header';
        self::fetch(APPPATH.self::$viewpageslug.$fold.DS.$headerFile.DOT.Config::get('FILE_EXT'), 'ro');
    }
    
    public static function footer($folder=false)
    {
        $fold = ($folder) ? DS.$folder : '';
        self::fetch(APPPATH.self::$viewpageslug.$fold.DS.'footer'.DOT.Config::get('FILE_EXT'), 'ro');
    }
    
    public static function sidebar($folder=false)
    {
        $fold = ($folder) ? DS.$folder : '';
        self::fetch(APPPATH.self::$viewpageslug.$fold.DS.'sidebar'.DOT.Config::get('FILE_EXT'), 'ro');
    }

    public static function template($filename = NULL, $data = array(), $inctype = 'ro')
    {
        self::fetch(APPPATH.self::$viewpageslug.DS.$filename.DOT.Config::get('FILE_EXT'), $inctype, $data);
    }
    
    public static function page($filename = NULL, $data = array())
    {
        self::fetch(APPPATH.self::$viewpageslug.DS.$filename.DOT.Config::get('FILE_EXT'), 'ro', $data);
    }

    public static function content($filename = NULL, $data = array())
    {
        self::fetch(APPPATH.self::$viewpageslug.DS.$filename.DOT.Config::get('FILE_EXT'), 'ro', $data);
    }
    
    public static function vendor($filename = NULL)
    {
        self::fetch(APPPATH.'vendors'.DS.$filename.DOT.Config::get('FILE_EXT'), 'ro');
    }
        
    public static function fetch($file = NULL, $load = 'ro', $data = array())
    {
        if(file_exists($file))
        {
            extract($data);
            
            switch(strtolower($load))
            {
                case 'require':
                case 'r':
                {
                        require($file);
                } break;
                case 'require_once':
                case 'ro':
                {
                        require_once($file);
                } break;
                case 'include':
                case 'i':
                {
                        include($file);
                }
                case 'include_once':
                case 'io':
                {
                        include_once($file);
                }
            }

            return true;
        }
        else 
        {
            return false;
        }
    }
    
    public static function reset( $t )
    {
        self::$$t = array();
    }
    
    public static function style( $path = false )
    {
        $environment = strtolower(Config::get('ENVIRONMENT'));
        $url = ($path) ? RDS.$path : '';
        
        echo '<link href="'.self::url(APPURI.'/views'.$url).'" rel="stylesheet" />';
    }
    
    public static function styles( $paths = array() )
    {
        $environment = strtolower(Config::get('ENVIRONMENT'));
        if(count($paths)) {
            foreach($paths as $path) {
                $url = ($path) ? RDS.$path : '';        
                echo '<link href="'.self::url(APPURI.'/views'.$url).'" rel="stylesheet" />';
            }
        }
    }
    
    public static function script( $path = false )
    {
        $environment = strtolower(Config::get('ENVIRONMENT'));
        $url = ($path) ? RDS.$path : '';
        
        echo '<script src="'.self::url(APPURI.'/views'.$url).'" type="text/javascript"></script>';
    }
    
    public static function scripts( $paths = array() )
    {
        $environment = strtolower(Config::get('ENVIRONMENT'));
        if(count($paths)) {
            foreach($paths as $path) {
                $url = ($path) ? RDS.$path : '';        
                echo '<script src="'.self::url(APPURI.'/views'.$url).'" type="text/javascript"></script>';
            }
        }
    }
    
    public static function headers()
    {
        Assets::header();
    }
    
    public static function footers()
    {
        Assets::footer();
    }
    
    public static function redirect( $path=false, $referrer = false )
    {
        $slug = '';

        if($path) {
            $environment = strtolower(Config::get('ENVIRONMENT'));
            $slug = ($path) ? $path : '';        
        }

        $url = Config::siteURL($slug);

        if($referrer) {
            $referrerurl = self::common()->getSession('referrer');
            $url = ($referrerurl) ? $referrerurl : Config::siteURL($slug);
            header('location: '.$url);
        }

        header('location: '.$url);
    }

    public static function referrer()
    {
        self::common()->setSession( 'referrer', Config::siteURL( trim($_SERVER['REQUEST_URI'], "/") ) );
    }
    
    public static function clientinfo()
    {
        return self::common()->getSession( 'CLIENTINFO' ) != NULL ? self::common()->getSession( 'CLIENTINFO' ) : false;
    }
    
    public static function getPreview()
    {
        return self::common()->getSession( 'preview' ) != NULL ? self::common()->getSession( 'preview' ) : '';
    }
    
    public static function getError()
    {
        return self::common()->getSession( 'error' );
    }
    
    public static function getReferrer()
    {
        return self::common()->getSession( 'referrer' );
    }
    
    public static function getMessage($echo = false)
    {
        $e = self::common()->getSession('error');
        $m = self::common()->getSession('message');
        $n = self::common()->getSession('notice');

        $r = '';
        if($e) {
            $r .= '<div class="error alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$e.'</div>';
            self::common()->setSession('error',false);
        } 
        
        if($m) {
            $r .= '<div class="message alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$m.'</div>';
            self::common()->setSession('message',false);
        }

        if($n) {
            $r .= '<div class="notice alert alert-success fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$n.'</div>';
        }

        if($echo) {
            echo $r;
        } else {
            return $r;
        }
    }
    
    public static function getCurrentPage()
    {
        return implode( '/', self::common()->segment );
    }

    public static function inlineCss( $identifier = '#MPF_general_rule', $rule = '' )
    {
        Assets::inline( $identifier, $rule );
    }

     /**
     * Form field.
     * 
     * @since   revised from Mo's framework
     * @access  public
     */	
    public static function form($type = 'text', $args, $e = true) {
        $label = isset($args['label']) ? '<label>'.$args['label'].'</label>' : '';	
        $name = isset($args['name']) ? ' name="'.$args['name'].'"' : '';	
        $value = isset($args['value']) ? ' value="'.$args['value'].'"' : '';	
        $class = isset($args['class']) ? ' class="'.$args['class'].'"' : '';	
        $id = isset($args['id']) ? ' id="'.$args['id'].'"' : ' id="'.$args['name'].'"';	
        $placeholder = isset($args['placeholder']) ? ' placeholder="'.$args['placeholder'].'"' : '';	
        $options = isset($args['options']) ? $args['options'] : array();	
        $rel = isset($args['rel']) ? ' rel="'.$args['rel'].'"' : '';
        $multi = isset($args['multiple']) ? ' multiple="true"' : '';
        $style = isset($args['style']) ? ' style="'.$args['style'].'"' : '';
        $readonly = isset($args['readonly']) ? ' readonly' : '';
        $disabled = isset($args['disabled']) ? ' disabled' : '';
        $inarray = isset($args['inarray']) ? $args['inarray'] : false;
        $custom = isset($args['custom']) ? $args['custom'] : false;
        $required = isset($args['required']) ? $args['required'] : false;

        switch($type){	
            case 'hidden':	
                    $return = $label.'<input type="hidden"'.$name.$value.$class.$id.' '.$custom.' />';	
            break;

            case 'text':	
                    $return = $label.'<input type="text"'.$name.$value.$class.$id.$rel.$placeholder.$style.$readonly.$disabled.' '.$custom.' />';	
            break;	
            case 'number':    
                    $return = $label.'<input type="number"'.$name.$value.$class.$id.$rel.$placeholder.$style.$readonly.$disabled.' '.$custom.' />'; 
            break;
            case 'textarea':	
                    $thevalue = isset($args['value']) ? $args['value'] : '';
                    $return = $label.'<textarea'.$name.$class.$id.$placeholder.$style.$readonly.$disabled.' '.$custom.' >'.stripslashes($thevalue).'</textarea>';	
            break;	
            case 'select':	
                    $return = $label.'<select'.$name.$id.$class.$rel.$multi.$style.$readonly.$disabled.' '.$custom.' '.$required.' >';	
                    foreach($options as $option) {	
                        $val = explode(':', $option);	
                        $thevalue = isset($args['value']) ? $args['value'] : '';
                        $sel = $thevalue == $val[0] ? 'selected="selected"' : '';					
                        if(count($val) > 1) {	
                            $return .= '<option value="'.$val[0].'" '.$sel.'>'.$val[1].'</option>';	
                        } else {	
                            $return .= '<option value="'.$val[0].'" '.$sel.'>'.$val[0].'</option>';	
                        }	
                    }	
                    $return .= '</select>';	
            break;
            case 'selecta':	
                    $return = $label.'<select'.$name.$id.$class.$rel.$multi.$style.$readonly.$disabled.' '.$custom.' '.$required.'>';	
                    $options = (array) $options;
                    foreach($options as $k => $v) {	
                        $thevalue = isset($args['value']) ? $args['value'] : '';
                        $sel = $thevalue == $k ? 'selected="selected"' : '';
                        if($inarray) {
                            if( (count($inarray) && in_array($k,$inarray)) || count($inarray) < 1 ) {
                                $return .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';		
                            }
                        } else {
                            $return .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
                        }
                    }	
                    $return .= '</select>';	
            break;
            case 'upload':
                $return = '<input '.$id.' class="file" type="file" data-min-file-count="0" '.$name.' data-show-upload="false" data-allowed-file-extensions=\'["jpeg","png","jpg"]\'>';
            break;
        }
        $return = $type != 'hidden' ? $return.'<div class="clearfix"></div>' : $return;
        
        if($e) {
            echo $return;
        } else {
            return $return;	
        }
    }
}